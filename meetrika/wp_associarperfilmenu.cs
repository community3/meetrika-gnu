/*
               File: WP_AssociarPerfilMenu
        Description: Perfis de Usu�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:34:52.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarperfilmenu : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarperfilmenu( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarperfilmenu( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Menu_Codigo )
      {
         this.AV7Menu_Codigo = aP0_Menu_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7Menu_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Menu_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMENU_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Menu_Codigo), "ZZZZZ9")));
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA642( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS642( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE642( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Perfis de Usu�rio") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117345297");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarperfilmenu.aspx") + "?" + UrlEncode("" +AV7Menu_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMENU_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Menu_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm642( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarPerfilMenu" ;
      }

      public override String GetPgmdesc( )
      {
         return "Perfis de Usu�rio" ;
      }

      protected void WB640( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_642( true) ;
         }
         else
         {
            wb_table1_2_642( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_642e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV16AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarPerfilMenu.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV17NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarPerfilMenu.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV18AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarPerfilMenu.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV19NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarPerfilMenu.htm");
         }
         wbLoad = true;
      }

      protected void START642( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Perfis de Usu�rio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP640( ) ;
      }

      protected void WS642( )
      {
         START642( ) ;
         EVT642( ) ;
      }

      protected void EVT642( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11642 */
                           E11642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12642 */
                           E12642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13642 */
                                 E13642 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14642 */
                           E14642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15642 */
                           E15642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16642 */
                           E16642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17642 */
                           E17642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18642 */
                           E18642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19642 */
                           E19642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20642 */
                           E20642 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18642 */
                           E18642 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19642 */
                           E19642 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE642( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm642( ) ;
            }
         }
      }

      protected void PA642( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV25AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF642( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF642( )
      {
         /* Execute user event: E12642 */
         E12642 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00642 */
            pr_default.execute(0, new Object[] {AV7Menu_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A277Menu_Codigo = H00642_A277Menu_Codigo[0];
               A278Menu_Nome = H00642_A278Menu_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_MENU_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
               /* Execute user event: E20642 */
               E20642 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB640( ) ;
         }
      }

      protected void STRUP640( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11642 */
         E11642 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A278Menu_Nome = cgiGet( edtMenu_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_MENU_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV25AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)));
            AV16AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV17NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV18AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
            AV19NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11642 */
         E11642 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11642( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV29GXLvl6 = 0;
            /* Using cursor H00643 */
            pr_default.execute(1, new Object[] {AV7Menu_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A277Menu_Codigo = H00643_A277Menu_Codigo[0];
               AV29GXLvl6 = 1;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV29GXLvl6 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H00644 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A7Perfil_AreaTrabalhoCod = H00644_A7Perfil_AreaTrabalhoCod[0];
               A3Perfil_Codigo = H00644_A3Perfil_Codigo[0];
               A276Perfil_Ativo = H00644_A276Perfil_Ativo[0];
               A4Perfil_Nome = H00644_A4Perfil_Nome[0];
               AV10Exist = false;
               /* Using cursor H00645 */
               pr_default.execute(3, new Object[] {AV7Menu_Codigo, A3Perfil_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A277Menu_Codigo = H00645_A277Menu_Codigo[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               AV13Description = A4Perfil_Nome;
               if ( AV10Exist )
               {
                  AV20AddedKeyList.Add(A3Perfil_Codigo, 0);
                  AV22AddedDscList.Add(AV13Description, 0);
               }
               else
               {
                  AV21NotAddedKeyList.Add(A3Perfil_Codigo, 0);
                  AV23NotAddedDscList.Add(AV13Description, 0);
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
      }

      protected void E12642( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV14i = 1;
         AV32GXV1 = 1;
         while ( AV32GXV1 <= AV20AddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV32GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV32GXV1 = (int)(AV32GXV1+1);
         }
         AV14i = 1;
         AV33GXV2 = 1;
         while ( AV33GXV2 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV33GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV14i = (int)(AV14i+1);
            AV33GXV2 = (int)(AV33GXV2+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E13642 */
         E13642 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13642( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV12Success = true;
         AV34GXV3 = 1;
         while ( AV34GXV3 <= AV20AddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV34GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            if ( AV12Success )
            {
               AV10Exist = false;
               /* Using cursor H00646 */
               pr_default.execute(4, new Object[] {AV7Menu_Codigo, AV8Perfil_Codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A7Perfil_AreaTrabalhoCod = H00646_A7Perfil_AreaTrabalhoCod[0];
                  A3Perfil_Codigo = H00646_A3Perfil_Codigo[0];
                  A277Menu_Codigo = H00646_A277Menu_Codigo[0];
                  A7Perfil_AreaTrabalhoCod = H00646_A7Perfil_AreaTrabalhoCod[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( ! AV10Exist )
               {
                  AV11MenuPerfil = new SdtMenuPerfil(context);
                  AV11MenuPerfil.gxTpr_Menu_codigo = AV7Menu_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11MenuPerfil", AV11MenuPerfil);
                  AV11MenuPerfil.gxTpr_Perfil_codigo = AV8Perfil_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11MenuPerfil", AV11MenuPerfil);
                  AV11MenuPerfil.Save();
                  if ( ! AV11MenuPerfil.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV14i = (int)(AV14i+1);
            AV34GXV3 = (int)(AV34GXV3+1);
         }
         AV14i = 1;
         AV36GXV4 = 1;
         while ( AV36GXV4 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV36GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            if ( AV12Success )
            {
               AV10Exist = false;
               /* Using cursor H00647 */
               pr_default.execute(5, new Object[] {AV7Menu_Codigo, AV8Perfil_Codigo, AV6WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A7Perfil_AreaTrabalhoCod = H00647_A7Perfil_AreaTrabalhoCod[0];
                  A3Perfil_Codigo = H00647_A3Perfil_Codigo[0];
                  A277Menu_Codigo = H00647_A277Menu_Codigo[0];
                  A7Perfil_AreaTrabalhoCod = H00647_A7Perfil_AreaTrabalhoCod[0];
                  AV10Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
               if ( AV10Exist )
               {
                  AV11MenuPerfil = new SdtMenuPerfil(context);
                  AV11MenuPerfil.Load(AV7Menu_Codigo, AV8Perfil_Codigo);
                  if ( AV11MenuPerfil.Success() )
                  {
                     AV11MenuPerfil.Delete();
                  }
                  if ( ! AV11MenuPerfil.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV14i = (int)(AV14i+1);
            AV36GXV4 = (int)(AV36GXV4+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociarPerfilMenu");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E14642( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15642( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16642( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17642( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21NotAddedKeyList = (IGxCollection)(AV20AddedKeyList.Clone());
         AV23NotAddedDscList = (IGxCollection)(AV22AddedDscList.Clone());
         AV22AddedDscList.Clear();
         AV20AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18642( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19642( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV39GXV6 = 1;
         AV38GXV5 = AV11MenuPerfil.GetMessages();
         while ( AV39GXV6 <= AV38GXV5.Count )
         {
            AV15Message = ((SdtMessages_Message)AV38GXV5.Item(AV39GXV6));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV39GXV6 = (int)(AV39GXV6+1);
         }
      }

      protected void S122( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16AddedKeyListXml)) )
         {
            AV22AddedDscList.FromXml(AV18AddedDscListXml, "Collection");
            AV20AddedKeyList.FromXml(AV16AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17NotAddedKeyListXml)) )
         {
            AV21NotAddedKeyList.FromXml(AV17NotAddedKeyListXml, "Collection");
            AV23NotAddedDscList.FromXml(AV19NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV20AddedKeyList.Count > 0 )
         {
            AV16AddedKeyListXml = AV20AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = AV22AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         else
         {
            AV16AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AddedKeyListXml", AV16AddedKeyListXml);
            AV18AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AddedDscListXml", AV18AddedDscListXml);
         }
         if ( AV21NotAddedKeyList.Count > 0 )
         {
            AV17NotAddedKeyListXml = AV21NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = AV23NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
         else
         {
            AV17NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotAddedKeyListXml", AV17NotAddedKeyListXml);
            AV19NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19NotAddedDscListXml", AV19NotAddedDscListXml);
         }
      }

      protected void S162( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV26InsertIndex = 1;
         AV40GXV7 = 1;
         while ( AV40GXV7 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV40GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV8Perfil_Codigo, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV14i = (int)(AV14i+1);
            AV40GXV7 = (int)(AV40GXV7+1);
         }
         AV21NotAddedKeyList.Clear();
         AV23NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV41GXV8 = 1;
         while ( AV41GXV8 <= AV21NotAddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV21NotAddedKeyList.GetNumeric(AV41GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            if ( AV8Perfil_Codigo == AV24NotAssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV41GXV8 = (int)(AV41GXV8+1);
         }
         if ( AV14i <= AV21NotAddedKeyList.Count )
         {
            AV13Description = ((String)AV23NotAddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV22AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22AddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV20AddedKeyList.Add(AV24NotAssociatedRecords, AV26InsertIndex);
            AV22AddedDscList.Add(AV13Description, AV26InsertIndex);
            AV21NotAddedKeyList.RemoveItem(AV14i);
            AV23NotAddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S142( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV14i = 1;
         AV42GXV9 = 1;
         while ( AV42GXV9 <= AV20AddedKeyList.Count )
         {
            AV8Perfil_Codigo = (int)(AV20AddedKeyList.GetNumeric(AV42GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0)));
            if ( AV8Perfil_Codigo == AV25AssociatedRecords )
            {
               if (true) break;
            }
            AV14i = (int)(AV14i+1);
            AV42GXV9 = (int)(AV42GXV9+1);
         }
         if ( AV14i <= AV20AddedKeyList.Count )
         {
            AV13Description = ((String)AV22AddedDscList.Item(AV14i));
            AV26InsertIndex = 1;
            while ( ( AV26InsertIndex <= AV23NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV23NotAddedDscList.Item(AV26InsertIndex)), AV13Description) < 0 ) )
            {
               AV26InsertIndex = (int)(AV26InsertIndex+1);
            }
            AV21NotAddedKeyList.Add(AV25AssociatedRecords, AV26InsertIndex);
            AV23NotAddedDscList.Add(AV13Description, AV26InsertIndex);
            AV20AddedKeyList.RemoveItem(AV14i);
            AV22AddedDscList.RemoveItem(AV14i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E20642( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_642( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_642( true) ;
         }
         else
         {
            wb_table2_8_642( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_642e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table3_16_642( true) ;
         }
         else
         {
            wb_table3_16_642( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_642e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table4_48_642( true) ;
         }
         else
         {
            wb_table4_48_642( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_642e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_642e( true) ;
         }
         else
         {
            wb_table1_2_642e( false) ;
         }
      }

      protected void wb_table4_48_642( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_642e( true) ;
         }
         else
         {
            wb_table4_48_642e( false) ;
         }
      }

      protected void wb_table3_16_642( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_642( true) ;
         }
         else
         {
            wb_table5_19_642( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_642e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_642e( true) ;
         }
         else
         {
            wb_table3_16_642e( false) ;
         }
      }

      protected void wb_table5_19_642( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Perfis N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Perfil Associado", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_AssociarPerfilMenu.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            wb_table6_31_642( true) ;
         }
         else
         {
            wb_table6_31_642( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_642e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WP_AssociarPerfilMenu.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_642e( true) ;
         }
         else
         {
            wb_table5_19_642e( false) ;
         }
      }

      protected void wb_table6_31_642( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_642e( true) ;
         }
         else
         {
            wb_table6_31_642e( false) ;
         }
      }

      protected void wb_table2_8_642( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar ao Menu :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Nome_Internalname, StringUtil.RTrim( A278Menu_Nome), StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Nome_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "NomeMenu30", "left", true, "HLP_WP_AssociarPerfilMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_642e( true) ;
         }
         else
         {
            wb_table2_8_642e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Menu_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Menu_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMENU_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Menu_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA642( ) ;
         WS642( ) ;
         WE642( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117345359");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarperfilmenu.js", "?20203117345360");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtMenu_Nome_Internalname = "MENU_NOME";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtMenu_Nome_Jsonclick = "";
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV16AddedKeyListXml = "";
         AV17NotAddedKeyListXml = "";
         AV18AddedDscListXml = "";
         AV19NotAddedDscListXml = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00642_A277Menu_Codigo = new int[1] ;
         H00642_A278Menu_Nome = new String[] {""} ;
         A278Menu_Nome = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         H00643_A277Menu_Codigo = new int[1] ;
         H00644_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H00644_A3Perfil_Codigo = new int[1] ;
         H00644_A276Perfil_Ativo = new bool[] {false} ;
         H00644_A4Perfil_Nome = new String[] {""} ;
         A4Perfil_Nome = "";
         H00645_A3Perfil_Codigo = new int[1] ;
         H00645_A277Menu_Codigo = new int[1] ;
         AV13Description = "";
         AV20AddedKeyList = new GxSimpleCollection();
         AV22AddedDscList = new GxSimpleCollection();
         AV21NotAddedKeyList = new GxSimpleCollection();
         AV23NotAddedDscList = new GxSimpleCollection();
         H00646_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H00646_A3Perfil_Codigo = new int[1] ;
         H00646_A277Menu_Codigo = new int[1] ;
         AV11MenuPerfil = new SdtMenuPerfil(context);
         H00647_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H00647_A3Perfil_Codigo = new int[1] ;
         H00647_A277Menu_Codigo = new int[1] ;
         AV38GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarperfilmenu__default(),
            new Object[][] {
                new Object[] {
               H00642_A277Menu_Codigo, H00642_A278Menu_Nome
               }
               , new Object[] {
               H00643_A277Menu_Codigo
               }
               , new Object[] {
               H00644_A7Perfil_AreaTrabalhoCod, H00644_A3Perfil_Codigo, H00644_A276Perfil_Ativo, H00644_A4Perfil_Nome
               }
               , new Object[] {
               H00645_A3Perfil_Codigo, H00645_A277Menu_Codigo
               }
               , new Object[] {
               H00646_A7Perfil_AreaTrabalhoCod, H00646_A3Perfil_Codigo, H00646_A277Menu_Codigo
               }
               , new Object[] {
               H00647_A7Perfil_AreaTrabalhoCod, H00647_A3Perfil_Codigo, H00647_A277Menu_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV29GXLvl6 ;
      private short nGXWrapped ;
      private int AV7Menu_Codigo ;
      private int wcpOAV7Menu_Codigo ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int AV24NotAssociatedRecords ;
      private int AV25AssociatedRecords ;
      private int A277Menu_Codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int A3Perfil_Codigo ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV14i ;
      private int AV32GXV1 ;
      private int AV8Perfil_Codigo ;
      private int AV33GXV2 ;
      private int AV34GXV3 ;
      private int AV36GXV4 ;
      private int AV39GXV6 ;
      private int AV26InsertIndex ;
      private int AV40GXV7 ;
      private int AV41GXV8 ;
      private int AV42GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String scmdbuf ;
      private String A278Menu_Nome ;
      private String edtMenu_Nome_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String A4Perfil_Nome ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtMenu_Nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A276Perfil_Ativo ;
      private bool AV10Exist ;
      private bool AV12Success ;
      private String AV16AddedKeyListXml ;
      private String AV17NotAddedKeyListXml ;
      private String AV18AddedDscListXml ;
      private String AV19NotAddedDscListXml ;
      private String AV13Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H00642_A277Menu_Codigo ;
      private String[] H00642_A278Menu_Nome ;
      private int[] H00643_A277Menu_Codigo ;
      private int[] H00644_A7Perfil_AreaTrabalhoCod ;
      private int[] H00644_A3Perfil_Codigo ;
      private bool[] H00644_A276Perfil_Ativo ;
      private String[] H00644_A4Perfil_Nome ;
      private int[] H00645_A3Perfil_Codigo ;
      private int[] H00645_A277Menu_Codigo ;
      private int[] H00646_A7Perfil_AreaTrabalhoCod ;
      private int[] H00646_A3Perfil_Codigo ;
      private int[] H00646_A277Menu_Codigo ;
      private int[] H00647_A7Perfil_AreaTrabalhoCod ;
      private int[] H00647_A3Perfil_Codigo ;
      private int[] H00647_A277Menu_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV21NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV38GXV5 ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private SdtMenuPerfil AV11MenuPerfil ;
      private SdtMessages_Message AV15Message ;
   }

   public class wp_associarperfilmenu__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00642 ;
          prmH00642 = new Object[] {
          new Object[] {"@AV7Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00643 ;
          prmH00643 = new Object[] {
          new Object[] {"@AV7Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00644 ;
          prmH00644 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00645 ;
          prmH00645 = new Object[] {
          new Object[] {"@AV7Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00646 ;
          prmH00646 = new Object[] {
          new Object[] {"@AV7Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Perfil_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00647 ;
          prmH00647 = new Object[] {
          new Object[] {"@AV7Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Perfil_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00642", "SELECT [Menu_Codigo], [Menu_Nome] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @AV7Menu_Codigo ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00642,1,0,true,true )
             ,new CursorDef("H00643", "SELECT [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @AV7Menu_Codigo ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00643,1,0,false,true )
             ,new CursorDef("H00644", "SELECT [Perfil_AreaTrabalhoCod], [Perfil_Codigo], [Perfil_Ativo], [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE ([Perfil_Ativo] = 1) AND ([Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Perfil_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00644,100,0,true,false )
             ,new CursorDef("H00645", "SELECT [Perfil_Codigo], [Menu_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @AV7Menu_Codigo and [Perfil_Codigo] = @Perfil_Codigo ORDER BY [Menu_Codigo], [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00645,1,0,false,true )
             ,new CursorDef("H00646", "SELECT T2.[Perfil_AreaTrabalhoCod], T1.[Perfil_Codigo], T1.[Menu_Codigo] FROM ([MenuPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Menu_Codigo] = @AV7Menu_Codigo and T1.[Perfil_Codigo] = @AV8Perfil_Codigo) AND (T2.[Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T1.[Menu_Codigo], T1.[Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00646,1,0,false,true )
             ,new CursorDef("H00647", "SELECT T2.[Perfil_AreaTrabalhoCod], T1.[Perfil_Codigo], T1.[Menu_Codigo] FROM ([MenuPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Menu_Codigo] = @AV7Menu_Codigo and T1.[Perfil_Codigo] = @AV8Perfil_Codigo) AND (T2.[Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T1.[Menu_Codigo], T1.[Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00647,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
