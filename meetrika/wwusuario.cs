/*
               File: WWUsuario
        Description: Work With Usuarios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:41.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwusuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         dynavAreatrabalho_codigo1 = new GXCombobox();
         cmbavUsuario_ativo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         dynavAreatrabalho_codigo2 = new GXCombobox();
         cmbavUsuario_ativo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         dynavAreatrabalho_codigo3 = new GXCombobox();
         cmbavUsuario_ativo3 = new GXCombobox();
         chkUsuario_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vAREATRABALHO_CODIGO1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvAREATRABALHO_CODIGO10I2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vAREATRABALHO_CODIGO2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvAREATRABALHO_CODIGO20I2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vAREATRABALHO_CODIGO3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvAREATRABALHO_CODIGO30I2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_116 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_116_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_116_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV172AreaTrabalho_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
               AV38Usuario_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Usuario_PessoaNom1", AV38Usuario_PessoaNom1);
               AV110Usuario_CargoNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Usuario_CargoNom1", AV110Usuario_CargoNom1);
               AV73Usuario_PessoaDoc1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_PessoaDoc1", AV73Usuario_PessoaDoc1);
               AV182Usuario_Ativo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182Usuario_Ativo1", AV182Usuario_Ativo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV173AreaTrabalho_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173AreaTrabalho_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)));
               AV39Usuario_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Usuario_PessoaNom2", AV39Usuario_PessoaNom2);
               AV112Usuario_CargoNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112Usuario_CargoNom2", AV112Usuario_CargoNom2);
               AV74Usuario_PessoaDoc2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Usuario_PessoaDoc2", AV74Usuario_PessoaDoc2);
               AV183Usuario_Ativo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183Usuario_Ativo2", AV183Usuario_Ativo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV184AreaTrabalho_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184AreaTrabalho_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)));
               AV40Usuario_PessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Usuario_PessoaNom3", AV40Usuario_PessoaNom3);
               AV185Usuario_CargoNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV185Usuario_CargoNom3", AV185Usuario_CargoNom3);
               AV78Usuario_PessoaDoc3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Usuario_PessoaDoc3", AV78Usuario_PessoaDoc3);
               AV187Usuario_Ativo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV187Usuario_Ativo3", AV187Usuario_Ativo3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV90TFUsuario_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFUsuario_PessoaNom", AV90TFUsuario_PessoaNom);
               AV91TFUsuario_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFUsuario_PessoaNom_Sel", AV91TFUsuario_PessoaNom_Sel);
               AV123TFUsuario_CargoNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123TFUsuario_CargoNom", AV123TFUsuario_CargoNom);
               AV124TFUsuario_CargoNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFUsuario_CargoNom_Sel", AV124TFUsuario_CargoNom_Sel);
               AV86TFUsuario_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFUsuario_Nome", AV86TFUsuario_Nome);
               AV87TFUsuario_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFUsuario_Nome_Sel", AV87TFUsuario_Nome_Sel);
               AV106TFUsuario_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFUsuario_Ativo_Sel", StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0));
               AV92ddo_Usuario_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_Usuario_PessoaNomTitleControlIdToReplace", AV92ddo_Usuario_PessoaNomTitleControlIdToReplace);
               AV125ddo_Usuario_CargoNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125ddo_Usuario_CargoNomTitleControlIdToReplace", AV125ddo_Usuario_CargoNomTitleControlIdToReplace);
               AV88ddo_Usuario_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_Usuario_NomeTitleControlIdToReplace", AV88ddo_Usuario_NomeTitleControlIdToReplace);
               AV107ddo_Usuario_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ddo_Usuario_AtivoTitleControlIdToReplace", AV107ddo_Usuario_AtivoTitleControlIdToReplace);
               AV199PaginaAtual = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV199PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV199PaginaAtual), 4, 0)));
               AV81Usuario_Entidade1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Usuario_Entidade1", AV81Usuario_Entidade1);
               AV82Usuario_Entidade2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Usuario_Entidade2", AV82Usuario_Entidade2);
               AV186Usuario_Entidade3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV186Usuario_Entidade3", AV186Usuario_Entidade3);
               AV250Pgmname = GetNextPar( );
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
               A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1228ContratadaUsuario_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
               A67ContratadaUsuario_ContratadaPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n67ContratadaUsuario_ContratadaPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67ContratadaUsuario_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0)));
               A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
               A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV193Codigos);
               AV192Preview = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV192Preview", StringUtil.LTrim( StringUtil.Str( (decimal)(AV192Preview), 4, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A1073Usuario_CargoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1073Usuario_CargoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1073Usuario_CargoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1073Usuario_CargoCod), 6, 0)));
               A54Usuario_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               AV83Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Usuario_Codigo), 6, 0)));
               A64ContratanteUsuario_ContratanteRaz = GetNextPar( );
               n64ContratanteUsuario_ContratanteRaz = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV204Pessoas);
               A68ContratadaUsuario_ContratadaPessoaNom = GetNextPar( );
               n68ContratadaUsuario_ContratadaPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68ContratadaUsuario_ContratadaPessoaNom", A68ContratadaUsuario_ContratadaPessoaNom);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0I2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0I2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117304234");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwusuario.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM1", StringUtil.RTrim( AV38Usuario_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_CARGONOM1", AV110Usuario_CargoNom1);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOADOC1", AV73Usuario_PessoaDoc1);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_ATIVO1", StringUtil.RTrim( AV182Usuario_Ativo1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM2", StringUtil.RTrim( AV39Usuario_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_CARGONOM2", AV112Usuario_CargoNom2);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOADOC2", AV74Usuario_PessoaDoc2);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_ATIVO2", StringUtil.RTrim( AV183Usuario_Ativo2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM3", StringUtil.RTrim( AV40Usuario_PessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_CARGONOM3", AV185Usuario_CargoNom3);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOADOC3", AV78Usuario_PessoaDoc3);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_ATIVO3", StringUtil.RTrim( AV187Usuario_Ativo3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM", StringUtil.RTrim( AV90TFUsuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM_SEL", StringUtil.RTrim( AV91TFUsuario_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_CARGONOM", AV123TFUsuario_CargoNom);
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_CARGONOM_SEL", AV124TFUsuario_CargoNom_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME", StringUtil.RTrim( AV86TFUsuario_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME_SEL", StringUtil.RTrim( AV87TFUsuario_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_116", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_116), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV189GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV190GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV108DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV108DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_PESSOANOMTITLEFILTERDATA", AV89Usuario_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_PESSOANOMTITLEFILTERDATA", AV89Usuario_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_CARGONOMTITLEFILTERDATA", AV122Usuario_CargoNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_CARGONOMTITLEFILTERDATA", AV122Usuario_CargoNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_NOMETITLEFILTERDATA", AV85Usuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_NOMETITLEFILTERDATA", AV85Usuario_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_ATIVOTITLEFILTERDATA", AV105Usuario_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_ATIVOTITLEFILTERDATA", AV105Usuario_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPAGINAATUAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV199PaginaAtual), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV250Pgmname));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV193Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV193Codigos);
         }
         GxWebStd.gx_hidden_field( context, "vPREVIEW", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV192Preview), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTERAZ", StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOAS", AV204Pessoas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOAS", AV204Pessoas);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM", StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_ENTIDADE", StringUtil.RTrim( A1083Usuario_Entidade));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Caption", StringUtil.RTrim( Ddo_usuario_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_usuario_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cls", StringUtil.RTrim( Ddo_usuario_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_usuario_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_usuario_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_usuario_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_usuario_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Caption", StringUtil.RTrim( Ddo_usuario_cargonom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Tooltip", StringUtil.RTrim( Ddo_usuario_cargonom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Cls", StringUtil.RTrim( Ddo_usuario_cargonom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_cargonom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_cargonom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_cargonom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_cargonom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_cargonom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_cargonom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Sortedstatus", StringUtil.RTrim( Ddo_usuario_cargonom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Includefilter", StringUtil.BoolToStr( Ddo_usuario_cargonom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Filtertype", StringUtil.RTrim( Ddo_usuario_cargonom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_cargonom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_cargonom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Datalisttype", StringUtil.RTrim( Ddo_usuario_cargonom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Datalistproc", StringUtil.RTrim( Ddo_usuario_cargonom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_cargonom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Sortasc", StringUtil.RTrim( Ddo_usuario_cargonom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Sortdsc", StringUtil.RTrim( Ddo_usuario_cargonom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Loadingdata", StringUtil.RTrim( Ddo_usuario_cargonom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Cleanfilter", StringUtil.RTrim( Ddo_usuario_cargonom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Noresultsfound", StringUtil.RTrim( Ddo_usuario_cargonom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_cargonom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Caption", StringUtil.RTrim( Ddo_usuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_usuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cls", StringUtil.RTrim( Ddo_usuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_usuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_usuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_usuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_usuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_usuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_usuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_usuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_usuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_usuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_usuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Caption", StringUtil.RTrim( Ddo_usuario_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_usuario_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Cls", StringUtil.RTrim( Ddo_usuario_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_usuario_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_usuario_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_usuario_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_usuario_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_usuario_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_usuario_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_usuario_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_usuario_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Activeeventkey", StringUtil.RTrim( Ddo_usuario_cargonom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_cargonom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_CARGONOM_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_cargonom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_usuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_usuario_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0I2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0I2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwusuario.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Work With Usuarios" ;
      }

      protected void WB0I0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_0I2( true) ;
         }
         else
         {
            wb_table1_2_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(137, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(138, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_Internalname, StringUtil.RTrim( AV90TFUsuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV90TFUsuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_sel_Internalname, StringUtil.RTrim( AV91TFUsuario_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV91TFUsuario_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_cargonom_Internalname, AV123TFUsuario_CargoNom, StringUtil.RTrim( context.localUtil.Format( AV123TFUsuario_CargoNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_cargonom_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_cargonom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_cargonom_sel_Internalname, AV124TFUsuario_CargoNom_Sel, StringUtil.RTrim( context.localUtil.Format( AV124TFUsuario_CargoNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_cargonom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_cargonom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_Internalname, StringUtil.RTrim( AV86TFUsuario_Nome), StringUtil.RTrim( context.localUtil.Format( AV86TFUsuario_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_sel_Internalname, StringUtil.RTrim( AV87TFUsuario_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV87TFUsuario_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV106TFUsuario_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_CARGONOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Internalname, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, AV88ddo_Usuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", 0, edtavDdo_usuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_ativotitlecontrolidtoreplace_Internalname, AV107ddo_Usuario_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", 0, edtavDdo_usuario_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuario.htm");
         }
         wbLoad = true;
      }

      protected void START0I2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Work With Usuarios", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0I0( ) ;
      }

      protected void WS0I2( )
      {
         START0I2( ) ;
         EVT0I2( ) ;
      }

      protected void EVT0I2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E110I2 */
                              E110I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E120I2 */
                              E120I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_CARGONOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E130I2 */
                              E130I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E140I2 */
                              E140I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E150I2 */
                              E150I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E160I2 */
                              E160I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E170I2 */
                              E170I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E180I2 */
                              E180I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E190I2 */
                              E190I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E200I2 */
                              E200I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E210I2 */
                              E210I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E220I2 */
                              E220I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E230I2 */
                              E230I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E240I2 */
                              E240I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E250I2 */
                              E250I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E260I2 */
                              E260I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E270I2 */
                              E270I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E280I2 */
                              E280I2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_116_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1162( ) ;
                              AV33Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV245Update_GXI : context.convertURL( context.PathToRelativeUrl( AV33Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV246Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV113Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV113Display)) ? AV247Display_GXI : context.convertURL( context.PathToRelativeUrl( AV113Display))));
                              A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                              A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
                              n58Usuario_PessoaNom = false;
                              A1074Usuario_CargoNom = StringUtil.Upper( cgiGet( edtUsuario_CargoNom_Internalname));
                              n1074Usuario_CargoNom = false;
                              A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                              n2Usuario_Nome = false;
                              AV198Entidade = StringUtil.Upper( cgiGet( edtavEntidade_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEntidade_Internalname, AV198Entidade);
                              A54Usuario_Ativo = StringUtil.StrToBool( cgiGet( chkUsuario_Ativo_Internalname));
                              AV31AssociarPerfil = cgiGet( edtavAssociarperfil_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarperfil_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarPerfil)) ? AV248Associarperfil_GXI : context.convertURL( context.PathToRelativeUrl( AV31AssociarPerfil))));
                              AV209AssociarContratadas = cgiGet( edtavAssociarcontratadas_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarcontratadas_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV209AssociarContratadas)) ? AV249Associarcontratadas_GXI : context.convertURL( context.PathToRelativeUrl( AV209AssociarContratadas))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E290I2 */
                                    E290I2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E300I2 */
                                    E300I2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E310I2 */
                                    E310I2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_codigo1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vAREATRABALHO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV172AreaTrabalho_Codigo1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV38Usuario_PessoaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_cargonom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM1"), AV110Usuario_CargoNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoadoc1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC1"), AV73Usuario_PessoaDoc1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_ativo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_ATIVO1"), AV182Usuario_Ativo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_codigo2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vAREATRABALHO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV173AreaTrabalho_Codigo2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV39Usuario_PessoaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_cargonom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM2"), AV112Usuario_CargoNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoadoc2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC2"), AV74Usuario_PessoaDoc2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_ativo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_ATIVO2"), AV183Usuario_Ativo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_codigo3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vAREATRABALHO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV184AreaTrabalho_Codigo3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoanom3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM3"), AV40Usuario_PessoaNom3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_cargonom3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM3"), AV185Usuario_CargoNom3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoadoc3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC3"), AV78Usuario_PessoaDoc3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_ativo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_ATIVO3"), AV187Usuario_Ativo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV90TFUsuario_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_pessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV91TFUsuario_PessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_cargonom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_CARGONOM"), AV123TFUsuario_CargoNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_cargonom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_CARGONOM_SEL"), AV124TFUsuario_CargoNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV86TFUsuario_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV87TFUsuario_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV106TFUsuario_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0I2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0I2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AREATRABALHO_CODIGO", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_PESSOANOM", "Pessoa F�sica", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_CARGONOM", "Cargo", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_PESSOADOC", "Documento", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_ENTIDADE", "Entidade", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_ATIVO", "Ativo?", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            dynavAreatrabalho_codigo1.Name = "vAREATRABALHO_CODIGO1";
            dynavAreatrabalho_codigo1.WebTags = "";
            cmbavUsuario_ativo1.Name = "vUSUARIO_ATIVO1";
            cmbavUsuario_ativo1.WebTags = "";
            cmbavUsuario_ativo1.addItem("", "(Todos)", 0);
            cmbavUsuario_ativo1.addItem("S", "Sim", 0);
            cmbavUsuario_ativo1.addItem("N", "N�o", 0);
            if ( cmbavUsuario_ativo1.ItemCount > 0 )
            {
               AV182Usuario_Ativo1 = cmbavUsuario_ativo1.getValidValue(AV182Usuario_Ativo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182Usuario_Ativo1", AV182Usuario_Ativo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AREATRABALHO_CODIGO", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_PESSOANOM", "Pessoa F�sica", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_CARGONOM", "Cargo", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_PESSOADOC", "Documento", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_ENTIDADE", "Entidade", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_ATIVO", "Ativo?", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            dynavAreatrabalho_codigo2.Name = "vAREATRABALHO_CODIGO2";
            dynavAreatrabalho_codigo2.WebTags = "";
            cmbavUsuario_ativo2.Name = "vUSUARIO_ATIVO2";
            cmbavUsuario_ativo2.WebTags = "";
            cmbavUsuario_ativo2.addItem("", "(Todos)", 0);
            cmbavUsuario_ativo2.addItem("S", "Sim", 0);
            cmbavUsuario_ativo2.addItem("N", "N�o", 0);
            if ( cmbavUsuario_ativo2.ItemCount > 0 )
            {
               AV183Usuario_Ativo2 = cmbavUsuario_ativo2.getValidValue(AV183Usuario_Ativo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183Usuario_Ativo2", AV183Usuario_Ativo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AREATRABALHO_CODIGO", "�rea de Trabalho", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_PESSOANOM", "Pessoa F�sica", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_CARGONOM", "Cargo", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_PESSOADOC", "Documento", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_ENTIDADE", "Entidade", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_ATIVO", "Ativo?", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            dynavAreatrabalho_codigo3.Name = "vAREATRABALHO_CODIGO3";
            dynavAreatrabalho_codigo3.WebTags = "";
            cmbavUsuario_ativo3.Name = "vUSUARIO_ATIVO3";
            cmbavUsuario_ativo3.WebTags = "";
            cmbavUsuario_ativo3.addItem("", "(Todos)", 0);
            cmbavUsuario_ativo3.addItem("S", "Sim", 0);
            cmbavUsuario_ativo3.addItem("N", "N�o", 0);
            if ( cmbavUsuario_ativo3.ItemCount > 0 )
            {
               AV187Usuario_Ativo3 = cmbavUsuario_ativo3.getValidValue(AV187Usuario_Ativo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV187Usuario_Ativo3", AV187Usuario_Ativo3);
            }
            GXCCtl = "USUARIO_ATIVO_" + sGXsfl_116_idx;
            chkUsuario_Ativo.Name = GXCCtl;
            chkUsuario_Ativo.WebTags = "";
            chkUsuario_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "TitleCaption", chkUsuario_Ativo.Caption);
            chkUsuario_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvAREATRABALHO_CODIGO10I2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAREATRABALHO_CODIGO1_data0I2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAREATRABALHO_CODIGO1_html0I2( )
      {
         int gxdynajaxvalue ;
         GXDLVvAREATRABALHO_CODIGO1_data0I2( ) ;
         gxdynajaxindex = 1;
         dynavAreatrabalho_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAreatrabalho_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAreatrabalho_codigo1.ItemCount > 0 )
         {
            AV172AreaTrabalho_Codigo1 = (int)(NumberUtil.Val( dynavAreatrabalho_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
         }
      }

      protected void GXDLVvAREATRABALHO_CODIGO1_data0I2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H000I2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H000I2_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H000I2_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvAREATRABALHO_CODIGO20I2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAREATRABALHO_CODIGO2_data0I2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAREATRABALHO_CODIGO2_html0I2( )
      {
         int gxdynajaxvalue ;
         GXDLVvAREATRABALHO_CODIGO2_data0I2( ) ;
         gxdynajaxindex = 1;
         dynavAreatrabalho_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAreatrabalho_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAreatrabalho_codigo2.ItemCount > 0 )
         {
            AV173AreaTrabalho_Codigo2 = (int)(NumberUtil.Val( dynavAreatrabalho_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173AreaTrabalho_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)));
         }
      }

      protected void GXDLVvAREATRABALHO_CODIGO2_data0I2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H000I3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H000I3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H000I3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvAREATRABALHO_CODIGO30I2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAREATRABALHO_CODIGO3_data0I2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAREATRABALHO_CODIGO3_html0I2( )
      {
         int gxdynajaxvalue ;
         GXDLVvAREATRABALHO_CODIGO3_data0I2( ) ;
         gxdynajaxindex = 1;
         dynavAreatrabalho_codigo3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAreatrabalho_codigo3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAreatrabalho_codigo3.ItemCount > 0 )
         {
            AV184AreaTrabalho_Codigo3 = (int)(NumberUtil.Val( dynavAreatrabalho_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184AreaTrabalho_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)));
         }
      }

      protected void GXDLVvAREATRABALHO_CODIGO3_data0I2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todas)");
         /* Using cursor H000I4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H000I4_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H000I4_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1162( ) ;
         while ( nGXsfl_116_idx <= nRC_GXsfl_116 )
         {
            sendrow_1162( ) ;
            nGXsfl_116_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_116_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_116_idx+1));
            sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
            SubsflControlProps_1162( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       int AV172AreaTrabalho_Codigo1 ,
                                       String AV38Usuario_PessoaNom1 ,
                                       String AV110Usuario_CargoNom1 ,
                                       String AV73Usuario_PessoaDoc1 ,
                                       String AV182Usuario_Ativo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       int AV173AreaTrabalho_Codigo2 ,
                                       String AV39Usuario_PessoaNom2 ,
                                       String AV112Usuario_CargoNom2 ,
                                       String AV74Usuario_PessoaDoc2 ,
                                       String AV183Usuario_Ativo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       int AV184AreaTrabalho_Codigo3 ,
                                       String AV40Usuario_PessoaNom3 ,
                                       String AV185Usuario_CargoNom3 ,
                                       String AV78Usuario_PessoaDoc3 ,
                                       String AV187Usuario_Ativo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV90TFUsuario_PessoaNom ,
                                       String AV91TFUsuario_PessoaNom_Sel ,
                                       String AV123TFUsuario_CargoNom ,
                                       String AV124TFUsuario_CargoNom_Sel ,
                                       String AV86TFUsuario_Nome ,
                                       String AV87TFUsuario_Nome_Sel ,
                                       short AV106TFUsuario_Ativo_Sel ,
                                       String AV92ddo_Usuario_PessoaNomTitleControlIdToReplace ,
                                       String AV125ddo_Usuario_CargoNomTitleControlIdToReplace ,
                                       String AV88ddo_Usuario_NomeTitleControlIdToReplace ,
                                       String AV107ddo_Usuario_AtivoTitleControlIdToReplace ,
                                       short AV199PaginaAtual ,
                                       String AV81Usuario_Entidade1 ,
                                       String AV82Usuario_Entidade2 ,
                                       String AV186Usuario_Entidade3 ,
                                       String AV250Pgmname ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       int A40Contratada_PessoaCod ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       int A67ContratadaUsuario_ContratadaPessoaCod ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int A5AreaTrabalho_Codigo ,
                                       int A60ContratanteUsuario_UsuarioCod ,
                                       int A1Usuario_Codigo ,
                                       IGxCollection AV193Codigos ,
                                       short AV192Preview ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A1073Usuario_CargoCod ,
                                       bool A54Usuario_Ativo ,
                                       int A63ContratanteUsuario_ContratanteCod ,
                                       int A29Contratante_Codigo ,
                                       int AV83Usuario_Codigo ,
                                       String A64ContratanteUsuario_ContratanteRaz ,
                                       IGxCollection AV204Pessoas ,
                                       String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0I2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "USUARIO_NOME", StringUtil.RTrim( A2Usuario_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_ATIVO", GetSecureSignedToken( "", A54Usuario_Ativo));
         GxWebStd.gx_hidden_field( context, "USUARIO_ATIVO", StringUtil.BoolToStr( A54Usuario_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( dynavAreatrabalho_codigo1.ItemCount > 0 )
         {
            AV172AreaTrabalho_Codigo1 = (int)(NumberUtil.Val( dynavAreatrabalho_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
         }
         if ( cmbavUsuario_ativo1.ItemCount > 0 )
         {
            AV182Usuario_Ativo1 = cmbavUsuario_ativo1.getValidValue(AV182Usuario_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182Usuario_Ativo1", AV182Usuario_Ativo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( dynavAreatrabalho_codigo2.ItemCount > 0 )
         {
            AV173AreaTrabalho_Codigo2 = (int)(NumberUtil.Val( dynavAreatrabalho_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173AreaTrabalho_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)));
         }
         if ( cmbavUsuario_ativo2.ItemCount > 0 )
         {
            AV183Usuario_Ativo2 = cmbavUsuario_ativo2.getValidValue(AV183Usuario_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183Usuario_Ativo2", AV183Usuario_Ativo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
         if ( dynavAreatrabalho_codigo3.ItemCount > 0 )
         {
            AV184AreaTrabalho_Codigo3 = (int)(NumberUtil.Val( dynavAreatrabalho_codigo3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184AreaTrabalho_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)));
         }
         if ( cmbavUsuario_ativo3.ItemCount > 0 )
         {
            AV187Usuario_Ativo3 = cmbavUsuario_ativo3.getValidValue(AV187Usuario_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV187Usuario_Ativo3", AV187Usuario_Ativo3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0I2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV250Pgmname = "WWUsuario";
         context.Gx_err = 0;
         edtavEntidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEntidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_Enabled), 5, 0)));
      }

      protected void RF0I2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 116;
         /* Execute user event: E300I2 */
         E300I2 ();
         nGXsfl_116_idx = 1;
         sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
         SubsflControlProps_1162( ) ;
         nGXsfl_116_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1162( ) ;
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A1Usuario_Codigo ,
                                                 AV193Codigos ,
                                                 AV212WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                                 AV215WWUsuarioDS_4_Usuario_pessoanom1 ,
                                                 AV213WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                                 AV216WWUsuarioDS_5_Usuario_cargonom1 ,
                                                 AV217WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                                 AV219WWUsuarioDS_8_Usuario_ativo1 ,
                                                 AV220WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                                 AV221WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                                 AV224WWUsuarioDS_13_Usuario_pessoanom2 ,
                                                 AV222WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                                 AV225WWUsuarioDS_14_Usuario_cargonom2 ,
                                                 AV226WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                                 AV228WWUsuarioDS_17_Usuario_ativo2 ,
                                                 AV229WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                                 AV230WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                                 AV233WWUsuarioDS_22_Usuario_pessoanom3 ,
                                                 AV231WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                                 AV234WWUsuarioDS_23_Usuario_cargonom3 ,
                                                 AV235WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                                 AV237WWUsuarioDS_26_Usuario_ativo3 ,
                                                 AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                                 AV238WWUsuarioDS_27_Tfusuario_pessoanom ,
                                                 AV241WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                                 AV240WWUsuarioDS_29_Tfusuario_cargonom ,
                                                 AV243WWUsuarioDS_32_Tfusuario_nome_sel ,
                                                 AV242WWUsuarioDS_31_Tfusuario_nome ,
                                                 AV244WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                                 AV172AreaTrabalho_Codigo1 ,
                                                 AV173AreaTrabalho_Codigo2 ,
                                                 AV184AreaTrabalho_Codigo3 ,
                                                 A58Usuario_PessoaNom ,
                                                 A1074Usuario_CargoNom ,
                                                 A325Usuario_PessoaDoc ,
                                                 A54Usuario_Ativo ,
                                                 A2Usuario_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV218WWUsuarioDS_7_Usuario_entidade1 ,
                                                 A1083Usuario_Entidade ,
                                                 AV227WWUsuarioDS_16_Usuario_entidade2 ,
                                                 AV236WWUsuarioDS_25_Usuario_entidade3 },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                                 }
            });
            lV215WWUsuarioDS_4_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV215WWUsuarioDS_4_Usuario_pessoanom1), 100, "%");
            lV216WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1), "%", "");
            lV216WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1), "%", "");
            lV217WWUsuarioDS_6_Usuario_pessoadoc1 = StringUtil.Concat( StringUtil.RTrim( AV217WWUsuarioDS_6_Usuario_pessoadoc1), "%", "");
            lV224WWUsuarioDS_13_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV224WWUsuarioDS_13_Usuario_pessoanom2), 100, "%");
            lV225WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2), "%", "");
            lV225WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2), "%", "");
            lV226WWUsuarioDS_15_Usuario_pessoadoc2 = StringUtil.Concat( StringUtil.RTrim( AV226WWUsuarioDS_15_Usuario_pessoadoc2), "%", "");
            lV233WWUsuarioDS_22_Usuario_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV233WWUsuarioDS_22_Usuario_pessoanom3), 100, "%");
            lV234WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3), "%", "");
            lV234WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3), "%", "");
            lV235WWUsuarioDS_24_Usuario_pessoadoc3 = StringUtil.Concat( StringUtil.RTrim( AV235WWUsuarioDS_24_Usuario_pessoadoc3), "%", "");
            lV238WWUsuarioDS_27_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV238WWUsuarioDS_27_Tfusuario_pessoanom), 100, "%");
            lV240WWUsuarioDS_29_Tfusuario_cargonom = StringUtil.Concat( StringUtil.RTrim( AV240WWUsuarioDS_29_Tfusuario_cargonom), "%", "");
            lV242WWUsuarioDS_31_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV242WWUsuarioDS_31_Tfusuario_nome), 50, "%");
            /* Using cursor H000I5 */
            pr_default.execute(3, new Object[] {A1Usuario_Codigo, lV215WWUsuarioDS_4_Usuario_pessoanom1, lV216WWUsuarioDS_5_Usuario_cargonom1, lV216WWUsuarioDS_5_Usuario_cargonom1, lV217WWUsuarioDS_6_Usuario_pessoadoc1, A1Usuario_Codigo, lV224WWUsuarioDS_13_Usuario_pessoanom2, lV225WWUsuarioDS_14_Usuario_cargonom2, lV225WWUsuarioDS_14_Usuario_cargonom2, lV226WWUsuarioDS_15_Usuario_pessoadoc2, A1Usuario_Codigo, lV233WWUsuarioDS_22_Usuario_pessoanom3, lV234WWUsuarioDS_23_Usuario_cargonom3, lV234WWUsuarioDS_23_Usuario_cargonom3, lV235WWUsuarioDS_24_Usuario_pessoadoc3, lV238WWUsuarioDS_27_Tfusuario_pessoanom, AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel, lV240WWUsuarioDS_29_Tfusuario_cargonom, AV241WWUsuarioDS_30_Tfusuario_cargonom_sel, lV242WWUsuarioDS_31_Tfusuario_nome, AV243WWUsuarioDS_32_Tfusuario_nome_sel});
            nGXsfl_116_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(3) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A57Usuario_PessoaCod = H000I5_A57Usuario_PessoaCod[0];
               A325Usuario_PessoaDoc = H000I5_A325Usuario_PessoaDoc[0];
               n325Usuario_PessoaDoc = H000I5_n325Usuario_PessoaDoc[0];
               A1073Usuario_CargoCod = H000I5_A1073Usuario_CargoCod[0];
               n1073Usuario_CargoCod = H000I5_n1073Usuario_CargoCod[0];
               A54Usuario_Ativo = H000I5_A54Usuario_Ativo[0];
               A2Usuario_Nome = H000I5_A2Usuario_Nome[0];
               n2Usuario_Nome = H000I5_n2Usuario_Nome[0];
               A1074Usuario_CargoNom = H000I5_A1074Usuario_CargoNom[0];
               n1074Usuario_CargoNom = H000I5_n1074Usuario_CargoNom[0];
               A58Usuario_PessoaNom = H000I5_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H000I5_n58Usuario_PessoaNom[0];
               A1Usuario_Codigo = H000I5_A1Usuario_Codigo[0];
               A325Usuario_PessoaDoc = H000I5_A325Usuario_PessoaDoc[0];
               n325Usuario_PessoaDoc = H000I5_n325Usuario_PessoaDoc[0];
               A58Usuario_PessoaNom = H000I5_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H000I5_n58Usuario_PessoaNom[0];
               A1074Usuario_CargoNom = H000I5_A1074Usuario_CargoNom[0];
               n1074Usuario_CargoNom = H000I5_n1074Usuario_CargoNom[0];
               GXt_char1 = A1083Usuario_Entidade;
               new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
               A1083Usuario_Entidade = GXt_char1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
               if ( ! ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218WWUsuarioDS_7_Usuario_entidade1)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV218WWUsuarioDS_7_Usuario_entidade1) , 255 , "%"),  ' ' ) ) )
               {
                  if ( ! ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227WWUsuarioDS_16_Usuario_entidade2)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV227WWUsuarioDS_16_Usuario_entidade2) , 255 , "%"),  ' ' ) ) )
                  {
                     if ( ! ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236WWUsuarioDS_25_Usuario_entidade3)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV236WWUsuarioDS_25_Usuario_entidade3) , 255 , "%"),  ' ' ) ) )
                     {
                        /* Execute user event: E310I2 */
                        E310I2 ();
                     }
                  }
               }
               pr_default.readNext(3);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(3) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(3);
            wbEnd = 116;
            WB0I0( ) ;
         }
         nGXsfl_116_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0I0( )
      {
         /* Before Start, stand alone formulas. */
         AV250Pgmname = "WWUsuario";
         context.Gx_err = 0;
         edtavEntidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEntidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntidade_Enabled), 5, 0)));
         GXVvAREATRABALHO_CODIGO1_html0I2( ) ;
         GXVvAREATRABALHO_CODIGO2_html0I2( ) ;
         GXVvAREATRABALHO_CODIGO3_html0I2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E290I2 */
         E290I2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV108DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_PESSOANOMTITLEFILTERDATA"), AV89Usuario_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_CARGONOMTITLEFILTERDATA"), AV122Usuario_CargoNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_NOMETITLEFILTERDATA"), AV85Usuario_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_ATIVOTITLEFILTERDATA"), AV105Usuario_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            dynavAreatrabalho_codigo1.Name = dynavAreatrabalho_codigo1_Internalname;
            dynavAreatrabalho_codigo1.CurrentValue = cgiGet( dynavAreatrabalho_codigo1_Internalname);
            AV172AreaTrabalho_Codigo1 = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
            AV38Usuario_PessoaNom1 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Usuario_PessoaNom1", AV38Usuario_PessoaNom1);
            AV110Usuario_CargoNom1 = StringUtil.Upper( cgiGet( edtavUsuario_cargonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Usuario_CargoNom1", AV110Usuario_CargoNom1);
            AV73Usuario_PessoaDoc1 = cgiGet( edtavUsuario_pessoadoc1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_PessoaDoc1", AV73Usuario_PessoaDoc1);
            AV81Usuario_Entidade1 = StringUtil.Upper( cgiGet( edtavUsuario_entidade1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Usuario_Entidade1", AV81Usuario_Entidade1);
            cmbavUsuario_ativo1.Name = cmbavUsuario_ativo1_Internalname;
            cmbavUsuario_ativo1.CurrentValue = cgiGet( cmbavUsuario_ativo1_Internalname);
            AV182Usuario_Ativo1 = cgiGet( cmbavUsuario_ativo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182Usuario_Ativo1", AV182Usuario_Ativo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            dynavAreatrabalho_codigo2.Name = dynavAreatrabalho_codigo2_Internalname;
            dynavAreatrabalho_codigo2.CurrentValue = cgiGet( dynavAreatrabalho_codigo2_Internalname);
            AV173AreaTrabalho_Codigo2 = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173AreaTrabalho_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)));
            AV39Usuario_PessoaNom2 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Usuario_PessoaNom2", AV39Usuario_PessoaNom2);
            AV112Usuario_CargoNom2 = StringUtil.Upper( cgiGet( edtavUsuario_cargonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112Usuario_CargoNom2", AV112Usuario_CargoNom2);
            AV74Usuario_PessoaDoc2 = cgiGet( edtavUsuario_pessoadoc2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Usuario_PessoaDoc2", AV74Usuario_PessoaDoc2);
            AV82Usuario_Entidade2 = StringUtil.Upper( cgiGet( edtavUsuario_entidade2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Usuario_Entidade2", AV82Usuario_Entidade2);
            cmbavUsuario_ativo2.Name = cmbavUsuario_ativo2_Internalname;
            cmbavUsuario_ativo2.CurrentValue = cgiGet( cmbavUsuario_ativo2_Internalname);
            AV183Usuario_Ativo2 = cgiGet( cmbavUsuario_ativo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183Usuario_Ativo2", AV183Usuario_Ativo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            dynavAreatrabalho_codigo3.Name = dynavAreatrabalho_codigo3_Internalname;
            dynavAreatrabalho_codigo3.CurrentValue = cgiGet( dynavAreatrabalho_codigo3_Internalname);
            AV184AreaTrabalho_Codigo3 = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184AreaTrabalho_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)));
            AV40Usuario_PessoaNom3 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Usuario_PessoaNom3", AV40Usuario_PessoaNom3);
            AV185Usuario_CargoNom3 = StringUtil.Upper( cgiGet( edtavUsuario_cargonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV185Usuario_CargoNom3", AV185Usuario_CargoNom3);
            AV78Usuario_PessoaDoc3 = cgiGet( edtavUsuario_pessoadoc3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Usuario_PessoaDoc3", AV78Usuario_PessoaDoc3);
            AV186Usuario_Entidade3 = StringUtil.Upper( cgiGet( edtavUsuario_entidade3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV186Usuario_Entidade3", AV186Usuario_Entidade3);
            cmbavUsuario_ativo3.Name = cmbavUsuario_ativo3_Internalname;
            cmbavUsuario_ativo3.CurrentValue = cgiGet( cmbavUsuario_ativo3_Internalname);
            AV187Usuario_Ativo3 = cgiGet( cmbavUsuario_ativo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV187Usuario_Ativo3", AV187Usuario_Ativo3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV90TFUsuario_PessoaNom = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFUsuario_PessoaNom", AV90TFUsuario_PessoaNom);
            AV91TFUsuario_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFUsuario_PessoaNom_Sel", AV91TFUsuario_PessoaNom_Sel);
            AV123TFUsuario_CargoNom = StringUtil.Upper( cgiGet( edtavTfusuario_cargonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123TFUsuario_CargoNom", AV123TFUsuario_CargoNom);
            AV124TFUsuario_CargoNom_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_cargonom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFUsuario_CargoNom_Sel", AV124TFUsuario_CargoNom_Sel);
            AV86TFUsuario_Nome = StringUtil.Upper( cgiGet( edtavTfusuario_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFUsuario_Nome", AV86TFUsuario_Nome);
            AV87TFUsuario_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFUsuario_Nome_Sel", AV87TFUsuario_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfusuario_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfusuario_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUSUARIO_ATIVO_SEL");
               GX_FocusControl = edtavTfusuario_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV106TFUsuario_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFUsuario_Ativo_Sel", StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0));
            }
            else
            {
               AV106TFUsuario_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfusuario_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFUsuario_Ativo_Sel", StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0));
            }
            AV92ddo_Usuario_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_Usuario_PessoaNomTitleControlIdToReplace", AV92ddo_Usuario_PessoaNomTitleControlIdToReplace);
            AV125ddo_Usuario_CargoNomTitleControlIdToReplace = cgiGet( edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125ddo_Usuario_CargoNomTitleControlIdToReplace", AV125ddo_Usuario_CargoNomTitleControlIdToReplace);
            AV88ddo_Usuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_Usuario_NomeTitleControlIdToReplace", AV88ddo_Usuario_NomeTitleControlIdToReplace);
            AV107ddo_Usuario_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_usuario_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ddo_Usuario_AtivoTitleControlIdToReplace", AV107ddo_Usuario_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_116 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_116"), ",", "."));
            AV189GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV190GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_usuario_pessoanom_Caption = cgiGet( "DDO_USUARIO_PESSOANOM_Caption");
            Ddo_usuario_pessoanom_Tooltip = cgiGet( "DDO_USUARIO_PESSOANOM_Tooltip");
            Ddo_usuario_pessoanom_Cls = cgiGet( "DDO_USUARIO_PESSOANOM_Cls");
            Ddo_usuario_pessoanom_Filteredtext_set = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_set");
            Ddo_usuario_pessoanom_Selectedvalue_set = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_set");
            Ddo_usuario_pessoanom_Dropdownoptionstype = cgiGet( "DDO_USUARIO_PESSOANOM_Dropdownoptionstype");
            Ddo_usuario_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_usuario_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortasc"));
            Ddo_usuario_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortdsc"));
            Ddo_usuario_pessoanom_Sortedstatus = cgiGet( "DDO_USUARIO_PESSOANOM_Sortedstatus");
            Ddo_usuario_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includefilter"));
            Ddo_usuario_pessoanom_Filtertype = cgiGet( "DDO_USUARIO_PESSOANOM_Filtertype");
            Ddo_usuario_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Filterisrange"));
            Ddo_usuario_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includedatalist"));
            Ddo_usuario_pessoanom_Datalisttype = cgiGet( "DDO_USUARIO_PESSOANOM_Datalisttype");
            Ddo_usuario_pessoanom_Datalistproc = cgiGet( "DDO_USUARIO_PESSOANOM_Datalistproc");
            Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_pessoanom_Sortasc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortasc");
            Ddo_usuario_pessoanom_Sortdsc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortdsc");
            Ddo_usuario_pessoanom_Loadingdata = cgiGet( "DDO_USUARIO_PESSOANOM_Loadingdata");
            Ddo_usuario_pessoanom_Cleanfilter = cgiGet( "DDO_USUARIO_PESSOANOM_Cleanfilter");
            Ddo_usuario_pessoanom_Noresultsfound = cgiGet( "DDO_USUARIO_PESSOANOM_Noresultsfound");
            Ddo_usuario_pessoanom_Searchbuttontext = cgiGet( "DDO_USUARIO_PESSOANOM_Searchbuttontext");
            Ddo_usuario_cargonom_Caption = cgiGet( "DDO_USUARIO_CARGONOM_Caption");
            Ddo_usuario_cargonom_Tooltip = cgiGet( "DDO_USUARIO_CARGONOM_Tooltip");
            Ddo_usuario_cargonom_Cls = cgiGet( "DDO_USUARIO_CARGONOM_Cls");
            Ddo_usuario_cargonom_Filteredtext_set = cgiGet( "DDO_USUARIO_CARGONOM_Filteredtext_set");
            Ddo_usuario_cargonom_Selectedvalue_set = cgiGet( "DDO_USUARIO_CARGONOM_Selectedvalue_set");
            Ddo_usuario_cargonom_Dropdownoptionstype = cgiGet( "DDO_USUARIO_CARGONOM_Dropdownoptionstype");
            Ddo_usuario_cargonom_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_CARGONOM_Titlecontrolidtoreplace");
            Ddo_usuario_cargonom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_CARGONOM_Includesortasc"));
            Ddo_usuario_cargonom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_CARGONOM_Includesortdsc"));
            Ddo_usuario_cargonom_Sortedstatus = cgiGet( "DDO_USUARIO_CARGONOM_Sortedstatus");
            Ddo_usuario_cargonom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_CARGONOM_Includefilter"));
            Ddo_usuario_cargonom_Filtertype = cgiGet( "DDO_USUARIO_CARGONOM_Filtertype");
            Ddo_usuario_cargonom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_CARGONOM_Filterisrange"));
            Ddo_usuario_cargonom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_CARGONOM_Includedatalist"));
            Ddo_usuario_cargonom_Datalisttype = cgiGet( "DDO_USUARIO_CARGONOM_Datalisttype");
            Ddo_usuario_cargonom_Datalistproc = cgiGet( "DDO_USUARIO_CARGONOM_Datalistproc");
            Ddo_usuario_cargonom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_CARGONOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_cargonom_Sortasc = cgiGet( "DDO_USUARIO_CARGONOM_Sortasc");
            Ddo_usuario_cargonom_Sortdsc = cgiGet( "DDO_USUARIO_CARGONOM_Sortdsc");
            Ddo_usuario_cargonom_Loadingdata = cgiGet( "DDO_USUARIO_CARGONOM_Loadingdata");
            Ddo_usuario_cargonom_Cleanfilter = cgiGet( "DDO_USUARIO_CARGONOM_Cleanfilter");
            Ddo_usuario_cargonom_Noresultsfound = cgiGet( "DDO_USUARIO_CARGONOM_Noresultsfound");
            Ddo_usuario_cargonom_Searchbuttontext = cgiGet( "DDO_USUARIO_CARGONOM_Searchbuttontext");
            Ddo_usuario_nome_Caption = cgiGet( "DDO_USUARIO_NOME_Caption");
            Ddo_usuario_nome_Tooltip = cgiGet( "DDO_USUARIO_NOME_Tooltip");
            Ddo_usuario_nome_Cls = cgiGet( "DDO_USUARIO_NOME_Cls");
            Ddo_usuario_nome_Filteredtext_set = cgiGet( "DDO_USUARIO_NOME_Filteredtext_set");
            Ddo_usuario_nome_Selectedvalue_set = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_set");
            Ddo_usuario_nome_Dropdownoptionstype = cgiGet( "DDO_USUARIO_NOME_Dropdownoptionstype");
            Ddo_usuario_nome_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_usuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortasc"));
            Ddo_usuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortdsc"));
            Ddo_usuario_nome_Sortedstatus = cgiGet( "DDO_USUARIO_NOME_Sortedstatus");
            Ddo_usuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includefilter"));
            Ddo_usuario_nome_Filtertype = cgiGet( "DDO_USUARIO_NOME_Filtertype");
            Ddo_usuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Filterisrange"));
            Ddo_usuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includedatalist"));
            Ddo_usuario_nome_Datalisttype = cgiGet( "DDO_USUARIO_NOME_Datalisttype");
            Ddo_usuario_nome_Datalistproc = cgiGet( "DDO_USUARIO_NOME_Datalistproc");
            Ddo_usuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_nome_Sortasc = cgiGet( "DDO_USUARIO_NOME_Sortasc");
            Ddo_usuario_nome_Sortdsc = cgiGet( "DDO_USUARIO_NOME_Sortdsc");
            Ddo_usuario_nome_Loadingdata = cgiGet( "DDO_USUARIO_NOME_Loadingdata");
            Ddo_usuario_nome_Cleanfilter = cgiGet( "DDO_USUARIO_NOME_Cleanfilter");
            Ddo_usuario_nome_Noresultsfound = cgiGet( "DDO_USUARIO_NOME_Noresultsfound");
            Ddo_usuario_nome_Searchbuttontext = cgiGet( "DDO_USUARIO_NOME_Searchbuttontext");
            Ddo_usuario_ativo_Caption = cgiGet( "DDO_USUARIO_ATIVO_Caption");
            Ddo_usuario_ativo_Tooltip = cgiGet( "DDO_USUARIO_ATIVO_Tooltip");
            Ddo_usuario_ativo_Cls = cgiGet( "DDO_USUARIO_ATIVO_Cls");
            Ddo_usuario_ativo_Selectedvalue_set = cgiGet( "DDO_USUARIO_ATIVO_Selectedvalue_set");
            Ddo_usuario_ativo_Dropdownoptionstype = cgiGet( "DDO_USUARIO_ATIVO_Dropdownoptionstype");
            Ddo_usuario_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_ATIVO_Titlecontrolidtoreplace");
            Ddo_usuario_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_ATIVO_Includesortasc"));
            Ddo_usuario_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_ATIVO_Includesortdsc"));
            Ddo_usuario_ativo_Sortedstatus = cgiGet( "DDO_USUARIO_ATIVO_Sortedstatus");
            Ddo_usuario_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_ATIVO_Includefilter"));
            Ddo_usuario_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_ATIVO_Includedatalist"));
            Ddo_usuario_ativo_Datalisttype = cgiGet( "DDO_USUARIO_ATIVO_Datalisttype");
            Ddo_usuario_ativo_Datalistfixedvalues = cgiGet( "DDO_USUARIO_ATIVO_Datalistfixedvalues");
            Ddo_usuario_ativo_Sortasc = cgiGet( "DDO_USUARIO_ATIVO_Sortasc");
            Ddo_usuario_ativo_Sortdsc = cgiGet( "DDO_USUARIO_ATIVO_Sortdsc");
            Ddo_usuario_ativo_Cleanfilter = cgiGet( "DDO_USUARIO_ATIVO_Cleanfilter");
            Ddo_usuario_ativo_Searchbuttontext = cgiGet( "DDO_USUARIO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_usuario_pessoanom_Activeeventkey = cgiGet( "DDO_USUARIO_PESSOANOM_Activeeventkey");
            Ddo_usuario_pessoanom_Filteredtext_get = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_get");
            Ddo_usuario_pessoanom_Selectedvalue_get = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_get");
            Ddo_usuario_cargonom_Activeeventkey = cgiGet( "DDO_USUARIO_CARGONOM_Activeeventkey");
            Ddo_usuario_cargonom_Filteredtext_get = cgiGet( "DDO_USUARIO_CARGONOM_Filteredtext_get");
            Ddo_usuario_cargonom_Selectedvalue_get = cgiGet( "DDO_USUARIO_CARGONOM_Selectedvalue_get");
            Ddo_usuario_nome_Activeeventkey = cgiGet( "DDO_USUARIO_NOME_Activeeventkey");
            Ddo_usuario_nome_Filteredtext_get = cgiGet( "DDO_USUARIO_NOME_Filteredtext_get");
            Ddo_usuario_nome_Selectedvalue_get = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_get");
            Ddo_usuario_ativo_Activeeventkey = cgiGet( "DDO_USUARIO_ATIVO_Activeeventkey");
            Ddo_usuario_ativo_Selectedvalue_get = cgiGet( "DDO_USUARIO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAREATRABALHO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV172AreaTrabalho_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV38Usuario_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM1"), AV110Usuario_CargoNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC1"), AV73Usuario_PessoaDoc1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_ATIVO1"), AV182Usuario_Ativo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAREATRABALHO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV173AreaTrabalho_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV39Usuario_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM2"), AV112Usuario_CargoNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC2"), AV74Usuario_PessoaDoc2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_ATIVO2"), AV183Usuario_Ativo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vAREATRABALHO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV184AreaTrabalho_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM3"), AV40Usuario_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM3"), AV185Usuario_CargoNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC3"), AV78Usuario_PessoaDoc3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_ATIVO3"), AV187Usuario_Ativo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV90TFUsuario_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV91TFUsuario_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_CARGONOM"), AV123TFUsuario_CargoNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_CARGONOM_SEL"), AV124TFUsuario_CargoNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV86TFUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV87TFUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV106TFUsuario_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E290I2 */
         E290I2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E290I2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV172AreaTrabalho_Codigo1 = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
         AV192Preview = 25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV192Preview", StringUtil.LTrim( StringUtil.Str( (decimal)(AV192Preview), 4, 0)));
         subGrid_Rows = AV192Preview;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "AREATRABALHO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "AREATRABALHO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "AREATRABALHO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfusuario_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_Visible), 5, 0)));
         edtavTfusuario_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_sel_Visible), 5, 0)));
         edtavTfusuario_cargonom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_cargonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_cargonom_Visible), 5, 0)));
         edtavTfusuario_cargonom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_cargonom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_cargonom_sel_Visible), 5, 0)));
         edtavTfusuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_Visible), 5, 0)));
         edtavTfusuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_sel_Visible), 5, 0)));
         edtavTfusuario_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_ativo_sel_Visible), 5, 0)));
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_usuario_pessoanom_Titlecontrolidtoreplace);
         AV92ddo_Usuario_PessoaNomTitleControlIdToReplace = Ddo_usuario_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_Usuario_PessoaNomTitleControlIdToReplace", AV92ddo_Usuario_PessoaNomTitleControlIdToReplace);
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuario_cargonom_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_CargoNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "TitleControlIdToReplace", Ddo_usuario_cargonom_Titlecontrolidtoreplace);
         AV125ddo_Usuario_CargoNomTitleControlIdToReplace = Ddo_usuario_cargonom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125ddo_Usuario_CargoNomTitleControlIdToReplace", AV125ddo_Usuario_CargoNomTitleControlIdToReplace);
         edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "TitleControlIdToReplace", Ddo_usuario_nome_Titlecontrolidtoreplace);
         AV88ddo_Usuario_NomeTitleControlIdToReplace = Ddo_usuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_Usuario_NomeTitleControlIdToReplace", AV88ddo_Usuario_NomeTitleControlIdToReplace);
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuario_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "TitleControlIdToReplace", Ddo_usuario_ativo_Titlecontrolidtoreplace);
         AV107ddo_Usuario_AtivoTitleControlIdToReplace = Ddo_usuario_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ddo_Usuario_AtivoTitleControlIdToReplace", AV107ddo_Usuario_AtivoTitleControlIdToReplace);
         edtavDdo_usuario_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Usu�rio";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Pessoa", 0);
         cmbavOrderedby.addItem("2", "Pessoa F�sica", 0);
         cmbavOrderedby.addItem("3", "Documento", 0);
         cmbavOrderedby.addItem("4", "Cargo", 0);
         cmbavOrderedby.addItem("5", "Usu�rio", 0);
         cmbavOrderedby.addItem("6", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV108DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV108DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
         AV199PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV199PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV199PaginaAtual), 4, 0)));
      }

      protected void E300I2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV89Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV122Usuario_CargoNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV105Usuario_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuario_PessoaNom_Titleformat = 2;
         edtUsuario_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa", AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Title", edtUsuario_PessoaNom_Title);
         edtUsuario_CargoNom_Titleformat = 2;
         edtUsuario_CargoNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Cargo", AV125ddo_Usuario_CargoNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_CargoNom_Internalname, "Title", edtUsuario_CargoNom_Title);
         edtUsuario_Nome_Titleformat = 2;
         edtUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV88ddo_Usuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Title", edtUsuario_Nome_Title);
         chkUsuario_Ativo_Titleformat = 2;
         chkUsuario_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV107ddo_Usuario_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "Title", chkUsuario_Ativo.Title.Text);
         AV189GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV189GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV189GridCurrentPage), 10, 0)));
         AV190GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV190GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV190GridPageCount), 10, 0)));
         AV189GridCurrentPage = AV199PaginaAtual;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV189GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV189GridCurrentPage), 10, 0)));
         edtavAssociarperfil_Title = "Perfis";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarperfil_Internalname, "Title", edtavAssociarperfil_Title);
         edtavAssociarcontratadas_Title = "Entidades";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarcontratadas_Internalname, "Title", edtavAssociarcontratadas_Title);
         edtavDelete_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Enabled), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         /* Execute user subroutine: 'FILTRAR' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'PAGECOUNT' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV89Usuario_PessoaNomTitleFilterData", AV89Usuario_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV122Usuario_CargoNomTitleFilterData", AV122Usuario_CargoNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV85Usuario_NomeTitleFilterData", AV85Usuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV105Usuario_AtivoTitleFilterData", AV105Usuario_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV204Pessoas", AV204Pessoas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV193Codigos", AV193Codigos);
      }

      protected void E110I2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         GXt_int3 = AV199PaginaAtual;
         new prc_currentpage(context ).execute(  Gridpaginationbar_Selectedpage, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gridpaginationbar_Internalname, "SelectedPage", Gridpaginationbar_Selectedpage);
         AV199PaginaAtual = (short)(GXt_int3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV199PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV199PaginaAtual), 4, 0)));
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV188PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV188PageToGo) ;
         }
      }

      protected void E120I2( )
      {
         /* Ddo_usuario_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV90TFUsuario_PessoaNom = Ddo_usuario_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFUsuario_PessoaNom", AV90TFUsuario_PessoaNom);
            AV91TFUsuario_PessoaNom_Sel = Ddo_usuario_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFUsuario_PessoaNom_Sel", AV91TFUsuario_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E130I2( )
      {
         /* Ddo_usuario_cargonom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_cargonom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_cargonom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "SortedStatus", Ddo_usuario_cargonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_cargonom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_cargonom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "SortedStatus", Ddo_usuario_cargonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_cargonom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV123TFUsuario_CargoNom = Ddo_usuario_cargonom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123TFUsuario_CargoNom", AV123TFUsuario_CargoNom);
            AV124TFUsuario_CargoNom_Sel = Ddo_usuario_cargonom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFUsuario_CargoNom_Sel", AV124TFUsuario_CargoNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E140I2( )
      {
         /* Ddo_usuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV86TFUsuario_Nome = Ddo_usuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFUsuario_Nome", AV86TFUsuario_Nome);
            AV87TFUsuario_Nome_Sel = Ddo_usuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFUsuario_Nome_Sel", AV87TFUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E150I2( )
      {
         /* Ddo_usuario_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "SortedStatus", Ddo_usuario_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "SortedStatus", Ddo_usuario_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV106TFUsuario_Ativo_Sel = (short)(NumberUtil.Val( Ddo_usuario_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFUsuario_Ativo_Sel", StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E310I2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1Usuario_Codigo);
            AV33Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV33Update);
            AV245Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV33Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV33Update);
            AV245Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1Usuario_Codigo);
            AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV246Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV32Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV246Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV113Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV113Display);
            AV247Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV113Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV113Display);
            AV247Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         AV31AssociarPerfil = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociarperfil_Internalname, AV31AssociarPerfil);
         AV248Associarperfil_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarperfil_Tooltiptext = "Clique aqui p/ associar os perfis deste usu�rio!";
         AV209AssociarContratadas = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociarcontratadas_Internalname, AV209AssociarContratadas);
         AV249Associarcontratadas_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarcontratadas_Tooltiptext = "Associar a Contratadas";
         edtUsuario_PessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtUsuario_CargoNom_Link = formatLink("viewgeral_cargo.aspx") + "?" + UrlEncode("" +A1073Usuario_CargoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A54Usuario_Ativo )
         {
            AV208Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV208Color = GXUtil.RGB( 255, 0, 0);
         }
         edtUsuario_PessoaNom_Forecolor = (int)(AV208Color);
         edtUsuario_CargoNom_Forecolor = (int)(AV208Color);
         edtUsuario_Nome_Forecolor = (int)(AV208Color);
         edtavEntidade_Forecolor = (int)(AV208Color);
         AV83Usuario_Codigo = A1Usuario_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83Usuario_Codigo), 6, 0)));
         /* Execute user subroutine: 'ENTIDADE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ! AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 116;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_1162( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_116_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(116, GridRow);
         }
      }

      protected void E160I2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E240I2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E170I2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavAreatrabalho_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Values", dynavAreatrabalho_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavAreatrabalho_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Values", dynavAreatrabalho_codigo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavUsuario_ativo1.CurrentValue = StringUtil.RTrim( AV182Usuario_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Values", cmbavUsuario_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavAreatrabalho_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Values", dynavAreatrabalho_codigo1.ToJavascriptSource());
         cmbavUsuario_ativo2.CurrentValue = StringUtil.RTrim( AV183Usuario_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Values", cmbavUsuario_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavUsuario_ativo3.CurrentValue = StringUtil.RTrim( AV187Usuario_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Values", cmbavUsuario_ativo3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E250I2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E260I2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E180I2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavAreatrabalho_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Values", dynavAreatrabalho_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavAreatrabalho_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Values", dynavAreatrabalho_codigo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavUsuario_ativo1.CurrentValue = StringUtil.RTrim( AV182Usuario_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Values", cmbavUsuario_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavAreatrabalho_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Values", dynavAreatrabalho_codigo1.ToJavascriptSource());
         cmbavUsuario_ativo2.CurrentValue = StringUtil.RTrim( AV183Usuario_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Values", cmbavUsuario_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavUsuario_ativo3.CurrentValue = StringUtil.RTrim( AV187Usuario_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Values", cmbavUsuario_ativo3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E270I2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E190I2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV172AreaTrabalho_Codigo1, AV38Usuario_PessoaNom1, AV110Usuario_CargoNom1, AV73Usuario_PessoaDoc1, AV182Usuario_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV173AreaTrabalho_Codigo2, AV39Usuario_PessoaNom2, AV112Usuario_CargoNom2, AV74Usuario_PessoaDoc2, AV183Usuario_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV184AreaTrabalho_Codigo3, AV40Usuario_PessoaNom3, AV185Usuario_CargoNom3, AV78Usuario_PessoaDoc3, AV187Usuario_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV90TFUsuario_PessoaNom, AV91TFUsuario_PessoaNom_Sel, AV123TFUsuario_CargoNom, AV124TFUsuario_CargoNom_Sel, AV86TFUsuario_Nome, AV87TFUsuario_Nome_Sel, AV106TFUsuario_Ativo_Sel, AV92ddo_Usuario_PessoaNomTitleControlIdToReplace, AV125ddo_Usuario_CargoNomTitleControlIdToReplace, AV88ddo_Usuario_NomeTitleControlIdToReplace, AV107ddo_Usuario_AtivoTitleControlIdToReplace, AV199PaginaAtual, AV81Usuario_Entidade1, AV82Usuario_Entidade2, AV186Usuario_Entidade3, AV250Pgmname, A52Contratada_AreaTrabalhoCod, A40Contratada_PessoaCod, A1228ContratadaUsuario_AreaTrabalhoCod, A67ContratadaUsuario_ContratadaPessoaCod, A69ContratadaUsuario_UsuarioCod, A5AreaTrabalho_Codigo, A60ContratanteUsuario_UsuarioCod, A1Usuario_Codigo, AV193Codigos, AV192Preview, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A1073Usuario_CargoCod, A54Usuario_Ativo, A63ContratanteUsuario_ContratanteCod, A29Contratante_Codigo, AV83Usuario_Codigo, A64ContratanteUsuario_ContratanteRaz, AV204Pessoas, A68ContratadaUsuario_ContratadaPessoaNom) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavAreatrabalho_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Values", dynavAreatrabalho_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavAreatrabalho_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Values", dynavAreatrabalho_codigo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavUsuario_ativo1.CurrentValue = StringUtil.RTrim( AV182Usuario_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Values", cmbavUsuario_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavAreatrabalho_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Values", dynavAreatrabalho_codigo1.ToJavascriptSource());
         cmbavUsuario_ativo2.CurrentValue = StringUtil.RTrim( AV183Usuario_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Values", cmbavUsuario_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavUsuario_ativo3.CurrentValue = StringUtil.RTrim( AV187Usuario_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Values", cmbavUsuario_ativo3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E280I2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E200I2( )
      {
         /* 'DoCleanFilters' Routine */
         AV199PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV199PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV199PaginaAtual), 4, 0)));
         /* Execute user subroutine: 'CLEANFILTERS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavAreatrabalho_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Values", dynavAreatrabalho_codigo1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavAreatrabalho_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Values", dynavAreatrabalho_codigo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavAreatrabalho_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Values", dynavAreatrabalho_codigo3.ToJavascriptSource());
         cmbavUsuario_ativo1.CurrentValue = StringUtil.RTrim( AV182Usuario_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Values", cmbavUsuario_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavUsuario_ativo2.CurrentValue = StringUtil.RTrim( AV183Usuario_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Values", cmbavUsuario_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavUsuario_ativo3.CurrentValue = StringUtil.RTrim( AV187Usuario_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Values", cmbavUsuario_ativo3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_usuario_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         Ddo_usuario_cargonom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "SortedStatus", Ddo_usuario_cargonom_Sortedstatus);
         Ddo_usuario_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         Ddo_usuario_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "SortedStatus", Ddo_usuario_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_usuario_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_usuario_cargonom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "SortedStatus", Ddo_usuario_cargonom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_usuario_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_usuario_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "SortedStatus", Ddo_usuario_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         dynavAreatrabalho_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo1.Visible), 5, 0)));
         edtavUsuario_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         edtavUsuario_cargonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom1_Visible), 5, 0)));
         edtavUsuario_pessoadoc1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc1_Visible), 5, 0)));
         edtavUsuario_entidade1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_entidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_entidade1_Visible), 5, 0)));
         cmbavUsuario_ativo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_ativo1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_CODIGO") == 0 )
         {
            dynavAreatrabalho_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 )
         {
            edtavUsuario_cargonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 )
         {
            edtavUsuario_pessoadoc1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_ENTIDADE") == 0 )
         {
            edtavUsuario_entidade1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_entidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_entidade1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_ATIVO") == 0 )
         {
            cmbavUsuario_ativo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_ativo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         dynavAreatrabalho_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo2.Visible), 5, 0)));
         edtavUsuario_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         edtavUsuario_cargonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom2_Visible), 5, 0)));
         edtavUsuario_pessoadoc2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc2_Visible), 5, 0)));
         edtavUsuario_entidade2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_entidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_entidade2_Visible), 5, 0)));
         cmbavUsuario_ativo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_ativo2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_CODIGO") == 0 )
         {
            dynavAreatrabalho_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 )
         {
            edtavUsuario_cargonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 )
         {
            edtavUsuario_pessoadoc2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_ENTIDADE") == 0 )
         {
            edtavUsuario_entidade2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_entidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_entidade2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_ATIVO") == 0 )
         {
            cmbavUsuario_ativo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_ativo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         dynavAreatrabalho_codigo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo3.Visible), 5, 0)));
         edtavUsuario_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom3_Visible), 5, 0)));
         edtavUsuario_cargonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom3_Visible), 5, 0)));
         edtavUsuario_pessoadoc3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc3_Visible), 5, 0)));
         edtavUsuario_entidade3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_entidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_entidade3_Visible), 5, 0)));
         cmbavUsuario_ativo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_ativo3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_CODIGO") == 0 )
         {
            dynavAreatrabalho_codigo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 )
         {
            edtavUsuario_cargonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 )
         {
            edtavUsuario_pessoadoc3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_ENTIDADE") == 0 )
         {
            edtavUsuario_entidade3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_entidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_entidade3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_ATIVO") == 0 )
         {
            cmbavUsuario_ativo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_ativo3.Visible), 5, 0)));
         }
      }

      protected void S232( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "AREATRABALHO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV173AreaTrabalho_Codigo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173AreaTrabalho_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "AREATRABALHO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV184AreaTrabalho_Codigo3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184AreaTrabalho_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S252( )
      {
         /* 'CLEANFILTERS' Routine */
         AV90TFUsuario_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFUsuario_PessoaNom", AV90TFUsuario_PessoaNom);
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "FilteredText_set", Ddo_usuario_pessoanom_Filteredtext_set);
         AV91TFUsuario_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFUsuario_PessoaNom_Sel", AV91TFUsuario_PessoaNom_Sel);
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SelectedValue_set", Ddo_usuario_pessoanom_Selectedvalue_set);
         AV123TFUsuario_CargoNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123TFUsuario_CargoNom", AV123TFUsuario_CargoNom);
         Ddo_usuario_cargonom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "FilteredText_set", Ddo_usuario_cargonom_Filteredtext_set);
         AV124TFUsuario_CargoNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFUsuario_CargoNom_Sel", AV124TFUsuario_CargoNom_Sel);
         Ddo_usuario_cargonom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "SelectedValue_set", Ddo_usuario_cargonom_Selectedvalue_set);
         AV86TFUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFUsuario_Nome", AV86TFUsuario_Nome);
         Ddo_usuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
         AV87TFUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFUsuario_Nome_Sel", AV87TFUsuario_Nome_Sel);
         Ddo_usuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
         AV106TFUsuario_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFUsuario_Ativo_Sel", StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0));
         Ddo_usuario_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "SelectedValue_set", Ddo_usuario_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "AREATRABALHO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV172AreaTrabalho_Codigo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV250Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV250Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV250Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S262 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S262( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV251GXV1 = 1;
         while ( AV251GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV251GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV90TFUsuario_PessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFUsuario_PessoaNom", AV90TFUsuario_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90TFUsuario_PessoaNom)) )
               {
                  Ddo_usuario_pessoanom_Filteredtext_set = AV90TFUsuario_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "FilteredText_set", Ddo_usuario_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV91TFUsuario_PessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFUsuario_PessoaNom_Sel", AV91TFUsuario_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91TFUsuario_PessoaNom_Sel)) )
               {
                  Ddo_usuario_pessoanom_Selectedvalue_set = AV91TFUsuario_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SelectedValue_set", Ddo_usuario_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_CARGONOM") == 0 )
            {
               AV123TFUsuario_CargoNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123TFUsuario_CargoNom", AV123TFUsuario_CargoNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123TFUsuario_CargoNom)) )
               {
                  Ddo_usuario_cargonom_Filteredtext_set = AV123TFUsuario_CargoNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "FilteredText_set", Ddo_usuario_cargonom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_CARGONOM_SEL") == 0 )
            {
               AV124TFUsuario_CargoNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFUsuario_CargoNom_Sel", AV124TFUsuario_CargoNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124TFUsuario_CargoNom_Sel)) )
               {
                  Ddo_usuario_cargonom_Selectedvalue_set = AV124TFUsuario_CargoNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_cargonom_Internalname, "SelectedValue_set", Ddo_usuario_cargonom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV86TFUsuario_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFUsuario_Nome", AV86TFUsuario_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFUsuario_Nome)) )
               {
                  Ddo_usuario_nome_Filteredtext_set = AV86TFUsuario_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV87TFUsuario_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFUsuario_Nome_Sel", AV87TFUsuario_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFUsuario_Nome_Sel)) )
               {
                  Ddo_usuario_nome_Selectedvalue_set = AV87TFUsuario_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_ATIVO_SEL") == 0 )
            {
               AV106TFUsuario_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106TFUsuario_Ativo_Sel", StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0));
               if ( ! (0==AV106TFUsuario_Ativo_Sel) )
               {
                  Ddo_usuario_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_ativo_Internalname, "SelectedValue_set", Ddo_usuario_ativo_Selectedvalue_set);
               }
            }
            AV251GXV1 = (int)(AV251GXV1+1);
         }
      }

      protected void S242( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_CODIGO") == 0 )
            {
               AV172AreaTrabalho_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172AreaTrabalho_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV38Usuario_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Usuario_PessoaNom1", AV38Usuario_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV110Usuario_CargoNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Usuario_CargoNom1", AV110Usuario_CargoNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 )
            {
               AV73Usuario_PessoaDoc1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_PessoaDoc1", AV73Usuario_PessoaDoc1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_ENTIDADE") == 0 )
            {
               AV81Usuario_Entidade1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Usuario_Entidade1", AV81Usuario_Entidade1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_ATIVO") == 0 )
            {
               AV182Usuario_Ativo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV182Usuario_Ativo1", AV182Usuario_Ativo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_CODIGO") == 0 )
               {
                  AV173AreaTrabalho_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173AreaTrabalho_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV39Usuario_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Usuario_PessoaNom2", AV39Usuario_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV112Usuario_CargoNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112Usuario_CargoNom2", AV112Usuario_CargoNom2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 )
               {
                  AV74Usuario_PessoaDoc2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Usuario_PessoaDoc2", AV74Usuario_PessoaDoc2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_ENTIDADE") == 0 )
               {
                  AV82Usuario_Entidade2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Usuario_Entidade2", AV82Usuario_Entidade2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_ATIVO") == 0 )
               {
                  AV183Usuario_Ativo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV183Usuario_Ativo2", AV183Usuario_Ativo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_CODIGO") == 0 )
                  {
                     AV184AreaTrabalho_Codigo3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV184AreaTrabalho_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
                  {
                     AV40Usuario_PessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Usuario_PessoaNom3", AV40Usuario_PessoaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV185Usuario_CargoNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV185Usuario_CargoNom3", AV185Usuario_CargoNom3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 )
                  {
                     AV78Usuario_PessoaDoc3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Usuario_PessoaDoc3", AV78Usuario_PessoaDoc3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_ENTIDADE") == 0 )
                  {
                     AV186Usuario_Entidade3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV186Usuario_Entidade3", AV186Usuario_Entidade3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_ATIVO") == 0 )
                  {
                     AV187Usuario_Ativo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV187Usuario_Ativo3", AV187Usuario_Ativo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV250Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90TFUsuario_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV90TFUsuario_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91TFUsuario_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV91TFUsuario_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123TFUsuario_CargoNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_CARGONOM";
            AV11GridStateFilterValue.gxTpr_Value = AV123TFUsuario_CargoNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124TFUsuario_CargoNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_CARGONOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV124TFUsuario_CargoNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFUsuario_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV86TFUsuario_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFUsuario_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV87TFUsuario_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV106TFUsuario_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV106TFUsuario_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV250Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S222( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_CODIGO") == 0 ) && ! (0==AV172AreaTrabalho_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Usuario_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV38Usuario_PessoaNom1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV110Usuario_CargoNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV110Usuario_CargoNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Usuario_PessoaDoc1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV73Usuario_PessoaDoc1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_ENTIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV81Usuario_Entidade1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV81Usuario_Entidade1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV182Usuario_Ativo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV182Usuario_Ativo1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_CODIGO") == 0 ) && ! (0==AV173AreaTrabalho_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Usuario_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Usuario_PessoaNom2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV112Usuario_CargoNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV112Usuario_CargoNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV74Usuario_PessoaDoc2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV74Usuario_PessoaDoc2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_ENTIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV82Usuario_Entidade2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV82Usuario_Entidade2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV183Usuario_Ativo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV183Usuario_Ativo2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_CODIGO") == 0 ) && ! (0==AV184AreaTrabalho_Codigo3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Usuario_PessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV40Usuario_PessoaNom3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV185Usuario_CargoNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV185Usuario_CargoNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Usuario_PessoaDoc3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV78Usuario_PessoaDoc3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_ENTIDADE") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV186Usuario_Entidade3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV186Usuario_Entidade3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV187Usuario_Ativo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV187Usuario_Ativo3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV250Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Usuario";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E210I2( )
      {
         /* Areatrabalho_codigo1_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E220I2( )
      {
         /* Areatrabalho_codigo2_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E230I2( )
      {
         /* Areatrabalho_codigo3_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void S182( )
      {
         /* 'FILTRAR' Routine */
         AV204Pessoas.Clear();
         AV193Codigos.Clear();
         if ( AV172AreaTrabalho_Codigo1 + AV173AreaTrabalho_Codigo2 + AV184AreaTrabalho_Codigo3 > 0 )
         {
            pr_default.dynParam(4, new Object[]{ new Object[]{
                                                 AV172AreaTrabalho_Codigo1 ,
                                                 AV173AreaTrabalho_Codigo2 ,
                                                 AV184AreaTrabalho_Codigo3 ,
                                                 A52Contratada_AreaTrabalhoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H000I6 */
            pr_default.execute(4, new Object[] {AV172AreaTrabalho_Codigo1, AV173AreaTrabalho_Codigo2, AV184AreaTrabalho_Codigo3});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A52Contratada_AreaTrabalhoCod = H000I6_A52Contratada_AreaTrabalhoCod[0];
               A40Contratada_PessoaCod = H000I6_A40Contratada_PessoaCod[0];
               AV204Pessoas.Add(A40Contratada_PessoaCod, 0);
               pr_default.readNext(4);
            }
            pr_default.close(4);
            pr_default.dynParam(5, new Object[]{ new Object[]{
                                                 A67ContratadaUsuario_ContratadaPessoaCod ,
                                                 AV204Pessoas ,
                                                 AV172AreaTrabalho_Codigo1 ,
                                                 AV173AreaTrabalho_Codigo2 ,
                                                 AV184AreaTrabalho_Codigo3 ,
                                                 A1228ContratadaUsuario_AreaTrabalhoCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H000I7 */
            pr_default.execute(5, new Object[] {AV172AreaTrabalho_Codigo1, AV173AreaTrabalho_Codigo2, AV184AreaTrabalho_Codigo3});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = H000I7_A66ContratadaUsuario_ContratadaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H000I7_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H000I7_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H000I7_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H000I7_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A69ContratadaUsuario_UsuarioCod = H000I7_A69ContratadaUsuario_UsuarioCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H000I7_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H000I7_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H000I7_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H000I7_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               AV193Codigos.Add(A69ContratadaUsuario_UsuarioCod, 0);
               pr_default.readNext(5);
            }
            pr_default.close(5);
            pr_default.dynParam(6, new Object[]{ new Object[]{
                                                 AV172AreaTrabalho_Codigo1 ,
                                                 AV173AreaTrabalho_Codigo2 ,
                                                 AV184AreaTrabalho_Codigo3 ,
                                                 A5AreaTrabalho_Codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H000I8 */
            pr_default.execute(6, new Object[] {AV172AreaTrabalho_Codigo1, AV173AreaTrabalho_Codigo2, AV184AreaTrabalho_Codigo3});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A29Contratante_Codigo = H000I8_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H000I8_n29Contratante_Codigo[0];
               A72AreaTrabalho_Ativo = H000I8_A72AreaTrabalho_Ativo[0];
               A5AreaTrabalho_Codigo = H000I8_A5AreaTrabalho_Codigo[0];
               /* Using cursor H000I9 */
               pr_default.execute(7, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A63ContratanteUsuario_ContratanteCod = H000I9_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = H000I9_A60ContratanteUsuario_UsuarioCod[0];
                  AV193Codigos.Add(A60ContratanteUsuario_UsuarioCod, 0);
                  pr_default.readNext(7);
               }
               pr_default.close(7);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
      }

      protected void S192( )
      {
         /* 'PAGECOUNT' Routine */
         AV200Registros = 0;
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV213WWUsuarioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV214WWUsuarioDS_3_Areatrabalho_codigo1 = AV172AreaTrabalho_Codigo1;
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = AV38Usuario_PessoaNom1;
         AV216WWUsuarioDS_5_Usuario_cargonom1 = AV110Usuario_CargoNom1;
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = AV73Usuario_PessoaDoc1;
         AV218WWUsuarioDS_7_Usuario_entidade1 = AV81Usuario_Entidade1;
         AV219WWUsuarioDS_8_Usuario_ativo1 = AV182Usuario_Ativo1;
         AV220WWUsuarioDS_9_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV222WWUsuarioDS_11_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV223WWUsuarioDS_12_Areatrabalho_codigo2 = AV173AreaTrabalho_Codigo2;
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = AV39Usuario_PessoaNom2;
         AV225WWUsuarioDS_14_Usuario_cargonom2 = AV112Usuario_CargoNom2;
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = AV74Usuario_PessoaDoc2;
         AV227WWUsuarioDS_16_Usuario_entidade2 = AV82Usuario_Entidade2;
         AV228WWUsuarioDS_17_Usuario_ativo2 = AV183Usuario_Ativo2;
         AV229WWUsuarioDS_18_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV231WWUsuarioDS_20_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV232WWUsuarioDS_21_Areatrabalho_codigo3 = AV184AreaTrabalho_Codigo3;
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = AV40Usuario_PessoaNom3;
         AV234WWUsuarioDS_23_Usuario_cargonom3 = AV185Usuario_CargoNom3;
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = AV78Usuario_PessoaDoc3;
         AV236WWUsuarioDS_25_Usuario_entidade3 = AV186Usuario_Entidade3;
         AV237WWUsuarioDS_26_Usuario_ativo3 = AV187Usuario_Ativo3;
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = AV90TFUsuario_PessoaNom;
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = AV91TFUsuario_PessoaNom_Sel;
         AV240WWUsuarioDS_29_Tfusuario_cargonom = AV123TFUsuario_CargoNom;
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = AV124TFUsuario_CargoNom_Sel;
         AV242WWUsuarioDS_31_Tfusuario_nome = AV86TFUsuario_Nome;
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = AV87TFUsuario_Nome_Sel;
         AV244WWUsuarioDS_33_Tfusuario_ativo_sel = AV106TFUsuario_Ativo_Sel;
         pr_default.dynParam(8, new Object[]{ new Object[]{
                                              A1Usuario_Codigo ,
                                              AV193Codigos ,
                                              AV212WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV215WWUsuarioDS_4_Usuario_pessoanom1 ,
                                              AV213WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV216WWUsuarioDS_5_Usuario_cargonom1 ,
                                              AV217WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                              AV219WWUsuarioDS_8_Usuario_ativo1 ,
                                              AV220WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                              AV221WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                              AV224WWUsuarioDS_13_Usuario_pessoanom2 ,
                                              AV222WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                              AV225WWUsuarioDS_14_Usuario_cargonom2 ,
                                              AV226WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                              AV228WWUsuarioDS_17_Usuario_ativo2 ,
                                              AV229WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                              AV230WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                              AV233WWUsuarioDS_22_Usuario_pessoanom3 ,
                                              AV231WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                              AV234WWUsuarioDS_23_Usuario_cargonom3 ,
                                              AV235WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                              AV237WWUsuarioDS_26_Usuario_ativo3 ,
                                              AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                              AV238WWUsuarioDS_27_Tfusuario_pessoanom ,
                                              AV241WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                              AV240WWUsuarioDS_29_Tfusuario_cargonom ,
                                              AV243WWUsuarioDS_32_Tfusuario_nome_sel ,
                                              AV242WWUsuarioDS_31_Tfusuario_nome ,
                                              AV244WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                              AV172AreaTrabalho_Codigo1 ,
                                              AV173AreaTrabalho_Codigo2 ,
                                              AV184AreaTrabalho_Codigo3 ,
                                              A58Usuario_PessoaNom ,
                                              A1074Usuario_CargoNom ,
                                              A325Usuario_PessoaDoc ,
                                              A54Usuario_Ativo ,
                                              A2Usuario_Nome ,
                                              AV218WWUsuarioDS_7_Usuario_entidade1 ,
                                              A1083Usuario_Entidade ,
                                              AV227WWUsuarioDS_16_Usuario_entidade2 ,
                                              AV236WWUsuarioDS_25_Usuario_entidade3 },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV215WWUsuarioDS_4_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV215WWUsuarioDS_4_Usuario_pessoanom1), 100, "%");
         lV216WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV216WWUsuarioDS_5_Usuario_cargonom1 = StringUtil.Concat( StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1), "%", "");
         lV217WWUsuarioDS_6_Usuario_pessoadoc1 = StringUtil.Concat( StringUtil.RTrim( AV217WWUsuarioDS_6_Usuario_pessoadoc1), "%", "");
         lV224WWUsuarioDS_13_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV224WWUsuarioDS_13_Usuario_pessoanom2), 100, "%");
         lV225WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV225WWUsuarioDS_14_Usuario_cargonom2 = StringUtil.Concat( StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2), "%", "");
         lV226WWUsuarioDS_15_Usuario_pessoadoc2 = StringUtil.Concat( StringUtil.RTrim( AV226WWUsuarioDS_15_Usuario_pessoadoc2), "%", "");
         lV233WWUsuarioDS_22_Usuario_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV233WWUsuarioDS_22_Usuario_pessoanom3), 100, "%");
         lV234WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV234WWUsuarioDS_23_Usuario_cargonom3 = StringUtil.Concat( StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3), "%", "");
         lV235WWUsuarioDS_24_Usuario_pessoadoc3 = StringUtil.Concat( StringUtil.RTrim( AV235WWUsuarioDS_24_Usuario_pessoadoc3), "%", "");
         lV238WWUsuarioDS_27_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV238WWUsuarioDS_27_Tfusuario_pessoanom), 100, "%");
         lV240WWUsuarioDS_29_Tfusuario_cargonom = StringUtil.Concat( StringUtil.RTrim( AV240WWUsuarioDS_29_Tfusuario_cargonom), "%", "");
         lV242WWUsuarioDS_31_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV242WWUsuarioDS_31_Tfusuario_nome), 50, "%");
         /* Using cursor H000I10 */
         pr_default.execute(8, new Object[] {A1Usuario_Codigo, lV215WWUsuarioDS_4_Usuario_pessoanom1, lV216WWUsuarioDS_5_Usuario_cargonom1, lV216WWUsuarioDS_5_Usuario_cargonom1, lV217WWUsuarioDS_6_Usuario_pessoadoc1, A1Usuario_Codigo, lV224WWUsuarioDS_13_Usuario_pessoanom2, lV225WWUsuarioDS_14_Usuario_cargonom2, lV225WWUsuarioDS_14_Usuario_cargonom2, lV226WWUsuarioDS_15_Usuario_pessoadoc2, A1Usuario_Codigo, lV233WWUsuarioDS_22_Usuario_pessoanom3, lV234WWUsuarioDS_23_Usuario_cargonom3, lV234WWUsuarioDS_23_Usuario_cargonom3, lV235WWUsuarioDS_24_Usuario_pessoadoc3, lV238WWUsuarioDS_27_Tfusuario_pessoanom, AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel, lV240WWUsuarioDS_29_Tfusuario_cargonom, AV241WWUsuarioDS_30_Tfusuario_cargonom_sel, lV242WWUsuarioDS_31_Tfusuario_nome, AV243WWUsuarioDS_32_Tfusuario_nome_sel});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A57Usuario_PessoaCod = H000I10_A57Usuario_PessoaCod[0];
            A1073Usuario_CargoCod = H000I10_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = H000I10_n1073Usuario_CargoCod[0];
            A2Usuario_Nome = H000I10_A2Usuario_Nome[0];
            n2Usuario_Nome = H000I10_n2Usuario_Nome[0];
            A54Usuario_Ativo = H000I10_A54Usuario_Ativo[0];
            A325Usuario_PessoaDoc = H000I10_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = H000I10_n325Usuario_PessoaDoc[0];
            A1074Usuario_CargoNom = H000I10_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = H000I10_n1074Usuario_CargoNom[0];
            A58Usuario_PessoaNom = H000I10_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H000I10_n58Usuario_PessoaNom[0];
            A1Usuario_Codigo = H000I10_A1Usuario_Codigo[0];
            A325Usuario_PessoaDoc = H000I10_A325Usuario_PessoaDoc[0];
            n325Usuario_PessoaDoc = H000I10_n325Usuario_PessoaDoc[0];
            A58Usuario_PessoaNom = H000I10_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H000I10_n58Usuario_PessoaNom[0];
            A1074Usuario_CargoNom = H000I10_A1074Usuario_CargoNom[0];
            n1074Usuario_CargoNom = H000I10_n1074Usuario_CargoNom[0];
            GXt_char1 = A1083Usuario_Entidade;
            new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
            A1083Usuario_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1083Usuario_Entidade", A1083Usuario_Entidade);
            if ( ! ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218WWUsuarioDS_7_Usuario_entidade1)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV218WWUsuarioDS_7_Usuario_entidade1) , 255 , "%"),  ' ' ) ) )
            {
               if ( ! ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227WWUsuarioDS_16_Usuario_entidade2)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV227WWUsuarioDS_16_Usuario_entidade2) , 255 , "%"),  ' ' ) ) )
               {
                  if ( ! ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ENTIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236WWUsuarioDS_25_Usuario_entidade3)) ) ) || ( StringUtil.Like( StringUtil.Upper( A1083Usuario_Entidade) , StringUtil.PadR( "%" + StringUtil.Upper( AV236WWUsuarioDS_25_Usuario_entidade3) , 255 , "%"),  ' ' ) ) )
                  {
                     AV200Registros = (short)(AV200Registros+1);
                  }
               }
            }
            pr_default.readNext(8);
         }
         pr_default.close(8);
         GXt_int3 = AV190GridPageCount;
         new prc_pagecount(context ).execute(  AV200Registros, ref  AV192Preview, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV192Preview", StringUtil.LTrim( StringUtil.Str( (decimal)(AV192Preview), 4, 0)));
         AV190GridPageCount = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV190GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV190GridPageCount), 10, 0)));
      }

      protected void S212( )
      {
         /* 'ENTIDADE' Routine */
         AV202q = 0;
         AV198Entidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEntidade_Internalname, AV198Entidade);
         AV203ToolTips = "";
         pr_default.dynParam(9, new Object[]{ new Object[]{
                                              AV172AreaTrabalho_Codigo1 ,
                                              AV173AreaTrabalho_Codigo2 ,
                                              AV184AreaTrabalho_Codigo3 ,
                                              A5AreaTrabalho_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H000I11 */
         pr_default.execute(9, new Object[] {AV172AreaTrabalho_Codigo1, AV173AreaTrabalho_Codigo2, AV184AreaTrabalho_Codigo3});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A29Contratante_Codigo = H000I11_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H000I11_n29Contratante_Codigo[0];
            A72AreaTrabalho_Ativo = H000I11_A72AreaTrabalho_Ativo[0];
            A5AreaTrabalho_Codigo = H000I11_A5AreaTrabalho_Codigo[0];
            /* Using cursor H000I12 */
            pr_default.execute(10, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, AV83Usuario_Codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A340ContratanteUsuario_ContratantePesCod = H000I12_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H000I12_n340ContratanteUsuario_ContratantePesCod[0];
               A63ContratanteUsuario_ContratanteCod = H000I12_A63ContratanteUsuario_ContratanteCod[0];
               A60ContratanteUsuario_UsuarioCod = H000I12_A60ContratanteUsuario_UsuarioCod[0];
               A64ContratanteUsuario_ContratanteRaz = H000I12_A64ContratanteUsuario_ContratanteRaz[0];
               n64ContratanteUsuario_ContratanteRaz = H000I12_n64ContratanteUsuario_ContratanteRaz[0];
               A340ContratanteUsuario_ContratantePesCod = H000I12_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = H000I12_n340ContratanteUsuario_ContratantePesCod[0];
               A64ContratanteUsuario_ContratanteRaz = H000I12_A64ContratanteUsuario_ContratanteRaz[0];
               n64ContratanteUsuario_ContratanteRaz = H000I12_n64ContratanteUsuario_ContratanteRaz[0];
               AV202q = (short)(AV202q+1);
               if ( AV202q == 1 )
               {
                  AV198Entidade = StringUtil.Trim( A64ContratanteUsuario_ContratanteRaz);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEntidade_Internalname, AV198Entidade);
               }
               else
               {
                  AV203ToolTips = AV203ToolTips + ", " + StringUtil.Trim( A64ContratanteUsuario_ContratanteRaz);
               }
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(10);
            pr_default.readNext(9);
         }
         pr_default.close(9);
         pr_default.dynParam(11, new Object[]{ new Object[]{
                                              A67ContratadaUsuario_ContratadaPessoaCod ,
                                              AV204Pessoas ,
                                              AV172AreaTrabalho_Codigo1 ,
                                              AV173AreaTrabalho_Codigo2 ,
                                              AV184AreaTrabalho_Codigo3 ,
                                              AV83Usuario_Codigo ,
                                              A69ContratadaUsuario_UsuarioCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H000I13 */
         pr_default.execute(11, new Object[] {AV83Usuario_Codigo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = H000I13_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = H000I13_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = H000I13_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = H000I13_A69ContratadaUsuario_UsuarioCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = H000I13_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = H000I13_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A67ContratadaUsuario_ContratadaPessoaCod = H000I13_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = H000I13_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = H000I13_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = H000I13_n68ContratadaUsuario_ContratadaPessoaNom[0];
            AV202q = (short)(AV202q+1);
            if ( AV202q == 1 )
            {
               AV198Entidade = StringUtil.Trim( A68ContratadaUsuario_ContratadaPessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEntidade_Internalname, AV198Entidade);
            }
            else
            {
               AV203ToolTips = AV203ToolTips + ", " + StringUtil.Trim( A68ContratadaUsuario_ContratadaPessoaNom);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(11);
         }
         pr_default.close(11);
         if ( AV202q > 1 )
         {
            AV202q = (short)(AV202q-1);
            AV198Entidade = AV198Entidade + "... (+" + StringUtil.Trim( StringUtil.Str( (decimal)(AV202q), 4, 0)) + ")";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEntidade_Internalname, AV198Entidade);
            edtavEntidade_Tooltiptext = AV203ToolTips;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEntidade_Internalname, "Tooltiptext", edtavEntidade_Tooltiptext);
         }
      }

      protected void wb_table1_2_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableTitleCell'>") ;
            wb_table2_8_0I2( true) ;
         }
         else
         {
            wb_table2_8_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_20_0I2( true) ;
         }
         else
         {
            wb_table3_20_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_110_0I2( true) ;
         }
         else
         {
            wb_table4_110_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table4_110_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_133_0I2( true) ;
         }
         else
         {
            wb_table5_133_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table5_133_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0I2e( true) ;
         }
         else
         {
            wb_table1_2_0I2e( false) ;
         }
      }

      protected void wb_table5_133_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_133_0I2e( true) ;
         }
         else
         {
            wb_table5_133_0I2e( false) ;
         }
      }

      protected void wb_table4_110_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_113_0I2( true) ;
         }
         else
         {
            wb_table6_113_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table6_113_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_110_0I2e( true) ;
         }
         else
         {
            wb_table4_110_0I2e( false) ;
         }
      }

      protected void wb_table6_113_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"116\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_CargoNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_CargoNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_CargoNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Entidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarperfil_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarcontratadas_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV113Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A58Usuario_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUsuario_PessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1074Usuario_CargoNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_CargoNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_CargoNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_CargoNom_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUsuario_CargoNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV198Entidade));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEntidade_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEntidade_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavEntidade_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A54Usuario_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31AssociarPerfil));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarperfil_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarperfil_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV209AssociarContratadas));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarcontratadas_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarcontratadas_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 116 )
         {
            wbEnd = 0;
            nRC_GXsfl_116 = (short)(nGXsfl_116_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_113_0I2e( true) ;
         }
         else
         {
            wb_table6_113_0I2e( false) ;
         }
      }

      protected void wb_table3_20_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table7_23_0I2( true) ;
         }
         else
         {
            wb_table7_23_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table7_23_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_0I2e( true) ;
         }
         else
         {
            wb_table3_20_0I2e( false) ;
         }
      }

      protected void wb_table7_23_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblUsuariotitle_Internalname, "Usu�rio(s)", "", "", lblUsuariotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_WWUsuario.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_33_0I2( true) ;
         }
         else
         {
            wb_table8_33_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table8_33_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_23_0I2e( true) ;
         }
         else
         {
            wb_table7_23_0I2e( false) ;
         }
      }

      protected void wb_table8_33_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_38_0I2( true) ;
         }
         else
         {
            wb_table9_38_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table9_38_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_33_0I2e( true) ;
         }
         else
         {
            wb_table8_33_0I2e( false) ;
         }
      }

      protected void wb_table9_38_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_41_0I2( true) ;
         }
         else
         {
            wb_table10_41_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table10_41_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_38_0I2e( true) ;
         }
         else
         {
            wb_table9_38_0I2e( false) ;
         }
      }

      protected void wb_table10_41_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWUsuario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_50_0I2( true) ;
         }
         else
         {
            wb_table11_50_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table11_50_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_WWUsuario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_72_0I2( true) ;
         }
         else
         {
            wb_table12_72_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table12_72_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "", true, "HLP_WWUsuario.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_94_0I2( true) ;
         }
         else
         {
            wb_table13_94_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table13_94_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_41_0I2e( true) ;
         }
         else
         {
            wb_table10_41_0I2e( false) ;
         }
      }

      protected void wb_table13_94_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "", true, "HLP_WWUsuario.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAreatrabalho_codigo3, dynavAreatrabalho_codigo3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0)), 1, dynavAreatrabalho_codigo3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVAREATRABALHO_CODIGO3.CLICK."+"'", "int", "", dynavAreatrabalho_codigo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", "", true, "HLP_WWUsuario.htm");
            dynavAreatrabalho_codigo3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV184AreaTrabalho_Codigo3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo3_Internalname, "Values", (String)(dynavAreatrabalho_codigo3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom3_Internalname, StringUtil.RTrim( AV40Usuario_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV40Usuario_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargonom3_Internalname, AV185Usuario_CargoNom3, StringUtil.RTrim( context.localUtil.Format( AV185Usuario_CargoNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargonom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoadoc3_Internalname, AV78Usuario_PessoaDoc3, StringUtil.RTrim( context.localUtil.Format( AV78Usuario_PessoaDoc3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoadoc3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoadoc3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_entidade3_Internalname, StringUtil.RTrim( AV186Usuario_Entidade3), StringUtil.RTrim( context.localUtil.Format( AV186Usuario_Entidade3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_entidade3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_entidade3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUsuario_ativo3, cmbavUsuario_ativo3_Internalname, StringUtil.RTrim( AV187Usuario_Ativo3), 1, cmbavUsuario_ativo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavUsuario_ativo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "", true, "HLP_WWUsuario.htm");
            cmbavUsuario_ativo3.CurrentValue = StringUtil.RTrim( AV187Usuario_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo3_Internalname, "Values", (String)(cmbavUsuario_ativo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_94_0I2e( true) ;
         }
         else
         {
            wb_table13_94_0I2e( false) ;
         }
      }

      protected void wb_table12_72_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "", true, "HLP_WWUsuario.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAreatrabalho_codigo2, dynavAreatrabalho_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0)), 1, dynavAreatrabalho_codigo2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVAREATRABALHO_CODIGO2.CLICK."+"'", "int", "", dynavAreatrabalho_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "", true, "HLP_WWUsuario.htm");
            dynavAreatrabalho_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV173AreaTrabalho_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo2_Internalname, "Values", (String)(dynavAreatrabalho_codigo2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom2_Internalname, StringUtil.RTrim( AV39Usuario_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV39Usuario_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargonom2_Internalname, AV112Usuario_CargoNom2, StringUtil.RTrim( context.localUtil.Format( AV112Usuario_CargoNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargonom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoadoc2_Internalname, AV74Usuario_PessoaDoc2, StringUtil.RTrim( context.localUtil.Format( AV74Usuario_PessoaDoc2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoadoc2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoadoc2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_entidade2_Internalname, StringUtil.RTrim( AV82Usuario_Entidade2), StringUtil.RTrim( context.localUtil.Format( AV82Usuario_Entidade2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_entidade2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_entidade2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUsuario_ativo2, cmbavUsuario_ativo2_Internalname, StringUtil.RTrim( AV183Usuario_Ativo2), 1, cmbavUsuario_ativo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavUsuario_ativo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "", true, "HLP_WWUsuario.htm");
            cmbavUsuario_ativo2.CurrentValue = StringUtil.RTrim( AV183Usuario_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo2_Internalname, "Values", (String)(cmbavUsuario_ativo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_72_0I2e( true) ;
         }
         else
         {
            wb_table12_72_0I2e( false) ;
         }
      }

      protected void wb_table11_50_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWUsuario.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAreatrabalho_codigo1, dynavAreatrabalho_codigo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0)), 1, dynavAreatrabalho_codigo1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVAREATRABALHO_CODIGO1.CLICK."+"'", "int", "", dynavAreatrabalho_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWUsuario.htm");
            dynavAreatrabalho_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV172AreaTrabalho_Codigo1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo1_Internalname, "Values", (String)(dynavAreatrabalho_codigo1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom1_Internalname, StringUtil.RTrim( AV38Usuario_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV38Usuario_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargonom1_Internalname, AV110Usuario_CargoNom1, StringUtil.RTrim( context.localUtil.Format( AV110Usuario_CargoNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargonom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoadoc1_Internalname, AV73Usuario_PessoaDoc1, StringUtil.RTrim( context.localUtil.Format( AV73Usuario_PessoaDoc1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoadoc1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoadoc1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_entidade1_Internalname, StringUtil.RTrim( AV81Usuario_Entidade1), StringUtil.RTrim( context.localUtil.Format( AV81Usuario_Entidade1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_entidade1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_entidade1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuario.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUsuario_ativo1, cmbavUsuario_ativo1_Internalname, StringUtil.RTrim( AV182Usuario_Ativo1), 1, cmbavUsuario_ativo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavUsuario_ativo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_WWUsuario.htm");
            cmbavUsuario_ativo1.CurrentValue = StringUtil.RTrim( AV182Usuario_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_ativo1_Internalname, "Values", (String)(cmbavUsuario_ativo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_50_0I2e( true) ;
         }
         else
         {
            wb_table11_50_0I2e( false) ;
         }
      }

      protected void wb_table2_8_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtabletitle_Internalname, tblTablemergedtabletitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table14_11_0I2( true) ;
         }
         else
         {
            wb_table14_11_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table14_11_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table15_15_0I2( true) ;
         }
         else
         {
            wb_table15_15_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table15_15_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0I2e( true) ;
         }
         else
         {
            wb_table2_8_0I2e( false) ;
         }
      }

      protected void wb_table15_15_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_15_0I2e( true) ;
         }
         else
         {
            wb_table15_15_0I2e( false) ;
         }
      }

      protected void wb_table14_11_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletitle_Internalname, tblTabletitle_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_11_0I2e( true) ;
         }
         else
         {
            wb_table14_11_0I2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0I2( ) ;
         WS0I2( ) ;
         WE0I2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117305446");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwusuario.js", "?20203117305447");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1162( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_116_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_116_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_116_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_116_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_116_idx;
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM_"+sGXsfl_116_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_116_idx;
         edtavEntidade_Internalname = "vENTIDADE_"+sGXsfl_116_idx;
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO_"+sGXsfl_116_idx;
         edtavAssociarperfil_Internalname = "vASSOCIARPERFIL_"+sGXsfl_116_idx;
         edtavAssociarcontratadas_Internalname = "vASSOCIARCONTRATADAS_"+sGXsfl_116_idx;
      }

      protected void SubsflControlProps_fel_1162( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_116_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_116_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_116_fel_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_116_fel_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_116_fel_idx;
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM_"+sGXsfl_116_fel_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_116_fel_idx;
         edtavEntidade_Internalname = "vENTIDADE_"+sGXsfl_116_fel_idx;
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO_"+sGXsfl_116_fel_idx;
         edtavAssociarperfil_Internalname = "vASSOCIARPERFIL_"+sGXsfl_116_fel_idx;
         edtavAssociarcontratadas_Internalname = "vASSOCIARCONTRATADAS_"+sGXsfl_116_fel_idx;
      }

      protected void sendrow_1162( )
      {
         SubsflControlProps_1162( ) ;
         WB0I0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_116_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_116_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_116_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV245Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV245Update_GXI : context.PathToRelativeUrl( AV33Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV246Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV246Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV113Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV113Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV247Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV113Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV113Display)) ? AV247Display_GXI : context.PathToRelativeUrl( AV113Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV113Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaNom_Internalname,StringUtil.RTrim( A58Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtUsuario_PessoaNom_Link,(String)"",(String)"",(String)"",(String)edtUsuario_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtUsuario_PessoaNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_CargoNom_Internalname,(String)A1074Usuario_CargoNom,StringUtil.RTrim( context.localUtil.Format( A1074Usuario_CargoNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtUsuario_CargoNom_Link,(String)"",(String)"",(String)"",(String)edtUsuario_CargoNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtUsuario_CargoNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtUsuario_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavEntidade_Enabled!=0)&&(edtavEntidade_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 124,'',false,'"+sGXsfl_116_idx+"',116)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEntidade_Internalname,StringUtil.RTrim( AV198Entidade),StringUtil.RTrim( context.localUtil.Format( AV198Entidade, "@!")),TempTags+((edtavEntidade_Enabled!=0)&&(edtavEntidade_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavEntidade_Enabled!=0)&&(edtavEntidade_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,124);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)edtavEntidade_Tooltiptext,(String)"",(String)edtavEntidade_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavEntidade_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavEntidade_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_Ativo_Internalname,StringUtil.BoolToStr( A54Usuario_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarperfil_Enabled!=0)&&(edtavAssociarperfil_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 126,'',false,'',116)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31AssociarPerfil_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarPerfil))&&String.IsNullOrEmpty(StringUtil.RTrim( AV248Associarperfil_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarPerfil)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarperfil_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarPerfil)) ? AV248Associarperfil_GXI : context.PathToRelativeUrl( AV31AssociarPerfil)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarperfil_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarperfil_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e320i2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31AssociarPerfil_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarcontratadas_Enabled!=0)&&(edtavAssociarcontratadas_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 127,'',false,'',116)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV209AssociarContratadas_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV209AssociarContratadas))&&String.IsNullOrEmpty(StringUtil.RTrim( AV249Associarcontratadas_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV209AssociarContratadas)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarcontratadas_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV209AssociarContratadas)) ? AV249Associarcontratadas_GXI : context.PathToRelativeUrl( AV209AssociarContratadas)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarcontratadas_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarcontratadas_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e330i2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV209AssociarContratadas_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_NOME"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_ATIVO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, A54Usuario_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_116_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_116_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_116_idx+1));
            sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
            SubsflControlProps_1162( ) ;
         }
         /* End function sendrow_1162 */
      }

      protected void init_default_properties( )
      {
         tblTabletitle_Internalname = "TABLETITLE";
         tblTablesearch_Internalname = "TABLESEARCH";
         tblTablemergedtabletitle_Internalname = "TABLEMERGEDTABLETITLE";
         lblUsuariotitle_Internalname = "USUARIOTITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         dynavAreatrabalho_codigo1_Internalname = "vAREATRABALHO_CODIGO1";
         edtavUsuario_pessoanom1_Internalname = "vUSUARIO_PESSOANOM1";
         edtavUsuario_cargonom1_Internalname = "vUSUARIO_CARGONOM1";
         edtavUsuario_pessoadoc1_Internalname = "vUSUARIO_PESSOADOC1";
         edtavUsuario_entidade1_Internalname = "vUSUARIO_ENTIDADE1";
         cmbavUsuario_ativo1_Internalname = "vUSUARIO_ATIVO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         dynavAreatrabalho_codigo2_Internalname = "vAREATRABALHO_CODIGO2";
         edtavUsuario_pessoanom2_Internalname = "vUSUARIO_PESSOANOM2";
         edtavUsuario_cargonom2_Internalname = "vUSUARIO_CARGONOM2";
         edtavUsuario_pessoadoc2_Internalname = "vUSUARIO_PESSOADOC2";
         edtavUsuario_entidade2_Internalname = "vUSUARIO_ENTIDADE2";
         cmbavUsuario_ativo2_Internalname = "vUSUARIO_ATIVO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         dynavAreatrabalho_codigo3_Internalname = "vAREATRABALHO_CODIGO3";
         edtavUsuario_pessoanom3_Internalname = "vUSUARIO_PESSOANOM3";
         edtavUsuario_cargonom3_Internalname = "vUSUARIO_CARGONOM3";
         edtavUsuario_pessoadoc3_Internalname = "vUSUARIO_PESSOADOC3";
         edtavUsuario_entidade3_Internalname = "vUSUARIO_ENTIDADE3";
         cmbavUsuario_ativo3_Internalname = "vUSUARIO_ATIVO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         edtavEntidade_Internalname = "vENTIDADE";
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO";
         edtavAssociarperfil_Internalname = "vASSOCIARPERFIL";
         edtavAssociarcontratadas_Internalname = "vASSOCIARCONTRATADAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblUsertable_Internalname = "USERTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfusuario_pessoanom_Internalname = "vTFUSUARIO_PESSOANOM";
         edtavTfusuario_pessoanom_sel_Internalname = "vTFUSUARIO_PESSOANOM_SEL";
         edtavTfusuario_cargonom_Internalname = "vTFUSUARIO_CARGONOM";
         edtavTfusuario_cargonom_sel_Internalname = "vTFUSUARIO_CARGONOM_SEL";
         edtavTfusuario_nome_Internalname = "vTFUSUARIO_NOME";
         edtavTfusuario_nome_sel_Internalname = "vTFUSUARIO_NOME_SEL";
         edtavTfusuario_ativo_sel_Internalname = "vTFUSUARIO_ATIVO_SEL";
         Ddo_usuario_pessoanom_Internalname = "DDO_USUARIO_PESSOANOM";
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_usuario_cargonom_Internalname = "DDO_USUARIO_CARGONOM";
         edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE";
         Ddo_usuario_nome_Internalname = "DDO_USUARIO_NOME";
         edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_usuario_ativo_Internalname = "DDO_USUARIO_ATIVO";
         edtavDdo_usuario_ativotitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavAssociarcontratadas_Jsonclick = "";
         edtavAssociarcontratadas_Visible = -1;
         edtavAssociarcontratadas_Enabled = 1;
         edtavAssociarperfil_Jsonclick = "";
         edtavAssociarperfil_Visible = -1;
         edtavAssociarperfil_Enabled = 1;
         edtavEntidade_Jsonclick = "";
         edtavEntidade_Visible = -1;
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_CargoNom_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_Codigo_Jsonclick = "";
         cmbavUsuario_ativo1_Jsonclick = "";
         edtavUsuario_entidade1_Jsonclick = "";
         edtavUsuario_pessoadoc1_Jsonclick = "";
         edtavUsuario_cargonom1_Jsonclick = "";
         edtavUsuario_pessoanom1_Jsonclick = "";
         dynavAreatrabalho_codigo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavUsuario_ativo2_Jsonclick = "";
         edtavUsuario_entidade2_Jsonclick = "";
         edtavUsuario_pessoadoc2_Jsonclick = "";
         edtavUsuario_cargonom2_Jsonclick = "";
         edtavUsuario_pessoanom2_Jsonclick = "";
         dynavAreatrabalho_codigo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavUsuario_ativo3_Jsonclick = "";
         edtavUsuario_entidade3_Jsonclick = "";
         edtavUsuario_pessoadoc3_Jsonclick = "";
         edtavUsuario_cargonom3_Jsonclick = "";
         edtavUsuario_pessoanom3_Jsonclick = "";
         dynavAreatrabalho_codigo3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAssociarcontratadas_Tooltiptext = "Associar a Contratadas";
         edtavAssociarperfil_Tooltiptext = "Clique aqui p/ associar os perfis deste usu�rio!";
         edtavEntidade_Enabled = 1;
         edtavEntidade_Forecolor = (int)(0xFFFFFF);
         edtUsuario_Nome_Forecolor = (int)(0xFFFFFF);
         edtUsuario_CargoNom_Link = "";
         edtUsuario_CargoNom_Forecolor = (int)(0xFFFFFF);
         edtUsuario_PessoaNom_Link = "";
         edtUsuario_PessoaNom_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkUsuario_Ativo_Titleformat = 0;
         edtUsuario_Nome_Titleformat = 0;
         edtUsuario_CargoNom_Titleformat = 0;
         edtUsuario_PessoaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavEntidade_Tooltiptext = "";
         cmbavDynamicfiltersoperator3.Visible = 1;
         cmbavUsuario_ativo3.Visible = 1;
         edtavUsuario_entidade3_Visible = 1;
         edtavUsuario_pessoadoc3_Visible = 1;
         edtavUsuario_cargonom3_Visible = 1;
         edtavUsuario_pessoanom3_Visible = 1;
         dynavAreatrabalho_codigo3.Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         cmbavUsuario_ativo2.Visible = 1;
         edtavUsuario_entidade2_Visible = 1;
         edtavUsuario_pessoadoc2_Visible = 1;
         edtavUsuario_cargonom2_Visible = 1;
         edtavUsuario_pessoanom2_Visible = 1;
         dynavAreatrabalho_codigo2.Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         cmbavUsuario_ativo1.Visible = 1;
         edtavUsuario_entidade1_Visible = 1;
         edtavUsuario_pessoadoc1_Visible = 1;
         edtavUsuario_cargonom1_Visible = 1;
         edtavUsuario_pessoanom1_Visible = 1;
         dynavAreatrabalho_codigo1.Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtavDelete_Enabled = 1;
         edtavAssociarcontratadas_Title = "";
         edtavAssociarperfil_Title = "";
         chkUsuario_Ativo.Title.Text = "Ativo?";
         edtUsuario_Nome_Title = "Usu�rio";
         edtUsuario_CargoNom_Title = "Cargo";
         edtUsuario_PessoaNom_Title = "Pessoa";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkUsuario_Ativo.Caption = "";
         edtavDdo_usuario_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavTfusuario_ativo_sel_Jsonclick = "";
         edtavTfusuario_ativo_sel_Visible = 1;
         edtavTfusuario_nome_sel_Jsonclick = "";
         edtavTfusuario_nome_sel_Visible = 1;
         edtavTfusuario_nome_Jsonclick = "";
         edtavTfusuario_nome_Visible = 1;
         edtavTfusuario_cargonom_sel_Jsonclick = "";
         edtavTfusuario_cargonom_sel_Visible = 1;
         edtavTfusuario_cargonom_Jsonclick = "";
         edtavTfusuario_cargonom_Visible = 1;
         edtavTfusuario_pessoanom_sel_Jsonclick = "";
         edtavTfusuario_pessoanom_sel_Visible = 1;
         edtavTfusuario_pessoanom_Jsonclick = "";
         edtavTfusuario_pessoanom_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_usuario_ativo_Searchbuttontext = "Pesquisar";
         Ddo_usuario_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_usuario_ativo_Datalisttype = "FixedValues";
         Ddo_usuario_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_usuario_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_ativo_Titlecontrolidtoreplace = "";
         Ddo_usuario_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_ativo_Cls = "ColumnSettings";
         Ddo_usuario_ativo_Tooltip = "Op��es";
         Ddo_usuario_ativo_Caption = "";
         Ddo_usuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_usuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_nome_Loadingdata = "Carregando dados...";
         Ddo_usuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_nome_Datalistproc = "GetWWUsuarioFilterData";
         Ddo_usuario_nome_Datalisttype = "Dynamic";
         Ddo_usuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_nome_Filtertype = "Character";
         Ddo_usuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Titlecontrolidtoreplace = "";
         Ddo_usuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_nome_Cls = "ColumnSettings";
         Ddo_usuario_nome_Tooltip = "Op��es";
         Ddo_usuario_nome_Caption = "";
         Ddo_usuario_cargonom_Searchbuttontext = "Pesquisar";
         Ddo_usuario_cargonom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_cargonom_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_cargonom_Loadingdata = "Carregando dados...";
         Ddo_usuario_cargonom_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_cargonom_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_cargonom_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_cargonom_Datalistproc = "GetWWUsuarioFilterData";
         Ddo_usuario_cargonom_Datalisttype = "Dynamic";
         Ddo_usuario_cargonom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_cargonom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_cargonom_Filtertype = "Character";
         Ddo_usuario_cargonom_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_cargonom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_cargonom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_cargonom_Titlecontrolidtoreplace = "";
         Ddo_usuario_cargonom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_cargonom_Cls = "ColumnSettings";
         Ddo_usuario_cargonom_Tooltip = "Op��es";
         Ddo_usuario_cargonom_Caption = "";
         Ddo_usuario_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_usuario_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_usuario_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_pessoanom_Datalistproc = "GetWWUsuarioFilterData";
         Ddo_usuario_pessoanom_Datalisttype = "Dynamic";
         Ddo_usuario_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_pessoanom_Filtertype = "Character";
         Ddo_usuario_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_usuario_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_pessoanom_Cls = "ColumnSettings";
         Ddo_usuario_pessoanom_Tooltip = "Op��es";
         Ddo_usuario_pessoanom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Work With Usuarios";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV89Usuario_PessoaNomTitleFilterData',fld:'vUSUARIO_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV122Usuario_CargoNomTitleFilterData',fld:'vUSUARIO_CARGONOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV85Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV105Usuario_AtivoTitleFilterData',fld:'vUSUARIO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUsuario_PessoaNom_Titleformat',ctrl:'USUARIO_PESSOANOM',prop:'Titleformat'},{av:'edtUsuario_PessoaNom_Title',ctrl:'USUARIO_PESSOANOM',prop:'Title'},{av:'edtUsuario_CargoNom_Titleformat',ctrl:'USUARIO_CARGONOM',prop:'Titleformat'},{av:'edtUsuario_CargoNom_Title',ctrl:'USUARIO_CARGONOM',prop:'Title'},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'chkUsuario_Ativo_Titleformat',ctrl:'USUARIO_ATIVO',prop:'Titleformat'},{av:'chkUsuario_Ativo.Title.Text',ctrl:'USUARIO_ATIVO',prop:'Title'},{av:'AV189GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV190GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavAssociarperfil_Title',ctrl:'vASSOCIARPERFIL',prop:'Title'},{av:'edtavAssociarcontratadas_Title',ctrl:'vASSOCIARCONTRATADAS',prop:'Title'},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED","{handler:'E120I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Activeeventkey',ctrl:'DDO_USUARIO_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_usuario_pessoanom_Filteredtext_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_usuario_pessoanom_Selectedvalue_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_cargonom_Sortedstatus',ctrl:'DDO_USUARIO_CARGONOM',prop:'SortedStatus'},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_ativo_Sortedstatus',ctrl:'DDO_USUARIO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIO_CARGONOM.ONOPTIONCLICKED","{handler:'E130I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_cargonom_Activeeventkey',ctrl:'DDO_USUARIO_CARGONOM',prop:'ActiveEventKey'},{av:'Ddo_usuario_cargonom_Filteredtext_get',ctrl:'DDO_USUARIO_CARGONOM',prop:'FilteredText_get'},{av:'Ddo_usuario_cargonom_Selectedvalue_get',ctrl:'DDO_USUARIO_CARGONOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_cargonom_Sortedstatus',ctrl:'DDO_USUARIO_CARGONOM',prop:'SortedStatus'},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_ativo_Sortedstatus',ctrl:'DDO_USUARIO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIO_NOME.ONOPTIONCLICKED","{handler:'E140I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Activeeventkey',ctrl:'DDO_USUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_usuario_nome_Filteredtext_get',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_usuario_nome_Selectedvalue_get',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_usuario_cargonom_Sortedstatus',ctrl:'DDO_USUARIO_CARGONOM',prop:'SortedStatus'},{av:'Ddo_usuario_ativo_Sortedstatus',ctrl:'DDO_USUARIO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIO_ATIVO.ONOPTIONCLICKED","{handler:'E150I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_ativo_Activeeventkey',ctrl:'DDO_USUARIO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_usuario_ativo_Selectedvalue_get',ctrl:'DDO_USUARIO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_ativo_Sortedstatus',ctrl:'DDO_USUARIO_ATIVO',prop:'SortedStatus'},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_usuario_cargonom_Sortedstatus',ctrl:'DDO_USUARIO_CARGONOM',prop:'SortedStatus'},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E310I2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV113Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'AV31AssociarPerfil',fld:'vASSOCIARPERFIL',pic:'',nv:''},{av:'edtavAssociarperfil_Tooltiptext',ctrl:'vASSOCIARPERFIL',prop:'Tooltiptext'},{av:'AV209AssociarContratadas',fld:'vASSOCIARCONTRATADAS',pic:'',nv:''},{av:'edtavAssociarcontratadas_Tooltiptext',ctrl:'vASSOCIARCONTRATADAS',prop:'Tooltiptext'},{av:'edtUsuario_PessoaNom_Link',ctrl:'USUARIO_PESSOANOM',prop:'Link'},{av:'edtUsuario_CargoNom_Link',ctrl:'USUARIO_CARGONOM',prop:'Link'},{av:'edtUsuario_PessoaNom_Forecolor',ctrl:'USUARIO_PESSOANOM',prop:'Forecolor'},{av:'edtUsuario_CargoNom_Forecolor',ctrl:'USUARIO_CARGONOM',prop:'Forecolor'},{av:'edtUsuario_Nome_Forecolor',ctrl:'USUARIO_NOME',prop:'Forecolor'},{av:'edtavEntidade_Forecolor',ctrl:'vENTIDADE',prop:'Forecolor'},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV198Entidade',fld:'vENTIDADE',pic:'@!',nv:''},{av:'edtavEntidade_Tooltiptext',ctrl:'vENTIDADE',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E160I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E240I2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E170I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'dynavAreatrabalho_codigo2'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_entidade2_Visible',ctrl:'vUSUARIO_ENTIDADE2',prop:'Visible'},{av:'cmbavUsuario_ativo2'},{av:'dynavAreatrabalho_codigo3'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_entidade3_Visible',ctrl:'vUSUARIO_ENTIDADE3',prop:'Visible'},{av:'cmbavUsuario_ativo3'},{av:'dynavAreatrabalho_codigo1'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'edtavUsuario_entidade1_Visible',ctrl:'vUSUARIO_ENTIDADE1',prop:'Visible'},{av:'cmbavUsuario_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E250I2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'dynavAreatrabalho_codigo1'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'edtavUsuario_entidade1_Visible',ctrl:'vUSUARIO_ENTIDADE1',prop:'Visible'},{av:'cmbavUsuario_ativo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E260I2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E180I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'dynavAreatrabalho_codigo2'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_entidade2_Visible',ctrl:'vUSUARIO_ENTIDADE2',prop:'Visible'},{av:'cmbavUsuario_ativo2'},{av:'dynavAreatrabalho_codigo3'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_entidade3_Visible',ctrl:'vUSUARIO_ENTIDADE3',prop:'Visible'},{av:'cmbavUsuario_ativo3'},{av:'dynavAreatrabalho_codigo1'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'edtavUsuario_entidade1_Visible',ctrl:'vUSUARIO_ENTIDADE1',prop:'Visible'},{av:'cmbavUsuario_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E270I2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'dynavAreatrabalho_codigo2'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_entidade2_Visible',ctrl:'vUSUARIO_ENTIDADE2',prop:'Visible'},{av:'cmbavUsuario_ativo2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E190I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'dynavAreatrabalho_codigo2'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_entidade2_Visible',ctrl:'vUSUARIO_ENTIDADE2',prop:'Visible'},{av:'cmbavUsuario_ativo2'},{av:'dynavAreatrabalho_codigo3'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_entidade3_Visible',ctrl:'vUSUARIO_ENTIDADE3',prop:'Visible'},{av:'cmbavUsuario_ativo3'},{av:'dynavAreatrabalho_codigo1'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'edtavUsuario_entidade1_Visible',ctrl:'vUSUARIO_ENTIDADE1',prop:'Visible'},{av:'cmbavUsuario_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E280I2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'dynavAreatrabalho_codigo3'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_entidade3_Visible',ctrl:'vUSUARIO_ENTIDADE3',prop:'Visible'},{av:'cmbavUsuario_ativo3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E200I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Filteredtext_set',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_set'},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Selectedvalue_set',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_set'},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'Ddo_usuario_cargonom_Filteredtext_set',ctrl:'DDO_USUARIO_CARGONOM',prop:'FilteredText_set'},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_cargonom_Selectedvalue_set',ctrl:'DDO_USUARIO_CARGONOM',prop:'SelectedValue_set'},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Filteredtext_set',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_set'},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Selectedvalue_set',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_set'},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_usuario_ativo_Selectedvalue_set',ctrl:'DDO_USUARIO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'dynavAreatrabalho_codigo1'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'edtavUsuario_entidade1_Visible',ctrl:'vUSUARIO_ENTIDADE1',prop:'Visible'},{av:'cmbavUsuario_ativo1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'dynavAreatrabalho_codigo2'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_entidade2_Visible',ctrl:'vUSUARIO_ENTIDADE2',prop:'Visible'},{av:'cmbavUsuario_ativo2'},{av:'dynavAreatrabalho_codigo3'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_entidade3_Visible',ctrl:'vUSUARIO_ENTIDADE3',prop:'Visible'},{av:'cmbavUsuario_ativo3'}]}");
         setEventMetadata("'DOASSOCIARPERFIL'","{handler:'E320I2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARCONTRATADAS'","{handler:'E330I2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VAREATRABALHO_CODIGO1.CLICK","{handler:'E210I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("VAREATRABALHO_CODIGO2.CLICK","{handler:'E220I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("VAREATRABALHO_CODIGO3.CLICK","{handler:'E230I2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV172AreaTrabalho_Codigo1',fld:'vAREATRABALHO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV38Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV110Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV73Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV182Usuario_Ativo1',fld:'vUSUARIO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV173AreaTrabalho_Codigo2',fld:'vAREATRABALHO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV39Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV112Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV74Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV183Usuario_Ativo2',fld:'vUSUARIO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV184AreaTrabalho_Codigo3',fld:'vAREATRABALHO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV40Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV185Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV78Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV187Usuario_Ativo3',fld:'vUSUARIO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV90TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV91TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV123TFUsuario_CargoNom',fld:'vTFUSUARIO_CARGONOM',pic:'@!',nv:''},{av:'AV124TFUsuario_CargoNom_Sel',fld:'vTFUSUARIO_CARGONOM_SEL',pic:'@!',nv:''},{av:'AV86TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV87TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV106TFUsuario_Ativo_Sel',fld:'vTFUSUARIO_ATIVO_SEL',pic:'9',nv:0},{av:'AV92ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Usuario_CargoNomTitleControlIdToReplace',fld:'vDDO_USUARIO_CARGONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107ddo_Usuario_AtivoTitleControlIdToReplace',fld:'vDDO_USUARIO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV199PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZ9',nv:0},{av:'AV81Usuario_Entidade1',fld:'vUSUARIO_ENTIDADE1',pic:'@!',nv:''},{av:'AV82Usuario_Entidade2',fld:'vUSUARIO_ENTIDADE2',pic:'@!',nv:''},{av:'AV186Usuario_Entidade3',fld:'vUSUARIO_ENTIDADE3',pic:'@!',nv:''},{av:'AV250Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A40Contratada_PessoaCod',fld:'CONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV193Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV192Preview',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1073Usuario_CargoCod',fld:'USUARIO_CARGOCOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',hsh:true,nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'AV204Pessoas',fld:'vPESSOAS',pic:'',nv:null},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_usuario_pessoanom_Activeeventkey = "";
         Ddo_usuario_pessoanom_Filteredtext_get = "";
         Ddo_usuario_pessoanom_Selectedvalue_get = "";
         Ddo_usuario_cargonom_Activeeventkey = "";
         Ddo_usuario_cargonom_Filteredtext_get = "";
         Ddo_usuario_cargonom_Selectedvalue_get = "";
         Ddo_usuario_nome_Activeeventkey = "";
         Ddo_usuario_nome_Filteredtext_get = "";
         Ddo_usuario_nome_Selectedvalue_get = "";
         Ddo_usuario_ativo_Activeeventkey = "";
         Ddo_usuario_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV38Usuario_PessoaNom1 = "";
         AV110Usuario_CargoNom1 = "";
         AV73Usuario_PessoaDoc1 = "";
         AV182Usuario_Ativo1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV39Usuario_PessoaNom2 = "";
         AV112Usuario_CargoNom2 = "";
         AV74Usuario_PessoaDoc2 = "";
         AV183Usuario_Ativo2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV40Usuario_PessoaNom3 = "";
         AV185Usuario_CargoNom3 = "";
         AV78Usuario_PessoaDoc3 = "";
         AV187Usuario_Ativo3 = "";
         AV90TFUsuario_PessoaNom = "";
         AV91TFUsuario_PessoaNom_Sel = "";
         AV123TFUsuario_CargoNom = "";
         AV124TFUsuario_CargoNom_Sel = "";
         AV86TFUsuario_Nome = "";
         AV87TFUsuario_Nome_Sel = "";
         AV92ddo_Usuario_PessoaNomTitleControlIdToReplace = "";
         AV125ddo_Usuario_CargoNomTitleControlIdToReplace = "";
         AV88ddo_Usuario_NomeTitleControlIdToReplace = "";
         AV107ddo_Usuario_AtivoTitleControlIdToReplace = "";
         AV81Usuario_Entidade1 = "";
         AV82Usuario_Entidade2 = "";
         AV186Usuario_Entidade3 = "";
         AV250Pgmname = "";
         AV193Codigos = new GxSimpleCollection();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A64ContratanteUsuario_ContratanteRaz = "";
         AV204Pessoas = new GxSimpleCollection();
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV108DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV89Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV122Usuario_CargoNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV105Usuario_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         A1083Usuario_Entidade = "";
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         Ddo_usuario_pessoanom_Sortedstatus = "";
         Ddo_usuario_cargonom_Filteredtext_set = "";
         Ddo_usuario_cargonom_Selectedvalue_set = "";
         Ddo_usuario_cargonom_Sortedstatus = "";
         Ddo_usuario_nome_Filteredtext_set = "";
         Ddo_usuario_nome_Selectedvalue_set = "";
         Ddo_usuario_nome_Sortedstatus = "";
         Ddo_usuario_ativo_Selectedvalue_set = "";
         Ddo_usuario_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33Update = "";
         AV245Update_GXI = "";
         AV32Delete = "";
         AV246Delete_GXI = "";
         AV113Display = "";
         AV247Display_GXI = "";
         A58Usuario_PessoaNom = "";
         A1074Usuario_CargoNom = "";
         A2Usuario_Nome = "";
         AV198Entidade = "";
         AV31AssociarPerfil = "";
         AV248Associarperfil_GXI = "";
         AV209AssociarContratadas = "";
         AV249Associarcontratadas_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H000I2_A5AreaTrabalho_Codigo = new int[1] ;
         H000I2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H000I2_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H000I3_A5AreaTrabalho_Codigo = new int[1] ;
         H000I3_A6AreaTrabalho_Descricao = new String[] {""} ;
         H000I3_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H000I4_A5AreaTrabalho_Codigo = new int[1] ;
         H000I4_A6AreaTrabalho_Descricao = new String[] {""} ;
         H000I4_A72AreaTrabalho_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV215WWUsuarioDS_4_Usuario_pessoanom1 = "";
         lV216WWUsuarioDS_5_Usuario_cargonom1 = "";
         lV217WWUsuarioDS_6_Usuario_pessoadoc1 = "";
         lV224WWUsuarioDS_13_Usuario_pessoanom2 = "";
         lV225WWUsuarioDS_14_Usuario_cargonom2 = "";
         lV226WWUsuarioDS_15_Usuario_pessoadoc2 = "";
         lV233WWUsuarioDS_22_Usuario_pessoanom3 = "";
         lV234WWUsuarioDS_23_Usuario_cargonom3 = "";
         lV235WWUsuarioDS_24_Usuario_pessoadoc3 = "";
         lV238WWUsuarioDS_27_Tfusuario_pessoanom = "";
         lV240WWUsuarioDS_29_Tfusuario_cargonom = "";
         lV242WWUsuarioDS_31_Tfusuario_nome = "";
         AV212WWUsuarioDS_1_Dynamicfiltersselector1 = "";
         AV215WWUsuarioDS_4_Usuario_pessoanom1 = "";
         AV216WWUsuarioDS_5_Usuario_cargonom1 = "";
         AV217WWUsuarioDS_6_Usuario_pessoadoc1 = "";
         AV219WWUsuarioDS_8_Usuario_ativo1 = "";
         AV221WWUsuarioDS_10_Dynamicfiltersselector2 = "";
         AV224WWUsuarioDS_13_Usuario_pessoanom2 = "";
         AV225WWUsuarioDS_14_Usuario_cargonom2 = "";
         AV226WWUsuarioDS_15_Usuario_pessoadoc2 = "";
         AV228WWUsuarioDS_17_Usuario_ativo2 = "";
         AV230WWUsuarioDS_19_Dynamicfiltersselector3 = "";
         AV233WWUsuarioDS_22_Usuario_pessoanom3 = "";
         AV234WWUsuarioDS_23_Usuario_cargonom3 = "";
         AV235WWUsuarioDS_24_Usuario_pessoadoc3 = "";
         AV237WWUsuarioDS_26_Usuario_ativo3 = "";
         AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel = "";
         AV238WWUsuarioDS_27_Tfusuario_pessoanom = "";
         AV241WWUsuarioDS_30_Tfusuario_cargonom_sel = "";
         AV240WWUsuarioDS_29_Tfusuario_cargonom = "";
         AV243WWUsuarioDS_32_Tfusuario_nome_sel = "";
         AV242WWUsuarioDS_31_Tfusuario_nome = "";
         A325Usuario_PessoaDoc = "";
         AV218WWUsuarioDS_7_Usuario_entidade1 = "";
         AV227WWUsuarioDS_16_Usuario_entidade2 = "";
         AV236WWUsuarioDS_25_Usuario_entidade3 = "";
         H000I5_A57Usuario_PessoaCod = new int[1] ;
         H000I5_A325Usuario_PessoaDoc = new String[] {""} ;
         H000I5_n325Usuario_PessoaDoc = new bool[] {false} ;
         H000I5_A1073Usuario_CargoCod = new int[1] ;
         H000I5_n1073Usuario_CargoCod = new bool[] {false} ;
         H000I5_A54Usuario_Ativo = new bool[] {false} ;
         H000I5_A2Usuario_Nome = new String[] {""} ;
         H000I5_n2Usuario_Nome = new bool[] {false} ;
         H000I5_A1074Usuario_CargoNom = new String[] {""} ;
         H000I5_n1074Usuario_CargoNom = new bool[] {false} ;
         H000I5_A58Usuario_PessoaNom = new String[] {""} ;
         H000I5_n58Usuario_PessoaNom = new bool[] {false} ;
         H000I5_A1Usuario_Codigo = new int[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         H000I6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H000I6_A40Contratada_PessoaCod = new int[1] ;
         H000I7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H000I7_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H000I7_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H000I7_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H000I7_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H000I7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H000I8_A29Contratante_Codigo = new int[1] ;
         H000I8_n29Contratante_Codigo = new bool[] {false} ;
         H000I8_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H000I8_A5AreaTrabalho_Codigo = new int[1] ;
         H000I9_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H000I9_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H000I10_A57Usuario_PessoaCod = new int[1] ;
         H000I10_A1073Usuario_CargoCod = new int[1] ;
         H000I10_n1073Usuario_CargoCod = new bool[] {false} ;
         H000I10_A2Usuario_Nome = new String[] {""} ;
         H000I10_n2Usuario_Nome = new bool[] {false} ;
         H000I10_A54Usuario_Ativo = new bool[] {false} ;
         H000I10_A325Usuario_PessoaDoc = new String[] {""} ;
         H000I10_n325Usuario_PessoaDoc = new bool[] {false} ;
         H000I10_A1074Usuario_CargoNom = new String[] {""} ;
         H000I10_n1074Usuario_CargoNom = new bool[] {false} ;
         H000I10_A58Usuario_PessoaNom = new String[] {""} ;
         H000I10_n58Usuario_PessoaNom = new bool[] {false} ;
         H000I10_A1Usuario_Codigo = new int[1] ;
         GXt_char1 = "";
         AV203ToolTips = "";
         H000I11_A29Contratante_Codigo = new int[1] ;
         H000I11_n29Contratante_Codigo = new bool[] {false} ;
         H000I11_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H000I11_A5AreaTrabalho_Codigo = new int[1] ;
         H000I12_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H000I12_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H000I12_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H000I12_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H000I12_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H000I12_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H000I13_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H000I13_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H000I13_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H000I13_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H000I13_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H000I13_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblUsuariotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwusuario__default(),
            new Object[][] {
                new Object[] {
               H000I2_A5AreaTrabalho_Codigo, H000I2_A6AreaTrabalho_Descricao, H000I2_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H000I3_A5AreaTrabalho_Codigo, H000I3_A6AreaTrabalho_Descricao, H000I3_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H000I4_A5AreaTrabalho_Codigo, H000I4_A6AreaTrabalho_Descricao, H000I4_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H000I5_A57Usuario_PessoaCod, H000I5_A325Usuario_PessoaDoc, H000I5_n325Usuario_PessoaDoc, H000I5_A1073Usuario_CargoCod, H000I5_n1073Usuario_CargoCod, H000I5_A54Usuario_Ativo, H000I5_A2Usuario_Nome, H000I5_n2Usuario_Nome, H000I5_A1074Usuario_CargoNom, H000I5_n1074Usuario_CargoNom,
               H000I5_A58Usuario_PessoaNom, H000I5_n58Usuario_PessoaNom, H000I5_A1Usuario_Codigo
               }
               , new Object[] {
               H000I6_A52Contratada_AreaTrabalhoCod, H000I6_A40Contratada_PessoaCod
               }
               , new Object[] {
               H000I7_A66ContratadaUsuario_ContratadaCod, H000I7_A67ContratadaUsuario_ContratadaPessoaCod, H000I7_n67ContratadaUsuario_ContratadaPessoaCod, H000I7_A1228ContratadaUsuario_AreaTrabalhoCod, H000I7_n1228ContratadaUsuario_AreaTrabalhoCod, H000I7_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               H000I8_A29Contratante_Codigo, H000I8_n29Contratante_Codigo, H000I8_A72AreaTrabalho_Ativo, H000I8_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H000I9_A63ContratanteUsuario_ContratanteCod, H000I9_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               H000I10_A57Usuario_PessoaCod, H000I10_A1073Usuario_CargoCod, H000I10_n1073Usuario_CargoCod, H000I10_A2Usuario_Nome, H000I10_n2Usuario_Nome, H000I10_A54Usuario_Ativo, H000I10_A325Usuario_PessoaDoc, H000I10_n325Usuario_PessoaDoc, H000I10_A1074Usuario_CargoNom, H000I10_n1074Usuario_CargoNom,
               H000I10_A58Usuario_PessoaNom, H000I10_n58Usuario_PessoaNom, H000I10_A1Usuario_Codigo
               }
               , new Object[] {
               H000I11_A29Contratante_Codigo, H000I11_n29Contratante_Codigo, H000I11_A72AreaTrabalho_Ativo, H000I11_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H000I12_A340ContratanteUsuario_ContratantePesCod, H000I12_n340ContratanteUsuario_ContratantePesCod, H000I12_A63ContratanteUsuario_ContratanteCod, H000I12_A60ContratanteUsuario_UsuarioCod, H000I12_A64ContratanteUsuario_ContratanteRaz, H000I12_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               H000I13_A66ContratadaUsuario_ContratadaCod, H000I13_A67ContratadaUsuario_ContratadaPessoaCod, H000I13_n67ContratadaUsuario_ContratadaPessoaCod, H000I13_A69ContratadaUsuario_UsuarioCod, H000I13_A68ContratadaUsuario_ContratadaPessoaNom, H000I13_n68ContratadaUsuario_ContratadaPessoaNom
               }
            }
         );
         AV250Pgmname = "WWUsuario";
         /* GeneXus formulas. */
         AV250Pgmname = "WWUsuario";
         context.Gx_err = 0;
         edtavEntidade_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_116 ;
      private short nGXsfl_116_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV106TFUsuario_Ativo_Sel ;
      private short AV199PaginaAtual ;
      private short AV192Preview ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_116_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV213WWUsuarioDS_2_Dynamicfiltersoperator1 ;
      private short AV222WWUsuarioDS_11_Dynamicfiltersoperator2 ;
      private short AV231WWUsuarioDS_20_Dynamicfiltersoperator3 ;
      private short AV244WWUsuarioDS_33_Tfusuario_ativo_sel ;
      private short edtUsuario_PessoaNom_Titleformat ;
      private short edtUsuario_CargoNom_Titleformat ;
      private short edtUsuario_Nome_Titleformat ;
      private short chkUsuario_Ativo_Titleformat ;
      private short AV200Registros ;
      private short AV202q ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV172AreaTrabalho_Codigo1 ;
      private int AV173AreaTrabalho_Codigo2 ;
      private int AV184AreaTrabalho_Codigo3 ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1Usuario_Codigo ;
      private int A1073Usuario_CargoCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A29Contratante_Codigo ;
      private int AV83Usuario_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_usuario_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_usuario_cargonom_Datalistupdateminimumcharacters ;
      private int Ddo_usuario_nome_Datalistupdateminimumcharacters ;
      private int edtavTfusuario_pessoanom_Visible ;
      private int edtavTfusuario_pessoanom_sel_Visible ;
      private int edtavTfusuario_cargonom_Visible ;
      private int edtavTfusuario_cargonom_sel_Visible ;
      private int edtavTfusuario_nome_Visible ;
      private int edtavTfusuario_nome_sel_Visible ;
      private int edtavTfusuario_ativo_sel_Visible ;
      private int edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuario_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuario_ativotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavEntidade_Enabled ;
      private int A57Usuario_PessoaCod ;
      private int AV214WWUsuarioDS_3_Areatrabalho_codigo1 ;
      private int AV223WWUsuarioDS_12_Areatrabalho_codigo2 ;
      private int AV232WWUsuarioDS_21_Areatrabalho_codigo3 ;
      private int edtavOrdereddsc_Visible ;
      private int edtavDelete_Enabled ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV188PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDisplay_Enabled ;
      private int edtUsuario_PessoaNom_Forecolor ;
      private int edtUsuario_CargoNom_Forecolor ;
      private int edtUsuario_Nome_Forecolor ;
      private int edtavEntidade_Forecolor ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUsuario_pessoanom1_Visible ;
      private int edtavUsuario_cargonom1_Visible ;
      private int edtavUsuario_pessoadoc1_Visible ;
      private int edtavUsuario_entidade1_Visible ;
      private int edtavUsuario_pessoanom2_Visible ;
      private int edtavUsuario_cargonom2_Visible ;
      private int edtavUsuario_pessoadoc2_Visible ;
      private int edtavUsuario_entidade2_Visible ;
      private int edtavUsuario_pessoanom3_Visible ;
      private int edtavUsuario_cargonom3_Visible ;
      private int edtavUsuario_pessoadoc3_Visible ;
      private int edtavUsuario_entidade3_Visible ;
      private int AV251GXV1 ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavEntidade_Visible ;
      private int edtavAssociarperfil_Enabled ;
      private int edtavAssociarperfil_Visible ;
      private int edtavAssociarcontratadas_Enabled ;
      private int edtavAssociarcontratadas_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV189GridCurrentPage ;
      private long AV190GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV208Color ;
      private long GXt_int3 ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_usuario_pessoanom_Activeeventkey ;
      private String Ddo_usuario_pessoanom_Filteredtext_get ;
      private String Ddo_usuario_pessoanom_Selectedvalue_get ;
      private String Ddo_usuario_cargonom_Activeeventkey ;
      private String Ddo_usuario_cargonom_Filteredtext_get ;
      private String Ddo_usuario_cargonom_Selectedvalue_get ;
      private String Ddo_usuario_nome_Activeeventkey ;
      private String Ddo_usuario_nome_Filteredtext_get ;
      private String Ddo_usuario_nome_Selectedvalue_get ;
      private String Ddo_usuario_ativo_Activeeventkey ;
      private String Ddo_usuario_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_116_idx="0001" ;
      private String AV38Usuario_PessoaNom1 ;
      private String AV182Usuario_Ativo1 ;
      private String AV39Usuario_PessoaNom2 ;
      private String AV183Usuario_Ativo2 ;
      private String AV40Usuario_PessoaNom3 ;
      private String AV187Usuario_Ativo3 ;
      private String AV90TFUsuario_PessoaNom ;
      private String AV91TFUsuario_PessoaNom_Sel ;
      private String AV86TFUsuario_Nome ;
      private String AV87TFUsuario_Nome_Sel ;
      private String AV81Usuario_Entidade1 ;
      private String AV82Usuario_Entidade2 ;
      private String AV186Usuario_Entidade3 ;
      private String AV250Pgmname ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1083Usuario_Entidade ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_usuario_pessoanom_Caption ;
      private String Ddo_usuario_pessoanom_Tooltip ;
      private String Ddo_usuario_pessoanom_Cls ;
      private String Ddo_usuario_pessoanom_Filteredtext_set ;
      private String Ddo_usuario_pessoanom_Selectedvalue_set ;
      private String Ddo_usuario_pessoanom_Dropdownoptionstype ;
      private String Ddo_usuario_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_usuario_pessoanom_Sortedstatus ;
      private String Ddo_usuario_pessoanom_Filtertype ;
      private String Ddo_usuario_pessoanom_Datalisttype ;
      private String Ddo_usuario_pessoanom_Datalistproc ;
      private String Ddo_usuario_pessoanom_Sortasc ;
      private String Ddo_usuario_pessoanom_Sortdsc ;
      private String Ddo_usuario_pessoanom_Loadingdata ;
      private String Ddo_usuario_pessoanom_Cleanfilter ;
      private String Ddo_usuario_pessoanom_Noresultsfound ;
      private String Ddo_usuario_pessoanom_Searchbuttontext ;
      private String Ddo_usuario_cargonom_Caption ;
      private String Ddo_usuario_cargonom_Tooltip ;
      private String Ddo_usuario_cargonom_Cls ;
      private String Ddo_usuario_cargonom_Filteredtext_set ;
      private String Ddo_usuario_cargonom_Selectedvalue_set ;
      private String Ddo_usuario_cargonom_Dropdownoptionstype ;
      private String Ddo_usuario_cargonom_Titlecontrolidtoreplace ;
      private String Ddo_usuario_cargonom_Sortedstatus ;
      private String Ddo_usuario_cargonom_Filtertype ;
      private String Ddo_usuario_cargonom_Datalisttype ;
      private String Ddo_usuario_cargonom_Datalistproc ;
      private String Ddo_usuario_cargonom_Sortasc ;
      private String Ddo_usuario_cargonom_Sortdsc ;
      private String Ddo_usuario_cargonom_Loadingdata ;
      private String Ddo_usuario_cargonom_Cleanfilter ;
      private String Ddo_usuario_cargonom_Noresultsfound ;
      private String Ddo_usuario_cargonom_Searchbuttontext ;
      private String Ddo_usuario_nome_Caption ;
      private String Ddo_usuario_nome_Tooltip ;
      private String Ddo_usuario_nome_Cls ;
      private String Ddo_usuario_nome_Filteredtext_set ;
      private String Ddo_usuario_nome_Selectedvalue_set ;
      private String Ddo_usuario_nome_Dropdownoptionstype ;
      private String Ddo_usuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_usuario_nome_Sortedstatus ;
      private String Ddo_usuario_nome_Filtertype ;
      private String Ddo_usuario_nome_Datalisttype ;
      private String Ddo_usuario_nome_Datalistproc ;
      private String Ddo_usuario_nome_Sortasc ;
      private String Ddo_usuario_nome_Sortdsc ;
      private String Ddo_usuario_nome_Loadingdata ;
      private String Ddo_usuario_nome_Cleanfilter ;
      private String Ddo_usuario_nome_Noresultsfound ;
      private String Ddo_usuario_nome_Searchbuttontext ;
      private String Ddo_usuario_ativo_Caption ;
      private String Ddo_usuario_ativo_Tooltip ;
      private String Ddo_usuario_ativo_Cls ;
      private String Ddo_usuario_ativo_Selectedvalue_set ;
      private String Ddo_usuario_ativo_Dropdownoptionstype ;
      private String Ddo_usuario_ativo_Titlecontrolidtoreplace ;
      private String Ddo_usuario_ativo_Sortedstatus ;
      private String Ddo_usuario_ativo_Datalisttype ;
      private String Ddo_usuario_ativo_Datalistfixedvalues ;
      private String Ddo_usuario_ativo_Sortasc ;
      private String Ddo_usuario_ativo_Sortdsc ;
      private String Ddo_usuario_ativo_Cleanfilter ;
      private String Ddo_usuario_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfusuario_pessoanom_Internalname ;
      private String edtavTfusuario_pessoanom_Jsonclick ;
      private String edtavTfusuario_pessoanom_sel_Internalname ;
      private String edtavTfusuario_pessoanom_sel_Jsonclick ;
      private String edtavTfusuario_cargonom_Internalname ;
      private String edtavTfusuario_cargonom_Jsonclick ;
      private String edtavTfusuario_cargonom_sel_Internalname ;
      private String edtavTfusuario_cargonom_sel_Jsonclick ;
      private String edtavTfusuario_nome_Internalname ;
      private String edtavTfusuario_nome_Jsonclick ;
      private String edtavTfusuario_nome_sel_Internalname ;
      private String edtavTfusuario_nome_sel_Jsonclick ;
      private String edtavTfusuario_ativo_sel_Internalname ;
      private String edtavTfusuario_ativo_sel_Jsonclick ;
      private String edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuario_cargonomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuario_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtUsuario_Codigo_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String edtUsuario_CargoNom_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String AV198Entidade ;
      private String edtavEntidade_Internalname ;
      private String chkUsuario_Ativo_Internalname ;
      private String edtavAssociarperfil_Internalname ;
      private String edtavAssociarcontratadas_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV215WWUsuarioDS_4_Usuario_pessoanom1 ;
      private String lV224WWUsuarioDS_13_Usuario_pessoanom2 ;
      private String lV233WWUsuarioDS_22_Usuario_pessoanom3 ;
      private String lV238WWUsuarioDS_27_Tfusuario_pessoanom ;
      private String lV242WWUsuarioDS_31_Tfusuario_nome ;
      private String AV215WWUsuarioDS_4_Usuario_pessoanom1 ;
      private String AV219WWUsuarioDS_8_Usuario_ativo1 ;
      private String AV224WWUsuarioDS_13_Usuario_pessoanom2 ;
      private String AV228WWUsuarioDS_17_Usuario_ativo2 ;
      private String AV233WWUsuarioDS_22_Usuario_pessoanom3 ;
      private String AV237WWUsuarioDS_26_Usuario_ativo3 ;
      private String AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel ;
      private String AV238WWUsuarioDS_27_Tfusuario_pessoanom ;
      private String AV243WWUsuarioDS_32_Tfusuario_nome_sel ;
      private String AV242WWUsuarioDS_31_Tfusuario_nome ;
      private String AV218WWUsuarioDS_7_Usuario_entidade1 ;
      private String AV227WWUsuarioDS_16_Usuario_entidade2 ;
      private String AV236WWUsuarioDS_25_Usuario_entidade3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String dynavAreatrabalho_codigo1_Internalname ;
      private String edtavUsuario_pessoanom1_Internalname ;
      private String edtavUsuario_cargonom1_Internalname ;
      private String edtavUsuario_pessoadoc1_Internalname ;
      private String edtavUsuario_entidade1_Internalname ;
      private String cmbavUsuario_ativo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String dynavAreatrabalho_codigo2_Internalname ;
      private String edtavUsuario_pessoanom2_Internalname ;
      private String edtavUsuario_cargonom2_Internalname ;
      private String edtavUsuario_pessoadoc2_Internalname ;
      private String edtavUsuario_entidade2_Internalname ;
      private String cmbavUsuario_ativo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String dynavAreatrabalho_codigo3_Internalname ;
      private String edtavUsuario_pessoanom3_Internalname ;
      private String edtavUsuario_cargonom3_Internalname ;
      private String edtavUsuario_pessoadoc3_Internalname ;
      private String edtavUsuario_entidade3_Internalname ;
      private String cmbavUsuario_ativo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_usuario_pessoanom_Internalname ;
      private String Ddo_usuario_cargonom_Internalname ;
      private String Ddo_usuario_nome_Internalname ;
      private String Ddo_usuario_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUsuario_PessoaNom_Title ;
      private String edtUsuario_CargoNom_Title ;
      private String edtUsuario_Nome_Title ;
      private String edtavAssociarperfil_Title ;
      private String edtavAssociarcontratadas_Title ;
      private String Gridpaginationbar_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavAssociarperfil_Tooltiptext ;
      private String edtavAssociarcontratadas_Tooltiptext ;
      private String edtUsuario_PessoaNom_Link ;
      private String edtUsuario_CargoNom_Link ;
      private String GXt_char1 ;
      private String AV203ToolTips ;
      private String edtavEntidade_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUsertable_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTableactions_Internalname ;
      private String lblUsuariotitle_Internalname ;
      private String lblUsuariotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String dynavAreatrabalho_codigo3_Jsonclick ;
      private String edtavUsuario_pessoanom3_Jsonclick ;
      private String edtavUsuario_cargonom3_Jsonclick ;
      private String edtavUsuario_pessoadoc3_Jsonclick ;
      private String edtavUsuario_entidade3_Jsonclick ;
      private String cmbavUsuario_ativo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String dynavAreatrabalho_codigo2_Jsonclick ;
      private String edtavUsuario_pessoanom2_Jsonclick ;
      private String edtavUsuario_cargonom2_Jsonclick ;
      private String edtavUsuario_pessoadoc2_Jsonclick ;
      private String edtavUsuario_entidade2_Jsonclick ;
      private String cmbavUsuario_ativo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String dynavAreatrabalho_codigo1_Jsonclick ;
      private String edtavUsuario_pessoanom1_Jsonclick ;
      private String edtavUsuario_cargonom1_Jsonclick ;
      private String edtavUsuario_pessoadoc1_Jsonclick ;
      private String edtavUsuario_entidade1_Jsonclick ;
      private String cmbavUsuario_ativo1_Jsonclick ;
      private String tblTablemergedtabletitle_Internalname ;
      private String tblTablesearch_Internalname ;
      private String tblTabletitle_Internalname ;
      private String sGXsfl_116_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String edtUsuario_CargoNom_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtavEntidade_Jsonclick ;
      private String edtavAssociarperfil_Jsonclick ;
      private String edtavAssociarcontratadas_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool n1073Usuario_CargoCod ;
      private bool A54Usuario_Ativo ;
      private bool n29Contratante_Codigo ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_usuario_pessoanom_Includesortasc ;
      private bool Ddo_usuario_pessoanom_Includesortdsc ;
      private bool Ddo_usuario_pessoanom_Includefilter ;
      private bool Ddo_usuario_pessoanom_Filterisrange ;
      private bool Ddo_usuario_pessoanom_Includedatalist ;
      private bool Ddo_usuario_cargonom_Includesortasc ;
      private bool Ddo_usuario_cargonom_Includesortdsc ;
      private bool Ddo_usuario_cargonom_Includefilter ;
      private bool Ddo_usuario_cargonom_Filterisrange ;
      private bool Ddo_usuario_cargonom_Includedatalist ;
      private bool Ddo_usuario_nome_Includesortasc ;
      private bool Ddo_usuario_nome_Includesortdsc ;
      private bool Ddo_usuario_nome_Includefilter ;
      private bool Ddo_usuario_nome_Filterisrange ;
      private bool Ddo_usuario_nome_Includedatalist ;
      private bool Ddo_usuario_ativo_Includesortasc ;
      private bool Ddo_usuario_ativo_Includesortdsc ;
      private bool Ddo_usuario_ativo_Includefilter ;
      private bool Ddo_usuario_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n58Usuario_PessoaNom ;
      private bool n1074Usuario_CargoNom ;
      private bool n2Usuario_Nome ;
      private bool AV220WWUsuarioDS_9_Dynamicfiltersenabled2 ;
      private bool AV229WWUsuarioDS_18_Dynamicfiltersenabled3 ;
      private bool n325Usuario_PessoaDoc ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool A72AreaTrabalho_Ativo ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool AV33Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV113Display_IsBlob ;
      private bool AV31AssociarPerfil_IsBlob ;
      private bool AV209AssociarContratadas_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV110Usuario_CargoNom1 ;
      private String AV73Usuario_PessoaDoc1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV112Usuario_CargoNom2 ;
      private String AV74Usuario_PessoaDoc2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV185Usuario_CargoNom3 ;
      private String AV78Usuario_PessoaDoc3 ;
      private String AV123TFUsuario_CargoNom ;
      private String AV124TFUsuario_CargoNom_Sel ;
      private String AV92ddo_Usuario_PessoaNomTitleControlIdToReplace ;
      private String AV125ddo_Usuario_CargoNomTitleControlIdToReplace ;
      private String AV88ddo_Usuario_NomeTitleControlIdToReplace ;
      private String AV107ddo_Usuario_AtivoTitleControlIdToReplace ;
      private String AV245Update_GXI ;
      private String AV246Delete_GXI ;
      private String AV247Display_GXI ;
      private String A1074Usuario_CargoNom ;
      private String AV248Associarperfil_GXI ;
      private String AV249Associarcontratadas_GXI ;
      private String lV216WWUsuarioDS_5_Usuario_cargonom1 ;
      private String lV217WWUsuarioDS_6_Usuario_pessoadoc1 ;
      private String lV225WWUsuarioDS_14_Usuario_cargonom2 ;
      private String lV226WWUsuarioDS_15_Usuario_pessoadoc2 ;
      private String lV234WWUsuarioDS_23_Usuario_cargonom3 ;
      private String lV235WWUsuarioDS_24_Usuario_pessoadoc3 ;
      private String lV240WWUsuarioDS_29_Tfusuario_cargonom ;
      private String AV212WWUsuarioDS_1_Dynamicfiltersselector1 ;
      private String AV216WWUsuarioDS_5_Usuario_cargonom1 ;
      private String AV217WWUsuarioDS_6_Usuario_pessoadoc1 ;
      private String AV221WWUsuarioDS_10_Dynamicfiltersselector2 ;
      private String AV225WWUsuarioDS_14_Usuario_cargonom2 ;
      private String AV226WWUsuarioDS_15_Usuario_pessoadoc2 ;
      private String AV230WWUsuarioDS_19_Dynamicfiltersselector3 ;
      private String AV234WWUsuarioDS_23_Usuario_cargonom3 ;
      private String AV235WWUsuarioDS_24_Usuario_pessoadoc3 ;
      private String AV241WWUsuarioDS_30_Tfusuario_cargonom_sel ;
      private String AV240WWUsuarioDS_29_Tfusuario_cargonom ;
      private String A325Usuario_PessoaDoc ;
      private String AV33Update ;
      private String AV32Delete ;
      private String AV113Display ;
      private String AV31AssociarPerfil ;
      private String AV209AssociarContratadas ;
      private IGxSession AV30Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox dynavAreatrabalho_codigo1 ;
      private GXCombobox cmbavUsuario_ativo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox dynavAreatrabalho_codigo2 ;
      private GXCombobox cmbavUsuario_ativo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox dynavAreatrabalho_codigo3 ;
      private GXCombobox cmbavUsuario_ativo3 ;
      private GXCheckbox chkUsuario_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H000I2_A5AreaTrabalho_Codigo ;
      private String[] H000I2_A6AreaTrabalho_Descricao ;
      private bool[] H000I2_A72AreaTrabalho_Ativo ;
      private int[] H000I3_A5AreaTrabalho_Codigo ;
      private String[] H000I3_A6AreaTrabalho_Descricao ;
      private bool[] H000I3_A72AreaTrabalho_Ativo ;
      private int[] H000I4_A5AreaTrabalho_Codigo ;
      private String[] H000I4_A6AreaTrabalho_Descricao ;
      private bool[] H000I4_A72AreaTrabalho_Ativo ;
      private int[] H000I5_A57Usuario_PessoaCod ;
      private String[] H000I5_A325Usuario_PessoaDoc ;
      private bool[] H000I5_n325Usuario_PessoaDoc ;
      private int[] H000I5_A1073Usuario_CargoCod ;
      private bool[] H000I5_n1073Usuario_CargoCod ;
      private bool[] H000I5_A54Usuario_Ativo ;
      private String[] H000I5_A2Usuario_Nome ;
      private bool[] H000I5_n2Usuario_Nome ;
      private String[] H000I5_A1074Usuario_CargoNom ;
      private bool[] H000I5_n1074Usuario_CargoNom ;
      private String[] H000I5_A58Usuario_PessoaNom ;
      private bool[] H000I5_n58Usuario_PessoaNom ;
      private int[] H000I5_A1Usuario_Codigo ;
      private int[] H000I6_A52Contratada_AreaTrabalhoCod ;
      private int[] H000I6_A40Contratada_PessoaCod ;
      private int[] H000I7_A66ContratadaUsuario_ContratadaCod ;
      private int[] H000I7_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H000I7_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] H000I7_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H000I7_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H000I7_A69ContratadaUsuario_UsuarioCod ;
      private int[] H000I8_A29Contratante_Codigo ;
      private bool[] H000I8_n29Contratante_Codigo ;
      private bool[] H000I8_A72AreaTrabalho_Ativo ;
      private int[] H000I8_A5AreaTrabalho_Codigo ;
      private int[] H000I9_A63ContratanteUsuario_ContratanteCod ;
      private int[] H000I9_A60ContratanteUsuario_UsuarioCod ;
      private int[] H000I10_A57Usuario_PessoaCod ;
      private int[] H000I10_A1073Usuario_CargoCod ;
      private bool[] H000I10_n1073Usuario_CargoCod ;
      private String[] H000I10_A2Usuario_Nome ;
      private bool[] H000I10_n2Usuario_Nome ;
      private bool[] H000I10_A54Usuario_Ativo ;
      private String[] H000I10_A325Usuario_PessoaDoc ;
      private bool[] H000I10_n325Usuario_PessoaDoc ;
      private String[] H000I10_A1074Usuario_CargoNom ;
      private bool[] H000I10_n1074Usuario_CargoNom ;
      private String[] H000I10_A58Usuario_PessoaNom ;
      private bool[] H000I10_n58Usuario_PessoaNom ;
      private int[] H000I10_A1Usuario_Codigo ;
      private int[] H000I11_A29Contratante_Codigo ;
      private bool[] H000I11_n29Contratante_Codigo ;
      private bool[] H000I11_A72AreaTrabalho_Ativo ;
      private int[] H000I11_A5AreaTrabalho_Codigo ;
      private int[] H000I12_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H000I12_n340ContratanteUsuario_ContratantePesCod ;
      private int[] H000I12_A63ContratanteUsuario_ContratanteCod ;
      private int[] H000I12_A60ContratanteUsuario_UsuarioCod ;
      private String[] H000I12_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H000I12_n64ContratanteUsuario_ContratanteRaz ;
      private int[] H000I13_A66ContratadaUsuario_ContratadaCod ;
      private int[] H000I13_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H000I13_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] H000I13_A69ContratadaUsuario_UsuarioCod ;
      private String[] H000I13_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H000I13_n68ContratadaUsuario_ContratadaPessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV193Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV204Pessoas ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV89Usuario_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV122Usuario_CargoNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV85Usuario_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV105Usuario_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV108DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000I5( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV193Codigos ,
                                             String AV212WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                             String AV215WWUsuarioDS_4_Usuario_pessoanom1 ,
                                             short AV213WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV216WWUsuarioDS_5_Usuario_cargonom1 ,
                                             String AV217WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                             String AV219WWUsuarioDS_8_Usuario_ativo1 ,
                                             bool AV220WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                             String AV221WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                             String AV224WWUsuarioDS_13_Usuario_pessoanom2 ,
                                             short AV222WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                             String AV225WWUsuarioDS_14_Usuario_cargonom2 ,
                                             String AV226WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                             String AV228WWUsuarioDS_17_Usuario_ativo2 ,
                                             bool AV229WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                             String AV230WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                             String AV233WWUsuarioDS_22_Usuario_pessoanom3 ,
                                             short AV231WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                             String AV234WWUsuarioDS_23_Usuario_cargonom3 ,
                                             String AV235WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                             String AV237WWUsuarioDS_26_Usuario_ativo3 ,
                                             String AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                             String AV238WWUsuarioDS_27_Tfusuario_pessoanom ,
                                             String AV241WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                             String AV240WWUsuarioDS_29_Tfusuario_cargonom ,
                                             String AV243WWUsuarioDS_32_Tfusuario_nome_sel ,
                                             String AV242WWUsuarioDS_31_Tfusuario_nome ,
                                             short AV244WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                             int AV172AreaTrabalho_Codigo1 ,
                                             int AV173AreaTrabalho_Codigo2 ,
                                             int AV184AreaTrabalho_Codigo3 ,
                                             String A58Usuario_PessoaNom ,
                                             String A1074Usuario_CargoNom ,
                                             String A325Usuario_PessoaDoc ,
                                             bool A54Usuario_Ativo ,
                                             String A2Usuario_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             String AV218WWUsuarioDS_7_Usuario_entidade1 ,
                                             String A1083Usuario_Entidade ,
                                             String AV227WWUsuarioDS_16_Usuario_entidade2 ,
                                             String AV236WWUsuarioDS_25_Usuario_entidade3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [21] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Ativo], T1.[Usuario_Nome], T3.[Cargo_Nome] AS Usuario_CargoNom, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM (([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod])";
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV215WWUsuarioDS_4_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV215WWUsuarioDS_4_Usuario_pessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV215WWUsuarioDS_4_Usuario_pessoanom1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV213WWUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV213WWUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV217WWUsuarioDS_6_Usuario_pessoadoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV217WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV217WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV219WWUsuarioDS_8_Usuario_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV219WWUsuarioDS_8_Usuario_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224WWUsuarioDS_13_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV224WWUsuarioDS_13_Usuario_pessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV224WWUsuarioDS_13_Usuario_pessoanom2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV222WWUsuarioDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV222WWUsuarioDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226WWUsuarioDS_15_Usuario_pessoadoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV226WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV226WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV228WWUsuarioDS_17_Usuario_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV228WWUsuarioDS_17_Usuario_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV233WWUsuarioDS_22_Usuario_pessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV233WWUsuarioDS_22_Usuario_pessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV233WWUsuarioDS_22_Usuario_pessoanom3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV231WWUsuarioDS_20_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV231WWUsuarioDS_20_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV235WWUsuarioDS_24_Usuario_pessoadoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV235WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV235WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV237WWUsuarioDS_26_Usuario_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV237WWUsuarioDS_26_Usuario_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV238WWUsuarioDS_27_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV238WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV238WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240WWUsuarioDS_29_Tfusuario_cargonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV240WWUsuarioDS_29_Tfusuario_cargonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV240WWUsuarioDS_29_Tfusuario_cargonom)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] = @AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] = @AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV243WWUsuarioDS_32_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV242WWUsuarioDS_31_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] like @lV242WWUsuarioDS_31_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] like @lV242WWUsuarioDS_31_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243WWUsuarioDS_32_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] = @AV243WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] = @AV243WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV244WWUsuarioDS_33_Tfusuario_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV244WWUsuarioDS_33_Tfusuario_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 0)";
            }
         }
         if ( AV172AreaTrabalho_Codigo1 + AV173AreaTrabalho_Codigo2 + AV184AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV193Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV193Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Nome] DESC";
         }
         else if ( AV13OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( AV13OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Cargo_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Cargo_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Nome]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Ativo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Ativo] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H000I6( IGxContext context ,
                                             int AV172AreaTrabalho_Codigo1 ,
                                             int AV173AreaTrabalho_Codigo2 ,
                                             int AV184AreaTrabalho_Codigo3 ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [3] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [Contratada_AreaTrabalhoCod], [Contratada_PessoaCod] FROM ( SELECT TOP(100) PERCENT [Contratada_AreaTrabalhoCod], [Contratada_PessoaCod] FROM [Contratada] WITH (NOLOCK)";
         if ( AV172AreaTrabalho_Codigo1 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_AreaTrabalhoCod] = @AV172AreaTrabalho_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_AreaTrabalhoCod] = @AV172AreaTrabalho_Codigo1)";
            }
         }
         else
         {
            GXv_int6[0] = 1;
         }
         if ( AV173AreaTrabalho_Codigo2 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_AreaTrabalhoCod] = @AV173AreaTrabalho_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_AreaTrabalhoCod] = @AV173AreaTrabalho_Codigo2)";
            }
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( AV184AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_AreaTrabalhoCod] = @AV184AreaTrabalho_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_AreaTrabalhoCod] = @AV184AreaTrabalho_Codigo3)";
            }
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Contratada_PessoaCod]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_H000I7( IGxContext context ,
                                             int A67ContratadaUsuario_ContratadaPessoaCod ,
                                             IGxCollection AV204Pessoas ,
                                             int AV172AreaTrabalho_Codigo1 ,
                                             int AV173AreaTrabalho_Codigo2 ,
                                             int AV184AreaTrabalho_Codigo3 ,
                                             int A1228ContratadaUsuario_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [3] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV204Pessoas, "T2.[Contratada_PessoaCod] IN (", ")") + ")";
         if ( AV172AreaTrabalho_Codigo1 > 0 )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV172AreaTrabalho_Codigo1)";
         }
         else
         {
            GXv_int8[0] = 1;
         }
         if ( AV173AreaTrabalho_Codigo2 > 0 )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV173AreaTrabalho_Codigo2)";
         }
         else
         {
            GXv_int8[1] = 1;
         }
         if ( AV184AreaTrabalho_Codigo3 > 0 )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV184AreaTrabalho_Codigo3)";
         }
         else
         {
            GXv_int8[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      protected Object[] conditional_H000I8( IGxContext context ,
                                             int AV172AreaTrabalho_Codigo1 ,
                                             int AV173AreaTrabalho_Codigo2 ,
                                             int AV184AreaTrabalho_Codigo3 ,
                                             int A5AreaTrabalho_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [3] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Ativo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK)";
         if ( AV172AreaTrabalho_Codigo1 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV172AreaTrabalho_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV172AreaTrabalho_Codigo1)";
            }
         }
         else
         {
            GXv_int10[0] = 1;
         }
         if ( AV173AreaTrabalho_Codigo2 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV173AreaTrabalho_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV173AreaTrabalho_Codigo2)";
            }
         }
         else
         {
            GXv_int10[1] = 1;
         }
         if ( AV184AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV184AreaTrabalho_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV184AreaTrabalho_Codigo3)";
            }
         }
         else
         {
            GXv_int10[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Codigo]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      protected Object[] conditional_H000I10( IGxContext context ,
                                              int A1Usuario_Codigo ,
                                              IGxCollection AV193Codigos ,
                                              String AV212WWUsuarioDS_1_Dynamicfiltersselector1 ,
                                              String AV215WWUsuarioDS_4_Usuario_pessoanom1 ,
                                              short AV213WWUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              String AV216WWUsuarioDS_5_Usuario_cargonom1 ,
                                              String AV217WWUsuarioDS_6_Usuario_pessoadoc1 ,
                                              String AV219WWUsuarioDS_8_Usuario_ativo1 ,
                                              bool AV220WWUsuarioDS_9_Dynamicfiltersenabled2 ,
                                              String AV221WWUsuarioDS_10_Dynamicfiltersselector2 ,
                                              String AV224WWUsuarioDS_13_Usuario_pessoanom2 ,
                                              short AV222WWUsuarioDS_11_Dynamicfiltersoperator2 ,
                                              String AV225WWUsuarioDS_14_Usuario_cargonom2 ,
                                              String AV226WWUsuarioDS_15_Usuario_pessoadoc2 ,
                                              String AV228WWUsuarioDS_17_Usuario_ativo2 ,
                                              bool AV229WWUsuarioDS_18_Dynamicfiltersenabled3 ,
                                              String AV230WWUsuarioDS_19_Dynamicfiltersselector3 ,
                                              String AV233WWUsuarioDS_22_Usuario_pessoanom3 ,
                                              short AV231WWUsuarioDS_20_Dynamicfiltersoperator3 ,
                                              String AV234WWUsuarioDS_23_Usuario_cargonom3 ,
                                              String AV235WWUsuarioDS_24_Usuario_pessoadoc3 ,
                                              String AV237WWUsuarioDS_26_Usuario_ativo3 ,
                                              String AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel ,
                                              String AV238WWUsuarioDS_27_Tfusuario_pessoanom ,
                                              String AV241WWUsuarioDS_30_Tfusuario_cargonom_sel ,
                                              String AV240WWUsuarioDS_29_Tfusuario_cargonom ,
                                              String AV243WWUsuarioDS_32_Tfusuario_nome_sel ,
                                              String AV242WWUsuarioDS_31_Tfusuario_nome ,
                                              short AV244WWUsuarioDS_33_Tfusuario_ativo_sel ,
                                              int AV172AreaTrabalho_Codigo1 ,
                                              int AV173AreaTrabalho_Codigo2 ,
                                              int AV184AreaTrabalho_Codigo3 ,
                                              String A58Usuario_PessoaNom ,
                                              String A1074Usuario_CargoNom ,
                                              String A325Usuario_PessoaDoc ,
                                              bool A54Usuario_Ativo ,
                                              String A2Usuario_Nome ,
                                              String AV218WWUsuarioDS_7_Usuario_entidade1 ,
                                              String A1083Usuario_Entidade ,
                                              String AV227WWUsuarioDS_16_Usuario_entidade2 ,
                                              String AV236WWUsuarioDS_25_Usuario_entidade3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [21] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Nome], T1.[Usuario_Ativo], T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T3.[Cargo_Nome] AS Usuario_CargoNom, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM (([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod])";
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int12[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV215WWUsuarioDS_4_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV215WWUsuarioDS_4_Usuario_pessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV215WWUsuarioDS_4_Usuario_pessoanom1)";
            }
         }
         else
         {
            GXv_int12[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV213WWUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int12[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_CARGONOM") == 0 ) && ( AV213WWUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV216WWUsuarioDS_5_Usuario_cargonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV216WWUsuarioDS_5_Usuario_cargonom1)";
            }
         }
         else
         {
            GXv_int12[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV217WWUsuarioDS_6_Usuario_pessoadoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV217WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV217WWUsuarioDS_6_Usuario_pessoadoc1)";
            }
         }
         else
         {
            GXv_int12[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV219WWUsuarioDS_8_Usuario_ativo1, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( ( StringUtil.StrCmp(AV212WWUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV219WWUsuarioDS_8_Usuario_ativo1, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int12[5] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224WWUsuarioDS_13_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV224WWUsuarioDS_13_Usuario_pessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV224WWUsuarioDS_13_Usuario_pessoanom2)";
            }
         }
         else
         {
            GXv_int12[6] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV222WWUsuarioDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int12[7] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_CARGONOM") == 0 ) && ( AV222WWUsuarioDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225WWUsuarioDS_14_Usuario_cargonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV225WWUsuarioDS_14_Usuario_cargonom2)";
            }
         }
         else
         {
            GXv_int12[8] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226WWUsuarioDS_15_Usuario_pessoadoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV226WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV226WWUsuarioDS_15_Usuario_pessoadoc2)";
            }
         }
         else
         {
            GXv_int12[9] = 1;
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV228WWUsuarioDS_17_Usuario_ativo2, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV220WWUsuarioDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV221WWUsuarioDS_10_Dynamicfiltersselector2, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV228WWUsuarioDS_17_Usuario_ativo2, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "AREATRABALHO_CODIGO") == 0 ) && ( false ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Codigo] = @Usuario_Codigo)";
            }
         }
         else
         {
            GXv_int12[10] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV233WWUsuarioDS_22_Usuario_pessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV233WWUsuarioDS_22_Usuario_pessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV233WWUsuarioDS_22_Usuario_pessoanom3)";
            }
         }
         else
         {
            GXv_int12[11] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV231WWUsuarioDS_20_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int12[12] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_CARGONOM") == 0 ) && ( AV231WWUsuarioDS_20_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV234WWUsuarioDS_23_Usuario_cargonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV234WWUsuarioDS_23_Usuario_cargonom3)";
            }
         }
         else
         {
            GXv_int12[13] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV235WWUsuarioDS_24_Usuario_pessoadoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV235WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV235WWUsuarioDS_24_Usuario_pessoadoc3)";
            }
         }
         else
         {
            GXv_int12[14] = 1;
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV237WWUsuarioDS_26_Usuario_ativo3, "S") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV229WWUsuarioDS_18_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV230WWUsuarioDS_19_Dynamicfiltersselector3, "USUARIO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV237WWUsuarioDS_26_Usuario_ativo3, "N") == 0 ) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (Not T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (Not T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV238WWUsuarioDS_27_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV238WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV238WWUsuarioDS_27_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int12[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int12[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240WWUsuarioDS_29_Tfusuario_cargonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV240WWUsuarioDS_29_Tfusuario_cargonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV240WWUsuarioDS_29_Tfusuario_cargonom)";
            }
         }
         else
         {
            GXv_int12[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] = @AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] = @AV241WWUsuarioDS_30_Tfusuario_cargonom_sel)";
            }
         }
         else
         {
            GXv_int12[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV243WWUsuarioDS_32_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV242WWUsuarioDS_31_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] like @lV242WWUsuarioDS_31_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] like @lV242WWUsuarioDS_31_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int12[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243WWUsuarioDS_32_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Nome] = @AV243WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Nome] = @AV243WWUsuarioDS_32_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int12[20] = 1;
         }
         if ( AV244WWUsuarioDS_33_Tfusuario_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 1)";
            }
         }
         if ( AV244WWUsuarioDS_33_Tfusuario_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Usuario_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Usuario_Ativo] = 0)";
            }
         }
         if ( AV172AreaTrabalho_Codigo1 + AV173AreaTrabalho_Codigo2 + AV184AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV193Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV193Codigos, "T1.[Usuario_Codigo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Usuario_Codigo]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      protected Object[] conditional_H000I11( IGxContext context ,
                                              int AV172AreaTrabalho_Codigo1 ,
                                              int AV173AreaTrabalho_Codigo2 ,
                                              int AV184AreaTrabalho_Codigo3 ,
                                              int A5AreaTrabalho_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int14 ;
         GXv_int14 = new short [3] ;
         Object[] GXv_Object15 ;
         GXv_Object15 = new Object [2] ;
         scmdbuf = "SELECT [Contratante_Codigo], [AreaTrabalho_Ativo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK)";
         if ( AV172AreaTrabalho_Codigo1 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV172AreaTrabalho_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV172AreaTrabalho_Codigo1)";
            }
         }
         else
         {
            GXv_int14[0] = 1;
         }
         if ( AV173AreaTrabalho_Codigo2 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV173AreaTrabalho_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV173AreaTrabalho_Codigo2)";
            }
         }
         else
         {
            GXv_int14[1] = 1;
         }
         if ( AV184AreaTrabalho_Codigo3 > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV184AreaTrabalho_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV184AreaTrabalho_Codigo3)";
            }
         }
         else
         {
            GXv_int14[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Codigo]";
         GXv_Object15[0] = scmdbuf;
         GXv_Object15[1] = GXv_int14;
         return GXv_Object15 ;
      }

      protected Object[] conditional_H000I13( IGxContext context ,
                                              int A67ContratadaUsuario_ContratadaPessoaCod ,
                                              IGxCollection AV204Pessoas ,
                                              int AV172AreaTrabalho_Codigo1 ,
                                              int AV173AreaTrabalho_Codigo2 ,
                                              int AV184AreaTrabalho_Codigo3 ,
                                              int AV83Usuario_Codigo ,
                                              int A69ContratadaUsuario_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int16 ;
         GXv_int16 = new short [1] ;
         Object[] GXv_Object17 ;
         GXv_Object17 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod], T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPe FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV83Usuario_Codigo)";
         if ( AV172AreaTrabalho_Codigo1 + AV173AreaTrabalho_Codigo2 + AV184AreaTrabalho_Codigo3 > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV204Pessoas, "T2.[Contratada_PessoaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_UsuarioCod]";
         GXv_Object17[0] = scmdbuf;
         GXv_Object17[1] = GXv_int16;
         return GXv_Object17 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H000I5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (bool)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] );
               case 4 :
                     return conditional_H000I6(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 5 :
                     return conditional_H000I7(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 6 :
                     return conditional_H000I8(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 8 :
                     return conditional_H000I10(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (bool)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] );
               case 9 :
                     return conditional_H000I11(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 11 :
                     return conditional_H000I13(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000I2 ;
          prmH000I2 = new Object[] {
          } ;
          Object[] prmH000I3 ;
          prmH000I3 = new Object[] {
          } ;
          Object[] prmH000I4 ;
          prmH000I4 = new Object[] {
          } ;
          Object[] prmH000I9 ;
          prmH000I9 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000I12 ;
          prmH000I12 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV83Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000I5 ;
          prmH000I5 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV215WWUsuarioDS_4_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV216WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV216WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV217WWUsuarioDS_6_Usuario_pessoadoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224WWUsuarioDS_13_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV225WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV225WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV226WWUsuarioDS_15_Usuario_pessoadoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV233WWUsuarioDS_22_Usuario_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV234WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV234WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV235WWUsuarioDS_24_Usuario_pessoadoc3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV238WWUsuarioDS_27_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV240WWUsuarioDS_29_Tfusuario_cargonom",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV241WWUsuarioDS_30_Tfusuario_cargonom_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV242WWUsuarioDS_31_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV243WWUsuarioDS_32_Tfusuario_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmH000I6 ;
          prmH000I6 = new Object[] {
          new Object[] {"@AV172AreaTrabalho_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173AreaTrabalho_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184AreaTrabalho_Codigo3",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000I7 ;
          prmH000I7 = new Object[] {
          new Object[] {"@AV172AreaTrabalho_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173AreaTrabalho_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184AreaTrabalho_Codigo3",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000I8 ;
          prmH000I8 = new Object[] {
          new Object[] {"@AV172AreaTrabalho_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173AreaTrabalho_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184AreaTrabalho_Codigo3",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000I10 ;
          prmH000I10 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV215WWUsuarioDS_4_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV216WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV216WWUsuarioDS_5_Usuario_cargonom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV217WWUsuarioDS_6_Usuario_pessoadoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224WWUsuarioDS_13_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV225WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV225WWUsuarioDS_14_Usuario_cargonom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV226WWUsuarioDS_15_Usuario_pessoadoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV233WWUsuarioDS_22_Usuario_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV234WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV234WWUsuarioDS_23_Usuario_cargonom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV235WWUsuarioDS_24_Usuario_pessoadoc3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV238WWUsuarioDS_27_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV239WWUsuarioDS_28_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV240WWUsuarioDS_29_Tfusuario_cargonom",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV241WWUsuarioDS_30_Tfusuario_cargonom_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV242WWUsuarioDS_31_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV243WWUsuarioDS_32_Tfusuario_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmH000I11 ;
          prmH000I11 = new Object[] {
          new Object[] {"@AV172AreaTrabalho_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173AreaTrabalho_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184AreaTrabalho_Codigo3",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000I13 ;
          prmH000I13 = new Object[] {
          new Object[] {"@AV83Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000I2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I2,0,0,true,false )
             ,new CursorDef("H000I3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I3,0,0,true,false )
             ,new CursorDef("H000I4", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I4,0,0,true,false )
             ,new CursorDef("H000I5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I5,11,0,true,false )
             ,new CursorDef("H000I6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I6,100,0,false,false )
             ,new CursorDef("H000I7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I7,100,0,false,false )
             ,new CursorDef("H000I8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I8,1,0,true,true )
             ,new CursorDef("H000I9", "SELECT [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo ORDER BY [ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I9,100,0,false,false )
             ,new CursorDef("H000I10", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I10,11,0,true,false )
             ,new CursorDef("H000I11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I11,100,0,true,false )
             ,new CursorDef("H000I12", "SELECT T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T1.[ContratanteUsuario_UsuarioCod], T3.[Pessoa_Nome] AS ContratanteUsuario_Contratante FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV83Usuario_Codigo ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I12,1,0,false,true )
             ,new CursorDef("H000I13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000I13,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
