/*
               File: type_SdtSolicitacoesItens
        Description: Solicitacoes Itens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:4.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SolicitacoesItens" )]
   [XmlType(TypeName =  "SolicitacoesItens" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSolicitacoesItens : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacoesItens( )
      {
         /* Constructor for serialization */
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao = "";
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo = "";
         gxTv_SdtSolicitacoesItens_Mode = "";
      }

      public SdtSolicitacoesItens( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV447SolicitacoesItens_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV447SolicitacoesItens_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"SolicitacoesItens_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "SolicitacoesItens");
         metadata.Set("BT", "SolicitacoesItens");
         metadata.Set("PK", "[ \"SolicitacoesItens_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"SolicitacoesItens_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"FuncaoUsuario_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Solicitacoes_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoesitens_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaousuario_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoesitens_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoesitens_arquivo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSolicitacoesItens deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSolicitacoesItens)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSolicitacoesItens obj ;
         obj = this;
         obj.gxTpr_Solicitacoesitens_codigo = deserialized.gxTpr_Solicitacoesitens_codigo;
         obj.gxTpr_Solicitacoes_codigo = deserialized.gxTpr_Solicitacoes_codigo;
         obj.gxTpr_Solicitacoesitens_descricao = deserialized.gxTpr_Solicitacoesitens_descricao;
         obj.gxTpr_Solicitacoesitens_arquivo = deserialized.gxTpr_Solicitacoesitens_arquivo;
         obj.gxTpr_Funcaousuario_codigo = deserialized.gxTpr_Funcaousuario_codigo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Solicitacoesitens_codigo_Z = deserialized.gxTpr_Solicitacoesitens_codigo_Z;
         obj.gxTpr_Solicitacoes_codigo_Z = deserialized.gxTpr_Solicitacoes_codigo_Z;
         obj.gxTpr_Funcaousuario_codigo_Z = deserialized.gxTpr_Funcaousuario_codigo_Z;
         obj.gxTpr_Solicitacoesitens_descricao_N = deserialized.gxTpr_Solicitacoesitens_descricao_N;
         obj.gxTpr_Solicitacoesitens_arquivo_N = deserialized.gxTpr_Solicitacoesitens_arquivo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_Codigo") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Codigo") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoes_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_descricao") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_Arquivo") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Codigo") )
               {
                  gxTv_SdtSolicitacoesItens_Funcaousuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSolicitacoesItens_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSolicitacoesItens_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_descricao_N") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_Arquivo_N") )
               {
                  gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SolicitacoesItens";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("SolicitacoesItens_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Solicitacoes_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Solicitacoes_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicitacoesItens_descricao", StringUtil.RTrim( gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicitacoesItens_Arquivo", context.FileToBase64( gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FuncaoUsuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Funcaousuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSolicitacoesItens_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicitacoesItens_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Solicitacoes_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("FuncaoUsuario_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicitacoesItens_descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicitacoesItens_Arquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("SolicitacoesItens_Codigo", gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo, false);
         AddObjectProperty("Solicitacoes_Codigo", gxTv_SdtSolicitacoesItens_Solicitacoes_codigo, false);
         AddObjectProperty("SolicitacoesItens_descricao", gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao, false);
         AddObjectProperty("SolicitacoesItens_Arquivo", gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo, false);
         AddObjectProperty("FuncaoUsuario_Codigo", gxTv_SdtSolicitacoesItens_Funcaousuario_codigo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSolicitacoesItens_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtSolicitacoesItens_Initialized, false);
            AddObjectProperty("SolicitacoesItens_Codigo_Z", gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z, false);
            AddObjectProperty("Solicitacoes_Codigo_Z", gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z, false);
            AddObjectProperty("FuncaoUsuario_Codigo_Z", gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z, false);
            AddObjectProperty("SolicitacoesItens_descricao_N", gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N, false);
            AddObjectProperty("SolicitacoesItens_Arquivo_N", gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "SolicitacoesItens_Codigo" )]
      [  XmlElement( ElementName = "SolicitacoesItens_Codigo"   )]
      public int gxTpr_Solicitacoesitens_codigo
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo ;
         }

         set {
            if ( gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo != value )
            {
               gxTv_SdtSolicitacoesItens_Mode = "INS";
               this.gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z_SetNull( );
            }
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Codigo" )]
      [  XmlElement( ElementName = "Solicitacoes_Codigo"   )]
      public int gxTpr_Solicitacoes_codigo
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoes_codigo ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoes_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SolicitacoesItens_descricao" )]
      [  XmlElement( ElementName = "SolicitacoesItens_descricao"   )]
      public String gxTpr_Solicitacoesitens_descricao
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N = 0;
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N = 1;
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicitacoesItens_Arquivo" )]
      [  XmlElement( ElementName = "SolicitacoesItens_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Solicitacoesitens_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo) ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Solicitacoesitens_arquivo
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N = 0;
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo = value;
         }

      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_SetBlob( String blob ,
                                                                               String fileName ,
                                                                               String fileType )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo = blob;
         return  ;
      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N = 1;
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoUsuario_Codigo" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Codigo"   )]
      public int gxTpr_Funcaousuario_codigo
      {
         get {
            return gxTv_SdtSolicitacoesItens_Funcaousuario_codigo ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Funcaousuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSolicitacoesItens_Mode ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Mode_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSolicitacoesItens_Initialized ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Initialized_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicitacoesItens_Codigo_Z" )]
      [  XmlElement( ElementName = "SolicitacoesItens_Codigo_Z"   )]
      public int gxTpr_Solicitacoesitens_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Codigo_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Codigo_Z"   )]
      public int gxTpr_Solicitacoes_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoUsuario_Codigo_Z" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Codigo_Z"   )]
      public int gxTpr_Funcaousuario_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicitacoesItens_descricao_N" )]
      [  XmlElement( ElementName = "SolicitacoesItens_descricao_N"   )]
      public short gxTpr_Solicitacoesitens_descricao_N
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicitacoesItens_Arquivo_N" )]
      [  XmlElement( ElementName = "SolicitacoesItens_Arquivo_N"   )]
      public short gxTpr_Solicitacoesitens_arquivo_N
      {
         get {
            return gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N ;
         }

         set {
            gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N_SetNull( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao = "";
         gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo = "";
         gxTv_SdtSolicitacoesItens_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "solicitacoesitens", "GeneXus.Programs.solicitacoesitens_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtSolicitacoesItens_Initialized ;
      private short gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao_N ;
      private short gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo ;
      private int gxTv_SdtSolicitacoesItens_Solicitacoes_codigo ;
      private int gxTv_SdtSolicitacoesItens_Funcaousuario_codigo ;
      private int gxTv_SdtSolicitacoesItens_Solicitacoesitens_codigo_Z ;
      private int gxTv_SdtSolicitacoesItens_Solicitacoes_codigo_Z ;
      private int gxTv_SdtSolicitacoesItens_Funcaousuario_codigo_Z ;
      private String gxTv_SdtSolicitacoesItens_Mode ;
      private String sTagName ;
      private String gxTv_SdtSolicitacoesItens_Solicitacoesitens_descricao ;
      private String gxTv_SdtSolicitacoesItens_Solicitacoesitens_arquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"SolicitacoesItens", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSolicitacoesItens_RESTInterface : GxGenericCollectionItem<SdtSolicitacoesItens>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacoesItens_RESTInterface( ) : base()
      {
      }

      public SdtSolicitacoesItens_RESTInterface( SdtSolicitacoesItens psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "SolicitacoesItens_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Solicitacoesitens_codigo
      {
         get {
            return sdt.gxTpr_Solicitacoesitens_codigo ;
         }

         set {
            sdt.gxTpr_Solicitacoesitens_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Solicitacoes_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Solicitacoes_codigo
      {
         get {
            return sdt.gxTpr_Solicitacoes_codigo ;
         }

         set {
            sdt.gxTpr_Solicitacoes_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SolicitacoesItens_descricao" , Order = 2 )]
      public String gxTpr_Solicitacoesitens_descricao
      {
         get {
            return sdt.gxTpr_Solicitacoesitens_descricao ;
         }

         set {
            sdt.gxTpr_Solicitacoesitens_descricao = (String)(value);
         }

      }

      [DataMember( Name = "SolicitacoesItens_Arquivo" , Order = 3 )]
      [GxUpload()]
      public String gxTpr_Solicitacoesitens_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Solicitacoesitens_arquivo) ;
         }

         set {
            sdt.gxTpr_Solicitacoesitens_arquivo = value;
         }

      }

      [DataMember( Name = "FuncaoUsuario_Codigo" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaousuario_codigo
      {
         get {
            return sdt.gxTpr_Funcaousuario_codigo ;
         }

         set {
            sdt.gxTpr_Funcaousuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSolicitacoesItens sdt
      {
         get {
            return (SdtSolicitacoesItens)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSolicitacoesItens() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 12 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
