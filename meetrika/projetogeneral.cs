/*
               File: ProjetoGeneral
        Description: Projeto General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:36.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projetogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public projetogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public projetogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Projeto_Codigo )
      {
         this.A648Projeto_Codigo = aP0_Projeto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbProjeto_TipoContagem = new GXCombobox();
         cmbProjeto_TecnicaContagem = new GXCombobox();
         cmbProjeto_Status = new GXCombobox();
         chkProjeto_Incremental = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A648Projeto_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PADH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ProjetoGeneral";
               context.Gx_err = 0;
               /* Using cursor H00DH3 */
               pr_default.execute(0, new Object[] {A648Projeto_Codigo});
               if ( (pr_default.getStatus(0) != 101) )
               {
                  A657Projeto_Esforco = H00DH3_A657Projeto_Esforco[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
                  A656Projeto_Prazo = H00DH3_A656Projeto_Prazo[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
                  A655Projeto_Custo = H00DH3_A655Projeto_Custo[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
               }
               else
               {
                  A657Projeto_Esforco = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
                  A656Projeto_Prazo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
                  A655Projeto_Custo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
               }
               pr_default.close(0);
               WSDH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Projeto General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117223689");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projetogeneral.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_TIPOPROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_TIPOCONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_TECNICACONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( sPrefix, A1232Projeto_Incremental));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormDH2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("projetogeneral.js", "?20203117223692");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ProjetoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto General" ;
      }

      protected void WBDH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "projetogeneral.aspx");
            }
            wb_table1_2_DH2( true) ;
         }
         else
         {
            wb_table1_2_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Projeto General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPDH0( ) ;
            }
         }
      }

      protected void WSDH2( )
      {
         STARTDH2( ) ;
         EVTDH2( ) ;
      }

      protected void EVTDH2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11DH2 */
                                    E11DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12DH2 */
                                    E12DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DH2 */
                                    E13DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DH2 */
                                    E14DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15DH2 */
                                    E15DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormDH2( ) ;
            }
         }
      }

      protected void PADH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbProjeto_TipoContagem.Name = "PROJETO_TIPOCONTAGEM";
            cmbProjeto_TipoContagem.WebTags = "";
            cmbProjeto_TipoContagem.addItem("", "(Nenhum)", 0);
            cmbProjeto_TipoContagem.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbProjeto_TipoContagem.addItem("M", "Projeto de Melhor�a", 0);
            cmbProjeto_TipoContagem.addItem("C", "Projeto de Contagem", 0);
            cmbProjeto_TipoContagem.addItem("A", "Aplica��o", 0);
            if ( cmbProjeto_TipoContagem.ItemCount > 0 )
            {
               A651Projeto_TipoContagem = cmbProjeto_TipoContagem.getValidValue(A651Projeto_TipoContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A651Projeto_TipoContagem", A651Projeto_TipoContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOCONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
            }
            cmbProjeto_TecnicaContagem.Name = "PROJETO_TECNICACONTAGEM";
            cmbProjeto_TecnicaContagem.WebTags = "";
            cmbProjeto_TecnicaContagem.addItem("", "Nenhuma", 0);
            cmbProjeto_TecnicaContagem.addItem("1", "Indicativa", 0);
            cmbProjeto_TecnicaContagem.addItem("2", "Estimada", 0);
            cmbProjeto_TecnicaContagem.addItem("3", "Detalhada", 0);
            if ( cmbProjeto_TecnicaContagem.ItemCount > 0 )
            {
               A652Projeto_TecnicaContagem = cmbProjeto_TecnicaContagem.getValidValue(A652Projeto_TecnicaContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A652Projeto_TecnicaContagem", A652Projeto_TecnicaContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TECNICACONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""))));
            }
            cmbProjeto_Status.Name = "PROJETO_STATUS";
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            }
            chkProjeto_Incremental.Name = "PROJETO_INCREMENTAL";
            chkProjeto_Incremental.WebTags = "";
            chkProjeto_Incremental.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
            chkProjeto_Incremental.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbProjeto_TipoContagem.ItemCount > 0 )
         {
            A651Projeto_TipoContagem = cmbProjeto_TipoContagem.getValidValue(A651Projeto_TipoContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A651Projeto_TipoContagem", A651Projeto_TipoContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOCONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
         }
         if ( cmbProjeto_TecnicaContagem.ItemCount > 0 )
         {
            A652Projeto_TecnicaContagem = cmbProjeto_TecnicaContagem.getValidValue(A652Projeto_TecnicaContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A652Projeto_TecnicaContagem", A652Projeto_TecnicaContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TECNICACONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""))));
         }
         if ( cmbProjeto_Status.ItemCount > 0 )
         {
            A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ProjetoGeneral";
         context.Gx_err = 0;
      }

      protected void RFDH2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00DH5 */
            pr_default.execute(1, new Object[] {A648Projeto_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1232Projeto_Incremental = H00DH5_A1232Projeto_Incremental[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( sPrefix, A1232Projeto_Incremental));
               n1232Projeto_Incremental = H00DH5_n1232Projeto_Incremental[0];
               A658Projeto_Status = H00DH5_A658Projeto_Status[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
               A653Projeto_Escopo = H00DH5_A653Projeto_Escopo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A653Projeto_Escopo", A653Projeto_Escopo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
               A652Projeto_TecnicaContagem = H00DH5_A652Projeto_TecnicaContagem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A652Projeto_TecnicaContagem", A652Projeto_TecnicaContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TECNICACONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""))));
               A651Projeto_TipoContagem = H00DH5_A651Projeto_TipoContagem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A651Projeto_TipoContagem", A651Projeto_TipoContagem);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOCONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
               A983Projeto_TipoProjetoCod = H00DH5_A983Projeto_TipoProjetoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOPROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9")));
               n983Projeto_TipoProjetoCod = H00DH5_n983Projeto_TipoProjetoCod[0];
               A650Projeto_Sigla = H00DH5_A650Projeto_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A650Projeto_Sigla", A650Projeto_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
               A649Projeto_Nome = H00DH5_A649Projeto_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A649Projeto_Nome", A649Projeto_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
               A657Projeto_Esforco = H00DH5_A657Projeto_Esforco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
               A656Projeto_Prazo = H00DH5_A656Projeto_Prazo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
               A655Projeto_Custo = H00DH5_A655Projeto_Custo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
               A657Projeto_Esforco = H00DH5_A657Projeto_Esforco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
               A656Projeto_Prazo = H00DH5_A656Projeto_Prazo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
               A655Projeto_Custo = H00DH5_A655Projeto_Custo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
               /* Execute user event: E12DH2 */
               E12DH2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBDH0( ) ;
         }
      }

      protected void STRUPDH0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ProjetoGeneral";
         context.Gx_err = 0;
         /* Using cursor H00DH7 */
         pr_default.execute(2, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A657Projeto_Esforco = H00DH7_A657Projeto_Esforco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            A656Projeto_Prazo = H00DH7_A656Projeto_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A655Projeto_Custo = H00DH7_A655Projeto_Custo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
         }
         else
         {
            A657Projeto_Esforco = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            A656Projeto_Prazo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A655Projeto_Custo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
         }
         pr_default.close(2);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DH2 */
         E11DH2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A649Projeto_Nome", A649Projeto_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
            A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A650Projeto_Sigla", A650Projeto_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
            A983Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtProjeto_TipoProjetoCod_Internalname), ",", "."));
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOPROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9")));
            cmbProjeto_TipoContagem.CurrentValue = cgiGet( cmbProjeto_TipoContagem_Internalname);
            A651Projeto_TipoContagem = cgiGet( cmbProjeto_TipoContagem_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A651Projeto_TipoContagem", A651Projeto_TipoContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOCONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""))));
            cmbProjeto_TecnicaContagem.CurrentValue = cgiGet( cmbProjeto_TecnicaContagem_Internalname);
            A652Projeto_TecnicaContagem = cgiGet( cmbProjeto_TecnicaContagem_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A652Projeto_TecnicaContagem", A652Projeto_TecnicaContagem);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TECNICACONTAGEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""))));
            A653Projeto_Escopo = cgiGet( edtProjeto_Escopo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A653Projeto_Escopo", A653Projeto_Escopo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
            A655Projeto_Custo = context.localUtil.CToN( cgiGet( edtProjeto_Custo_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
            A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Prazo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A657Projeto_Esforco = (short)(context.localUtil.CToN( cgiGet( edtProjeto_Esforco_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
            A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            A1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( chkProjeto_Incremental_Internalname));
            n1232Projeto_Incremental = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( sPrefix, A1232Projeto_Incremental));
            /* Read saved values. */
            wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DH2 */
         E11DH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11DH2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12DH2( )
      {
         /* Load Routine */
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void E13DH2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A648Projeto_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14DH2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A648Projeto_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15DH2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwprojeto.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Projeto";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Projeto_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Projeto_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DH2( true) ;
         }
         else
         {
            wb_table2_8_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_66_DH2( true) ;
         }
         else
         {
            wb_table3_66_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_66_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DH2e( true) ;
         }
         else
         {
            wb_table1_2_DH2e( false) ;
         }
      }

      protected void wb_table3_66_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_66_DH2e( true) ;
         }
         else
         {
            wb_table3_66_DH2e( false) ;
         }
      }

      protected void wb_table2_8_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_nome_Internalname, "Projeto", "", "", lblTextblockprojeto_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Nome_Internalname, StringUtil.RTrim( A649Projeto_Nome), StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "Nome", "left", true, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_sigla_Internalname, "Sigla", "", "", lblTextblockprojeto_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Sigla_Internalname, StringUtil.RTrim( A650Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_tipoprojetocod_Internalname, "Tipo de Projeto", "", "", lblTextblockprojeto_tipoprojetocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_TipoProjetoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A983Projeto_TipoProjetoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_TipoProjetoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_tipocontagem_Internalname, "Tipo de Contagem", "", "", lblTextblockprojeto_tipocontagem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjeto_TipoContagem, cmbProjeto_TipoContagem_Internalname, StringUtil.RTrim( A651Projeto_TipoContagem), 1, cmbProjeto_TipoContagem_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ProjetoGeneral.htm");
            cmbProjeto_TipoContagem.CurrentValue = StringUtil.RTrim( A651Projeto_TipoContagem);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbProjeto_TipoContagem_Internalname, "Values", (String)(cmbProjeto_TipoContagem.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_tecnicacontagem_Internalname, "T�cnica de Contagem", "", "", lblTextblockprojeto_tecnicacontagem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjeto_TecnicaContagem, cmbProjeto_TecnicaContagem_Internalname, StringUtil.RTrim( A652Projeto_TecnicaContagem), 1, cmbProjeto_TecnicaContagem_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ProjetoGeneral.htm");
            cmbProjeto_TecnicaContagem.CurrentValue = StringUtil.RTrim( A652Projeto_TecnicaContagem);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbProjeto_TecnicaContagem_Internalname, "Values", (String)(cmbProjeto_TecnicaContagem.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_escopo_Internalname, "Escopo", "", "", lblTextblockprojeto_escopo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjeto_Escopo_Internalname, A653Projeto_Escopo, "", "", 0, 1, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_custo_Internalname, "Custo", "", "", lblTextblockprojeto_custo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Custo_Internalname, StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 18, 2, ",", "")), context.localUtil.Format( A655Projeto_Custo, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Custo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_prazo_Internalname, "Prazo", "", "", lblTextblockprojeto_prazo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A656Projeto_Prazo), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Prazo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_esforco_Internalname, "Esfor�o", "", "", lblTextblockprojeto_esforco_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A657Projeto_Esforco), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Esforco_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_status_Internalname, "Status", "", "", lblTextblockprojeto_status_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjeto_Status, cmbProjeto_Status_Internalname, StringUtil.RTrim( A658Projeto_Status), 1, cmbProjeto_Status_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ProjetoGeneral.htm");
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_incremental_Internalname, "Incremental", "", "", lblTextblockprojeto_incremental_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkProjeto_Incremental_Internalname, StringUtil.BoolToStr( A1232Projeto_Incremental), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DH2e( true) ;
         }
         else
         {
            wb_table2_8_DH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A648Projeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADH2( ) ;
         WSDH2( ) ;
         WEDH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA648Projeto_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PADH2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "projetogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PADH2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A648Projeto_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A648Projeto_Codigo != wcpOA648Projeto_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA648Projeto_Codigo = A648Projeto_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA648Projeto_Codigo = cgiGet( sPrefix+"A648Projeto_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA648Projeto_Codigo) > 0 )
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA648Projeto_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         else
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A648Projeto_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PADH2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSDH2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSDH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA648Projeto_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_CTRL", StringUtil.RTrim( sCtrlA648Projeto_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEDH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117223774");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("projetogeneral.js", "?20203117223775");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockprojeto_nome_Internalname = sPrefix+"TEXTBLOCKPROJETO_NOME";
         edtProjeto_Nome_Internalname = sPrefix+"PROJETO_NOME";
         lblTextblockprojeto_sigla_Internalname = sPrefix+"TEXTBLOCKPROJETO_SIGLA";
         edtProjeto_Sigla_Internalname = sPrefix+"PROJETO_SIGLA";
         lblTextblockprojeto_tipoprojetocod_Internalname = sPrefix+"TEXTBLOCKPROJETO_TIPOPROJETOCOD";
         edtProjeto_TipoProjetoCod_Internalname = sPrefix+"PROJETO_TIPOPROJETOCOD";
         lblTextblockprojeto_tipocontagem_Internalname = sPrefix+"TEXTBLOCKPROJETO_TIPOCONTAGEM";
         cmbProjeto_TipoContagem_Internalname = sPrefix+"PROJETO_TIPOCONTAGEM";
         lblTextblockprojeto_tecnicacontagem_Internalname = sPrefix+"TEXTBLOCKPROJETO_TECNICACONTAGEM";
         cmbProjeto_TecnicaContagem_Internalname = sPrefix+"PROJETO_TECNICACONTAGEM";
         lblTextblockprojeto_escopo_Internalname = sPrefix+"TEXTBLOCKPROJETO_ESCOPO";
         edtProjeto_Escopo_Internalname = sPrefix+"PROJETO_ESCOPO";
         lblTextblockprojeto_custo_Internalname = sPrefix+"TEXTBLOCKPROJETO_CUSTO";
         edtProjeto_Custo_Internalname = sPrefix+"PROJETO_CUSTO";
         lblTextblockprojeto_prazo_Internalname = sPrefix+"TEXTBLOCKPROJETO_PRAZO";
         edtProjeto_Prazo_Internalname = sPrefix+"PROJETO_PRAZO";
         lblTextblockprojeto_esforco_Internalname = sPrefix+"TEXTBLOCKPROJETO_ESFORCO";
         edtProjeto_Esforco_Internalname = sPrefix+"PROJETO_ESFORCO";
         lblTextblockprojeto_status_Internalname = sPrefix+"TEXTBLOCKPROJETO_STATUS";
         cmbProjeto_Status_Internalname = sPrefix+"PROJETO_STATUS";
         lblTextblockprojeto_incremental_Internalname = sPrefix+"TEXTBLOCKPROJETO_INCREMENTAL";
         chkProjeto_Incremental_Internalname = sPrefix+"PROJETO_INCREMENTAL";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbProjeto_Status_Jsonclick = "";
         edtProjeto_Esforco_Jsonclick = "";
         edtProjeto_Prazo_Jsonclick = "";
         edtProjeto_Custo_Jsonclick = "";
         cmbProjeto_TecnicaContagem_Jsonclick = "";
         cmbProjeto_TipoContagem_Jsonclick = "";
         edtProjeto_TipoProjetoCod_Jsonclick = "";
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_Nome_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         chkProjeto_Incremental.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13DH2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14DH2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15DH2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         scmdbuf = "";
         H00DH3_A657Projeto_Esforco = new short[1] ;
         H00DH3_A656Projeto_Prazo = new short[1] ;
         H00DH3_A655Projeto_Custo = new decimal[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A649Projeto_Nome = "";
         A650Projeto_Sigla = "";
         A651Projeto_TipoContagem = "";
         A652Projeto_TecnicaContagem = "";
         A653Projeto_Escopo = "";
         A658Projeto_Status = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00DH5_A1232Projeto_Incremental = new bool[] {false} ;
         H00DH5_n1232Projeto_Incremental = new bool[] {false} ;
         H00DH5_A658Projeto_Status = new String[] {""} ;
         H00DH5_A653Projeto_Escopo = new String[] {""} ;
         H00DH5_A652Projeto_TecnicaContagem = new String[] {""} ;
         H00DH5_A651Projeto_TipoContagem = new String[] {""} ;
         H00DH5_A983Projeto_TipoProjetoCod = new int[1] ;
         H00DH5_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         H00DH5_A650Projeto_Sigla = new String[] {""} ;
         H00DH5_A649Projeto_Nome = new String[] {""} ;
         H00DH5_A648Projeto_Codigo = new int[1] ;
         H00DH5_A657Projeto_Esforco = new short[1] ;
         H00DH5_A656Projeto_Prazo = new short[1] ;
         H00DH5_A655Projeto_Custo = new decimal[1] ;
         H00DH7_A657Projeto_Esforco = new short[1] ;
         H00DH7_A656Projeto_Prazo = new short[1] ;
         H00DH7_A655Projeto_Custo = new decimal[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockprojeto_nome_Jsonclick = "";
         lblTextblockprojeto_sigla_Jsonclick = "";
         lblTextblockprojeto_tipoprojetocod_Jsonclick = "";
         lblTextblockprojeto_tipocontagem_Jsonclick = "";
         lblTextblockprojeto_tecnicacontagem_Jsonclick = "";
         lblTextblockprojeto_escopo_Jsonclick = "";
         lblTextblockprojeto_custo_Jsonclick = "";
         lblTextblockprojeto_prazo_Jsonclick = "";
         lblTextblockprojeto_esforco_Jsonclick = "";
         lblTextblockprojeto_status_Jsonclick = "";
         lblTextblockprojeto_incremental_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA648Projeto_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetogeneral__default(),
            new Object[][] {
                new Object[] {
               H00DH3_A657Projeto_Esforco, H00DH3_A656Projeto_Prazo, H00DH3_A655Projeto_Custo
               }
               , new Object[] {
               H00DH5_A1232Projeto_Incremental, H00DH5_n1232Projeto_Incremental, H00DH5_A658Projeto_Status, H00DH5_A653Projeto_Escopo, H00DH5_A652Projeto_TecnicaContagem, H00DH5_A651Projeto_TipoContagem, H00DH5_A983Projeto_TipoProjetoCod, H00DH5_n983Projeto_TipoProjetoCod, H00DH5_A650Projeto_Sigla, H00DH5_A649Projeto_Nome,
               H00DH5_A648Projeto_Codigo, H00DH5_A657Projeto_Esforco, H00DH5_A656Projeto_Prazo, H00DH5_A655Projeto_Custo
               }
               , new Object[] {
               H00DH7_A657Projeto_Esforco, H00DH7_A656Projeto_Prazo, H00DH7_A655Projeto_Custo
               }
            }
         );
         AV14Pgmname = "ProjetoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ProjetoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A657Projeto_Esforco ;
      private short A656Projeto_Prazo ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A648Projeto_Codigo ;
      private int wcpOA648Projeto_Codigo ;
      private int A983Projeto_TipoProjetoCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7Projeto_Codigo ;
      private int idxLst ;
      private decimal A655Projeto_Custo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A649Projeto_Nome ;
      private String A650Projeto_Sigla ;
      private String A651Projeto_TipoContagem ;
      private String A652Projeto_TecnicaContagem ;
      private String A658Projeto_Status ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkProjeto_Incremental_Internalname ;
      private String edtProjeto_Nome_Internalname ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_TipoProjetoCod_Internalname ;
      private String cmbProjeto_TipoContagem_Internalname ;
      private String cmbProjeto_TecnicaContagem_Internalname ;
      private String edtProjeto_Escopo_Internalname ;
      private String edtProjeto_Custo_Internalname ;
      private String edtProjeto_Prazo_Internalname ;
      private String edtProjeto_Esforco_Internalname ;
      private String cmbProjeto_Status_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockprojeto_nome_Internalname ;
      private String lblTextblockprojeto_nome_Jsonclick ;
      private String edtProjeto_Nome_Jsonclick ;
      private String lblTextblockprojeto_sigla_Internalname ;
      private String lblTextblockprojeto_sigla_Jsonclick ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String lblTextblockprojeto_tipoprojetocod_Internalname ;
      private String lblTextblockprojeto_tipoprojetocod_Jsonclick ;
      private String edtProjeto_TipoProjetoCod_Jsonclick ;
      private String lblTextblockprojeto_tipocontagem_Internalname ;
      private String lblTextblockprojeto_tipocontagem_Jsonclick ;
      private String cmbProjeto_TipoContagem_Jsonclick ;
      private String lblTextblockprojeto_tecnicacontagem_Internalname ;
      private String lblTextblockprojeto_tecnicacontagem_Jsonclick ;
      private String cmbProjeto_TecnicaContagem_Jsonclick ;
      private String lblTextblockprojeto_escopo_Internalname ;
      private String lblTextblockprojeto_escopo_Jsonclick ;
      private String lblTextblockprojeto_custo_Internalname ;
      private String lblTextblockprojeto_custo_Jsonclick ;
      private String edtProjeto_Custo_Jsonclick ;
      private String lblTextblockprojeto_prazo_Internalname ;
      private String lblTextblockprojeto_prazo_Jsonclick ;
      private String edtProjeto_Prazo_Jsonclick ;
      private String lblTextblockprojeto_esforco_Internalname ;
      private String lblTextblockprojeto_esforco_Jsonclick ;
      private String edtProjeto_Esforco_Jsonclick ;
      private String lblTextblockprojeto_status_Internalname ;
      private String lblTextblockprojeto_status_Jsonclick ;
      private String cmbProjeto_Status_Jsonclick ;
      private String lblTextblockprojeto_incremental_Internalname ;
      private String lblTextblockprojeto_incremental_Jsonclick ;
      private String sCtrlA648Projeto_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1232Projeto_Incremental ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1232Projeto_Incremental ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool returnInSub ;
      private String A653Projeto_Escopo ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbProjeto_TipoContagem ;
      private GXCombobox cmbProjeto_TecnicaContagem ;
      private GXCombobox cmbProjeto_Status ;
      private GXCheckbox chkProjeto_Incremental ;
      private IDataStoreProvider pr_default ;
      private short[] H00DH3_A657Projeto_Esforco ;
      private short[] H00DH3_A656Projeto_Prazo ;
      private decimal[] H00DH3_A655Projeto_Custo ;
      private bool[] H00DH5_A1232Projeto_Incremental ;
      private bool[] H00DH5_n1232Projeto_Incremental ;
      private String[] H00DH5_A658Projeto_Status ;
      private String[] H00DH5_A653Projeto_Escopo ;
      private String[] H00DH5_A652Projeto_TecnicaContagem ;
      private String[] H00DH5_A651Projeto_TipoContagem ;
      private int[] H00DH5_A983Projeto_TipoProjetoCod ;
      private bool[] H00DH5_n983Projeto_TipoProjetoCod ;
      private String[] H00DH5_A650Projeto_Sigla ;
      private String[] H00DH5_A649Projeto_Nome ;
      private int[] H00DH5_A648Projeto_Codigo ;
      private short[] H00DH5_A657Projeto_Esforco ;
      private short[] H00DH5_A656Projeto_Prazo ;
      private decimal[] H00DH5_A655Projeto_Custo ;
      private short[] H00DH7_A657Projeto_Esforco ;
      private short[] H00DH7_A656Projeto_Prazo ;
      private decimal[] H00DH7_A655Projeto_Custo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class projetogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DH3 ;
          prmH00DH3 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DH5 ;
          prmH00DH5 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DH7 ;
          prmH00DH7 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DH3", "SELECT COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo FROM (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Custo]) AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DH3,1,0,true,true )
             ,new CursorDef("H00DH5", "SELECT T1.[Projeto_Incremental], T1.[Projeto_Status], T1.[Projeto_Escopo], T1.[Projeto_TecnicaContagem], T1.[Projeto_TipoContagem], T1.[Projeto_TipoProjetoCod], T1.[Projeto_Sigla], T1.[Projeto_Nome], T1.[Projeto_Codigo], COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo FROM ([Projeto] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Custo]) AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = T1.[Projeto_Codigo]) WHERE T1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY T1.[Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DH5,1,0,true,true )
             ,new CursorDef("H00DH7", "SELECT COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo FROM (SELECT SUM([Sistema_Esforco]) AS Projeto_Esforco, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Custo]) AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DH7,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((short[]) buf[11])[0] = rslt.getShort(10) ;
                ((short[]) buf[12])[0] = rslt.getShort(11) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(12) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
