/*
               File: Geral_Grupo_Cargo
        Description: Grupo de Cargos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:55.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_grupo_cargo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7GrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRUPOCARGO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GrupoCargo_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkGrupoCargo_Ativo.Name = "GRUPOCARGO_ATIVO";
         chkGrupoCargo_Ativo.WebTags = "";
         chkGrupoCargo_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkGrupoCargo_Ativo_Internalname, "TitleCaption", chkGrupoCargo_Ativo.Caption);
         chkGrupoCargo_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Grupo de Cargos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGrupoCargo_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public geral_grupo_cargo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public geral_grupo_cargo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_GrupoCargo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7GrupoCargo_Codigo = aP1_GrupoCargo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkGrupoCargo_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2481( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2481e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGrupoCargo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ",", "")), ((edtGrupoCargo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGrupoCargo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtGrupoCargo_Codigo_Visible, edtGrupoCargo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_Grupo_Cargo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2481( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2481( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2481e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_2481( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_2481e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2481e( true) ;
         }
         else
         {
            wb_table1_2_2481e( false) ;
         }
      }

      protected void wb_table3_26_2481( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Grupo_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Grupo_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Grupo_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_2481e( true) ;
         }
         else
         {
            wb_table3_26_2481e( false) ;
         }
      }

      protected void wb_table2_5_2481( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2481( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2481e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2481e( true) ;
         }
         else
         {
            wb_table2_5_2481e( false) ;
         }
      }

      protected void wb_table4_13_2481( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgrupocargo_nome_Internalname, "Nome", "", "", lblTextblockgrupocargo_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGrupoCargo_Nome_Internalname, A616GrupoCargo_Nome, StringUtil.RTrim( context.localUtil.Format( A616GrupoCargo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGrupoCargo_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGrupoCargo_Nome_Enabled, 0, "text", "", 380, "px", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgrupocargo_ativo_Internalname, "Ativo?", "", "", lblTextblockgrupocargo_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockgrupocargo_ativo_Visible, 1, 0, "HLP_Geral_Grupo_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkGrupoCargo_Ativo_Internalname, StringUtil.BoolToStr( A626GrupoCargo_Ativo), "", "", chkGrupoCargo_Ativo.Visible, chkGrupoCargo_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2481e( true) ;
         }
         else
         {
            wb_table4_13_2481e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11242 */
         E11242 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A616GrupoCargo_Nome = StringUtil.Upper( cgiGet( edtGrupoCargo_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A616GrupoCargo_Nome", A616GrupoCargo_Nome);
               A626GrupoCargo_Ativo = StringUtil.StrToBool( cgiGet( chkGrupoCargo_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A626GrupoCargo_Ativo", A626GrupoCargo_Ativo);
               A615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoCargo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
               /* Read saved values. */
               Z615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z615GrupoCargo_Codigo"), ",", "."));
               Z616GrupoCargo_Nome = cgiGet( "Z616GrupoCargo_Nome");
               Z626GrupoCargo_Ativo = StringUtil.StrToBool( cgiGet( "Z626GrupoCargo_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vGRUPOCARGO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Geral_Grupo_Cargo";
               A615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoCargo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A615GrupoCargo_Codigo != Z615GrupoCargo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("geral_grupo_cargo:[SecurityCheckFailed value for]"+"GrupoCargo_Codigo:"+context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("geral_grupo_cargo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A615GrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode81 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode81;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound81 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_240( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "GRUPOCARGO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtGrupoCargo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11242 */
                           E11242 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12242 */
                           E12242 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12242 */
            E12242 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2481( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2481( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_240( )
      {
         BeforeValidate2481( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2481( ) ;
            }
            else
            {
               CheckExtendedTable2481( ) ;
               CloseExtendedTableCursors2481( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption240( )
      {
      }

      protected void E11242( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtGrupoCargo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoCargo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoCargo_Codigo_Visible), 5, 0)));
      }

      protected void E12242( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwgeral_grupo_cargo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2481( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z616GrupoCargo_Nome = T00243_A616GrupoCargo_Nome[0];
               Z626GrupoCargo_Ativo = T00243_A626GrupoCargo_Ativo[0];
            }
            else
            {
               Z616GrupoCargo_Nome = A616GrupoCargo_Nome;
               Z626GrupoCargo_Ativo = A626GrupoCargo_Ativo;
            }
         }
         if ( GX_JID == -6 )
         {
            Z615GrupoCargo_Codigo = A615GrupoCargo_Codigo;
            Z616GrupoCargo_Nome = A616GrupoCargo_Nome;
            Z626GrupoCargo_Ativo = A626GrupoCargo_Ativo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtGrupoCargo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoCargo_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtGrupoCargo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoCargo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7GrupoCargo_Codigo) )
         {
            A615GrupoCargo_Codigo = AV7GrupoCargo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkGrupoCargo_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkGrupoCargo_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkGrupoCargo_Ativo.Visible), 5, 0)));
         lblTextblockgrupocargo_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockgrupocargo_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockgrupocargo_ativo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A626GrupoCargo_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A626GrupoCargo_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A626GrupoCargo_Ativo", A626GrupoCargo_Ativo);
         }
      }

      protected void Load2481( )
      {
         /* Using cursor T00244 */
         pr_default.execute(2, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound81 = 1;
            A616GrupoCargo_Nome = T00244_A616GrupoCargo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A616GrupoCargo_Nome", A616GrupoCargo_Nome);
            A626GrupoCargo_Ativo = T00244_A626GrupoCargo_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A626GrupoCargo_Ativo", A626GrupoCargo_Ativo);
            ZM2481( -6) ;
         }
         pr_default.close(2);
         OnLoadActions2481( ) ;
      }

      protected void OnLoadActions2481( )
      {
      }

      protected void CheckExtendedTable2481( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors2481( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2481( )
      {
         /* Using cursor T00245 */
         pr_default.execute(3, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound81 = 1;
         }
         else
         {
            RcdFound81 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00243 */
         pr_default.execute(1, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2481( 6) ;
            RcdFound81 = 1;
            A615GrupoCargo_Codigo = T00243_A615GrupoCargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
            A616GrupoCargo_Nome = T00243_A616GrupoCargo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A616GrupoCargo_Nome", A616GrupoCargo_Nome);
            A626GrupoCargo_Ativo = T00243_A626GrupoCargo_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A626GrupoCargo_Ativo", A626GrupoCargo_Ativo);
            Z615GrupoCargo_Codigo = A615GrupoCargo_Codigo;
            sMode81 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2481( ) ;
            if ( AnyError == 1 )
            {
               RcdFound81 = 0;
               InitializeNonKey2481( ) ;
            }
            Gx_mode = sMode81;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound81 = 0;
            InitializeNonKey2481( ) ;
            sMode81 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode81;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2481( ) ;
         if ( RcdFound81 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound81 = 0;
         /* Using cursor T00246 */
         pr_default.execute(4, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T00246_A615GrupoCargo_Codigo[0] < A615GrupoCargo_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T00246_A615GrupoCargo_Codigo[0] > A615GrupoCargo_Codigo ) ) )
            {
               A615GrupoCargo_Codigo = T00246_A615GrupoCargo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
               RcdFound81 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound81 = 0;
         /* Using cursor T00247 */
         pr_default.execute(5, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T00247_A615GrupoCargo_Codigo[0] > A615GrupoCargo_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T00247_A615GrupoCargo_Codigo[0] < A615GrupoCargo_Codigo ) ) )
            {
               A615GrupoCargo_Codigo = T00247_A615GrupoCargo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
               RcdFound81 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2481( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGrupoCargo_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2481( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound81 == 1 )
            {
               if ( A615GrupoCargo_Codigo != Z615GrupoCargo_Codigo )
               {
                  A615GrupoCargo_Codigo = Z615GrupoCargo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "GRUPOCARGO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGrupoCargo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGrupoCargo_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2481( ) ;
                  GX_FocusControl = edtGrupoCargo_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A615GrupoCargo_Codigo != Z615GrupoCargo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtGrupoCargo_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2481( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "GRUPOCARGO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtGrupoCargo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtGrupoCargo_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2481( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A615GrupoCargo_Codigo != Z615GrupoCargo_Codigo )
         {
            A615GrupoCargo_Codigo = Z615GrupoCargo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "GRUPOCARGO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGrupoCargo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGrupoCargo_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2481( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00242 */
            pr_default.execute(0, new Object[] {A615GrupoCargo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Grupo_Cargo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z616GrupoCargo_Nome, T00242_A616GrupoCargo_Nome[0]) != 0 ) || ( Z626GrupoCargo_Ativo != T00242_A626GrupoCargo_Ativo[0] ) )
            {
               if ( StringUtil.StrCmp(Z616GrupoCargo_Nome, T00242_A616GrupoCargo_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("geral_grupo_cargo:[seudo value changed for attri]"+"GrupoCargo_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z616GrupoCargo_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00242_A616GrupoCargo_Nome[0]);
               }
               if ( Z626GrupoCargo_Ativo != T00242_A626GrupoCargo_Ativo[0] )
               {
                  GXUtil.WriteLog("geral_grupo_cargo:[seudo value changed for attri]"+"GrupoCargo_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z626GrupoCargo_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00242_A626GrupoCargo_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Geral_Grupo_Cargo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2481( )
      {
         BeforeValidate2481( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2481( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2481( 0) ;
            CheckOptimisticConcurrency2481( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2481( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2481( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00248 */
                     pr_default.execute(6, new Object[] {A616GrupoCargo_Nome, A626GrupoCargo_Ativo});
                     A615GrupoCargo_Codigo = T00248_A615GrupoCargo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Grupo_Cargo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption240( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2481( ) ;
            }
            EndLevel2481( ) ;
         }
         CloseExtendedTableCursors2481( ) ;
      }

      protected void Update2481( )
      {
         BeforeValidate2481( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2481( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2481( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2481( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2481( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00249 */
                     pr_default.execute(7, new Object[] {A616GrupoCargo_Nome, A626GrupoCargo_Ativo, A615GrupoCargo_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Grupo_Cargo") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Grupo_Cargo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2481( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2481( ) ;
         }
         CloseExtendedTableCursors2481( ) ;
      }

      protected void DeferredUpdate2481( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2481( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2481( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2481( ) ;
            AfterConfirm2481( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2481( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002410 */
                  pr_default.execute(8, new Object[] {A615GrupoCargo_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Geral_Grupo_Cargo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode81 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2481( ) ;
         Gx_mode = sMode81;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2481( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T002411 */
            pr_default.execute(9, new Object[] {A615GrupoCargo_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Cargos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel2481( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2481( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Geral_Grupo_Cargo");
            if ( AnyError == 0 )
            {
               ConfirmValues240( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Geral_Grupo_Cargo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2481( )
      {
         /* Scan By routine */
         /* Using cursor T002412 */
         pr_default.execute(10);
         RcdFound81 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound81 = 1;
            A615GrupoCargo_Codigo = T002412_A615GrupoCargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2481( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound81 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound81 = 1;
            A615GrupoCargo_Codigo = T002412_A615GrupoCargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2481( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm2481( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2481( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2481( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2481( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2481( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2481( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2481( )
      {
         edtGrupoCargo_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoCargo_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoCargo_Nome_Enabled), 5, 0)));
         chkGrupoCargo_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkGrupoCargo_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkGrupoCargo_Ativo.Enabled), 5, 0)));
         edtGrupoCargo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoCargo_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues240( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117215676");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_grupo_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7GrupoCargo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z615GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z616GrupoCargo_Nome", Z616GrupoCargo_Nome);
         GxWebStd.gx_boolean_hidden_field( context, "Z626GrupoCargo_Ativo", Z626GrupoCargo_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vGRUPOCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRUPOCARGO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7GrupoCargo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Geral_Grupo_Cargo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("geral_grupo_cargo:[SendSecurityCheck value for]"+"GrupoCargo_Codigo:"+context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("geral_grupo_cargo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("geral_grupo_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7GrupoCargo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Geral_Grupo_Cargo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Grupo de Cargos" ;
      }

      protected void InitializeNonKey2481( )
      {
         A616GrupoCargo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A616GrupoCargo_Nome", A616GrupoCargo_Nome);
         A626GrupoCargo_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A626GrupoCargo_Ativo", A626GrupoCargo_Ativo);
         Z616GrupoCargo_Nome = "";
         Z626GrupoCargo_Ativo = false;
      }

      protected void InitAll2481( )
      {
         A615GrupoCargo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         InitializeNonKey2481( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A626GrupoCargo_Ativo = i626GrupoCargo_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A626GrupoCargo_Ativo", A626GrupoCargo_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117215695");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("geral_grupo_cargo.js", "?20203117215695");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockgrupocargo_nome_Internalname = "TEXTBLOCKGRUPOCARGO_NOME";
         edtGrupoCargo_Nome_Internalname = "GRUPOCARGO_NOME";
         lblTextblockgrupocargo_ativo_Internalname = "TEXTBLOCKGRUPOCARGO_ATIVO";
         chkGrupoCargo_Ativo_Internalname = "GRUPOCARGO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtGrupoCargo_Codigo_Internalname = "GRUPOCARGO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Grupo de Cargos";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Grupo de Cargos";
         chkGrupoCargo_Ativo.Enabled = 1;
         chkGrupoCargo_Ativo.Visible = 1;
         lblTextblockgrupocargo_ativo_Visible = 1;
         edtGrupoCargo_Nome_Jsonclick = "";
         edtGrupoCargo_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtGrupoCargo_Codigo_Jsonclick = "";
         edtGrupoCargo_Codigo_Enabled = 0;
         edtGrupoCargo_Codigo_Visible = 1;
         chkGrupoCargo_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12242',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z616GrupoCargo_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockgrupocargo_nome_Jsonclick = "";
         A616GrupoCargo_Nome = "";
         lblTextblockgrupocargo_ativo_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode81 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T00244_A615GrupoCargo_Codigo = new int[1] ;
         T00244_A616GrupoCargo_Nome = new String[] {""} ;
         T00244_A626GrupoCargo_Ativo = new bool[] {false} ;
         T00245_A615GrupoCargo_Codigo = new int[1] ;
         T00243_A615GrupoCargo_Codigo = new int[1] ;
         T00243_A616GrupoCargo_Nome = new String[] {""} ;
         T00243_A626GrupoCargo_Ativo = new bool[] {false} ;
         T00246_A615GrupoCargo_Codigo = new int[1] ;
         T00247_A615GrupoCargo_Codigo = new int[1] ;
         T00242_A615GrupoCargo_Codigo = new int[1] ;
         T00242_A616GrupoCargo_Nome = new String[] {""} ;
         T00242_A626GrupoCargo_Ativo = new bool[] {false} ;
         T00248_A615GrupoCargo_Codigo = new int[1] ;
         T002411_A617Cargo_Codigo = new int[1] ;
         T002412_A615GrupoCargo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_grupo_cargo__default(),
            new Object[][] {
                new Object[] {
               T00242_A615GrupoCargo_Codigo, T00242_A616GrupoCargo_Nome, T00242_A626GrupoCargo_Ativo
               }
               , new Object[] {
               T00243_A615GrupoCargo_Codigo, T00243_A616GrupoCargo_Nome, T00243_A626GrupoCargo_Ativo
               }
               , new Object[] {
               T00244_A615GrupoCargo_Codigo, T00244_A616GrupoCargo_Nome, T00244_A626GrupoCargo_Ativo
               }
               , new Object[] {
               T00245_A615GrupoCargo_Codigo
               }
               , new Object[] {
               T00246_A615GrupoCargo_Codigo
               }
               , new Object[] {
               T00247_A615GrupoCargo_Codigo
               }
               , new Object[] {
               T00248_A615GrupoCargo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002411_A617Cargo_Codigo
               }
               , new Object[] {
               T002412_A615GrupoCargo_Codigo
               }
            }
         );
         Z626GrupoCargo_Ativo = true;
         A626GrupoCargo_Ativo = true;
         i626GrupoCargo_Ativo = true;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound81 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7GrupoCargo_Codigo ;
      private int Z615GrupoCargo_Codigo ;
      private int AV7GrupoCargo_Codigo ;
      private int trnEnded ;
      private int A615GrupoCargo_Codigo ;
      private int edtGrupoCargo_Codigo_Enabled ;
      private int edtGrupoCargo_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtGrupoCargo_Nome_Enabled ;
      private int lblTextblockgrupocargo_ativo_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkGrupoCargo_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGrupoCargo_Nome_Internalname ;
      private String edtGrupoCargo_Codigo_Internalname ;
      private String edtGrupoCargo_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockgrupocargo_nome_Internalname ;
      private String lblTextblockgrupocargo_nome_Jsonclick ;
      private String edtGrupoCargo_Nome_Jsonclick ;
      private String lblTextblockgrupocargo_ativo_Internalname ;
      private String lblTextblockgrupocargo_ativo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode81 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z626GrupoCargo_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A626GrupoCargo_Ativo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i626GrupoCargo_Ativo ;
      private String Z616GrupoCargo_Nome ;
      private String A616GrupoCargo_Nome ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkGrupoCargo_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] T00244_A615GrupoCargo_Codigo ;
      private String[] T00244_A616GrupoCargo_Nome ;
      private bool[] T00244_A626GrupoCargo_Ativo ;
      private int[] T00245_A615GrupoCargo_Codigo ;
      private int[] T00243_A615GrupoCargo_Codigo ;
      private String[] T00243_A616GrupoCargo_Nome ;
      private bool[] T00243_A626GrupoCargo_Ativo ;
      private int[] T00246_A615GrupoCargo_Codigo ;
      private int[] T00247_A615GrupoCargo_Codigo ;
      private int[] T00242_A615GrupoCargo_Codigo ;
      private String[] T00242_A616GrupoCargo_Nome ;
      private bool[] T00242_A626GrupoCargo_Ativo ;
      private int[] T00248_A615GrupoCargo_Codigo ;
      private int[] T002411_A617Cargo_Codigo ;
      private int[] T002412_A615GrupoCargo_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class geral_grupo_cargo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00244 ;
          prmT00244 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00245 ;
          prmT00245 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00243 ;
          prmT00243 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00246 ;
          prmT00246 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00247 ;
          prmT00247 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00242 ;
          prmT00242 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00248 ;
          prmT00248 = new Object[] {
          new Object[] {"@GrupoCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@GrupoCargo_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT00249 ;
          prmT00249 = new Object[] {
          new Object[] {"@GrupoCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@GrupoCargo_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002410 ;
          prmT002410 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002411 ;
          prmT002411 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002412 ;
          prmT002412 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00242", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (UPDLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00242,1,0,true,false )
             ,new CursorDef("T00243", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome], [GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00243,1,0,true,false )
             ,new CursorDef("T00244", "SELECT TM1.[GrupoCargo_Codigo], TM1.[GrupoCargo_Nome], TM1.[GrupoCargo_Ativo] FROM [Geral_Grupo_Cargo] TM1 WITH (NOLOCK) WHERE TM1.[GrupoCargo_Codigo] = @GrupoCargo_Codigo ORDER BY TM1.[GrupoCargo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00244,100,0,true,false )
             ,new CursorDef("T00245", "SELECT [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00245,1,0,true,false )
             ,new CursorDef("T00246", "SELECT TOP 1 [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE ( [GrupoCargo_Codigo] > @GrupoCargo_Codigo) ORDER BY [GrupoCargo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00246,1,0,true,true )
             ,new CursorDef("T00247", "SELECT TOP 1 [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE ( [GrupoCargo_Codigo] < @GrupoCargo_Codigo) ORDER BY [GrupoCargo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00247,1,0,true,true )
             ,new CursorDef("T00248", "INSERT INTO [Geral_Grupo_Cargo]([GrupoCargo_Nome], [GrupoCargo_Ativo]) VALUES(@GrupoCargo_Nome, @GrupoCargo_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT00248)
             ,new CursorDef("T00249", "UPDATE [Geral_Grupo_Cargo] SET [GrupoCargo_Nome]=@GrupoCargo_Nome, [GrupoCargo_Ativo]=@GrupoCargo_Ativo  WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo", GxErrorMask.GX_NOMASK,prmT00249)
             ,new CursorDef("T002410", "DELETE FROM [Geral_Grupo_Cargo]  WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo", GxErrorMask.GX_NOMASK,prmT002410)
             ,new CursorDef("T002411", "SELECT TOP 1 [Cargo_Codigo] FROM [Geral_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002411,1,0,true,true )
             ,new CursorDef("T002412", "SELECT [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) ORDER BY [GrupoCargo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002412,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
