/*
               File: ContratoServicosArtefatos_BC
        Description: Artefato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:21.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosartefatos_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratoservicosartefatos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosartefatos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4E194( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4E194( ) ;
         standaloneModal( ) ;
         AddRow4E194( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E114E2 */
            E114E2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4E0( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4E194( ) ;
            }
            else
            {
               CheckExtendedTable4E194( ) ;
               if ( AnyError == 0 )
               {
                  ZM4E194( 2) ;
                  ZM4E194( 3) ;
               }
               CloseExtendedTableCursors4E194( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E124E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
      }

      protected void E114E2( )
      {
         /* After Trn Routine */
      }

      protected void ZM4E194( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1768ContratoServicosArtefato_Obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1751Artefatos_Descricao = A1751Artefatos_Descricao;
         }
         if ( GX_JID == -1 )
         {
            Z1768ContratoServicosArtefato_Obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            Z1751Artefatos_Descricao = A1751Artefatos_Descricao;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4E194( )
      {
         /* Using cursor BC004E6 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound194 = 1;
            A1751Artefatos_Descricao = BC004E6_A1751Artefatos_Descricao[0];
            A1768ContratoServicosArtefato_Obrigatorio = BC004E6_A1768ContratoServicosArtefato_Obrigatorio[0];
            n1768ContratoServicosArtefato_Obrigatorio = BC004E6_n1768ContratoServicosArtefato_Obrigatorio[0];
            ZM4E194( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4E194( ) ;
      }

      protected void OnLoadActions4E194( )
      {
      }

      protected void CheckExtendedTable4E194( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004E4 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004E5 */
         pr_default.execute(3, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
         }
         A1751Artefatos_Descricao = BC004E5_A1751Artefatos_Descricao[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4E194( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4E194( )
      {
         /* Using cursor BC004E7 */
         pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound194 = 1;
         }
         else
         {
            RcdFound194 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004E3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4E194( 1) ;
            RcdFound194 = 1;
            A1768ContratoServicosArtefato_Obrigatorio = BC004E3_A1768ContratoServicosArtefato_Obrigatorio[0];
            n1768ContratoServicosArtefato_Obrigatorio = BC004E3_n1768ContratoServicosArtefato_Obrigatorio[0];
            A160ContratoServicos_Codigo = BC004E3_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = BC004E3_A1749Artefatos_Codigo[0];
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            sMode194 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4E194( ) ;
            if ( AnyError == 1 )
            {
               RcdFound194 = 0;
               InitializeNonKey4E194( ) ;
            }
            Gx_mode = sMode194;
         }
         else
         {
            RcdFound194 = 0;
            InitializeNonKey4E194( ) ;
            sMode194 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode194;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4E194( ) ;
         if ( RcdFound194 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4E0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4E194( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004E2 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosArtefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1768ContratoServicosArtefato_Obrigatorio != BC004E2_A1768ContratoServicosArtefato_Obrigatorio[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosArtefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4E194( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4E194( 0) ;
            CheckOptimisticConcurrency4E194( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4E194( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4E194( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004E8 */
                     pr_default.execute(6, new Object[] {n1768ContratoServicosArtefato_Obrigatorio, A1768ContratoServicosArtefato_Obrigatorio, A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4E194( ) ;
            }
            EndLevel4E194( ) ;
         }
         CloseExtendedTableCursors4E194( ) ;
      }

      protected void Update4E194( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4E194( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4E194( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4E194( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004E9 */
                     pr_default.execute(7, new Object[] {n1768ContratoServicosArtefato_Obrigatorio, A1768ContratoServicosArtefato_Obrigatorio, A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosArtefatos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4E194( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4E194( ) ;
         }
         CloseExtendedTableCursors4E194( ) ;
      }

      protected void DeferredUpdate4E194( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4E194( ) ;
            AfterConfirm4E194( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4E194( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004E10 */
                  pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode194 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4E194( ) ;
         Gx_mode = sMode194;
      }

      protected void OnDeleteControls4E194( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004E11 */
            pr_default.execute(9, new Object[] {A1749Artefatos_Codigo});
            A1751Artefatos_Descricao = BC004E11_A1751Artefatos_Descricao[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel4E194( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4E194( )
      {
         /* Scan By routine */
         /* Using cursor BC004E12 */
         pr_default.execute(10, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         RcdFound194 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound194 = 1;
            A1751Artefatos_Descricao = BC004E12_A1751Artefatos_Descricao[0];
            A1768ContratoServicosArtefato_Obrigatorio = BC004E12_A1768ContratoServicosArtefato_Obrigatorio[0];
            n1768ContratoServicosArtefato_Obrigatorio = BC004E12_n1768ContratoServicosArtefato_Obrigatorio[0];
            A160ContratoServicos_Codigo = BC004E12_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = BC004E12_A1749Artefatos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4E194( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound194 = 0;
         ScanKeyLoad4E194( ) ;
      }

      protected void ScanKeyLoad4E194( )
      {
         sMode194 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound194 = 1;
            A1751Artefatos_Descricao = BC004E12_A1751Artefatos_Descricao[0];
            A1768ContratoServicosArtefato_Obrigatorio = BC004E12_A1768ContratoServicosArtefato_Obrigatorio[0];
            n1768ContratoServicosArtefato_Obrigatorio = BC004E12_n1768ContratoServicosArtefato_Obrigatorio[0];
            A160ContratoServicos_Codigo = BC004E12_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = BC004E12_A1749Artefatos_Codigo[0];
         }
         Gx_mode = sMode194;
      }

      protected void ScanKeyEnd4E194( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm4E194( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4E194( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4E194( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4E194( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4E194( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4E194( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4E194( )
      {
      }

      protected void AddRow4E194( )
      {
         VarsToRow194( bcContratoServicosArtefatos) ;
      }

      protected void ReadRow4E194( )
      {
         RowToVars194( bcContratoServicosArtefatos, 1) ;
      }

      protected void InitializeNonKey4E194( )
      {
         A1751Artefatos_Descricao = "";
         A1768ContratoServicosArtefato_Obrigatorio = false;
         n1768ContratoServicosArtefato_Obrigatorio = false;
         Z1768ContratoServicosArtefato_Obrigatorio = false;
      }

      protected void InitAll4E194( )
      {
         A160ContratoServicos_Codigo = 0;
         A1749Artefatos_Codigo = 0;
         InitializeNonKey4E194( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow194( SdtContratoServicosArtefatos obj194 )
      {
         obj194.gxTpr_Mode = Gx_mode;
         obj194.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
         obj194.gxTpr_Contratoservicosartefato_obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
         obj194.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj194.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
         obj194.gxTpr_Contratoservicos_codigo_Z = Z160ContratoServicos_Codigo;
         obj194.gxTpr_Artefatos_codigo_Z = Z1749Artefatos_Codigo;
         obj194.gxTpr_Artefatos_descricao_Z = Z1751Artefatos_Descricao;
         obj194.gxTpr_Contratoservicosartefato_obrigatorio_Z = Z1768ContratoServicosArtefato_Obrigatorio;
         obj194.gxTpr_Contratoservicosartefato_obrigatorio_N = (short)(Convert.ToInt16(n1768ContratoServicosArtefato_Obrigatorio));
         obj194.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow194( SdtContratoServicosArtefatos obj194 )
      {
         obj194.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj194.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
         return  ;
      }

      public void RowToVars194( SdtContratoServicosArtefatos obj194 ,
                                int forceLoad )
      {
         Gx_mode = obj194.gxTpr_Mode;
         A1751Artefatos_Descricao = obj194.gxTpr_Artefatos_descricao;
         A1768ContratoServicosArtefato_Obrigatorio = obj194.gxTpr_Contratoservicosartefato_obrigatorio;
         n1768ContratoServicosArtefato_Obrigatorio = false;
         A160ContratoServicos_Codigo = obj194.gxTpr_Contratoservicos_codigo;
         A1749Artefatos_Codigo = obj194.gxTpr_Artefatos_codigo;
         Z160ContratoServicos_Codigo = obj194.gxTpr_Contratoservicos_codigo_Z;
         Z1749Artefatos_Codigo = obj194.gxTpr_Artefatos_codigo_Z;
         Z1751Artefatos_Descricao = obj194.gxTpr_Artefatos_descricao_Z;
         Z1768ContratoServicosArtefato_Obrigatorio = obj194.gxTpr_Contratoservicosartefato_obrigatorio_Z;
         n1768ContratoServicosArtefato_Obrigatorio = (bool)(Convert.ToBoolean(obj194.gxTpr_Contratoservicosartefato_obrigatorio_N));
         Gx_mode = obj194.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A160ContratoServicos_Codigo = (int)getParm(obj,0);
         A1749Artefatos_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4E194( ) ;
         ScanKeyStart4E194( ) ;
         if ( RcdFound194 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004E13 */
            pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC004E11 */
            pr_default.execute(9, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
               AnyError = 1;
            }
            A1751Artefatos_Descricao = BC004E11_A1751Artefatos_Descricao[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
         }
         ZM4E194( -1) ;
         OnLoadActions4E194( ) ;
         AddRow4E194( ) ;
         ScanKeyEnd4E194( ) ;
         if ( RcdFound194 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars194( bcContratoServicosArtefatos, 0) ;
         ScanKeyStart4E194( ) ;
         if ( RcdFound194 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004E13 */
            pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC004E11 */
            pr_default.execute(9, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
               AnyError = 1;
            }
            A1751Artefatos_Descricao = BC004E11_A1751Artefatos_Descricao[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
         }
         ZM4E194( -1) ;
         OnLoadActions4E194( ) ;
         AddRow4E194( ) ;
         ScanKeyEnd4E194( ) ;
         if ( RcdFound194 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars194( bcContratoServicosArtefatos, 0) ;
         nKeyPressed = 1;
         GetKey4E194( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4E194( ) ;
         }
         else
         {
            if ( RcdFound194 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4E194( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4E194( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4E194( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow194( bcContratoServicosArtefatos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars194( bcContratoServicosArtefatos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4E194( ) ;
         if ( RcdFound194 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
            {
               A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
               A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
         context.RollbackDataStores( "ContratoServicosArtefatos_BC");
         VarsToRow194( bcContratoServicosArtefatos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoServicosArtefatos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoServicosArtefatos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoServicosArtefatos )
         {
            bcContratoServicosArtefatos = (SdtContratoServicosArtefatos)(sdt);
            if ( StringUtil.StrCmp(bcContratoServicosArtefatos.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosArtefatos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow194( bcContratoServicosArtefatos) ;
            }
            else
            {
               RowToVars194( bcContratoServicosArtefatos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoServicosArtefatos.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosArtefatos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars194( bcContratoServicosArtefatos, 1) ;
         return  ;
      }

      public SdtContratoServicosArtefatos ContratoServicosArtefatos_BC
      {
         get {
            return bcContratoServicosArtefatos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z1751Artefatos_Descricao = "";
         A1751Artefatos_Descricao = "";
         BC004E6_A1751Artefatos_Descricao = new String[] {""} ;
         BC004E6_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E6_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E6_A160ContratoServicos_Codigo = new int[1] ;
         BC004E6_A1749Artefatos_Codigo = new int[1] ;
         BC004E4_A160ContratoServicos_Codigo = new int[1] ;
         BC004E5_A1751Artefatos_Descricao = new String[] {""} ;
         BC004E7_A160ContratoServicos_Codigo = new int[1] ;
         BC004E7_A1749Artefatos_Codigo = new int[1] ;
         BC004E3_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E3_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E3_A160ContratoServicos_Codigo = new int[1] ;
         BC004E3_A1749Artefatos_Codigo = new int[1] ;
         sMode194 = "";
         BC004E2_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E2_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E2_A160ContratoServicos_Codigo = new int[1] ;
         BC004E2_A1749Artefatos_Codigo = new int[1] ;
         BC004E11_A1751Artefatos_Descricao = new String[] {""} ;
         BC004E12_A1751Artefatos_Descricao = new String[] {""} ;
         BC004E12_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E12_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         BC004E12_A160ContratoServicos_Codigo = new int[1] ;
         BC004E12_A1749Artefatos_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004E13_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosartefatos_bc__default(),
            new Object[][] {
                new Object[] {
               BC004E2_A1768ContratoServicosArtefato_Obrigatorio, BC004E2_n1768ContratoServicosArtefato_Obrigatorio, BC004E2_A160ContratoServicos_Codigo, BC004E2_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004E3_A1768ContratoServicosArtefato_Obrigatorio, BC004E3_n1768ContratoServicosArtefato_Obrigatorio, BC004E3_A160ContratoServicos_Codigo, BC004E3_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004E4_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC004E5_A1751Artefatos_Descricao
               }
               , new Object[] {
               BC004E6_A1751Artefatos_Descricao, BC004E6_A1768ContratoServicosArtefato_Obrigatorio, BC004E6_n1768ContratoServicosArtefato_Obrigatorio, BC004E6_A160ContratoServicos_Codigo, BC004E6_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004E7_A160ContratoServicos_Codigo, BC004E7_A1749Artefatos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004E11_A1751Artefatos_Descricao
               }
               , new Object[] {
               BC004E12_A1751Artefatos_Descricao, BC004E12_A1768ContratoServicosArtefato_Obrigatorio, BC004E12_n1768ContratoServicosArtefato_Obrigatorio, BC004E12_A160ContratoServicos_Codigo, BC004E12_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC004E13_A160ContratoServicos_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E124E2 */
         E124E2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound194 ;
      private int trnEnded ;
      private int Z160ContratoServicos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int Z1749Artefatos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode194 ;
      private bool Z1768ContratoServicosArtefato_Obrigatorio ;
      private bool A1768ContratoServicosArtefato_Obrigatorio ;
      private bool n1768ContratoServicosArtefato_Obrigatorio ;
      private String Z1751Artefatos_Descricao ;
      private String A1751Artefatos_Descricao ;
      private IGxSession AV11WebSession ;
      private SdtContratoServicosArtefatos bcContratoServicosArtefatos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC004E6_A1751Artefatos_Descricao ;
      private bool[] BC004E6_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] BC004E6_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] BC004E6_A160ContratoServicos_Codigo ;
      private int[] BC004E6_A1749Artefatos_Codigo ;
      private int[] BC004E4_A160ContratoServicos_Codigo ;
      private String[] BC004E5_A1751Artefatos_Descricao ;
      private int[] BC004E7_A160ContratoServicos_Codigo ;
      private int[] BC004E7_A1749Artefatos_Codigo ;
      private bool[] BC004E3_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] BC004E3_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] BC004E3_A160ContratoServicos_Codigo ;
      private int[] BC004E3_A1749Artefatos_Codigo ;
      private bool[] BC004E2_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] BC004E2_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] BC004E2_A160ContratoServicos_Codigo ;
      private int[] BC004E2_A1749Artefatos_Codigo ;
      private String[] BC004E11_A1751Artefatos_Descricao ;
      private String[] BC004E12_A1751Artefatos_Descricao ;
      private bool[] BC004E12_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] BC004E12_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] BC004E12_A160ContratoServicos_Codigo ;
      private int[] BC004E12_A1749Artefatos_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC004E13_A160ContratoServicos_Codigo ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratoservicosartefatos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004E6 ;
          prmBC004E6 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E4 ;
          prmBC004E4 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E5 ;
          prmBC004E5 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E7 ;
          prmBC004E7 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E3 ;
          prmBC004E3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E2 ;
          prmBC004E2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E8 ;
          prmBC004E8 = new Object[] {
          new Object[] {"@ContratoServicosArtefato_Obrigatorio",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E9 ;
          prmBC004E9 = new Object[] {
          new Object[] {"@ContratoServicosArtefato_Obrigatorio",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E10 ;
          prmBC004E10 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E12 ;
          prmBC004E12 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E13 ;
          prmBC004E13 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004E11 ;
          prmBC004E11 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004E2", "SELECT [ContratoServicosArtefato_Obrigatorio], [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E2,1,0,true,false )
             ,new CursorDef("BC004E3", "SELECT [ContratoServicosArtefato_Obrigatorio], [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E3,1,0,true,false )
             ,new CursorDef("BC004E4", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E4,1,0,true,false )
             ,new CursorDef("BC004E5", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E5,1,0,true,false )
             ,new CursorDef("BC004E6", "SELECT T2.[Artefatos_Descricao], TM1.[ContratoServicosArtefato_Obrigatorio], TM1.[ContratoServicos_Codigo], TM1.[Artefatos_Codigo] FROM ([ContratoServicosArtefatos] TM1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = TM1.[Artefatos_Codigo]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[Artefatos_Codigo] = @Artefatos_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E6,100,0,true,false )
             ,new CursorDef("BC004E7", "SELECT [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E7,1,0,true,false )
             ,new CursorDef("BC004E8", "INSERT INTO [ContratoServicosArtefatos]([ContratoServicosArtefato_Obrigatorio], [ContratoServicos_Codigo], [Artefatos_Codigo]) VALUES(@ContratoServicosArtefato_Obrigatorio, @ContratoServicos_Codigo, @Artefatos_Codigo)", GxErrorMask.GX_NOMASK,prmBC004E8)
             ,new CursorDef("BC004E9", "UPDATE [ContratoServicosArtefatos] SET [ContratoServicosArtefato_Obrigatorio]=@ContratoServicosArtefato_Obrigatorio  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmBC004E9)
             ,new CursorDef("BC004E10", "DELETE FROM [ContratoServicosArtefatos]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmBC004E10)
             ,new CursorDef("BC004E11", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E11,1,0,true,false )
             ,new CursorDef("BC004E12", "SELECT T2.[Artefatos_Descricao], TM1.[ContratoServicosArtefato_Obrigatorio], TM1.[ContratoServicos_Codigo], TM1.[Artefatos_Codigo] FROM ([ContratoServicosArtefatos] TM1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = TM1.[Artefatos_Codigo]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[Artefatos_Codigo] = @Artefatos_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E12,100,0,true,false )
             ,new CursorDef("BC004E13", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004E13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
