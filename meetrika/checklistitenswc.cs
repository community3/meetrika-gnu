/*
               File: CheckListItensWC
        Description: Check List Itens WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:53.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class checklistitenswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public checklistitenswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public checklistitenswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Check_Codigo )
      {
         this.AV7Check_Codigo = aP0_Check_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbCheckList_Obrigatorio = new GXCombobox();
         cmbCheckList_Impeditivo = new GXCombobox();
         cmbCheckList_SeNaoCumpre = new GXCombobox();
         cmbCheckList_NaoCnfQdo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Check_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV17TFCheckList_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFCheckList_Descricao", AV17TFCheckList_Descricao);
                  AV18TFCheckList_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFCheckList_Descricao_Sel", AV18TFCheckList_Descricao_Sel);
                  AV40TFCheckList_Impeditivo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCheckList_Impeditivo_Sel", StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0));
                  AV7Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0)));
                  AV19ddo_CheckList_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_CheckList_DescricaoTitleControlIdToReplace", AV19ddo_CheckList_DescricaoTitleControlIdToReplace);
                  AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace", AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace);
                  AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace", AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace);
                  AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace", AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace);
                  AV56Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV37TFCheckList_SeNaoCumpre_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV44TFCheckList_NaoCnfQdo_Sels);
                  A758CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A426NaoConformidade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
                  A1846CheckList_NaoCnfCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1846CheckList_NaoCnfCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1846CheckList_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1846CheckList_NaoCnfCod), 6, 0)));
                  A427NaoConformidade_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A427NaoConformidade_Nome", A427NaoConformidade_Nome);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, AV40TFCheckList_Impeditivo_Sel, AV7Check_Codigo, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, AV56Pgmname, AV37TFCheckList_SeNaoCumpre_Sels, AV44TFCheckList_NaoCnfQdo_Sels, A758CheckList_Codigo, A426NaoConformidade_Codigo, A1846CheckList_NaoCnfCod, A427NaoConformidade_Nome, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAO82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV56Pgmname = "CheckListItensWC";
               context.Gx_err = 0;
               edtavNaoconformidade_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNaoconformidade_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome_Enabled), 5, 0)));
               WSO82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Check List Itens WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117225361");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("checklistitenswc.aspx") + "?" + UrlEncode("" +AV7Check_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCHECKLIST_DESCRICAO", AV17TFCheckList_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCHECKLIST_DESCRICAO_SEL", AV18TFCheckList_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCHECKLIST_IMPEDITIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV23DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV23DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCHECKLIST_DESCRICAOTITLEFILTERDATA", AV16CheckList_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCHECKLIST_DESCRICAOTITLEFILTERDATA", AV16CheckList_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCHECKLIST_IMPEDITIVOTITLEFILTERDATA", AV39CheckList_ImpeditivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCHECKLIST_IMPEDITIVOTITLEFILTERDATA", AV39CheckList_ImpeditivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCHECKLIST_SENAOCUMPRETITLEFILTERDATA", AV35CheckList_SeNaoCumpreTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCHECKLIST_SENAOCUMPRETITLEFILTERDATA", AV35CheckList_SeNaoCumpreTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCHECKLIST_NAOCNFQDOTITLEFILTERDATA", AV42CheckList_NaoCnfQdoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCHECKLIST_NAOCNFQDOTITLEFILTERDATA", AV42CheckList_NaoCnfQdoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Check_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV56Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCHECKLIST_SENAOCUMPRE_SELS", AV37TFCheckList_SeNaoCumpre_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCHECKLIST_SENAOCUMPRE_SELS", AV37TFCheckList_SeNaoCumpre_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCHECKLIST_NAOCNFQDO_SELS", AV44TFCheckList_NaoCnfQdo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCHECKLIST_NAOCNFQDO_SELS", AV44TFCheckList_NaoCnfQdo_Sels);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"NAOCONFORMIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A426NaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1846CheckList_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NAOCONFORMIDADE_NOME", StringUtil.RTrim( A427NaoConformidade_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Caption", StringUtil.RTrim( Ddo_checklist_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_checklist_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Cls", StringUtil.RTrim( Ddo_checklist_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_checklist_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_checklist_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_checklist_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_checklist_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_checklist_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_checklist_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_checklist_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_checklist_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_checklist_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_checklist_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Caption", StringUtil.RTrim( Ddo_checklist_impeditivo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Tooltip", StringUtil.RTrim( Ddo_checklist_impeditivo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Cls", StringUtil.RTrim( Ddo_checklist_impeditivo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_impeditivo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_impeditivo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_impeditivo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_impeditivo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_impeditivo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_impeditivo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_impeditivo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_impeditivo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Datalisttype", StringUtil.RTrim( Ddo_checklist_impeditivo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_checklist_impeditivo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Sortasc", StringUtil.RTrim( Ddo_checklist_impeditivo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Sortdsc", StringUtil.RTrim( Ddo_checklist_impeditivo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_impeditivo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_impeditivo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Caption", StringUtil.RTrim( Ddo_checklist_senaocumpre_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Tooltip", StringUtil.RTrim( Ddo_checklist_senaocumpre_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Cls", StringUtil.RTrim( Ddo_checklist_senaocumpre_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_senaocumpre_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_senaocumpre_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_senaocumpre_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_senaocumpre_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_senaocumpre_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Sortedstatus", StringUtil.RTrim( Ddo_checklist_senaocumpre_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includefilter", StringUtil.BoolToStr( Ddo_checklist_senaocumpre_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_senaocumpre_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Datalisttype", StringUtil.RTrim( Ddo_checklist_senaocumpre_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_checklist_senaocumpre_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Datalistfixedvalues", StringUtil.RTrim( Ddo_checklist_senaocumpre_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Sortasc", StringUtil.RTrim( Ddo_checklist_senaocumpre_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Sortdsc", StringUtil.RTrim( Ddo_checklist_senaocumpre_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Cleanfilter", StringUtil.RTrim( Ddo_checklist_senaocumpre_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_senaocumpre_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Caption", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Tooltip", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Cls", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_naocnfqdo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_naocnfqdo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_naocnfqdo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_naocnfqdo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Datalisttype", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_checklist_naocnfqdo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Datalistfixedvalues", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Sortasc", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Sortdsc", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_checklist_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_impeditivo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_impeditivo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Activeeventkey", StringUtil.RTrim( Ddo_checklist_senaocumpre_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_senaocumpre_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_naocnfqdo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormO82( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("checklistitenswc.js", "?20203117225533");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "CheckListItensWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check List Itens WC" ;
      }

      protected void WBO80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "checklistitenswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_O82( true) ;
         }
         else
         {
            wb_table1_2_O82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_O82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListItensWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_CheckListItensWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfchecklist_descricao_Internalname, AV17TFCheckList_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, edtavTfchecklist_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_CheckListItensWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfchecklist_descricao_sel_Internalname, AV18TFCheckList_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, edtavTfchecklist_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_CheckListItensWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfchecklist_impeditivo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFCheckList_Impeditivo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfchecklist_impeditivo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfchecklist_impeditivo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CheckListItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CHECKLIST_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", 0, edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CheckListItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CHECKLIST_IMPEDITIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Internalname, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CheckListItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CHECKLIST_SENAOCUMPREContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Internalname, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", 0, edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CheckListItensWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CHECKLIST_NAOCNFQDOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Internalname, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", 0, edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_CheckListItensWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTO82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Check List Itens WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPO80( ) ;
            }
         }
      }

      protected void WSO82( )
      {
         STARTO82( ) ;
         EVTO82( ) ;
      }

      protected void EVTO82( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11O82 */
                                    E11O82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12O82 */
                                    E12O82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_IMPEDITIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13O82 */
                                    E13O82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_SENAOCUMPRE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14O82 */
                                    E14O82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_NAOCNFQDO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15O82 */
                                    E15O82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16O82 */
                                    E16O82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO80( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV27Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27Update)) ? AV53Update_GXI : context.convertURL( context.PathToRelativeUrl( AV27Update))));
                              AV28Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV54Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV28Delete))));
                              A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
                              n1839Check_Codigo = false;
                              A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", "."));
                              A763CheckList_Descricao = cgiGet( edtCheckList_Descricao_Internalname);
                              cmbCheckList_Obrigatorio.Name = cmbCheckList_Obrigatorio_Internalname;
                              cmbCheckList_Obrigatorio.CurrentValue = cgiGet( cmbCheckList_Obrigatorio_Internalname);
                              A1845CheckList_Obrigatorio = StringUtil.StrToBool( cgiGet( cmbCheckList_Obrigatorio_Internalname));
                              n1845CheckList_Obrigatorio = false;
                              cmbCheckList_Impeditivo.Name = cmbCheckList_Impeditivo_Internalname;
                              cmbCheckList_Impeditivo.CurrentValue = cgiGet( cmbCheckList_Impeditivo_Internalname);
                              A1850CheckList_Impeditivo = StringUtil.StrToBool( cgiGet( cmbCheckList_Impeditivo_Internalname));
                              n1850CheckList_Impeditivo = false;
                              cmbCheckList_SeNaoCumpre.Name = cmbCheckList_SeNaoCumpre_Internalname;
                              cmbCheckList_SeNaoCumpre.CurrentValue = cgiGet( cmbCheckList_SeNaoCumpre_Internalname);
                              A1848CheckList_SeNaoCumpre = (short)(NumberUtil.Val( cgiGet( cmbCheckList_SeNaoCumpre_Internalname), "."));
                              n1848CheckList_SeNaoCumpre = false;
                              AV50NaoConformidade_Nome = StringUtil.Upper( cgiGet( edtavNaoconformidade_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNaoconformidade_nome_Internalname, AV50NaoConformidade_Nome);
                              cmbCheckList_NaoCnfQdo.Name = cmbCheckList_NaoCnfQdo_Internalname;
                              cmbCheckList_NaoCnfQdo.CurrentValue = cgiGet( cmbCheckList_NaoCnfQdo_Internalname);
                              A1851CheckList_NaoCnfQdo = cgiGet( cmbCheckList_NaoCnfQdo_Internalname);
                              n1851CheckList_NaoCnfQdo = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17O82 */
                                          E17O82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18O82 */
                                          E18O82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19O82 */
                                          E19O82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfchecklist_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCHECKLIST_DESCRICAO"), AV17TFCheckList_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfchecklist_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCHECKLIST_DESCRICAO_SEL"), AV18TFCheckList_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfchecklist_impeditivo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCHECKLIST_IMPEDITIVO_SEL"), ",", ".") != Convert.ToDecimal( AV40TFCheckList_Impeditivo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPO80( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEO82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormO82( ) ;
            }
         }
      }

      protected void PAO82( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CHECKLIST_OBRIGATORIO_" + sGXsfl_8_idx;
            cmbCheckList_Obrigatorio.Name = GXCCtl;
            cmbCheckList_Obrigatorio.WebTags = "";
            cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
            {
               A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
               n1845CheckList_Obrigatorio = false;
            }
            GXCCtl = "CHECKLIST_IMPEDITIVO_" + sGXsfl_8_idx;
            cmbCheckList_Impeditivo.Name = GXCCtl;
            cmbCheckList_Impeditivo.WebTags = "";
            cmbCheckList_Impeditivo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbCheckList_Impeditivo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbCheckList_Impeditivo.ItemCount > 0 )
            {
               A1850CheckList_Impeditivo = StringUtil.StrToBool( cmbCheckList_Impeditivo.getValidValue(StringUtil.BoolToStr( A1850CheckList_Impeditivo)));
               n1850CheckList_Impeditivo = false;
            }
            GXCCtl = "CHECKLIST_SENAOCUMPRE_" + sGXsfl_8_idx;
            cmbCheckList_SeNaoCumpre.Name = GXCCtl;
            cmbCheckList_SeNaoCumpre.WebTags = "";
            cmbCheckList_SeNaoCumpre.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhuma)", 0);
            cmbCheckList_SeNaoCumpre.addItem("1", "Retorna", 0);
            if ( cmbCheckList_SeNaoCumpre.ItemCount > 0 )
            {
               A1848CheckList_SeNaoCumpre = (short)(NumberUtil.Val( cmbCheckList_SeNaoCumpre.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0))), "."));
               n1848CheckList_SeNaoCumpre = false;
            }
            GXCCtl = "CHECKLIST_NAOCNFQDO_" + sGXsfl_8_idx;
            cmbCheckList_NaoCnfQdo.Name = GXCCtl;
            cmbCheckList_NaoCnfQdo.WebTags = "";
            cmbCheckList_NaoCnfQdo.addItem("", "---", 0);
            cmbCheckList_NaoCnfQdo.addItem("S", "SIM", 0);
            cmbCheckList_NaoCnfQdo.addItem("N", "N�O", 0);
            if ( cmbCheckList_NaoCnfQdo.ItemCount > 0 )
            {
               A1851CheckList_NaoCnfQdo = cmbCheckList_NaoCnfQdo.getValidValue(A1851CheckList_NaoCnfQdo);
               n1851CheckList_NaoCnfQdo = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV17TFCheckList_Descricao ,
                                       String AV18TFCheckList_Descricao_Sel ,
                                       short AV40TFCheckList_Impeditivo_Sel ,
                                       int AV7Check_Codigo ,
                                       String AV19ddo_CheckList_DescricaoTitleControlIdToReplace ,
                                       String AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace ,
                                       String AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace ,
                                       String AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace ,
                                       String AV56Pgmname ,
                                       IGxCollection AV37TFCheckList_SeNaoCumpre_Sels ,
                                       IGxCollection AV44TFCheckList_NaoCnfQdo_Sels ,
                                       int A758CheckList_Codigo ,
                                       int A426NaoConformidade_Codigo ,
                                       int A1846CheckList_NaoCnfCod ,
                                       String A427NaoConformidade_Nome ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFO82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECK_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_DESCRICAO", GetSecureSignedToken( sPrefix, A763CheckList_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_DESCRICAO", A763CheckList_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1845CheckList_Obrigatorio));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_OBRIGATORIO", StringUtil.BoolToStr( A1845CheckList_Obrigatorio));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_IMPEDITIVO", GetSecureSignedToken( sPrefix, A1850CheckList_Impeditivo));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_IMPEDITIVO", StringUtil.BoolToStr( A1850CheckList_Impeditivo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_SENAOCUMPRE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1848CheckList_SeNaoCumpre), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_SENAOCUMPRE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_NAOCNFQDO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1851CheckList_NaoCnfQdo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CHECKLIST_NAOCNFQDO", StringUtil.RTrim( A1851CheckList_NaoCnfQdo));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFO82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV56Pgmname = "CheckListItensWC";
         context.Gx_err = 0;
         edtavNaoconformidade_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNaoconformidade_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome_Enabled), 5, 0)));
      }

      protected void RFO82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E18O82 */
         E18O82 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1848CheckList_SeNaoCumpre ,
                                                 AV37TFCheckList_SeNaoCumpre_Sels ,
                                                 A1851CheckList_NaoCnfQdo ,
                                                 AV44TFCheckList_NaoCnfQdo_Sels ,
                                                 AV18TFCheckList_Descricao_Sel ,
                                                 AV17TFCheckList_Descricao ,
                                                 AV40TFCheckList_Impeditivo_Sel ,
                                                 AV37TFCheckList_SeNaoCumpre_Sels.Count ,
                                                 AV44TFCheckList_NaoCnfQdo_Sels.Count ,
                                                 A763CheckList_Descricao ,
                                                 A1850CheckList_Impeditivo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1839Check_Codigo ,
                                                 AV7Check_Codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV17TFCheckList_Descricao = StringUtil.Concat( StringUtil.RTrim( AV17TFCheckList_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFCheckList_Descricao", AV17TFCheckList_Descricao);
            /* Using cursor H00O82 */
            pr_default.execute(0, new Object[] {AV7Check_Codigo, lV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1846CheckList_NaoCnfCod = H00O82_A1846CheckList_NaoCnfCod[0];
               n1846CheckList_NaoCnfCod = H00O82_n1846CheckList_NaoCnfCod[0];
               A1851CheckList_NaoCnfQdo = H00O82_A1851CheckList_NaoCnfQdo[0];
               n1851CheckList_NaoCnfQdo = H00O82_n1851CheckList_NaoCnfQdo[0];
               A1848CheckList_SeNaoCumpre = H00O82_A1848CheckList_SeNaoCumpre[0];
               n1848CheckList_SeNaoCumpre = H00O82_n1848CheckList_SeNaoCumpre[0];
               A1850CheckList_Impeditivo = H00O82_A1850CheckList_Impeditivo[0];
               n1850CheckList_Impeditivo = H00O82_n1850CheckList_Impeditivo[0];
               A1845CheckList_Obrigatorio = H00O82_A1845CheckList_Obrigatorio[0];
               n1845CheckList_Obrigatorio = H00O82_n1845CheckList_Obrigatorio[0];
               A763CheckList_Descricao = H00O82_A763CheckList_Descricao[0];
               A758CheckList_Codigo = H00O82_A758CheckList_Codigo[0];
               A1839Check_Codigo = H00O82_A1839Check_Codigo[0];
               n1839Check_Codigo = H00O82_n1839Check_Codigo[0];
               /* Execute user event: E19O82 */
               E19O82 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBO80( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1848CheckList_SeNaoCumpre ,
                                              AV37TFCheckList_SeNaoCumpre_Sels ,
                                              A1851CheckList_NaoCnfQdo ,
                                              AV44TFCheckList_NaoCnfQdo_Sels ,
                                              AV18TFCheckList_Descricao_Sel ,
                                              AV17TFCheckList_Descricao ,
                                              AV40TFCheckList_Impeditivo_Sel ,
                                              AV37TFCheckList_SeNaoCumpre_Sels.Count ,
                                              AV44TFCheckList_NaoCnfQdo_Sels.Count ,
                                              A763CheckList_Descricao ,
                                              A1850CheckList_Impeditivo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1839Check_Codigo ,
                                              AV7Check_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV17TFCheckList_Descricao = StringUtil.Concat( StringUtil.RTrim( AV17TFCheckList_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFCheckList_Descricao", AV17TFCheckList_Descricao);
         /* Using cursor H00O83 */
         pr_default.execute(1, new Object[] {AV7Check_Codigo, lV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel});
         GRID_nRecordCount = H00O83_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, AV40TFCheckList_Impeditivo_Sel, AV7Check_Codigo, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, AV56Pgmname, AV37TFCheckList_SeNaoCumpre_Sels, AV44TFCheckList_NaoCnfQdo_Sels, A758CheckList_Codigo, A426NaoConformidade_Codigo, A1846CheckList_NaoCnfCod, A427NaoConformidade_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, AV40TFCheckList_Impeditivo_Sel, AV7Check_Codigo, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, AV56Pgmname, AV37TFCheckList_SeNaoCumpre_Sels, AV44TFCheckList_NaoCnfQdo_Sels, A758CheckList_Codigo, A426NaoConformidade_Codigo, A1846CheckList_NaoCnfCod, A427NaoConformidade_Nome, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, AV40TFCheckList_Impeditivo_Sel, AV7Check_Codigo, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, AV56Pgmname, AV37TFCheckList_SeNaoCumpre_Sels, AV44TFCheckList_NaoCnfQdo_Sels, A758CheckList_Codigo, A426NaoConformidade_Codigo, A1846CheckList_NaoCnfCod, A427NaoConformidade_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, AV40TFCheckList_Impeditivo_Sel, AV7Check_Codigo, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, AV56Pgmname, AV37TFCheckList_SeNaoCumpre_Sels, AV44TFCheckList_NaoCnfQdo_Sels, A758CheckList_Codigo, A426NaoConformidade_Codigo, A1846CheckList_NaoCnfCod, A427NaoConformidade_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFCheckList_Descricao, AV18TFCheckList_Descricao_Sel, AV40TFCheckList_Impeditivo_Sel, AV7Check_Codigo, AV19ddo_CheckList_DescricaoTitleControlIdToReplace, AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, AV56Pgmname, AV37TFCheckList_SeNaoCumpre_Sels, AV44TFCheckList_NaoCnfQdo_Sels, A758CheckList_Codigo, A426NaoConformidade_Codigo, A1846CheckList_NaoCnfCod, A427NaoConformidade_Nome, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPO80( )
      {
         /* Before Start, stand alone formulas. */
         AV56Pgmname = "CheckListItensWC";
         context.Gx_err = 0;
         edtavNaoconformidade_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNaoconformidade_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNaoconformidade_nome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17O82 */
         E17O82 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV23DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCHECKLIST_DESCRICAOTITLEFILTERDATA"), AV16CheckList_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCHECKLIST_IMPEDITIVOTITLEFILTERDATA"), AV39CheckList_ImpeditivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCHECKLIST_SENAOCUMPRETITLEFILTERDATA"), AV35CheckList_SeNaoCumpreTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCHECKLIST_NAOCNFQDOTITLEFILTERDATA"), AV42CheckList_NaoCnfQdoTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV17TFCheckList_Descricao = cgiGet( edtavTfchecklist_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFCheckList_Descricao", AV17TFCheckList_Descricao);
            AV18TFCheckList_Descricao_Sel = cgiGet( edtavTfchecklist_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFCheckList_Descricao_Sel", AV18TFCheckList_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_impeditivo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_impeditivo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECKLIST_IMPEDITIVO_SEL");
               GX_FocusControl = edtavTfchecklist_impeditivo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFCheckList_Impeditivo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCheckList_Impeditivo_Sel", StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0));
            }
            else
            {
               AV40TFCheckList_Impeditivo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfchecklist_impeditivo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCheckList_Impeditivo_Sel", StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0));
            }
            AV19ddo_CheckList_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_CheckList_DescricaoTitleControlIdToReplace", AV19ddo_CheckList_DescricaoTitleControlIdToReplace);
            AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace = cgiGet( edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace", AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace);
            AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace = cgiGet( edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace", AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace);
            AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace = cgiGet( edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace", AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV25GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV26GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Check_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Check_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_checklist_descricao_Caption = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Caption");
            Ddo_checklist_descricao_Tooltip = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Tooltip");
            Ddo_checklist_descricao_Cls = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Cls");
            Ddo_checklist_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Filteredtext_set");
            Ddo_checklist_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Selectedvalue_set");
            Ddo_checklist_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Dropdownoptionstype");
            Ddo_checklist_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_checklist_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Includesortasc"));
            Ddo_checklist_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Includesortdsc"));
            Ddo_checklist_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Sortedstatus");
            Ddo_checklist_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Includefilter"));
            Ddo_checklist_descricao_Filtertype = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Filtertype");
            Ddo_checklist_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Filterisrange"));
            Ddo_checklist_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Includedatalist"));
            Ddo_checklist_descricao_Datalisttype = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Datalisttype");
            Ddo_checklist_descricao_Datalistproc = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Datalistproc");
            Ddo_checklist_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_checklist_descricao_Sortasc = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Sortasc");
            Ddo_checklist_descricao_Sortdsc = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Sortdsc");
            Ddo_checklist_descricao_Loadingdata = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Loadingdata");
            Ddo_checklist_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Cleanfilter");
            Ddo_checklist_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Noresultsfound");
            Ddo_checklist_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Searchbuttontext");
            Ddo_checklist_impeditivo_Caption = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Caption");
            Ddo_checklist_impeditivo_Tooltip = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Tooltip");
            Ddo_checklist_impeditivo_Cls = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Cls");
            Ddo_checklist_impeditivo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Selectedvalue_set");
            Ddo_checklist_impeditivo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Dropdownoptionstype");
            Ddo_checklist_impeditivo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Titlecontrolidtoreplace");
            Ddo_checklist_impeditivo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includesortasc"));
            Ddo_checklist_impeditivo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includesortdsc"));
            Ddo_checklist_impeditivo_Sortedstatus = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Sortedstatus");
            Ddo_checklist_impeditivo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includefilter"));
            Ddo_checklist_impeditivo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Includedatalist"));
            Ddo_checklist_impeditivo_Datalisttype = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Datalisttype");
            Ddo_checklist_impeditivo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Datalistfixedvalues");
            Ddo_checklist_impeditivo_Sortasc = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Sortasc");
            Ddo_checklist_impeditivo_Sortdsc = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Sortdsc");
            Ddo_checklist_impeditivo_Cleanfilter = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Cleanfilter");
            Ddo_checklist_impeditivo_Searchbuttontext = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Searchbuttontext");
            Ddo_checklist_senaocumpre_Caption = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Caption");
            Ddo_checklist_senaocumpre_Tooltip = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Tooltip");
            Ddo_checklist_senaocumpre_Cls = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Cls");
            Ddo_checklist_senaocumpre_Selectedvalue_set = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Selectedvalue_set");
            Ddo_checklist_senaocumpre_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Dropdownoptionstype");
            Ddo_checklist_senaocumpre_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Titlecontrolidtoreplace");
            Ddo_checklist_senaocumpre_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includesortasc"));
            Ddo_checklist_senaocumpre_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includesortdsc"));
            Ddo_checklist_senaocumpre_Sortedstatus = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Sortedstatus");
            Ddo_checklist_senaocumpre_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includefilter"));
            Ddo_checklist_senaocumpre_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Includedatalist"));
            Ddo_checklist_senaocumpre_Datalisttype = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Datalisttype");
            Ddo_checklist_senaocumpre_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Allowmultipleselection"));
            Ddo_checklist_senaocumpre_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Datalistfixedvalues");
            Ddo_checklist_senaocumpre_Sortasc = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Sortasc");
            Ddo_checklist_senaocumpre_Sortdsc = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Sortdsc");
            Ddo_checklist_senaocumpre_Cleanfilter = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Cleanfilter");
            Ddo_checklist_senaocumpre_Searchbuttontext = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Searchbuttontext");
            Ddo_checklist_naocnfqdo_Caption = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Caption");
            Ddo_checklist_naocnfqdo_Tooltip = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Tooltip");
            Ddo_checklist_naocnfqdo_Cls = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Cls");
            Ddo_checklist_naocnfqdo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Selectedvalue_set");
            Ddo_checklist_naocnfqdo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Dropdownoptionstype");
            Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Titlecontrolidtoreplace");
            Ddo_checklist_naocnfqdo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includesortasc"));
            Ddo_checklist_naocnfqdo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includesortdsc"));
            Ddo_checklist_naocnfqdo_Sortedstatus = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Sortedstatus");
            Ddo_checklist_naocnfqdo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includefilter"));
            Ddo_checklist_naocnfqdo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Includedatalist"));
            Ddo_checklist_naocnfqdo_Datalisttype = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Datalisttype");
            Ddo_checklist_naocnfqdo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Allowmultipleselection"));
            Ddo_checklist_naocnfqdo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Datalistfixedvalues");
            Ddo_checklist_naocnfqdo_Sortasc = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Sortasc");
            Ddo_checklist_naocnfqdo_Sortdsc = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Sortdsc");
            Ddo_checklist_naocnfqdo_Cleanfilter = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Cleanfilter");
            Ddo_checklist_naocnfqdo_Searchbuttontext = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_checklist_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Activeeventkey");
            Ddo_checklist_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Filteredtext_get");
            Ddo_checklist_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_CHECKLIST_DESCRICAO_Selectedvalue_get");
            Ddo_checklist_impeditivo_Activeeventkey = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Activeeventkey");
            Ddo_checklist_impeditivo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CHECKLIST_IMPEDITIVO_Selectedvalue_get");
            Ddo_checklist_senaocumpre_Activeeventkey = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Activeeventkey");
            Ddo_checklist_senaocumpre_Selectedvalue_get = cgiGet( sPrefix+"DDO_CHECKLIST_SENAOCUMPRE_Selectedvalue_get");
            Ddo_checklist_naocnfqdo_Activeeventkey = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Activeeventkey");
            Ddo_checklist_naocnfqdo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CHECKLIST_NAOCNFQDO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCHECKLIST_DESCRICAO"), AV17TFCheckList_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCHECKLIST_DESCRICAO_SEL"), AV18TFCheckList_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCHECKLIST_IMPEDITIVO_SEL"), ",", ".") != Convert.ToDecimal( AV40TFCheckList_Impeditivo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17O82 */
         E17O82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17O82( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfchecklist_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfchecklist_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_descricao_Visible), 5, 0)));
         edtavTfchecklist_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfchecklist_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_descricao_sel_Visible), 5, 0)));
         edtavTfchecklist_impeditivo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfchecklist_impeditivo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_impeditivo_sel_Visible), 5, 0)));
         Ddo_checklist_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "TitleControlIdToReplace", Ddo_checklist_descricao_Titlecontrolidtoreplace);
         AV19ddo_CheckList_DescricaoTitleControlIdToReplace = Ddo_checklist_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ddo_CheckList_DescricaoTitleControlIdToReplace", AV19ddo_CheckList_DescricaoTitleControlIdToReplace);
         edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_impeditivo_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_Impeditivo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_impeditivo_Internalname, "TitleControlIdToReplace", Ddo_checklist_impeditivo_Titlecontrolidtoreplace);
         AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace = Ddo_checklist_impeditivo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace", AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace);
         edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_senaocumpre_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_SeNaoCumpre";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_senaocumpre_Internalname, "TitleControlIdToReplace", Ddo_checklist_senaocumpre_Titlecontrolidtoreplace);
         AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace = Ddo_checklist_senaocumpre_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace", AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace);
         edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_NaoCnfQdo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_naocnfqdo_Internalname, "TitleControlIdToReplace", Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace);
         AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace = Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace", AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace);
         edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV23DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV23DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18O82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16CheckList_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39CheckList_ImpeditivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35CheckList_SeNaoCumpreTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42CheckList_NaoCnfQdoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCheckList_Descricao_Titleformat = 2;
         edtCheckList_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV19ddo_CheckList_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtCheckList_Descricao_Internalname, "Title", edtCheckList_Descricao_Title);
         cmbCheckList_Impeditivo_Titleformat = 2;
         cmbCheckList_Impeditivo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Impeditivo", AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_Impeditivo_Internalname, "Title", cmbCheckList_Impeditivo.Title.Text);
         cmbCheckList_SeNaoCumpre_Titleformat = 2;
         cmbCheckList_SeNaoCumpre.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "A��o", AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_SeNaoCumpre_Internalname, "Title", cmbCheckList_SeNaoCumpre.Title.Text);
         cmbCheckList_NaoCnfQdo_Titleformat = 2;
         cmbCheckList_NaoCnfQdo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Quando", AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_NaoCnfQdo_Internalname, "Title", cmbCheckList_NaoCnfQdo.Title.Text);
         AV25GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25GridCurrentPage), 10, 0)));
         AV26GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV16CheckList_DescricaoTitleFilterData", AV16CheckList_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV39CheckList_ImpeditivoTitleFilterData", AV39CheckList_ImpeditivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV35CheckList_SeNaoCumpreTitleFilterData", AV35CheckList_SeNaoCumpreTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42CheckList_NaoCnfQdoTitleFilterData", AV42CheckList_NaoCnfQdoTitleFilterData);
      }

      protected void E11O82( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV24PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV24PageToGo) ;
         }
      }

      protected void E12O82( )
      {
         /* Ddo_checklist_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFCheckList_Descricao = Ddo_checklist_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFCheckList_Descricao", AV17TFCheckList_Descricao);
            AV18TFCheckList_Descricao_Sel = Ddo_checklist_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFCheckList_Descricao_Sel", AV18TFCheckList_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13O82( )
      {
         /* Ddo_checklist_impeditivo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_impeditivo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_impeditivo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_impeditivo_Internalname, "SortedStatus", Ddo_checklist_impeditivo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_impeditivo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_impeditivo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_impeditivo_Internalname, "SortedStatus", Ddo_checklist_impeditivo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_impeditivo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFCheckList_Impeditivo_Sel = (short)(NumberUtil.Val( Ddo_checklist_impeditivo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCheckList_Impeditivo_Sel", StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14O82( )
      {
         /* Ddo_checklist_senaocumpre_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_senaocumpre_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_senaocumpre_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_senaocumpre_Internalname, "SortedStatus", Ddo_checklist_senaocumpre_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_senaocumpre_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_senaocumpre_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_senaocumpre_Internalname, "SortedStatus", Ddo_checklist_senaocumpre_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_senaocumpre_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFCheckList_SeNaoCumpre_SelsJson = Ddo_checklist_senaocumpre_Selectedvalue_get;
            AV37TFCheckList_SeNaoCumpre_Sels.FromJSonString(StringUtil.StringReplace( AV36TFCheckList_SeNaoCumpre_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV37TFCheckList_SeNaoCumpre_Sels", AV37TFCheckList_SeNaoCumpre_Sels);
      }

      protected void E15O82( )
      {
         /* Ddo_checklist_naocnfqdo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_naocnfqdo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_naocnfqdo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_naocnfqdo_Internalname, "SortedStatus", Ddo_checklist_naocnfqdo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_naocnfqdo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_naocnfqdo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_naocnfqdo_Internalname, "SortedStatus", Ddo_checklist_naocnfqdo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_naocnfqdo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFCheckList_NaoCnfQdo_SelsJson = Ddo_checklist_naocnfqdo_Selectedvalue_get;
            AV44TFCheckList_NaoCnfQdo_Sels.FromJSonString(AV43TFCheckList_NaoCnfQdo_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV44TFCheckList_NaoCnfQdo_Sels", AV44TFCheckList_NaoCnfQdo_Sels);
      }

      private void E19O82( )
      {
         /* Grid_Load Routine */
         AV27Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV27Update);
         AV53Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A758CheckList_Codigo) + "," + UrlEncode("" +AV7Check_Codigo);
         AV28Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV28Delete);
         AV54Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A758CheckList_Codigo) + "," + UrlEncode("" +AV7Check_Codigo);
         /* Using cursor H00O84 */
         pr_default.execute(2, new Object[] {n1846CheckList_NaoCnfCod, A1846CheckList_NaoCnfCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A426NaoConformidade_Codigo = H00O84_A426NaoConformidade_Codigo[0];
            A427NaoConformidade_Nome = H00O84_A427NaoConformidade_Nome[0];
            AV50NaoConformidade_Nome = A427NaoConformidade_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNaoconformidade_nome_Internalname, AV50NaoConformidade_Nome);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E16O82( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7Check_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_checklist_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
         Ddo_checklist_impeditivo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_impeditivo_Internalname, "SortedStatus", Ddo_checklist_impeditivo_Sortedstatus);
         Ddo_checklist_senaocumpre_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_senaocumpre_Internalname, "SortedStatus", Ddo_checklist_senaocumpre_Sortedstatus);
         Ddo_checklist_naocnfqdo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_naocnfqdo_Internalname, "SortedStatus", Ddo_checklist_naocnfqdo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_checklist_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_checklist_impeditivo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_impeditivo_Internalname, "SortedStatus", Ddo_checklist_impeditivo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_checklist_senaocumpre_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_senaocumpre_Internalname, "SortedStatus", Ddo_checklist_senaocumpre_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_checklist_naocnfqdo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_naocnfqdo_Internalname, "SortedStatus", Ddo_checklist_naocnfqdo_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV56Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV56Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV56Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV57GXV1 = 1;
         while ( AV57GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV57GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO") == 0 )
            {
               AV17TFCheckList_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFCheckList_Descricao", AV17TFCheckList_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFCheckList_Descricao)) )
               {
                  Ddo_checklist_descricao_Filteredtext_set = AV17TFCheckList_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "FilteredText_set", Ddo_checklist_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO_SEL") == 0 )
            {
               AV18TFCheckList_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFCheckList_Descricao_Sel", AV18TFCheckList_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCheckList_Descricao_Sel)) )
               {
                  Ddo_checklist_descricao_Selectedvalue_set = AV18TFCheckList_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_descricao_Internalname, "SelectedValue_set", Ddo_checklist_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_IMPEDITIVO_SEL") == 0 )
            {
               AV40TFCheckList_Impeditivo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCheckList_Impeditivo_Sel", StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0));
               if ( ! (0==AV40TFCheckList_Impeditivo_Sel) )
               {
                  Ddo_checklist_impeditivo_Selectedvalue_set = StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_impeditivo_Internalname, "SelectedValue_set", Ddo_checklist_impeditivo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_SENAOCUMPRE_SEL") == 0 )
            {
               AV36TFCheckList_SeNaoCumpre_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV37TFCheckList_SeNaoCumpre_Sels.FromJSonString(AV36TFCheckList_SeNaoCumpre_SelsJson);
               if ( ! ( AV37TFCheckList_SeNaoCumpre_Sels.Count == 0 ) )
               {
                  Ddo_checklist_senaocumpre_Selectedvalue_set = AV36TFCheckList_SeNaoCumpre_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_senaocumpre_Internalname, "SelectedValue_set", Ddo_checklist_senaocumpre_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_NAOCNFQDO_SEL") == 0 )
            {
               AV43TFCheckList_NaoCnfQdo_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV44TFCheckList_NaoCnfQdo_Sels.FromJSonString(AV43TFCheckList_NaoCnfQdo_SelsJson);
               if ( ! ( AV44TFCheckList_NaoCnfQdo_Sels.Count == 0 ) )
               {
                  Ddo_checklist_naocnfqdo_Selectedvalue_set = AV43TFCheckList_NaoCnfQdo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_checklist_naocnfqdo_Internalname, "SelectedValue_set", Ddo_checklist_naocnfqdo_Selectedvalue_set);
               }
            }
            AV57GXV1 = (int)(AV57GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV56Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFCheckList_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV17TFCheckList_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCheckList_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV18TFCheckList_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV40TFCheckList_Impeditivo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_IMPEDITIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV40TFCheckList_Impeditivo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV37TFCheckList_SeNaoCumpre_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_SENAOCUMPRE_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV37TFCheckList_SeNaoCumpre_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV44TFCheckList_NaoCnfQdo_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_NAOCNFQDO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV44TFCheckList_NaoCnfQdo_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Check_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CHECK_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV56Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV56Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "CheckList";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Check_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_O82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 5, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_O82( true) ;
         }
         else
         {
            wb_table2_5_O82( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_O82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_CheckListItensWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_O82e( true) ;
         }
         else
         {
            wb_table1_2_O82e( false) ;
         }
      }

      protected void wb_table2_5_O82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Check List") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Check List Item") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCheckList_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtCheckList_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCheckList_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Obrigat�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheckList_Impeditivo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheckList_Impeditivo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheckList_Impeditivo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheckList_SeNaoCumpre_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheckList_SeNaoCumpre.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheckList_SeNaoCumpre.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "N�o Conformidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheckList_NaoCnfQdo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheckList_NaoCnfQdo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheckList_NaoCnfQdo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV27Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A763CheckList_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCheckList_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCheckList_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1845CheckList_Obrigatorio));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1850CheckList_Impeditivo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheckList_Impeditivo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheckList_Impeditivo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheckList_SeNaoCumpre.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheckList_SeNaoCumpre_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV50NaoConformidade_Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNaoconformidade_nome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1851CheckList_NaoCnfQdo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheckList_NaoCnfQdo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheckList_NaoCnfQdo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_O82e( true) ;
         }
         else
         {
            wb_table2_5_O82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Check_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAO82( ) ;
         WSO82( ) ;
         WEO82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Check_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAO82( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "checklistitenswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAO82( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Check_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0)));
         }
         wcpOAV7Check_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Check_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Check_Codigo != wcpOAV7Check_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Check_Codigo = AV7Check_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Check_Codigo = cgiGet( sPrefix+"AV7Check_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Check_Codigo) > 0 )
         {
            AV7Check_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Check_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0)));
         }
         else
         {
            AV7Check_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Check_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAO82( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSO82( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSO82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Check_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Check_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Check_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Check_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Check_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEO82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311723033");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("checklistitenswc.js", "?2020311723033");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_idx;
         edtCheck_Codigo_Internalname = sPrefix+"CHECK_CODIGO_"+sGXsfl_8_idx;
         edtCheckList_Codigo_Internalname = sPrefix+"CHECKLIST_CODIGO_"+sGXsfl_8_idx;
         edtCheckList_Descricao_Internalname = sPrefix+"CHECKLIST_DESCRICAO_"+sGXsfl_8_idx;
         cmbCheckList_Obrigatorio_Internalname = sPrefix+"CHECKLIST_OBRIGATORIO_"+sGXsfl_8_idx;
         cmbCheckList_Impeditivo_Internalname = sPrefix+"CHECKLIST_IMPEDITIVO_"+sGXsfl_8_idx;
         cmbCheckList_SeNaoCumpre_Internalname = sPrefix+"CHECKLIST_SENAOCUMPRE_"+sGXsfl_8_idx;
         edtavNaoconformidade_nome_Internalname = sPrefix+"vNAOCONFORMIDADE_NOME_"+sGXsfl_8_idx;
         cmbCheckList_NaoCnfQdo_Internalname = sPrefix+"CHECKLIST_NAOCNFQDO_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_fel_idx;
         edtCheck_Codigo_Internalname = sPrefix+"CHECK_CODIGO_"+sGXsfl_8_fel_idx;
         edtCheckList_Codigo_Internalname = sPrefix+"CHECKLIST_CODIGO_"+sGXsfl_8_fel_idx;
         edtCheckList_Descricao_Internalname = sPrefix+"CHECKLIST_DESCRICAO_"+sGXsfl_8_fel_idx;
         cmbCheckList_Obrigatorio_Internalname = sPrefix+"CHECKLIST_OBRIGATORIO_"+sGXsfl_8_fel_idx;
         cmbCheckList_Impeditivo_Internalname = sPrefix+"CHECKLIST_IMPEDITIVO_"+sGXsfl_8_fel_idx;
         cmbCheckList_SeNaoCumpre_Internalname = sPrefix+"CHECKLIST_SENAOCUMPRE_"+sGXsfl_8_fel_idx;
         edtavNaoconformidade_nome_Internalname = sPrefix+"vNAOCONFORMIDADE_NOME_"+sGXsfl_8_fel_idx;
         cmbCheckList_NaoCnfQdo_Internalname = sPrefix+"CHECKLIST_NAOCNFQDO_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBO80( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV27Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV53Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV27Update)) ? AV53Update_GXI : context.PathToRelativeUrl( AV27Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV27Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV54Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV54Delete_GXI : context.PathToRelativeUrl( AV28Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheck_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheck_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheckList_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheckList_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheckList_Descricao_Internalname,(String)A763CheckList_Descricao,(String)A763CheckList_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheckList_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)8,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECKLIST_OBRIGATORIO_" + sGXsfl_8_idx;
               cmbCheckList_Obrigatorio.Name = GXCCtl;
               cmbCheckList_Obrigatorio.WebTags = "";
               cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
               {
                  A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
                  n1845CheckList_Obrigatorio = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheckList_Obrigatorio,(String)cmbCheckList_Obrigatorio_Internalname,StringUtil.BoolToStr( A1845CheckList_Obrigatorio),(short)1,(String)cmbCheckList_Obrigatorio_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheckList_Obrigatorio.CurrentValue = StringUtil.BoolToStr( A1845CheckList_Obrigatorio);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_Obrigatorio_Internalname, "Values", (String)(cmbCheckList_Obrigatorio.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECKLIST_IMPEDITIVO_" + sGXsfl_8_idx;
               cmbCheckList_Impeditivo.Name = GXCCtl;
               cmbCheckList_Impeditivo.WebTags = "";
               cmbCheckList_Impeditivo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbCheckList_Impeditivo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbCheckList_Impeditivo.ItemCount > 0 )
               {
                  A1850CheckList_Impeditivo = StringUtil.StrToBool( cmbCheckList_Impeditivo.getValidValue(StringUtil.BoolToStr( A1850CheckList_Impeditivo)));
                  n1850CheckList_Impeditivo = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheckList_Impeditivo,(String)cmbCheckList_Impeditivo_Internalname,StringUtil.BoolToStr( A1850CheckList_Impeditivo),(short)1,(String)cmbCheckList_Impeditivo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheckList_Impeditivo.CurrentValue = StringUtil.BoolToStr( A1850CheckList_Impeditivo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_Impeditivo_Internalname, "Values", (String)(cmbCheckList_Impeditivo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECKLIST_SENAOCUMPRE_" + sGXsfl_8_idx;
               cmbCheckList_SeNaoCumpre.Name = GXCCtl;
               cmbCheckList_SeNaoCumpre.WebTags = "";
               cmbCheckList_SeNaoCumpre.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhuma)", 0);
               cmbCheckList_SeNaoCumpre.addItem("1", "Retorna", 0);
               if ( cmbCheckList_SeNaoCumpre.ItemCount > 0 )
               {
                  A1848CheckList_SeNaoCumpre = (short)(NumberUtil.Val( cmbCheckList_SeNaoCumpre.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0))), "."));
                  n1848CheckList_SeNaoCumpre = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheckList_SeNaoCumpre,(String)cmbCheckList_SeNaoCumpre_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0)),(short)1,(String)cmbCheckList_SeNaoCumpre_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheckList_SeNaoCumpre.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1848CheckList_SeNaoCumpre), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_SeNaoCumpre_Internalname, "Values", (String)(cmbCheckList_SeNaoCumpre.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNaoconformidade_nome_Internalname,StringUtil.RTrim( AV50NaoConformidade_Nome),StringUtil.RTrim( context.localUtil.Format( AV50NaoConformidade_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNaoconformidade_nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavNaoconformidade_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECKLIST_NAOCNFQDO_" + sGXsfl_8_idx;
               cmbCheckList_NaoCnfQdo.Name = GXCCtl;
               cmbCheckList_NaoCnfQdo.WebTags = "";
               cmbCheckList_NaoCnfQdo.addItem("", "---", 0);
               cmbCheckList_NaoCnfQdo.addItem("S", "SIM", 0);
               cmbCheckList_NaoCnfQdo.addItem("N", "N�O", 0);
               if ( cmbCheckList_NaoCnfQdo.ItemCount > 0 )
               {
                  A1851CheckList_NaoCnfQdo = cmbCheckList_NaoCnfQdo.getValidValue(A1851CheckList_NaoCnfQdo);
                  n1851CheckList_NaoCnfQdo = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheckList_NaoCnfQdo,(String)cmbCheckList_NaoCnfQdo_Internalname,StringUtil.RTrim( A1851CheckList_NaoCnfQdo),(short)1,(String)cmbCheckList_NaoCnfQdo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheckList_NaoCnfQdo.CurrentValue = StringUtil.RTrim( A1851CheckList_NaoCnfQdo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_NaoCnfQdo_Internalname, "Values", (String)(cmbCheckList_NaoCnfQdo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECK_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_DESCRICAO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A763CheckList_Descricao));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_OBRIGATORIO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1845CheckList_Obrigatorio));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_IMPEDITIVO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1850CheckList_Impeditivo));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_SENAOCUMPRE"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1848CheckList_SeNaoCumpre), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_NAOCNFQDO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A1851CheckList_NaoCnfQdo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtCheck_Codigo_Internalname = sPrefix+"CHECK_CODIGO";
         edtCheckList_Codigo_Internalname = sPrefix+"CHECKLIST_CODIGO";
         edtCheckList_Descricao_Internalname = sPrefix+"CHECKLIST_DESCRICAO";
         cmbCheckList_Obrigatorio_Internalname = sPrefix+"CHECKLIST_OBRIGATORIO";
         cmbCheckList_Impeditivo_Internalname = sPrefix+"CHECKLIST_IMPEDITIVO";
         cmbCheckList_SeNaoCumpre_Internalname = sPrefix+"CHECKLIST_SENAOCUMPRE";
         edtavNaoconformidade_nome_Internalname = sPrefix+"vNAOCONFORMIDADE_NOME";
         cmbCheckList_NaoCnfQdo_Internalname = sPrefix+"CHECKLIST_NAOCNFQDO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfchecklist_descricao_Internalname = sPrefix+"vTFCHECKLIST_DESCRICAO";
         edtavTfchecklist_descricao_sel_Internalname = sPrefix+"vTFCHECKLIST_DESCRICAO_SEL";
         edtavTfchecklist_impeditivo_sel_Internalname = sPrefix+"vTFCHECKLIST_IMPEDITIVO_SEL";
         Ddo_checklist_descricao_Internalname = sPrefix+"DDO_CHECKLIST_DESCRICAO";
         edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_checklist_impeditivo_Internalname = sPrefix+"DDO_CHECKLIST_IMPEDITIVO";
         edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE";
         Ddo_checklist_senaocumpre_Internalname = sPrefix+"DDO_CHECKLIST_SENAOCUMPRE";
         edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE";
         Ddo_checklist_naocnfqdo_Internalname = sPrefix+"DDO_CHECKLIST_NAOCNFQDO";
         edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbCheckList_NaoCnfQdo_Jsonclick = "";
         edtavNaoconformidade_nome_Jsonclick = "";
         cmbCheckList_SeNaoCumpre_Jsonclick = "";
         cmbCheckList_Impeditivo_Jsonclick = "";
         cmbCheckList_Obrigatorio_Jsonclick = "";
         edtCheckList_Descricao_Jsonclick = "";
         edtCheckList_Codigo_Jsonclick = "";
         edtCheck_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavNaoconformidade_nome_Enabled = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbCheckList_NaoCnfQdo_Titleformat = 0;
         cmbCheckList_SeNaoCumpre_Titleformat = 0;
         cmbCheckList_Impeditivo_Titleformat = 0;
         edtCheckList_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbCheckList_NaoCnfQdo.Title.Text = "Quando";
         cmbCheckList_SeNaoCumpre.Title.Text = "A��o";
         cmbCheckList_Impeditivo.Title.Text = "Impeditivo";
         edtCheckList_Descricao_Title = "Descri��o";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfchecklist_impeditivo_sel_Jsonclick = "";
         edtavTfchecklist_impeditivo_sel_Visible = 1;
         edtavTfchecklist_descricao_sel_Visible = 1;
         edtavTfchecklist_descricao_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_checklist_naocnfqdo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_checklist_naocnfqdo_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_naocnfqdo_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_naocnfqdo_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_naocnfqdo_Datalistfixedvalues = "S:SIM,N:N�O";
         Ddo_checklist_naocnfqdo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_checklist_naocnfqdo_Datalisttype = "FixedValues";
         Ddo_checklist_naocnfqdo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_naocnfqdo_Includefilter = Convert.ToBoolean( 0);
         Ddo_checklist_naocnfqdo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_naocnfqdo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace = "";
         Ddo_checklist_naocnfqdo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_naocnfqdo_Cls = "ColumnSettings";
         Ddo_checklist_naocnfqdo_Tooltip = "Op��es";
         Ddo_checklist_naocnfqdo_Caption = "";
         Ddo_checklist_senaocumpre_Searchbuttontext = "Filtrar Selecionados";
         Ddo_checklist_senaocumpre_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_senaocumpre_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_senaocumpre_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_senaocumpre_Datalistfixedvalues = "1:Retorna";
         Ddo_checklist_senaocumpre_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_checklist_senaocumpre_Datalisttype = "FixedValues";
         Ddo_checklist_senaocumpre_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_senaocumpre_Includefilter = Convert.ToBoolean( 0);
         Ddo_checklist_senaocumpre_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_senaocumpre_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_senaocumpre_Titlecontrolidtoreplace = "";
         Ddo_checklist_senaocumpre_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_senaocumpre_Cls = "ColumnSettings";
         Ddo_checklist_senaocumpre_Tooltip = "Op��es";
         Ddo_checklist_senaocumpre_Caption = "";
         Ddo_checklist_impeditivo_Searchbuttontext = "Pesquisar";
         Ddo_checklist_impeditivo_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_impeditivo_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_impeditivo_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_impeditivo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_checklist_impeditivo_Datalisttype = "FixedValues";
         Ddo_checklist_impeditivo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_impeditivo_Includefilter = Convert.ToBoolean( 0);
         Ddo_checklist_impeditivo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_impeditivo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_impeditivo_Titlecontrolidtoreplace = "";
         Ddo_checklist_impeditivo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_impeditivo_Cls = "ColumnSettings";
         Ddo_checklist_impeditivo_Tooltip = "Op��es";
         Ddo_checklist_impeditivo_Caption = "";
         Ddo_checklist_descricao_Searchbuttontext = "Pesquisar";
         Ddo_checklist_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_checklist_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_descricao_Loadingdata = "Carregando dados...";
         Ddo_checklist_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_checklist_descricao_Datalistproc = "GetCheckListItensWCFilterData";
         Ddo_checklist_descricao_Datalisttype = "Dynamic";
         Ddo_checklist_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_checklist_descricao_Filtertype = "Character";
         Ddo_checklist_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Titlecontrolidtoreplace = "";
         Ddo_checklist_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_descricao_Cls = "ColumnSettings";
         Ddo_checklist_descricao_Tooltip = "Op��es";
         Ddo_checklist_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV19ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace',fld:'vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16CheckList_DescricaoTitleFilterData',fld:'vCHECKLIST_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV39CheckList_ImpeditivoTitleFilterData',fld:'vCHECKLIST_IMPEDITIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV35CheckList_SeNaoCumpreTitleFilterData',fld:'vCHECKLIST_SENAOCUMPRETITLEFILTERDATA',pic:'',nv:null},{av:'AV42CheckList_NaoCnfQdoTitleFilterData',fld:'vCHECKLIST_NAOCNFQDOTITLEFILTERDATA',pic:'',nv:null},{av:'edtCheckList_Descricao_Titleformat',ctrl:'CHECKLIST_DESCRICAO',prop:'Titleformat'},{av:'edtCheckList_Descricao_Title',ctrl:'CHECKLIST_DESCRICAO',prop:'Title'},{av:'cmbCheckList_Impeditivo'},{av:'cmbCheckList_SeNaoCumpre'},{av:'cmbCheckList_NaoCnfQdo'},{av:'AV25GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV26GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11O82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace',fld:'vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CHECKLIST_DESCRICAO.ONOPTIONCLICKED","{handler:'E12O82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace',fld:'vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_checklist_descricao_Activeeventkey',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_checklist_descricao_Filteredtext_get',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_checklist_descricao_Selectedvalue_get',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_checklist_impeditivo_Sortedstatus',ctrl:'DDO_CHECKLIST_IMPEDITIVO',prop:'SortedStatus'},{av:'Ddo_checklist_senaocumpre_Sortedstatus',ctrl:'DDO_CHECKLIST_SENAOCUMPRE',prop:'SortedStatus'},{av:'Ddo_checklist_naocnfqdo_Sortedstatus',ctrl:'DDO_CHECKLIST_NAOCNFQDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_IMPEDITIVO.ONOPTIONCLICKED","{handler:'E13O82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace',fld:'vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_checklist_impeditivo_Activeeventkey',ctrl:'DDO_CHECKLIST_IMPEDITIVO',prop:'ActiveEventKey'},{av:'Ddo_checklist_impeditivo_Selectedvalue_get',ctrl:'DDO_CHECKLIST_IMPEDITIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_impeditivo_Sortedstatus',ctrl:'DDO_CHECKLIST_IMPEDITIVO',prop:'SortedStatus'},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_senaocumpre_Sortedstatus',ctrl:'DDO_CHECKLIST_SENAOCUMPRE',prop:'SortedStatus'},{av:'Ddo_checklist_naocnfqdo_Sortedstatus',ctrl:'DDO_CHECKLIST_NAOCNFQDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_SENAOCUMPRE.ONOPTIONCLICKED","{handler:'E14O82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace',fld:'vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_checklist_senaocumpre_Activeeventkey',ctrl:'DDO_CHECKLIST_SENAOCUMPRE',prop:'ActiveEventKey'},{av:'Ddo_checklist_senaocumpre_Selectedvalue_get',ctrl:'DDO_CHECKLIST_SENAOCUMPRE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_senaocumpre_Sortedstatus',ctrl:'DDO_CHECKLIST_SENAOCUMPRE',prop:'SortedStatus'},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_impeditivo_Sortedstatus',ctrl:'DDO_CHECKLIST_IMPEDITIVO',prop:'SortedStatus'},{av:'Ddo_checklist_naocnfqdo_Sortedstatus',ctrl:'DDO_CHECKLIST_NAOCNFQDO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_NAOCNFQDO.ONOPTIONCLICKED","{handler:'E15O82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV18TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV40TFCheckList_Impeditivo_Sel',fld:'vTFCHECKLIST_IMPEDITIVO_SEL',pic:'9',nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_IMPEDITIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace',fld:'vDDO_CHECKLIST_SENAOCUMPRETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_NAOCNFQDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV37TFCheckList_SeNaoCumpre_Sels',fld:'vTFCHECKLIST_SENAOCUMPRE_SELS',pic:'',nv:null},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_checklist_naocnfqdo_Activeeventkey',ctrl:'DDO_CHECKLIST_NAOCNFQDO',prop:'ActiveEventKey'},{av:'Ddo_checklist_naocnfqdo_Selectedvalue_get',ctrl:'DDO_CHECKLIST_NAOCNFQDO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_naocnfqdo_Sortedstatus',ctrl:'DDO_CHECKLIST_NAOCNFQDO',prop:'SortedStatus'},{av:'AV44TFCheckList_NaoCnfQdo_Sels',fld:'vTFCHECKLIST_NAOCNFQDO_SELS',pic:'',nv:null},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_impeditivo_Sortedstatus',ctrl:'DDO_CHECKLIST_IMPEDITIVO',prop:'SortedStatus'},{av:'Ddo_checklist_senaocumpre_Sortedstatus',ctrl:'DDO_CHECKLIST_SENAOCUMPRE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19O82',iparms:[{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A426NaoConformidade_Codigo',fld:'NAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1846CheckList_NaoCnfCod',fld:'CHECKLIST_NAOCNFCOD',pic:'ZZZZZ9',nv:0},{av:'A427NaoConformidade_Nome',fld:'NAOCONFORMIDADE_NOME',pic:'@!',nv:''}],oparms:[{av:'AV27Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV50NaoConformidade_Nome',fld:'vNAOCONFORMIDADE_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16O82',iparms:[{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_checklist_descricao_Activeeventkey = "";
         Ddo_checklist_descricao_Filteredtext_get = "";
         Ddo_checklist_descricao_Selectedvalue_get = "";
         Ddo_checklist_impeditivo_Activeeventkey = "";
         Ddo_checklist_impeditivo_Selectedvalue_get = "";
         Ddo_checklist_senaocumpre_Activeeventkey = "";
         Ddo_checklist_senaocumpre_Selectedvalue_get = "";
         Ddo_checklist_naocnfqdo_Activeeventkey = "";
         Ddo_checklist_naocnfqdo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17TFCheckList_Descricao = "";
         AV18TFCheckList_Descricao_Sel = "";
         AV19ddo_CheckList_DescricaoTitleControlIdToReplace = "";
         AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace = "";
         AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace = "";
         AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace = "";
         AV56Pgmname = "";
         AV37TFCheckList_SeNaoCumpre_Sels = new GxSimpleCollection();
         AV44TFCheckList_NaoCnfQdo_Sels = new GxSimpleCollection();
         A427NaoConformidade_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV23DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV16CheckList_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39CheckList_ImpeditivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35CheckList_SeNaoCumpreTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42CheckList_NaoCnfQdoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_checklist_descricao_Filteredtext_set = "";
         Ddo_checklist_descricao_Selectedvalue_set = "";
         Ddo_checklist_descricao_Sortedstatus = "";
         Ddo_checklist_impeditivo_Selectedvalue_set = "";
         Ddo_checklist_impeditivo_Sortedstatus = "";
         Ddo_checklist_senaocumpre_Selectedvalue_set = "";
         Ddo_checklist_senaocumpre_Sortedstatus = "";
         Ddo_checklist_naocnfqdo_Selectedvalue_set = "";
         Ddo_checklist_naocnfqdo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27Update = "";
         AV53Update_GXI = "";
         AV28Delete = "";
         AV54Delete_GXI = "";
         A763CheckList_Descricao = "";
         AV50NaoConformidade_Nome = "";
         A1851CheckList_NaoCnfQdo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17TFCheckList_Descricao = "";
         H00O82_A1846CheckList_NaoCnfCod = new int[1] ;
         H00O82_n1846CheckList_NaoCnfCod = new bool[] {false} ;
         H00O82_A1851CheckList_NaoCnfQdo = new String[] {""} ;
         H00O82_n1851CheckList_NaoCnfQdo = new bool[] {false} ;
         H00O82_A1848CheckList_SeNaoCumpre = new short[1] ;
         H00O82_n1848CheckList_SeNaoCumpre = new bool[] {false} ;
         H00O82_A1850CheckList_Impeditivo = new bool[] {false} ;
         H00O82_n1850CheckList_Impeditivo = new bool[] {false} ;
         H00O82_A1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O82_n1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O82_A763CheckList_Descricao = new String[] {""} ;
         H00O82_A758CheckList_Codigo = new int[1] ;
         H00O82_A1839Check_Codigo = new int[1] ;
         H00O82_n1839Check_Codigo = new bool[] {false} ;
         H00O83_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV36TFCheckList_SeNaoCumpre_SelsJson = "";
         AV43TFCheckList_NaoCnfQdo_SelsJson = "";
         H00O84_A426NaoConformidade_Codigo = new int[1] ;
         H00O84_A427NaoConformidade_Nome = new String[] {""} ;
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Check_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.checklistitenswc__default(),
            new Object[][] {
                new Object[] {
               H00O82_A1846CheckList_NaoCnfCod, H00O82_n1846CheckList_NaoCnfCod, H00O82_A1851CheckList_NaoCnfQdo, H00O82_n1851CheckList_NaoCnfQdo, H00O82_A1848CheckList_SeNaoCumpre, H00O82_n1848CheckList_SeNaoCumpre, H00O82_A1850CheckList_Impeditivo, H00O82_n1850CheckList_Impeditivo, H00O82_A1845CheckList_Obrigatorio, H00O82_n1845CheckList_Obrigatorio,
               H00O82_A763CheckList_Descricao, H00O82_A758CheckList_Codigo, H00O82_A1839Check_Codigo, H00O82_n1839Check_Codigo
               }
               , new Object[] {
               H00O83_AGRID_nRecordCount
               }
               , new Object[] {
               H00O84_A426NaoConformidade_Codigo, H00O84_A427NaoConformidade_Nome
               }
            }
         );
         AV56Pgmname = "CheckListItensWC";
         /* GeneXus formulas. */
         AV56Pgmname = "CheckListItensWC";
         context.Gx_err = 0;
         edtavNaoconformidade_nome_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short AV40TFCheckList_Impeditivo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1848CheckList_SeNaoCumpre ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtCheckList_Descricao_Titleformat ;
      private short cmbCheckList_Impeditivo_Titleformat ;
      private short cmbCheckList_SeNaoCumpre_Titleformat ;
      private short cmbCheckList_NaoCnfQdo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Check_Codigo ;
      private int wcpOAV7Check_Codigo ;
      private int subGrid_Rows ;
      private int A758CheckList_Codigo ;
      private int A426NaoConformidade_Codigo ;
      private int A1846CheckList_NaoCnfCod ;
      private int edtavNaoconformidade_nome_Enabled ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_checklist_descricao_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfchecklist_descricao_Visible ;
      private int edtavTfchecklist_descricao_sel_Visible ;
      private int edtavTfchecklist_impeditivo_sel_Visible ;
      private int edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Visible ;
      private int A1839Check_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV37TFCheckList_SeNaoCumpre_Sels_Count ;
      private int AV44TFCheckList_NaoCnfQdo_Sels_Count ;
      private int AV24PageToGo ;
      private int AV57GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV25GridCurrentPage ;
      private long AV26GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_checklist_descricao_Activeeventkey ;
      private String Ddo_checklist_descricao_Filteredtext_get ;
      private String Ddo_checklist_descricao_Selectedvalue_get ;
      private String Ddo_checklist_impeditivo_Activeeventkey ;
      private String Ddo_checklist_impeditivo_Selectedvalue_get ;
      private String Ddo_checklist_senaocumpre_Activeeventkey ;
      private String Ddo_checklist_senaocumpre_Selectedvalue_get ;
      private String Ddo_checklist_naocnfqdo_Activeeventkey ;
      private String Ddo_checklist_naocnfqdo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV56Pgmname ;
      private String A427NaoConformidade_Nome ;
      private String GXKey ;
      private String edtavNaoconformidade_nome_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_checklist_descricao_Caption ;
      private String Ddo_checklist_descricao_Tooltip ;
      private String Ddo_checklist_descricao_Cls ;
      private String Ddo_checklist_descricao_Filteredtext_set ;
      private String Ddo_checklist_descricao_Selectedvalue_set ;
      private String Ddo_checklist_descricao_Dropdownoptionstype ;
      private String Ddo_checklist_descricao_Titlecontrolidtoreplace ;
      private String Ddo_checklist_descricao_Sortedstatus ;
      private String Ddo_checklist_descricao_Filtertype ;
      private String Ddo_checklist_descricao_Datalisttype ;
      private String Ddo_checklist_descricao_Datalistproc ;
      private String Ddo_checklist_descricao_Sortasc ;
      private String Ddo_checklist_descricao_Sortdsc ;
      private String Ddo_checklist_descricao_Loadingdata ;
      private String Ddo_checklist_descricao_Cleanfilter ;
      private String Ddo_checklist_descricao_Noresultsfound ;
      private String Ddo_checklist_descricao_Searchbuttontext ;
      private String Ddo_checklist_impeditivo_Caption ;
      private String Ddo_checklist_impeditivo_Tooltip ;
      private String Ddo_checklist_impeditivo_Cls ;
      private String Ddo_checklist_impeditivo_Selectedvalue_set ;
      private String Ddo_checklist_impeditivo_Dropdownoptionstype ;
      private String Ddo_checklist_impeditivo_Titlecontrolidtoreplace ;
      private String Ddo_checklist_impeditivo_Sortedstatus ;
      private String Ddo_checklist_impeditivo_Datalisttype ;
      private String Ddo_checklist_impeditivo_Datalistfixedvalues ;
      private String Ddo_checklist_impeditivo_Sortasc ;
      private String Ddo_checklist_impeditivo_Sortdsc ;
      private String Ddo_checklist_impeditivo_Cleanfilter ;
      private String Ddo_checklist_impeditivo_Searchbuttontext ;
      private String Ddo_checklist_senaocumpre_Caption ;
      private String Ddo_checklist_senaocumpre_Tooltip ;
      private String Ddo_checklist_senaocumpre_Cls ;
      private String Ddo_checklist_senaocumpre_Selectedvalue_set ;
      private String Ddo_checklist_senaocumpre_Dropdownoptionstype ;
      private String Ddo_checklist_senaocumpre_Titlecontrolidtoreplace ;
      private String Ddo_checklist_senaocumpre_Sortedstatus ;
      private String Ddo_checklist_senaocumpre_Datalisttype ;
      private String Ddo_checklist_senaocumpre_Datalistfixedvalues ;
      private String Ddo_checklist_senaocumpre_Sortasc ;
      private String Ddo_checklist_senaocumpre_Sortdsc ;
      private String Ddo_checklist_senaocumpre_Cleanfilter ;
      private String Ddo_checklist_senaocumpre_Searchbuttontext ;
      private String Ddo_checklist_naocnfqdo_Caption ;
      private String Ddo_checklist_naocnfqdo_Tooltip ;
      private String Ddo_checklist_naocnfqdo_Cls ;
      private String Ddo_checklist_naocnfqdo_Selectedvalue_set ;
      private String Ddo_checklist_naocnfqdo_Dropdownoptionstype ;
      private String Ddo_checklist_naocnfqdo_Titlecontrolidtoreplace ;
      private String Ddo_checklist_naocnfqdo_Sortedstatus ;
      private String Ddo_checklist_naocnfqdo_Datalisttype ;
      private String Ddo_checklist_naocnfqdo_Datalistfixedvalues ;
      private String Ddo_checklist_naocnfqdo_Sortasc ;
      private String Ddo_checklist_naocnfqdo_Sortdsc ;
      private String Ddo_checklist_naocnfqdo_Cleanfilter ;
      private String Ddo_checklist_naocnfqdo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfchecklist_descricao_Internalname ;
      private String edtavTfchecklist_descricao_sel_Internalname ;
      private String edtavTfchecklist_impeditivo_sel_Internalname ;
      private String edtavTfchecklist_impeditivo_sel_Jsonclick ;
      private String edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_impeditivotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_senaocumpretitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_naocnfqdotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtCheck_Codigo_Internalname ;
      private String edtCheckList_Codigo_Internalname ;
      private String edtCheckList_Descricao_Internalname ;
      private String cmbCheckList_Obrigatorio_Internalname ;
      private String cmbCheckList_Impeditivo_Internalname ;
      private String cmbCheckList_SeNaoCumpre_Internalname ;
      private String AV50NaoConformidade_Nome ;
      private String cmbCheckList_NaoCnfQdo_Internalname ;
      private String A1851CheckList_NaoCnfQdo ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_checklist_descricao_Internalname ;
      private String Ddo_checklist_impeditivo_Internalname ;
      private String Ddo_checklist_senaocumpre_Internalname ;
      private String Ddo_checklist_naocnfqdo_Internalname ;
      private String edtCheckList_Descricao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7Check_Codigo ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCheck_Codigo_Jsonclick ;
      private String edtCheckList_Codigo_Jsonclick ;
      private String edtCheckList_Descricao_Jsonclick ;
      private String cmbCheckList_Obrigatorio_Jsonclick ;
      private String cmbCheckList_Impeditivo_Jsonclick ;
      private String cmbCheckList_SeNaoCumpre_Jsonclick ;
      private String edtavNaoconformidade_nome_Jsonclick ;
      private String cmbCheckList_NaoCnfQdo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n1846CheckList_NaoCnfCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_checklist_descricao_Includesortasc ;
      private bool Ddo_checklist_descricao_Includesortdsc ;
      private bool Ddo_checklist_descricao_Includefilter ;
      private bool Ddo_checklist_descricao_Filterisrange ;
      private bool Ddo_checklist_descricao_Includedatalist ;
      private bool Ddo_checklist_impeditivo_Includesortasc ;
      private bool Ddo_checklist_impeditivo_Includesortdsc ;
      private bool Ddo_checklist_impeditivo_Includefilter ;
      private bool Ddo_checklist_impeditivo_Includedatalist ;
      private bool Ddo_checklist_senaocumpre_Includesortasc ;
      private bool Ddo_checklist_senaocumpre_Includesortdsc ;
      private bool Ddo_checklist_senaocumpre_Includefilter ;
      private bool Ddo_checklist_senaocumpre_Includedatalist ;
      private bool Ddo_checklist_senaocumpre_Allowmultipleselection ;
      private bool Ddo_checklist_naocnfqdo_Includesortasc ;
      private bool Ddo_checklist_naocnfqdo_Includesortdsc ;
      private bool Ddo_checklist_naocnfqdo_Includefilter ;
      private bool Ddo_checklist_naocnfqdo_Includedatalist ;
      private bool Ddo_checklist_naocnfqdo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1839Check_Codigo ;
      private bool A1845CheckList_Obrigatorio ;
      private bool n1845CheckList_Obrigatorio ;
      private bool A1850CheckList_Impeditivo ;
      private bool n1850CheckList_Impeditivo ;
      private bool n1848CheckList_SeNaoCumpre ;
      private bool n1851CheckList_NaoCnfQdo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV27Update_IsBlob ;
      private bool AV28Delete_IsBlob ;
      private String A763CheckList_Descricao ;
      private String AV36TFCheckList_SeNaoCumpre_SelsJson ;
      private String AV43TFCheckList_NaoCnfQdo_SelsJson ;
      private String AV17TFCheckList_Descricao ;
      private String AV18TFCheckList_Descricao_Sel ;
      private String AV19ddo_CheckList_DescricaoTitleControlIdToReplace ;
      private String AV41ddo_CheckList_ImpeditivoTitleControlIdToReplace ;
      private String AV38ddo_CheckList_SeNaoCumpreTitleControlIdToReplace ;
      private String AV45ddo_CheckList_NaoCnfQdoTitleControlIdToReplace ;
      private String AV53Update_GXI ;
      private String AV54Delete_GXI ;
      private String lV17TFCheckList_Descricao ;
      private String AV27Update ;
      private String AV28Delete ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbCheckList_Obrigatorio ;
      private GXCombobox cmbCheckList_Impeditivo ;
      private GXCombobox cmbCheckList_SeNaoCumpre ;
      private GXCombobox cmbCheckList_NaoCnfQdo ;
      private IDataStoreProvider pr_default ;
      private int[] H00O82_A1846CheckList_NaoCnfCod ;
      private bool[] H00O82_n1846CheckList_NaoCnfCod ;
      private String[] H00O82_A1851CheckList_NaoCnfQdo ;
      private bool[] H00O82_n1851CheckList_NaoCnfQdo ;
      private short[] H00O82_A1848CheckList_SeNaoCumpre ;
      private bool[] H00O82_n1848CheckList_SeNaoCumpre ;
      private bool[] H00O82_A1850CheckList_Impeditivo ;
      private bool[] H00O82_n1850CheckList_Impeditivo ;
      private bool[] H00O82_A1845CheckList_Obrigatorio ;
      private bool[] H00O82_n1845CheckList_Obrigatorio ;
      private String[] H00O82_A763CheckList_Descricao ;
      private int[] H00O82_A758CheckList_Codigo ;
      private int[] H00O82_A1839Check_Codigo ;
      private bool[] H00O82_n1839Check_Codigo ;
      private long[] H00O83_AGRID_nRecordCount ;
      private int[] H00O84_A426NaoConformidade_Codigo ;
      private String[] H00O84_A427NaoConformidade_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV37TFCheckList_SeNaoCumpre_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44TFCheckList_NaoCnfQdo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16CheckList_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39CheckList_ImpeditivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35CheckList_SeNaoCumpreTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42CheckList_NaoCnfQdoTitleFilterData ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV23DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class checklistitenswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00O82( IGxContext context ,
                                             short A1848CheckList_SeNaoCumpre ,
                                             IGxCollection AV37TFCheckList_SeNaoCumpre_Sels ,
                                             String A1851CheckList_NaoCnfQdo ,
                                             IGxCollection AV44TFCheckList_NaoCnfQdo_Sels ,
                                             String AV18TFCheckList_Descricao_Sel ,
                                             String AV17TFCheckList_Descricao ,
                                             short AV40TFCheckList_Impeditivo_Sel ,
                                             int AV37TFCheckList_SeNaoCumpre_Sels_Count ,
                                             int AV44TFCheckList_NaoCnfQdo_Sels_Count ,
                                             String A763CheckList_Descricao ,
                                             bool A1850CheckList_Impeditivo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1839Check_Codigo ,
                                             int AV7Check_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [CheckList_NaoCnfCod], [CheckList_NaoCnfQdo], [CheckList_SeNaoCumpre], [CheckList_Impeditivo], [CheckList_Obrigatorio], [CheckList_Descricao], [CheckList_Codigo], [Check_Codigo]";
         sFromString = " FROM [CheckList] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Check_Codigo] = @AV7Check_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCheckList_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFCheckList_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([CheckList_Descricao] like @lV17TFCheckList_Descricao)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCheckList_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([CheckList_Descricao] = @AV18TFCheckList_Descricao_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV40TFCheckList_Impeditivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([CheckList_Impeditivo] = 1)";
         }
         if ( AV40TFCheckList_Impeditivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([CheckList_Impeditivo] = 0)";
         }
         if ( AV37TFCheckList_SeNaoCumpre_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV37TFCheckList_SeNaoCumpre_Sels, "[CheckList_SeNaoCumpre] IN (", ")") + ")";
         }
         if ( AV44TFCheckList_NaoCnfQdo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44TFCheckList_NaoCnfQdo_Sels, "[CheckList_NaoCnfQdo] IN (", ")") + ")";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo], [CheckList_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo] DESC, [CheckList_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo], [CheckList_Impeditivo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo] DESC, [CheckList_Impeditivo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo], [CheckList_SeNaoCumpre]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo] DESC, [CheckList_SeNaoCumpre] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo], [CheckList_NaoCnfQdo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo] DESC, [CheckList_NaoCnfQdo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00O83( IGxContext context ,
                                             short A1848CheckList_SeNaoCumpre ,
                                             IGxCollection AV37TFCheckList_SeNaoCumpre_Sels ,
                                             String A1851CheckList_NaoCnfQdo ,
                                             IGxCollection AV44TFCheckList_NaoCnfQdo_Sels ,
                                             String AV18TFCheckList_Descricao_Sel ,
                                             String AV17TFCheckList_Descricao ,
                                             short AV40TFCheckList_Impeditivo_Sel ,
                                             int AV37TFCheckList_SeNaoCumpre_Sels_Count ,
                                             int AV44TFCheckList_NaoCnfQdo_Sels_Count ,
                                             String A763CheckList_Descricao ,
                                             bool A1850CheckList_Impeditivo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1839Check_Codigo ,
                                             int AV7Check_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [CheckList] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Check_Codigo] = @AV7Check_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCheckList_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFCheckList_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([CheckList_Descricao] like @lV17TFCheckList_Descricao)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCheckList_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([CheckList_Descricao] = @AV18TFCheckList_Descricao_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV40TFCheckList_Impeditivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([CheckList_Impeditivo] = 1)";
         }
         if ( AV40TFCheckList_Impeditivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([CheckList_Impeditivo] = 0)";
         }
         if ( AV37TFCheckList_SeNaoCumpre_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV37TFCheckList_SeNaoCumpre_Sels, "[CheckList_SeNaoCumpre] IN (", ")") + ")";
         }
         if ( AV44TFCheckList_NaoCnfQdo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44TFCheckList_NaoCnfQdo_Sels, "[CheckList_NaoCnfQdo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00O82(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (short)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
               case 1 :
                     return conditional_H00O83(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (short)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00O84 ;
          prmH00O84 = new Object[] {
          new Object[] {"@CheckList_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00O82 ;
          prmH00O82 = new Object[] {
          new Object[] {"@AV7Check_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17TFCheckList_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFCheckList_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00O83 ;
          prmH00O83 = new Object[] {
          new Object[] {"@AV7Check_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17TFCheckList_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFCheckList_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00O82", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O82,11,0,true,false )
             ,new CursorDef("H00O83", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O83,1,0,true,false )
             ,new CursorDef("H00O84", "SELECT TOP 1 [NaoConformidade_Codigo], [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @CheckList_NaoCnfCod ORDER BY [NaoConformidade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O84,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
