/*
               File: GetCheckListServicosWCFilterData
        Description: Get Check List Servicos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:13.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getchecklistservicoswcfilterdata : GXProcedure
   {
      public getchecklistservicoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getchecklistservicoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getchecklistservicoswcfilterdata objgetchecklistservicoswcfilterdata;
         objgetchecklistservicoswcfilterdata = new getchecklistservicoswcfilterdata();
         objgetchecklistservicoswcfilterdata.AV14DDOName = aP0_DDOName;
         objgetchecklistservicoswcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetchecklistservicoswcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetchecklistservicoswcfilterdata.AV18OptionsJson = "" ;
         objgetchecklistservicoswcfilterdata.AV21OptionsDescJson = "" ;
         objgetchecklistservicoswcfilterdata.AV23OptionIndexesJson = "" ;
         objgetchecklistservicoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetchecklistservicoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetchecklistservicoswcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getchecklistservicoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_SERVICO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("CheckListServicosWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "CheckListServicosWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("CheckListServicosWCGridState"), "");
         }
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV33GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA") == 0 )
            {
               AV10TFServico_Sigla = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA_SEL") == 0 )
            {
               AV11TFServico_Sigla_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&CHECK_CODIGO") == 0 )
            {
               AV30Check_Codigo = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV33GXV1 = (int)(AV33GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICO_SIGLAOPTIONS' Routine */
         AV10TFServico_Sigla = AV12SearchTxt;
         AV11TFServico_Sigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFServico_Sigla_Sel ,
                                              AV10TFServico_Sigla ,
                                              A605Servico_Sigla ,
                                              AV30Check_Codigo ,
                                              A1839Check_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Sigla), 15, "%");
         /* Using cursor P00T12 */
         pr_default.execute(0, new Object[] {AV30Check_Codigo, lV10TFServico_Sigla, AV11TFServico_Sigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKT12 = false;
            A155Servico_Codigo = P00T12_A155Servico_Codigo[0];
            A1839Check_Codigo = P00T12_A1839Check_Codigo[0];
            A605Servico_Sigla = P00T12_A605Servico_Sigla[0];
            A605Servico_Sigla = P00T12_A605Servico_Sigla[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00T12_A1839Check_Codigo[0] == A1839Check_Codigo ) && ( StringUtil.StrCmp(P00T12_A605Servico_Sigla[0], A605Servico_Sigla) == 0 ) )
            {
               BRKT12 = false;
               A155Servico_Codigo = P00T12_A155Servico_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKT12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
            {
               AV16Option = A605Servico_Sigla;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKT12 )
            {
               BRKT12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFServico_Sigla = "";
         AV11TFServico_Sigla_Sel = "";
         scmdbuf = "";
         lV10TFServico_Sigla = "";
         A605Servico_Sigla = "";
         P00T12_A155Servico_Codigo = new int[1] ;
         P00T12_A1839Check_Codigo = new int[1] ;
         P00T12_A605Servico_Sigla = new String[] {""} ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getchecklistservicoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00T12_A155Servico_Codigo, P00T12_A1839Check_Codigo, P00T12_A605Servico_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV33GXV1 ;
      private int AV30Check_Codigo ;
      private int A1839Check_Codigo ;
      private int A155Servico_Codigo ;
      private long AV24count ;
      private String AV10TFServico_Sigla ;
      private String AV11TFServico_Sigla_Sel ;
      private String scmdbuf ;
      private String lV10TFServico_Sigla ;
      private String A605Servico_Sigla ;
      private bool returnInSub ;
      private bool BRKT12 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00T12_A155Servico_Codigo ;
      private int[] P00T12_A1839Check_Codigo ;
      private String[] P00T12_A605Servico_Sigla ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getchecklistservicoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00T12( IGxContext context ,
                                             String AV11TFServico_Sigla_Sel ,
                                             String AV10TFServico_Sigla ,
                                             String A605Servico_Sigla ,
                                             int AV30Check_Codigo ,
                                             int A1839Check_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T1.[Check_Codigo], T2.[Servico_Sigla] FROM ([ServicoCheck] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Check_Codigo] = @AV30Check_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV10TFServico_Sigla)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV11TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Check_Codigo], T2.[Servico_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00T12(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T12 ;
          prmP00T12 = new Object[] {
          new Object[] {"@AV30Check_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11TFServico_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getchecklistservicoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getchecklistservicoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getchecklistservicoswcfilterdata") )
          {
             return  ;
          }
          getchecklistservicoswcfilterdata worker = new getchecklistservicoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
