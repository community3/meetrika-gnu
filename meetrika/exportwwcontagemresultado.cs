/*
               File: ExportWWContagemResultado
        Description: Export WWContagem Resultado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:58:39.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportwwcontagemresultado : GXProcedure
   {
      public exportwwcontagemresultado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportwwcontagemresultado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           String aP2_TFContratada_AreaTrabalhoDes ,
                           String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                           DateTime aP4_TFContagemResultado_DataDmn ,
                           DateTime aP5_TFContagemResultado_DataDmn_To ,
                           DateTime aP6_TFContagemResultado_DataUltCnt ,
                           DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                           DateTime aP8_TFContagemResultado_DataPrevista ,
                           DateTime aP9_TFContagemResultado_DataPrevista_To ,
                           String aP10_TFContagemResultado_OsFsOsFm ,
                           String aP11_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP12_TFContagemResultado_Descricao ,
                           String aP13_TFContagemResultado_Descricao_Sel ,
                           String aP14_TFContagemrResultado_SistemaSigla ,
                           String aP15_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP16_TFContagemResultado_ContratadaSigla ,
                           String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP18_TFContagemResultado_CntNum ,
                           String aP19_TFContagemResultado_CntNum_Sel ,
                           String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                           String aP21_TFContagemResultado_StatusUltCnt_SelsJson ,
                           short aP22_TFContagemResultado_Baseline_Sel ,
                           String aP23_TFContagemResultado_Servico ,
                           int aP24_TFContagemResultado_Servico_Sel ,
                           String aP25_TFContagemResultado_Servico_SelDsc ,
                           String aP26_TFContagemResultado_EsforcoTotal ,
                           String aP27_TFContagemResultado_EsforcoTotal_Sel ,
                           decimal aP28_TFContagemResultado_PFFinal ,
                           decimal aP29_TFContagemResultado_PFFinal_To ,
                           short aP30_OrderedBy ,
                           bool aP31_OrderedDsc ,
                           String aP32_GridStateXML ,
                           out String aP33_Filename ,
                           out String aP34_ErrorMessage )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV89Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV150TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         this.AV151TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         this.AV152TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV153TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV154TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV155TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV197TFContagemResultado_DataPrevista = aP8_TFContagemResultado_DataPrevista;
         this.AV198TFContagemResultado_DataPrevista_To = aP9_TFContagemResultado_DataPrevista_To;
         this.AV156TFContagemResultado_OsFsOsFm = aP10_TFContagemResultado_OsFsOsFm;
         this.AV157TFContagemResultado_OsFsOsFm_Sel = aP11_TFContagemResultado_OsFsOsFm_Sel;
         this.AV158TFContagemResultado_Descricao = aP12_TFContagemResultado_Descricao;
         this.AV159TFContagemResultado_Descricao_Sel = aP13_TFContagemResultado_Descricao_Sel;
         this.AV160TFContagemrResultado_SistemaSigla = aP14_TFContagemrResultado_SistemaSigla;
         this.AV161TFContagemrResultado_SistemaSigla_Sel = aP15_TFContagemrResultado_SistemaSigla_Sel;
         this.AV162TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         this.AV163TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         this.AV201TFContagemResultado_CntNum = aP18_TFContagemResultado_CntNum;
         this.AV202TFContagemResultado_CntNum_Sel = aP19_TFContagemResultado_CntNum_Sel;
         this.AV164TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         this.AV167TFContagemResultado_StatusUltCnt_SelsJson = aP21_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV170TFContagemResultado_Baseline_Sel = aP22_TFContagemResultado_Baseline_Sel;
         this.AV171TFContagemResultado_Servico = aP23_TFContagemResultado_Servico;
         this.AV172TFContagemResultado_Servico_Sel = aP24_TFContagemResultado_Servico_Sel;
         this.AV185TFContagemResultado_Servico_SelDsc = aP25_TFContagemResultado_Servico_SelDsc;
         this.AV181TFContagemResultado_EsforcoTotal = aP26_TFContagemResultado_EsforcoTotal;
         this.AV182TFContagemResultado_EsforcoTotal_Sel = aP27_TFContagemResultado_EsforcoTotal_Sel;
         this.AV183TFContagemResultado_PFFinal = aP28_TFContagemResultado_PFFinal;
         this.AV184TFContagemResultado_PFFinal_To = aP29_TFContagemResultado_PFFinal_To;
         this.AV16OrderedBy = aP30_OrderedBy;
         this.AV17OrderedDsc = aP31_OrderedDsc;
         this.AV57GridStateXML = aP32_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP33_Filename=this.AV11Filename;
         aP34_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_Contratada_Codigo ,
                                String aP2_TFContratada_AreaTrabalhoDes ,
                                String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                                DateTime aP4_TFContagemResultado_DataDmn ,
                                DateTime aP5_TFContagemResultado_DataDmn_To ,
                                DateTime aP6_TFContagemResultado_DataUltCnt ,
                                DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                DateTime aP8_TFContagemResultado_DataPrevista ,
                                DateTime aP9_TFContagemResultado_DataPrevista_To ,
                                String aP10_TFContagemResultado_OsFsOsFm ,
                                String aP11_TFContagemResultado_OsFsOsFm_Sel ,
                                String aP12_TFContagemResultado_Descricao ,
                                String aP13_TFContagemResultado_Descricao_Sel ,
                                String aP14_TFContagemrResultado_SistemaSigla ,
                                String aP15_TFContagemrResultado_SistemaSigla_Sel ,
                                String aP16_TFContagemResultado_ContratadaSigla ,
                                String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                                String aP18_TFContagemResultado_CntNum ,
                                String aP19_TFContagemResultado_CntNum_Sel ,
                                String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                                String aP21_TFContagemResultado_StatusUltCnt_SelsJson ,
                                short aP22_TFContagemResultado_Baseline_Sel ,
                                String aP23_TFContagemResultado_Servico ,
                                int aP24_TFContagemResultado_Servico_Sel ,
                                String aP25_TFContagemResultado_Servico_SelDsc ,
                                String aP26_TFContagemResultado_EsforcoTotal ,
                                String aP27_TFContagemResultado_EsforcoTotal_Sel ,
                                decimal aP28_TFContagemResultado_PFFinal ,
                                decimal aP29_TFContagemResultado_PFFinal_To ,
                                short aP30_OrderedBy ,
                                bool aP31_OrderedDsc ,
                                String aP32_GridStateXML ,
                                out String aP33_Filename )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV89Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV150TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         this.AV151TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         this.AV152TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV153TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV154TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV155TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV197TFContagemResultado_DataPrevista = aP8_TFContagemResultado_DataPrevista;
         this.AV198TFContagemResultado_DataPrevista_To = aP9_TFContagemResultado_DataPrevista_To;
         this.AV156TFContagemResultado_OsFsOsFm = aP10_TFContagemResultado_OsFsOsFm;
         this.AV157TFContagemResultado_OsFsOsFm_Sel = aP11_TFContagemResultado_OsFsOsFm_Sel;
         this.AV158TFContagemResultado_Descricao = aP12_TFContagemResultado_Descricao;
         this.AV159TFContagemResultado_Descricao_Sel = aP13_TFContagemResultado_Descricao_Sel;
         this.AV160TFContagemrResultado_SistemaSigla = aP14_TFContagemrResultado_SistemaSigla;
         this.AV161TFContagemrResultado_SistemaSigla_Sel = aP15_TFContagemrResultado_SistemaSigla_Sel;
         this.AV162TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         this.AV163TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         this.AV201TFContagemResultado_CntNum = aP18_TFContagemResultado_CntNum;
         this.AV202TFContagemResultado_CntNum_Sel = aP19_TFContagemResultado_CntNum_Sel;
         this.AV164TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         this.AV167TFContagemResultado_StatusUltCnt_SelsJson = aP21_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV170TFContagemResultado_Baseline_Sel = aP22_TFContagemResultado_Baseline_Sel;
         this.AV171TFContagemResultado_Servico = aP23_TFContagemResultado_Servico;
         this.AV172TFContagemResultado_Servico_Sel = aP24_TFContagemResultado_Servico_Sel;
         this.AV185TFContagemResultado_Servico_SelDsc = aP25_TFContagemResultado_Servico_SelDsc;
         this.AV181TFContagemResultado_EsforcoTotal = aP26_TFContagemResultado_EsforcoTotal;
         this.AV182TFContagemResultado_EsforcoTotal_Sel = aP27_TFContagemResultado_EsforcoTotal_Sel;
         this.AV183TFContagemResultado_PFFinal = aP28_TFContagemResultado_PFFinal;
         this.AV184TFContagemResultado_PFFinal_To = aP29_TFContagemResultado_PFFinal_To;
         this.AV16OrderedBy = aP30_OrderedBy;
         this.AV17OrderedDsc = aP31_OrderedDsc;
         this.AV57GridStateXML = aP32_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP33_Filename=this.AV11Filename;
         aP34_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 String aP2_TFContratada_AreaTrabalhoDes ,
                                 String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                                 DateTime aP4_TFContagemResultado_DataDmn ,
                                 DateTime aP5_TFContagemResultado_DataDmn_To ,
                                 DateTime aP6_TFContagemResultado_DataUltCnt ,
                                 DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                 DateTime aP8_TFContagemResultado_DataPrevista ,
                                 DateTime aP9_TFContagemResultado_DataPrevista_To ,
                                 String aP10_TFContagemResultado_OsFsOsFm ,
                                 String aP11_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP12_TFContagemResultado_Descricao ,
                                 String aP13_TFContagemResultado_Descricao_Sel ,
                                 String aP14_TFContagemrResultado_SistemaSigla ,
                                 String aP15_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP16_TFContagemResultado_ContratadaSigla ,
                                 String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP18_TFContagemResultado_CntNum ,
                                 String aP19_TFContagemResultado_CntNum_Sel ,
                                 String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                                 String aP21_TFContagemResultado_StatusUltCnt_SelsJson ,
                                 short aP22_TFContagemResultado_Baseline_Sel ,
                                 String aP23_TFContagemResultado_Servico ,
                                 int aP24_TFContagemResultado_Servico_Sel ,
                                 String aP25_TFContagemResultado_Servico_SelDsc ,
                                 String aP26_TFContagemResultado_EsforcoTotal ,
                                 String aP27_TFContagemResultado_EsforcoTotal_Sel ,
                                 decimal aP28_TFContagemResultado_PFFinal ,
                                 decimal aP29_TFContagemResultado_PFFinal_To ,
                                 short aP30_OrderedBy ,
                                 bool aP31_OrderedDsc ,
                                 String aP32_GridStateXML ,
                                 out String aP33_Filename ,
                                 out String aP34_ErrorMessage )
      {
         exportwwcontagemresultado objexportwwcontagemresultado;
         objexportwwcontagemresultado = new exportwwcontagemresultado();
         objexportwwcontagemresultado.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportwwcontagemresultado.AV89Contratada_Codigo = aP1_Contratada_Codigo;
         objexportwwcontagemresultado.AV150TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         objexportwwcontagemresultado.AV151TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         objexportwwcontagemresultado.AV152TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         objexportwwcontagemresultado.AV153TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         objexportwwcontagemresultado.AV154TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         objexportwwcontagemresultado.AV155TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         objexportwwcontagemresultado.AV197TFContagemResultado_DataPrevista = aP8_TFContagemResultado_DataPrevista;
         objexportwwcontagemresultado.AV198TFContagemResultado_DataPrevista_To = aP9_TFContagemResultado_DataPrevista_To;
         objexportwwcontagemresultado.AV156TFContagemResultado_OsFsOsFm = aP10_TFContagemResultado_OsFsOsFm;
         objexportwwcontagemresultado.AV157TFContagemResultado_OsFsOsFm_Sel = aP11_TFContagemResultado_OsFsOsFm_Sel;
         objexportwwcontagemresultado.AV158TFContagemResultado_Descricao = aP12_TFContagemResultado_Descricao;
         objexportwwcontagemresultado.AV159TFContagemResultado_Descricao_Sel = aP13_TFContagemResultado_Descricao_Sel;
         objexportwwcontagemresultado.AV160TFContagemrResultado_SistemaSigla = aP14_TFContagemrResultado_SistemaSigla;
         objexportwwcontagemresultado.AV161TFContagemrResultado_SistemaSigla_Sel = aP15_TFContagemrResultado_SistemaSigla_Sel;
         objexportwwcontagemresultado.AV162TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         objexportwwcontagemresultado.AV163TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         objexportwwcontagemresultado.AV201TFContagemResultado_CntNum = aP18_TFContagemResultado_CntNum;
         objexportwwcontagemresultado.AV202TFContagemResultado_CntNum_Sel = aP19_TFContagemResultado_CntNum_Sel;
         objexportwwcontagemresultado.AV164TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         objexportwwcontagemresultado.AV167TFContagemResultado_StatusUltCnt_SelsJson = aP21_TFContagemResultado_StatusUltCnt_SelsJson;
         objexportwwcontagemresultado.AV170TFContagemResultado_Baseline_Sel = aP22_TFContagemResultado_Baseline_Sel;
         objexportwwcontagemresultado.AV171TFContagemResultado_Servico = aP23_TFContagemResultado_Servico;
         objexportwwcontagemresultado.AV172TFContagemResultado_Servico_Sel = aP24_TFContagemResultado_Servico_Sel;
         objexportwwcontagemresultado.AV185TFContagemResultado_Servico_SelDsc = aP25_TFContagemResultado_Servico_SelDsc;
         objexportwwcontagemresultado.AV181TFContagemResultado_EsforcoTotal = aP26_TFContagemResultado_EsforcoTotal;
         objexportwwcontagemresultado.AV182TFContagemResultado_EsforcoTotal_Sel = aP27_TFContagemResultado_EsforcoTotal_Sel;
         objexportwwcontagemresultado.AV183TFContagemResultado_PFFinal = aP28_TFContagemResultado_PFFinal;
         objexportwwcontagemresultado.AV184TFContagemResultado_PFFinal_To = aP29_TFContagemResultado_PFFinal_To;
         objexportwwcontagemresultado.AV16OrderedBy = aP30_OrderedBy;
         objexportwwcontagemresultado.AV17OrderedDsc = aP31_OrderedDsc;
         objexportwwcontagemresultado.AV57GridStateXML = aP32_GridStateXML;
         objexportwwcontagemresultado.AV11Filename = "" ;
         objexportwwcontagemresultado.AV12ErrorMessage = "" ;
         objexportwwcontagemresultado.context.SetSubmitInitialConfig(context);
         objexportwwcontagemresultado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportwwcontagemresultado);
         aP33_Filename=this.AV11Filename;
         aP34_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportwwcontagemresultado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         if ( false )
         {
            AV15Random = (int)(NumberUtil.Random( )*10000);
            AV11Filename = "ExportWWContagemResultado-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
            AV10ExcelDocument.Open(AV11Filename);
            /* Execute user subroutine: 'CHECKSTATUS' */
            S121 ();
            if (returnInSub) return;
            AV10ExcelDocument.Clear();
         }
         AV142Codigos.FromXml(AV91WebSession.Get("Codigos"), "Collection");
         AV90Contratadas.FromXml(AV91WebSession.Get("Contratadas"), "Collection");
         AV72SDT_FiltroConsContadorFM.FromXml(AV91WebSession.Get("FiltroConsultaContador"), "");
         if ( (0==AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod) )
         {
            AV200AreaTrabalho_Codigo = AV9WWPContext.gxTpr_Areatrabalho_codigo;
         }
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "PublicTempStorage\\ExportWWContagemResultado-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Resultado das Contagens";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         if ( (0==AV18Contratada_AreaTrabalhoCod) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Todas";
         }
         if ( ! ( (0==AV18Contratada_AreaTrabalhoCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV18Contratada_AreaTrabalhoCod;
         }
         if ( ! ( (0==AV89Contratada_Codigo) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Contratada";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV89Contratada_Codigo;
         }
         AV58GridState.gxTpr_Dynamicfilters.FromXml(AV57GridStateXML, "");
         if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(1));
            AV19DynamicFiltersSelector1 = AV59GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV20DynamicFiltersOperator1 = AV59GridStateDynamicFilter.gxTpr_Operator;
               AV21ContagemResultado_OsFsOsFm1 = AV59GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContagemResultado_OsFsOsFm1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV20DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV21ContagemResultado_OsFsOsFm1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV22ContagemResultado_DataDmn1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
               AV23ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV22ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV23ContagemResultado_DataDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV22ContagemResultado_DataDmn1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV23ContagemResultado_DataDmn_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV73ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
               AV74ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV73ContagemResultado_DataUltCnt1) || ! (DateTime.MinValue==AV74ContagemResultado_DataUltCnt_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV73ContagemResultado_DataUltCnt1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV74ContagemResultado_DataUltCnt_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV187ContagemResultado_DataPrevista1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
               AV188ContagemResultado_DataPrevista_To1 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV187ContagemResultado_DataPrevista1) || ! (DateTime.MinValue==AV188ContagemResultado_DataPrevista_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV187ContagemResultado_DataPrevista1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV188ContagemResultado_DataPrevista_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV27ContagemResultado_StatusDmn1 = AV59GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ContagemResultado_StatusDmn1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ContagemResultado_StatusDmn1)) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV27ContagemResultado_StatusDmn1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "OUTROSSTATUS") == 0 )
            {
               AV93OutrosStatus1 = AV59GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93OutrosStatus1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda +";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV93OutrosStatus1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
            {
               AV79ContagemResultado_StatusUltCnt1 = (short)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV79ContagemResultado_StatusUltCnt1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! (0==AV79ContagemResultado_StatusUltCnt1) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV79ContagemResultado_StatusUltCnt1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV69ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV69ContagemResultado_Servico1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV69ContagemResultado_Servico1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 )
            {
               AV204ContagemResultado_CntSrvPrrCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV204ContagemResultado_CntSrvPrrCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prioridade";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV204ContagemResultado_CntSrvPrrCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV60ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV60ContagemResultado_ContadorFM1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV60ContagemResultado_ContadorFM1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV24ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV24ContagemResultado_SistemaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV24ContagemResultado_SistemaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV25ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV25ContagemResultado_ContratadaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV25ContagemResultado_ContratadaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
            {
               AV85ContagemResultado_ContratadaOrigemCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV85ContagemResultado_ContratadaOrigemCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV85ContagemResultado_ContratadaOrigemCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV26ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV26ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV26ContagemResultado_NaoCnfDmnCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV63ContagemResultado_Baseline1 = AV59GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_Baseline1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV63ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV63ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
            {
               AV20DynamicFiltersOperator1 = AV59GridStateDynamicFilter.gxTpr_Operator;
               AV30ContagemResultado_EsforcoSoma1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV30ContagemResultado_EsforcoSoma1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV20DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                  }
                  else if ( AV20DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV30ContagemResultado_EsforcoSoma1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV82ContagemResultado_Agrupador1 = AV59GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ContagemResultado_Agrupador1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV82ContagemResultado_Agrupador1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV136ContagemResultado_Descricao1 = AV59GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV136ContagemResultado_Descricao1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV136ContagemResultado_Descricao1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV143ContagemResultado_Codigo1 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV143ContagemResultado_Codigo1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "ID";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV143ContagemResultado_Codigo1;
               }
            }
            if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV31DynamicFiltersEnabled2 = true;
               AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(2));
               AV32DynamicFiltersSelector2 = AV59GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV33DynamicFiltersOperator2 = AV59GridStateDynamicFilter.gxTpr_Operator;
                  AV34ContagemResultado_OsFsOsFm2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_OsFsOsFm2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV33DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV33DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV33DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV34ContagemResultado_OsFsOsFm2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV35ContagemResultado_DataDmn2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                  AV36ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV35ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV36ContagemResultado_DataDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV35ContagemResultado_DataDmn2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV36ContagemResultado_DataDmn_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV75ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                  AV76ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV75ContagemResultado_DataUltCnt2) || ! (DateTime.MinValue==AV76ContagemResultado_DataUltCnt_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV75ContagemResultado_DataUltCnt2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV76ContagemResultado_DataUltCnt_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV189ContagemResultado_DataPrevista2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                  AV190ContagemResultado_DataPrevista_To2 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV189ContagemResultado_DataPrevista2) || ! (DateTime.MinValue==AV190ContagemResultado_DataPrevista_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV189ContagemResultado_DataPrevista2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV190ContagemResultado_DataPrevista_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV40ContagemResultado_StatusDmn2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContagemResultado_StatusDmn2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40ContagemResultado_StatusDmn2)) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV40ContagemResultado_StatusDmn2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "OUTROSSTATUS") == 0 )
               {
                  AV94OutrosStatus2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94OutrosStatus2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda +";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV94OutrosStatus2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
               {
                  AV80ContagemResultado_StatusUltCnt2 = (short)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV80ContagemResultado_StatusUltCnt2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! (0==AV80ContagemResultado_StatusUltCnt2) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV80ContagemResultado_StatusUltCnt2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV70ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV70ContagemResultado_Servico2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV70ContagemResultado_Servico2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 )
               {
                  AV205ContagemResultado_CntSrvPrrCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV205ContagemResultado_CntSrvPrrCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prioridade";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV205ContagemResultado_CntSrvPrrCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV61ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV61ContagemResultado_ContadorFM2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV61ContagemResultado_ContadorFM2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV37ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV37ContagemResultado_SistemaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV37ContagemResultado_SistemaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV38ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV38ContagemResultado_ContratadaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV38ContagemResultado_ContratadaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
               {
                  AV86ContagemResultado_ContratadaOrigemCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV86ContagemResultado_ContratadaOrigemCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV86ContagemResultado_ContratadaOrigemCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV39ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV39ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV39ContagemResultado_NaoCnfDmnCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV64ContagemResultado_Baseline2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64ContagemResultado_Baseline2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV64ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV64ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
               {
                  AV33DynamicFiltersOperator2 = AV59GridStateDynamicFilter.gxTpr_Operator;
                  AV43ContagemResultado_EsforcoSoma2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV43ContagemResultado_EsforcoSoma2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV33DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                     }
                     else if ( AV33DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                     }
                     else if ( AV33DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV43ContagemResultado_EsforcoSoma2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV83ContagemResultado_Agrupador2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83ContagemResultado_Agrupador2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV83ContagemResultado_Agrupador2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV137ContagemResultado_Descricao2 = AV59GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137ContagemResultado_Descricao2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV137ContagemResultado_Descricao2;
                  }
               }
               else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector2, "CONTAGEMRESULTADO_CODIGO") == 0 )
               {
                  AV144ContagemResultado_Codigo2 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV144ContagemResultado_Codigo2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "ID";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV144ContagemResultado_Codigo2;
                  }
               }
               if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV59GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV46DynamicFiltersOperator3 = AV59GridStateDynamicFilter.gxTpr_Operator;
                     AV47ContagemResultado_OsFsOsFm3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContagemResultado_OsFsOsFm3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV46DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV46DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV46DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV47ContagemResultado_OsFsOsFm3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV48ContagemResultado_DataDmn3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                     AV49ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV49ContagemResultado_DataDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV48ContagemResultado_DataDmn3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV49ContagemResultado_DataDmn_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV77ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                     AV78ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV77ContagemResultado_DataUltCnt3) || ! (DateTime.MinValue==AV78ContagemResultado_DataUltCnt_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV77ContagemResultado_DataUltCnt3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV78ContagemResultado_DataUltCnt_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV191ContagemResultado_DataPrevista3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                     AV192ContagemResultado_DataPrevista_To3 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV191ContagemResultado_DataPrevista3) || ! (DateTime.MinValue==AV192ContagemResultado_DataPrevista_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV191ContagemResultado_DataPrevista3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV192ContagemResultado_DataPrevista_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV53ContagemResultado_StatusDmn3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_StatusDmn3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_StatusDmn3)) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV53ContagemResultado_StatusDmn3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "OUTROSSTATUS") == 0 )
                  {
                     AV95OutrosStatus3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95OutrosStatus3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda +";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV95OutrosStatus3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
                  {
                     AV81ContagemResultado_StatusUltCnt3 = (short)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV81ContagemResultado_StatusUltCnt3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! (0==AV81ContagemResultado_StatusUltCnt3) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV81ContagemResultado_StatusUltCnt3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV71ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV71ContagemResultado_Servico3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV71ContagemResultado_Servico3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 )
                  {
                     AV206ContagemResultado_CntSrvPrrCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV206ContagemResultado_CntSrvPrrCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prioridade";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV206ContagemResultado_CntSrvPrrCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV62ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV62ContagemResultado_ContadorFM3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV62ContagemResultado_ContadorFM3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV50ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV50ContagemResultado_SistemaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV50ContagemResultado_SistemaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV51ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV51ContagemResultado_ContratadaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV51ContagemResultado_ContratadaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                  {
                     AV87ContagemResultado_ContratadaOrigemCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV87ContagemResultado_ContratadaOrigemCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV87ContagemResultado_ContratadaOrigemCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV52ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV52ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV52ContagemResultado_NaoCnfDmnCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV65ContagemResultado_Baseline3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65ContagemResultado_Baseline3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV65ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV65ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                  {
                     AV46DynamicFiltersOperator3 = AV59GridStateDynamicFilter.gxTpr_Operator;
                     AV56ContagemResultado_EsforcoSoma3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV56ContagemResultado_EsforcoSoma3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV46DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                        }
                        else if ( AV46DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                        }
                        else if ( AV46DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV56ContagemResultado_EsforcoSoma3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV84ContagemResultado_Agrupador3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84ContagemResultado_Agrupador3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV84ContagemResultado_Agrupador3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV138ContagemResultado_Descricao3 = AV59GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV138ContagemResultado_Descricao3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV138ContagemResultado_Descricao3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CONTAGEMRESULTADO_CODIGO") == 0 )
                  {
                     AV145ContagemResultado_Codigo3 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV145ContagemResultado_Codigo3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "ID";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV145ContagemResultado_Codigo3;
                     }
                  }
                  if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV96DynamicFiltersEnabled4 = true;
                     AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(4));
                     AV97DynamicFiltersSelector4 = AV59GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV98DynamicFiltersOperator4 = AV59GridStateDynamicFilter.gxTpr_Operator;
                        AV99ContagemResultado_OsFsOsFm4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99ContagemResultado_OsFsOsFm4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           if ( AV98DynamicFiltersOperator4 == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                           }
                           else if ( AV98DynamicFiltersOperator4 == 1 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                           }
                           else if ( AV98DynamicFiltersOperator4 == 2 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                           }
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV99ContagemResultado_OsFsOsFm4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV100ContagemResultado_DataDmn4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                        AV101ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV100ContagemResultado_DataDmn4) || ! (DateTime.MinValue==AV101ContagemResultado_DataDmn_To4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV100ContagemResultado_DataDmn4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV101ContagemResultado_DataDmn_To4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                     {
                        AV102ContagemResultado_DataUltCnt4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                        AV103ContagemResultado_DataUltCnt_To4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV102ContagemResultado_DataUltCnt4) || ! (DateTime.MinValue==AV103ContagemResultado_DataUltCnt_To4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV102ContagemResultado_DataUltCnt4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV103ContagemResultado_DataUltCnt_To4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV193ContagemResultado_DataPrevista4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                        AV194ContagemResultado_DataPrevista_To4 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV193ContagemResultado_DataPrevista4) || ! (DateTime.MinValue==AV194ContagemResultado_DataPrevista_To4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV193ContagemResultado_DataPrevista4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           GXt_dtime1 = DateTimeUtil.ResetTime( AV194ContagemResultado_DataPrevista_To4 ) ;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV104ContagemResultado_StatusDmn4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ContagemResultado_StatusDmn4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ContagemResultado_StatusDmn4)) )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV104ContagemResultado_StatusDmn4);
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "OUTROSSTATUS") == 0 )
                     {
                        AV105OutrosStatus4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105OutrosStatus4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda +";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV105OutrosStatus4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
                     {
                        AV106ContagemResultado_StatusUltCnt4 = (short)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV106ContagemResultado_StatusUltCnt4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           if ( ! (0==AV106ContagemResultado_StatusUltCnt4) )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV106ContagemResultado_StatusUltCnt4);
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV107ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV107ContagemResultado_Servico4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV107ContagemResultado_Servico4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 )
                     {
                        AV207ContagemResultado_CntSrvPrrCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV207ContagemResultado_CntSrvPrrCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prioridade";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV207ContagemResultado_CntSrvPrrCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV108ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV108ContagemResultado_ContadorFM4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV108ContagemResultado_ContadorFM4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV109ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV109ContagemResultado_SistemaCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV109ContagemResultado_SistemaCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV110ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV110ContagemResultado_ContratadaCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV110ContagemResultado_ContratadaCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                     {
                        AV111ContagemResultado_ContratadaOrigemCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV111ContagemResultado_ContratadaOrigemCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV111ContagemResultado_ContratadaOrigemCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV112ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV112ContagemResultado_NaoCnfDmnCod4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV112ContagemResultado_NaoCnfDmnCod4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV113ContagemResultado_Baseline4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ContagemResultado_Baseline4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                           if ( StringUtil.StrCmp(StringUtil.Trim( AV113ContagemResultado_Baseline4), "S") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV113ContagemResultado_Baseline4), "N") == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                     {
                        AV98DynamicFiltersOperator4 = AV59GridStateDynamicFilter.gxTpr_Operator;
                        AV114ContagemResultado_EsforcoSoma4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV114ContagemResultado_EsforcoSoma4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           if ( AV98DynamicFiltersOperator4 == 0 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                           }
                           else if ( AV98DynamicFiltersOperator4 == 1 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                           }
                           else if ( AV98DynamicFiltersOperator4 == 2 )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                           }
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV114ContagemResultado_EsforcoSoma4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV115ContagemResultado_Agrupador4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115ContagemResultado_Agrupador4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV115ContagemResultado_Agrupador4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV139ContagemResultado_Descricao4 = AV59GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139ContagemResultado_Descricao4)) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV139ContagemResultado_Descricao4;
                        }
                     }
                     else if ( StringUtil.StrCmp(AV97DynamicFiltersSelector4, "CONTAGEMRESULTADO_CODIGO") == 0 )
                     {
                        AV146ContagemResultado_Codigo4 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV146ContagemResultado_Codigo4) )
                        {
                           AV13CellRow = (int)(AV13CellRow+1);
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "ID";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV146ContagemResultado_Codigo4;
                        }
                     }
                     if ( AV58GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV116DynamicFiltersEnabled5 = true;
                        AV59GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV58GridState.gxTpr_Dynamicfilters.Item(5));
                        AV117DynamicFiltersSelector5 = AV59GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV118DynamicFiltersOperator5 = AV59GridStateDynamicFilter.gxTpr_Operator;
                           AV119ContagemResultado_OsFsOsFm5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119ContagemResultado_OsFsOsFm5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              if ( AV118DynamicFiltersOperator5 == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                              }
                              else if ( AV118DynamicFiltersOperator5 == 1 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                              }
                              else if ( AV118DynamicFiltersOperator5 == 2 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                              }
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV119ContagemResultado_OsFsOsFm5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV120ContagemResultado_DataDmn5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                           AV121ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV120ContagemResultado_DataDmn5) || ! (DateTime.MinValue==AV121ContagemResultado_DataDmn_To5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV120ContagemResultado_DataDmn5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV121ContagemResultado_DataDmn_To5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                        {
                           AV122ContagemResultado_DataUltCnt5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                           AV123ContagemResultado_DataUltCnt_To5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV122ContagemResultado_DataUltCnt5) || ! (DateTime.MinValue==AV123ContagemResultado_DataUltCnt_To5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV122ContagemResultado_DataUltCnt5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV123ContagemResultado_DataUltCnt_To5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV195ContagemResultado_DataPrevista5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Value, 2);
                           AV196ContagemResultado_DataPrevista_To5 = context.localUtil.CToD( AV59GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV195ContagemResultado_DataPrevista5) || ! (DateTime.MinValue==AV196ContagemResultado_DataPrevista_To5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Prevista";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV195ContagemResultado_DataPrevista5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV196ContagemResultado_DataPrevista_To5 ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV124ContagemResultado_StatusDmn5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124ContagemResultado_StatusDmn5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124ContagemResultado_StatusDmn5)) )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV124ContagemResultado_StatusDmn5);
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "OUTROSSTATUS") == 0 )
                        {
                           AV125OutrosStatus5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125OutrosStatus5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Demanda +";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV125OutrosStatus5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
                        {
                           AV126ContagemResultado_StatusUltCnt5 = (short)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV126ContagemResultado_StatusUltCnt5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              if ( ! (0==AV126ContagemResultado_StatusUltCnt5) )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV126ContagemResultado_StatusUltCnt5);
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV127ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV127ContagemResultado_Servico5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV127ContagemResultado_Servico5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 )
                        {
                           AV208ContagemResultado_CntSrvPrrCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV208ContagemResultado_CntSrvPrrCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prioridade";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV208ContagemResultado_CntSrvPrrCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV128ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV128ContagemResultado_ContadorFM5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV128ContagemResultado_ContadorFM5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV129ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV129ContagemResultado_SistemaCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV129ContagemResultado_SistemaCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV130ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV130ContagemResultado_ContratadaCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV130ContagemResultado_ContratadaCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                        {
                           AV131ContagemResultado_ContratadaOrigemCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV131ContagemResultado_ContratadaOrigemCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV131ContagemResultado_ContratadaOrigemCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV132ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV132ContagemResultado_NaoCnfDmnCod5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV132ContagemResultado_NaoCnfDmnCod5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV133ContagemResultado_Baseline5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV133ContagemResultado_Baseline5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                              if ( StringUtil.StrCmp(StringUtil.Trim( AV133ContagemResultado_Baseline5), "S") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV133ContagemResultado_Baseline5), "N") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                        {
                           AV118DynamicFiltersOperator5 = AV59GridStateDynamicFilter.gxTpr_Operator;
                           AV134ContagemResultado_EsforcoSoma5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV134ContagemResultado_EsforcoSoma5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              if ( AV118DynamicFiltersOperator5 == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (>)";
                              }
                              else if ( AV118DynamicFiltersOperator5 == 1 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (<)";
                              }
                              else if ( AV118DynamicFiltersOperator5 == 2 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o Demanda (=)";
                              }
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV134ContagemResultado_EsforcoSoma5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV135ContagemResultado_Agrupador5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135ContagemResultado_Agrupador5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV135ContagemResultado_Agrupador5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV140ContagemResultado_Descricao5 = AV59GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140ContagemResultado_Descricao5)) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV140ContagemResultado_Descricao5;
                           }
                        }
                        else if ( StringUtil.StrCmp(AV117DynamicFiltersSelector5, "CONTAGEMRESULTADO_CODIGO") == 0 )
                        {
                           AV147ContagemResultado_Codigo5 = (int)(NumberUtil.Val( AV59GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV147ContagemResultado_Codigo5) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "ID";
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV147ContagemResultado_Codigo5;
                           }
                        }
                     }
                  }
               }
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV151TFContratada_AreaTrabalhoDes_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "�rea";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV151TFContratada_AreaTrabalhoDes_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV150TFContratada_AreaTrabalhoDes)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "�rea";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV150TFContratada_AreaTrabalhoDes;
            }
         }
         if ( ! ( (DateTime.MinValue==AV152TFContagemResultado_DataDmn) && (DateTime.MinValue==AV153TFContagemResultado_DataDmn_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Demanda";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV152TFContagemResultado_DataDmn ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV153TFContagemResultado_DataDmn_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( (DateTime.MinValue==AV154TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV155TFContagemResultado_DataUltCnt_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contagem";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV154TFContagemResultado_DataUltCnt ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV155TFContagemResultado_DataUltCnt_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( (DateTime.MinValue==AV197TFContagemResultado_DataPrevista) && (DateTime.MinValue==AV198TFContagemResultado_DataPrevista_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Previs�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV197TFContagemResultado_DataPrevista;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV198TFContagemResultado_DataPrevista_To;
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV157TFContagemResultado_OsFsOsFm_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV157TFContagemResultado_OsFsOsFm_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV156TFContagemResultado_OsFsOsFm)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV156TFContagemResultado_OsFsOsFm;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV159TFContagemResultado_Descricao_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV159TFContagemResultado_Descricao_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV158TFContagemResultado_Descricao)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV158TFContagemResultado_Descricao;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV161TFContagemrResultado_SistemaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV161TFContagemrResultado_SistemaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV160TFContagemrResultado_SistemaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV160TFContagemrResultado_SistemaSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV163TFContagemResultado_ContratadaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV163TFContagemResultado_ContratadaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV162TFContagemResultado_ContratadaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV162TFContagemResultado_ContratadaSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV202TFContagemResultado_CntNum_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV202TFContagemResultado_CntNum_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV201TFContagemResultado_CntNum)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contrato";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV201TFContagemResultado_CntNum;
            }
         }
         AV165TFContagemResultado_StatusDmn_Sels.FromJSonString(AV164TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( ( AV165TFContagemResultado_StatusDmn_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Dmn";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV186i = 1;
            AV211GXV1 = 1;
            while ( AV211GXV1 <= AV165TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV166TFContagemResultado_StatusDmn_Sel = AV165TFContagemResultado_StatusDmn_Sels.GetString(AV211GXV1);
               if ( AV186i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatusdemanda.getDescription(context,AV166TFContagemResultado_StatusDmn_Sel);
               AV186i = (long)(AV186i+1);
               AV211GXV1 = (int)(AV211GXV1+1);
            }
         }
         AV168TFContagemResultado_StatusUltCnt_Sels.FromJSonString(AV167TFContagemResultado_StatusUltCnt_SelsJson);
         if ( ! ( ( AV168TFContagemResultado_StatusUltCnt_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Cnt";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV186i = 1;
            AV212GXV2 = 1;
            while ( AV212GXV2 <= AV168TFContagemResultado_StatusUltCnt_Sels.Count )
            {
               AV169TFContagemResultado_StatusUltCnt_Sel = (short)(AV168TFContagemResultado_StatusUltCnt_Sels.GetNumeric(AV212GXV2));
               if ( AV186i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatuscontagem.getDescription(context,AV169TFContagemResultado_StatusUltCnt_Sel);
               AV186i = (long)(AV186i+1);
               AV212GXV2 = (int)(AV212GXV2+1);
            }
         }
         if ( ! ( (0==AV170TFContagemResultado_Baseline_Sel) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "BS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( AV170TFContagemResultado_Baseline_Sel == 1 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Marcado";
            }
            else if ( AV170TFContagemResultado_Baseline_Sel == 2 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Desmarcado";
            }
         }
         if ( ! ( (0==AV172TFContagemResultado_Servico_Sel) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV185TFContagemResultado_Servico_SelDsc;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV171TFContagemResultado_Servico)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV171TFContagemResultado_Servico;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV182TFContagemResultado_EsforcoTotal_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV182TFContagemResultado_EsforcoTotal_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV181TFContagemResultado_EsforcoTotal)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV181TFContagemResultado_EsforcoTotal;
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV183TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV184TFContagemResultado_PFFinal_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PF Final";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV183TFContagemResultado_PFFinal);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV184TFContagemResultado_PFFinal_To);
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Demanda";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "Contagem";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "Previs�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "OS Ref|OS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "Titulo";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "Sistema";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "Prestadora";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "Contrato";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "Status Dmn";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "Status Cnt";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "BS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = "Servi�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = "Esfor�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Text = "PF Final";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Text = "Bruto";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+15, 1, 1).Text = "Liquido";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+16, 1, 1).Text = "Final";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV88Usuario_EhGestor = (bool)(AV9WWPContext.gxTpr_Userehadministradorgam||AV9WWPContext.gxTpr_Userehgestor);
         AV203Diferenca = (short)((0-AV13CellRow)*-1);
         AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod = AV18Contratada_AreaTrabalhoCod;
         AV215WWContagemResultadoDS_2_Contratada_codigo = AV89Contratada_Codigo;
         AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 = AV19DynamicFiltersSelector1;
         AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 = AV20DynamicFiltersOperator1;
         AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = AV21ContagemResultado_OsFsOsFm1;
         AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1 = AV22ContagemResultado_DataDmn1;
         AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 = AV23ContagemResultado_DataDmn_To1;
         AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 = AV73ContagemResultado_DataUltCnt1;
         AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 = AV74ContagemResultado_DataUltCnt_To1;
         AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1 = AV187ContagemResultado_DataPrevista1;
         AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 = AV188ContagemResultado_DataPrevista_To1;
         AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1 = AV27ContagemResultado_StatusDmn1;
         AV226WWContagemResultadoDS_13_Outrosstatus1 = AV93OutrosStatus1;
         AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 = AV79ContagemResultado_StatusUltCnt1;
         AV228WWContagemResultadoDS_15_Contagemresultado_servico1 = AV69ContagemResultado_Servico1;
         AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 = AV204ContagemResultado_CntSrvPrrCod1;
         AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1 = AV60ContagemResultado_ContadorFM1;
         AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1 = AV25ContagemResultado_ContratadaCod1;
         AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 = AV85ContagemResultado_ContratadaOrigemCod1;
         AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 = AV26ContagemResultado_NaoCnfDmnCod1;
         AV235WWContagemResultadoDS_22_Contagemresultado_baseline1 = AV63ContagemResultado_Baseline1;
         AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 = AV30ContagemResultado_EsforcoSoma1;
         AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1 = AV82ContagemResultado_Agrupador1;
         AV238WWContagemResultadoDS_25_Contagemresultado_descricao1 = AV136ContagemResultado_Descricao1;
         AV239WWContagemResultadoDS_26_Contagemresultado_codigo1 = AV143ContagemResultado_Codigo1;
         AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 = AV31DynamicFiltersEnabled2;
         AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 = AV32DynamicFiltersSelector2;
         AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 = AV33DynamicFiltersOperator2;
         AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = AV34ContagemResultado_OsFsOsFm2;
         AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2 = AV35ContagemResultado_DataDmn2;
         AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 = AV36ContagemResultado_DataDmn_To2;
         AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 = AV75ContagemResultado_DataUltCnt2;
         AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 = AV76ContagemResultado_DataUltCnt_To2;
         AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2 = AV189ContagemResultado_DataPrevista2;
         AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 = AV190ContagemResultado_DataPrevista_To2;
         AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2 = AV40ContagemResultado_StatusDmn2;
         AV251WWContagemResultadoDS_38_Outrosstatus2 = AV94OutrosStatus2;
         AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 = AV80ContagemResultado_StatusUltCnt2;
         AV253WWContagemResultadoDS_40_Contagemresultado_servico2 = AV70ContagemResultado_Servico2;
         AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 = AV205ContagemResultado_CntSrvPrrCod2;
         AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2 = AV61ContagemResultado_ContadorFM2;
         AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2 = AV38ContagemResultado_ContratadaCod2;
         AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 = AV86ContagemResultado_ContratadaOrigemCod2;
         AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 = AV39ContagemResultado_NaoCnfDmnCod2;
         AV260WWContagemResultadoDS_47_Contagemresultado_baseline2 = AV64ContagemResultado_Baseline2;
         AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 = AV43ContagemResultado_EsforcoSoma2;
         AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2 = AV83ContagemResultado_Agrupador2;
         AV263WWContagemResultadoDS_50_Contagemresultado_descricao2 = AV137ContagemResultado_Descricao2;
         AV264WWContagemResultadoDS_51_Contagemresultado_codigo2 = AV144ContagemResultado_Codigo2;
         AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 = AV46DynamicFiltersOperator3;
         AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = AV47ContagemResultado_OsFsOsFm3;
         AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3 = AV48ContagemResultado_DataDmn3;
         AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 = AV49ContagemResultado_DataDmn_To3;
         AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 = AV77ContagemResultado_DataUltCnt3;
         AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 = AV78ContagemResultado_DataUltCnt_To3;
         AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3 = AV191ContagemResultado_DataPrevista3;
         AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 = AV192ContagemResultado_DataPrevista_To3;
         AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3 = AV53ContagemResultado_StatusDmn3;
         AV276WWContagemResultadoDS_63_Outrosstatus3 = AV95OutrosStatus3;
         AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 = AV81ContagemResultado_StatusUltCnt3;
         AV278WWContagemResultadoDS_65_Contagemresultado_servico3 = AV71ContagemResultado_Servico3;
         AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 = AV206ContagemResultado_CntSrvPrrCod3;
         AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3 = AV62ContagemResultado_ContadorFM3;
         AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3 = AV51ContagemResultado_ContratadaCod3;
         AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 = AV87ContagemResultado_ContratadaOrigemCod3;
         AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 = AV52ContagemResultado_NaoCnfDmnCod3;
         AV285WWContagemResultadoDS_72_Contagemresultado_baseline3 = AV65ContagemResultado_Baseline3;
         AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 = AV56ContagemResultado_EsforcoSoma3;
         AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3 = AV84ContagemResultado_Agrupador3;
         AV288WWContagemResultadoDS_75_Contagemresultado_descricao3 = AV138ContagemResultado_Descricao3;
         AV289WWContagemResultadoDS_76_Contagemresultado_codigo3 = AV145ContagemResultado_Codigo3;
         AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 = AV96DynamicFiltersEnabled4;
         AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 = AV97DynamicFiltersSelector4;
         AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 = AV98DynamicFiltersOperator4;
         AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = AV99ContagemResultado_OsFsOsFm4;
         AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4 = AV100ContagemResultado_DataDmn4;
         AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 = AV101ContagemResultado_DataDmn_To4;
         AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 = AV102ContagemResultado_DataUltCnt4;
         AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 = AV103ContagemResultado_DataUltCnt_To4;
         AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4 = AV193ContagemResultado_DataPrevista4;
         AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 = AV194ContagemResultado_DataPrevista_To4;
         AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4 = AV104ContagemResultado_StatusDmn4;
         AV301WWContagemResultadoDS_88_Outrosstatus4 = AV105OutrosStatus4;
         AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 = AV106ContagemResultado_StatusUltCnt4;
         AV303WWContagemResultadoDS_90_Contagemresultado_servico4 = AV107ContagemResultado_Servico4;
         AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 = AV207ContagemResultado_CntSrvPrrCod4;
         AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4 = AV108ContagemResultado_ContadorFM4;
         AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4 = AV109ContagemResultado_SistemaCod4;
         AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4 = AV110ContagemResultado_ContratadaCod4;
         AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 = AV111ContagemResultado_ContratadaOrigemCod4;
         AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 = AV112ContagemResultado_NaoCnfDmnCod4;
         AV310WWContagemResultadoDS_97_Contagemresultado_baseline4 = AV113ContagemResultado_Baseline4;
         AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 = AV114ContagemResultado_EsforcoSoma4;
         AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4 = AV115ContagemResultado_Agrupador4;
         AV313WWContagemResultadoDS_100_Contagemresultado_descricao4 = AV139ContagemResultado_Descricao4;
         AV314WWContagemResultadoDS_101_Contagemresultado_codigo4 = AV146ContagemResultado_Codigo4;
         AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 = AV116DynamicFiltersEnabled5;
         AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 = AV117DynamicFiltersSelector5;
         AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 = AV118DynamicFiltersOperator5;
         AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = AV119ContagemResultado_OsFsOsFm5;
         AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5 = AV120ContagemResultado_DataDmn5;
         AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 = AV121ContagemResultado_DataDmn_To5;
         AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 = AV122ContagemResultado_DataUltCnt5;
         AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 = AV123ContagemResultado_DataUltCnt_To5;
         AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5 = AV195ContagemResultado_DataPrevista5;
         AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 = AV196ContagemResultado_DataPrevista_To5;
         AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5 = AV124ContagemResultado_StatusDmn5;
         AV326WWContagemResultadoDS_113_Outrosstatus5 = AV125OutrosStatus5;
         AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 = AV126ContagemResultado_StatusUltCnt5;
         AV328WWContagemResultadoDS_115_Contagemresultado_servico5 = AV127ContagemResultado_Servico5;
         AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 = AV208ContagemResultado_CntSrvPrrCod5;
         AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5 = AV128ContagemResultado_ContadorFM5;
         AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5 = AV129ContagemResultado_SistemaCod5;
         AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5 = AV130ContagemResultado_ContratadaCod5;
         AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 = AV131ContagemResultado_ContratadaOrigemCod5;
         AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 = AV132ContagemResultado_NaoCnfDmnCod5;
         AV335WWContagemResultadoDS_122_Contagemresultado_baseline5 = AV133ContagemResultado_Baseline5;
         AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 = AV134ContagemResultado_EsforcoSoma5;
         AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5 = AV135ContagemResultado_Agrupador5;
         AV338WWContagemResultadoDS_125_Contagemresultado_descricao5 = AV140ContagemResultado_Descricao5;
         AV339WWContagemResultadoDS_126_Contagemresultado_codigo5 = AV147ContagemResultado_Codigo5;
         AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = AV150TFContratada_AreaTrabalhoDes;
         AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel = AV151TFContratada_AreaTrabalhoDes_Sel;
         AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn = AV152TFContagemResultado_DataDmn;
         AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to = AV153TFContagemResultado_DataDmn_To;
         AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt = AV154TFContagemResultado_DataUltCnt;
         AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to = AV155TFContagemResultado_DataUltCnt_To;
         AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista = AV197TFContagemResultado_DataPrevista;
         AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to = AV198TFContagemResultado_DataPrevista_To;
         AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = AV156TFContagemResultado_OsFsOsFm;
         AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel = AV157TFContagemResultado_OsFsOsFm_Sel;
         AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao = AV158TFContagemResultado_Descricao;
         AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel = AV159TFContagemResultado_Descricao_Sel;
         AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = AV160TFContagemrResultado_SistemaSigla;
         AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel = AV161TFContagemrResultado_SistemaSigla_Sel;
         AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = AV162TFContagemResultado_ContratadaSigla;
         AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel = AV163TFContagemResultado_ContratadaSigla_Sel;
         AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = AV201TFContagemResultado_CntNum;
         AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel = AV202TFContagemResultado_CntNum_Sel;
         AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels = AV165TFContagemResultado_StatusDmn_Sels;
         AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels = AV168TFContagemResultado_StatusUltCnt_Sels;
         AV360WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel = AV170TFContagemResultado_Baseline_Sel;
         AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico = AV171TFContagemResultado_Servico;
         AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel = AV172TFContagemResultado_Servico_Sel;
         AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal = AV181TFContagemResultado_EsforcoTotal;
         AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel = AV182TFContagemResultado_EsforcoTotal_Sel;
         AV365WWContagemResultadoDS_152_Tfcontagemresultado_pffinal = AV183TFContagemResultado_PFFinal;
         AV366WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to = AV184TFContagemResultado_PFFinal_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              A456ContagemResultado_Codigo ,
                                              AV142Codigos ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV90Contratadas ,
                                              AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 ,
                                              AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 ,
                                              AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ,
                                              AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1 ,
                                              AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 ,
                                              AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1 ,
                                              AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 ,
                                              AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1 ,
                                              AV226WWContagemResultadoDS_13_Outrosstatus1 ,
                                              AV228WWContagemResultadoDS_15_Contagemresultado_servico1 ,
                                              AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 ,
                                              AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1 ,
                                              AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1 ,
                                              AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 ,
                                              AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 ,
                                              AV235WWContagemResultadoDS_22_Contagemresultado_baseline1 ,
                                              AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 ,
                                              AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1 ,
                                              AV238WWContagemResultadoDS_25_Contagemresultado_descricao1 ,
                                              AV239WWContagemResultadoDS_26_Contagemresultado_codigo1 ,
                                              AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 ,
                                              AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 ,
                                              AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 ,
                                              AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ,
                                              AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2 ,
                                              AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 ,
                                              AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2 ,
                                              AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 ,
                                              AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2 ,
                                              AV251WWContagemResultadoDS_38_Outrosstatus2 ,
                                              AV253WWContagemResultadoDS_40_Contagemresultado_servico2 ,
                                              AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 ,
                                              AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2 ,
                                              AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2 ,
                                              AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 ,
                                              AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 ,
                                              AV260WWContagemResultadoDS_47_Contagemresultado_baseline2 ,
                                              AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 ,
                                              AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2 ,
                                              AV263WWContagemResultadoDS_50_Contagemresultado_descricao2 ,
                                              AV264WWContagemResultadoDS_51_Contagemresultado_codigo2 ,
                                              AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 ,
                                              AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 ,
                                              AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 ,
                                              AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ,
                                              AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3 ,
                                              AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 ,
                                              AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3 ,
                                              AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 ,
                                              AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3 ,
                                              AV276WWContagemResultadoDS_63_Outrosstatus3 ,
                                              AV278WWContagemResultadoDS_65_Contagemresultado_servico3 ,
                                              AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 ,
                                              AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3 ,
                                              AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3 ,
                                              AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 ,
                                              AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 ,
                                              AV285WWContagemResultadoDS_72_Contagemresultado_baseline3 ,
                                              AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 ,
                                              AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3 ,
                                              AV288WWContagemResultadoDS_75_Contagemresultado_descricao3 ,
                                              AV289WWContagemResultadoDS_76_Contagemresultado_codigo3 ,
                                              AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 ,
                                              AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 ,
                                              AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 ,
                                              AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ,
                                              AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4 ,
                                              AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 ,
                                              AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4 ,
                                              AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 ,
                                              AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4 ,
                                              AV301WWContagemResultadoDS_88_Outrosstatus4 ,
                                              AV303WWContagemResultadoDS_90_Contagemresultado_servico4 ,
                                              AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 ,
                                              AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4 ,
                                              AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4 ,
                                              AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 ,
                                              AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 ,
                                              AV310WWContagemResultadoDS_97_Contagemresultado_baseline4 ,
                                              AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 ,
                                              AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4 ,
                                              AV313WWContagemResultadoDS_100_Contagemresultado_descricao4 ,
                                              AV314WWContagemResultadoDS_101_Contagemresultado_codigo4 ,
                                              AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 ,
                                              AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 ,
                                              AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 ,
                                              AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ,
                                              AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5 ,
                                              AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 ,
                                              AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5 ,
                                              AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 ,
                                              AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5 ,
                                              AV326WWContagemResultadoDS_113_Outrosstatus5 ,
                                              AV328WWContagemResultadoDS_115_Contagemresultado_servico5 ,
                                              AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 ,
                                              AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5 ,
                                              AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5 ,
                                              AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 ,
                                              AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 ,
                                              AV335WWContagemResultadoDS_122_Contagemresultado_baseline5 ,
                                              AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 ,
                                              AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5 ,
                                              AV338WWContagemResultadoDS_125_Contagemresultado_descricao5 ,
                                              AV339WWContagemResultadoDS_126_Contagemresultado_codigo5 ,
                                              AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel ,
                                              AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ,
                                              AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn ,
                                              AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to ,
                                              AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista ,
                                              AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to ,
                                              AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel ,
                                              AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ,
                                              AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel ,
                                              AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao ,
                                              AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ,
                                              AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ,
                                              AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel ,
                                              AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ,
                                              AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV360WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel ,
                                              AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel ,
                                              AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico ,
                                              AV142Codigos.Count ,
                                              AV90Contratadas.Count ,
                                              AV18Contratada_AreaTrabalhoCod ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Abertas ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Solicitadas ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Soconfirmadas ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Servico ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Sistema ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Semliquidar ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Lote ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV9WWPContext.gxTpr_Contratada_pessoacod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A1443ContagemResultado_CntSrvPrrCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A805ContagemResultado_ContratadaOrigemCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A598ContagemResultado_Baseline ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A1612ContagemResultado_CntNum ,
                                              A605Servico_Sigla ,
                                              A1043ContagemResultado_LiqLogCod ,
                                              A597ContagemResultado_LoteAceiteCod ,
                                              AV16OrderedBy ,
                                              AV17OrderedDsc ,
                                              AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 ,
                                              AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 ,
                                              AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 ,
                                              AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 ,
                                              AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2 ,
                                              AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 ,
                                              AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 ,
                                              AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 ,
                                              AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3 ,
                                              AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 ,
                                              AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 ,
                                              AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 ,
                                              AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4 ,
                                              AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 ,
                                              AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 ,
                                              AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 ,
                                              AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5 ,
                                              AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt ,
                                              AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to ,
                                              AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel ,
                                              AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal ,
                                              A486ContagemResultado_EsforcoTotal ,
                                              AV365WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV366WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ,
                                              AV58GridState.gxTpr_Dynamicfilters.Count ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod ,
                                              Gx_date ,
                                              AV88Usuario_EhGestor ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              AV9WWPContext.gxTpr_Userid ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Ano ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Mes ,
                                              AV72SDT_FiltroConsContadorFM.gxTpr_Comerro ,
                                              A585ContagemResultado_Erros ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.SHORT
                                              }
         });
         lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV238WWContagemResultadoDS_25_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV238WWContagemResultadoDS_25_Contagemresultado_descricao1), "%", "");
         lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV263WWContagemResultadoDS_50_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV263WWContagemResultadoDS_50_Contagemresultado_descricao2), "%", "");
         lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV288WWContagemResultadoDS_75_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV288WWContagemResultadoDS_75_Contagemresultado_descricao3), "%", "");
         lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV313WWContagemResultadoDS_100_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV313WWContagemResultadoDS_100_Contagemresultado_descricao4), "%", "");
         lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV338WWContagemResultadoDS_125_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV338WWContagemResultadoDS_125_Contagemresultado_descricao5), "%", "");
         lV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes), "%", "");
         lV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm), "%", "");
         lV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao), "%", "");
         lV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
         lV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = StringUtil.PadR( StringUtil.RTrim( AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum), 20, "%");
         lV361WWContagemResultadoDS_148_Tfcontagemresultado_servico = StringUtil.PadR( StringUtil.RTrim( AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico), 15, "%");
         /* Using cursor P003W4 */
         pr_default.execute(0, new Object[] {AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1, AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1, AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1, AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1, AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1, AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1, AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1, AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1, AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1, AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2, AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2, AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2, AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2, AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2, AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2, AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2, AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2, AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2, AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3, AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3, AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3, AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3, AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3, AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3, AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3, AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3, AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3, AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4, AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4, AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4, AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4, AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4, AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4, AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4, AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4, AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4, AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5, AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5, AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5, AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5, AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5, AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5, AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5, AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5, AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5, AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt, AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt, AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to, AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to, AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels.Count, AV58GridState.gxTpr_Dynamicfilters.Count, AV142Codigos.Count, AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV72SDT_FiltroConsContadorFM.gxTpr_Sistema, AV72SDT_FiltroConsContadorFM.gxTpr_Lote, AV72SDT_FiltroConsContadorFM.gxTpr_Solicitadas, AV72SDT_FiltroConsContadorFM.gxTpr_Semliquidar, Gx_date, AV142Codigos.Count, AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV142Codigos.Count, AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV72SDT_FiltroConsContadorFM.gxTpr_Ano, AV72SDT_FiltroConsContadorFM.gxTpr_Mes, AV88Usuario_EhGestor, AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV142Codigos.Count, AV72SDT_FiltroConsContadorFM.gxTpr_Mes, AV72SDT_FiltroConsContadorFM.gxTpr_Mes, AV142Codigos.Count, AV72SDT_FiltroConsContadorFM.gxTpr_Ano, AV72SDT_FiltroConsContadorFM.gxTpr_Ano, AV142Codigos.Count, AV72SDT_FiltroConsContadorFM.gxTpr_Soconfirmadas, AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod, AV9WWPContext.gxTpr_Contratada_codigo, AV9WWPContext.gxTpr_Contratada_pessoacod, AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1, AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1,
         AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1, AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1, AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1, AV226WWContagemResultadoDS_13_Outrosstatus1, AV228WWContagemResultadoDS_15_Contagemresultado_servico1, AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1, AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1, AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1, AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1, AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1, AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1, AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1, AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1, AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1, lV238WWContagemResultadoDS_25_Contagemresultado_descricao1, AV239WWContagemResultadoDS_26_Contagemresultado_codigo1, AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2, AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2, AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2, AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2, AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2, AV251WWContagemResultadoDS_38_Outrosstatus2, AV253WWContagemResultadoDS_40_Contagemresultado_servico2, AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2, AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2, AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2, AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2, AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2, AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2, AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2, AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2, AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2, lV263WWContagemResultadoDS_50_Contagemresultado_descricao2, AV264WWContagemResultadoDS_51_Contagemresultado_codigo2, AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3, AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3, AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3, AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3, AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3, AV276WWContagemResultadoDS_63_Outrosstatus3, AV278WWContagemResultadoDS_65_Contagemresultado_servico3, AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3, AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3, AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3, AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3, AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3, AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3, AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3, AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3, AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3, lV288WWContagemResultadoDS_75_Contagemresultado_descricao3, AV289WWContagemResultadoDS_76_Contagemresultado_codigo3, AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4, AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4, AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4, AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4, AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4, AV301WWContagemResultadoDS_88_Outrosstatus4, AV303WWContagemResultadoDS_90_Contagemresultado_servico4, AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4, AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4, AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4, AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4, AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4, AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4, AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4, AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4, AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4, lV313WWContagemResultadoDS_100_Contagemresultado_descricao4, AV314WWContagemResultadoDS_101_Contagemresultado_codigo4, AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5, AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5, AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5, AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5, AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5, AV326WWContagemResultadoDS_113_Outrosstatus5, AV328WWContagemResultadoDS_115_Contagemresultado_servico5, AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5, AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5, AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5, AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5, AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5, AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5, AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5, AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5, AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5, lV338WWContagemResultadoDS_125_Contagemresultado_descricao5, AV339WWContagemResultadoDS_126_Contagemresultado_codigo5, lV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes, AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel, AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn, AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to, AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista, AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to, lV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm, AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel, lV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao, AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel, lV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla, AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel, lV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla,
         AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel, lV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum, AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel, lV361WWContagemResultadoDS_148_Tfcontagemresultado_servico, AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel, AV72SDT_FiltroConsContadorFM.gxTpr_Servico, AV72SDT_FiltroConsContadorFM.gxTpr_Sistema, AV72SDT_FiltroConsContadorFM.gxTpr_Lote});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A146Modulo_Codigo = P003W4_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P003W4_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P003W4_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P003W4_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P003W4_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P003W4_n830AreaTrabalho_ServicoPadrao[0];
            A1553ContagemResultado_CntSrvCod = P003W4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003W4_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P003W4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003W4_n1603ContagemResultado_CntCod[0];
            A1583ContagemResultado_TipoRegistro = P003W4_A1583ContagemResultado_TipoRegistro[0];
            A597ContagemResultado_LoteAceiteCod = P003W4_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P003W4_n597ContagemResultado_LoteAceiteCod[0];
            A1043ContagemResultado_LiqLogCod = P003W4_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = P003W4_n1043ContagemResultado_LiqLogCod[0];
            A605Servico_Sigla = P003W4_A605Servico_Sigla[0];
            n605Servico_Sigla = P003W4_n605Servico_Sigla[0];
            A1612ContagemResultado_CntNum = P003W4_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P003W4_n1612ContagemResultado_CntNum[0];
            A803ContagemResultado_ContratadaSigla = P003W4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003W4_n803ContagemResultado_ContratadaSigla[0];
            A509ContagemrResultado_SistemaSigla = P003W4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003W4_n509ContagemrResultado_SistemaSigla[0];
            A501ContagemResultado_OsFsOsFm = P003W4_A501ContagemResultado_OsFsOsFm[0];
            A53Contratada_AreaTrabalhoDes = P003W4_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P003W4_n53Contratada_AreaTrabalhoDes[0];
            A494ContagemResultado_Descricao = P003W4_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003W4_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P003W4_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003W4_n1046ContagemResultado_Agrupador[0];
            A598ContagemResultado_Baseline = P003W4_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003W4_n598ContagemResultado_Baseline[0];
            A468ContagemResultado_NaoCnfDmnCod = P003W4_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003W4_n468ContagemResultado_NaoCnfDmnCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P003W4_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P003W4_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = P003W4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003W4_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P003W4_A508ContagemResultado_Owner[0];
            A1443ContagemResultado_CntSrvPrrCod = P003W4_A1443ContagemResultado_CntSrvPrrCod[0];
            n1443ContagemResultado_CntSrvPrrCod = P003W4_n1443ContagemResultado_CntSrvPrrCod[0];
            A601ContagemResultado_Servico = P003W4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003W4_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P003W4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003W4_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P003W4_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P003W4_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P003W4_A471ContagemResultado_DataDmn[0];
            A499ContagemResultado_ContratadaPessoaCod = P003W4_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P003W4_n499ContagemResultado_ContratadaPessoaCod[0];
            A490ContagemResultado_ContratadaCod = P003W4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003W4_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P003W4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003W4_n52Contratada_AreaTrabalhoCod[0];
            A684ContagemResultado_PFBFSUltima = P003W4_A684ContagemResultado_PFBFSUltima[0];
            A457ContagemResultado_Demanda = P003W4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003W4_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P003W4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003W4_n493ContagemResultado_DemandaFM[0];
            A510ContagemResultado_EsforcoSoma = P003W4_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P003W4_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P003W4_A584ContagemResultado_ContadorFM[0];
            A531ContagemResultado_StatusUltCnt = P003W4_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P003W4_A566ContagemResultado_DataUltCnt[0];
            A456ContagemResultado_Codigo = P003W4_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P003W4_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P003W4_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P003W4_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P003W4_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P003W4_A605Servico_Sigla[0];
            n605Servico_Sigla = P003W4_n605Servico_Sigla[0];
            A1603ContagemResultado_CntCod = P003W4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003W4_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P003W4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003W4_n601ContagemResultado_Servico[0];
            A1612ContagemResultado_CntNum = P003W4_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P003W4_n1612ContagemResultado_CntNum[0];
            A509ContagemrResultado_SistemaSigla = P003W4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003W4_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P003W4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003W4_n803ContagemResultado_ContratadaSigla[0];
            A499ContagemResultado_ContratadaPessoaCod = P003W4_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P003W4_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P003W4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003W4_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P003W4_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P003W4_n53Contratada_AreaTrabalhoDes[0];
            A684ContagemResultado_PFBFSUltima = P003W4_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003W4_A584ContagemResultado_ContadorFM[0];
            A531ContagemResultado_StatusUltCnt = P003W4_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P003W4_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P003W4_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P003W4_n510ContagemResultado_EsforcoSoma[0];
            if ( ! ( ! ( AV88Usuario_EhGestor || AV9WWPContext.gxTpr_Userehfinanceiro || AV9WWPContext.gxTpr_Userehcontratante ) && ( AV142Codigos.Count == 0 ) && (0==AV72SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod) && (0==AV72SDT_FiltroConsContadorFM.gxTpr_Sistema) && (0==AV72SDT_FiltroConsContadorFM.gxTpr_Lote) && ! AV72SDT_FiltroConsContadorFM.gxTpr_Solicitadas && ! AV72SDT_FiltroConsContadorFM.gxTpr_Semliquidar ) || ( ( A508ContagemResultado_Owner == AV9WWPContext.gxTpr_Userid ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) || new prc_existecontagem(context).executeUdp(  A456ContagemResultado_Codigo,  AV9WWPContext.gxTpr_Userid) ) )
            {
               GXt_char2 = A486ContagemResultado_EsforcoTotal;
               new prc_contageresultado_esforcototal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_char2) ;
               A486ContagemResultado_EsforcoTotal = GXt_char2;
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal)) ) ) || ( StringUtil.Like( A486ContagemResultado_EsforcoTotal , StringUtil.PadR( AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal , 10 , "%"),  ' ' ) ) )
               {
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel)) || ( ( StringUtil.StrCmp(A486ContagemResultado_EsforcoTotal, AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel) == 0 ) ) )
                  {
                     GXt_decimal3 = A574ContagemResultado_PFFinal;
                     new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal3) ;
                     A574ContagemResultado_PFFinal = GXt_decimal3;
                     if ( (Convert.ToDecimal(0)==AV365WWContagemResultadoDS_152_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV365WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ) ) )
                     {
                        if ( (Convert.ToDecimal(0)==AV366WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV366WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ) ) )
                        {
                           GXt_int4 = A585ContagemResultado_Erros;
                           new prc_pendenciasdaos(context ).execute(  A456ContagemResultado_Codigo, out  GXt_int4) ;
                           A585ContagemResultado_Erros = GXt_int4;
                           if ( ! ( ( AV142Codigos.Count == 0 ) && ( AV72SDT_FiltroConsContadorFM.gxTpr_Comerro ) ) || ( ( A585ContagemResultado_Erros > 0 ) ) )
                           {
                              AV13CellRow = (int)(AV13CellRow+1);
                              /* Execute user subroutine: 'BEFOREWRITELINE' */
                              S172 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(0);
                                 returnInSub = true;
                                 if (true) return;
                              }
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = A53Contratada_AreaTrabalhoDes;
                              GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                              GXt_dtime1 = DateTimeUtil.ResetTime( A566ContagemResultado_DataUltCnt ) ;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Date = GXt_dtime1;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = A1351ContagemResultado_DataPrevista;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = A501ContagemResultado_OsFsOsFm;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = A494ContagemResultado_Descricao;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = A509ContagemrResultado_SistemaSigla;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = A803ContagemResultado_ContratadaSigla;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = A1612ContagemResultado_CntNum;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,A531ContagemResultado_StatusUltCnt);
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "";
                              if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "*";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                              {
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "";
                              }
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = A605Servico_Sigla;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+13, 1, 1).Text = A486ContagemResultado_EsforcoTotal;
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+14, 1, 1).Number = (double)(A574ContagemResultado_PFFinal);
                              /* Execute user subroutine: 'AFTERWRITELINE' */
                              S182 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(0);
                                 returnInSub = true;
                                 if (true) return;
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10ExcelDocument = new ExcelDocumentI();
         AV142Codigos = new GxSimpleCollection();
         AV91WebSession = context.GetSession();
         AV90Contratadas = new GxSimpleCollection();
         AV72SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         AV58GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV59GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV19DynamicFiltersSelector1 = "";
         AV21ContagemResultado_OsFsOsFm1 = "";
         AV22ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV23ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV73ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV74ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV187ContagemResultado_DataPrevista1 = DateTime.MinValue;
         AV188ContagemResultado_DataPrevista_To1 = DateTime.MinValue;
         AV27ContagemResultado_StatusDmn1 = "";
         AV93OutrosStatus1 = "";
         AV63ContagemResultado_Baseline1 = "";
         AV82ContagemResultado_Agrupador1 = "";
         AV136ContagemResultado_Descricao1 = "";
         AV32DynamicFiltersSelector2 = "";
         AV34ContagemResultado_OsFsOsFm2 = "";
         AV35ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV36ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV75ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV76ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV189ContagemResultado_DataPrevista2 = DateTime.MinValue;
         AV190ContagemResultado_DataPrevista_To2 = DateTime.MinValue;
         AV40ContagemResultado_StatusDmn2 = "";
         AV94OutrosStatus2 = "";
         AV64ContagemResultado_Baseline2 = "";
         AV83ContagemResultado_Agrupador2 = "";
         AV137ContagemResultado_Descricao2 = "";
         AV45DynamicFiltersSelector3 = "";
         AV47ContagemResultado_OsFsOsFm3 = "";
         AV48ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV49ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV77ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV78ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV191ContagemResultado_DataPrevista3 = DateTime.MinValue;
         AV192ContagemResultado_DataPrevista_To3 = DateTime.MinValue;
         AV53ContagemResultado_StatusDmn3 = "";
         AV95OutrosStatus3 = "";
         AV65ContagemResultado_Baseline3 = "";
         AV84ContagemResultado_Agrupador3 = "";
         AV138ContagemResultado_Descricao3 = "";
         AV97DynamicFiltersSelector4 = "";
         AV99ContagemResultado_OsFsOsFm4 = "";
         AV100ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV101ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV102ContagemResultado_DataUltCnt4 = DateTime.MinValue;
         AV103ContagemResultado_DataUltCnt_To4 = DateTime.MinValue;
         AV193ContagemResultado_DataPrevista4 = DateTime.MinValue;
         AV194ContagemResultado_DataPrevista_To4 = DateTime.MinValue;
         AV104ContagemResultado_StatusDmn4 = "";
         AV105OutrosStatus4 = "";
         AV113ContagemResultado_Baseline4 = "";
         AV115ContagemResultado_Agrupador4 = "";
         AV139ContagemResultado_Descricao4 = "";
         AV117DynamicFiltersSelector5 = "";
         AV119ContagemResultado_OsFsOsFm5 = "";
         AV120ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV121ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV122ContagemResultado_DataUltCnt5 = DateTime.MinValue;
         AV123ContagemResultado_DataUltCnt_To5 = DateTime.MinValue;
         AV195ContagemResultado_DataPrevista5 = DateTime.MinValue;
         AV196ContagemResultado_DataPrevista_To5 = DateTime.MinValue;
         AV124ContagemResultado_StatusDmn5 = "";
         AV125OutrosStatus5 = "";
         AV133ContagemResultado_Baseline5 = "";
         AV135ContagemResultado_Agrupador5 = "";
         AV140ContagemResultado_Descricao5 = "";
         AV165TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV166TFContagemResultado_StatusDmn_Sel = "";
         AV168TFContagemResultado_StatusUltCnt_Sels = new GxSimpleCollection();
         AV88Usuario_EhGestor = false;
         AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 = "";
         AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = "";
         AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1 = "";
         AV226WWContagemResultadoDS_13_Outrosstatus1 = "";
         AV235WWContagemResultadoDS_22_Contagemresultado_baseline1 = "";
         AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1 = "";
         AV238WWContagemResultadoDS_25_Contagemresultado_descricao1 = "";
         AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 = "";
         AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = "";
         AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2 = "";
         AV251WWContagemResultadoDS_38_Outrosstatus2 = "";
         AV260WWContagemResultadoDS_47_Contagemresultado_baseline2 = "";
         AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2 = "";
         AV263WWContagemResultadoDS_50_Contagemresultado_descricao2 = "";
         AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 = "";
         AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = "";
         AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3 = "";
         AV276WWContagemResultadoDS_63_Outrosstatus3 = "";
         AV285WWContagemResultadoDS_72_Contagemresultado_baseline3 = "";
         AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3 = "";
         AV288WWContagemResultadoDS_75_Contagemresultado_descricao3 = "";
         AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 = "";
         AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = "";
         AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 = DateTime.MinValue;
         AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 = DateTime.MinValue;
         AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4 = "";
         AV301WWContagemResultadoDS_88_Outrosstatus4 = "";
         AV310WWContagemResultadoDS_97_Contagemresultado_baseline4 = "";
         AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4 = "";
         AV313WWContagemResultadoDS_100_Contagemresultado_descricao4 = "";
         AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 = "";
         AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = "";
         AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 = DateTime.MinValue;
         AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 = DateTime.MinValue;
         AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5 = "";
         AV326WWContagemResultadoDS_113_Outrosstatus5 = "";
         AV335WWContagemResultadoDS_122_Contagemresultado_baseline5 = "";
         AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5 = "";
         AV338WWContagemResultadoDS_125_Contagemresultado_descricao5 = "";
         AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = "";
         AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel = "";
         AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to = (DateTime)(DateTime.MinValue);
         AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = "";
         AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel = "";
         AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao = "";
         AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel = "";
         AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = "";
         AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel = "";
         AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = "";
         AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel = "";
         AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = "";
         AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel = "";
         AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels = new GxSimpleCollection();
         AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico = "";
         AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal = "";
         AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel = "";
         scmdbuf = "";
         lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = "";
         lV238WWContagemResultadoDS_25_Contagemresultado_descricao1 = "";
         lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = "";
         lV263WWContagemResultadoDS_50_Contagemresultado_descricao2 = "";
         lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = "";
         lV288WWContagemResultadoDS_75_Contagemresultado_descricao3 = "";
         lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = "";
         lV313WWContagemResultadoDS_100_Contagemresultado_descricao4 = "";
         lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = "";
         lV338WWContagemResultadoDS_125_Contagemresultado_descricao5 = "";
         lV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = "";
         lV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = "";
         lV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao = "";
         lV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = "";
         lV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = "";
         lV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = "";
         lV361WWContagemResultadoDS_148_Tfcontagemresultado_servico = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A53Contratada_AreaTrabalhoDes = "";
         A509ContagemrResultado_SistemaSigla = "";
         A803ContagemResultado_ContratadaSigla = "";
         A1612ContagemResultado_CntNum = "";
         A605Servico_Sigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A486ContagemResultado_EsforcoTotal = "";
         Gx_date = DateTime.MinValue;
         P003W4_A146Modulo_Codigo = new int[1] ;
         P003W4_n146Modulo_Codigo = new bool[] {false} ;
         P003W4_A127Sistema_Codigo = new int[1] ;
         P003W4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P003W4_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P003W4_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P003W4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003W4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003W4_A1603ContagemResultado_CntCod = new int[1] ;
         P003W4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003W4_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P003W4_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003W4_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003W4_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P003W4_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P003W4_A605Servico_Sigla = new String[] {""} ;
         P003W4_n605Servico_Sigla = new bool[] {false} ;
         P003W4_A1612ContagemResultado_CntNum = new String[] {""} ;
         P003W4_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P003W4_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P003W4_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P003W4_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003W4_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003W4_A501ContagemResultado_OsFsOsFm = new String[] {""} ;
         P003W4_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P003W4_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P003W4_A494ContagemResultado_Descricao = new String[] {""} ;
         P003W4_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003W4_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P003W4_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P003W4_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003W4_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003W4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P003W4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P003W4_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P003W4_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P003W4_A489ContagemResultado_SistemaCod = new int[1] ;
         P003W4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003W4_A508ContagemResultado_Owner = new int[1] ;
         P003W4_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         P003W4_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         P003W4_A601ContagemResultado_Servico = new int[1] ;
         P003W4_n601ContagemResultado_Servico = new bool[] {false} ;
         P003W4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003W4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003W4_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P003W4_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P003W4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003W4_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P003W4_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P003W4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003W4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003W4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003W4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003W4_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P003W4_A457ContagemResultado_Demanda = new String[] {""} ;
         P003W4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003W4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003W4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003W4_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P003W4_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P003W4_A584ContagemResultado_ContadorFM = new int[1] ;
         P003W4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P003W4_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P003W4_A456ContagemResultado_Codigo = new int[1] ;
         A501ContagemResultado_OsFsOsFm = "";
         GXt_char2 = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportwwcontagemresultado__default(),
            new Object[][] {
                new Object[] {
               P003W4_A146Modulo_Codigo, P003W4_n146Modulo_Codigo, P003W4_A127Sistema_Codigo, P003W4_A135Sistema_AreaTrabalhoCod, P003W4_A830AreaTrabalho_ServicoPadrao, P003W4_n830AreaTrabalho_ServicoPadrao, P003W4_A1553ContagemResultado_CntSrvCod, P003W4_n1553ContagemResultado_CntSrvCod, P003W4_A1603ContagemResultado_CntCod, P003W4_n1603ContagemResultado_CntCod,
               P003W4_A1583ContagemResultado_TipoRegistro, P003W4_A597ContagemResultado_LoteAceiteCod, P003W4_n597ContagemResultado_LoteAceiteCod, P003W4_A1043ContagemResultado_LiqLogCod, P003W4_n1043ContagemResultado_LiqLogCod, P003W4_A605Servico_Sigla, P003W4_n605Servico_Sigla, P003W4_A1612ContagemResultado_CntNum, P003W4_n1612ContagemResultado_CntNum, P003W4_A803ContagemResultado_ContratadaSigla,
               P003W4_n803ContagemResultado_ContratadaSigla, P003W4_A509ContagemrResultado_SistemaSigla, P003W4_n509ContagemrResultado_SistemaSigla, P003W4_A501ContagemResultado_OsFsOsFm, P003W4_A53Contratada_AreaTrabalhoDes, P003W4_n53Contratada_AreaTrabalhoDes, P003W4_A494ContagemResultado_Descricao, P003W4_n494ContagemResultado_Descricao, P003W4_A1046ContagemResultado_Agrupador, P003W4_n1046ContagemResultado_Agrupador,
               P003W4_A598ContagemResultado_Baseline, P003W4_n598ContagemResultado_Baseline, P003W4_A468ContagemResultado_NaoCnfDmnCod, P003W4_n468ContagemResultado_NaoCnfDmnCod, P003W4_A805ContagemResultado_ContratadaOrigemCod, P003W4_n805ContagemResultado_ContratadaOrigemCod, P003W4_A489ContagemResultado_SistemaCod, P003W4_n489ContagemResultado_SistemaCod, P003W4_A508ContagemResultado_Owner, P003W4_A1443ContagemResultado_CntSrvPrrCod,
               P003W4_n1443ContagemResultado_CntSrvPrrCod, P003W4_A601ContagemResultado_Servico, P003W4_n601ContagemResultado_Servico, P003W4_A484ContagemResultado_StatusDmn, P003W4_n484ContagemResultado_StatusDmn, P003W4_A1351ContagemResultado_DataPrevista, P003W4_n1351ContagemResultado_DataPrevista, P003W4_A471ContagemResultado_DataDmn, P003W4_A499ContagemResultado_ContratadaPessoaCod, P003W4_n499ContagemResultado_ContratadaPessoaCod,
               P003W4_A490ContagemResultado_ContratadaCod, P003W4_n490ContagemResultado_ContratadaCod, P003W4_A52Contratada_AreaTrabalhoCod, P003W4_n52Contratada_AreaTrabalhoCod, P003W4_A684ContagemResultado_PFBFSUltima, P003W4_A457ContagemResultado_Demanda, P003W4_n457ContagemResultado_Demanda, P003W4_A493ContagemResultado_DemandaFM, P003W4_n493ContagemResultado_DemandaFM, P003W4_A510ContagemResultado_EsforcoSoma,
               P003W4_n510ContagemResultado_EsforcoSoma, P003W4_A584ContagemResultado_ContadorFM, P003W4_A531ContagemResultado_StatusUltCnt, P003W4_A566ContagemResultado_DataUltCnt, P003W4_A456ContagemResultado_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV170TFContagemResultado_Baseline_Sel ;
      private short AV16OrderedBy ;
      private short AV20DynamicFiltersOperator1 ;
      private short AV79ContagemResultado_StatusUltCnt1 ;
      private short AV33DynamicFiltersOperator2 ;
      private short AV80ContagemResultado_StatusUltCnt2 ;
      private short AV46DynamicFiltersOperator3 ;
      private short AV81ContagemResultado_StatusUltCnt3 ;
      private short AV98DynamicFiltersOperator4 ;
      private short AV106ContagemResultado_StatusUltCnt4 ;
      private short AV118DynamicFiltersOperator5 ;
      private short AV126ContagemResultado_StatusUltCnt5 ;
      private short AV169TFContagemResultado_StatusUltCnt_Sel ;
      private short AV203Diferenca ;
      private short AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 ;
      private short AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 ;
      private short AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 ;
      private short AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 ;
      private short AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 ;
      private short AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 ;
      private short AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 ;
      private short AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 ;
      private short AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 ;
      private short AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 ;
      private short AV360WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel ;
      private short AV72SDT_FiltroConsContadorFM_gxTpr_Ano ;
      private short AV9WWPContext_gxTpr_Userid ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A585ContagemResultado_Erros ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short GXt_int4 ;
      private int AV18Contratada_AreaTrabalhoCod ;
      private int AV89Contratada_Codigo ;
      private int AV172TFContagemResultado_Servico_Sel ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV15Random ;
      private int AV200AreaTrabalho_Codigo ;
      private int AV69ContagemResultado_Servico1 ;
      private int AV204ContagemResultado_CntSrvPrrCod1 ;
      private int AV60ContagemResultado_ContadorFM1 ;
      private int AV24ContagemResultado_SistemaCod1 ;
      private int AV25ContagemResultado_ContratadaCod1 ;
      private int AV85ContagemResultado_ContratadaOrigemCod1 ;
      private int AV26ContagemResultado_NaoCnfDmnCod1 ;
      private int AV30ContagemResultado_EsforcoSoma1 ;
      private int AV143ContagemResultado_Codigo1 ;
      private int AV70ContagemResultado_Servico2 ;
      private int AV205ContagemResultado_CntSrvPrrCod2 ;
      private int AV61ContagemResultado_ContadorFM2 ;
      private int AV37ContagemResultado_SistemaCod2 ;
      private int AV38ContagemResultado_ContratadaCod2 ;
      private int AV86ContagemResultado_ContratadaOrigemCod2 ;
      private int AV39ContagemResultado_NaoCnfDmnCod2 ;
      private int AV43ContagemResultado_EsforcoSoma2 ;
      private int AV144ContagemResultado_Codigo2 ;
      private int AV71ContagemResultado_Servico3 ;
      private int AV206ContagemResultado_CntSrvPrrCod3 ;
      private int AV62ContagemResultado_ContadorFM3 ;
      private int AV50ContagemResultado_SistemaCod3 ;
      private int AV51ContagemResultado_ContratadaCod3 ;
      private int AV87ContagemResultado_ContratadaOrigemCod3 ;
      private int AV52ContagemResultado_NaoCnfDmnCod3 ;
      private int AV56ContagemResultado_EsforcoSoma3 ;
      private int AV145ContagemResultado_Codigo3 ;
      private int AV107ContagemResultado_Servico4 ;
      private int AV207ContagemResultado_CntSrvPrrCod4 ;
      private int AV108ContagemResultado_ContadorFM4 ;
      private int AV109ContagemResultado_SistemaCod4 ;
      private int AV110ContagemResultado_ContratadaCod4 ;
      private int AV111ContagemResultado_ContratadaOrigemCod4 ;
      private int AV112ContagemResultado_NaoCnfDmnCod4 ;
      private int AV114ContagemResultado_EsforcoSoma4 ;
      private int AV146ContagemResultado_Codigo4 ;
      private int AV127ContagemResultado_Servico5 ;
      private int AV208ContagemResultado_CntSrvPrrCod5 ;
      private int AV128ContagemResultado_ContadorFM5 ;
      private int AV129ContagemResultado_SistemaCod5 ;
      private int AV130ContagemResultado_ContratadaCod5 ;
      private int AV131ContagemResultado_ContratadaOrigemCod5 ;
      private int AV132ContagemResultado_NaoCnfDmnCod5 ;
      private int AV134ContagemResultado_EsforcoSoma5 ;
      private int AV147ContagemResultado_Codigo5 ;
      private int AV211GXV1 ;
      private int AV212GXV2 ;
      private int AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod ;
      private int AV215WWContagemResultadoDS_2_Contratada_codigo ;
      private int AV228WWContagemResultadoDS_15_Contagemresultado_servico1 ;
      private int AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 ;
      private int AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1 ;
      private int AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1 ;
      private int AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1 ;
      private int AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 ;
      private int AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 ;
      private int AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 ;
      private int AV239WWContagemResultadoDS_26_Contagemresultado_codigo1 ;
      private int AV253WWContagemResultadoDS_40_Contagemresultado_servico2 ;
      private int AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 ;
      private int AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2 ;
      private int AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2 ;
      private int AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2 ;
      private int AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 ;
      private int AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 ;
      private int AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 ;
      private int AV264WWContagemResultadoDS_51_Contagemresultado_codigo2 ;
      private int AV278WWContagemResultadoDS_65_Contagemresultado_servico3 ;
      private int AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 ;
      private int AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3 ;
      private int AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3 ;
      private int AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3 ;
      private int AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 ;
      private int AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 ;
      private int AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 ;
      private int AV289WWContagemResultadoDS_76_Contagemresultado_codigo3 ;
      private int AV303WWContagemResultadoDS_90_Contagemresultado_servico4 ;
      private int AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 ;
      private int AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4 ;
      private int AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4 ;
      private int AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4 ;
      private int AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 ;
      private int AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 ;
      private int AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 ;
      private int AV314WWContagemResultadoDS_101_Contagemresultado_codigo4 ;
      private int AV328WWContagemResultadoDS_115_Contagemresultado_servico5 ;
      private int AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 ;
      private int AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5 ;
      private int AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5 ;
      private int AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5 ;
      private int AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 ;
      private int AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 ;
      private int AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 ;
      private int AV339WWContagemResultadoDS_126_Contagemresultado_codigo5 ;
      private int AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Contratada_pessoacod ;
      private int AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV142Codigos_Count ;
      private int AV90Contratadas_Count ;
      private int AV72SDT_FiltroConsContadorFM_gxTpr_Servico ;
      private int AV72SDT_FiltroConsContadorFM_gxTpr_Sistema ;
      private int AV72SDT_FiltroConsContadorFM_gxTpr_Lote ;
      private int AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels_Count ;
      private int AV58GridState_gxTpr_Dynamicfilters_Count ;
      private int AV72SDT_FiltroConsContadorFM_gxTpr_Contagemresultado_contadorfmcod ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A510ContagemResultado_EsforcoSoma ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private long AV186i ;
      private long AV72SDT_FiltroConsContadorFM_gxTpr_Mes ;
      private decimal AV183TFContagemResultado_PFFinal ;
      private decimal AV184TFContagemResultado_PFFinal_To ;
      private decimal AV365WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ;
      private decimal AV366WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal GXt_decimal3 ;
      private String AV160TFContagemrResultado_SistemaSigla ;
      private String AV161TFContagemrResultado_SistemaSigla_Sel ;
      private String AV162TFContagemResultado_ContratadaSigla ;
      private String AV163TFContagemResultado_ContratadaSigla_Sel ;
      private String AV201TFContagemResultado_CntNum ;
      private String AV202TFContagemResultado_CntNum_Sel ;
      private String AV171TFContagemResultado_Servico ;
      private String AV181TFContagemResultado_EsforcoTotal ;
      private String AV182TFContagemResultado_EsforcoTotal_Sel ;
      private String AV27ContagemResultado_StatusDmn1 ;
      private String AV93OutrosStatus1 ;
      private String AV63ContagemResultado_Baseline1 ;
      private String AV82ContagemResultado_Agrupador1 ;
      private String AV40ContagemResultado_StatusDmn2 ;
      private String AV94OutrosStatus2 ;
      private String AV64ContagemResultado_Baseline2 ;
      private String AV83ContagemResultado_Agrupador2 ;
      private String AV53ContagemResultado_StatusDmn3 ;
      private String AV95OutrosStatus3 ;
      private String AV65ContagemResultado_Baseline3 ;
      private String AV84ContagemResultado_Agrupador3 ;
      private String AV104ContagemResultado_StatusDmn4 ;
      private String AV105OutrosStatus4 ;
      private String AV113ContagemResultado_Baseline4 ;
      private String AV115ContagemResultado_Agrupador4 ;
      private String AV124ContagemResultado_StatusDmn5 ;
      private String AV125OutrosStatus5 ;
      private String AV133ContagemResultado_Baseline5 ;
      private String AV135ContagemResultado_Agrupador5 ;
      private String AV166TFContagemResultado_StatusDmn_Sel ;
      private String AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1 ;
      private String AV226WWContagemResultadoDS_13_Outrosstatus1 ;
      private String AV235WWContagemResultadoDS_22_Contagemresultado_baseline1 ;
      private String AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1 ;
      private String AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2 ;
      private String AV251WWContagemResultadoDS_38_Outrosstatus2 ;
      private String AV260WWContagemResultadoDS_47_Contagemresultado_baseline2 ;
      private String AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2 ;
      private String AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3 ;
      private String AV276WWContagemResultadoDS_63_Outrosstatus3 ;
      private String AV285WWContagemResultadoDS_72_Contagemresultado_baseline3 ;
      private String AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3 ;
      private String AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4 ;
      private String AV301WWContagemResultadoDS_88_Outrosstatus4 ;
      private String AV310WWContagemResultadoDS_97_Contagemresultado_baseline4 ;
      private String AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4 ;
      private String AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5 ;
      private String AV326WWContagemResultadoDS_113_Outrosstatus5 ;
      private String AV335WWContagemResultadoDS_122_Contagemresultado_baseline5 ;
      private String AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5 ;
      private String AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ;
      private String AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ;
      private String AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel ;
      private String AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ;
      private String AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel ;
      private String AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico ;
      private String AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal ;
      private String AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel ;
      private String scmdbuf ;
      private String lV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ;
      private String lV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ;
      private String lV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ;
      private String lV361WWContagemResultadoDS_148_Tfcontagemresultado_servico ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A1612ContagemResultado_CntNum ;
      private String A605Servico_Sigla ;
      private String A486ContagemResultado_EsforcoTotal ;
      private String GXt_char2 ;
      private DateTime AV197TFContagemResultado_DataPrevista ;
      private DateTime AV198TFContagemResultado_DataPrevista_To ;
      private DateTime AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista ;
      private DateTime AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime GXt_dtime1 ;
      private DateTime AV152TFContagemResultado_DataDmn ;
      private DateTime AV153TFContagemResultado_DataDmn_To ;
      private DateTime AV154TFContagemResultado_DataUltCnt ;
      private DateTime AV155TFContagemResultado_DataUltCnt_To ;
      private DateTime AV22ContagemResultado_DataDmn1 ;
      private DateTime AV23ContagemResultado_DataDmn_To1 ;
      private DateTime AV73ContagemResultado_DataUltCnt1 ;
      private DateTime AV74ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV187ContagemResultado_DataPrevista1 ;
      private DateTime AV188ContagemResultado_DataPrevista_To1 ;
      private DateTime AV35ContagemResultado_DataDmn2 ;
      private DateTime AV36ContagemResultado_DataDmn_To2 ;
      private DateTime AV75ContagemResultado_DataUltCnt2 ;
      private DateTime AV76ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV189ContagemResultado_DataPrevista2 ;
      private DateTime AV190ContagemResultado_DataPrevista_To2 ;
      private DateTime AV48ContagemResultado_DataDmn3 ;
      private DateTime AV49ContagemResultado_DataDmn_To3 ;
      private DateTime AV77ContagemResultado_DataUltCnt3 ;
      private DateTime AV78ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV191ContagemResultado_DataPrevista3 ;
      private DateTime AV192ContagemResultado_DataPrevista_To3 ;
      private DateTime AV100ContagemResultado_DataDmn4 ;
      private DateTime AV101ContagemResultado_DataDmn_To4 ;
      private DateTime AV102ContagemResultado_DataUltCnt4 ;
      private DateTime AV103ContagemResultado_DataUltCnt_To4 ;
      private DateTime AV193ContagemResultado_DataPrevista4 ;
      private DateTime AV194ContagemResultado_DataPrevista_To4 ;
      private DateTime AV120ContagemResultado_DataDmn5 ;
      private DateTime AV121ContagemResultado_DataDmn_To5 ;
      private DateTime AV122ContagemResultado_DataUltCnt5 ;
      private DateTime AV123ContagemResultado_DataUltCnt_To5 ;
      private DateTime AV195ContagemResultado_DataPrevista5 ;
      private DateTime AV196ContagemResultado_DataPrevista_To5 ;
      private DateTime AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 ;
      private DateTime AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1 ;
      private DateTime AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 ;
      private DateTime AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2 ;
      private DateTime AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 ;
      private DateTime AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 ;
      private DateTime AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2 ;
      private DateTime AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 ;
      private DateTime AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3 ;
      private DateTime AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 ;
      private DateTime AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 ;
      private DateTime AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3 ;
      private DateTime AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 ;
      private DateTime AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4 ;
      private DateTime AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 ;
      private DateTime AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 ;
      private DateTime AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 ;
      private DateTime AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4 ;
      private DateTime AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 ;
      private DateTime AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5 ;
      private DateTime AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 ;
      private DateTime AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 ;
      private DateTime AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 ;
      private DateTime AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5 ;
      private DateTime AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 ;
      private DateTime AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn ;
      private DateTime AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to ;
      private DateTime AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt ;
      private DateTime AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime Gx_date ;
      private bool AV17OrderedDsc ;
      private bool returnInSub ;
      private bool AV31DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool AV96DynamicFiltersEnabled4 ;
      private bool AV116DynamicFiltersEnabled5 ;
      private bool AV88Usuario_EhGestor ;
      private bool AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 ;
      private bool AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 ;
      private bool AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 ;
      private bool AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 ;
      private bool AV72SDT_FiltroConsContadorFM_gxTpr_Abertas ;
      private bool AV72SDT_FiltroConsContadorFM_gxTpr_Solicitadas ;
      private bool AV72SDT_FiltroConsContadorFM_gxTpr_Soconfirmadas ;
      private bool AV72SDT_FiltroConsContadorFM_gxTpr_Semliquidar ;
      private bool AV72SDT_FiltroConsContadorFM_gxTpr_Comerro ;
      private bool AV9WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV9WWPContext_gxTpr_Userehcontratante ;
      private bool A598ContagemResultado_Baseline ;
      private bool n146Modulo_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n605Servico_Sigla ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n598ContagemResultado_Baseline ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n510ContagemResultado_EsforcoSoma ;
      private String AV164TFContagemResultado_StatusDmn_SelsJson ;
      private String AV167TFContagemResultado_StatusUltCnt_SelsJson ;
      private String AV57GridStateXML ;
      private String AV150TFContratada_AreaTrabalhoDes ;
      private String AV151TFContratada_AreaTrabalhoDes_Sel ;
      private String AV156TFContagemResultado_OsFsOsFm ;
      private String AV157TFContagemResultado_OsFsOsFm_Sel ;
      private String AV158TFContagemResultado_Descricao ;
      private String AV159TFContagemResultado_Descricao_Sel ;
      private String AV185TFContagemResultado_Servico_SelDsc ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV19DynamicFiltersSelector1 ;
      private String AV21ContagemResultado_OsFsOsFm1 ;
      private String AV136ContagemResultado_Descricao1 ;
      private String AV32DynamicFiltersSelector2 ;
      private String AV34ContagemResultado_OsFsOsFm2 ;
      private String AV137ContagemResultado_Descricao2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String AV47ContagemResultado_OsFsOsFm3 ;
      private String AV138ContagemResultado_Descricao3 ;
      private String AV97DynamicFiltersSelector4 ;
      private String AV99ContagemResultado_OsFsOsFm4 ;
      private String AV139ContagemResultado_Descricao4 ;
      private String AV117DynamicFiltersSelector5 ;
      private String AV119ContagemResultado_OsFsOsFm5 ;
      private String AV140ContagemResultado_Descricao5 ;
      private String AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 ;
      private String AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ;
      private String AV238WWContagemResultadoDS_25_Contagemresultado_descricao1 ;
      private String AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 ;
      private String AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ;
      private String AV263WWContagemResultadoDS_50_Contagemresultado_descricao2 ;
      private String AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 ;
      private String AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ;
      private String AV288WWContagemResultadoDS_75_Contagemresultado_descricao3 ;
      private String AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 ;
      private String AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ;
      private String AV313WWContagemResultadoDS_100_Contagemresultado_descricao4 ;
      private String AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 ;
      private String AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ;
      private String AV338WWContagemResultadoDS_125_Contagemresultado_descricao5 ;
      private String AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ;
      private String AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel ;
      private String AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ;
      private String AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel ;
      private String AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao ;
      private String AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel ;
      private String lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ;
      private String lV238WWContagemResultadoDS_25_Contagemresultado_descricao1 ;
      private String lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ;
      private String lV263WWContagemResultadoDS_50_Contagemresultado_descricao2 ;
      private String lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ;
      private String lV288WWContagemResultadoDS_75_Contagemresultado_descricao3 ;
      private String lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ;
      private String lV313WWContagemResultadoDS_100_Contagemresultado_descricao4 ;
      private String lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ;
      private String lV338WWContagemResultadoDS_125_Contagemresultado_descricao5 ;
      private String lV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ;
      private String lV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ;
      private String lV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A501ContagemResultado_OsFsOsFm ;
      private IGxSession AV91WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003W4_A146Modulo_Codigo ;
      private bool[] P003W4_n146Modulo_Codigo ;
      private int[] P003W4_A127Sistema_Codigo ;
      private int[] P003W4_A135Sistema_AreaTrabalhoCod ;
      private int[] P003W4_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P003W4_n830AreaTrabalho_ServicoPadrao ;
      private int[] P003W4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003W4_n1553ContagemResultado_CntSrvCod ;
      private int[] P003W4_A1603ContagemResultado_CntCod ;
      private bool[] P003W4_n1603ContagemResultado_CntCod ;
      private short[] P003W4_A1583ContagemResultado_TipoRegistro ;
      private int[] P003W4_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003W4_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003W4_A1043ContagemResultado_LiqLogCod ;
      private bool[] P003W4_n1043ContagemResultado_LiqLogCod ;
      private String[] P003W4_A605Servico_Sigla ;
      private bool[] P003W4_n605Servico_Sigla ;
      private String[] P003W4_A1612ContagemResultado_CntNum ;
      private bool[] P003W4_n1612ContagemResultado_CntNum ;
      private String[] P003W4_A803ContagemResultado_ContratadaSigla ;
      private bool[] P003W4_n803ContagemResultado_ContratadaSigla ;
      private String[] P003W4_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003W4_n509ContagemrResultado_SistemaSigla ;
      private String[] P003W4_A501ContagemResultado_OsFsOsFm ;
      private String[] P003W4_A53Contratada_AreaTrabalhoDes ;
      private bool[] P003W4_n53Contratada_AreaTrabalhoDes ;
      private String[] P003W4_A494ContagemResultado_Descricao ;
      private bool[] P003W4_n494ContagemResultado_Descricao ;
      private String[] P003W4_A1046ContagemResultado_Agrupador ;
      private bool[] P003W4_n1046ContagemResultado_Agrupador ;
      private bool[] P003W4_A598ContagemResultado_Baseline ;
      private bool[] P003W4_n598ContagemResultado_Baseline ;
      private int[] P003W4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003W4_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P003W4_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P003W4_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P003W4_A489ContagemResultado_SistemaCod ;
      private bool[] P003W4_n489ContagemResultado_SistemaCod ;
      private int[] P003W4_A508ContagemResultado_Owner ;
      private int[] P003W4_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] P003W4_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] P003W4_A601ContagemResultado_Servico ;
      private bool[] P003W4_n601ContagemResultado_Servico ;
      private String[] P003W4_A484ContagemResultado_StatusDmn ;
      private bool[] P003W4_n484ContagemResultado_StatusDmn ;
      private DateTime[] P003W4_A1351ContagemResultado_DataPrevista ;
      private bool[] P003W4_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P003W4_A471ContagemResultado_DataDmn ;
      private int[] P003W4_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P003W4_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P003W4_A490ContagemResultado_ContratadaCod ;
      private bool[] P003W4_n490ContagemResultado_ContratadaCod ;
      private int[] P003W4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003W4_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P003W4_A684ContagemResultado_PFBFSUltima ;
      private String[] P003W4_A457ContagemResultado_Demanda ;
      private bool[] P003W4_n457ContagemResultado_Demanda ;
      private String[] P003W4_A493ContagemResultado_DemandaFM ;
      private bool[] P003W4_n493ContagemResultado_DemandaFM ;
      private int[] P003W4_A510ContagemResultado_EsforcoSoma ;
      private bool[] P003W4_n510ContagemResultado_EsforcoSoma ;
      private int[] P003W4_A584ContagemResultado_ContadorFM ;
      private short[] P003W4_A531ContagemResultado_StatusUltCnt ;
      private DateTime[] P003W4_A566ContagemResultado_DataUltCnt ;
      private int[] P003W4_A456ContagemResultado_Codigo ;
      private String aP33_Filename ;
      private String aP34_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV142Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV90Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV168TFContagemResultado_StatusUltCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV165TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPGridState AV58GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV59GridStateDynamicFilter ;
      private SdtSDT_FiltroConsContadorFM AV72SDT_FiltroConsContadorFM ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class exportwwcontagemresultado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003W4( IGxContext context ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             IGxCollection AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV142Codigos ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV90Contratadas ,
                                             int AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 ,
                                             short AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 ,
                                             String AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ,
                                             DateTime AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 ,
                                             DateTime AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1 ,
                                             DateTime AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 ,
                                             String AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1 ,
                                             String AV226WWContagemResultadoDS_13_Outrosstatus1 ,
                                             int AV228WWContagemResultadoDS_15_Contagemresultado_servico1 ,
                                             int AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 ,
                                             int AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1 ,
                                             int AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1 ,
                                             int AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 ,
                                             int AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 ,
                                             String AV235WWContagemResultadoDS_22_Contagemresultado_baseline1 ,
                                             int AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 ,
                                             String AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1 ,
                                             String AV238WWContagemResultadoDS_25_Contagemresultado_descricao1 ,
                                             int AV239WWContagemResultadoDS_26_Contagemresultado_codigo1 ,
                                             bool AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 ,
                                             String AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 ,
                                             short AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 ,
                                             String AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ,
                                             DateTime AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2 ,
                                             DateTime AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 ,
                                             DateTime AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2 ,
                                             DateTime AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 ,
                                             String AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2 ,
                                             String AV251WWContagemResultadoDS_38_Outrosstatus2 ,
                                             int AV253WWContagemResultadoDS_40_Contagemresultado_servico2 ,
                                             int AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 ,
                                             int AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2 ,
                                             int AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2 ,
                                             int AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 ,
                                             int AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 ,
                                             String AV260WWContagemResultadoDS_47_Contagemresultado_baseline2 ,
                                             int AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 ,
                                             String AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2 ,
                                             String AV263WWContagemResultadoDS_50_Contagemresultado_descricao2 ,
                                             int AV264WWContagemResultadoDS_51_Contagemresultado_codigo2 ,
                                             bool AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 ,
                                             String AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 ,
                                             short AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 ,
                                             String AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ,
                                             DateTime AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3 ,
                                             DateTime AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 ,
                                             DateTime AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3 ,
                                             DateTime AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 ,
                                             String AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3 ,
                                             String AV276WWContagemResultadoDS_63_Outrosstatus3 ,
                                             int AV278WWContagemResultadoDS_65_Contagemresultado_servico3 ,
                                             int AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 ,
                                             int AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3 ,
                                             int AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3 ,
                                             int AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 ,
                                             int AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 ,
                                             String AV285WWContagemResultadoDS_72_Contagemresultado_baseline3 ,
                                             int AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 ,
                                             String AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3 ,
                                             String AV288WWContagemResultadoDS_75_Contagemresultado_descricao3 ,
                                             int AV289WWContagemResultadoDS_76_Contagemresultado_codigo3 ,
                                             bool AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 ,
                                             String AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 ,
                                             short AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 ,
                                             String AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ,
                                             DateTime AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4 ,
                                             DateTime AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 ,
                                             DateTime AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4 ,
                                             DateTime AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 ,
                                             String AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4 ,
                                             String AV301WWContagemResultadoDS_88_Outrosstatus4 ,
                                             int AV303WWContagemResultadoDS_90_Contagemresultado_servico4 ,
                                             int AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 ,
                                             int AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4 ,
                                             int AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4 ,
                                             int AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 ,
                                             int AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 ,
                                             String AV310WWContagemResultadoDS_97_Contagemresultado_baseline4 ,
                                             int AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 ,
                                             String AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4 ,
                                             String AV313WWContagemResultadoDS_100_Contagemresultado_descricao4 ,
                                             int AV314WWContagemResultadoDS_101_Contagemresultado_codigo4 ,
                                             bool AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 ,
                                             String AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 ,
                                             short AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 ,
                                             String AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ,
                                             DateTime AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5 ,
                                             DateTime AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 ,
                                             DateTime AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5 ,
                                             DateTime AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 ,
                                             String AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5 ,
                                             String AV326WWContagemResultadoDS_113_Outrosstatus5 ,
                                             int AV328WWContagemResultadoDS_115_Contagemresultado_servico5 ,
                                             int AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 ,
                                             int AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5 ,
                                             int AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5 ,
                                             int AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 ,
                                             int AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 ,
                                             String AV335WWContagemResultadoDS_122_Contagemresultado_baseline5 ,
                                             int AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 ,
                                             String AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5 ,
                                             String AV338WWContagemResultadoDS_125_Contagemresultado_descricao5 ,
                                             int AV339WWContagemResultadoDS_126_Contagemresultado_codigo5 ,
                                             String AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel ,
                                             String AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ,
                                             DateTime AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn ,
                                             DateTime AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista ,
                                             DateTime AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to ,
                                             String AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ,
                                             String AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel ,
                                             String AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao ,
                                             String AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ,
                                             String AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ,
                                             String AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel ,
                                             String AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ,
                                             int AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV360WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel ,
                                             int AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel ,
                                             String AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico ,
                                             int AV142Codigos_Count ,
                                             int AV90Contratadas_Count ,
                                             int AV18Contratada_AreaTrabalhoCod ,
                                             bool AV72SDT_FiltroConsContadorFM_gxTpr_Abertas ,
                                             bool AV72SDT_FiltroConsContadorFM_gxTpr_Solicitadas ,
                                             bool AV72SDT_FiltroConsContadorFM_gxTpr_Soconfirmadas ,
                                             int AV72SDT_FiltroConsContadorFM_gxTpr_Servico ,
                                             int AV72SDT_FiltroConsContadorFM_gxTpr_Sistema ,
                                             bool AV72SDT_FiltroConsContadorFM_gxTpr_Semliquidar ,
                                             int AV72SDT_FiltroConsContadorFM_gxTpr_Lote ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             int AV9WWPContext_gxTpr_Contratada_pessoacod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A1443ContagemResultado_CntSrvPrrCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A805ContagemResultado_ContratadaOrigemCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A510ContagemResultado_EsforcoSoma ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A1612ContagemResultado_CntNum ,
                                             String A605Servico_Sigla ,
                                             int A1043ContagemResultado_LiqLogCod ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             short AV16OrderedBy ,
                                             bool AV17OrderedDsc ,
                                             DateTime AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 ,
                                             short AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 ,
                                             int AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             DateTime AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 ,
                                             DateTime AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 ,
                                             short AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 ,
                                             int AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2 ,
                                             DateTime AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 ,
                                             DateTime AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 ,
                                             short AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 ,
                                             int AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3 ,
                                             DateTime AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 ,
                                             DateTime AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 ,
                                             short AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 ,
                                             int AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4 ,
                                             DateTime AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 ,
                                             DateTime AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 ,
                                             short AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 ,
                                             int AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5 ,
                                             DateTime AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to ,
                                             int AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels_Count ,
                                             String AV364WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel ,
                                             String AV363WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal ,
                                             String A486ContagemResultado_EsforcoTotal ,
                                             decimal AV365WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV366WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ,
                                             int AV58GridState_gxTpr_Dynamicfilters_Count ,
                                             int AV72SDT_FiltroConsContadorFM_gxTpr_Contagemresultado_contadorfmcod ,
                                             DateTime Gx_date ,
                                             bool AV88Usuario_EhGestor ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             bool AV9WWPContext_gxTpr_Userehcontratante ,
                                             short AV9WWPContext_gxTpr_Userid ,
                                             short AV72SDT_FiltroConsContadorFM_gxTpr_Ano ,
                                             long AV72SDT_FiltroConsContadorFM_gxTpr_Mes ,
                                             bool AV72SDT_FiltroConsContadorFM_gxTpr_Comerro ,
                                             short A585ContagemResultado_Erros ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [255] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_LiqLogCod], T5.[Servico_Sigla], T7.[Contrato_Numero] AS ContagemResultado_CntNum, T9.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T8.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, RTRIM(LTRIM(COALESCE( T1.[ContagemResultado_Demanda], ''))) + CASE  WHEN (COALESCE( T1.[ContagemResultado_DemandaFM], '') = '') THEN '' ELSE '|' + RTRIM(LTRIM(COALESCE( T1.[ContagemResultado_DemandaFM], ''))) END AS ContagemResultado_OsFsOsFm, T10.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Baseline], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_CntSrvPrrCod], T6.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T9.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T9.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, COALESCE( T11.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T11.[ContagemResultado_ContadorFM],";
         scmdbuf = scmdbuf + " 0) AS ContagemResultado_ContadorFM, COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_Codigo] FROM ((((((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T7 WITH (NOLOCK) ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) LEFT JOIN [Sistema] T8 WITH (NOLOCK) ON T8.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T10 WITH (NOLOCK) ON T10.[AreaTrabalho_Codigo] = T9.[Contratada_AreaTrabalhoCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T11 ON T11.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo]";
         scmdbuf = scmdbuf + " FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T12 ON T12.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV216WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV241WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV266WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV291WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV316WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and ((@AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (@AV359WWCCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV359WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and (Not ( @AV58Grid_7DynamicfiltersCount < 1 and @AV142Codigos_Count = 0 and (@AV72SDT__6Contagemresultado_c = convert(int, 0)) and (@AV72SDT__5Sistema = convert(int, 0)) and (@AV72SDT__4Lote = convert(int, 0)) and @AV72SDT__3Solicitadas = 0 and @AV72SDT__2Semliquidar = 0) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) = @Gx_date))";
         scmdbuf = scmdbuf + " and (Not ( @AV142Codigos_Count = 0 and Not (@AV72SDT__6Contagemresultado_c = convert(int, 0))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV72SDT__6Contagemresultado_c))";
         scmdbuf = scmdbuf + " and (Not ( @AV142Codigos_Count = 0 and Not (@AV72SDT__6Contagemresultado_c = convert(int, 0)) and @AV72SDT__9Ano + @AV72SDT__8Mes = 0 and Not @AV88Usuario_EhGestor = 1) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV72SDT__6Contagemresultado_c))";
         scmdbuf = scmdbuf + " and (Not ( @AV142Codigos_Count = 0 and Not (@AV72SDT__8Mes = convert(int, 0))) or ( MONTH(COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV72SDT__8Mes))";
         scmdbuf = scmdbuf + " and (Not ( @AV142Codigos_Count = 0 and Not (@AV72SDT__9Ano = convert(int, 0))) or ( YEAR(COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV72SDT__9Ano))";
         scmdbuf = scmdbuf + " and (Not ( @AV142Codigos_Count = 0 and @AV72SDT__10Soconfirmadas = 1) or ( COALESCE( T11.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_AreaTrabalhoCod] = @AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int5[111] = 1;
         }
         if ( ( AV9WWPContext_gxTpr_Contratada_codigo > 0 ) && ( AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod > 0 ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_11Contratada_codigo)";
         }
         else
         {
            GXv_int5[112] = 1;
         }
         if ( ( AV9WWPContext_gxTpr_Contratada_codigo > 0 ) && (0==AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_PessoaCod] = @AV9WWPCo_12Contratada_pessoac)";
         }
         else
         {
            GXv_int5[113] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[114] = 1;
            GXv_int5[115] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[116] = 1;
            GXv_int5[117] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[118] = 1;
            GXv_int5[119] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int5[120] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int5[121] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int5[122] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int5[123] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int5[124] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226WWContagemResultadoDS_13_Outrosstatus1)) && ( StringUtil.StrCmp(AV226WWContagemResultadoDS_13_Outrosstatus1, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV226WWContagemResultadoDS_13_Outrosstatus1)) > 0)";
         }
         else
         {
            GXv_int5[125] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV228WWContagemResultadoDS_15_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV228WWContagemResultadoDS_15_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int5[126] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1)";
         }
         else
         {
            GXv_int5[127] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int5[128] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1) && ! (0==AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int5[129] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1)";
         }
         else
         {
            GXv_int5[130] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int5[131] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV235WWContagemResultadoDS_22_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV235WWContagemResultadoDS_22_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int5[132] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int5[133] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV217WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int5[134] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int5[135] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV238WWContagemResultadoDS_25_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV238WWContagemResultadoDS_25_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int5[136] = 1;
         }
         if ( ( StringUtil.StrCmp(AV216WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV239WWContagemResultadoDS_26_Contagemresultado_codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV239WWContagemResultadoDS_26_Contagemresultado_codigo1)";
         }
         else
         {
            GXv_int5[137] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[138] = 1;
            GXv_int5[139] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[140] = 1;
            GXv_int5[141] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[142] = 1;
            GXv_int5[143] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int5[144] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int5[145] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int5[146] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int5[147] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int5[148] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV251WWContagemResultadoDS_38_Outrosstatus2)) && ( StringUtil.StrCmp(AV251WWContagemResultadoDS_38_Outrosstatus2, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV251WWContagemResultadoDS_38_Outrosstatus2)) > 0)";
         }
         else
         {
            GXv_int5[149] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV253WWContagemResultadoDS_40_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV253WWContagemResultadoDS_40_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int5[150] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2)";
         }
         else
         {
            GXv_int5[151] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int5[152] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2) && ! (0==AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int5[153] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2)";
         }
         else
         {
            GXv_int5[154] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int5[155] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV260WWContagemResultadoDS_47_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV260WWContagemResultadoDS_47_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int5[156] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int5[157] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV242WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int5[158] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int5[159] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV263WWContagemResultadoDS_50_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV263WWContagemResultadoDS_50_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int5[160] = 1;
         }
         if ( AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV241WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV264WWContagemResultadoDS_51_Contagemresultado_codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV264WWContagemResultadoDS_51_Contagemresultado_codigo2)";
         }
         else
         {
            GXv_int5[161] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[162] = 1;
            GXv_int5[163] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[164] = 1;
            GXv_int5[165] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[166] = 1;
            GXv_int5[167] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int5[168] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int5[169] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int5[170] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int5[171] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int5[172] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV276WWContagemResultadoDS_63_Outrosstatus3)) && ( StringUtil.StrCmp(AV276WWContagemResultadoDS_63_Outrosstatus3, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV276WWContagemResultadoDS_63_Outrosstatus3)) > 0)";
         }
         else
         {
            GXv_int5[173] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV278WWContagemResultadoDS_65_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV278WWContagemResultadoDS_65_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int5[174] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3)";
         }
         else
         {
            GXv_int5[175] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int5[176] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3) && ! (0==AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int5[177] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3)";
         }
         else
         {
            GXv_int5[178] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int5[179] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV285WWContagemResultadoDS_72_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV285WWContagemResultadoDS_72_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int5[180] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int5[181] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV267WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int5[182] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int5[183] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV288WWContagemResultadoDS_75_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV288WWContagemResultadoDS_75_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int5[184] = 1;
         }
         if ( AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV266WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV289WWContagemResultadoDS_76_Contagemresultado_codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV289WWContagemResultadoDS_76_Contagemresultado_codigo3)";
         }
         else
         {
            GXv_int5[185] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[186] = 1;
            GXv_int5[187] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[188] = 1;
            GXv_int5[189] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[190] = 1;
            GXv_int5[191] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int5[192] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int5[193] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int5[194] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int5[195] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int5[196] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV301WWContagemResultadoDS_88_Outrosstatus4)) && ( StringUtil.StrCmp(AV301WWContagemResultadoDS_88_Outrosstatus4, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV301WWContagemResultadoDS_88_Outrosstatus4)) > 0)";
         }
         else
         {
            GXv_int5[197] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV303WWContagemResultadoDS_90_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV303WWContagemResultadoDS_90_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int5[198] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4)";
         }
         else
         {
            GXv_int5[199] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int5[200] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4) && ! (0==AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int5[201] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4)";
         }
         else
         {
            GXv_int5[202] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int5[203] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV310WWContagemResultadoDS_97_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV310WWContagemResultadoDS_97_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! (0==AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4)";
         }
         else
         {
            GXv_int5[204] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! (0==AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4)";
         }
         else
         {
            GXv_int5[205] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV292WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! (0==AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4)";
         }
         else
         {
            GXv_int5[206] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int5[207] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV313WWContagemResultadoDS_100_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV313WWContagemResultadoDS_100_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int5[208] = 1;
         }
         if ( AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV291WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV314WWContagemResultadoDS_101_Contagemresultado_codigo4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV314WWContagemResultadoDS_101_Contagemresultado_codigo4)";
         }
         else
         {
            GXv_int5[209] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[210] = 1;
            GXv_int5[211] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[212] = 1;
            GXv_int5[213] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[214] = 1;
            GXv_int5[215] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int5[216] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int5[217] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int5[218] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int5[219] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int5[220] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV326WWContagemResultadoDS_113_Outrosstatus5)) && ( StringUtil.StrCmp(AV326WWContagemResultadoDS_113_Outrosstatus5, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV326WWContagemResultadoDS_113_Outrosstatus5)) > 0)";
         }
         else
         {
            GXv_int5[221] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV328WWContagemResultadoDS_115_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV328WWContagemResultadoDS_115_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int5[222] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5)";
         }
         else
         {
            GXv_int5[223] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int5[224] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5) && ! (0==AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int5[225] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5)";
         }
         else
         {
            GXv_int5[226] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int5[227] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV335WWContagemResultadoDS_122_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV335WWContagemResultadoDS_122_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! (0==AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5)";
         }
         else
         {
            GXv_int5[228] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! (0==AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5)";
         }
         else
         {
            GXv_int5[229] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV317WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! (0==AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5)";
         }
         else
         {
            GXv_int5[230] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int5[231] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV338WWContagemResultadoDS_125_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV338WWContagemResultadoDS_125_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int5[232] = 1;
         }
         if ( AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV316WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV339WWContagemResultadoDS_126_Contagemresultado_codigo5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV339WWContagemResultadoDS_126_Contagemresultado_codigo5)";
         }
         else
         {
            GXv_int5[233] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes)) ) )
         {
            sWhereString = sWhereString + " and (T10.[AreaTrabalho_Descricao] like @lV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes)";
         }
         else
         {
            GXv_int5[234] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel)) )
         {
            sWhereString = sWhereString + " and (T10.[AreaTrabalho_Descricao] = @AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel)";
         }
         else
         {
            GXv_int5[235] = 1;
         }
         if ( ! (DateTime.MinValue==AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int5[236] = 1;
         }
         if ( ! (DateTime.MinValue==AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int5[237] = 1;
         }
         if ( ! (DateTime.MinValue==AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int5[238] = 1;
         }
         if ( ! (DateTime.MinValue==AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int5[239] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int5[240] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int5[241] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int5[242] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int5[243] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T8.[Sistema_Sigla] like @lV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int5[244] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T8.[Sistema_Sigla] = @AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int5[245] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_Sigla] like @lV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int5[246] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_Sigla] = @AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int5[247] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Contrato_Numero] like @lV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum)";
         }
         else
         {
            GXv_int5[248] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Contrato_Numero] = @AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel)";
         }
         else
         {
            GXv_int5[249] = 1;
         }
         if ( AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV358WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV360WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV360WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( (0==AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV361WWContagemResultadoDS_148_Tfcontagemresultado_servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV361WWContagemResultadoDS_148_Tfcontagemresultado_servico)";
         }
         else
         {
            GXv_int5[250] = 1;
         }
         if ( ! (0==AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel)";
         }
         else
         {
            GXv_int5[251] = 1;
         }
         if ( AV142Codigos_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         }
         if ( ( AV142Codigos_Count == 0 ) && ( AV90Contratadas_Count > 0 ) && (0==AV18Contratada_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV90Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( ( AV142Codigos_Count == 0 ) && ( AV72SDT_FiltroConsContadorFM_gxTpr_Abertas ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = 'A')";
         }
         if ( ( AV142Codigos_Count == 0 ) && ( AV72SDT_FiltroConsContadorFM_gxTpr_Solicitadas ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'B' or T1.[ContagemResultado_StatusDmn] = 'E')";
         }
         if ( ( AV142Codigos_Count == 0 ) && ( AV72SDT_FiltroConsContadorFM_gxTpr_Soconfirmadas ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'C' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')";
         }
         if ( ( AV142Codigos_Count == 0 ) && ! (0==AV72SDT_FiltroConsContadorFM_gxTpr_Servico) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV72SDT__13Servico)";
         }
         else
         {
            GXv_int5[252] = 1;
         }
         if ( ( AV142Codigos_Count == 0 ) && ! (0==AV72SDT_FiltroConsContadorFM_gxTpr_Sistema) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV72SDT__5Sistema)";
         }
         else
         {
            GXv_int5[253] = 1;
         }
         if ( ( AV142Codigos_Count == 0 ) && ( AV72SDT_FiltroConsContadorFM_gxTpr_Semliquidar ) )
         {
            sWhereString = sWhereString + " and (( T1.[ContagemResultado_LiqLogCod] IS NULL or (T1.[ContagemResultado_LiqLogCod] = convert(int, 0))) and ( T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L'))";
         }
         if ( ( AV142Codigos_Count == 0 ) && ! (0==AV72SDT_FiltroConsContadorFM_gxTpr_Lote) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_LoteAceiteCod] = @AV72SDT__4Lote)";
         }
         else
         {
            GXv_int5[254] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV16OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo] DESC";
         }
         else if ( AV16OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV16OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn] DESC";
         }
         else if ( AV16OrderedBy == 4 )
         {
            scmdbuf = scmdbuf + " ORDER BY T11.[ContagemResultado_StatusUltCnt] DESC";
         }
         else if ( AV16OrderedBy == 5 )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_OsFsOsFm] DESC";
         }
         else if ( AV16OrderedBy == 6 )
         {
            scmdbuf = scmdbuf + " ORDER BY T8.[Sistema_Sigla] DESC";
         }
         else if ( AV16OrderedBy == 7 )
         {
            scmdbuf = scmdbuf + " ORDER BY T11.[ContagemResultado_DataUltCnt] DESC";
         }
         else if ( ( AV16OrderedBy == 8 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T10.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV16OrderedBy == 8 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T10.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV16OrderedBy == 9 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista]";
         }
         else if ( ( AV16OrderedBy == 9 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista] DESC";
         }
         else if ( ( AV16OrderedBy == 10 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao]";
         }
         else if ( ( AV16OrderedBy == 10 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao] DESC";
         }
         else if ( ( AV16OrderedBy == 11 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[Contratada_Sigla]";
         }
         else if ( ( AV16OrderedBy == 11 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[Contratada_Sigla] DESC";
         }
         else if ( ( AV16OrderedBy == 12 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[Contrato_Numero]";
         }
         else if ( ( AV16OrderedBy == 12 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[Contrato_Numero] DESC";
         }
         else if ( ( AV16OrderedBy == 13 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline]";
         }
         else if ( ( AV16OrderedBy == 13 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline] DESC";
         }
         else if ( ( AV16OrderedBy == 14 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Codigo]";
         }
         else if ( ( AV16OrderedBy == 14 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Codigo] DESC";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P003W4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (int)dynConstraints[6] , (IGxCollection)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (bool)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (String)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (int)dynConstraints[47] , (String)dynConstraints[48] , (String)dynConstraints[49] , (int)dynConstraints[50] , (bool)dynConstraints[51] , (String)dynConstraints[52] , (short)dynConstraints[53] , (String)dynConstraints[54] , (DateTime)dynConstraints[55] , (DateTime)dynConstraints[56] , (DateTime)dynConstraints[57] , (DateTime)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (int)dynConstraints[61] , (int)dynConstraints[62] , (int)dynConstraints[63] , (int)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (String)dynConstraints[69] , (String)dynConstraints[70] , (int)dynConstraints[71] , (bool)dynConstraints[72] , (String)dynConstraints[73] , (short)dynConstraints[74] , (String)dynConstraints[75] , (DateTime)dynConstraints[76] , (DateTime)dynConstraints[77] , (DateTime)dynConstraints[78] , (DateTime)dynConstraints[79] , (String)dynConstraints[80] , (String)dynConstraints[81] , (int)dynConstraints[82] , (int)dynConstraints[83] , (int)dynConstraints[84] , (int)dynConstraints[85] , (int)dynConstraints[86] , (int)dynConstraints[87] , (String)dynConstraints[88] , (int)dynConstraints[89] , (String)dynConstraints[90] , (String)dynConstraints[91] , (int)dynConstraints[92] , (bool)dynConstraints[93] , (String)dynConstraints[94] , (short)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (String)dynConstraints[101] , (String)dynConstraints[102] , (int)dynConstraints[103] , (int)dynConstraints[104] , (int)dynConstraints[105] , (int)dynConstraints[106] , (int)dynConstraints[107] , (int)dynConstraints[108] , (String)dynConstraints[109] , (int)dynConstraints[110] , (String)dynConstraints[111] , (String)dynConstraints[112] , (int)dynConstraints[113] , (String)dynConstraints[114] , (String)dynConstraints[115] , (DateTime)dynConstraints[116] , (DateTime)dynConstraints[117] , (DateTime)dynConstraints[118] , (DateTime)dynConstraints[119] , (String)dynConstraints[120] , (String)dynConstraints[121] , (String)dynConstraints[122] , (String)dynConstraints[123] , (String)dynConstraints[124] , (String)dynConstraints[125] , (String)dynConstraints[126] , (String)dynConstraints[127] , (String)dynConstraints[128] , (String)dynConstraints[129] , (int)dynConstraints[130] , (short)dynConstraints[131] , (int)dynConstraints[132] , (String)dynConstraints[133] , (int)dynConstraints[134] , (int)dynConstraints[135] , (int)dynConstraints[136] , (bool)dynConstraints[137] , (bool)dynConstraints[138] , (bool)dynConstraints[139] , (int)dynConstraints[140] , (int)dynConstraints[141] , (bool)dynConstraints[142] , (int)dynConstraints[143] , (int)dynConstraints[144] , (int)dynConstraints[145] , (int)dynConstraints[146] , (String)dynConstraints[147] , (String)dynConstraints[148] , (DateTime)dynConstraints[149] , (DateTime)dynConstraints[150] , (int)dynConstraints[151] , (int)dynConstraints[152] , (int)dynConstraints[153] , (int)dynConstraints[154] , (int)dynConstraints[155] , (bool)dynConstraints[156] , (int)dynConstraints[157] , (String)dynConstraints[158] , (String)dynConstraints[159] , (String)dynConstraints[160] , (String)dynConstraints[161] , (String)dynConstraints[162] , (String)dynConstraints[163] , (String)dynConstraints[164] , (int)dynConstraints[165] , (int)dynConstraints[166] , (short)dynConstraints[167] , (bool)dynConstraints[168] , (DateTime)dynConstraints[169] , (DateTime)dynConstraints[170] , (DateTime)dynConstraints[171] , (short)dynConstraints[172] , (int)dynConstraints[173] , (int)dynConstraints[174] , (int)dynConstraints[175] , (DateTime)dynConstraints[176] , (DateTime)dynConstraints[177] , (short)dynConstraints[178] , (int)dynConstraints[179] , (DateTime)dynConstraints[180] , (DateTime)dynConstraints[181] , (short)dynConstraints[182] , (int)dynConstraints[183] , (DateTime)dynConstraints[184] , (DateTime)dynConstraints[185] , (short)dynConstraints[186] , (int)dynConstraints[187] , (DateTime)dynConstraints[188] , (DateTime)dynConstraints[189] , (short)dynConstraints[190] , (int)dynConstraints[191] , (DateTime)dynConstraints[192] , (DateTime)dynConstraints[193] , (int)dynConstraints[194] , (String)dynConstraints[195] , (String)dynConstraints[196] , (String)dynConstraints[197] , (decimal)dynConstraints[198] , (decimal)dynConstraints[199] , (decimal)dynConstraints[200] , (int)dynConstraints[201] , (int)dynConstraints[202] , (DateTime)dynConstraints[203] , (bool)dynConstraints[204] , (bool)dynConstraints[205] , (bool)dynConstraints[206] , (short)dynConstraints[207] , (short)dynConstraints[208] , (long)dynConstraints[209] , (bool)dynConstraints[210] , (short)dynConstraints[211] , (decimal)dynConstraints[212] , (short)dynConstraints[213] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003W4 ;
          prmP003W4 = new Object[] {
          new Object[] {"@AV216WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV221WWContagemResultadoDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV222WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV227WWContagemResultadoDS_14_Contagemresultado_statusultcnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV216WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV230WWContagemResultadoDS_17_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV241WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV246WWContagemResultadoDS_33_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV241WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV241WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV252WWContagemResultadoDS_39_Contagemresultado_statusultcnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV240WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV241WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255WWContagemResultadoDS_42_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV266WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV271WWContagemResultadoDS_58_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV266WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV272WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV266WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV277WWContagemResultadoDS_64_Contagemresultado_statusultcnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV265WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV266WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV280WWContagemResultadoDS_67_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV291WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV296WWContagemResultadoDS_83_Contagemresultado_dataultcnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV291WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV297WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV291WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV302WWContagemResultadoDS_89_Contagemresultado_statusultcnt4",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV290WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV291WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV305WWContagemResultadoDS_92_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV316WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV321WWContagemResultadoDS_108_Contagemresultado_dataultcnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV316WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV322WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV316WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV327WWContagemResultadoDS_114_Contagemresultado_statusultcnt5",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV315WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV316WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV330WWContagemResultadoDS_117_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV344WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV345WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV359WWCCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV58Grid_7DynamicfiltersCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV142Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72SDT__6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__5Sistema",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__4Lote",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__3Solicitadas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV72SDT__2Semliquidar",SqlDbType.Bit,4,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV142Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72SDT__6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72SDT__6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__9Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV72SDT__8Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV88Usuario_EhGestor",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV72SDT__6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72SDT__8Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV72SDT__8Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV142Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72SDT__9Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV72SDT__9Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV142Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72SDT__10Soconfirmadas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV214WWContagemResultadoDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_11Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_12Contratada_pessoac",SqlDbType.Int,6,0} ,
          new Object[] {"@AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV219WWContagemResultadoDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV220WWContagemResultadoDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV223WWContagemResultadoDS_10_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV224WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV225WWContagemResultadoDS_12_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV226WWContagemResultadoDS_13_Outrosstatus1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV228WWContagemResultadoDS_15_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV231WWContagemResultadoDS_18_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232WWContagemResultadoDS_19_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV233WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV234WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV236WWContagemResultadoDS_23_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV237WWContagemResultadoDS_24_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV238WWContagemResultadoDS_25_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV239WWContagemResultadoDS_26_Contagemresultado_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV243WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV244WWContagemResultadoDS_31_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV245WWContagemResultadoDS_32_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248WWContagemResultadoDS_35_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250WWContagemResultadoDS_37_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV251WWContagemResultadoDS_38_Outrosstatus2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV253WWContagemResultadoDS_40_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV256WWContagemResultadoDS_43_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV257WWContagemResultadoDS_44_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV258WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV259WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV261WWContagemResultadoDS_48_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV262WWContagemResultadoDS_49_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV263WWContagemResultadoDS_50_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV264WWContagemResultadoDS_51_Contagemresultado_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV268WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV269WWContagemResultadoDS_56_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV270WWContagemResultadoDS_57_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV273WWContagemResultadoDS_60_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV274WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV275WWContagemResultadoDS_62_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV276WWContagemResultadoDS_63_Outrosstatus3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV278WWContagemResultadoDS_65_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV279WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV281WWContagemResultadoDS_68_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV282WWContagemResultadoDS_69_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV283WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV284WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV286WWContagemResultadoDS_73_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV287WWContagemResultadoDS_74_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV288WWContagemResultadoDS_75_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV289WWContagemResultadoDS_76_Contagemresultado_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV293WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV294WWContagemResultadoDS_81_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV295WWContagemResultadoDS_82_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV298WWContagemResultadoDS_85_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV299WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV300WWContagemResultadoDS_87_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV301WWContagemResultadoDS_88_Outrosstatus4",SqlDbType.Char,20,0} ,
          new Object[] {"@AV303WWContagemResultadoDS_90_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV304WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV306WWContagemResultadoDS_93_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV307WWContagemResultadoDS_94_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV308WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV309WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4",SqlDbType.Int,8,0} ,
          new Object[] {"@AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4",SqlDbType.Int,8,0} ,
          new Object[] {"@AV311WWContagemResultadoDS_98_Contagemresultado_esforcosoma4",SqlDbType.Int,8,0} ,
          new Object[] {"@AV312WWContagemResultadoDS_99_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV313WWContagemResultadoDS_100_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV314WWContagemResultadoDS_101_Contagemresultado_codigo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV318WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV319WWContagemResultadoDS_106_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV320WWContagemResultadoDS_107_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV323WWContagemResultadoDS_110_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV324WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV325WWContagemResultadoDS_112_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV326WWContagemResultadoDS_113_Outrosstatus5",SqlDbType.Char,20,0} ,
          new Object[] {"@AV328WWContagemResultadoDS_115_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV329WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV331WWContagemResultadoDS_118_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV332WWContagemResultadoDS_119_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV333WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV334WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5",SqlDbType.Int,8,0} ,
          new Object[] {"@AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5",SqlDbType.Int,8,0} ,
          new Object[] {"@AV336WWContagemResultadoDS_123_Contagemresultado_esforcosoma5",SqlDbType.Int,8,0} ,
          new Object[] {"@AV337WWContagemResultadoDS_124_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV338WWContagemResultadoDS_125_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV339WWContagemResultadoDS_126_Contagemresultado_codigo5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV340WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV341WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV342WWContagemResultadoDS_129_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV343WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV346WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV347WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV348WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV349WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV350WWContagemResultadoDS_137_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV351WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV352WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV353WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV354WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV355WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV356WWContagemResultadoDS_143_Tfcontagemresultado_cntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV357WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV361WWContagemResultadoDS_148_Tfcontagemresultado_servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV362WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__13Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__5Sistema",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72SDT__4Lote",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003W4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003W4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 25) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((String[]) buf[24])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((int[]) buf[34])[0] = rslt.getInt(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((int[]) buf[36])[0] = rslt.getInt(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((int[]) buf[38])[0] = rslt.getInt(22) ;
                ((int[]) buf[39])[0] = rslt.getInt(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((int[]) buf[41])[0] = rslt.getInt(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getString(25, 1) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[45])[0] = rslt.getGXDateTime(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((DateTime[]) buf[47])[0] = rslt.getGXDate(27) ;
                ((int[]) buf[48])[0] = rslt.getInt(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((int[]) buf[50])[0] = rslt.getInt(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((decimal[]) buf[54])[0] = rslt.getDecimal(31) ;
                ((String[]) buf[55])[0] = rslt.getVarchar(32) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(32);
                ((String[]) buf[57])[0] = rslt.getVarchar(33) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(33);
                ((int[]) buf[59])[0] = rslt.getInt(34) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(34);
                ((int[]) buf[61])[0] = rslt.getInt(35) ;
                ((short[]) buf[62])[0] = rslt.getShort(36) ;
                ((DateTime[]) buf[63])[0] = rslt.getGXDate(37) ;
                ((int[]) buf[64])[0] = rslt.getInt(38) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[255]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[256]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[257]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[259]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[260]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[262]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[263]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[265]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[266]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[267]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[268]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[270]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[271]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[272]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[273]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[274]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[275]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[276]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[277]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[278]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[279]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[280]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[281]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[282]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[283]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[284]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[285]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[286]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[287]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[288]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[289]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[290]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[291]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[292]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[293]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[294]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[295]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[296]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[297]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[298]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[299]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[300]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[301]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[302]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[304]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[305]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[306]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[308]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[309]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[310]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[311]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[312]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[313]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[314]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[315]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[318]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[319]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[320]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[321]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[322]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[323]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[325]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[326]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[327]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[329]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[330]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[331]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[332]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[334]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[336]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[337]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[338]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[339]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[340]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[341]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[342]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[343]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[344]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[345]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[346]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[347]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[348]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[349]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[350]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[351]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[352]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[354]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[355]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[356]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[358]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[359]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[360]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[361]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[362]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[363]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[364]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[365]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[366]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[367]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[368]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[369]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[370]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[371]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[372]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[373]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[374]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[375]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[376]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[377]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[378]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[380]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[381]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[382]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[383]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[384]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[385]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[386]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[387]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[388]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[389]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[390]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[391]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[392]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[393]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[394]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[395]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[396]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[397]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[398]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[399]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[400]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[401]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[402]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[403]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[405]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[406]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[407]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[408]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[409]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[410]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[411]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[412]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[413]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[414]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[415]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[416]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[420]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[421]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[422]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[423]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[424]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[425]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[426]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[427]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[428]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[429]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[430]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[431]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[432]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[433]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[434]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[435]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[436]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[437]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[438]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[439]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[440]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[441]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[442]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[443]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[444]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[445]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[446]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[447]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[448]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[449]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[450]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[451]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[452]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[453]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[454]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[455]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[456]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[457]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[458]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[459]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[460]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[461]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[462]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[463]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[464]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[465]);
                }
                if ( (short)parms[211] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[466]);
                }
                if ( (short)parms[212] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[467]);
                }
                if ( (short)parms[213] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[468]);
                }
                if ( (short)parms[214] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[469]);
                }
                if ( (short)parms[215] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[470]);
                }
                if ( (short)parms[216] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[471]);
                }
                if ( (short)parms[217] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[472]);
                }
                if ( (short)parms[218] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[473]);
                }
                if ( (short)parms[219] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[474]);
                }
                if ( (short)parms[220] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[475]);
                }
                if ( (short)parms[221] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[476]);
                }
                if ( (short)parms[222] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[477]);
                }
                if ( (short)parms[223] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[478]);
                }
                if ( (short)parms[224] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[479]);
                }
                if ( (short)parms[225] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[480]);
                }
                if ( (short)parms[226] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[481]);
                }
                if ( (short)parms[227] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[482]);
                }
                if ( (short)parms[228] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[483]);
                }
                if ( (short)parms[229] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[484]);
                }
                if ( (short)parms[230] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[485]);
                }
                if ( (short)parms[231] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[486]);
                }
                if ( (short)parms[232] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[487]);
                }
                if ( (short)parms[233] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[488]);
                }
                if ( (short)parms[234] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[489]);
                }
                if ( (short)parms[235] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[490]);
                }
                if ( (short)parms[236] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[491]);
                }
                if ( (short)parms[237] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[492]);
                }
                if ( (short)parms[238] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[493]);
                }
                if ( (short)parms[239] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[494]);
                }
                if ( (short)parms[240] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[495]);
                }
                if ( (short)parms[241] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[496]);
                }
                if ( (short)parms[242] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[497]);
                }
                if ( (short)parms[243] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[498]);
                }
                if ( (short)parms[244] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[499]);
                }
                if ( (short)parms[245] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[500]);
                }
                if ( (short)parms[246] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[501]);
                }
                if ( (short)parms[247] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[502]);
                }
                if ( (short)parms[248] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[503]);
                }
                if ( (short)parms[249] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[504]);
                }
                if ( (short)parms[250] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[505]);
                }
                if ( (short)parms[251] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[506]);
                }
                if ( (short)parms[252] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[507]);
                }
                if ( (short)parms[253] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[508]);
                }
                if ( (short)parms[254] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[509]);
                }
                return;
       }
    }

 }

}
