/*
               File: Gestao
        Description: Gest�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:54.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gestao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"GESTAO_CONTRATADACOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAGESTAO_CONTRATADACOD3U175( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A1484Gestao_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A1484Gestao_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A1485Gestao_AreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1485Gestao_AreaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A1485Gestao_AreaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A1489Gestao_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1489Gestao_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A1489Gestao_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A1490Gestao_GestorCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1490Gestao_GestorCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A1490Gestao_GestorCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynGestao_ContratadaCod.Name = "GESTAO_CONTRATADACOD";
         dynGestao_ContratadaCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Gest�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGestao_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public gestao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gestao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynGestao_ContratadaCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynGestao_ContratadaCod.ItemCount > 0 )
         {
            A1484Gestao_ContratadaCod = (int)(NumberUtil.Val( dynGestao_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            context.WriteHtmlText( "st_gesta") ;
            wb_table1_3_3U175( true) ;
         }
         return  ;
      }

      protected void wb_table1_3_3U175e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_3_3U175( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_6_3U175( true) ;
         }
         return  ;
      }

      protected void wb_table2_6_3U175e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Gest�o", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;\""+" ", "HLP_Gestao.htm");
            wb_table3_29_3U175( true) ;
         }
         return  ;
      }

      protected void wb_table3_29_3U175e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_3U175e( true) ;
         }
         else
         {
            wb_table1_3_3U175e( false) ;
         }
      }

      protected void wb_table3_29_3U175( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_35_3U175( true) ;
         }
         return  ;
      }

      protected void wb_table4_35_3U175e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gestao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gestao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_3U175e( true) ;
         }
         else
         {
            wb_table3_29_3U175e( false) ;
         }
      }

      protected void wb_table4_35_3U175( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_codigo_Internalname, "C�digo", "", "", lblTextblockgestao_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1482Gestao_Codigo), 6, 0, ",", "")), ((edtGestao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1482Gestao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1482Gestao_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_datahora_Internalname, "Data", "", "", lblTextblockgestao_datahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtGestao_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtGestao_DataHora_Internalname, context.localUtil.TToC( A1483Gestao_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1483Gestao_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_DataHora_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_DataHora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_Gestao.htm");
            GxWebStd.gx_bitmap( context, edtGestao_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtGestao_DataHora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Gestao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_contratadacod_Internalname, "Contratada", "", "", lblTextblockgestao_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynGestao_ContratadaCod, dynGestao_ContratadaCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)), 1, dynGestao_ContratadaCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynGestao_ContratadaCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_Gestao.htm");
            dynGestao_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGestao_ContratadaCod_Internalname, "Values", (String)(dynGestao_ContratadaCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_areacod_Internalname, "�rea", "", "", lblTextblockgestao_areacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_AreaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1485Gestao_AreaCod), 6, 0, ",", "")), ((edtGestao_AreaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1485Gestao_AreaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1485Gestao_AreaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_AreaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_AreaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_vermelho_Internalname, "Vermelho", "", "", lblTextblockgestao_vermelho_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_Vermelho_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1486Gestao_Vermelho), 8, 0, ",", "")), ((edtGestao_Vermelho_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1486Gestao_Vermelho), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1486Gestao_Vermelho), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_Vermelho_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_Vermelho_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_laranja_Internalname, "Laranja", "", "", lblTextblockgestao_laranja_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_Laranja_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1487Gestao_Laranja), 8, 0, ",", "")), ((edtGestao_Laranja_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1487Gestao_Laranja), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1487Gestao_Laranja), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_Laranja_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_Laranja_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_amarelo_Internalname, "Amarelo", "", "", lblTextblockgestao_amarelo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_Amarelo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1488Gestao_Amarelo), 8, 0, ",", "")), ((edtGestao_Amarelo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1488Gestao_Amarelo), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1488Gestao_Amarelo), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_Amarelo_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_Amarelo_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_usuariocod_Internalname, "Usu�rio", "", "", lblTextblockgestao_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1489Gestao_UsuarioCod), 6, 0, ",", "")), ((edtGestao_UsuarioCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1489Gestao_UsuarioCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1489Gestao_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_UsuarioCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgestao_gestorcod_Internalname, "Gestor", "", "", lblTextblockgestao_gestorcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGestao_GestorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1490Gestao_GestorCod), 6, 0, ",", "")), ((edtGestao_GestorCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1490Gestao_GestorCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1490Gestao_GestorCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGestao_GestorCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtGestao_GestorCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Gestao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_35_3U175e( true) ;
         }
         else
         {
            wb_table4_35_3U175e( false) ;
         }
      }

      protected void wb_table2_6_3U175( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Gestao.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_6_3U175e( true) ;
         }
         else
         {
            wb_table2_6_3U175e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1482Gestao_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
               }
               else
               {
                  A1482Gestao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGestao_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtGestao_DataHora_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "GESTAO_DATAHORA");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1483Gestao_DataHora = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1483Gestao_DataHora", context.localUtil.TToC( A1483Gestao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1483Gestao_DataHora = context.localUtil.CToT( cgiGet( edtGestao_DataHora_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1483Gestao_DataHora", context.localUtil.TToC( A1483Gestao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               dynGestao_ContratadaCod.CurrentValue = cgiGet( dynGestao_ContratadaCod_Internalname);
               A1484Gestao_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynGestao_ContratadaCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_AreaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_AreaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_AREACOD");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_AreaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1485Gestao_AreaCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1485Gestao_AreaCod), 6, 0)));
               }
               else
               {
                  A1485Gestao_AreaCod = (int)(context.localUtil.CToN( cgiGet( edtGestao_AreaCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1485Gestao_AreaCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_Vermelho_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_Vermelho_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_VERMELHO");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_Vermelho_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1486Gestao_Vermelho = 0;
                  n1486Gestao_Vermelho = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1486Gestao_Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1486Gestao_Vermelho), 8, 0)));
               }
               else
               {
                  A1486Gestao_Vermelho = (int)(context.localUtil.CToN( cgiGet( edtGestao_Vermelho_Internalname), ",", "."));
                  n1486Gestao_Vermelho = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1486Gestao_Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1486Gestao_Vermelho), 8, 0)));
               }
               n1486Gestao_Vermelho = ((0==A1486Gestao_Vermelho) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_Laranja_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_Laranja_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_LARANJA");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_Laranja_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1487Gestao_Laranja = 0;
                  n1487Gestao_Laranja = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1487Gestao_Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(A1487Gestao_Laranja), 8, 0)));
               }
               else
               {
                  A1487Gestao_Laranja = (int)(context.localUtil.CToN( cgiGet( edtGestao_Laranja_Internalname), ",", "."));
                  n1487Gestao_Laranja = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1487Gestao_Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(A1487Gestao_Laranja), 8, 0)));
               }
               n1487Gestao_Laranja = ((0==A1487Gestao_Laranja) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_Amarelo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_Amarelo_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_AMARELO");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_Amarelo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1488Gestao_Amarelo = 0;
                  n1488Gestao_Amarelo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1488Gestao_Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1488Gestao_Amarelo), 8, 0)));
               }
               else
               {
                  A1488Gestao_Amarelo = (int)(context.localUtil.CToN( cgiGet( edtGestao_Amarelo_Internalname), ",", "."));
                  n1488Gestao_Amarelo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1488Gestao_Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1488Gestao_Amarelo), 8, 0)));
               }
               n1488Gestao_Amarelo = ((0==A1488Gestao_Amarelo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1489Gestao_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1489Gestao_UsuarioCod), 6, 0)));
               }
               else
               {
                  A1489Gestao_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtGestao_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1489Gestao_UsuarioCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtGestao_GestorCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGestao_GestorCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GESTAO_GESTORCOD");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_GestorCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1490Gestao_GestorCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1490Gestao_GestorCod), 6, 0)));
               }
               else
               {
                  A1490Gestao_GestorCod = (int)(context.localUtil.CToN( cgiGet( edtGestao_GestorCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1490Gestao_GestorCod), 6, 0)));
               }
               /* Read saved values. */
               Z1482Gestao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1482Gestao_Codigo"), ",", "."));
               Z1483Gestao_DataHora = context.localUtil.CToT( cgiGet( "Z1483Gestao_DataHora"), 0);
               Z1486Gestao_Vermelho = (int)(context.localUtil.CToN( cgiGet( "Z1486Gestao_Vermelho"), ",", "."));
               n1486Gestao_Vermelho = ((0==A1486Gestao_Vermelho) ? true : false);
               Z1487Gestao_Laranja = (int)(context.localUtil.CToN( cgiGet( "Z1487Gestao_Laranja"), ",", "."));
               n1487Gestao_Laranja = ((0==A1487Gestao_Laranja) ? true : false);
               Z1488Gestao_Amarelo = (int)(context.localUtil.CToN( cgiGet( "Z1488Gestao_Amarelo"), ",", "."));
               n1488Gestao_Amarelo = ((0==A1488Gestao_Amarelo) ? true : false);
               Z1484Gestao_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "Z1484Gestao_ContratadaCod"), ",", "."));
               Z1485Gestao_AreaCod = (int)(context.localUtil.CToN( cgiGet( "Z1485Gestao_AreaCod"), ",", "."));
               Z1489Gestao_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z1489Gestao_UsuarioCod"), ",", "."));
               Z1490Gestao_GestorCod = (int)(context.localUtil.CToN( cgiGet( "Z1490Gestao_GestorCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1482Gestao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3U175( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3U175( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3U0( )
      {
      }

      protected void ZM3U175( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1483Gestao_DataHora = T003U3_A1483Gestao_DataHora[0];
               Z1486Gestao_Vermelho = T003U3_A1486Gestao_Vermelho[0];
               Z1487Gestao_Laranja = T003U3_A1487Gestao_Laranja[0];
               Z1488Gestao_Amarelo = T003U3_A1488Gestao_Amarelo[0];
               Z1484Gestao_ContratadaCod = T003U3_A1484Gestao_ContratadaCod[0];
               Z1485Gestao_AreaCod = T003U3_A1485Gestao_AreaCod[0];
               Z1489Gestao_UsuarioCod = T003U3_A1489Gestao_UsuarioCod[0];
               Z1490Gestao_GestorCod = T003U3_A1490Gestao_GestorCod[0];
            }
            else
            {
               Z1483Gestao_DataHora = A1483Gestao_DataHora;
               Z1486Gestao_Vermelho = A1486Gestao_Vermelho;
               Z1487Gestao_Laranja = A1487Gestao_Laranja;
               Z1488Gestao_Amarelo = A1488Gestao_Amarelo;
               Z1484Gestao_ContratadaCod = A1484Gestao_ContratadaCod;
               Z1485Gestao_AreaCod = A1485Gestao_AreaCod;
               Z1489Gestao_UsuarioCod = A1489Gestao_UsuarioCod;
               Z1490Gestao_GestorCod = A1490Gestao_GestorCod;
            }
         }
         if ( GX_JID == -3 )
         {
            Z1482Gestao_Codigo = A1482Gestao_Codigo;
            Z1483Gestao_DataHora = A1483Gestao_DataHora;
            Z1486Gestao_Vermelho = A1486Gestao_Vermelho;
            Z1487Gestao_Laranja = A1487Gestao_Laranja;
            Z1488Gestao_Amarelo = A1488Gestao_Amarelo;
            Z1484Gestao_ContratadaCod = A1484Gestao_ContratadaCod;
            Z1485Gestao_AreaCod = A1485Gestao_AreaCod;
            Z1489Gestao_UsuarioCod = A1489Gestao_UsuarioCod;
            Z1490Gestao_GestorCod = A1490Gestao_GestorCod;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAGESTAO_CONTRATADACOD_html3U175( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3U175( )
      {
         /* Using cursor T003U8 */
         pr_default.execute(6, new Object[] {A1482Gestao_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound175 = 1;
            A1483Gestao_DataHora = T003U8_A1483Gestao_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1483Gestao_DataHora", context.localUtil.TToC( A1483Gestao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A1486Gestao_Vermelho = T003U8_A1486Gestao_Vermelho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1486Gestao_Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1486Gestao_Vermelho), 8, 0)));
            n1486Gestao_Vermelho = T003U8_n1486Gestao_Vermelho[0];
            A1487Gestao_Laranja = T003U8_A1487Gestao_Laranja[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1487Gestao_Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(A1487Gestao_Laranja), 8, 0)));
            n1487Gestao_Laranja = T003U8_n1487Gestao_Laranja[0];
            A1488Gestao_Amarelo = T003U8_A1488Gestao_Amarelo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1488Gestao_Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1488Gestao_Amarelo), 8, 0)));
            n1488Gestao_Amarelo = T003U8_n1488Gestao_Amarelo[0];
            A1484Gestao_ContratadaCod = T003U8_A1484Gestao_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)));
            A1485Gestao_AreaCod = T003U8_A1485Gestao_AreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1485Gestao_AreaCod), 6, 0)));
            A1489Gestao_UsuarioCod = T003U8_A1489Gestao_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1489Gestao_UsuarioCod), 6, 0)));
            A1490Gestao_GestorCod = T003U8_A1490Gestao_GestorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1490Gestao_GestorCod), 6, 0)));
            ZM3U175( -3) ;
         }
         pr_default.close(6);
         OnLoadActions3U175( ) ;
      }

      protected void OnLoadActions3U175( )
      {
      }

      protected void CheckExtendedTable3U175( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1483Gestao_DataHora) || ( A1483Gestao_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "GESTAO_DATAHORA");
            AnyError = 1;
            GX_FocusControl = edtGestao_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003U4 */
         pr_default.execute(2, new Object[] {A1484Gestao_ContratadaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Contratada'.", "ForeignKeyNotFound", 1, "GESTAO_CONTRATADACOD");
            AnyError = 1;
            GX_FocusControl = dynGestao_ContratadaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T003U5 */
         pr_default.execute(3, new Object[] {A1485Gestao_AreaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Area Trabalho'.", "ForeignKeyNotFound", 1, "GESTAO_AREACOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_AreaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T003U6 */
         pr_default.execute(4, new Object[] {A1489Gestao_UsuarioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Usuario'.", "ForeignKeyNotFound", 1, "GESTAO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(4);
         /* Using cursor T003U7 */
         pr_default.execute(5, new Object[] {A1490Gestao_GestorCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Usuario Gestor'.", "ForeignKeyNotFound", 1, "GESTAO_GESTORCOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_GestorCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors3U175( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_4( int A1484Gestao_ContratadaCod )
      {
         /* Using cursor T003U9 */
         pr_default.execute(7, new Object[] {A1484Gestao_ContratadaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Contratada'.", "ForeignKeyNotFound", 1, "GESTAO_CONTRATADACOD");
            AnyError = 1;
            GX_FocusControl = dynGestao_ContratadaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_5( int A1485Gestao_AreaCod )
      {
         /* Using cursor T003U10 */
         pr_default.execute(8, new Object[] {A1485Gestao_AreaCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Area Trabalho'.", "ForeignKeyNotFound", 1, "GESTAO_AREACOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_AreaCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_6( int A1489Gestao_UsuarioCod )
      {
         /* Using cursor T003U11 */
         pr_default.execute(9, new Object[] {A1489Gestao_UsuarioCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Usuario'.", "ForeignKeyNotFound", 1, "GESTAO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_7( int A1490Gestao_GestorCod )
      {
         /* Using cursor T003U12 */
         pr_default.execute(10, new Object[] {A1490Gestao_GestorCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Usuario Gestor'.", "ForeignKeyNotFound", 1, "GESTAO_GESTORCOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_GestorCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey3U175( )
      {
         /* Using cursor T003U13 */
         pr_default.execute(11, new Object[] {A1482Gestao_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound175 = 1;
         }
         else
         {
            RcdFound175 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003U3 */
         pr_default.execute(1, new Object[] {A1482Gestao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3U175( 3) ;
            RcdFound175 = 1;
            A1482Gestao_Codigo = T003U3_A1482Gestao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
            A1483Gestao_DataHora = T003U3_A1483Gestao_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1483Gestao_DataHora", context.localUtil.TToC( A1483Gestao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A1486Gestao_Vermelho = T003U3_A1486Gestao_Vermelho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1486Gestao_Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1486Gestao_Vermelho), 8, 0)));
            n1486Gestao_Vermelho = T003U3_n1486Gestao_Vermelho[0];
            A1487Gestao_Laranja = T003U3_A1487Gestao_Laranja[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1487Gestao_Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(A1487Gestao_Laranja), 8, 0)));
            n1487Gestao_Laranja = T003U3_n1487Gestao_Laranja[0];
            A1488Gestao_Amarelo = T003U3_A1488Gestao_Amarelo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1488Gestao_Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1488Gestao_Amarelo), 8, 0)));
            n1488Gestao_Amarelo = T003U3_n1488Gestao_Amarelo[0];
            A1484Gestao_ContratadaCod = T003U3_A1484Gestao_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)));
            A1485Gestao_AreaCod = T003U3_A1485Gestao_AreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1485Gestao_AreaCod), 6, 0)));
            A1489Gestao_UsuarioCod = T003U3_A1489Gestao_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1489Gestao_UsuarioCod), 6, 0)));
            A1490Gestao_GestorCod = T003U3_A1490Gestao_GestorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1490Gestao_GestorCod), 6, 0)));
            Z1482Gestao_Codigo = A1482Gestao_Codigo;
            sMode175 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3U175( ) ;
            if ( AnyError == 1 )
            {
               RcdFound175 = 0;
               InitializeNonKey3U175( ) ;
            }
            Gx_mode = sMode175;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound175 = 0;
            InitializeNonKey3U175( ) ;
            sMode175 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode175;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3U175( ) ;
         if ( RcdFound175 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound175 = 0;
         /* Using cursor T003U14 */
         pr_default.execute(12, new Object[] {A1482Gestao_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T003U14_A1482Gestao_Codigo[0] < A1482Gestao_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T003U14_A1482Gestao_Codigo[0] > A1482Gestao_Codigo ) ) )
            {
               A1482Gestao_Codigo = T003U14_A1482Gestao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
               RcdFound175 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound175 = 0;
         /* Using cursor T003U15 */
         pr_default.execute(13, new Object[] {A1482Gestao_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T003U15_A1482Gestao_Codigo[0] > A1482Gestao_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T003U15_A1482Gestao_Codigo[0] < A1482Gestao_Codigo ) ) )
            {
               A1482Gestao_Codigo = T003U15_A1482Gestao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
               RcdFound175 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3U175( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGestao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3U175( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound175 == 1 )
            {
               if ( A1482Gestao_Codigo != Z1482Gestao_Codigo )
               {
                  A1482Gestao_Codigo = Z1482Gestao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "GESTAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGestao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGestao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3U175( ) ;
                  GX_FocusControl = edtGestao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1482Gestao_Codigo != Z1482Gestao_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtGestao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3U175( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "GESTAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtGestao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtGestao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3U175( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1482Gestao_Codigo != Z1482Gestao_Codigo )
         {
            A1482Gestao_Codigo = Z1482Gestao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "GESTAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGestao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGestao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound175 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "GESTAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGestao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtGestao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3U175( ) ;
         if ( RcdFound175 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGestao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3U175( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound175 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGestao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound175 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGestao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3U175( ) ;
         if ( RcdFound175 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound175 != 0 )
            {
               ScanNext3U175( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtGestao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3U175( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3U175( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003U2 */
            pr_default.execute(0, new Object[] {A1482Gestao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Gestao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1483Gestao_DataHora != T003U2_A1483Gestao_DataHora[0] ) || ( Z1486Gestao_Vermelho != T003U2_A1486Gestao_Vermelho[0] ) || ( Z1487Gestao_Laranja != T003U2_A1487Gestao_Laranja[0] ) || ( Z1488Gestao_Amarelo != T003U2_A1488Gestao_Amarelo[0] ) || ( Z1484Gestao_ContratadaCod != T003U2_A1484Gestao_ContratadaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1485Gestao_AreaCod != T003U2_A1485Gestao_AreaCod[0] ) || ( Z1489Gestao_UsuarioCod != T003U2_A1489Gestao_UsuarioCod[0] ) || ( Z1490Gestao_GestorCod != T003U2_A1490Gestao_GestorCod[0] ) )
            {
               if ( Z1483Gestao_DataHora != T003U2_A1483Gestao_DataHora[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_DataHora");
                  GXUtil.WriteLogRaw("Old: ",Z1483Gestao_DataHora);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1483Gestao_DataHora[0]);
               }
               if ( Z1486Gestao_Vermelho != T003U2_A1486Gestao_Vermelho[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_Vermelho");
                  GXUtil.WriteLogRaw("Old: ",Z1486Gestao_Vermelho);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1486Gestao_Vermelho[0]);
               }
               if ( Z1487Gestao_Laranja != T003U2_A1487Gestao_Laranja[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_Laranja");
                  GXUtil.WriteLogRaw("Old: ",Z1487Gestao_Laranja);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1487Gestao_Laranja[0]);
               }
               if ( Z1488Gestao_Amarelo != T003U2_A1488Gestao_Amarelo[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_Amarelo");
                  GXUtil.WriteLogRaw("Old: ",Z1488Gestao_Amarelo);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1488Gestao_Amarelo[0]);
               }
               if ( Z1484Gestao_ContratadaCod != T003U2_A1484Gestao_ContratadaCod[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_ContratadaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1484Gestao_ContratadaCod);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1484Gestao_ContratadaCod[0]);
               }
               if ( Z1485Gestao_AreaCod != T003U2_A1485Gestao_AreaCod[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_AreaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1485Gestao_AreaCod);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1485Gestao_AreaCod[0]);
               }
               if ( Z1489Gestao_UsuarioCod != T003U2_A1489Gestao_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z1489Gestao_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1489Gestao_UsuarioCod[0]);
               }
               if ( Z1490Gestao_GestorCod != T003U2_A1490Gestao_GestorCod[0] )
               {
                  GXUtil.WriteLog("gestao:[seudo value changed for attri]"+"Gestao_GestorCod");
                  GXUtil.WriteLogRaw("Old: ",Z1490Gestao_GestorCod);
                  GXUtil.WriteLogRaw("Current: ",T003U2_A1490Gestao_GestorCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Gestao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3U175( )
      {
         BeforeValidate3U175( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3U175( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3U175( 0) ;
            CheckOptimisticConcurrency3U175( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3U175( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3U175( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003U16 */
                     pr_default.execute(14, new Object[] {A1483Gestao_DataHora, n1486Gestao_Vermelho, A1486Gestao_Vermelho, n1487Gestao_Laranja, A1487Gestao_Laranja, n1488Gestao_Amarelo, A1488Gestao_Amarelo, A1484Gestao_ContratadaCod, A1485Gestao_AreaCod, A1489Gestao_UsuarioCod, A1490Gestao_GestorCod});
                     A1482Gestao_Codigo = T003U16_A1482Gestao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Gestao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3U0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3U175( ) ;
            }
            EndLevel3U175( ) ;
         }
         CloseExtendedTableCursors3U175( ) ;
      }

      protected void Update3U175( )
      {
         BeforeValidate3U175( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3U175( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3U175( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3U175( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3U175( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003U17 */
                     pr_default.execute(15, new Object[] {A1483Gestao_DataHora, n1486Gestao_Vermelho, A1486Gestao_Vermelho, n1487Gestao_Laranja, A1487Gestao_Laranja, n1488Gestao_Amarelo, A1488Gestao_Amarelo, A1484Gestao_ContratadaCod, A1485Gestao_AreaCod, A1489Gestao_UsuarioCod, A1490Gestao_GestorCod, A1482Gestao_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Gestao") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Gestao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3U175( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3U0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3U175( ) ;
         }
         CloseExtendedTableCursors3U175( ) ;
      }

      protected void DeferredUpdate3U175( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3U175( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3U175( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3U175( ) ;
            AfterConfirm3U175( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3U175( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003U18 */
                  pr_default.execute(16, new Object[] {A1482Gestao_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("Gestao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound175 == 0 )
                        {
                           InitAll3U175( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3U0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode175 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3U175( ) ;
         Gx_mode = sMode175;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3U175( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3U175( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3U175( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Gestao");
            if ( AnyError == 0 )
            {
               ConfirmValues3U0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Gestao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3U175( )
      {
         /* Using cursor T003U19 */
         pr_default.execute(17);
         RcdFound175 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound175 = 1;
            A1482Gestao_Codigo = T003U19_A1482Gestao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3U175( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound175 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound175 = 1;
            A1482Gestao_Codigo = T003U19_A1482Gestao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3U175( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm3U175( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3U175( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3U175( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3U175( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3U175( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3U175( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3U175( )
      {
         edtGestao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_Codigo_Enabled), 5, 0)));
         edtGestao_DataHora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_DataHora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_DataHora_Enabled), 5, 0)));
         dynGestao_ContratadaCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGestao_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGestao_ContratadaCod.Enabled), 5, 0)));
         edtGestao_AreaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_AreaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_AreaCod_Enabled), 5, 0)));
         edtGestao_Vermelho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_Vermelho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_Vermelho_Enabled), 5, 0)));
         edtGestao_Laranja_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_Laranja_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_Laranja_Enabled), 5, 0)));
         edtGestao_Amarelo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_Amarelo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_Amarelo_Enabled), 5, 0)));
         edtGestao_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_UsuarioCod_Enabled), 5, 0)));
         edtGestao_GestorCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGestao_GestorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGestao_GestorCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3U0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117285573");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gestao.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1482Gestao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1482Gestao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1483Gestao_DataHora", context.localUtil.TToC( Z1483Gestao_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1486Gestao_Vermelho", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1486Gestao_Vermelho), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1487Gestao_Laranja", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1487Gestao_Laranja), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1488Gestao_Amarelo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1488Gestao_Amarelo), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1484Gestao_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1485Gestao_AreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1489Gestao_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1490Gestao_GestorCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gestao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Gestao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Gest�o" ;
      }

      protected void InitializeNonKey3U175( )
      {
         A1483Gestao_DataHora = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1483Gestao_DataHora", context.localUtil.TToC( A1483Gestao_DataHora, 8, 5, 0, 3, "/", ":", " "));
         A1484Gestao_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1484Gestao_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0)));
         A1485Gestao_AreaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1485Gestao_AreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1485Gestao_AreaCod), 6, 0)));
         A1486Gestao_Vermelho = 0;
         n1486Gestao_Vermelho = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1486Gestao_Vermelho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1486Gestao_Vermelho), 8, 0)));
         n1486Gestao_Vermelho = ((0==A1486Gestao_Vermelho) ? true : false);
         A1487Gestao_Laranja = 0;
         n1487Gestao_Laranja = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1487Gestao_Laranja", StringUtil.LTrim( StringUtil.Str( (decimal)(A1487Gestao_Laranja), 8, 0)));
         n1487Gestao_Laranja = ((0==A1487Gestao_Laranja) ? true : false);
         A1488Gestao_Amarelo = 0;
         n1488Gestao_Amarelo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1488Gestao_Amarelo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1488Gestao_Amarelo), 8, 0)));
         n1488Gestao_Amarelo = ((0==A1488Gestao_Amarelo) ? true : false);
         A1489Gestao_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1489Gestao_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1489Gestao_UsuarioCod), 6, 0)));
         A1490Gestao_GestorCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1490Gestao_GestorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1490Gestao_GestorCod), 6, 0)));
         Z1483Gestao_DataHora = (DateTime)(DateTime.MinValue);
         Z1486Gestao_Vermelho = 0;
         Z1487Gestao_Laranja = 0;
         Z1488Gestao_Amarelo = 0;
         Z1484Gestao_ContratadaCod = 0;
         Z1485Gestao_AreaCod = 0;
         Z1489Gestao_UsuarioCod = 0;
         Z1490Gestao_GestorCod = 0;
      }

      protected void InitAll3U175( )
      {
         A1482Gestao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1482Gestao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1482Gestao_Codigo), 6, 0)));
         InitializeNonKey3U175( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117285578");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gestao.js", "?20203117285579");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockgestao_codigo_Internalname = "TEXTBLOCKGESTAO_CODIGO";
         edtGestao_Codigo_Internalname = "GESTAO_CODIGO";
         lblTextblockgestao_datahora_Internalname = "TEXTBLOCKGESTAO_DATAHORA";
         edtGestao_DataHora_Internalname = "GESTAO_DATAHORA";
         lblTextblockgestao_contratadacod_Internalname = "TEXTBLOCKGESTAO_CONTRATADACOD";
         dynGestao_ContratadaCod_Internalname = "GESTAO_CONTRATADACOD";
         lblTextblockgestao_areacod_Internalname = "TEXTBLOCKGESTAO_AREACOD";
         edtGestao_AreaCod_Internalname = "GESTAO_AREACOD";
         lblTextblockgestao_vermelho_Internalname = "TEXTBLOCKGESTAO_VERMELHO";
         edtGestao_Vermelho_Internalname = "GESTAO_VERMELHO";
         lblTextblockgestao_laranja_Internalname = "TEXTBLOCKGESTAO_LARANJA";
         edtGestao_Laranja_Internalname = "GESTAO_LARANJA";
         lblTextblockgestao_amarelo_Internalname = "TEXTBLOCKGESTAO_AMARELO";
         edtGestao_Amarelo_Internalname = "GESTAO_AMARELO";
         lblTextblockgestao_usuariocod_Internalname = "TEXTBLOCKGESTAO_USUARIOCOD";
         edtGestao_UsuarioCod_Internalname = "GESTAO_USUARIOCOD";
         lblTextblockgestao_gestorcod_Internalname = "TEXTBLOCKGESTAO_GESTORCOD";
         edtGestao_GestorCod_Internalname = "GESTAO_GESTORCOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Gest�o";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtGestao_GestorCod_Jsonclick = "";
         edtGestao_GestorCod_Enabled = 1;
         edtGestao_UsuarioCod_Jsonclick = "";
         edtGestao_UsuarioCod_Enabled = 1;
         edtGestao_Amarelo_Jsonclick = "";
         edtGestao_Amarelo_Enabled = 1;
         edtGestao_Laranja_Jsonclick = "";
         edtGestao_Laranja_Enabled = 1;
         edtGestao_Vermelho_Jsonclick = "";
         edtGestao_Vermelho_Enabled = 1;
         edtGestao_AreaCod_Jsonclick = "";
         edtGestao_AreaCod_Enabled = 1;
         dynGestao_ContratadaCod_Jsonclick = "";
         dynGestao_ContratadaCod.Enabled = 1;
         edtGestao_DataHora_Jsonclick = "";
         edtGestao_DataHora_Enabled = 1;
         edtGestao_Codigo_Jsonclick = "";
         edtGestao_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAGESTAO_CONTRATADACOD3U175( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAGESTAO_CONTRATADACOD_data3U175( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAGESTAO_CONTRATADACOD_html3U175( )
      {
         int gxdynajaxvalue ;
         GXDLAGESTAO_CONTRATADACOD_data3U175( ) ;
         gxdynajaxindex = 1;
         dynGestao_ContratadaCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynGestao_ContratadaCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAGESTAO_CONTRATADACOD_data3U175( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T003U20 */
         pr_default.execute(18);
         while ( (pr_default.getStatus(18) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003U20_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003U20_A41Contratada_PessoaNom[0]));
            pr_default.readNext(18);
         }
         pr_default.close(18);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtGestao_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Gestao_codigo( int GX_Parm1 ,
                                       DateTime GX_Parm2 ,
                                       int GX_Parm3 ,
                                       int GX_Parm4 ,
                                       int GX_Parm5 ,
                                       GXCombobox dynGX_Parm6 ,
                                       int GX_Parm7 ,
                                       int GX_Parm8 ,
                                       int GX_Parm9 )
      {
         A1482Gestao_Codigo = GX_Parm1;
         A1483Gestao_DataHora = GX_Parm2;
         A1486Gestao_Vermelho = GX_Parm3;
         n1486Gestao_Vermelho = false;
         A1487Gestao_Laranja = GX_Parm4;
         n1487Gestao_Laranja = false;
         A1488Gestao_Amarelo = GX_Parm5;
         n1488Gestao_Amarelo = false;
         dynGestao_ContratadaCod = dynGX_Parm6;
         A1484Gestao_ContratadaCod = (int)(NumberUtil.Val( dynGestao_ContratadaCod.CurrentValue, "."));
         A1485Gestao_AreaCod = GX_Parm7;
         A1489Gestao_UsuarioCod = GX_Parm8;
         A1490Gestao_GestorCod = GX_Parm9;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         GXAGESTAO_CONTRATADACOD_html3U175( ) ;
         dynGestao_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0));
         isValidOutput.Add(context.localUtil.TToC( A1483Gestao_DataHora, 10, 8, 0, 3, "/", ":", " "));
         if ( dynGestao_ContratadaCod.ItemCount > 0 )
         {
            A1484Gestao_ContratadaCod = (int)(NumberUtil.Val( dynGestao_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0))), "."));
         }
         dynGestao_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1484Gestao_ContratadaCod), 6, 0));
         isValidOutput.Add(dynGestao_ContratadaCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1485Gestao_AreaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1486Gestao_Vermelho), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1487Gestao_Laranja), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1488Gestao_Amarelo), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1489Gestao_UsuarioCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1490Gestao_GestorCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1482Gestao_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1483Gestao_DataHora, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1484Gestao_ContratadaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1485Gestao_AreaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1486Gestao_Vermelho), 8, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1487Gestao_Laranja), 8, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1488Gestao_Amarelo), 8, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1489Gestao_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1490Gestao_GestorCod), 6, 0, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Gestao_contratadacod( GXCombobox dynGX_Parm1 )
      {
         dynGestao_ContratadaCod = dynGX_Parm1;
         A1484Gestao_ContratadaCod = (int)(NumberUtil.Val( dynGestao_ContratadaCod.CurrentValue, "."));
         /* Using cursor T003U21 */
         pr_default.execute(19, new Object[] {A1484Gestao_ContratadaCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Contratada'.", "ForeignKeyNotFound", 1, "GESTAO_CONTRATADACOD");
            AnyError = 1;
            GX_FocusControl = dynGestao_ContratadaCod_Internalname;
         }
         pr_default.close(19);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Gestao_areacod( int GX_Parm1 )
      {
         A1485Gestao_AreaCod = GX_Parm1;
         /* Using cursor T003U22 */
         pr_default.execute(20, new Object[] {A1485Gestao_AreaCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Area Trabalho'.", "ForeignKeyNotFound", 1, "GESTAO_AREACOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_AreaCod_Internalname;
         }
         pr_default.close(20);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Gestao_usuariocod( int GX_Parm1 )
      {
         A1489Gestao_UsuarioCod = GX_Parm1;
         /* Using cursor T003U23 */
         pr_default.execute(21, new Object[] {A1489Gestao_UsuarioCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Usuario'.", "ForeignKeyNotFound", 1, "GESTAO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_UsuarioCod_Internalname;
         }
         pr_default.close(21);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Gestao_gestorcod( int GX_Parm1 )
      {
         A1490Gestao_GestorCod = GX_Parm1;
         /* Using cursor T003U24 */
         pr_default.execute(22, new Object[] {A1490Gestao_GestorCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Gestao_Usuario Gestor'.", "ForeignKeyNotFound", 1, "GESTAO_GESTORCOD");
            AnyError = 1;
            GX_FocusControl = edtGestao_GestorCod_Internalname;
         }
         pr_default.close(22);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1483Gestao_DataHora = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockgestao_codigo_Jsonclick = "";
         lblTextblockgestao_datahora_Jsonclick = "";
         A1483Gestao_DataHora = (DateTime)(DateTime.MinValue);
         lblTextblockgestao_contratadacod_Jsonclick = "";
         lblTextblockgestao_areacod_Jsonclick = "";
         lblTextblockgestao_vermelho_Jsonclick = "";
         lblTextblockgestao_laranja_Jsonclick = "";
         lblTextblockgestao_amarelo_Jsonclick = "";
         lblTextblockgestao_usuariocod_Jsonclick = "";
         lblTextblockgestao_gestorcod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T003U8_A1482Gestao_Codigo = new int[1] ;
         T003U8_A1483Gestao_DataHora = new DateTime[] {DateTime.MinValue} ;
         T003U8_A1486Gestao_Vermelho = new int[1] ;
         T003U8_n1486Gestao_Vermelho = new bool[] {false} ;
         T003U8_A1487Gestao_Laranja = new int[1] ;
         T003U8_n1487Gestao_Laranja = new bool[] {false} ;
         T003U8_A1488Gestao_Amarelo = new int[1] ;
         T003U8_n1488Gestao_Amarelo = new bool[] {false} ;
         T003U8_A1484Gestao_ContratadaCod = new int[1] ;
         T003U8_A1485Gestao_AreaCod = new int[1] ;
         T003U8_A1489Gestao_UsuarioCod = new int[1] ;
         T003U8_A1490Gestao_GestorCod = new int[1] ;
         T003U4_A1484Gestao_ContratadaCod = new int[1] ;
         T003U5_A1485Gestao_AreaCod = new int[1] ;
         T003U6_A1489Gestao_UsuarioCod = new int[1] ;
         T003U7_A1490Gestao_GestorCod = new int[1] ;
         T003U9_A1484Gestao_ContratadaCod = new int[1] ;
         T003U10_A1485Gestao_AreaCod = new int[1] ;
         T003U11_A1489Gestao_UsuarioCod = new int[1] ;
         T003U12_A1490Gestao_GestorCod = new int[1] ;
         T003U13_A1482Gestao_Codigo = new int[1] ;
         T003U3_A1482Gestao_Codigo = new int[1] ;
         T003U3_A1483Gestao_DataHora = new DateTime[] {DateTime.MinValue} ;
         T003U3_A1486Gestao_Vermelho = new int[1] ;
         T003U3_n1486Gestao_Vermelho = new bool[] {false} ;
         T003U3_A1487Gestao_Laranja = new int[1] ;
         T003U3_n1487Gestao_Laranja = new bool[] {false} ;
         T003U3_A1488Gestao_Amarelo = new int[1] ;
         T003U3_n1488Gestao_Amarelo = new bool[] {false} ;
         T003U3_A1484Gestao_ContratadaCod = new int[1] ;
         T003U3_A1485Gestao_AreaCod = new int[1] ;
         T003U3_A1489Gestao_UsuarioCod = new int[1] ;
         T003U3_A1490Gestao_GestorCod = new int[1] ;
         sMode175 = "";
         T003U14_A1482Gestao_Codigo = new int[1] ;
         T003U15_A1482Gestao_Codigo = new int[1] ;
         T003U2_A1482Gestao_Codigo = new int[1] ;
         T003U2_A1483Gestao_DataHora = new DateTime[] {DateTime.MinValue} ;
         T003U2_A1486Gestao_Vermelho = new int[1] ;
         T003U2_n1486Gestao_Vermelho = new bool[] {false} ;
         T003U2_A1487Gestao_Laranja = new int[1] ;
         T003U2_n1487Gestao_Laranja = new bool[] {false} ;
         T003U2_A1488Gestao_Amarelo = new int[1] ;
         T003U2_n1488Gestao_Amarelo = new bool[] {false} ;
         T003U2_A1484Gestao_ContratadaCod = new int[1] ;
         T003U2_A1485Gestao_AreaCod = new int[1] ;
         T003U2_A1489Gestao_UsuarioCod = new int[1] ;
         T003U2_A1490Gestao_GestorCod = new int[1] ;
         T003U16_A1482Gestao_Codigo = new int[1] ;
         T003U19_A1482Gestao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003U20_A40Contratada_PessoaCod = new int[1] ;
         T003U20_A39Contratada_Codigo = new int[1] ;
         T003U20_A41Contratada_PessoaNom = new String[] {""} ;
         T003U20_n41Contratada_PessoaNom = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T003U21_A1484Gestao_ContratadaCod = new int[1] ;
         T003U22_A1485Gestao_AreaCod = new int[1] ;
         T003U23_A1489Gestao_UsuarioCod = new int[1] ;
         T003U24_A1490Gestao_GestorCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gestao__default(),
            new Object[][] {
                new Object[] {
               T003U2_A1482Gestao_Codigo, T003U2_A1483Gestao_DataHora, T003U2_A1486Gestao_Vermelho, T003U2_n1486Gestao_Vermelho, T003U2_A1487Gestao_Laranja, T003U2_n1487Gestao_Laranja, T003U2_A1488Gestao_Amarelo, T003U2_n1488Gestao_Amarelo, T003U2_A1484Gestao_ContratadaCod, T003U2_A1485Gestao_AreaCod,
               T003U2_A1489Gestao_UsuarioCod, T003U2_A1490Gestao_GestorCod
               }
               , new Object[] {
               T003U3_A1482Gestao_Codigo, T003U3_A1483Gestao_DataHora, T003U3_A1486Gestao_Vermelho, T003U3_n1486Gestao_Vermelho, T003U3_A1487Gestao_Laranja, T003U3_n1487Gestao_Laranja, T003U3_A1488Gestao_Amarelo, T003U3_n1488Gestao_Amarelo, T003U3_A1484Gestao_ContratadaCod, T003U3_A1485Gestao_AreaCod,
               T003U3_A1489Gestao_UsuarioCod, T003U3_A1490Gestao_GestorCod
               }
               , new Object[] {
               T003U4_A1484Gestao_ContratadaCod
               }
               , new Object[] {
               T003U5_A1485Gestao_AreaCod
               }
               , new Object[] {
               T003U6_A1489Gestao_UsuarioCod
               }
               , new Object[] {
               T003U7_A1490Gestao_GestorCod
               }
               , new Object[] {
               T003U8_A1482Gestao_Codigo, T003U8_A1483Gestao_DataHora, T003U8_A1486Gestao_Vermelho, T003U8_n1486Gestao_Vermelho, T003U8_A1487Gestao_Laranja, T003U8_n1487Gestao_Laranja, T003U8_A1488Gestao_Amarelo, T003U8_n1488Gestao_Amarelo, T003U8_A1484Gestao_ContratadaCod, T003U8_A1485Gestao_AreaCod,
               T003U8_A1489Gestao_UsuarioCod, T003U8_A1490Gestao_GestorCod
               }
               , new Object[] {
               T003U9_A1484Gestao_ContratadaCod
               }
               , new Object[] {
               T003U10_A1485Gestao_AreaCod
               }
               , new Object[] {
               T003U11_A1489Gestao_UsuarioCod
               }
               , new Object[] {
               T003U12_A1490Gestao_GestorCod
               }
               , new Object[] {
               T003U13_A1482Gestao_Codigo
               }
               , new Object[] {
               T003U14_A1482Gestao_Codigo
               }
               , new Object[] {
               T003U15_A1482Gestao_Codigo
               }
               , new Object[] {
               T003U16_A1482Gestao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003U19_A1482Gestao_Codigo
               }
               , new Object[] {
               T003U20_A40Contratada_PessoaCod, T003U20_A39Contratada_Codigo, T003U20_A41Contratada_PessoaNom, T003U20_n41Contratada_PessoaNom
               }
               , new Object[] {
               T003U21_A1484Gestao_ContratadaCod
               }
               , new Object[] {
               T003U22_A1485Gestao_AreaCod
               }
               , new Object[] {
               T003U23_A1489Gestao_UsuarioCod
               }
               , new Object[] {
               T003U24_A1490Gestao_GestorCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound175 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1482Gestao_Codigo ;
      private int Z1486Gestao_Vermelho ;
      private int Z1487Gestao_Laranja ;
      private int Z1488Gestao_Amarelo ;
      private int Z1484Gestao_ContratadaCod ;
      private int Z1485Gestao_AreaCod ;
      private int Z1489Gestao_UsuarioCod ;
      private int Z1490Gestao_GestorCod ;
      private int A1484Gestao_ContratadaCod ;
      private int A1485Gestao_AreaCod ;
      private int A1489Gestao_UsuarioCod ;
      private int A1490Gestao_GestorCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1482Gestao_Codigo ;
      private int edtGestao_Codigo_Enabled ;
      private int edtGestao_DataHora_Enabled ;
      private int edtGestao_AreaCod_Enabled ;
      private int A1486Gestao_Vermelho ;
      private int edtGestao_Vermelho_Enabled ;
      private int A1487Gestao_Laranja ;
      private int edtGestao_Laranja_Enabled ;
      private int A1488Gestao_Amarelo ;
      private int edtGestao_Amarelo_Enabled ;
      private int edtGestao_UsuarioCod_Enabled ;
      private int edtGestao_GestorCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGestao_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockgestao_codigo_Internalname ;
      private String lblTextblockgestao_codigo_Jsonclick ;
      private String edtGestao_Codigo_Jsonclick ;
      private String lblTextblockgestao_datahora_Internalname ;
      private String lblTextblockgestao_datahora_Jsonclick ;
      private String edtGestao_DataHora_Internalname ;
      private String edtGestao_DataHora_Jsonclick ;
      private String lblTextblockgestao_contratadacod_Internalname ;
      private String lblTextblockgestao_contratadacod_Jsonclick ;
      private String dynGestao_ContratadaCod_Internalname ;
      private String dynGestao_ContratadaCod_Jsonclick ;
      private String lblTextblockgestao_areacod_Internalname ;
      private String lblTextblockgestao_areacod_Jsonclick ;
      private String edtGestao_AreaCod_Internalname ;
      private String edtGestao_AreaCod_Jsonclick ;
      private String lblTextblockgestao_vermelho_Internalname ;
      private String lblTextblockgestao_vermelho_Jsonclick ;
      private String edtGestao_Vermelho_Internalname ;
      private String edtGestao_Vermelho_Jsonclick ;
      private String lblTextblockgestao_laranja_Internalname ;
      private String lblTextblockgestao_laranja_Jsonclick ;
      private String edtGestao_Laranja_Internalname ;
      private String edtGestao_Laranja_Jsonclick ;
      private String lblTextblockgestao_amarelo_Internalname ;
      private String lblTextblockgestao_amarelo_Jsonclick ;
      private String edtGestao_Amarelo_Internalname ;
      private String edtGestao_Amarelo_Jsonclick ;
      private String lblTextblockgestao_usuariocod_Internalname ;
      private String lblTextblockgestao_usuariocod_Jsonclick ;
      private String edtGestao_UsuarioCod_Internalname ;
      private String edtGestao_UsuarioCod_Jsonclick ;
      private String lblTextblockgestao_gestorcod_Internalname ;
      private String lblTextblockgestao_gestorcod_Jsonclick ;
      private String edtGestao_GestorCod_Internalname ;
      private String edtGestao_GestorCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode175 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private DateTime Z1483Gestao_DataHora ;
      private DateTime A1483Gestao_DataHora ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1486Gestao_Vermelho ;
      private bool n1487Gestao_Laranja ;
      private bool n1488Gestao_Amarelo ;
      private bool Gx_longc ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynGestao_ContratadaCod ;
      private IDataStoreProvider pr_default ;
      private int[] T003U8_A1482Gestao_Codigo ;
      private DateTime[] T003U8_A1483Gestao_DataHora ;
      private int[] T003U8_A1486Gestao_Vermelho ;
      private bool[] T003U8_n1486Gestao_Vermelho ;
      private int[] T003U8_A1487Gestao_Laranja ;
      private bool[] T003U8_n1487Gestao_Laranja ;
      private int[] T003U8_A1488Gestao_Amarelo ;
      private bool[] T003U8_n1488Gestao_Amarelo ;
      private int[] T003U8_A1484Gestao_ContratadaCod ;
      private int[] T003U8_A1485Gestao_AreaCod ;
      private int[] T003U8_A1489Gestao_UsuarioCod ;
      private int[] T003U8_A1490Gestao_GestorCod ;
      private int[] T003U4_A1484Gestao_ContratadaCod ;
      private int[] T003U5_A1485Gestao_AreaCod ;
      private int[] T003U6_A1489Gestao_UsuarioCod ;
      private int[] T003U7_A1490Gestao_GestorCod ;
      private int[] T003U9_A1484Gestao_ContratadaCod ;
      private int[] T003U10_A1485Gestao_AreaCod ;
      private int[] T003U11_A1489Gestao_UsuarioCod ;
      private int[] T003U12_A1490Gestao_GestorCod ;
      private int[] T003U13_A1482Gestao_Codigo ;
      private int[] T003U3_A1482Gestao_Codigo ;
      private DateTime[] T003U3_A1483Gestao_DataHora ;
      private int[] T003U3_A1486Gestao_Vermelho ;
      private bool[] T003U3_n1486Gestao_Vermelho ;
      private int[] T003U3_A1487Gestao_Laranja ;
      private bool[] T003U3_n1487Gestao_Laranja ;
      private int[] T003U3_A1488Gestao_Amarelo ;
      private bool[] T003U3_n1488Gestao_Amarelo ;
      private int[] T003U3_A1484Gestao_ContratadaCod ;
      private int[] T003U3_A1485Gestao_AreaCod ;
      private int[] T003U3_A1489Gestao_UsuarioCod ;
      private int[] T003U3_A1490Gestao_GestorCod ;
      private int[] T003U14_A1482Gestao_Codigo ;
      private int[] T003U15_A1482Gestao_Codigo ;
      private int[] T003U2_A1482Gestao_Codigo ;
      private DateTime[] T003U2_A1483Gestao_DataHora ;
      private int[] T003U2_A1486Gestao_Vermelho ;
      private bool[] T003U2_n1486Gestao_Vermelho ;
      private int[] T003U2_A1487Gestao_Laranja ;
      private bool[] T003U2_n1487Gestao_Laranja ;
      private int[] T003U2_A1488Gestao_Amarelo ;
      private bool[] T003U2_n1488Gestao_Amarelo ;
      private int[] T003U2_A1484Gestao_ContratadaCod ;
      private int[] T003U2_A1485Gestao_AreaCod ;
      private int[] T003U2_A1489Gestao_UsuarioCod ;
      private int[] T003U2_A1490Gestao_GestorCod ;
      private int[] T003U16_A1482Gestao_Codigo ;
      private int[] T003U19_A1482Gestao_Codigo ;
      private int[] T003U20_A40Contratada_PessoaCod ;
      private int[] T003U20_A39Contratada_Codigo ;
      private String[] T003U20_A41Contratada_PessoaNom ;
      private bool[] T003U20_n41Contratada_PessoaNom ;
      private int[] T003U21_A1484Gestao_ContratadaCod ;
      private int[] T003U22_A1485Gestao_AreaCod ;
      private int[] T003U23_A1489Gestao_UsuarioCod ;
      private int[] T003U24_A1490Gestao_GestorCod ;
      private GXWebForm Form ;
   }

   public class gestao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003U8 ;
          prmT003U8 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U4 ;
          prmT003U4 = new Object[] {
          new Object[] {"@Gestao_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U5 ;
          prmT003U5 = new Object[] {
          new Object[] {"@Gestao_AreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U6 ;
          prmT003U6 = new Object[] {
          new Object[] {"@Gestao_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U7 ;
          prmT003U7 = new Object[] {
          new Object[] {"@Gestao_GestorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U9 ;
          prmT003U9 = new Object[] {
          new Object[] {"@Gestao_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U10 ;
          prmT003U10 = new Object[] {
          new Object[] {"@Gestao_AreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U11 ;
          prmT003U11 = new Object[] {
          new Object[] {"@Gestao_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U12 ;
          prmT003U12 = new Object[] {
          new Object[] {"@Gestao_GestorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U13 ;
          prmT003U13 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U3 ;
          prmT003U3 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U14 ;
          prmT003U14 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U15 ;
          prmT003U15 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U2 ;
          prmT003U2 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U16 ;
          prmT003U16 = new Object[] {
          new Object[] {"@Gestao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Gestao_Vermelho",SqlDbType.Int,8,0} ,
          new Object[] {"@Gestao_Laranja",SqlDbType.Int,8,0} ,
          new Object[] {"@Gestao_Amarelo",SqlDbType.Int,8,0} ,
          new Object[] {"@Gestao_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_AreaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_GestorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U17 ;
          prmT003U17 = new Object[] {
          new Object[] {"@Gestao_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Gestao_Vermelho",SqlDbType.Int,8,0} ,
          new Object[] {"@Gestao_Laranja",SqlDbType.Int,8,0} ,
          new Object[] {"@Gestao_Amarelo",SqlDbType.Int,8,0} ,
          new Object[] {"@Gestao_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_AreaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_GestorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U18 ;
          prmT003U18 = new Object[] {
          new Object[] {"@Gestao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U19 ;
          prmT003U19 = new Object[] {
          } ;
          Object[] prmT003U20 ;
          prmT003U20 = new Object[] {
          } ;
          Object[] prmT003U21 ;
          prmT003U21 = new Object[] {
          new Object[] {"@Gestao_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U22 ;
          prmT003U22 = new Object[] {
          new Object[] {"@Gestao_AreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U23 ;
          prmT003U23 = new Object[] {
          new Object[] {"@Gestao_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003U24 ;
          prmT003U24 = new Object[] {
          new Object[] {"@Gestao_GestorCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003U2", "SELECT [Gestao_Codigo], [Gestao_DataHora], [Gestao_Vermelho], [Gestao_Laranja], [Gestao_Amarelo], [Gestao_ContratadaCod] AS Gestao_ContratadaCod, [Gestao_AreaCod] AS Gestao_AreaCod, [Gestao_UsuarioCod] AS Gestao_UsuarioCod, [Gestao_GestorCod] AS Gestao_GestorCod FROM [Gestao] WITH (UPDLOCK) WHERE [Gestao_Codigo] = @Gestao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U2,1,0,true,false )
             ,new CursorDef("T003U3", "SELECT [Gestao_Codigo], [Gestao_DataHora], [Gestao_Vermelho], [Gestao_Laranja], [Gestao_Amarelo], [Gestao_ContratadaCod] AS Gestao_ContratadaCod, [Gestao_AreaCod] AS Gestao_AreaCod, [Gestao_UsuarioCod] AS Gestao_UsuarioCod, [Gestao_GestorCod] AS Gestao_GestorCod FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_Codigo] = @Gestao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U3,1,0,true,false )
             ,new CursorDef("T003U4", "SELECT [Contratada_Codigo] AS Gestao_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Gestao_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U4,1,0,true,false )
             ,new CursorDef("T003U5", "SELECT [AreaTrabalho_Codigo] AS Gestao_AreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Gestao_AreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U5,1,0,true,false )
             ,new CursorDef("T003U6", "SELECT [Usuario_Codigo] AS Gestao_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Gestao_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U6,1,0,true,false )
             ,new CursorDef("T003U7", "SELECT [Usuario_Codigo] AS Gestao_GestorCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Gestao_GestorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U7,1,0,true,false )
             ,new CursorDef("T003U8", "SELECT TM1.[Gestao_Codigo], TM1.[Gestao_DataHora], TM1.[Gestao_Vermelho], TM1.[Gestao_Laranja], TM1.[Gestao_Amarelo], TM1.[Gestao_ContratadaCod] AS Gestao_ContratadaCod, TM1.[Gestao_AreaCod] AS Gestao_AreaCod, TM1.[Gestao_UsuarioCod] AS Gestao_UsuarioCod, TM1.[Gestao_GestorCod] AS Gestao_GestorCod FROM [Gestao] TM1 WITH (NOLOCK) WHERE TM1.[Gestao_Codigo] = @Gestao_Codigo ORDER BY TM1.[Gestao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003U8,100,0,true,false )
             ,new CursorDef("T003U9", "SELECT [Contratada_Codigo] AS Gestao_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Gestao_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U9,1,0,true,false )
             ,new CursorDef("T003U10", "SELECT [AreaTrabalho_Codigo] AS Gestao_AreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Gestao_AreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U10,1,0,true,false )
             ,new CursorDef("T003U11", "SELECT [Usuario_Codigo] AS Gestao_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Gestao_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U11,1,0,true,false )
             ,new CursorDef("T003U12", "SELECT [Usuario_Codigo] AS Gestao_GestorCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Gestao_GestorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U12,1,0,true,false )
             ,new CursorDef("T003U13", "SELECT [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_Codigo] = @Gestao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003U13,1,0,true,false )
             ,new CursorDef("T003U14", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE ( [Gestao_Codigo] > @Gestao_Codigo) ORDER BY [Gestao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003U14,1,0,true,true )
             ,new CursorDef("T003U15", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE ( [Gestao_Codigo] < @Gestao_Codigo) ORDER BY [Gestao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003U15,1,0,true,true )
             ,new CursorDef("T003U16", "INSERT INTO [Gestao]([Gestao_DataHora], [Gestao_Vermelho], [Gestao_Laranja], [Gestao_Amarelo], [Gestao_ContratadaCod], [Gestao_AreaCod], [Gestao_UsuarioCod], [Gestao_GestorCod]) VALUES(@Gestao_DataHora, @Gestao_Vermelho, @Gestao_Laranja, @Gestao_Amarelo, @Gestao_ContratadaCod, @Gestao_AreaCod, @Gestao_UsuarioCod, @Gestao_GestorCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003U16)
             ,new CursorDef("T003U17", "UPDATE [Gestao] SET [Gestao_DataHora]=@Gestao_DataHora, [Gestao_Vermelho]=@Gestao_Vermelho, [Gestao_Laranja]=@Gestao_Laranja, [Gestao_Amarelo]=@Gestao_Amarelo, [Gestao_ContratadaCod]=@Gestao_ContratadaCod, [Gestao_AreaCod]=@Gestao_AreaCod, [Gestao_UsuarioCod]=@Gestao_UsuarioCod, [Gestao_GestorCod]=@Gestao_GestorCod  WHERE [Gestao_Codigo] = @Gestao_Codigo", GxErrorMask.GX_NOMASK,prmT003U17)
             ,new CursorDef("T003U18", "DELETE FROM [Gestao]  WHERE [Gestao_Codigo] = @Gestao_Codigo", GxErrorMask.GX_NOMASK,prmT003U18)
             ,new CursorDef("T003U19", "SELECT [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) ORDER BY [Gestao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003U19,100,0,true,false )
             ,new CursorDef("T003U20", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U20,0,0,true,false )
             ,new CursorDef("T003U21", "SELECT [Contratada_Codigo] AS Gestao_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Gestao_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U21,1,0,true,false )
             ,new CursorDef("T003U22", "SELECT [AreaTrabalho_Codigo] AS Gestao_AreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Gestao_AreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U22,1,0,true,false )
             ,new CursorDef("T003U23", "SELECT [Usuario_Codigo] AS Gestao_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Gestao_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U23,1,0,true,false )
             ,new CursorDef("T003U24", "SELECT [Usuario_Codigo] AS Gestao_GestorCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Gestao_GestorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003U24,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                stmt.SetParameter(5, (int)parms[7]);
                stmt.SetParameter(6, (int)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                stmt.SetParameter(8, (int)parms[10]);
                return;
             case 15 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                stmt.SetParameter(5, (int)parms[7]);
                stmt.SetParameter(6, (int)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                stmt.SetParameter(8, (int)parms[10]);
                stmt.SetParameter(9, (int)parms[11]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
