/*
               File: Proposta_BC
        Description: Proposta
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:55:45.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class proposta_bc : GXHttpHandler, IGxSilentTrn
   {
      public proposta_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public proposta_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow48187( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey48187( ) ;
         standaloneModal( ) ;
         AddRow48187( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11482 */
            E11482 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1685Proposta_Codigo = A1685Proposta_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_480( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls48187( ) ;
            }
            else
            {
               CheckExtendedTable48187( ) ;
               if ( AnyError == 0 )
               {
                  ZM48187( 7) ;
                  ZM48187( 8) ;
                  ZM48187( 9) ;
                  ZM48187( 10) ;
                  ZM48187( 11) ;
                  ZM48187( 12) ;
                  ZM48187( 13) ;
                  ZM48187( 14) ;
                  ZM48187( 15) ;
               }
               CloseExtendedTableCursors48187( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12482( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "AreaTrabalho_Codigo") == 0 )
               {
                  AV11Insert_AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Proposta_OSCodigo") == 0 )
               {
                  AV12Insert_Proposta_OSCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
            }
         }
      }

      protected void E11482( )
      {
         /* After Trn Routine */
      }

      protected void ZM48187( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z1687Proposta_Vigencia = A1687Proposta_Vigencia;
            Z1688Proposta_Valor = A1688Proposta_Valor;
            Z1689Proposta_Status = A1689Proposta_Status;
            Z1694Proposta_RestricaoImplantacao = A1694Proposta_RestricaoImplantacao;
            Z1695Proposta_RestricaoCusto = A1695Proposta_RestricaoCusto;
            Z1696Proposta_Ativo = A1696Proposta_Ativo;
            Z1699Proposta_ContratadaCod = A1699Proposta_ContratadaCod;
            Z1700Proposta_ContratadaRazSocial = A1700Proposta_ContratadaRazSocial;
            Z1702Proposta_Prazo = A1702Proposta_Prazo;
            Z1793Proposta_PrazoDias = A1793Proposta_PrazoDias;
            Z1703Proposta_Esforco = A1703Proposta_Esforco;
            Z1794Proposta_Liquido = A1794Proposta_Liquido;
            Z1704Proposta_CntSrvCod = A1704Proposta_CntSrvCod;
            Z1722Proposta_ValorUndCnt = A1722Proposta_ValorUndCnt;
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z1686Proposta_OSCodigo = A1686Proposta_OSCodigo;
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z1764Proposta_OSDmn = A1764Proposta_OSDmn;
            Z1711Proposta_OSDmnFM = A1711Proposta_OSDmnFM;
            Z1792Proposta_OSInicio = A1792Proposta_OSInicio;
            Z1705Proposta_OSServico = A1705Proposta_OSServico;
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z1707Proposta_OSPrzTpDias = A1707Proposta_OSPrzTpDias;
            Z1721Proposta_OSPrzRsp = A1721Proposta_OSPrzRsp;
            Z1763Proposta_OSCntSrvFtrm = A1763Proposta_OSCntSrvFtrm;
            Z1715Proposta_OSCntCod = A1715Proposta_OSCntCod;
            Z1713Proposta_OSUndCntCod = A1713Proposta_OSUndCntCod;
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z1710Proposta_OSUndCntSgl = A1710Proposta_OSUndCntSgl;
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
         if ( GX_JID == -6 )
         {
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            Z1687Proposta_Vigencia = A1687Proposta_Vigencia;
            Z1688Proposta_Valor = A1688Proposta_Valor;
            Z1689Proposta_Status = A1689Proposta_Status;
            Z1690Proposta_Objetivo = A1690Proposta_Objetivo;
            Z1691Proposta_Escopo = A1691Proposta_Escopo;
            Z1692Proposta_Oportunidade = A1692Proposta_Oportunidade;
            Z1693Proposta_Perspectiva = A1693Proposta_Perspectiva;
            Z1694Proposta_RestricaoImplantacao = A1694Proposta_RestricaoImplantacao;
            Z1695Proposta_RestricaoCusto = A1695Proposta_RestricaoCusto;
            Z1696Proposta_Ativo = A1696Proposta_Ativo;
            Z1699Proposta_ContratadaCod = A1699Proposta_ContratadaCod;
            Z1700Proposta_ContratadaRazSocial = A1700Proposta_ContratadaRazSocial;
            Z1702Proposta_Prazo = A1702Proposta_Prazo;
            Z1793Proposta_PrazoDias = A1793Proposta_PrazoDias;
            Z1703Proposta_Esforco = A1703Proposta_Esforco;
            Z1794Proposta_Liquido = A1794Proposta_Liquido;
            Z1704Proposta_CntSrvCod = A1704Proposta_CntSrvCod;
            Z1722Proposta_ValorUndCnt = A1722Proposta_ValorUndCnt;
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z1686Proposta_OSCodigo = A1686Proposta_OSCodigo;
            Z1764Proposta_OSDmn = A1764Proposta_OSDmn;
            Z1711Proposta_OSDmnFM = A1711Proposta_OSDmnFM;
            Z1792Proposta_OSInicio = A1792Proposta_OSInicio;
            Z1705Proposta_OSServico = A1705Proposta_OSServico;
            Z1707Proposta_OSPrzTpDias = A1707Proposta_OSPrzTpDias;
            Z1721Proposta_OSPrzRsp = A1721Proposta_OSPrzRsp;
            Z1763Proposta_OSCntSrvFtrm = A1763Proposta_OSCntSrvFtrm;
            Z1715Proposta_OSCntCod = A1715Proposta_OSCntCod;
            Z1713Proposta_OSUndCntCod = A1713Proposta_OSUndCntCod;
            Z1710Proposta_OSUndCntSgl = A1710Proposta_OSUndCntSgl;
            Z1717Proposta_OSPFB = A1717Proposta_OSPFB;
            Z1718Proposta_OSPFL = A1718Proposta_OSPFL;
            Z1719Proposta_OSDataCnt = A1719Proposta_OSDataCnt;
            Z1720Proposta_OSHoraCnt = A1720Proposta_OSHoraCnt;
         }
      }

      protected void standaloneNotModal( )
      {
         AV14Pgmname = "Proposta_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load48187( )
      {
         /* Using cursor BC004821 */
         pr_default.execute(11, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound187 = 1;
            A1687Proposta_Vigencia = BC004821_A1687Proposta_Vigencia[0];
            n1687Proposta_Vigencia = BC004821_n1687Proposta_Vigencia[0];
            A1688Proposta_Valor = BC004821_A1688Proposta_Valor[0];
            A1689Proposta_Status = BC004821_A1689Proposta_Status[0];
            n1689Proposta_Status = BC004821_n1689Proposta_Status[0];
            A1690Proposta_Objetivo = BC004821_A1690Proposta_Objetivo[0];
            A1691Proposta_Escopo = BC004821_A1691Proposta_Escopo[0];
            n1691Proposta_Escopo = BC004821_n1691Proposta_Escopo[0];
            A1692Proposta_Oportunidade = BC004821_A1692Proposta_Oportunidade[0];
            n1692Proposta_Oportunidade = BC004821_n1692Proposta_Oportunidade[0];
            A1693Proposta_Perspectiva = BC004821_A1693Proposta_Perspectiva[0];
            n1693Proposta_Perspectiva = BC004821_n1693Proposta_Perspectiva[0];
            A1694Proposta_RestricaoImplantacao = BC004821_A1694Proposta_RestricaoImplantacao[0];
            n1694Proposta_RestricaoImplantacao = BC004821_n1694Proposta_RestricaoImplantacao[0];
            A1695Proposta_RestricaoCusto = BC004821_A1695Proposta_RestricaoCusto[0];
            n1695Proposta_RestricaoCusto = BC004821_n1695Proposta_RestricaoCusto[0];
            A1696Proposta_Ativo = BC004821_A1696Proposta_Ativo[0];
            A1699Proposta_ContratadaCod = BC004821_A1699Proposta_ContratadaCod[0];
            A1700Proposta_ContratadaRazSocial = BC004821_A1700Proposta_ContratadaRazSocial[0];
            n1700Proposta_ContratadaRazSocial = BC004821_n1700Proposta_ContratadaRazSocial[0];
            A1702Proposta_Prazo = BC004821_A1702Proposta_Prazo[0];
            A1793Proposta_PrazoDias = BC004821_A1793Proposta_PrazoDias[0];
            n1793Proposta_PrazoDias = BC004821_n1793Proposta_PrazoDias[0];
            A1703Proposta_Esforco = BC004821_A1703Proposta_Esforco[0];
            A1794Proposta_Liquido = BC004821_A1794Proposta_Liquido[0];
            n1794Proposta_Liquido = BC004821_n1794Proposta_Liquido[0];
            A1704Proposta_CntSrvCod = BC004821_A1704Proposta_CntSrvCod[0];
            A1722Proposta_ValorUndCnt = BC004821_A1722Proposta_ValorUndCnt[0];
            A1764Proposta_OSDmn = BC004821_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = BC004821_n1764Proposta_OSDmn[0];
            A1711Proposta_OSDmnFM = BC004821_A1711Proposta_OSDmnFM[0];
            n1711Proposta_OSDmnFM = BC004821_n1711Proposta_OSDmnFM[0];
            A1710Proposta_OSUndCntSgl = BC004821_A1710Proposta_OSUndCntSgl[0];
            n1710Proposta_OSUndCntSgl = BC004821_n1710Proposta_OSUndCntSgl[0];
            A1707Proposta_OSPrzTpDias = BC004821_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = BC004821_n1707Proposta_OSPrzTpDias[0];
            A1721Proposta_OSPrzRsp = BC004821_A1721Proposta_OSPrzRsp[0];
            n1721Proposta_OSPrzRsp = BC004821_n1721Proposta_OSPrzRsp[0];
            A1763Proposta_OSCntSrvFtrm = BC004821_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = BC004821_n1763Proposta_OSCntSrvFtrm[0];
            A1792Proposta_OSInicio = BC004821_A1792Proposta_OSInicio[0];
            n1792Proposta_OSInicio = BC004821_n1792Proposta_OSInicio[0];
            A648Projeto_Codigo = BC004821_A648Projeto_Codigo[0];
            n648Projeto_Codigo = BC004821_n648Projeto_Codigo[0];
            A5AreaTrabalho_Codigo = BC004821_A5AreaTrabalho_Codigo[0];
            A1686Proposta_OSCodigo = BC004821_A1686Proposta_OSCodigo[0];
            A1705Proposta_OSServico = BC004821_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = BC004821_n1705Proposta_OSServico[0];
            A1715Proposta_OSCntCod = BC004821_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = BC004821_n1715Proposta_OSCntCod[0];
            A1713Proposta_OSUndCntCod = BC004821_A1713Proposta_OSUndCntCod[0];
            n1713Proposta_OSUndCntCod = BC004821_n1713Proposta_OSUndCntCod[0];
            A1717Proposta_OSPFB = BC004821_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = BC004821_n1717Proposta_OSPFB[0];
            A1718Proposta_OSPFL = BC004821_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = BC004821_n1718Proposta_OSPFL[0];
            A1719Proposta_OSDataCnt = BC004821_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = BC004821_n1719Proposta_OSDataCnt[0];
            A1720Proposta_OSHoraCnt = BC004821_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = BC004821_n1720Proposta_OSHoraCnt[0];
            ZM48187( -6) ;
         }
         pr_default.close(11);
         OnLoadActions48187( ) ;
      }

      protected void OnLoadActions48187( )
      {
      }

      protected void CheckExtendedTable48187( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00484 */
         pr_default.execute(2, new Object[] {n648Projeto_Codigo, A648Projeto_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A648Projeto_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_CODIGO");
               AnyError = 1;
            }
         }
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A1687Proposta_Vigencia) || ( A1687Proposta_Vigencia >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Proposta_Vigencia fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00485 */
         pr_default.execute(3, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A1694Proposta_RestricaoImplantacao) || ( A1694Proposta_RestricaoImplantacao >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Proposta_Restricao Implantacao fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A1702Proposta_Prazo) || ( A1702Proposta_Prazo >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Prazo fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00486 */
         pr_default.execute(4, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Dados da OS'.", "ForeignKeyNotFound", 1, "PROPOSTA_OSCODIGO");
            AnyError = 1;
         }
         A1764Proposta_OSDmn = BC00486_A1764Proposta_OSDmn[0];
         n1764Proposta_OSDmn = BC00486_n1764Proposta_OSDmn[0];
         A1711Proposta_OSDmnFM = BC00486_A1711Proposta_OSDmnFM[0];
         n1711Proposta_OSDmnFM = BC00486_n1711Proposta_OSDmnFM[0];
         A1792Proposta_OSInicio = BC00486_A1792Proposta_OSInicio[0];
         n1792Proposta_OSInicio = BC00486_n1792Proposta_OSInicio[0];
         A1705Proposta_OSServico = BC00486_A1705Proposta_OSServico[0];
         n1705Proposta_OSServico = BC00486_n1705Proposta_OSServico[0];
         pr_default.close(4);
         /* Using cursor BC00487 */
         pr_default.execute(5, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1705Proposta_OSServico) ) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1707Proposta_OSPrzTpDias = BC00487_A1707Proposta_OSPrzTpDias[0];
         n1707Proposta_OSPrzTpDias = BC00487_n1707Proposta_OSPrzTpDias[0];
         A1721Proposta_OSPrzRsp = BC00487_A1721Proposta_OSPrzRsp[0];
         n1721Proposta_OSPrzRsp = BC00487_n1721Proposta_OSPrzRsp[0];
         A1763Proposta_OSCntSrvFtrm = BC00487_A1763Proposta_OSCntSrvFtrm[0];
         n1763Proposta_OSCntSrvFtrm = BC00487_n1763Proposta_OSCntSrvFtrm[0];
         A1715Proposta_OSCntCod = BC00487_A1715Proposta_OSCntCod[0];
         n1715Proposta_OSCntCod = BC00487_n1715Proposta_OSCntCod[0];
         A1713Proposta_OSUndCntCod = BC00487_A1713Proposta_OSUndCntCod[0];
         n1713Proposta_OSUndCntCod = BC00487_n1713Proposta_OSUndCntCod[0];
         pr_default.close(5);
         /* Using cursor BC00488 */
         pr_default.execute(6, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1713Proposta_OSUndCntCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Unidades de Medi��o'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1710Proposta_OSUndCntSgl = BC00488_A1710Proposta_OSUndCntSgl[0];
         n1710Proposta_OSUndCntSgl = BC00488_n1710Proposta_OSUndCntSgl[0];
         pr_default.close(6);
         /* Using cursor BC004810 */
         pr_default.execute(7, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A1717Proposta_OSPFB = BC004810_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = BC004810_n1717Proposta_OSPFB[0];
         }
         else
         {
            A1717Proposta_OSPFB = 0;
            n1717Proposta_OSPFB = false;
         }
         pr_default.close(7);
         /* Using cursor BC004812 */
         pr_default.execute(8, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A1718Proposta_OSPFL = BC004812_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = BC004812_n1718Proposta_OSPFL[0];
         }
         else
         {
            A1718Proposta_OSPFL = 0;
            n1718Proposta_OSPFL = false;
         }
         pr_default.close(8);
         /* Using cursor BC004814 */
         pr_default.execute(9, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A1719Proposta_OSDataCnt = BC004814_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = BC004814_n1719Proposta_OSDataCnt[0];
         }
         else
         {
            A1719Proposta_OSDataCnt = DateTime.MinValue;
            n1719Proposta_OSDataCnt = false;
         }
         pr_default.close(9);
         /* Using cursor BC004816 */
         pr_default.execute(10, new Object[] {A1686Proposta_OSCodigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A1720Proposta_OSHoraCnt = BC004816_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = BC004816_n1720Proposta_OSHoraCnt[0];
         }
         else
         {
            A1720Proposta_OSHoraCnt = "";
            n1720Proposta_OSHoraCnt = false;
         }
         pr_default.close(10);
      }

      protected void CloseExtendedTableCursors48187( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(9);
         pr_default.close(10);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey48187( )
      {
         /* Using cursor BC004822 */
         pr_default.execute(12, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound187 = 1;
         }
         else
         {
            RcdFound187 = 0;
         }
         pr_default.close(12);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00483 */
         pr_default.execute(1, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM48187( 6) ;
            RcdFound187 = 1;
            A1685Proposta_Codigo = BC00483_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC00483_n1685Proposta_Codigo[0];
            A1687Proposta_Vigencia = BC00483_A1687Proposta_Vigencia[0];
            n1687Proposta_Vigencia = BC00483_n1687Proposta_Vigencia[0];
            A1688Proposta_Valor = BC00483_A1688Proposta_Valor[0];
            A1689Proposta_Status = BC00483_A1689Proposta_Status[0];
            n1689Proposta_Status = BC00483_n1689Proposta_Status[0];
            A1690Proposta_Objetivo = BC00483_A1690Proposta_Objetivo[0];
            A1691Proposta_Escopo = BC00483_A1691Proposta_Escopo[0];
            n1691Proposta_Escopo = BC00483_n1691Proposta_Escopo[0];
            A1692Proposta_Oportunidade = BC00483_A1692Proposta_Oportunidade[0];
            n1692Proposta_Oportunidade = BC00483_n1692Proposta_Oportunidade[0];
            A1693Proposta_Perspectiva = BC00483_A1693Proposta_Perspectiva[0];
            n1693Proposta_Perspectiva = BC00483_n1693Proposta_Perspectiva[0];
            A1694Proposta_RestricaoImplantacao = BC00483_A1694Proposta_RestricaoImplantacao[0];
            n1694Proposta_RestricaoImplantacao = BC00483_n1694Proposta_RestricaoImplantacao[0];
            A1695Proposta_RestricaoCusto = BC00483_A1695Proposta_RestricaoCusto[0];
            n1695Proposta_RestricaoCusto = BC00483_n1695Proposta_RestricaoCusto[0];
            A1696Proposta_Ativo = BC00483_A1696Proposta_Ativo[0];
            A1699Proposta_ContratadaCod = BC00483_A1699Proposta_ContratadaCod[0];
            A1700Proposta_ContratadaRazSocial = BC00483_A1700Proposta_ContratadaRazSocial[0];
            n1700Proposta_ContratadaRazSocial = BC00483_n1700Proposta_ContratadaRazSocial[0];
            A1702Proposta_Prazo = BC00483_A1702Proposta_Prazo[0];
            A1793Proposta_PrazoDias = BC00483_A1793Proposta_PrazoDias[0];
            n1793Proposta_PrazoDias = BC00483_n1793Proposta_PrazoDias[0];
            A1703Proposta_Esforco = BC00483_A1703Proposta_Esforco[0];
            A1794Proposta_Liquido = BC00483_A1794Proposta_Liquido[0];
            n1794Proposta_Liquido = BC00483_n1794Proposta_Liquido[0];
            A1704Proposta_CntSrvCod = BC00483_A1704Proposta_CntSrvCod[0];
            A1722Proposta_ValorUndCnt = BC00483_A1722Proposta_ValorUndCnt[0];
            A648Projeto_Codigo = BC00483_A648Projeto_Codigo[0];
            n648Projeto_Codigo = BC00483_n648Projeto_Codigo[0];
            A5AreaTrabalho_Codigo = BC00483_A5AreaTrabalho_Codigo[0];
            A1686Proposta_OSCodigo = BC00483_A1686Proposta_OSCodigo[0];
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
            sMode187 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load48187( ) ;
            if ( AnyError == 1 )
            {
               RcdFound187 = 0;
               InitializeNonKey48187( ) ;
            }
            Gx_mode = sMode187;
         }
         else
         {
            RcdFound187 = 0;
            InitializeNonKey48187( ) ;
            sMode187 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode187;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey48187( ) ;
         if ( RcdFound187 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_480( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency48187( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00482 */
            pr_default.execute(0, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PROPOSTA"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1687Proposta_Vigencia != BC00482_A1687Proposta_Vigencia[0] ) || ( Z1688Proposta_Valor != BC00482_A1688Proposta_Valor[0] ) || ( StringUtil.StrCmp(Z1689Proposta_Status, BC00482_A1689Proposta_Status[0]) != 0 ) || ( Z1694Proposta_RestricaoImplantacao != BC00482_A1694Proposta_RestricaoImplantacao[0] ) || ( Z1695Proposta_RestricaoCusto != BC00482_A1695Proposta_RestricaoCusto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1696Proposta_Ativo != BC00482_A1696Proposta_Ativo[0] ) || ( Z1699Proposta_ContratadaCod != BC00482_A1699Proposta_ContratadaCod[0] ) || ( StringUtil.StrCmp(Z1700Proposta_ContratadaRazSocial, BC00482_A1700Proposta_ContratadaRazSocial[0]) != 0 ) || ( Z1702Proposta_Prazo != BC00482_A1702Proposta_Prazo[0] ) || ( Z1793Proposta_PrazoDias != BC00482_A1793Proposta_PrazoDias[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1703Proposta_Esforco != BC00482_A1703Proposta_Esforco[0] ) || ( Z1794Proposta_Liquido != BC00482_A1794Proposta_Liquido[0] ) || ( Z1704Proposta_CntSrvCod != BC00482_A1704Proposta_CntSrvCod[0] ) || ( Z1722Proposta_ValorUndCnt != BC00482_A1722Proposta_ValorUndCnt[0] ) || ( Z648Projeto_Codigo != BC00482_A648Projeto_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z5AreaTrabalho_Codigo != BC00482_A5AreaTrabalho_Codigo[0] ) || ( Z1686Proposta_OSCodigo != BC00482_A1686Proposta_OSCodigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PROPOSTA"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert48187( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable48187( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM48187( 0) ;
            CheckOptimisticConcurrency48187( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm48187( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert48187( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004823 */
                     pr_default.execute(13, new Object[] {n1687Proposta_Vigencia, A1687Proposta_Vigencia, A1688Proposta_Valor, n1689Proposta_Status, A1689Proposta_Status, A1690Proposta_Objetivo, n1691Proposta_Escopo, A1691Proposta_Escopo, n1692Proposta_Oportunidade, A1692Proposta_Oportunidade, n1693Proposta_Perspectiva, A1693Proposta_Perspectiva, n1694Proposta_RestricaoImplantacao, A1694Proposta_RestricaoImplantacao, n1695Proposta_RestricaoCusto, A1695Proposta_RestricaoCusto, A1696Proposta_Ativo, A1699Proposta_ContratadaCod, n1700Proposta_ContratadaRazSocial, A1700Proposta_ContratadaRazSocial, A1702Proposta_Prazo, n1793Proposta_PrazoDias, A1793Proposta_PrazoDias, A1703Proposta_Esforco, n1794Proposta_Liquido, A1794Proposta_Liquido, A1704Proposta_CntSrvCod, A1722Proposta_ValorUndCnt, n648Projeto_Codigo, A648Projeto_Codigo, A5AreaTrabalho_Codigo, A1686Proposta_OSCodigo});
                     A1685Proposta_Codigo = BC004823_A1685Proposta_Codigo[0];
                     n1685Proposta_Codigo = BC004823_n1685Proposta_Codigo[0];
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("PROPOSTA") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load48187( ) ;
            }
            EndLevel48187( ) ;
         }
         CloseExtendedTableCursors48187( ) ;
      }

      protected void Update48187( )
      {
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable48187( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency48187( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm48187( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate48187( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004824 */
                     pr_default.execute(14, new Object[] {n1687Proposta_Vigencia, A1687Proposta_Vigencia, A1688Proposta_Valor, n1689Proposta_Status, A1689Proposta_Status, A1690Proposta_Objetivo, n1691Proposta_Escopo, A1691Proposta_Escopo, n1692Proposta_Oportunidade, A1692Proposta_Oportunidade, n1693Proposta_Perspectiva, A1693Proposta_Perspectiva, n1694Proposta_RestricaoImplantacao, A1694Proposta_RestricaoImplantacao, n1695Proposta_RestricaoCusto, A1695Proposta_RestricaoCusto, A1696Proposta_Ativo, A1699Proposta_ContratadaCod, n1700Proposta_ContratadaRazSocial, A1700Proposta_ContratadaRazSocial, A1702Proposta_Prazo, n1793Proposta_PrazoDias, A1793Proposta_PrazoDias, A1703Proposta_Esforco, n1794Proposta_Liquido, A1794Proposta_Liquido, A1704Proposta_CntSrvCod, A1722Proposta_ValorUndCnt, n648Projeto_Codigo, A648Projeto_Codigo, A5AreaTrabalho_Codigo, A1686Proposta_OSCodigo, n1685Proposta_Codigo, A1685Proposta_Codigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("PROPOSTA") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PROPOSTA"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate48187( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel48187( ) ;
         }
         CloseExtendedTableCursors48187( ) ;
      }

      protected void DeferredUpdate48187( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate48187( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency48187( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls48187( ) ;
            AfterConfirm48187( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete48187( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004825 */
                  pr_default.execute(15, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("PROPOSTA") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode187 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel48187( ) ;
         Gx_mode = sMode187;
      }

      protected void OnDeleteControls48187( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004826 */
            pr_default.execute(16, new Object[] {A1686Proposta_OSCodigo});
            A1764Proposta_OSDmn = BC004826_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = BC004826_n1764Proposta_OSDmn[0];
            A1711Proposta_OSDmnFM = BC004826_A1711Proposta_OSDmnFM[0];
            n1711Proposta_OSDmnFM = BC004826_n1711Proposta_OSDmnFM[0];
            A1792Proposta_OSInicio = BC004826_A1792Proposta_OSInicio[0];
            n1792Proposta_OSInicio = BC004826_n1792Proposta_OSInicio[0];
            A1705Proposta_OSServico = BC004826_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = BC004826_n1705Proposta_OSServico[0];
            pr_default.close(16);
            /* Using cursor BC004827 */
            pr_default.execute(17, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
            A1707Proposta_OSPrzTpDias = BC004827_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = BC004827_n1707Proposta_OSPrzTpDias[0];
            A1721Proposta_OSPrzRsp = BC004827_A1721Proposta_OSPrzRsp[0];
            n1721Proposta_OSPrzRsp = BC004827_n1721Proposta_OSPrzRsp[0];
            A1763Proposta_OSCntSrvFtrm = BC004827_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = BC004827_n1763Proposta_OSCntSrvFtrm[0];
            A1715Proposta_OSCntCod = BC004827_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = BC004827_n1715Proposta_OSCntCod[0];
            A1713Proposta_OSUndCntCod = BC004827_A1713Proposta_OSUndCntCod[0];
            n1713Proposta_OSUndCntCod = BC004827_n1713Proposta_OSUndCntCod[0];
            pr_default.close(17);
            /* Using cursor BC004828 */
            pr_default.execute(18, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
            A1710Proposta_OSUndCntSgl = BC004828_A1710Proposta_OSUndCntSgl[0];
            n1710Proposta_OSUndCntSgl = BC004828_n1710Proposta_OSUndCntSgl[0];
            pr_default.close(18);
            /* Using cursor BC004830 */
            pr_default.execute(19, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               A1717Proposta_OSPFB = BC004830_A1717Proposta_OSPFB[0];
               n1717Proposta_OSPFB = BC004830_n1717Proposta_OSPFB[0];
            }
            else
            {
               A1717Proposta_OSPFB = 0;
               n1717Proposta_OSPFB = false;
            }
            pr_default.close(19);
            /* Using cursor BC004832 */
            pr_default.execute(20, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               A1718Proposta_OSPFL = BC004832_A1718Proposta_OSPFL[0];
               n1718Proposta_OSPFL = BC004832_n1718Proposta_OSPFL[0];
            }
            else
            {
               A1718Proposta_OSPFL = 0;
               n1718Proposta_OSPFL = false;
            }
            pr_default.close(20);
            /* Using cursor BC004834 */
            pr_default.execute(21, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               A1719Proposta_OSDataCnt = BC004834_A1719Proposta_OSDataCnt[0];
               n1719Proposta_OSDataCnt = BC004834_n1719Proposta_OSDataCnt[0];
            }
            else
            {
               A1719Proposta_OSDataCnt = DateTime.MinValue;
               n1719Proposta_OSDataCnt = false;
            }
            pr_default.close(21);
            /* Using cursor BC004836 */
            pr_default.execute(22, new Object[] {A1686Proposta_OSCodigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               A1720Proposta_OSHoraCnt = BC004836_A1720Proposta_OSHoraCnt[0];
               n1720Proposta_OSHoraCnt = BC004836_n1720Proposta_OSHoraCnt[0];
            }
            else
            {
               A1720Proposta_OSHoraCnt = "";
               n1720Proposta_OSHoraCnt = false;
            }
            pr_default.close(22);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC004837 */
            pr_default.execute(23, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T213"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
         }
      }

      protected void EndLevel48187( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete48187( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart48187( )
      {
         /* Scan By routine */
         /* Using cursor BC004842 */
         pr_default.execute(24, new Object[] {n1685Proposta_Codigo, A1685Proposta_Codigo});
         RcdFound187 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound187 = 1;
            A1685Proposta_Codigo = BC004842_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC004842_n1685Proposta_Codigo[0];
            A1687Proposta_Vigencia = BC004842_A1687Proposta_Vigencia[0];
            n1687Proposta_Vigencia = BC004842_n1687Proposta_Vigencia[0];
            A1688Proposta_Valor = BC004842_A1688Proposta_Valor[0];
            A1689Proposta_Status = BC004842_A1689Proposta_Status[0];
            n1689Proposta_Status = BC004842_n1689Proposta_Status[0];
            A1690Proposta_Objetivo = BC004842_A1690Proposta_Objetivo[0];
            A1691Proposta_Escopo = BC004842_A1691Proposta_Escopo[0];
            n1691Proposta_Escopo = BC004842_n1691Proposta_Escopo[0];
            A1692Proposta_Oportunidade = BC004842_A1692Proposta_Oportunidade[0];
            n1692Proposta_Oportunidade = BC004842_n1692Proposta_Oportunidade[0];
            A1693Proposta_Perspectiva = BC004842_A1693Proposta_Perspectiva[0];
            n1693Proposta_Perspectiva = BC004842_n1693Proposta_Perspectiva[0];
            A1694Proposta_RestricaoImplantacao = BC004842_A1694Proposta_RestricaoImplantacao[0];
            n1694Proposta_RestricaoImplantacao = BC004842_n1694Proposta_RestricaoImplantacao[0];
            A1695Proposta_RestricaoCusto = BC004842_A1695Proposta_RestricaoCusto[0];
            n1695Proposta_RestricaoCusto = BC004842_n1695Proposta_RestricaoCusto[0];
            A1696Proposta_Ativo = BC004842_A1696Proposta_Ativo[0];
            A1699Proposta_ContratadaCod = BC004842_A1699Proposta_ContratadaCod[0];
            A1700Proposta_ContratadaRazSocial = BC004842_A1700Proposta_ContratadaRazSocial[0];
            n1700Proposta_ContratadaRazSocial = BC004842_n1700Proposta_ContratadaRazSocial[0];
            A1702Proposta_Prazo = BC004842_A1702Proposta_Prazo[0];
            A1793Proposta_PrazoDias = BC004842_A1793Proposta_PrazoDias[0];
            n1793Proposta_PrazoDias = BC004842_n1793Proposta_PrazoDias[0];
            A1703Proposta_Esforco = BC004842_A1703Proposta_Esforco[0];
            A1794Proposta_Liquido = BC004842_A1794Proposta_Liquido[0];
            n1794Proposta_Liquido = BC004842_n1794Proposta_Liquido[0];
            A1704Proposta_CntSrvCod = BC004842_A1704Proposta_CntSrvCod[0];
            A1722Proposta_ValorUndCnt = BC004842_A1722Proposta_ValorUndCnt[0];
            A1764Proposta_OSDmn = BC004842_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = BC004842_n1764Proposta_OSDmn[0];
            A1711Proposta_OSDmnFM = BC004842_A1711Proposta_OSDmnFM[0];
            n1711Proposta_OSDmnFM = BC004842_n1711Proposta_OSDmnFM[0];
            A1710Proposta_OSUndCntSgl = BC004842_A1710Proposta_OSUndCntSgl[0];
            n1710Proposta_OSUndCntSgl = BC004842_n1710Proposta_OSUndCntSgl[0];
            A1707Proposta_OSPrzTpDias = BC004842_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = BC004842_n1707Proposta_OSPrzTpDias[0];
            A1721Proposta_OSPrzRsp = BC004842_A1721Proposta_OSPrzRsp[0];
            n1721Proposta_OSPrzRsp = BC004842_n1721Proposta_OSPrzRsp[0];
            A1763Proposta_OSCntSrvFtrm = BC004842_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = BC004842_n1763Proposta_OSCntSrvFtrm[0];
            A1792Proposta_OSInicio = BC004842_A1792Proposta_OSInicio[0];
            n1792Proposta_OSInicio = BC004842_n1792Proposta_OSInicio[0];
            A648Projeto_Codigo = BC004842_A648Projeto_Codigo[0];
            n648Projeto_Codigo = BC004842_n648Projeto_Codigo[0];
            A5AreaTrabalho_Codigo = BC004842_A5AreaTrabalho_Codigo[0];
            A1686Proposta_OSCodigo = BC004842_A1686Proposta_OSCodigo[0];
            A1705Proposta_OSServico = BC004842_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = BC004842_n1705Proposta_OSServico[0];
            A1715Proposta_OSCntCod = BC004842_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = BC004842_n1715Proposta_OSCntCod[0];
            A1713Proposta_OSUndCntCod = BC004842_A1713Proposta_OSUndCntCod[0];
            n1713Proposta_OSUndCntCod = BC004842_n1713Proposta_OSUndCntCod[0];
            A1717Proposta_OSPFB = BC004842_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = BC004842_n1717Proposta_OSPFB[0];
            A1718Proposta_OSPFL = BC004842_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = BC004842_n1718Proposta_OSPFL[0];
            A1719Proposta_OSDataCnt = BC004842_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = BC004842_n1719Proposta_OSDataCnt[0];
            A1720Proposta_OSHoraCnt = BC004842_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = BC004842_n1720Proposta_OSHoraCnt[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext48187( )
      {
         /* Scan next routine */
         pr_default.readNext(24);
         RcdFound187 = 0;
         ScanKeyLoad48187( ) ;
      }

      protected void ScanKeyLoad48187( )
      {
         sMode187 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound187 = 1;
            A1685Proposta_Codigo = BC004842_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = BC004842_n1685Proposta_Codigo[0];
            A1687Proposta_Vigencia = BC004842_A1687Proposta_Vigencia[0];
            n1687Proposta_Vigencia = BC004842_n1687Proposta_Vigencia[0];
            A1688Proposta_Valor = BC004842_A1688Proposta_Valor[0];
            A1689Proposta_Status = BC004842_A1689Proposta_Status[0];
            n1689Proposta_Status = BC004842_n1689Proposta_Status[0];
            A1690Proposta_Objetivo = BC004842_A1690Proposta_Objetivo[0];
            A1691Proposta_Escopo = BC004842_A1691Proposta_Escopo[0];
            n1691Proposta_Escopo = BC004842_n1691Proposta_Escopo[0];
            A1692Proposta_Oportunidade = BC004842_A1692Proposta_Oportunidade[0];
            n1692Proposta_Oportunidade = BC004842_n1692Proposta_Oportunidade[0];
            A1693Proposta_Perspectiva = BC004842_A1693Proposta_Perspectiva[0];
            n1693Proposta_Perspectiva = BC004842_n1693Proposta_Perspectiva[0];
            A1694Proposta_RestricaoImplantacao = BC004842_A1694Proposta_RestricaoImplantacao[0];
            n1694Proposta_RestricaoImplantacao = BC004842_n1694Proposta_RestricaoImplantacao[0];
            A1695Proposta_RestricaoCusto = BC004842_A1695Proposta_RestricaoCusto[0];
            n1695Proposta_RestricaoCusto = BC004842_n1695Proposta_RestricaoCusto[0];
            A1696Proposta_Ativo = BC004842_A1696Proposta_Ativo[0];
            A1699Proposta_ContratadaCod = BC004842_A1699Proposta_ContratadaCod[0];
            A1700Proposta_ContratadaRazSocial = BC004842_A1700Proposta_ContratadaRazSocial[0];
            n1700Proposta_ContratadaRazSocial = BC004842_n1700Proposta_ContratadaRazSocial[0];
            A1702Proposta_Prazo = BC004842_A1702Proposta_Prazo[0];
            A1793Proposta_PrazoDias = BC004842_A1793Proposta_PrazoDias[0];
            n1793Proposta_PrazoDias = BC004842_n1793Proposta_PrazoDias[0];
            A1703Proposta_Esforco = BC004842_A1703Proposta_Esforco[0];
            A1794Proposta_Liquido = BC004842_A1794Proposta_Liquido[0];
            n1794Proposta_Liquido = BC004842_n1794Proposta_Liquido[0];
            A1704Proposta_CntSrvCod = BC004842_A1704Proposta_CntSrvCod[0];
            A1722Proposta_ValorUndCnt = BC004842_A1722Proposta_ValorUndCnt[0];
            A1764Proposta_OSDmn = BC004842_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = BC004842_n1764Proposta_OSDmn[0];
            A1711Proposta_OSDmnFM = BC004842_A1711Proposta_OSDmnFM[0];
            n1711Proposta_OSDmnFM = BC004842_n1711Proposta_OSDmnFM[0];
            A1710Proposta_OSUndCntSgl = BC004842_A1710Proposta_OSUndCntSgl[0];
            n1710Proposta_OSUndCntSgl = BC004842_n1710Proposta_OSUndCntSgl[0];
            A1707Proposta_OSPrzTpDias = BC004842_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = BC004842_n1707Proposta_OSPrzTpDias[0];
            A1721Proposta_OSPrzRsp = BC004842_A1721Proposta_OSPrzRsp[0];
            n1721Proposta_OSPrzRsp = BC004842_n1721Proposta_OSPrzRsp[0];
            A1763Proposta_OSCntSrvFtrm = BC004842_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = BC004842_n1763Proposta_OSCntSrvFtrm[0];
            A1792Proposta_OSInicio = BC004842_A1792Proposta_OSInicio[0];
            n1792Proposta_OSInicio = BC004842_n1792Proposta_OSInicio[0];
            A648Projeto_Codigo = BC004842_A648Projeto_Codigo[0];
            n648Projeto_Codigo = BC004842_n648Projeto_Codigo[0];
            A5AreaTrabalho_Codigo = BC004842_A5AreaTrabalho_Codigo[0];
            A1686Proposta_OSCodigo = BC004842_A1686Proposta_OSCodigo[0];
            A1705Proposta_OSServico = BC004842_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = BC004842_n1705Proposta_OSServico[0];
            A1715Proposta_OSCntCod = BC004842_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = BC004842_n1715Proposta_OSCntCod[0];
            A1713Proposta_OSUndCntCod = BC004842_A1713Proposta_OSUndCntCod[0];
            n1713Proposta_OSUndCntCod = BC004842_n1713Proposta_OSUndCntCod[0];
            A1717Proposta_OSPFB = BC004842_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = BC004842_n1717Proposta_OSPFB[0];
            A1718Proposta_OSPFL = BC004842_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = BC004842_n1718Proposta_OSPFL[0];
            A1719Proposta_OSDataCnt = BC004842_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = BC004842_n1719Proposta_OSDataCnt[0];
            A1720Proposta_OSHoraCnt = BC004842_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = BC004842_n1720Proposta_OSHoraCnt[0];
         }
         Gx_mode = sMode187;
      }

      protected void ScanKeyEnd48187( )
      {
         pr_default.close(24);
      }

      protected void AfterConfirm48187( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert48187( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate48187( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete48187( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete48187( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate48187( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes48187( )
      {
      }

      protected void AddRow48187( )
      {
         VarsToRow187( bcProposta) ;
      }

      protected void ReadRow48187( )
      {
         RowToVars187( bcProposta, 1) ;
      }

      protected void InitializeNonKey48187( )
      {
         A648Projeto_Codigo = 0;
         n648Projeto_Codigo = false;
         A1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         n1687Proposta_Vigencia = false;
         A1688Proposta_Valor = 0;
         A1689Proposta_Status = "";
         n1689Proposta_Status = false;
         A5AreaTrabalho_Codigo = 0;
         A1690Proposta_Objetivo = "";
         A1691Proposta_Escopo = "";
         n1691Proposta_Escopo = false;
         A1692Proposta_Oportunidade = "";
         n1692Proposta_Oportunidade = false;
         A1693Proposta_Perspectiva = "";
         n1693Proposta_Perspectiva = false;
         A1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         n1694Proposta_RestricaoImplantacao = false;
         A1695Proposta_RestricaoCusto = 0;
         n1695Proposta_RestricaoCusto = false;
         A1696Proposta_Ativo = false;
         A1699Proposta_ContratadaCod = 0;
         A1700Proposta_ContratadaRazSocial = "";
         n1700Proposta_ContratadaRazSocial = false;
         A1702Proposta_Prazo = DateTime.MinValue;
         A1793Proposta_PrazoDias = 0;
         n1793Proposta_PrazoDias = false;
         A1703Proposta_Esforco = 0;
         A1794Proposta_Liquido = 0;
         n1794Proposta_Liquido = false;
         A1704Proposta_CntSrvCod = 0;
         A1722Proposta_ValorUndCnt = 0;
         A1686Proposta_OSCodigo = 0;
         A1705Proposta_OSServico = 0;
         n1705Proposta_OSServico = false;
         A1715Proposta_OSCntCod = 0;
         n1715Proposta_OSCntCod = false;
         A1764Proposta_OSDmn = "";
         n1764Proposta_OSDmn = false;
         A1711Proposta_OSDmnFM = "";
         n1711Proposta_OSDmnFM = false;
         A1713Proposta_OSUndCntCod = 0;
         n1713Proposta_OSUndCntCod = false;
         A1710Proposta_OSUndCntSgl = "";
         n1710Proposta_OSUndCntSgl = false;
         A1707Proposta_OSPrzTpDias = "";
         n1707Proposta_OSPrzTpDias = false;
         A1721Proposta_OSPrzRsp = 0;
         n1721Proposta_OSPrzRsp = false;
         A1717Proposta_OSPFB = 0;
         n1717Proposta_OSPFB = false;
         A1718Proposta_OSPFL = 0;
         n1718Proposta_OSPFL = false;
         A1719Proposta_OSDataCnt = DateTime.MinValue;
         n1719Proposta_OSDataCnt = false;
         A1720Proposta_OSHoraCnt = "";
         n1720Proposta_OSHoraCnt = false;
         A1763Proposta_OSCntSrvFtrm = "";
         n1763Proposta_OSCntSrvFtrm = false;
         A1792Proposta_OSInicio = DateTime.MinValue;
         n1792Proposta_OSInicio = false;
         Z1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         Z1688Proposta_Valor = 0;
         Z1689Proposta_Status = "";
         Z1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         Z1695Proposta_RestricaoCusto = 0;
         Z1696Proposta_Ativo = false;
         Z1699Proposta_ContratadaCod = 0;
         Z1700Proposta_ContratadaRazSocial = "";
         Z1702Proposta_Prazo = DateTime.MinValue;
         Z1793Proposta_PrazoDias = 0;
         Z1703Proposta_Esforco = 0;
         Z1794Proposta_Liquido = 0;
         Z1704Proposta_CntSrvCod = 0;
         Z1722Proposta_ValorUndCnt = 0;
         Z648Projeto_Codigo = 0;
         Z5AreaTrabalho_Codigo = 0;
         Z1686Proposta_OSCodigo = 0;
      }

      protected void InitAll48187( )
      {
         A1685Proposta_Codigo = 0;
         n1685Proposta_Codigo = false;
         InitializeNonKey48187( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow187( SdtProposta obj187 )
      {
         obj187.gxTpr_Mode = Gx_mode;
         obj187.gxTpr_Projeto_codigo = A648Projeto_Codigo;
         obj187.gxTpr_Proposta_vigencia = A1687Proposta_Vigencia;
         obj187.gxTpr_Proposta_valor = A1688Proposta_Valor;
         obj187.gxTpr_Proposta_status = A1689Proposta_Status;
         obj187.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
         obj187.gxTpr_Proposta_objetivo = A1690Proposta_Objetivo;
         obj187.gxTpr_Proposta_escopo = A1691Proposta_Escopo;
         obj187.gxTpr_Proposta_oportunidade = A1692Proposta_Oportunidade;
         obj187.gxTpr_Proposta_perspectiva = A1693Proposta_Perspectiva;
         obj187.gxTpr_Proposta_restricaoimplantacao = A1694Proposta_RestricaoImplantacao;
         obj187.gxTpr_Proposta_restricaocusto = A1695Proposta_RestricaoCusto;
         obj187.gxTpr_Proposta_ativo = A1696Proposta_Ativo;
         obj187.gxTpr_Proposta_contratadacod = A1699Proposta_ContratadaCod;
         obj187.gxTpr_Proposta_contratadarazsocial = A1700Proposta_ContratadaRazSocial;
         obj187.gxTpr_Proposta_prazo = A1702Proposta_Prazo;
         obj187.gxTpr_Proposta_prazodias = A1793Proposta_PrazoDias;
         obj187.gxTpr_Proposta_esforco = A1703Proposta_Esforco;
         obj187.gxTpr_Proposta_liquido = A1794Proposta_Liquido;
         obj187.gxTpr_Proposta_cntsrvcod = A1704Proposta_CntSrvCod;
         obj187.gxTpr_Proposta_valorundcnt = A1722Proposta_ValorUndCnt;
         obj187.gxTpr_Proposta_oscodigo = A1686Proposta_OSCodigo;
         obj187.gxTpr_Proposta_osservico = A1705Proposta_OSServico;
         obj187.gxTpr_Proposta_oscntcod = A1715Proposta_OSCntCod;
         obj187.gxTpr_Proposta_osdmn = A1764Proposta_OSDmn;
         obj187.gxTpr_Proposta_osdmnfm = A1711Proposta_OSDmnFM;
         obj187.gxTpr_Proposta_osundcntcod = A1713Proposta_OSUndCntCod;
         obj187.gxTpr_Proposta_osundcntsgl = A1710Proposta_OSUndCntSgl;
         obj187.gxTpr_Proposta_osprztpdias = A1707Proposta_OSPrzTpDias;
         obj187.gxTpr_Proposta_osprzrsp = A1721Proposta_OSPrzRsp;
         obj187.gxTpr_Proposta_ospfb = A1717Proposta_OSPFB;
         obj187.gxTpr_Proposta_ospfl = A1718Proposta_OSPFL;
         obj187.gxTpr_Proposta_osdatacnt = A1719Proposta_OSDataCnt;
         obj187.gxTpr_Proposta_oshoracnt = A1720Proposta_OSHoraCnt;
         obj187.gxTpr_Proposta_oscntsrvftrm = A1763Proposta_OSCntSrvFtrm;
         obj187.gxTpr_Proposta_osinicio = A1792Proposta_OSInicio;
         obj187.gxTpr_Proposta_codigo = A1685Proposta_Codigo;
         obj187.gxTpr_Proposta_codigo_Z = Z1685Proposta_Codigo;
         obj187.gxTpr_Projeto_codigo_Z = Z648Projeto_Codigo;
         obj187.gxTpr_Proposta_vigencia_Z = Z1687Proposta_Vigencia;
         obj187.gxTpr_Proposta_valor_Z = Z1688Proposta_Valor;
         obj187.gxTpr_Proposta_status_Z = Z1689Proposta_Status;
         obj187.gxTpr_Areatrabalho_codigo_Z = Z5AreaTrabalho_Codigo;
         obj187.gxTpr_Proposta_restricaoimplantacao_Z = Z1694Proposta_RestricaoImplantacao;
         obj187.gxTpr_Proposta_restricaocusto_Z = Z1695Proposta_RestricaoCusto;
         obj187.gxTpr_Proposta_ativo_Z = Z1696Proposta_Ativo;
         obj187.gxTpr_Proposta_contratadacod_Z = Z1699Proposta_ContratadaCod;
         obj187.gxTpr_Proposta_contratadarazsocial_Z = Z1700Proposta_ContratadaRazSocial;
         obj187.gxTpr_Proposta_prazo_Z = Z1702Proposta_Prazo;
         obj187.gxTpr_Proposta_prazodias_Z = Z1793Proposta_PrazoDias;
         obj187.gxTpr_Proposta_esforco_Z = Z1703Proposta_Esforco;
         obj187.gxTpr_Proposta_liquido_Z = Z1794Proposta_Liquido;
         obj187.gxTpr_Proposta_cntsrvcod_Z = Z1704Proposta_CntSrvCod;
         obj187.gxTpr_Proposta_valorundcnt_Z = Z1722Proposta_ValorUndCnt;
         obj187.gxTpr_Proposta_oscodigo_Z = Z1686Proposta_OSCodigo;
         obj187.gxTpr_Proposta_osservico_Z = Z1705Proposta_OSServico;
         obj187.gxTpr_Proposta_oscntcod_Z = Z1715Proposta_OSCntCod;
         obj187.gxTpr_Proposta_osdmn_Z = Z1764Proposta_OSDmn;
         obj187.gxTpr_Proposta_osdmnfm_Z = Z1711Proposta_OSDmnFM;
         obj187.gxTpr_Proposta_osundcntcod_Z = Z1713Proposta_OSUndCntCod;
         obj187.gxTpr_Proposta_osundcntsgl_Z = Z1710Proposta_OSUndCntSgl;
         obj187.gxTpr_Proposta_osprztpdias_Z = Z1707Proposta_OSPrzTpDias;
         obj187.gxTpr_Proposta_osprzrsp_Z = Z1721Proposta_OSPrzRsp;
         obj187.gxTpr_Proposta_ospfb_Z = Z1717Proposta_OSPFB;
         obj187.gxTpr_Proposta_ospfl_Z = Z1718Proposta_OSPFL;
         obj187.gxTpr_Proposta_osdatacnt_Z = Z1719Proposta_OSDataCnt;
         obj187.gxTpr_Proposta_oshoracnt_Z = Z1720Proposta_OSHoraCnt;
         obj187.gxTpr_Proposta_oscntsrvftrm_Z = Z1763Proposta_OSCntSrvFtrm;
         obj187.gxTpr_Proposta_osinicio_Z = Z1792Proposta_OSInicio;
         obj187.gxTpr_Proposta_codigo_N = (short)(Convert.ToInt16(n1685Proposta_Codigo));
         obj187.gxTpr_Projeto_codigo_N = (short)(Convert.ToInt16(n648Projeto_Codigo));
         obj187.gxTpr_Proposta_vigencia_N = (short)(Convert.ToInt16(n1687Proposta_Vigencia));
         obj187.gxTpr_Proposta_status_N = (short)(Convert.ToInt16(n1689Proposta_Status));
         obj187.gxTpr_Proposta_escopo_N = (short)(Convert.ToInt16(n1691Proposta_Escopo));
         obj187.gxTpr_Proposta_oportunidade_N = (short)(Convert.ToInt16(n1692Proposta_Oportunidade));
         obj187.gxTpr_Proposta_perspectiva_N = (short)(Convert.ToInt16(n1693Proposta_Perspectiva));
         obj187.gxTpr_Proposta_restricaoimplantacao_N = (short)(Convert.ToInt16(n1694Proposta_RestricaoImplantacao));
         obj187.gxTpr_Proposta_restricaocusto_N = (short)(Convert.ToInt16(n1695Proposta_RestricaoCusto));
         obj187.gxTpr_Proposta_contratadarazsocial_N = (short)(Convert.ToInt16(n1700Proposta_ContratadaRazSocial));
         obj187.gxTpr_Proposta_prazodias_N = (short)(Convert.ToInt16(n1793Proposta_PrazoDias));
         obj187.gxTpr_Proposta_liquido_N = (short)(Convert.ToInt16(n1794Proposta_Liquido));
         obj187.gxTpr_Proposta_osservico_N = (short)(Convert.ToInt16(n1705Proposta_OSServico));
         obj187.gxTpr_Proposta_oscntcod_N = (short)(Convert.ToInt16(n1715Proposta_OSCntCod));
         obj187.gxTpr_Proposta_osdmn_N = (short)(Convert.ToInt16(n1764Proposta_OSDmn));
         obj187.gxTpr_Proposta_osdmnfm_N = (short)(Convert.ToInt16(n1711Proposta_OSDmnFM));
         obj187.gxTpr_Proposta_osundcntcod_N = (short)(Convert.ToInt16(n1713Proposta_OSUndCntCod));
         obj187.gxTpr_Proposta_osundcntsgl_N = (short)(Convert.ToInt16(n1710Proposta_OSUndCntSgl));
         obj187.gxTpr_Proposta_osprztpdias_N = (short)(Convert.ToInt16(n1707Proposta_OSPrzTpDias));
         obj187.gxTpr_Proposta_osprzrsp_N = (short)(Convert.ToInt16(n1721Proposta_OSPrzRsp));
         obj187.gxTpr_Proposta_ospfb_N = (short)(Convert.ToInt16(n1717Proposta_OSPFB));
         obj187.gxTpr_Proposta_ospfl_N = (short)(Convert.ToInt16(n1718Proposta_OSPFL));
         obj187.gxTpr_Proposta_osdatacnt_N = (short)(Convert.ToInt16(n1719Proposta_OSDataCnt));
         obj187.gxTpr_Proposta_oshoracnt_N = (short)(Convert.ToInt16(n1720Proposta_OSHoraCnt));
         obj187.gxTpr_Proposta_oscntsrvftrm_N = (short)(Convert.ToInt16(n1763Proposta_OSCntSrvFtrm));
         obj187.gxTpr_Proposta_osinicio_N = (short)(Convert.ToInt16(n1792Proposta_OSInicio));
         obj187.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow187( SdtProposta obj187 )
      {
         obj187.gxTpr_Proposta_codigo = A1685Proposta_Codigo;
         return  ;
      }

      public void RowToVars187( SdtProposta obj187 ,
                                int forceLoad )
      {
         Gx_mode = obj187.gxTpr_Mode;
         A648Projeto_Codigo = obj187.gxTpr_Projeto_codigo;
         n648Projeto_Codigo = false;
         A1687Proposta_Vigencia = obj187.gxTpr_Proposta_vigencia;
         n1687Proposta_Vigencia = false;
         A1688Proposta_Valor = obj187.gxTpr_Proposta_valor;
         A1689Proposta_Status = obj187.gxTpr_Proposta_status;
         n1689Proposta_Status = false;
         A5AreaTrabalho_Codigo = obj187.gxTpr_Areatrabalho_codigo;
         A1690Proposta_Objetivo = obj187.gxTpr_Proposta_objetivo;
         A1691Proposta_Escopo = obj187.gxTpr_Proposta_escopo;
         n1691Proposta_Escopo = false;
         A1692Proposta_Oportunidade = obj187.gxTpr_Proposta_oportunidade;
         n1692Proposta_Oportunidade = false;
         A1693Proposta_Perspectiva = obj187.gxTpr_Proposta_perspectiva;
         n1693Proposta_Perspectiva = false;
         A1694Proposta_RestricaoImplantacao = obj187.gxTpr_Proposta_restricaoimplantacao;
         n1694Proposta_RestricaoImplantacao = false;
         A1695Proposta_RestricaoCusto = obj187.gxTpr_Proposta_restricaocusto;
         n1695Proposta_RestricaoCusto = false;
         A1696Proposta_Ativo = obj187.gxTpr_Proposta_ativo;
         A1699Proposta_ContratadaCod = obj187.gxTpr_Proposta_contratadacod;
         A1700Proposta_ContratadaRazSocial = obj187.gxTpr_Proposta_contratadarazsocial;
         n1700Proposta_ContratadaRazSocial = false;
         A1702Proposta_Prazo = obj187.gxTpr_Proposta_prazo;
         A1793Proposta_PrazoDias = obj187.gxTpr_Proposta_prazodias;
         n1793Proposta_PrazoDias = false;
         A1703Proposta_Esforco = obj187.gxTpr_Proposta_esforco;
         A1794Proposta_Liquido = obj187.gxTpr_Proposta_liquido;
         n1794Proposta_Liquido = false;
         A1704Proposta_CntSrvCod = obj187.gxTpr_Proposta_cntsrvcod;
         A1722Proposta_ValorUndCnt = obj187.gxTpr_Proposta_valorundcnt;
         A1686Proposta_OSCodigo = obj187.gxTpr_Proposta_oscodigo;
         A1705Proposta_OSServico = obj187.gxTpr_Proposta_osservico;
         n1705Proposta_OSServico = false;
         A1715Proposta_OSCntCod = obj187.gxTpr_Proposta_oscntcod;
         n1715Proposta_OSCntCod = false;
         A1764Proposta_OSDmn = obj187.gxTpr_Proposta_osdmn;
         n1764Proposta_OSDmn = false;
         A1711Proposta_OSDmnFM = obj187.gxTpr_Proposta_osdmnfm;
         n1711Proposta_OSDmnFM = false;
         A1713Proposta_OSUndCntCod = obj187.gxTpr_Proposta_osundcntcod;
         n1713Proposta_OSUndCntCod = false;
         A1710Proposta_OSUndCntSgl = obj187.gxTpr_Proposta_osundcntsgl;
         n1710Proposta_OSUndCntSgl = false;
         A1707Proposta_OSPrzTpDias = obj187.gxTpr_Proposta_osprztpdias;
         n1707Proposta_OSPrzTpDias = false;
         A1721Proposta_OSPrzRsp = obj187.gxTpr_Proposta_osprzrsp;
         n1721Proposta_OSPrzRsp = false;
         A1717Proposta_OSPFB = obj187.gxTpr_Proposta_ospfb;
         n1717Proposta_OSPFB = false;
         A1718Proposta_OSPFL = obj187.gxTpr_Proposta_ospfl;
         n1718Proposta_OSPFL = false;
         A1719Proposta_OSDataCnt = obj187.gxTpr_Proposta_osdatacnt;
         n1719Proposta_OSDataCnt = false;
         A1720Proposta_OSHoraCnt = obj187.gxTpr_Proposta_oshoracnt;
         n1720Proposta_OSHoraCnt = false;
         A1763Proposta_OSCntSrvFtrm = obj187.gxTpr_Proposta_oscntsrvftrm;
         n1763Proposta_OSCntSrvFtrm = false;
         A1792Proposta_OSInicio = obj187.gxTpr_Proposta_osinicio;
         n1792Proposta_OSInicio = false;
         A1685Proposta_Codigo = obj187.gxTpr_Proposta_codigo;
         n1685Proposta_Codigo = false;
         Z1685Proposta_Codigo = obj187.gxTpr_Proposta_codigo_Z;
         Z648Projeto_Codigo = obj187.gxTpr_Projeto_codigo_Z;
         Z1687Proposta_Vigencia = obj187.gxTpr_Proposta_vigencia_Z;
         Z1688Proposta_Valor = obj187.gxTpr_Proposta_valor_Z;
         Z1689Proposta_Status = obj187.gxTpr_Proposta_status_Z;
         Z5AreaTrabalho_Codigo = obj187.gxTpr_Areatrabalho_codigo_Z;
         Z1694Proposta_RestricaoImplantacao = obj187.gxTpr_Proposta_restricaoimplantacao_Z;
         Z1695Proposta_RestricaoCusto = obj187.gxTpr_Proposta_restricaocusto_Z;
         Z1696Proposta_Ativo = obj187.gxTpr_Proposta_ativo_Z;
         Z1699Proposta_ContratadaCod = obj187.gxTpr_Proposta_contratadacod_Z;
         Z1700Proposta_ContratadaRazSocial = obj187.gxTpr_Proposta_contratadarazsocial_Z;
         Z1702Proposta_Prazo = obj187.gxTpr_Proposta_prazo_Z;
         Z1793Proposta_PrazoDias = obj187.gxTpr_Proposta_prazodias_Z;
         Z1703Proposta_Esforco = obj187.gxTpr_Proposta_esforco_Z;
         Z1794Proposta_Liquido = obj187.gxTpr_Proposta_liquido_Z;
         Z1704Proposta_CntSrvCod = obj187.gxTpr_Proposta_cntsrvcod_Z;
         Z1722Proposta_ValorUndCnt = obj187.gxTpr_Proposta_valorundcnt_Z;
         Z1686Proposta_OSCodigo = obj187.gxTpr_Proposta_oscodigo_Z;
         Z1705Proposta_OSServico = obj187.gxTpr_Proposta_osservico_Z;
         Z1715Proposta_OSCntCod = obj187.gxTpr_Proposta_oscntcod_Z;
         Z1764Proposta_OSDmn = obj187.gxTpr_Proposta_osdmn_Z;
         Z1711Proposta_OSDmnFM = obj187.gxTpr_Proposta_osdmnfm_Z;
         Z1713Proposta_OSUndCntCod = obj187.gxTpr_Proposta_osundcntcod_Z;
         Z1710Proposta_OSUndCntSgl = obj187.gxTpr_Proposta_osundcntsgl_Z;
         Z1707Proposta_OSPrzTpDias = obj187.gxTpr_Proposta_osprztpdias_Z;
         Z1721Proposta_OSPrzRsp = obj187.gxTpr_Proposta_osprzrsp_Z;
         Z1717Proposta_OSPFB = obj187.gxTpr_Proposta_ospfb_Z;
         Z1718Proposta_OSPFL = obj187.gxTpr_Proposta_ospfl_Z;
         Z1719Proposta_OSDataCnt = obj187.gxTpr_Proposta_osdatacnt_Z;
         Z1720Proposta_OSHoraCnt = obj187.gxTpr_Proposta_oshoracnt_Z;
         Z1763Proposta_OSCntSrvFtrm = obj187.gxTpr_Proposta_oscntsrvftrm_Z;
         Z1792Proposta_OSInicio = obj187.gxTpr_Proposta_osinicio_Z;
         n1685Proposta_Codigo = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_codigo_N));
         n648Projeto_Codigo = (bool)(Convert.ToBoolean(obj187.gxTpr_Projeto_codigo_N));
         n1687Proposta_Vigencia = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_vigencia_N));
         n1689Proposta_Status = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_status_N));
         n1691Proposta_Escopo = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_escopo_N));
         n1692Proposta_Oportunidade = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_oportunidade_N));
         n1693Proposta_Perspectiva = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_perspectiva_N));
         n1694Proposta_RestricaoImplantacao = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_restricaoimplantacao_N));
         n1695Proposta_RestricaoCusto = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_restricaocusto_N));
         n1700Proposta_ContratadaRazSocial = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_contratadarazsocial_N));
         n1793Proposta_PrazoDias = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_prazodias_N));
         n1794Proposta_Liquido = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_liquido_N));
         n1705Proposta_OSServico = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osservico_N));
         n1715Proposta_OSCntCod = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_oscntcod_N));
         n1764Proposta_OSDmn = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osdmn_N));
         n1711Proposta_OSDmnFM = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osdmnfm_N));
         n1713Proposta_OSUndCntCod = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osundcntcod_N));
         n1710Proposta_OSUndCntSgl = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osundcntsgl_N));
         n1707Proposta_OSPrzTpDias = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osprztpdias_N));
         n1721Proposta_OSPrzRsp = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osprzrsp_N));
         n1717Proposta_OSPFB = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_ospfb_N));
         n1718Proposta_OSPFL = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_ospfl_N));
         n1719Proposta_OSDataCnt = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osdatacnt_N));
         n1720Proposta_OSHoraCnt = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_oshoracnt_N));
         n1763Proposta_OSCntSrvFtrm = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_oscntsrvftrm_N));
         n1792Proposta_OSInicio = (bool)(Convert.ToBoolean(obj187.gxTpr_Proposta_osinicio_N));
         Gx_mode = obj187.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1685Proposta_Codigo = (int)getParm(obj,0);
         n1685Proposta_Codigo = false;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey48187( ) ;
         ScanKeyStart48187( ) ;
         if ( RcdFound187 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
         }
         ZM48187( -6) ;
         OnLoadActions48187( ) ;
         AddRow48187( ) ;
         ScanKeyEnd48187( ) ;
         if ( RcdFound187 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars187( bcProposta, 0) ;
         ScanKeyStart48187( ) ;
         if ( RcdFound187 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1685Proposta_Codigo = A1685Proposta_Codigo;
         }
         ZM48187( -6) ;
         OnLoadActions48187( ) ;
         AddRow48187( ) ;
         ScanKeyEnd48187( ) ;
         if ( RcdFound187 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars187( bcProposta, 0) ;
         nKeyPressed = 1;
         GetKey48187( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert48187( ) ;
         }
         else
         {
            if ( RcdFound187 == 1 )
            {
               if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
               {
                  A1685Proposta_Codigo = Z1685Proposta_Codigo;
                  n1685Proposta_Codigo = false;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update48187( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert48187( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert48187( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow187( bcProposta) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars187( bcProposta, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey48187( ) ;
         if ( RcdFound187 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
            {
               A1685Proposta_Codigo = Z1685Proposta_Codigo;
               n1685Proposta_Codigo = false;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1685Proposta_Codigo != Z1685Proposta_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
         context.RollbackDataStores( "Proposta_BC");
         VarsToRow187( bcProposta) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcProposta.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcProposta.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcProposta )
         {
            bcProposta = (SdtProposta)(sdt);
            if ( StringUtil.StrCmp(bcProposta.gxTpr_Mode, "") == 0 )
            {
               bcProposta.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow187( bcProposta) ;
            }
            else
            {
               RowToVars187( bcProposta, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcProposta.gxTpr_Mode, "") == 0 )
            {
               bcProposta.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars187( bcProposta, 1) ;
         return  ;
      }

      public SdtProposta Proposta_BC
      {
         get {
            return bcProposta ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         A1687Proposta_Vigencia = (DateTime)(DateTime.MinValue);
         Z1689Proposta_Status = "";
         A1689Proposta_Status = "";
         Z1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         A1694Proposta_RestricaoImplantacao = (DateTime)(DateTime.MinValue);
         Z1700Proposta_ContratadaRazSocial = "";
         A1700Proposta_ContratadaRazSocial = "";
         Z1702Proposta_Prazo = DateTime.MinValue;
         A1702Proposta_Prazo = DateTime.MinValue;
         Z1719Proposta_OSDataCnt = DateTime.MinValue;
         A1719Proposta_OSDataCnt = DateTime.MinValue;
         Z1720Proposta_OSHoraCnt = "";
         A1720Proposta_OSHoraCnt = "";
         Z1764Proposta_OSDmn = "";
         A1764Proposta_OSDmn = "";
         Z1711Proposta_OSDmnFM = "";
         A1711Proposta_OSDmnFM = "";
         Z1792Proposta_OSInicio = DateTime.MinValue;
         A1792Proposta_OSInicio = DateTime.MinValue;
         Z1707Proposta_OSPrzTpDias = "";
         A1707Proposta_OSPrzTpDias = "";
         Z1763Proposta_OSCntSrvFtrm = "";
         A1763Proposta_OSCntSrvFtrm = "";
         Z1710Proposta_OSUndCntSgl = "";
         A1710Proposta_OSUndCntSgl = "";
         Z1690Proposta_Objetivo = "";
         A1690Proposta_Objetivo = "";
         Z1691Proposta_Escopo = "";
         A1691Proposta_Escopo = "";
         Z1692Proposta_Oportunidade = "";
         A1692Proposta_Oportunidade = "";
         Z1693Proposta_Perspectiva = "";
         A1693Proposta_Perspectiva = "";
         BC004821_A1685Proposta_Codigo = new int[1] ;
         BC004821_n1685Proposta_Codigo = new bool[] {false} ;
         BC004821_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         BC004821_n1687Proposta_Vigencia = new bool[] {false} ;
         BC004821_A1688Proposta_Valor = new decimal[1] ;
         BC004821_A1689Proposta_Status = new String[] {""} ;
         BC004821_n1689Proposta_Status = new bool[] {false} ;
         BC004821_A1690Proposta_Objetivo = new String[] {""} ;
         BC004821_A1691Proposta_Escopo = new String[] {""} ;
         BC004821_n1691Proposta_Escopo = new bool[] {false} ;
         BC004821_A1692Proposta_Oportunidade = new String[] {""} ;
         BC004821_n1692Proposta_Oportunidade = new bool[] {false} ;
         BC004821_A1693Proposta_Perspectiva = new String[] {""} ;
         BC004821_n1693Proposta_Perspectiva = new bool[] {false} ;
         BC004821_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         BC004821_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         BC004821_A1695Proposta_RestricaoCusto = new decimal[1] ;
         BC004821_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         BC004821_A1696Proposta_Ativo = new bool[] {false} ;
         BC004821_A1699Proposta_ContratadaCod = new int[1] ;
         BC004821_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         BC004821_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         BC004821_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC004821_A1793Proposta_PrazoDias = new short[1] ;
         BC004821_n1793Proposta_PrazoDias = new bool[] {false} ;
         BC004821_A1703Proposta_Esforco = new int[1] ;
         BC004821_A1794Proposta_Liquido = new int[1] ;
         BC004821_n1794Proposta_Liquido = new bool[] {false} ;
         BC004821_A1704Proposta_CntSrvCod = new int[1] ;
         BC004821_A1722Proposta_ValorUndCnt = new decimal[1] ;
         BC004821_A1764Proposta_OSDmn = new String[] {""} ;
         BC004821_n1764Proposta_OSDmn = new bool[] {false} ;
         BC004821_A1711Proposta_OSDmnFM = new String[] {""} ;
         BC004821_n1711Proposta_OSDmnFM = new bool[] {false} ;
         BC004821_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         BC004821_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         BC004821_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         BC004821_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         BC004821_A1721Proposta_OSPrzRsp = new short[1] ;
         BC004821_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         BC004821_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         BC004821_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         BC004821_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         BC004821_n1792Proposta_OSInicio = new bool[] {false} ;
         BC004821_A648Projeto_Codigo = new int[1] ;
         BC004821_n648Projeto_Codigo = new bool[] {false} ;
         BC004821_A5AreaTrabalho_Codigo = new int[1] ;
         BC004821_A1686Proposta_OSCodigo = new int[1] ;
         BC004821_A1705Proposta_OSServico = new int[1] ;
         BC004821_n1705Proposta_OSServico = new bool[] {false} ;
         BC004821_A1715Proposta_OSCntCod = new int[1] ;
         BC004821_n1715Proposta_OSCntCod = new bool[] {false} ;
         BC004821_A1713Proposta_OSUndCntCod = new int[1] ;
         BC004821_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         BC004821_A1717Proposta_OSPFB = new decimal[1] ;
         BC004821_n1717Proposta_OSPFB = new bool[] {false} ;
         BC004821_A1718Proposta_OSPFL = new decimal[1] ;
         BC004821_n1718Proposta_OSPFL = new bool[] {false} ;
         BC004821_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         BC004821_n1719Proposta_OSDataCnt = new bool[] {false} ;
         BC004821_A1720Proposta_OSHoraCnt = new String[] {""} ;
         BC004821_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         BC00484_A648Projeto_Codigo = new int[1] ;
         BC00484_n648Projeto_Codigo = new bool[] {false} ;
         BC00485_A5AreaTrabalho_Codigo = new int[1] ;
         BC00486_A1764Proposta_OSDmn = new String[] {""} ;
         BC00486_n1764Proposta_OSDmn = new bool[] {false} ;
         BC00486_A1711Proposta_OSDmnFM = new String[] {""} ;
         BC00486_n1711Proposta_OSDmnFM = new bool[] {false} ;
         BC00486_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         BC00486_n1792Proposta_OSInicio = new bool[] {false} ;
         BC00486_A1705Proposta_OSServico = new int[1] ;
         BC00486_n1705Proposta_OSServico = new bool[] {false} ;
         BC00487_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         BC00487_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         BC00487_A1721Proposta_OSPrzRsp = new short[1] ;
         BC00487_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         BC00487_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         BC00487_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         BC00487_A1715Proposta_OSCntCod = new int[1] ;
         BC00487_n1715Proposta_OSCntCod = new bool[] {false} ;
         BC00487_A1713Proposta_OSUndCntCod = new int[1] ;
         BC00487_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         BC00488_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         BC00488_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         BC004810_A1717Proposta_OSPFB = new decimal[1] ;
         BC004810_n1717Proposta_OSPFB = new bool[] {false} ;
         BC004812_A1718Proposta_OSPFL = new decimal[1] ;
         BC004812_n1718Proposta_OSPFL = new bool[] {false} ;
         BC004814_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         BC004814_n1719Proposta_OSDataCnt = new bool[] {false} ;
         BC004816_A1720Proposta_OSHoraCnt = new String[] {""} ;
         BC004816_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         BC004822_A1685Proposta_Codigo = new int[1] ;
         BC004822_n1685Proposta_Codigo = new bool[] {false} ;
         BC00483_A1685Proposta_Codigo = new int[1] ;
         BC00483_n1685Proposta_Codigo = new bool[] {false} ;
         BC00483_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         BC00483_n1687Proposta_Vigencia = new bool[] {false} ;
         BC00483_A1688Proposta_Valor = new decimal[1] ;
         BC00483_A1689Proposta_Status = new String[] {""} ;
         BC00483_n1689Proposta_Status = new bool[] {false} ;
         BC00483_A1690Proposta_Objetivo = new String[] {""} ;
         BC00483_A1691Proposta_Escopo = new String[] {""} ;
         BC00483_n1691Proposta_Escopo = new bool[] {false} ;
         BC00483_A1692Proposta_Oportunidade = new String[] {""} ;
         BC00483_n1692Proposta_Oportunidade = new bool[] {false} ;
         BC00483_A1693Proposta_Perspectiva = new String[] {""} ;
         BC00483_n1693Proposta_Perspectiva = new bool[] {false} ;
         BC00483_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         BC00483_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         BC00483_A1695Proposta_RestricaoCusto = new decimal[1] ;
         BC00483_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         BC00483_A1696Proposta_Ativo = new bool[] {false} ;
         BC00483_A1699Proposta_ContratadaCod = new int[1] ;
         BC00483_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         BC00483_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         BC00483_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC00483_A1793Proposta_PrazoDias = new short[1] ;
         BC00483_n1793Proposta_PrazoDias = new bool[] {false} ;
         BC00483_A1703Proposta_Esforco = new int[1] ;
         BC00483_A1794Proposta_Liquido = new int[1] ;
         BC00483_n1794Proposta_Liquido = new bool[] {false} ;
         BC00483_A1704Proposta_CntSrvCod = new int[1] ;
         BC00483_A1722Proposta_ValorUndCnt = new decimal[1] ;
         BC00483_A648Projeto_Codigo = new int[1] ;
         BC00483_n648Projeto_Codigo = new bool[] {false} ;
         BC00483_A5AreaTrabalho_Codigo = new int[1] ;
         BC00483_A1686Proposta_OSCodigo = new int[1] ;
         sMode187 = "";
         BC00482_A1685Proposta_Codigo = new int[1] ;
         BC00482_n1685Proposta_Codigo = new bool[] {false} ;
         BC00482_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         BC00482_n1687Proposta_Vigencia = new bool[] {false} ;
         BC00482_A1688Proposta_Valor = new decimal[1] ;
         BC00482_A1689Proposta_Status = new String[] {""} ;
         BC00482_n1689Proposta_Status = new bool[] {false} ;
         BC00482_A1690Proposta_Objetivo = new String[] {""} ;
         BC00482_A1691Proposta_Escopo = new String[] {""} ;
         BC00482_n1691Proposta_Escopo = new bool[] {false} ;
         BC00482_A1692Proposta_Oportunidade = new String[] {""} ;
         BC00482_n1692Proposta_Oportunidade = new bool[] {false} ;
         BC00482_A1693Proposta_Perspectiva = new String[] {""} ;
         BC00482_n1693Proposta_Perspectiva = new bool[] {false} ;
         BC00482_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         BC00482_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         BC00482_A1695Proposta_RestricaoCusto = new decimal[1] ;
         BC00482_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         BC00482_A1696Proposta_Ativo = new bool[] {false} ;
         BC00482_A1699Proposta_ContratadaCod = new int[1] ;
         BC00482_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         BC00482_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         BC00482_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC00482_A1793Proposta_PrazoDias = new short[1] ;
         BC00482_n1793Proposta_PrazoDias = new bool[] {false} ;
         BC00482_A1703Proposta_Esforco = new int[1] ;
         BC00482_A1794Proposta_Liquido = new int[1] ;
         BC00482_n1794Proposta_Liquido = new bool[] {false} ;
         BC00482_A1704Proposta_CntSrvCod = new int[1] ;
         BC00482_A1722Proposta_ValorUndCnt = new decimal[1] ;
         BC00482_A648Projeto_Codigo = new int[1] ;
         BC00482_n648Projeto_Codigo = new bool[] {false} ;
         BC00482_A5AreaTrabalho_Codigo = new int[1] ;
         BC00482_A1686Proposta_OSCodigo = new int[1] ;
         BC004823_A1685Proposta_Codigo = new int[1] ;
         BC004823_n1685Proposta_Codigo = new bool[] {false} ;
         BC004826_A1764Proposta_OSDmn = new String[] {""} ;
         BC004826_n1764Proposta_OSDmn = new bool[] {false} ;
         BC004826_A1711Proposta_OSDmnFM = new String[] {""} ;
         BC004826_n1711Proposta_OSDmnFM = new bool[] {false} ;
         BC004826_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         BC004826_n1792Proposta_OSInicio = new bool[] {false} ;
         BC004826_A1705Proposta_OSServico = new int[1] ;
         BC004826_n1705Proposta_OSServico = new bool[] {false} ;
         BC004827_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         BC004827_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         BC004827_A1721Proposta_OSPrzRsp = new short[1] ;
         BC004827_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         BC004827_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         BC004827_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         BC004827_A1715Proposta_OSCntCod = new int[1] ;
         BC004827_n1715Proposta_OSCntCod = new bool[] {false} ;
         BC004827_A1713Proposta_OSUndCntCod = new int[1] ;
         BC004827_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         BC004828_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         BC004828_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         BC004830_A1717Proposta_OSPFB = new decimal[1] ;
         BC004830_n1717Proposta_OSPFB = new bool[] {false} ;
         BC004832_A1718Proposta_OSPFL = new decimal[1] ;
         BC004832_n1718Proposta_OSPFL = new bool[] {false} ;
         BC004834_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         BC004834_n1719Proposta_OSDataCnt = new bool[] {false} ;
         BC004836_A1720Proposta_OSHoraCnt = new String[] {""} ;
         BC004836_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         BC004837_A1919Requisito_Codigo = new int[1] ;
         BC004842_A1685Proposta_Codigo = new int[1] ;
         BC004842_n1685Proposta_Codigo = new bool[] {false} ;
         BC004842_A1687Proposta_Vigencia = new DateTime[] {DateTime.MinValue} ;
         BC004842_n1687Proposta_Vigencia = new bool[] {false} ;
         BC004842_A1688Proposta_Valor = new decimal[1] ;
         BC004842_A1689Proposta_Status = new String[] {""} ;
         BC004842_n1689Proposta_Status = new bool[] {false} ;
         BC004842_A1690Proposta_Objetivo = new String[] {""} ;
         BC004842_A1691Proposta_Escopo = new String[] {""} ;
         BC004842_n1691Proposta_Escopo = new bool[] {false} ;
         BC004842_A1692Proposta_Oportunidade = new String[] {""} ;
         BC004842_n1692Proposta_Oportunidade = new bool[] {false} ;
         BC004842_A1693Proposta_Perspectiva = new String[] {""} ;
         BC004842_n1693Proposta_Perspectiva = new bool[] {false} ;
         BC004842_A1694Proposta_RestricaoImplantacao = new DateTime[] {DateTime.MinValue} ;
         BC004842_n1694Proposta_RestricaoImplantacao = new bool[] {false} ;
         BC004842_A1695Proposta_RestricaoCusto = new decimal[1] ;
         BC004842_n1695Proposta_RestricaoCusto = new bool[] {false} ;
         BC004842_A1696Proposta_Ativo = new bool[] {false} ;
         BC004842_A1699Proposta_ContratadaCod = new int[1] ;
         BC004842_A1700Proposta_ContratadaRazSocial = new String[] {""} ;
         BC004842_n1700Proposta_ContratadaRazSocial = new bool[] {false} ;
         BC004842_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         BC004842_A1793Proposta_PrazoDias = new short[1] ;
         BC004842_n1793Proposta_PrazoDias = new bool[] {false} ;
         BC004842_A1703Proposta_Esforco = new int[1] ;
         BC004842_A1794Proposta_Liquido = new int[1] ;
         BC004842_n1794Proposta_Liquido = new bool[] {false} ;
         BC004842_A1704Proposta_CntSrvCod = new int[1] ;
         BC004842_A1722Proposta_ValorUndCnt = new decimal[1] ;
         BC004842_A1764Proposta_OSDmn = new String[] {""} ;
         BC004842_n1764Proposta_OSDmn = new bool[] {false} ;
         BC004842_A1711Proposta_OSDmnFM = new String[] {""} ;
         BC004842_n1711Proposta_OSDmnFM = new bool[] {false} ;
         BC004842_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         BC004842_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         BC004842_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         BC004842_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         BC004842_A1721Proposta_OSPrzRsp = new short[1] ;
         BC004842_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         BC004842_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         BC004842_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         BC004842_A1792Proposta_OSInicio = new DateTime[] {DateTime.MinValue} ;
         BC004842_n1792Proposta_OSInicio = new bool[] {false} ;
         BC004842_A648Projeto_Codigo = new int[1] ;
         BC004842_n648Projeto_Codigo = new bool[] {false} ;
         BC004842_A5AreaTrabalho_Codigo = new int[1] ;
         BC004842_A1686Proposta_OSCodigo = new int[1] ;
         BC004842_A1705Proposta_OSServico = new int[1] ;
         BC004842_n1705Proposta_OSServico = new bool[] {false} ;
         BC004842_A1715Proposta_OSCntCod = new int[1] ;
         BC004842_n1715Proposta_OSCntCod = new bool[] {false} ;
         BC004842_A1713Proposta_OSUndCntCod = new int[1] ;
         BC004842_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         BC004842_A1717Proposta_OSPFB = new decimal[1] ;
         BC004842_n1717Proposta_OSPFB = new bool[] {false} ;
         BC004842_A1718Proposta_OSPFL = new decimal[1] ;
         BC004842_n1718Proposta_OSPFL = new bool[] {false} ;
         BC004842_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         BC004842_n1719Proposta_OSDataCnt = new bool[] {false} ;
         BC004842_A1720Proposta_OSHoraCnt = new String[] {""} ;
         BC004842_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.proposta_bc__default(),
            new Object[][] {
                new Object[] {
               BC00482_A1685Proposta_Codigo, BC00482_A1687Proposta_Vigencia, BC00482_n1687Proposta_Vigencia, BC00482_A1688Proposta_Valor, BC00482_A1689Proposta_Status, BC00482_n1689Proposta_Status, BC00482_A1690Proposta_Objetivo, BC00482_A1691Proposta_Escopo, BC00482_n1691Proposta_Escopo, BC00482_A1692Proposta_Oportunidade,
               BC00482_n1692Proposta_Oportunidade, BC00482_A1693Proposta_Perspectiva, BC00482_n1693Proposta_Perspectiva, BC00482_A1694Proposta_RestricaoImplantacao, BC00482_n1694Proposta_RestricaoImplantacao, BC00482_A1695Proposta_RestricaoCusto, BC00482_n1695Proposta_RestricaoCusto, BC00482_A1696Proposta_Ativo, BC00482_A1699Proposta_ContratadaCod, BC00482_A1700Proposta_ContratadaRazSocial,
               BC00482_n1700Proposta_ContratadaRazSocial, BC00482_A1702Proposta_Prazo, BC00482_A1793Proposta_PrazoDias, BC00482_n1793Proposta_PrazoDias, BC00482_A1703Proposta_Esforco, BC00482_A1794Proposta_Liquido, BC00482_n1794Proposta_Liquido, BC00482_A1704Proposta_CntSrvCod, BC00482_A1722Proposta_ValorUndCnt, BC00482_A648Projeto_Codigo,
               BC00482_n648Projeto_Codigo, BC00482_A5AreaTrabalho_Codigo, BC00482_A1686Proposta_OSCodigo
               }
               , new Object[] {
               BC00483_A1685Proposta_Codigo, BC00483_A1687Proposta_Vigencia, BC00483_n1687Proposta_Vigencia, BC00483_A1688Proposta_Valor, BC00483_A1689Proposta_Status, BC00483_n1689Proposta_Status, BC00483_A1690Proposta_Objetivo, BC00483_A1691Proposta_Escopo, BC00483_n1691Proposta_Escopo, BC00483_A1692Proposta_Oportunidade,
               BC00483_n1692Proposta_Oportunidade, BC00483_A1693Proposta_Perspectiva, BC00483_n1693Proposta_Perspectiva, BC00483_A1694Proposta_RestricaoImplantacao, BC00483_n1694Proposta_RestricaoImplantacao, BC00483_A1695Proposta_RestricaoCusto, BC00483_n1695Proposta_RestricaoCusto, BC00483_A1696Proposta_Ativo, BC00483_A1699Proposta_ContratadaCod, BC00483_A1700Proposta_ContratadaRazSocial,
               BC00483_n1700Proposta_ContratadaRazSocial, BC00483_A1702Proposta_Prazo, BC00483_A1793Proposta_PrazoDias, BC00483_n1793Proposta_PrazoDias, BC00483_A1703Proposta_Esforco, BC00483_A1794Proposta_Liquido, BC00483_n1794Proposta_Liquido, BC00483_A1704Proposta_CntSrvCod, BC00483_A1722Proposta_ValorUndCnt, BC00483_A648Projeto_Codigo,
               BC00483_n648Projeto_Codigo, BC00483_A5AreaTrabalho_Codigo, BC00483_A1686Proposta_OSCodigo
               }
               , new Object[] {
               BC00484_A648Projeto_Codigo
               }
               , new Object[] {
               BC00485_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               BC00486_A1764Proposta_OSDmn, BC00486_n1764Proposta_OSDmn, BC00486_A1711Proposta_OSDmnFM, BC00486_n1711Proposta_OSDmnFM, BC00486_A1792Proposta_OSInicio, BC00486_n1792Proposta_OSInicio, BC00486_A1705Proposta_OSServico, BC00486_n1705Proposta_OSServico
               }
               , new Object[] {
               BC00487_A1707Proposta_OSPrzTpDias, BC00487_n1707Proposta_OSPrzTpDias, BC00487_A1721Proposta_OSPrzRsp, BC00487_n1721Proposta_OSPrzRsp, BC00487_A1763Proposta_OSCntSrvFtrm, BC00487_n1763Proposta_OSCntSrvFtrm, BC00487_A1715Proposta_OSCntCod, BC00487_n1715Proposta_OSCntCod, BC00487_A1713Proposta_OSUndCntCod, BC00487_n1713Proposta_OSUndCntCod
               }
               , new Object[] {
               BC00488_A1710Proposta_OSUndCntSgl, BC00488_n1710Proposta_OSUndCntSgl
               }
               , new Object[] {
               BC004810_A1717Proposta_OSPFB, BC004810_n1717Proposta_OSPFB
               }
               , new Object[] {
               BC004812_A1718Proposta_OSPFL, BC004812_n1718Proposta_OSPFL
               }
               , new Object[] {
               BC004814_A1719Proposta_OSDataCnt, BC004814_n1719Proposta_OSDataCnt
               }
               , new Object[] {
               BC004816_A1720Proposta_OSHoraCnt, BC004816_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               BC004821_A1685Proposta_Codigo, BC004821_A1687Proposta_Vigencia, BC004821_n1687Proposta_Vigencia, BC004821_A1688Proposta_Valor, BC004821_A1689Proposta_Status, BC004821_n1689Proposta_Status, BC004821_A1690Proposta_Objetivo, BC004821_A1691Proposta_Escopo, BC004821_n1691Proposta_Escopo, BC004821_A1692Proposta_Oportunidade,
               BC004821_n1692Proposta_Oportunidade, BC004821_A1693Proposta_Perspectiva, BC004821_n1693Proposta_Perspectiva, BC004821_A1694Proposta_RestricaoImplantacao, BC004821_n1694Proposta_RestricaoImplantacao, BC004821_A1695Proposta_RestricaoCusto, BC004821_n1695Proposta_RestricaoCusto, BC004821_A1696Proposta_Ativo, BC004821_A1699Proposta_ContratadaCod, BC004821_A1700Proposta_ContratadaRazSocial,
               BC004821_n1700Proposta_ContratadaRazSocial, BC004821_A1702Proposta_Prazo, BC004821_A1793Proposta_PrazoDias, BC004821_n1793Proposta_PrazoDias, BC004821_A1703Proposta_Esforco, BC004821_A1794Proposta_Liquido, BC004821_n1794Proposta_Liquido, BC004821_A1704Proposta_CntSrvCod, BC004821_A1722Proposta_ValorUndCnt, BC004821_A1764Proposta_OSDmn,
               BC004821_n1764Proposta_OSDmn, BC004821_A1711Proposta_OSDmnFM, BC004821_n1711Proposta_OSDmnFM, BC004821_A1710Proposta_OSUndCntSgl, BC004821_n1710Proposta_OSUndCntSgl, BC004821_A1707Proposta_OSPrzTpDias, BC004821_n1707Proposta_OSPrzTpDias, BC004821_A1721Proposta_OSPrzRsp, BC004821_n1721Proposta_OSPrzRsp, BC004821_A1763Proposta_OSCntSrvFtrm,
               BC004821_n1763Proposta_OSCntSrvFtrm, BC004821_A1792Proposta_OSInicio, BC004821_n1792Proposta_OSInicio, BC004821_A648Projeto_Codigo, BC004821_n648Projeto_Codigo, BC004821_A5AreaTrabalho_Codigo, BC004821_A1686Proposta_OSCodigo, BC004821_A1705Proposta_OSServico, BC004821_n1705Proposta_OSServico, BC004821_A1715Proposta_OSCntCod,
               BC004821_n1715Proposta_OSCntCod, BC004821_A1713Proposta_OSUndCntCod, BC004821_n1713Proposta_OSUndCntCod, BC004821_A1717Proposta_OSPFB, BC004821_n1717Proposta_OSPFB, BC004821_A1718Proposta_OSPFL, BC004821_n1718Proposta_OSPFL, BC004821_A1719Proposta_OSDataCnt, BC004821_n1719Proposta_OSDataCnt, BC004821_A1720Proposta_OSHoraCnt,
               BC004821_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               BC004822_A1685Proposta_Codigo
               }
               , new Object[] {
               BC004823_A1685Proposta_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004826_A1764Proposta_OSDmn, BC004826_n1764Proposta_OSDmn, BC004826_A1711Proposta_OSDmnFM, BC004826_n1711Proposta_OSDmnFM, BC004826_A1792Proposta_OSInicio, BC004826_n1792Proposta_OSInicio, BC004826_A1705Proposta_OSServico, BC004826_n1705Proposta_OSServico
               }
               , new Object[] {
               BC004827_A1707Proposta_OSPrzTpDias, BC004827_n1707Proposta_OSPrzTpDias, BC004827_A1721Proposta_OSPrzRsp, BC004827_n1721Proposta_OSPrzRsp, BC004827_A1763Proposta_OSCntSrvFtrm, BC004827_n1763Proposta_OSCntSrvFtrm, BC004827_A1715Proposta_OSCntCod, BC004827_n1715Proposta_OSCntCod, BC004827_A1713Proposta_OSUndCntCod, BC004827_n1713Proposta_OSUndCntCod
               }
               , new Object[] {
               BC004828_A1710Proposta_OSUndCntSgl, BC004828_n1710Proposta_OSUndCntSgl
               }
               , new Object[] {
               BC004830_A1717Proposta_OSPFB, BC004830_n1717Proposta_OSPFB
               }
               , new Object[] {
               BC004832_A1718Proposta_OSPFL, BC004832_n1718Proposta_OSPFL
               }
               , new Object[] {
               BC004834_A1719Proposta_OSDataCnt, BC004834_n1719Proposta_OSDataCnt
               }
               , new Object[] {
               BC004836_A1720Proposta_OSHoraCnt, BC004836_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               BC004837_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004842_A1685Proposta_Codigo, BC004842_A1687Proposta_Vigencia, BC004842_n1687Proposta_Vigencia, BC004842_A1688Proposta_Valor, BC004842_A1689Proposta_Status, BC004842_n1689Proposta_Status, BC004842_A1690Proposta_Objetivo, BC004842_A1691Proposta_Escopo, BC004842_n1691Proposta_Escopo, BC004842_A1692Proposta_Oportunidade,
               BC004842_n1692Proposta_Oportunidade, BC004842_A1693Proposta_Perspectiva, BC004842_n1693Proposta_Perspectiva, BC004842_A1694Proposta_RestricaoImplantacao, BC004842_n1694Proposta_RestricaoImplantacao, BC004842_A1695Proposta_RestricaoCusto, BC004842_n1695Proposta_RestricaoCusto, BC004842_A1696Proposta_Ativo, BC004842_A1699Proposta_ContratadaCod, BC004842_A1700Proposta_ContratadaRazSocial,
               BC004842_n1700Proposta_ContratadaRazSocial, BC004842_A1702Proposta_Prazo, BC004842_A1793Proposta_PrazoDias, BC004842_n1793Proposta_PrazoDias, BC004842_A1703Proposta_Esforco, BC004842_A1794Proposta_Liquido, BC004842_n1794Proposta_Liquido, BC004842_A1704Proposta_CntSrvCod, BC004842_A1722Proposta_ValorUndCnt, BC004842_A1764Proposta_OSDmn,
               BC004842_n1764Proposta_OSDmn, BC004842_A1711Proposta_OSDmnFM, BC004842_n1711Proposta_OSDmnFM, BC004842_A1710Proposta_OSUndCntSgl, BC004842_n1710Proposta_OSUndCntSgl, BC004842_A1707Proposta_OSPrzTpDias, BC004842_n1707Proposta_OSPrzTpDias, BC004842_A1721Proposta_OSPrzRsp, BC004842_n1721Proposta_OSPrzRsp, BC004842_A1763Proposta_OSCntSrvFtrm,
               BC004842_n1763Proposta_OSCntSrvFtrm, BC004842_A1792Proposta_OSInicio, BC004842_n1792Proposta_OSInicio, BC004842_A648Projeto_Codigo, BC004842_n648Projeto_Codigo, BC004842_A5AreaTrabalho_Codigo, BC004842_A1686Proposta_OSCodigo, BC004842_A1705Proposta_OSServico, BC004842_n1705Proposta_OSServico, BC004842_A1715Proposta_OSCntCod,
               BC004842_n1715Proposta_OSCntCod, BC004842_A1713Proposta_OSUndCntCod, BC004842_n1713Proposta_OSUndCntCod, BC004842_A1717Proposta_OSPFB, BC004842_n1717Proposta_OSPFB, BC004842_A1718Proposta_OSPFL, BC004842_n1718Proposta_OSPFL, BC004842_A1719Proposta_OSDataCnt, BC004842_n1719Proposta_OSDataCnt, BC004842_A1720Proposta_OSHoraCnt,
               BC004842_n1720Proposta_OSHoraCnt
               }
            }
         );
         AV14Pgmname = "Proposta_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12482 */
         E12482 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z1793Proposta_PrazoDias ;
      private short A1793Proposta_PrazoDias ;
      private short Z1721Proposta_OSPrzRsp ;
      private short A1721Proposta_OSPrzRsp ;
      private short RcdFound187 ;
      private int trnEnded ;
      private int Z1685Proposta_Codigo ;
      private int A1685Proposta_Codigo ;
      private int AV15GXV1 ;
      private int AV11Insert_AreaTrabalho_Codigo ;
      private int AV12Insert_Proposta_OSCodigo ;
      private int Z1699Proposta_ContratadaCod ;
      private int A1699Proposta_ContratadaCod ;
      private int Z1703Proposta_Esforco ;
      private int A1703Proposta_Esforco ;
      private int Z1794Proposta_Liquido ;
      private int A1794Proposta_Liquido ;
      private int Z1704Proposta_CntSrvCod ;
      private int A1704Proposta_CntSrvCod ;
      private int Z648Projeto_Codigo ;
      private int A648Projeto_Codigo ;
      private int Z5AreaTrabalho_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int Z1686Proposta_OSCodigo ;
      private int A1686Proposta_OSCodigo ;
      private int Z1705Proposta_OSServico ;
      private int A1705Proposta_OSServico ;
      private int Z1715Proposta_OSCntCod ;
      private int A1715Proposta_OSCntCod ;
      private int Z1713Proposta_OSUndCntCod ;
      private int A1713Proposta_OSUndCntCod ;
      private decimal Z1688Proposta_Valor ;
      private decimal A1688Proposta_Valor ;
      private decimal Z1695Proposta_RestricaoCusto ;
      private decimal A1695Proposta_RestricaoCusto ;
      private decimal Z1722Proposta_ValorUndCnt ;
      private decimal A1722Proposta_ValorUndCnt ;
      private decimal Z1717Proposta_OSPFB ;
      private decimal A1717Proposta_OSPFB ;
      private decimal Z1718Proposta_OSPFL ;
      private decimal A1718Proposta_OSPFL ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV14Pgmname ;
      private String Z1689Proposta_Status ;
      private String A1689Proposta_Status ;
      private String Z1720Proposta_OSHoraCnt ;
      private String A1720Proposta_OSHoraCnt ;
      private String Z1707Proposta_OSPrzTpDias ;
      private String A1707Proposta_OSPrzTpDias ;
      private String Z1710Proposta_OSUndCntSgl ;
      private String A1710Proposta_OSUndCntSgl ;
      private String sMode187 ;
      private DateTime Z1687Proposta_Vigencia ;
      private DateTime A1687Proposta_Vigencia ;
      private DateTime Z1694Proposta_RestricaoImplantacao ;
      private DateTime A1694Proposta_RestricaoImplantacao ;
      private DateTime Z1702Proposta_Prazo ;
      private DateTime A1702Proposta_Prazo ;
      private DateTime Z1719Proposta_OSDataCnt ;
      private DateTime A1719Proposta_OSDataCnt ;
      private DateTime Z1792Proposta_OSInicio ;
      private DateTime A1792Proposta_OSInicio ;
      private bool Z1696Proposta_Ativo ;
      private bool A1696Proposta_Ativo ;
      private bool n1685Proposta_Codigo ;
      private bool n1687Proposta_Vigencia ;
      private bool n1689Proposta_Status ;
      private bool n1691Proposta_Escopo ;
      private bool n1692Proposta_Oportunidade ;
      private bool n1693Proposta_Perspectiva ;
      private bool n1694Proposta_RestricaoImplantacao ;
      private bool n1695Proposta_RestricaoCusto ;
      private bool n1700Proposta_ContratadaRazSocial ;
      private bool n1793Proposta_PrazoDias ;
      private bool n1794Proposta_Liquido ;
      private bool n1764Proposta_OSDmn ;
      private bool n1711Proposta_OSDmnFM ;
      private bool n1710Proposta_OSUndCntSgl ;
      private bool n1707Proposta_OSPrzTpDias ;
      private bool n1721Proposta_OSPrzRsp ;
      private bool n1763Proposta_OSCntSrvFtrm ;
      private bool n1792Proposta_OSInicio ;
      private bool n648Projeto_Codigo ;
      private bool n1705Proposta_OSServico ;
      private bool n1715Proposta_OSCntCod ;
      private bool n1713Proposta_OSUndCntCod ;
      private bool n1717Proposta_OSPFB ;
      private bool n1718Proposta_OSPFL ;
      private bool n1719Proposta_OSDataCnt ;
      private bool n1720Proposta_OSHoraCnt ;
      private bool Gx_longc ;
      private String Z1690Proposta_Objetivo ;
      private String A1690Proposta_Objetivo ;
      private String Z1691Proposta_Escopo ;
      private String A1691Proposta_Escopo ;
      private String Z1692Proposta_Oportunidade ;
      private String A1692Proposta_Oportunidade ;
      private String Z1693Proposta_Perspectiva ;
      private String A1693Proposta_Perspectiva ;
      private String Z1700Proposta_ContratadaRazSocial ;
      private String A1700Proposta_ContratadaRazSocial ;
      private String Z1764Proposta_OSDmn ;
      private String A1764Proposta_OSDmn ;
      private String Z1711Proposta_OSDmnFM ;
      private String A1711Proposta_OSDmnFM ;
      private String Z1763Proposta_OSCntSrvFtrm ;
      private String A1763Proposta_OSCntSrvFtrm ;
      private IGxSession AV10WebSession ;
      private SdtProposta bcProposta ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004821_A1685Proposta_Codigo ;
      private bool[] BC004821_n1685Proposta_Codigo ;
      private DateTime[] BC004821_A1687Proposta_Vigencia ;
      private bool[] BC004821_n1687Proposta_Vigencia ;
      private decimal[] BC004821_A1688Proposta_Valor ;
      private String[] BC004821_A1689Proposta_Status ;
      private bool[] BC004821_n1689Proposta_Status ;
      private String[] BC004821_A1690Proposta_Objetivo ;
      private String[] BC004821_A1691Proposta_Escopo ;
      private bool[] BC004821_n1691Proposta_Escopo ;
      private String[] BC004821_A1692Proposta_Oportunidade ;
      private bool[] BC004821_n1692Proposta_Oportunidade ;
      private String[] BC004821_A1693Proposta_Perspectiva ;
      private bool[] BC004821_n1693Proposta_Perspectiva ;
      private DateTime[] BC004821_A1694Proposta_RestricaoImplantacao ;
      private bool[] BC004821_n1694Proposta_RestricaoImplantacao ;
      private decimal[] BC004821_A1695Proposta_RestricaoCusto ;
      private bool[] BC004821_n1695Proposta_RestricaoCusto ;
      private bool[] BC004821_A1696Proposta_Ativo ;
      private int[] BC004821_A1699Proposta_ContratadaCod ;
      private String[] BC004821_A1700Proposta_ContratadaRazSocial ;
      private bool[] BC004821_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] BC004821_A1702Proposta_Prazo ;
      private short[] BC004821_A1793Proposta_PrazoDias ;
      private bool[] BC004821_n1793Proposta_PrazoDias ;
      private int[] BC004821_A1703Proposta_Esforco ;
      private int[] BC004821_A1794Proposta_Liquido ;
      private bool[] BC004821_n1794Proposta_Liquido ;
      private int[] BC004821_A1704Proposta_CntSrvCod ;
      private decimal[] BC004821_A1722Proposta_ValorUndCnt ;
      private String[] BC004821_A1764Proposta_OSDmn ;
      private bool[] BC004821_n1764Proposta_OSDmn ;
      private String[] BC004821_A1711Proposta_OSDmnFM ;
      private bool[] BC004821_n1711Proposta_OSDmnFM ;
      private String[] BC004821_A1710Proposta_OSUndCntSgl ;
      private bool[] BC004821_n1710Proposta_OSUndCntSgl ;
      private String[] BC004821_A1707Proposta_OSPrzTpDias ;
      private bool[] BC004821_n1707Proposta_OSPrzTpDias ;
      private short[] BC004821_A1721Proposta_OSPrzRsp ;
      private bool[] BC004821_n1721Proposta_OSPrzRsp ;
      private String[] BC004821_A1763Proposta_OSCntSrvFtrm ;
      private bool[] BC004821_n1763Proposta_OSCntSrvFtrm ;
      private DateTime[] BC004821_A1792Proposta_OSInicio ;
      private bool[] BC004821_n1792Proposta_OSInicio ;
      private int[] BC004821_A648Projeto_Codigo ;
      private bool[] BC004821_n648Projeto_Codigo ;
      private int[] BC004821_A5AreaTrabalho_Codigo ;
      private int[] BC004821_A1686Proposta_OSCodigo ;
      private int[] BC004821_A1705Proposta_OSServico ;
      private bool[] BC004821_n1705Proposta_OSServico ;
      private int[] BC004821_A1715Proposta_OSCntCod ;
      private bool[] BC004821_n1715Proposta_OSCntCod ;
      private int[] BC004821_A1713Proposta_OSUndCntCod ;
      private bool[] BC004821_n1713Proposta_OSUndCntCod ;
      private decimal[] BC004821_A1717Proposta_OSPFB ;
      private bool[] BC004821_n1717Proposta_OSPFB ;
      private decimal[] BC004821_A1718Proposta_OSPFL ;
      private bool[] BC004821_n1718Proposta_OSPFL ;
      private DateTime[] BC004821_A1719Proposta_OSDataCnt ;
      private bool[] BC004821_n1719Proposta_OSDataCnt ;
      private String[] BC004821_A1720Proposta_OSHoraCnt ;
      private bool[] BC004821_n1720Proposta_OSHoraCnt ;
      private int[] BC00484_A648Projeto_Codigo ;
      private bool[] BC00484_n648Projeto_Codigo ;
      private int[] BC00485_A5AreaTrabalho_Codigo ;
      private String[] BC00486_A1764Proposta_OSDmn ;
      private bool[] BC00486_n1764Proposta_OSDmn ;
      private String[] BC00486_A1711Proposta_OSDmnFM ;
      private bool[] BC00486_n1711Proposta_OSDmnFM ;
      private DateTime[] BC00486_A1792Proposta_OSInicio ;
      private bool[] BC00486_n1792Proposta_OSInicio ;
      private int[] BC00486_A1705Proposta_OSServico ;
      private bool[] BC00486_n1705Proposta_OSServico ;
      private String[] BC00487_A1707Proposta_OSPrzTpDias ;
      private bool[] BC00487_n1707Proposta_OSPrzTpDias ;
      private short[] BC00487_A1721Proposta_OSPrzRsp ;
      private bool[] BC00487_n1721Proposta_OSPrzRsp ;
      private String[] BC00487_A1763Proposta_OSCntSrvFtrm ;
      private bool[] BC00487_n1763Proposta_OSCntSrvFtrm ;
      private int[] BC00487_A1715Proposta_OSCntCod ;
      private bool[] BC00487_n1715Proposta_OSCntCod ;
      private int[] BC00487_A1713Proposta_OSUndCntCod ;
      private bool[] BC00487_n1713Proposta_OSUndCntCod ;
      private String[] BC00488_A1710Proposta_OSUndCntSgl ;
      private bool[] BC00488_n1710Proposta_OSUndCntSgl ;
      private decimal[] BC004810_A1717Proposta_OSPFB ;
      private bool[] BC004810_n1717Proposta_OSPFB ;
      private decimal[] BC004812_A1718Proposta_OSPFL ;
      private bool[] BC004812_n1718Proposta_OSPFL ;
      private DateTime[] BC004814_A1719Proposta_OSDataCnt ;
      private bool[] BC004814_n1719Proposta_OSDataCnt ;
      private String[] BC004816_A1720Proposta_OSHoraCnt ;
      private bool[] BC004816_n1720Proposta_OSHoraCnt ;
      private int[] BC004822_A1685Proposta_Codigo ;
      private bool[] BC004822_n1685Proposta_Codigo ;
      private int[] BC00483_A1685Proposta_Codigo ;
      private bool[] BC00483_n1685Proposta_Codigo ;
      private DateTime[] BC00483_A1687Proposta_Vigencia ;
      private bool[] BC00483_n1687Proposta_Vigencia ;
      private decimal[] BC00483_A1688Proposta_Valor ;
      private String[] BC00483_A1689Proposta_Status ;
      private bool[] BC00483_n1689Proposta_Status ;
      private String[] BC00483_A1690Proposta_Objetivo ;
      private String[] BC00483_A1691Proposta_Escopo ;
      private bool[] BC00483_n1691Proposta_Escopo ;
      private String[] BC00483_A1692Proposta_Oportunidade ;
      private bool[] BC00483_n1692Proposta_Oportunidade ;
      private String[] BC00483_A1693Proposta_Perspectiva ;
      private bool[] BC00483_n1693Proposta_Perspectiva ;
      private DateTime[] BC00483_A1694Proposta_RestricaoImplantacao ;
      private bool[] BC00483_n1694Proposta_RestricaoImplantacao ;
      private decimal[] BC00483_A1695Proposta_RestricaoCusto ;
      private bool[] BC00483_n1695Proposta_RestricaoCusto ;
      private bool[] BC00483_A1696Proposta_Ativo ;
      private int[] BC00483_A1699Proposta_ContratadaCod ;
      private String[] BC00483_A1700Proposta_ContratadaRazSocial ;
      private bool[] BC00483_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] BC00483_A1702Proposta_Prazo ;
      private short[] BC00483_A1793Proposta_PrazoDias ;
      private bool[] BC00483_n1793Proposta_PrazoDias ;
      private int[] BC00483_A1703Proposta_Esforco ;
      private int[] BC00483_A1794Proposta_Liquido ;
      private bool[] BC00483_n1794Proposta_Liquido ;
      private int[] BC00483_A1704Proposta_CntSrvCod ;
      private decimal[] BC00483_A1722Proposta_ValorUndCnt ;
      private int[] BC00483_A648Projeto_Codigo ;
      private bool[] BC00483_n648Projeto_Codigo ;
      private int[] BC00483_A5AreaTrabalho_Codigo ;
      private int[] BC00483_A1686Proposta_OSCodigo ;
      private int[] BC00482_A1685Proposta_Codigo ;
      private bool[] BC00482_n1685Proposta_Codigo ;
      private DateTime[] BC00482_A1687Proposta_Vigencia ;
      private bool[] BC00482_n1687Proposta_Vigencia ;
      private decimal[] BC00482_A1688Proposta_Valor ;
      private String[] BC00482_A1689Proposta_Status ;
      private bool[] BC00482_n1689Proposta_Status ;
      private String[] BC00482_A1690Proposta_Objetivo ;
      private String[] BC00482_A1691Proposta_Escopo ;
      private bool[] BC00482_n1691Proposta_Escopo ;
      private String[] BC00482_A1692Proposta_Oportunidade ;
      private bool[] BC00482_n1692Proposta_Oportunidade ;
      private String[] BC00482_A1693Proposta_Perspectiva ;
      private bool[] BC00482_n1693Proposta_Perspectiva ;
      private DateTime[] BC00482_A1694Proposta_RestricaoImplantacao ;
      private bool[] BC00482_n1694Proposta_RestricaoImplantacao ;
      private decimal[] BC00482_A1695Proposta_RestricaoCusto ;
      private bool[] BC00482_n1695Proposta_RestricaoCusto ;
      private bool[] BC00482_A1696Proposta_Ativo ;
      private int[] BC00482_A1699Proposta_ContratadaCod ;
      private String[] BC00482_A1700Proposta_ContratadaRazSocial ;
      private bool[] BC00482_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] BC00482_A1702Proposta_Prazo ;
      private short[] BC00482_A1793Proposta_PrazoDias ;
      private bool[] BC00482_n1793Proposta_PrazoDias ;
      private int[] BC00482_A1703Proposta_Esforco ;
      private int[] BC00482_A1794Proposta_Liquido ;
      private bool[] BC00482_n1794Proposta_Liquido ;
      private int[] BC00482_A1704Proposta_CntSrvCod ;
      private decimal[] BC00482_A1722Proposta_ValorUndCnt ;
      private int[] BC00482_A648Projeto_Codigo ;
      private bool[] BC00482_n648Projeto_Codigo ;
      private int[] BC00482_A5AreaTrabalho_Codigo ;
      private int[] BC00482_A1686Proposta_OSCodigo ;
      private int[] BC004823_A1685Proposta_Codigo ;
      private bool[] BC004823_n1685Proposta_Codigo ;
      private String[] BC004826_A1764Proposta_OSDmn ;
      private bool[] BC004826_n1764Proposta_OSDmn ;
      private String[] BC004826_A1711Proposta_OSDmnFM ;
      private bool[] BC004826_n1711Proposta_OSDmnFM ;
      private DateTime[] BC004826_A1792Proposta_OSInicio ;
      private bool[] BC004826_n1792Proposta_OSInicio ;
      private int[] BC004826_A1705Proposta_OSServico ;
      private bool[] BC004826_n1705Proposta_OSServico ;
      private String[] BC004827_A1707Proposta_OSPrzTpDias ;
      private bool[] BC004827_n1707Proposta_OSPrzTpDias ;
      private short[] BC004827_A1721Proposta_OSPrzRsp ;
      private bool[] BC004827_n1721Proposta_OSPrzRsp ;
      private String[] BC004827_A1763Proposta_OSCntSrvFtrm ;
      private bool[] BC004827_n1763Proposta_OSCntSrvFtrm ;
      private int[] BC004827_A1715Proposta_OSCntCod ;
      private bool[] BC004827_n1715Proposta_OSCntCod ;
      private int[] BC004827_A1713Proposta_OSUndCntCod ;
      private bool[] BC004827_n1713Proposta_OSUndCntCod ;
      private String[] BC004828_A1710Proposta_OSUndCntSgl ;
      private bool[] BC004828_n1710Proposta_OSUndCntSgl ;
      private decimal[] BC004830_A1717Proposta_OSPFB ;
      private bool[] BC004830_n1717Proposta_OSPFB ;
      private decimal[] BC004832_A1718Proposta_OSPFL ;
      private bool[] BC004832_n1718Proposta_OSPFL ;
      private DateTime[] BC004834_A1719Proposta_OSDataCnt ;
      private bool[] BC004834_n1719Proposta_OSDataCnt ;
      private String[] BC004836_A1720Proposta_OSHoraCnt ;
      private bool[] BC004836_n1720Proposta_OSHoraCnt ;
      private int[] BC004837_A1919Requisito_Codigo ;
      private int[] BC004842_A1685Proposta_Codigo ;
      private bool[] BC004842_n1685Proposta_Codigo ;
      private DateTime[] BC004842_A1687Proposta_Vigencia ;
      private bool[] BC004842_n1687Proposta_Vigencia ;
      private decimal[] BC004842_A1688Proposta_Valor ;
      private String[] BC004842_A1689Proposta_Status ;
      private bool[] BC004842_n1689Proposta_Status ;
      private String[] BC004842_A1690Proposta_Objetivo ;
      private String[] BC004842_A1691Proposta_Escopo ;
      private bool[] BC004842_n1691Proposta_Escopo ;
      private String[] BC004842_A1692Proposta_Oportunidade ;
      private bool[] BC004842_n1692Proposta_Oportunidade ;
      private String[] BC004842_A1693Proposta_Perspectiva ;
      private bool[] BC004842_n1693Proposta_Perspectiva ;
      private DateTime[] BC004842_A1694Proposta_RestricaoImplantacao ;
      private bool[] BC004842_n1694Proposta_RestricaoImplantacao ;
      private decimal[] BC004842_A1695Proposta_RestricaoCusto ;
      private bool[] BC004842_n1695Proposta_RestricaoCusto ;
      private bool[] BC004842_A1696Proposta_Ativo ;
      private int[] BC004842_A1699Proposta_ContratadaCod ;
      private String[] BC004842_A1700Proposta_ContratadaRazSocial ;
      private bool[] BC004842_n1700Proposta_ContratadaRazSocial ;
      private DateTime[] BC004842_A1702Proposta_Prazo ;
      private short[] BC004842_A1793Proposta_PrazoDias ;
      private bool[] BC004842_n1793Proposta_PrazoDias ;
      private int[] BC004842_A1703Proposta_Esforco ;
      private int[] BC004842_A1794Proposta_Liquido ;
      private bool[] BC004842_n1794Proposta_Liquido ;
      private int[] BC004842_A1704Proposta_CntSrvCod ;
      private decimal[] BC004842_A1722Proposta_ValorUndCnt ;
      private String[] BC004842_A1764Proposta_OSDmn ;
      private bool[] BC004842_n1764Proposta_OSDmn ;
      private String[] BC004842_A1711Proposta_OSDmnFM ;
      private bool[] BC004842_n1711Proposta_OSDmnFM ;
      private String[] BC004842_A1710Proposta_OSUndCntSgl ;
      private bool[] BC004842_n1710Proposta_OSUndCntSgl ;
      private String[] BC004842_A1707Proposta_OSPrzTpDias ;
      private bool[] BC004842_n1707Proposta_OSPrzTpDias ;
      private short[] BC004842_A1721Proposta_OSPrzRsp ;
      private bool[] BC004842_n1721Proposta_OSPrzRsp ;
      private String[] BC004842_A1763Proposta_OSCntSrvFtrm ;
      private bool[] BC004842_n1763Proposta_OSCntSrvFtrm ;
      private DateTime[] BC004842_A1792Proposta_OSInicio ;
      private bool[] BC004842_n1792Proposta_OSInicio ;
      private int[] BC004842_A648Projeto_Codigo ;
      private bool[] BC004842_n648Projeto_Codigo ;
      private int[] BC004842_A5AreaTrabalho_Codigo ;
      private int[] BC004842_A1686Proposta_OSCodigo ;
      private int[] BC004842_A1705Proposta_OSServico ;
      private bool[] BC004842_n1705Proposta_OSServico ;
      private int[] BC004842_A1715Proposta_OSCntCod ;
      private bool[] BC004842_n1715Proposta_OSCntCod ;
      private int[] BC004842_A1713Proposta_OSUndCntCod ;
      private bool[] BC004842_n1713Proposta_OSUndCntCod ;
      private decimal[] BC004842_A1717Proposta_OSPFB ;
      private bool[] BC004842_n1717Proposta_OSPFB ;
      private decimal[] BC004842_A1718Proposta_OSPFL ;
      private bool[] BC004842_n1718Proposta_OSPFL ;
      private DateTime[] BC004842_A1719Proposta_OSDataCnt ;
      private bool[] BC004842_n1719Proposta_OSDataCnt ;
      private String[] BC004842_A1720Proposta_OSHoraCnt ;
      private bool[] BC004842_n1720Proposta_OSHoraCnt ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class proposta_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004821 ;
          prmBC004821 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          String cmdBufferBC004821 ;
          cmdBufferBC004821=" SELECT TM1.[Proposta_Codigo], TM1.[Proposta_Vigencia], TM1.[Proposta_Valor], TM1.[Proposta_Status], TM1.[Proposta_Objetivo], TM1.[Proposta_Escopo], TM1.[Proposta_Oportunidade], TM1.[Proposta_Perspectiva], TM1.[Proposta_RestricaoImplantacao], TM1.[Proposta_RestricaoCusto], TM1.[Proposta_Ativo], TM1.[Proposta_PrestatoraCod], TM1.[Proposta_PrestadoraRazSocial], TM1.[Proposta_Prazo], TM1.[Proposta_PrazoDias], TM1.[Proposta_Esforco], TM1.[Proposta_Liquido], TM1.[Proposta_CntSrvCod], TM1.[Proposta_ValorUndCnt], T2.[ContagemResultado_Demanda] AS Proposta_OSDmn, T2.[ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, T4.[UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl, T3.[ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, T3.[ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, T3.[ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, T2.[ContagemResultado_DataInicio] AS Proposta_OSInicio, TM1.[Projeto_Codigo], TM1.[AreaTrabalho_Codigo], TM1.[Proposta_OSCodigo] AS Proposta_OSCodigo, T2.[ContagemResultado_CntSrvCod] AS Proposta_OSServico, T3.[Contrato_Codigo] AS Proposta_OSCntCod, T3.[ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod, COALESCE( T5.[Proposta_OSPFB], 0) AS Proposta_OSPFB, COALESCE( T6.[Proposta_OSPFL], 0) AS Proposta_OSPFL, COALESCE( T7.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt, COALESCE( T8.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (((((((dbo.[Proposta] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T4 WITH (NOLOCK) ON T4.[UnidadeMedicao_Codigo] = T3.[ContratoServicos_UnidadeContratada]) "
          + " LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) WHERE TM1.[Proposta_Codigo] = @Proposta_Codigo ORDER BY TM1.[Proposta_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC00484 ;
          prmBC00484 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00485 ;
          prmBC00485 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00486 ;
          prmBC00486 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00487 ;
          prmBC00487 = new Object[] {
          new Object[] {"@Proposta_OSServico",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00488 ;
          prmBC00488 = new Object[] {
          new Object[] {"@Proposta_OSUndCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004810 ;
          prmBC004810 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004812 ;
          prmBC004812 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004814 ;
          prmBC004814 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004816 ;
          prmBC004816 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004822 ;
          prmBC004822 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC00483 ;
          prmBC00483 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC00482 ;
          prmBC00482 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC004823 ;
          prmBC004823 = new Object[] {
          new Object[] {"@Proposta_Vigencia",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_Valor",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@Proposta_Objetivo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Escopo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Oportunidade",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Perspectiva",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_RestricaoImplantacao",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_RestricaoCusto",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Ativo",SqlDbType.Bit,1,0} ,
          new Object[] {"@Proposta_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ContratadaRazSocial",SqlDbType.VarChar,120,0} ,
          new Object[] {"@Proposta_Prazo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Proposta_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Proposta_Esforco",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_Liquido",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ValorUndCnt",SqlDbType.Decimal,10,2} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004824 ;
          prmBC004824 = new Object[] {
          new Object[] {"@Proposta_Vigencia",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_Valor",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Status",SqlDbType.Char,3,0} ,
          new Object[] {"@Proposta_Objetivo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Escopo",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Oportunidade",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_Perspectiva",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Proposta_RestricaoImplantacao",SqlDbType.DateTime,10,8} ,
          new Object[] {"@Proposta_RestricaoCusto",SqlDbType.Decimal,15,4} ,
          new Object[] {"@Proposta_Ativo",SqlDbType.Bit,1,0} ,
          new Object[] {"@Proposta_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ContratadaRazSocial",SqlDbType.VarChar,120,0} ,
          new Object[] {"@Proposta_Prazo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Proposta_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Proposta_Esforco",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_Liquido",SqlDbType.Int,8,0} ,
          new Object[] {"@Proposta_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_ValorUndCnt",SqlDbType.Decimal,10,2} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC004825 ;
          prmBC004825 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC004826 ;
          prmBC004826 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004827 ;
          prmBC004827 = new Object[] {
          new Object[] {"@Proposta_OSServico",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004828 ;
          prmBC004828 = new Object[] {
          new Object[] {"@Proposta_OSUndCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004830 ;
          prmBC004830 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004832 ;
          prmBC004832 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004834 ;
          prmBC004834 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004836 ;
          prmBC004836 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004837 ;
          prmBC004837 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          Object[] prmBC004842 ;
          prmBC004842 = new Object[] {
          new Object[] {"@Proposta_Codigo",SqlDbType.Int,9,0}
          } ;
          String cmdBufferBC004842 ;
          cmdBufferBC004842=" SELECT TM1.[Proposta_Codigo], TM1.[Proposta_Vigencia], TM1.[Proposta_Valor], TM1.[Proposta_Status], TM1.[Proposta_Objetivo], TM1.[Proposta_Escopo], TM1.[Proposta_Oportunidade], TM1.[Proposta_Perspectiva], TM1.[Proposta_RestricaoImplantacao], TM1.[Proposta_RestricaoCusto], TM1.[Proposta_Ativo], TM1.[Proposta_PrestatoraCod], TM1.[Proposta_PrestadoraRazSocial], TM1.[Proposta_Prazo], TM1.[Proposta_PrazoDias], TM1.[Proposta_Esforco], TM1.[Proposta_Liquido], TM1.[Proposta_CntSrvCod], TM1.[Proposta_ValorUndCnt], T2.[ContagemResultado_Demanda] AS Proposta_OSDmn, T2.[ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, T4.[UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl, T3.[ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, T3.[ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, T3.[ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, T2.[ContagemResultado_DataInicio] AS Proposta_OSInicio, TM1.[Projeto_Codigo], TM1.[AreaTrabalho_Codigo], TM1.[Proposta_OSCodigo] AS Proposta_OSCodigo, T2.[ContagemResultado_CntSrvCod] AS Proposta_OSServico, T3.[Contrato_Codigo] AS Proposta_OSCntCod, T3.[ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod, COALESCE( T5.[Proposta_OSPFB], 0) AS Proposta_OSPFB, COALESCE( T6.[Proposta_OSPFL], 0) AS Proposta_OSPFL, COALESCE( T7.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt, COALESCE( T8.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (((((((dbo.[Proposta] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T4 WITH (NOLOCK) ON T4.[UnidadeMedicao_Codigo] = T3.[ContratoServicos_UnidadeContratada]) "
          + " LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = TM1.[Proposta_OSCodigo]) WHERE TM1.[Proposta_Codigo] = @Proposta_Codigo ORDER BY TM1.[Proposta_Codigo]  OPTION (FAST 100)" ;
          def= new CursorDef[] {
              new CursorDef("BC00482", "SELECT [Proposta_Codigo], [Proposta_Vigencia], [Proposta_Valor], [Proposta_Status], [Proposta_Objetivo], [Proposta_Escopo], [Proposta_Oportunidade], [Proposta_Perspectiva], [Proposta_RestricaoImplantacao], [Proposta_RestricaoCusto], [Proposta_Ativo], [Proposta_PrestatoraCod], [Proposta_PrestadoraRazSocial], [Proposta_Prazo], [Proposta_PrazoDias], [Proposta_Esforco], [Proposta_Liquido], [Proposta_CntSrvCod], [Proposta_ValorUndCnt], [Projeto_Codigo], [AreaTrabalho_Codigo], [Proposta_OSCodigo] AS Proposta_OSCodigo FROM dbo.[Proposta] WITH (UPDLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00482,1,0,true,false )
             ,new CursorDef("BC00483", "SELECT [Proposta_Codigo], [Proposta_Vigencia], [Proposta_Valor], [Proposta_Status], [Proposta_Objetivo], [Proposta_Escopo], [Proposta_Oportunidade], [Proposta_Perspectiva], [Proposta_RestricaoImplantacao], [Proposta_RestricaoCusto], [Proposta_Ativo], [Proposta_PrestatoraCod], [Proposta_PrestadoraRazSocial], [Proposta_Prazo], [Proposta_PrazoDias], [Proposta_Esforco], [Proposta_Liquido], [Proposta_CntSrvCod], [Proposta_ValorUndCnt], [Projeto_Codigo], [AreaTrabalho_Codigo], [Proposta_OSCodigo] AS Proposta_OSCodigo FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00483,1,0,true,false )
             ,new CursorDef("BC00484", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00484,1,0,true,false )
             ,new CursorDef("BC00485", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00485,1,0,true,false )
             ,new CursorDef("BC00486", "SELECT [ContagemResultado_Demanda] AS Proposta_OSDmn, [ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, [ContagemResultado_DataInicio] AS Proposta_OSInicio, [ContagemResultado_CntSrvCod] AS Proposta_OSServico FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00486,1,0,true,false )
             ,new CursorDef("BC00487", "SELECT [ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, [ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, [ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, [Contrato_Codigo] AS Proposta_OSCntCod, [ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_OSServico ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00487,1,0,true,false )
             ,new CursorDef("BC00488", "SELECT [UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @Proposta_OSUndCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00488,1,0,true,false )
             ,new CursorDef("BC004810", "SELECT COALESCE( T1.[Proposta_OSPFB], 0) AS Proposta_OSPFB FROM (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004810,1,0,true,false )
             ,new CursorDef("BC004812", "SELECT COALESCE( T1.[Proposta_OSPFL], 0) AS Proposta_OSPFL FROM (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004812,1,0,true,false )
             ,new CursorDef("BC004814", "SELECT COALESCE( T1.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt FROM (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004814,1,0,true,false )
             ,new CursorDef("BC004816", "SELECT COALESCE( T1.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004816,1,0,true,false )
             ,new CursorDef("BC004821", cmdBufferBC004821,true, GxErrorMask.GX_NOMASK, false, this,prmBC004821,100,0,true,false )
             ,new CursorDef("BC004822", "SELECT [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004822,1,0,true,false )
             ,new CursorDef("BC004823", "INSERT INTO dbo.[Proposta]([Proposta_Vigencia], [Proposta_Valor], [Proposta_Status], [Proposta_Objetivo], [Proposta_Escopo], [Proposta_Oportunidade], [Proposta_Perspectiva], [Proposta_RestricaoImplantacao], [Proposta_RestricaoCusto], [Proposta_Ativo], [Proposta_PrestatoraCod], [Proposta_PrestadoraRazSocial], [Proposta_Prazo], [Proposta_PrazoDias], [Proposta_Esforco], [Proposta_Liquido], [Proposta_CntSrvCod], [Proposta_ValorUndCnt], [Projeto_Codigo], [AreaTrabalho_Codigo], [Proposta_OSCodigo]) VALUES(@Proposta_Vigencia, @Proposta_Valor, @Proposta_Status, @Proposta_Objetivo, @Proposta_Escopo, @Proposta_Oportunidade, @Proposta_Perspectiva, @Proposta_RestricaoImplantacao, @Proposta_RestricaoCusto, @Proposta_Ativo, @Proposta_ContratadaCod, @Proposta_ContratadaRazSocial, @Proposta_Prazo, @Proposta_PrazoDias, @Proposta_Esforco, @Proposta_Liquido, @Proposta_CntSrvCod, @Proposta_ValorUndCnt, @Projeto_Codigo, @AreaTrabalho_Codigo, @Proposta_OSCodigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004823)
             ,new CursorDef("BC004824", "UPDATE dbo.[Proposta] SET [Proposta_Vigencia]=@Proposta_Vigencia, [Proposta_Valor]=@Proposta_Valor, [Proposta_Status]=@Proposta_Status, [Proposta_Objetivo]=@Proposta_Objetivo, [Proposta_Escopo]=@Proposta_Escopo, [Proposta_Oportunidade]=@Proposta_Oportunidade, [Proposta_Perspectiva]=@Proposta_Perspectiva, [Proposta_RestricaoImplantacao]=@Proposta_RestricaoImplantacao, [Proposta_RestricaoCusto]=@Proposta_RestricaoCusto, [Proposta_Ativo]=@Proposta_Ativo, [Proposta_PrestatoraCod]=@Proposta_ContratadaCod, [Proposta_PrestadoraRazSocial]=@Proposta_ContratadaRazSocial, [Proposta_Prazo]=@Proposta_Prazo, [Proposta_PrazoDias]=@Proposta_PrazoDias, [Proposta_Esforco]=@Proposta_Esforco, [Proposta_Liquido]=@Proposta_Liquido, [Proposta_CntSrvCod]=@Proposta_CntSrvCod, [Proposta_ValorUndCnt]=@Proposta_ValorUndCnt, [Projeto_Codigo]=@Projeto_Codigo, [AreaTrabalho_Codigo]=@AreaTrabalho_Codigo, [Proposta_OSCodigo]=@Proposta_OSCodigo  WHERE [Proposta_Codigo] = @Proposta_Codigo", GxErrorMask.GX_NOMASK,prmBC004824)
             ,new CursorDef("BC004825", "DELETE FROM dbo.[Proposta]  WHERE [Proposta_Codigo] = @Proposta_Codigo", GxErrorMask.GX_NOMASK,prmBC004825)
             ,new CursorDef("BC004826", "SELECT [ContagemResultado_Demanda] AS Proposta_OSDmn, [ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, [ContagemResultado_DataInicio] AS Proposta_OSInicio, [ContagemResultado_CntSrvCod] AS Proposta_OSServico FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004826,1,0,true,false )
             ,new CursorDef("BC004827", "SELECT [ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, [ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, [ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, [Contrato_Codigo] AS Proposta_OSCntCod, [ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_OSServico ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004827,1,0,true,false )
             ,new CursorDef("BC004828", "SELECT [UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @Proposta_OSUndCntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004828,1,0,true,false )
             ,new CursorDef("BC004830", "SELECT COALESCE( T1.[Proposta_OSPFB], 0) AS Proposta_OSPFB FROM (SELECT MIN([ContagemResultado_PFBFM]) AS Proposta_OSPFB, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004830,1,0,true,false )
             ,new CursorDef("BC004832", "SELECT COALESCE( T1.[Proposta_OSPFL], 0) AS Proposta_OSPFL FROM (SELECT MIN([ContagemResultado_PFLFM]) AS Proposta_OSPFL, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004832,1,0,true,false )
             ,new CursorDef("BC004834", "SELECT COALESCE( T1.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt FROM (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004834,1,0,true,false )
             ,new CursorDef("BC004836", "SELECT COALESCE( T1.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @Proposta_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004836,1,0,true,false )
             ,new CursorDef("BC004837", "SELECT TOP 1 [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE [Proposta_Codigo] = @Proposta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004837,1,0,true,true )
             ,new CursorDef("BC004842", cmdBufferBC004842,true, GxErrorMask.GX_NOMASK, false, this,prmBC004842,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((int[]) buf[29])[0] = rslt.getInt(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((int[]) buf[31])[0] = rslt.getInt(21) ;
                ((int[]) buf[32])[0] = rslt.getInt(22) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((int[]) buf[29])[0] = rslt.getInt(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((int[]) buf[31])[0] = rslt.getInt(21) ;
                ((int[]) buf[32])[0] = rslt.getInt(22) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((String[]) buf[29])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((String[]) buf[33])[0] = rslt.getString(22, 15) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((String[]) buf[35])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((short[]) buf[37])[0] = rslt.getShort(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                ((String[]) buf[39])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[41])[0] = rslt.getGXDate(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((int[]) buf[46])[0] = rslt.getInt(29) ;
                ((int[]) buf[47])[0] = rslt.getInt(30) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(30);
                ((int[]) buf[49])[0] = rslt.getInt(31) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(31);
                ((int[]) buf[51])[0] = rslt.getInt(32) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(32);
                ((decimal[]) buf[53])[0] = rslt.getDecimal(33) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(33);
                ((decimal[]) buf[55])[0] = rslt.getDecimal(34) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(34);
                ((DateTime[]) buf[57])[0] = rslt.getGXDate(35) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(35);
                ((String[]) buf[59])[0] = rslt.getString(36, 5) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(36);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((bool[]) buf[17])[0] = rslt.getBool(11) ;
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((String[]) buf[19])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((int[]) buf[25])[0] = rslt.getInt(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((int[]) buf[27])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((String[]) buf[29])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(20);
                ((String[]) buf[31])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(21);
                ((String[]) buf[33])[0] = rslt.getString(22, 15) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((String[]) buf[35])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((short[]) buf[37])[0] = rslt.getShort(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                ((String[]) buf[39])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[41])[0] = rslt.getGXDate(26) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(26);
                ((int[]) buf[43])[0] = rslt.getInt(27) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(27);
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                ((int[]) buf[46])[0] = rslt.getInt(29) ;
                ((int[]) buf[47])[0] = rslt.getInt(30) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(30);
                ((int[]) buf[49])[0] = rslt.getInt(31) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(31);
                ((int[]) buf[51])[0] = rslt.getInt(32) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(32);
                ((decimal[]) buf[53])[0] = rslt.getDecimal(33) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(33);
                ((decimal[]) buf[55])[0] = rslt.getDecimal(34) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(34);
                ((DateTime[]) buf[57])[0] = rslt.getGXDate(35) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(35);
                ((String[]) buf[59])[0] = rslt.getString(36, 5) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(36);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (decimal)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[15]);
                }
                stmt.SetParameter(10, (bool)parms[16]);
                stmt.SetParameter(11, (int)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[19]);
                }
                stmt.SetParameter(13, (DateTime)parms[20]);
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[22]);
                }
                stmt.SetParameter(15, (int)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[25]);
                }
                stmt.SetParameter(17, (int)parms[26]);
                stmt.SetParameter(18, (decimal)parms[27]);
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[29]);
                }
                stmt.SetParameter(20, (int)parms[30]);
                stmt.SetParameter(21, (int)parms[31]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (decimal)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[15]);
                }
                stmt.SetParameter(10, (bool)parms[16]);
                stmt.SetParameter(11, (int)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[19]);
                }
                stmt.SetParameter(13, (DateTime)parms[20]);
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[22]);
                }
                stmt.SetParameter(15, (int)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[25]);
                }
                stmt.SetParameter(17, (int)parms[26]);
                stmt.SetParameter(18, (decimal)parms[27]);
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[29]);
                }
                stmt.SetParameter(20, (int)parms[30]);
                stmt.SetParameter(21, (int)parms[31]);
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[33]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
