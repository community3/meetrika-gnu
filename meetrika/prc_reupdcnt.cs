/*
               File: PRC_REUpdCnt
        Description: RE Updfsyr  Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:46.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_reupdcnt : GXProcedure
   {
      public prc_reupdcnt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_reupdcnt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           decimal aP1_ContagemResultado_PFBFM ,
                           decimal aP2_ContagemResultado_PFLFM ,
                           int aP3_UserId ,
                           String aP4_HoraCnt )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_PFBFM = aP1_ContagemResultado_PFBFM;
         this.AV11ContagemResultado_PFLFM = aP2_ContagemResultado_PFLFM;
         this.AV8UserId = aP3_UserId;
         this.AV9HoraCnt = aP4_HoraCnt;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 decimal aP1_ContagemResultado_PFBFM ,
                                 decimal aP2_ContagemResultado_PFLFM ,
                                 int aP3_UserId ,
                                 String aP4_HoraCnt )
      {
         prc_reupdcnt objprc_reupdcnt;
         objprc_reupdcnt = new prc_reupdcnt();
         objprc_reupdcnt.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_reupdcnt.AV10ContagemResultado_PFBFM = aP1_ContagemResultado_PFBFM;
         objprc_reupdcnt.AV11ContagemResultado_PFLFM = aP2_ContagemResultado_PFLFM;
         objprc_reupdcnt.AV8UserId = aP3_UserId;
         objprc_reupdcnt.AV9HoraCnt = aP4_HoraCnt;
         objprc_reupdcnt.context.SetSubmitInitialConfig(context);
         objprc_reupdcnt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_reupdcnt);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_reupdcnt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00C72 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A461ContagemResultado_PFLFM = P00C72_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P00C72_n461ContagemResultado_PFLFM[0];
            A460ContagemResultado_PFBFM = P00C72_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P00C72_n460ContagemResultado_PFBFM[0];
            A511ContagemResultado_HoraCnt = P00C72_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P00C72_A473ContagemResultado_DataCnt[0];
            A1756ContagemResultado_NvlCnt = P00C72_A1756ContagemResultado_NvlCnt[0];
            n1756ContagemResultado_NvlCnt = P00C72_n1756ContagemResultado_NvlCnt[0];
            A901ContagemResultadoContagens_Prazo = P00C72_A901ContagemResultadoContagens_Prazo[0];
            n901ContagemResultadoContagens_Prazo = P00C72_n901ContagemResultadoContagens_Prazo[0];
            A854ContagemResultado_TipoPla = P00C72_A854ContagemResultado_TipoPla[0];
            n854ContagemResultado_TipoPla = P00C72_n854ContagemResultado_TipoPla[0];
            A853ContagemResultado_NomePla = P00C72_A853ContagemResultado_NomePla[0];
            n853ContagemResultado_NomePla = P00C72_n853ContagemResultado_NomePla[0];
            A833ContagemResultado_CstUntPrd = P00C72_A833ContagemResultado_CstUntPrd[0];
            n833ContagemResultado_CstUntPrd = P00C72_n833ContagemResultado_CstUntPrd[0];
            A800ContagemResultado_Deflator = P00C72_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P00C72_n800ContagemResultado_Deflator[0];
            A517ContagemResultado_Ultima = P00C72_A517ContagemResultado_Ultima[0];
            A482ContagemResultadoContagens_Esforco = P00C72_A482ContagemResultadoContagens_Esforco[0];
            A483ContagemResultado_StatusCnt = P00C72_A483ContagemResultado_StatusCnt[0];
            A469ContagemResultado_NaoCnfCntCod = P00C72_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = P00C72_n469ContagemResultado_NaoCnfCntCod[0];
            A470ContagemResultado_ContadorFMCod = P00C72_A470ContagemResultado_ContadorFMCod[0];
            A463ContagemResultado_ParecerTcn = P00C72_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = P00C72_n463ContagemResultado_ParecerTcn[0];
            A462ContagemResultado_Divergencia = P00C72_A462ContagemResultado_Divergencia[0];
            A459ContagemResultado_PFLFS = P00C72_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P00C72_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P00C72_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P00C72_n458ContagemResultado_PFBFS[0];
            A481ContagemResultado_TimeCnt = P00C72_A481ContagemResultado_TimeCnt[0];
            n481ContagemResultado_TimeCnt = P00C72_n481ContagemResultado_TimeCnt[0];
            A852ContagemResultado_Planilha = P00C72_A852ContagemResultado_Planilha[0];
            n852ContagemResultado_Planilha = P00C72_n852ContagemResultado_Planilha[0];
            A470ContagemResultado_ContadorFMCod = AV8UserId;
            /*
               INSERT RECORD ON TABLE ContagemResultadoContagens

            */
            W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            W460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            W461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            A473ContagemResultado_DataCnt = DateTimeUtil.ServerDate( context, "DEFAULT");
            A511ContagemResultado_HoraCnt = AV9HoraCnt;
            A460ContagemResultado_PFBFM = AV10ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A461ContagemResultado_PFLFM = AV11ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            /* Using cursor P00C73 */
            A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
            n854ContagemResultado_TipoPla = false;
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n481ContagemResultado_TimeCnt, A481ContagemResultado_TimeCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n901ContagemResultadoContagens_Prazo, A901ContagemResultadoContagens_Prazo, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
            A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
            A460ContagemResultado_PFBFM = W460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A461ContagemResultado_PFLFM = W461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            /* End Insert */
            /* Using cursor P00C74 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00C75 */
            pr_default.execute(3, new Object[] {A470ContagemResultado_ContadorFMCod, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P00C76 */
            pr_default.execute(4, new Object[] {A470ContagemResultado_ContadorFMCod, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00C72_A456ContagemResultado_Codigo = new int[1] ;
         P00C72_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00C72_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P00C72_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00C72_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00C72_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00C72_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00C72_A1756ContagemResultado_NvlCnt = new short[1] ;
         P00C72_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         P00C72_A901ContagemResultadoContagens_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00C72_n901ContagemResultadoContagens_Prazo = new bool[] {false} ;
         P00C72_A854ContagemResultado_TipoPla = new String[] {""} ;
         P00C72_n854ContagemResultado_TipoPla = new bool[] {false} ;
         P00C72_A853ContagemResultado_NomePla = new String[] {""} ;
         P00C72_n853ContagemResultado_NomePla = new bool[] {false} ;
         P00C72_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P00C72_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         P00C72_A800ContagemResultado_Deflator = new decimal[1] ;
         P00C72_n800ContagemResultado_Deflator = new bool[] {false} ;
         P00C72_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00C72_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P00C72_A483ContagemResultado_StatusCnt = new short[1] ;
         P00C72_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P00C72_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P00C72_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P00C72_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P00C72_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P00C72_A462ContagemResultado_Divergencia = new decimal[1] ;
         P00C72_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00C72_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00C72_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00C72_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00C72_A481ContagemResultado_TimeCnt = new DateTime[] {DateTime.MinValue} ;
         P00C72_n481ContagemResultado_TimeCnt = new bool[] {false} ;
         P00C72_A852ContagemResultado_Planilha = new String[] {""} ;
         P00C72_n852ContagemResultado_Planilha = new bool[] {false} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A901ContagemResultadoContagens_Prazo = (DateTime)(DateTime.MinValue);
         A854ContagemResultado_TipoPla = "";
         A853ContagemResultado_NomePla = "";
         A463ContagemResultado_ParecerTcn = "";
         A481ContagemResultado_TimeCnt = (DateTime)(DateTime.MinValue);
         A852ContagemResultado_Planilha = "";
         W473ContagemResultado_DataCnt = DateTime.MinValue;
         W511ContagemResultado_HoraCnt = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_reupdcnt__default(),
            new Object[][] {
                new Object[] {
               P00C72_A456ContagemResultado_Codigo, P00C72_A461ContagemResultado_PFLFM, P00C72_n461ContagemResultado_PFLFM, P00C72_A460ContagemResultado_PFBFM, P00C72_n460ContagemResultado_PFBFM, P00C72_A511ContagemResultado_HoraCnt, P00C72_A473ContagemResultado_DataCnt, P00C72_A1756ContagemResultado_NvlCnt, P00C72_n1756ContagemResultado_NvlCnt, P00C72_A901ContagemResultadoContagens_Prazo,
               P00C72_n901ContagemResultadoContagens_Prazo, P00C72_A854ContagemResultado_TipoPla, P00C72_n854ContagemResultado_TipoPla, P00C72_A853ContagemResultado_NomePla, P00C72_n853ContagemResultado_NomePla, P00C72_A833ContagemResultado_CstUntPrd, P00C72_n833ContagemResultado_CstUntPrd, P00C72_A800ContagemResultado_Deflator, P00C72_n800ContagemResultado_Deflator, P00C72_A517ContagemResultado_Ultima,
               P00C72_A482ContagemResultadoContagens_Esforco, P00C72_A483ContagemResultado_StatusCnt, P00C72_A469ContagemResultado_NaoCnfCntCod, P00C72_n469ContagemResultado_NaoCnfCntCod, P00C72_A470ContagemResultado_ContadorFMCod, P00C72_A463ContagemResultado_ParecerTcn, P00C72_n463ContagemResultado_ParecerTcn, P00C72_A462ContagemResultado_Divergencia, P00C72_A459ContagemResultado_PFLFS, P00C72_n459ContagemResultado_PFLFS,
               P00C72_A458ContagemResultado_PFBFS, P00C72_n458ContagemResultado_PFBFS, P00C72_A481ContagemResultado_TimeCnt, P00C72_n481ContagemResultado_TimeCnt, P00C72_A852ContagemResultado_Planilha, P00C72_n852ContagemResultado_Planilha
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1756ContagemResultado_NvlCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private int A456ContagemResultado_Codigo ;
      private int AV8UserId ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int GX_INS72 ;
      private decimal AV10ContagemResultado_PFBFM ;
      private decimal AV11ContagemResultado_PFLFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal W460ContagemResultado_PFBFM ;
      private decimal W461ContagemResultado_PFLFM ;
      private String AV9HoraCnt ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private String A854ContagemResultado_TipoPla ;
      private String A853ContagemResultado_NomePla ;
      private String W511ContagemResultado_HoraCnt ;
      private String Gx_emsg ;
      private DateTime A901ContagemResultadoContagens_Prazo ;
      private DateTime A481ContagemResultado_TimeCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime W473ContagemResultado_DataCnt ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n901ContagemResultadoContagens_Prazo ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n800ContagemResultado_Deflator ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n481ContagemResultado_TimeCnt ;
      private bool n852ContagemResultado_Planilha ;
      private String A463ContagemResultado_ParecerTcn ;
      private String A852ContagemResultado_Planilha ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00C72_A456ContagemResultado_Codigo ;
      private decimal[] P00C72_A461ContagemResultado_PFLFM ;
      private bool[] P00C72_n461ContagemResultado_PFLFM ;
      private decimal[] P00C72_A460ContagemResultado_PFBFM ;
      private bool[] P00C72_n460ContagemResultado_PFBFM ;
      private String[] P00C72_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00C72_A473ContagemResultado_DataCnt ;
      private short[] P00C72_A1756ContagemResultado_NvlCnt ;
      private bool[] P00C72_n1756ContagemResultado_NvlCnt ;
      private DateTime[] P00C72_A901ContagemResultadoContagens_Prazo ;
      private bool[] P00C72_n901ContagemResultadoContagens_Prazo ;
      private String[] P00C72_A854ContagemResultado_TipoPla ;
      private bool[] P00C72_n854ContagemResultado_TipoPla ;
      private String[] P00C72_A853ContagemResultado_NomePla ;
      private bool[] P00C72_n853ContagemResultado_NomePla ;
      private decimal[] P00C72_A833ContagemResultado_CstUntPrd ;
      private bool[] P00C72_n833ContagemResultado_CstUntPrd ;
      private decimal[] P00C72_A800ContagemResultado_Deflator ;
      private bool[] P00C72_n800ContagemResultado_Deflator ;
      private bool[] P00C72_A517ContagemResultado_Ultima ;
      private short[] P00C72_A482ContagemResultadoContagens_Esforco ;
      private short[] P00C72_A483ContagemResultado_StatusCnt ;
      private int[] P00C72_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P00C72_n469ContagemResultado_NaoCnfCntCod ;
      private int[] P00C72_A470ContagemResultado_ContadorFMCod ;
      private String[] P00C72_A463ContagemResultado_ParecerTcn ;
      private bool[] P00C72_n463ContagemResultado_ParecerTcn ;
      private decimal[] P00C72_A462ContagemResultado_Divergencia ;
      private decimal[] P00C72_A459ContagemResultado_PFLFS ;
      private bool[] P00C72_n459ContagemResultado_PFLFS ;
      private decimal[] P00C72_A458ContagemResultado_PFBFS ;
      private bool[] P00C72_n458ContagemResultado_PFBFS ;
      private DateTime[] P00C72_A481ContagemResultado_TimeCnt ;
      private bool[] P00C72_n481ContagemResultado_TimeCnt ;
      private String[] P00C72_A852ContagemResultado_Planilha ;
      private bool[] P00C72_n852ContagemResultado_Planilha ;
   }

   public class prc_reupdcnt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C72 ;
          prmP00C72 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00C73 ;
          prmP00C73 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_TimeCnt",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoContagens_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00C74 ;
          prmP00C74 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00C75 ;
          prmP00C75 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00C76 ;
          prmP00C76 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C72", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PFLFM], [ContagemResultado_PFBFM], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt], [ContagemResultado_NvlCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_TipoPla], [ContagemResultado_NomePla], [ContagemResultado_CstUntPrd], [ContagemResultado_Deflator], [ContagemResultado_Ultima], [ContagemResultadoContagens_Esforco], [ContagemResultado_StatusCnt], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_ContadorFMCod], [ContagemResultado_ParecerTcn], [ContagemResultado_Divergencia], [ContagemResultado_PFLFS], [ContagemResultado_PFBFS], [ContagemResultado_TimeCnt], [ContagemResultado_Planilha] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C72,1,0,true,true )
             ,new CursorDef("P00C73", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_TimeCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, @ContagemResultadoContagens_Prazo, @ContagemResultado_NvlCnt)", GxErrorMask.GX_NOMASK,prmP00C73)
             ,new CursorDef("P00C74", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00C74)
             ,new CursorDef("P00C75", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_ContadorFMCod]=@ContagemResultado_ContadorFMCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00C75)
             ,new CursorDef("P00C76", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_ContadorFMCod]=@ContagemResultado_ContadorFMCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00C76)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((int[]) buf[24])[0] = rslt.getInt(16) ;
                ((String[]) buf[25])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((decimal[]) buf[27])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(19);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(20);
                ((DateTime[]) buf[32])[0] = rslt.getGXDateTime(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getBLOBFile(22, rslt.getString(8, 10), rslt.getString(9, 50)) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[12]);
                }
                stmt.SetParameter(9, (decimal)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                stmt.SetParameter(11, (int)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[18]);
                }
                stmt.SetParameter(13, (short)parms[19]);
                stmt.SetParameter(14, (short)parms[20]);
                stmt.SetParameter(15, (bool)parms[21]);
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(17, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(21, (DateTime)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(22, (short)parms[35]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
       }
    }

 }

}
