/*
               File: About
        Description: About
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:56:2.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class about : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public about( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public about( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAHV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTHV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311856224");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("about.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_HOSTMENSURACAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1163ParametrosSistema_HostMensuracao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_SQLCOMPUTERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_SQLSERVERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_SQLEDITION", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_SQLINSTANCE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_DBNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEHV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTHV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("about.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "About" ;
      }

      public override String GetPgmdesc( )
      {
         return "About" ;
      }

      protected void WBHV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_HV2( true) ;
         }
         else
         {
            wb_table1_2_HV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_HV2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTHV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "About", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPHV0( ) ;
      }

      protected void WSHV2( )
      {
         STARTHV2( ) ;
         EVTHV2( ) ;
      }

      protected void EVTHV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11HV2 */
                              E11HV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12HV2 */
                              E12HV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEHV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAHV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFHV2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00HV2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A330ParametrosSistema_Codigo = H00HV2_A330ParametrosSistema_Codigo[0];
               A1170ParametrosSistema_DBName = H00HV2_A1170ParametrosSistema_DBName[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1170ParametrosSistema_DBName", A1170ParametrosSistema_DBName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_DBNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, ""))));
               n1170ParametrosSistema_DBName = H00HV2_n1170ParametrosSistema_DBName[0];
               A1169ParametrosSistema_SQLInstance = H00HV2_A1169ParametrosSistema_SQLInstance[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1169ParametrosSistema_SQLInstance", A1169ParametrosSistema_SQLInstance);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLINSTANCE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, ""))));
               n1169ParametrosSistema_SQLInstance = H00HV2_n1169ParametrosSistema_SQLInstance[0];
               A1168ParametrosSistema_SQLEdition = H00HV2_A1168ParametrosSistema_SQLEdition[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1168ParametrosSistema_SQLEdition", A1168ParametrosSistema_SQLEdition);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLEDITION", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, ""))));
               n1168ParametrosSistema_SQLEdition = H00HV2_n1168ParametrosSistema_SQLEdition[0];
               A1172ParametrosSistema_SQLServerName = H00HV2_A1172ParametrosSistema_SQLServerName[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1172ParametrosSistema_SQLServerName", A1172ParametrosSistema_SQLServerName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLSERVERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, ""))));
               n1172ParametrosSistema_SQLServerName = H00HV2_n1172ParametrosSistema_SQLServerName[0];
               A1171ParametrosSistema_SQLComputerName = H00HV2_A1171ParametrosSistema_SQLComputerName[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1171ParametrosSistema_SQLComputerName", A1171ParametrosSistema_SQLComputerName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLCOMPUTERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, ""))));
               n1171ParametrosSistema_SQLComputerName = H00HV2_n1171ParametrosSistema_SQLComputerName[0];
               A1163ParametrosSistema_HostMensuracao = H00HV2_A1163ParametrosSistema_HostMensuracao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_HOSTMENSURACAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1163ParametrosSistema_HostMensuracao, ""))));
               n1163ParametrosSistema_HostMensuracao = H00HV2_n1163ParametrosSistema_HostMensuracao[0];
               /* Execute user event: E12HV2 */
               E12HV2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBHV0( ) ;
         }
      }

      protected void STRUPHV0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11HV2 */
         E11HV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1163ParametrosSistema_HostMensuracao = cgiGet( edtParametrosSistema_HostMensuracao_Internalname);
            n1163ParametrosSistema_HostMensuracao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_HOSTMENSURACAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1163ParametrosSistema_HostMensuracao, ""))));
            A1171ParametrosSistema_SQLComputerName = cgiGet( edtParametrosSistema_SQLComputerName_Internalname);
            n1171ParametrosSistema_SQLComputerName = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1171ParametrosSistema_SQLComputerName", A1171ParametrosSistema_SQLComputerName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLCOMPUTERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, ""))));
            A1172ParametrosSistema_SQLServerName = cgiGet( edtParametrosSistema_SQLServerName_Internalname);
            n1172ParametrosSistema_SQLServerName = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1172ParametrosSistema_SQLServerName", A1172ParametrosSistema_SQLServerName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLSERVERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, ""))));
            A1168ParametrosSistema_SQLEdition = cgiGet( edtParametrosSistema_SQLEdition_Internalname);
            n1168ParametrosSistema_SQLEdition = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1168ParametrosSistema_SQLEdition", A1168ParametrosSistema_SQLEdition);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLEDITION", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, ""))));
            A1169ParametrosSistema_SQLInstance = cgiGet( edtParametrosSistema_SQLInstance_Internalname);
            n1169ParametrosSistema_SQLInstance = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1169ParametrosSistema_SQLInstance", A1169ParametrosSistema_SQLInstance);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_SQLINSTANCE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, ""))));
            A1170ParametrosSistema_DBName = cgiGet( edtParametrosSistema_DBName_Internalname);
            n1170ParametrosSistema_DBName = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1170ParametrosSistema_DBName", A1170ParametrosSistema_DBName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_DBNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, ""))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11HV2 */
         E11HV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11HV2( )
      {
         /* Start Routine */
         /* User SQL Command. */
         cmdBuffer="  Update ParametrosSistema set ParametrosSistema_SQLComputerName = convert(sysname,serverproperty('ComputerNamePhysicalNetBIOS')), ParametrosSistema_SQLServerName = convert(sysname,serverproperty('ServerName')), ParametrosSistema_SQLEdition = convert(sysname,serverproperty('Edition')), ParametrosSistema_SQLInstance = convert(sysname,serverproperty('InstanceName')), ParametrosSistema_DBName = convert(sysname,db_name()) where ParametrosSistema_Codigo = 1 "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
      }

      protected void nextLoad( )
      {
      }

      protected void E12HV2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_HV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "URL de Mensuração", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_HostMensuracao_Internalname, A1163ParametrosSistema_HostMensuracao, StringUtil.RTrim( context.localUtil.Format( A1163ParametrosSistema_HostMensuracao, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_HostMensuracao_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:14.0pt; font-weight:normal; font-style:normal;", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "SQL Computer Name", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_SQLComputerName_Internalname, A1171ParametrosSistema_SQLComputerName, StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_SQLComputerName_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:14.0pt; font-weight:normal; font-style:normal;", "", "", 1, 0, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "SQL Server Name", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_SQLServerName_Internalname, A1172ParametrosSistema_SQLServerName, StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_SQLServerName_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:14.0pt; font-weight:normal; font-style:normal;", "", "", 1, 0, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "SQL Edition", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_SQLEdition_Internalname, A1168ParametrosSistema_SQLEdition, StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_SQLEdition_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:14.0pt; font-weight:normal; font-style:normal;", "", "", 1, 0, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "SQL Instance", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_SQLInstance_Internalname, A1169ParametrosSistema_SQLInstance, StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_SQLInstance_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:14.0pt; font-weight:normal; font-style:normal;", "", "", 1, 0, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "DB Name", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_DBName_Internalname, A1170ParametrosSistema_DBName, StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_DBName_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:14.0pt; font-weight:normal; font-style:normal;", "", "", 1, 0, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_About.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_HV2e( true) ;
         }
         else
         {
            wb_table1_2_HV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHV2( ) ;
         WSHV2( ) ;
         WEHV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311856239");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("about.js", "?2020311856239");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtParametrosSistema_HostMensuracao_Internalname = "PARAMETROSSISTEMA_HOSTMENSURACAO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtParametrosSistema_SQLComputerName_Internalname = "PARAMETROSSISTEMA_SQLCOMPUTERNAME";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtParametrosSistema_SQLServerName_Internalname = "PARAMETROSSISTEMA_SQLSERVERNAME";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtParametrosSistema_SQLEdition_Internalname = "PARAMETROSSISTEMA_SQLEDITION";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtParametrosSistema_SQLInstance_Internalname = "PARAMETROSSISTEMA_SQLINSTANCE";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtParametrosSistema_DBName_Internalname = "PARAMETROSSISTEMA_DBNAME";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtParametrosSistema_DBName_Jsonclick = "";
         edtParametrosSistema_SQLInstance_Jsonclick = "";
         edtParametrosSistema_SQLEdition_Jsonclick = "";
         edtParametrosSistema_SQLServerName_Jsonclick = "";
         edtParametrosSistema_SQLComputerName_Jsonclick = "";
         edtParametrosSistema_HostMensuracao_Jsonclick = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "About";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1163ParametrosSistema_HostMensuracao = "";
         A1171ParametrosSistema_SQLComputerName = "";
         A1172ParametrosSistema_SQLServerName = "";
         A1168ParametrosSistema_SQLEdition = "";
         A1169ParametrosSistema_SQLInstance = "";
         A1170ParametrosSistema_DBName = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00HV2_A330ParametrosSistema_Codigo = new int[1] ;
         H00HV2_A1170ParametrosSistema_DBName = new String[] {""} ;
         H00HV2_n1170ParametrosSistema_DBName = new bool[] {false} ;
         H00HV2_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         H00HV2_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         H00HV2_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         H00HV2_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         H00HV2_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         H00HV2_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         H00HV2_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         H00HV2_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         H00HV2_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         H00HV2_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         cmdBuffer = "";
         sStyleString = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.about__default(),
            new Object[][] {
                new Object[] {
               H00HV2_A330ParametrosSistema_Codigo, H00HV2_A1170ParametrosSistema_DBName, H00HV2_n1170ParametrosSistema_DBName, H00HV2_A1169ParametrosSistema_SQLInstance, H00HV2_n1169ParametrosSistema_SQLInstance, H00HV2_A1168ParametrosSistema_SQLEdition, H00HV2_n1168ParametrosSistema_SQLEdition, H00HV2_A1172ParametrosSistema_SQLServerName, H00HV2_n1172ParametrosSistema_SQLServerName, H00HV2_A1171ParametrosSistema_SQLComputerName,
               H00HV2_n1171ParametrosSistema_SQLComputerName, H00HV2_A1163ParametrosSistema_HostMensuracao, H00HV2_n1163ParametrosSistema_HostMensuracao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private int A330ParametrosSistema_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtParametrosSistema_HostMensuracao_Internalname ;
      private String edtParametrosSistema_SQLComputerName_Internalname ;
      private String edtParametrosSistema_SQLServerName_Internalname ;
      private String edtParametrosSistema_SQLEdition_Internalname ;
      private String edtParametrosSistema_SQLInstance_Internalname ;
      private String edtParametrosSistema_DBName_Internalname ;
      private String cmdBuffer ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtParametrosSistema_HostMensuracao_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtParametrosSistema_SQLComputerName_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtParametrosSistema_SQLServerName_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtParametrosSistema_SQLEdition_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtParametrosSistema_SQLInstance_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtParametrosSistema_DBName_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1170ParametrosSistema_DBName ;
      private bool n1169ParametrosSistema_SQLInstance ;
      private bool n1168ParametrosSistema_SQLEdition ;
      private bool n1172ParametrosSistema_SQLServerName ;
      private bool n1171ParametrosSistema_SQLComputerName ;
      private bool n1163ParametrosSistema_HostMensuracao ;
      private bool returnInSub ;
      private String A1163ParametrosSistema_HostMensuracao ;
      private String A1171ParametrosSistema_SQLComputerName ;
      private String A1172ParametrosSistema_SQLServerName ;
      private String A1168ParametrosSistema_SQLEdition ;
      private String A1169ParametrosSistema_SQLInstance ;
      private String A1170ParametrosSistema_DBName ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00HV2_A330ParametrosSistema_Codigo ;
      private String[] H00HV2_A1170ParametrosSistema_DBName ;
      private bool[] H00HV2_n1170ParametrosSistema_DBName ;
      private String[] H00HV2_A1169ParametrosSistema_SQLInstance ;
      private bool[] H00HV2_n1169ParametrosSistema_SQLInstance ;
      private String[] H00HV2_A1168ParametrosSistema_SQLEdition ;
      private bool[] H00HV2_n1168ParametrosSistema_SQLEdition ;
      private String[] H00HV2_A1172ParametrosSistema_SQLServerName ;
      private bool[] H00HV2_n1172ParametrosSistema_SQLServerName ;
      private String[] H00HV2_A1171ParametrosSistema_SQLComputerName ;
      private bool[] H00HV2_n1171ParametrosSistema_SQLComputerName ;
      private String[] H00HV2_A1163ParametrosSistema_HostMensuracao ;
      private bool[] H00HV2_n1163ParametrosSistema_HostMensuracao ;
      private GxCommand RGZ ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class about__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HV2 ;
          prmH00HV2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HV2", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_DBName], [ParametrosSistema_SQLInstance], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLComputerName], [ParametrosSistema_HostMensuracao] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HV2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
