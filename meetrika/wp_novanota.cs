/*
               File: WP_NovaNota
        Description: Nota para a OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:18:38.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_novanota : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_novanota( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_novanota( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV46ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavUsuario_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUSUARIO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvUSUARIO_CODIGOJZ2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV46ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV46ContagemResultado_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAJZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavDatahora_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatahora_Enabled), 5, 0)));
               dynavUsuario_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
               GXVvUSUARIO_CODIGO_htmlJZ2( ) ;
               WSJZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEJZ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221183999");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_novanota.aspx") + "?" + UrlEncode("" +AV46ContagemResultado_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV42Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV42Codigos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADONOTAS", AV41ContagemResultadoNotas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADONOTAS", AV41ContagemResultadoNotas);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_STATUSULTCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47ContagemResultado_StatusUltCnt), 2, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV34WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV34WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDATAHORA", GetSecureSignedToken( "", AV37DataHora));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV46ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV46ContagemResultado_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormJZ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_NovaNota" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nota para a OS" ;
      }

      protected void WBJZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_JZ2( true) ;
         }
         else
         {
            wb_table1_2_JZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JZ2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTJZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Nota para a OS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJZ0( ) ;
      }

      protected void WSJZ2( )
      {
         STARTJZ2( ) ;
         EVTJZ2( ) ;
      }

      protected void EVTJZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11JZ2 */
                           E11JZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E12JZ2 */
                                 E12JZ2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13JZ2 */
                           E13JZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14JZ2 */
                           E14JZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15JZ2 */
                           E15JZ2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEJZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJZ2( ) ;
            }
         }
      }

      protected void PAJZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavUsuario_codigo.Name = "vUSUARIO_CODIGO";
            dynavUsuario_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavDatahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvUSUARIO_CODIGOJZ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUSUARIO_CODIGO_dataJZ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUSUARIO_CODIGO_htmlJZ2( )
      {
         int gxdynajaxvalue ;
         GXDLVvUSUARIO_CODIGO_dataJZ2( ) ;
         gxdynajaxindex = 1;
         dynavUsuario_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUsuario_codigo.ItemCount > 0 )
         {
            AV36Usuario_Codigo = (int)(NumberUtil.Val( dynavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvUSUARIO_CODIGO_dataJZ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00JZ2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JZ2_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JZ2_A58Usuario_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavUsuario_codigo.ItemCount > 0 )
         {
            AV36Usuario_Codigo = (int)(NumberUtil.Val( dynavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDatahora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatahora_Enabled), 5, 0)));
         dynavUsuario_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
      }

      protected void RFJZ2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E14JZ2 */
         E14JZ2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15JZ2 */
            E15JZ2 ();
            WBJZ0( ) ;
         }
      }

      protected void STRUPJZ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavDatahora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatahora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatahora_Enabled), 5, 0)));
         dynavUsuario_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
         GXVvUSUARIO_CODIGO_htmlJZ2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11JZ2 */
         E11JZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatahora_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Hora"}), 1, "vDATAHORA");
               GX_FocusControl = edtavDatahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37DataHora = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DataHora", context.localUtil.Format(AV37DataHora, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATAHORA", GetSecureSignedToken( "", AV37DataHora));
            }
            else
            {
               AV37DataHora = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatahora_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DataHora", context.localUtil.Format(AV37DataHora, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATAHORA", GetSecureSignedToken( "", AV37DataHora));
            }
            dynavUsuario_codigo.CurrentValue = cgiGet( dynavUsuario_codigo_Internalname);
            AV36Usuario_Codigo = (int)(NumberUtil.Val( cgiGet( dynavUsuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0)));
            AV38Nota = cgiGet( edtavNota_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Nota", AV38Nota);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvUSUARIO_CODIGO_htmlJZ2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11JZ2 */
         E11JZ2 ();
         if (returnInSub) return;
      }

      protected void E11JZ2( )
      {
         /* Start Routine */
         AV42Codigos.FromXml(AV43WebSession.Get("Codigos"), "Collection");
         AV43WebSession.Remove("Codigos");
         if ( (0==AV42Codigos.IndexOf(AV46ContagemResultado_Codigo)) )
         {
            AV42Codigos.Add(AV46ContagemResultado_Codigo, 0);
         }
         if ( AV42Codigos.Count > 1 )
         {
            Form.Caption = "Nova nota para as OS selecionadas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
         else
         {
            /* Using cursor H00JZ4 */
            pr_default.execute(1, new Object[] {AV46ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A490ContagemResultado_ContratadaCod = H00JZ4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00JZ4_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00JZ4_A456ContagemResultado_Codigo[0];
               A803ContagemResultado_ContratadaSigla = H00JZ4_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00JZ4_n803ContagemResultado_ContratadaSigla[0];
               A457ContagemResultado_Demanda = H00JZ4_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00JZ4_n457ContagemResultado_Demanda[0];
               A531ContagemResultado_StatusUltCnt = H00JZ4_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = H00JZ4_n531ContagemResultado_StatusUltCnt[0];
               A803ContagemResultado_ContratadaSigla = H00JZ4_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00JZ4_n803ContagemResultado_ContratadaSigla[0];
               A531ContagemResultado_StatusUltCnt = H00JZ4_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = H00JZ4_n531ContagemResultado_StatusUltCnt[0];
               Form.Caption = "Nova nota para OS "+A457ContagemResultado_Demanda+" ("+A803ContagemResultado_ContratadaSigla+")";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
               AV47ContagemResultado_StatusUltCnt = A531ContagemResultado_StatusUltCnt;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusUltCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47ContagemResultado_StatusUltCnt), 2, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12JZ2 */
         E12JZ2 ();
         if (returnInSub) return;
      }

      protected void E12JZ2( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38Nota)) )
         {
            GX_msglist.addItem("Nota � obrigat�rio!");
         }
         else
         {
            AV51GXV1 = 1;
            while ( AV51GXV1 <= AV42Codigos.Count )
            {
               AV8Codigo = (int)(AV42Codigos.GetNumeric(AV51GXV1));
               AV45OS.Load(AV8Codigo);
               AV31StatusDmn = AV45OS.gxTpr_Contagemresultado_statusdmn;
               AV27PrazoEntrega = DateTimeUtil.ResetTime( AV45OS.gxTpr_Contagemresultado_dataentrega ) ;
               AV27PrazoEntrega = DateTimeUtil.TAdd( AV27PrazoEntrega, 3600*(DateTimeUtil.Hour( AV45OS.gxTpr_Contagemresultado_horaentrega)));
               AV27PrazoEntrega = DateTimeUtil.TAdd( AV27PrazoEntrega, 60*(DateTimeUtil.Minute( AV45OS.gxTpr_Contagemresultado_horaentrega)));
               AV41ContagemResultadoNotas.gxTpr_Contagemresultadonota_datahora = DateTimeUtil.ServerNow( context, "DEFAULT");
               AV41ContagemResultadoNotas.gxTpr_Contagemresultadonota_demandacod = AV8Codigo;
               AV41ContagemResultadoNotas.gxTpr_Contagemresultadonota_nota = AV38Nota;
               AV41ContagemResultadoNotas.gxTpr_Contagemresultadonota_usuariocod = AV36Usuario_Codigo;
               AV41ContagemResultadoNotas.Save();
               new prc_inslogresponsavel(context ).execute( ref  AV8Codigo,  0,  "NO",  "D",  AV36Usuario_Codigo,  0,  AV31StatusDmn,  AV31StatusDmn,  AV38Nota,  AV27PrazoEntrega,  false) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Nota", AV38Nota);
               AV51GXV1 = (int)(AV51GXV1+1);
            }
            context.CommitDataStores( "WP_NovaNota");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41ContagemResultadoNotas", AV41ContagemResultadoNotas);
      }

      protected void E13JZ2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14JZ2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV34WWPContext) ;
         AV37DataHora = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DataHora", context.localUtil.Format(AV37DataHora, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDATAHORA", GetSecureSignedToken( "", AV37DataHora));
         AV36Usuario_Codigo = AV34WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0)));
         if ( AV47ContagemResultado_StatusUltCnt == 7 )
         {
            dynavUsuario_codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
         }
         else
         {
            dynavUsuario_codigo.Enabled = (AV34WWPContext.gxTpr_Userehgestor ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUsuario_codigo.Enabled), 5, 0)));
         }
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34WWPContext", AV34WWPContext);
         dynavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Values", dynavUsuario_codigo.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         bttBtnenter_Visible = ((AV34WWPContext.gxTpr_Insert) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E15JZ2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_JZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_JZ2( true) ;
         }
         else
         {
            wb_table2_8_JZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_JZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_25_JZ2( true) ;
         }
         else
         {
            wb_table3_25_JZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_25_JZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JZ2e( true) ;
         }
         else
         {
            wb_table1_2_JZ2e( false) ;
         }
      }

      protected void wb_table3_25_JZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 15, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_25_JZ2e( true) ;
         }
         else
         {
            wb_table3_25_JZ2e( false) ;
         }
      }

      protected void wb_table2_8_JZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdatahora_Internalname, "Data", "", "", lblTextblockdatahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatahora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatahora_Internalname, context.localUtil.Format(AV37DataHora, "99/99/99"), context.localUtil.Format( AV37DataHora, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatahora_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavDatahora_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_NovaNota.htm");
            GxWebStd.gx_bitmap( context, edtavDatahora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavDatahora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_NovaNota.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_codigo_Internalname, "Usu�rio", "", "", lblTextblockusuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUsuario_codigo, dynavUsuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0)), 1, dynavUsuario_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavUsuario_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_WP_NovaNota.htm");
            dynavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36Usuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavUsuario_codigo_Internalname, "Values", (String)(dynavUsuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknota_Internalname, "Nota", "", "", lblTextblocknota_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNota_Internalname, AV38Nota, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", 0, 1, 1, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_NovaNota.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_JZ2e( true) ;
         }
         else
         {
            wb_table2_8_JZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV46ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV46ContagemResultado_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJZ2( ) ;
         WSJZ2( ) ;
         WEJZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221184025");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_novanota.js", "?202031221184025");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockdatahora_Internalname = "TEXTBLOCKDATAHORA";
         edtavDatahora_Internalname = "vDATAHORA";
         lblTextblockusuario_codigo_Internalname = "TEXTBLOCKUSUARIO_CODIGO";
         dynavUsuario_codigo_Internalname = "vUSUARIO_CODIGO";
         lblTextblocknota_Internalname = "TEXTBLOCKNOTA";
         edtavNota_Internalname = "vNOTA";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavUsuario_codigo_Jsonclick = "";
         edtavDatahora_Jsonclick = "";
         edtavDatahora_Enabled = 1;
         bttBtnenter_Visible = 1;
         lblTbjava_Visible = 1;
         dynavUsuario_codigo.Enabled = 1;
         lblTbjava_Caption = "tbJava";
         Form.Caption = "Nota para a OS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV47ContagemResultado_StatusUltCnt',fld:'vCONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'AV34WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV34WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV37DataHora',fld:'vDATAHORA',pic:'',hsh:true,nv:''},{av:'AV36Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'dynavUsuario_codigo'},{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E12JZ2',iparms:[{av:'AV38Nota',fld:'vNOTA',pic:'',nv:''},{av:'AV42Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV41ContagemResultadoNotas',fld:'vCONTAGEMRESULTADONOTAS',pic:'',nv:null},{av:'AV36Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV41ContagemResultadoNotas',fld:'vCONTAGEMRESULTADONOTAS',pic:'',nv:null}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E13JZ2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV42Codigos = new GxSimpleCollection();
         AV41ContagemResultadoNotas = new SdtContagemResultadoNotas(context);
         AV34WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37DataHora = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00JZ2_A57Usuario_PessoaCod = new int[1] ;
         H00JZ2_A1Usuario_Codigo = new int[1] ;
         H00JZ2_A58Usuario_PessoaNom = new String[] {""} ;
         H00JZ2_n58Usuario_PessoaNom = new bool[] {false} ;
         H00JZ2_A54Usuario_Ativo = new bool[] {false} ;
         AV38Nota = "";
         AV43WebSession = context.GetSession();
         H00JZ4_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JZ4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JZ4_A456ContagemResultado_Codigo = new int[1] ;
         H00JZ4_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00JZ4_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00JZ4_A457ContagemResultado_Demanda = new String[] {""} ;
         H00JZ4_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00JZ4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         H00JZ4_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         A803ContagemResultado_ContratadaSigla = "";
         A457ContagemResultado_Demanda = "";
         AV45OS = new SdtContagemResultado(context);
         AV31StatusDmn = "";
         AV27PrazoEntrega = (DateTime)(DateTime.MinValue);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockdatahora_Jsonclick = "";
         lblTextblockusuario_codigo_Jsonclick = "";
         lblTextblocknota_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_novanota__default(),
            new Object[][] {
                new Object[] {
               H00JZ2_A57Usuario_PessoaCod, H00JZ2_A1Usuario_Codigo, H00JZ2_A58Usuario_PessoaNom, H00JZ2_n58Usuario_PessoaNom, H00JZ2_A54Usuario_Ativo
               }
               , new Object[] {
               H00JZ4_A490ContagemResultado_ContratadaCod, H00JZ4_n490ContagemResultado_ContratadaCod, H00JZ4_A456ContagemResultado_Codigo, H00JZ4_A803ContagemResultado_ContratadaSigla, H00JZ4_n803ContagemResultado_ContratadaSigla, H00JZ4_A457ContagemResultado_Demanda, H00JZ4_n457ContagemResultado_Demanda, H00JZ4_A531ContagemResultado_StatusUltCnt, H00JZ4_n531ContagemResultado_StatusUltCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDatahora_Enabled = 0;
         dynavUsuario_codigo.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short AV47ContagemResultado_StatusUltCnt ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short nGXWrapped ;
      private int AV46ContagemResultado_Codigo ;
      private int wcpOAV46ContagemResultado_Codigo ;
      private int edtavDatahora_Enabled ;
      private int gxdynajaxindex ;
      private int AV36Usuario_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int lblTbjava_Visible ;
      private int AV51GXV1 ;
      private int AV8Codigo ;
      private int bttBtnenter_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavDatahora_Internalname ;
      private String dynavUsuario_codigo_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavNota_Internalname ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String AV31StatusDmn ;
      private String bttBtnenter_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbjava_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String TempTags ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTextblockdatahora_Internalname ;
      private String lblTextblockdatahora_Jsonclick ;
      private String edtavDatahora_Jsonclick ;
      private String lblTextblockusuario_codigo_Internalname ;
      private String lblTextblockusuario_codigo_Jsonclick ;
      private String dynavUsuario_codigo_Jsonclick ;
      private String lblTextblocknota_Internalname ;
      private String lblTextblocknota_Jsonclick ;
      private DateTime AV27PrazoEntrega ;
      private DateTime AV37DataHora ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n457ContagemResultado_Demanda ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private String AV38Nota ;
      private String A457ContagemResultado_Demanda ;
      private IGxSession AV43WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavUsuario_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00JZ2_A57Usuario_PessoaCod ;
      private int[] H00JZ2_A1Usuario_Codigo ;
      private String[] H00JZ2_A58Usuario_PessoaNom ;
      private bool[] H00JZ2_n58Usuario_PessoaNom ;
      private bool[] H00JZ2_A54Usuario_Ativo ;
      private int[] H00JZ4_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JZ4_n490ContagemResultado_ContratadaCod ;
      private int[] H00JZ4_A456ContagemResultado_Codigo ;
      private String[] H00JZ4_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00JZ4_n803ContagemResultado_ContratadaSigla ;
      private String[] H00JZ4_A457ContagemResultado_Demanda ;
      private bool[] H00JZ4_n457ContagemResultado_Demanda ;
      private short[] H00JZ4_A531ContagemResultado_StatusUltCnt ;
      private bool[] H00JZ4_n531ContagemResultado_StatusUltCnt ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV42Codigos ;
      private SdtContagemResultadoNotas AV41ContagemResultadoNotas ;
      private SdtContagemResultado AV45OS ;
      private wwpbaseobjects.SdtWWPContext AV34WWPContext ;
   }

   public class wp_novanota__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JZ2 ;
          prmH00JZ2 = new Object[] {
          } ;
          Object[] prmH00JZ4 ;
          prmH00JZ4 = new Object[] {
          new Object[] {"@AV46ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JZ2", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Ativo] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Ativo] = 1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JZ2,0,0,true,false )
             ,new CursorDef("H00JZ4", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Demanda], COALESCE( T3.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV46ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JZ4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
