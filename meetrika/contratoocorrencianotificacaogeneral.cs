/*
               File: ContratoOcorrenciaNotificacaoGeneral
        Description: Contrato Ocorrencia Notificacao General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:6.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoocorrencianotificacaogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoocorrencianotificacaogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoocorrencianotificacaogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoOcorrenciaNotificacao_Codigo )
      {
         this.A297ContratoOcorrenciaNotificacao_Codigo = aP0_ContratoOcorrenciaNotificacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A297ContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A297ContratoOcorrenciaNotificacao_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA7E2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContratoOcorrenciaNotificacaoGeneral";
               context.Gx_err = 0;
               WS7E2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Ocorrencia Notificacao General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311720649");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoocorrencianotificacaogeneral.aspx") + "?" + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA", GetSecureSignedToken( sPrefix, A298ContratoOcorrenciaNotificacao_Data));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", GetSecureSignedToken( sPrefix, A301ContratoOcorrenciaNotificacao_Cumprido));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoOcorrenciaNotificacaoGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoocorrencianotificacaogeneral:[SendSecurityCheck value for]"+"ContratoOcorrenciaNotificacao_Responsavel:"+context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm7E2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoocorrencianotificacaogeneral.js", "?2020311720653");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoOcorrenciaNotificacaoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Ocorrencia Notificacao General" ;
      }

      protected void WB7E0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoocorrencianotificacaogeneral.aspx");
            }
            wb_table1_2_7E2( true) ;
         }
         else
         {
            wb_table1_2_7E2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7E2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START7E2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Ocorrencia Notificacao General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP7E0( ) ;
            }
         }
      }

      protected void WS7E2( )
      {
         START7E2( ) ;
         EVT7E2( ) ;
      }

      protected void EVT7E2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E117E2 */
                                    E117E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E127E2 */
                                    E127E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E137E2 */
                                    E137E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E147E2 */
                                    E147E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7E2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm7E2( ) ;
            }
         }
      }

      protected void PA7E2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7E2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoOcorrenciaNotificacaoGeneral";
         context.Gx_err = 0;
      }

      protected void RF7E2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H007E2 */
            pr_default.execute(0, new Object[] {A297ContratoOcorrenciaNotificacao_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A306ContratoOcorrenciaNotificacao_Docto = H007E2_A306ContratoOcorrenciaNotificacao_Docto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
               n306ContratoOcorrenciaNotificacao_Docto = H007E2_n306ContratoOcorrenciaNotificacao_Docto[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007E2_A304ContratoOcorrenciaNotificacao_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
               n304ContratoOcorrenciaNotificacao_Nome = H007E2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A303ContratoOcorrenciaNotificacao_Responsavel = H007E2_A303ContratoOcorrenciaNotificacao_Responsavel[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
               A302ContratoOcorrenciaNotificacao_Protocolo = H007E2_A302ContratoOcorrenciaNotificacao_Protocolo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A302ContratoOcorrenciaNotificacao_Protocolo", A302ContratoOcorrenciaNotificacao_Protocolo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
               n302ContratoOcorrenciaNotificacao_Protocolo = H007E2_n302ContratoOcorrenciaNotificacao_Protocolo[0];
               A301ContratoOcorrenciaNotificacao_Cumprido = H007E2_A301ContratoOcorrenciaNotificacao_Cumprido[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", GetSecureSignedToken( sPrefix, A301ContratoOcorrenciaNotificacao_Cumprido));
               n301ContratoOcorrenciaNotificacao_Cumprido = H007E2_n301ContratoOcorrenciaNotificacao_Cumprido[0];
               A300ContratoOcorrenciaNotificacao_Descricao = H007E2_A300ContratoOcorrenciaNotificacao_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A300ContratoOcorrenciaNotificacao_Descricao", A300ContratoOcorrenciaNotificacao_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
               A299ContratoOcorrenciaNotificacao_Prazo = H007E2_A299ContratoOcorrenciaNotificacao_Prazo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
               A298ContratoOcorrenciaNotificacao_Data = H007E2_A298ContratoOcorrenciaNotificacao_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA", GetSecureSignedToken( sPrefix, A298ContratoOcorrenciaNotificacao_Data));
               A77Contrato_Numero = H007E2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A74Contrato_Codigo = H007E2_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A294ContratoOcorrencia_Codigo = H007E2_A294ContratoOcorrencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
               A306ContratoOcorrenciaNotificacao_Docto = H007E2_A306ContratoOcorrenciaNotificacao_Docto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
               n306ContratoOcorrenciaNotificacao_Docto = H007E2_n306ContratoOcorrenciaNotificacao_Docto[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007E2_A304ContratoOcorrenciaNotificacao_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
               n304ContratoOcorrenciaNotificacao_Nome = H007E2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A74Contrato_Codigo = H007E2_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A77Contrato_Numero = H007E2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               /* Execute user event: E127E2 */
               E127E2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB7E0( ) ;
         }
      }

      protected void STRUP7E0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContratoOcorrenciaNotificacaoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E117E2 */
         E117E2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
            A298ContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Data_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A298ContratoOcorrenciaNotificacao_Data", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA", GetSecureSignedToken( sPrefix, A298ContratoOcorrenciaNotificacao_Data));
            A299ContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A299ContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
            A300ContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A300ContratoOcorrenciaNotificacao_Descricao", A300ContratoOcorrenciaNotificacao_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
            A301ContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Cumprido_Internalname), 0));
            n301ContratoOcorrenciaNotificacao_Cumprido = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A301ContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", GetSecureSignedToken( sPrefix, A301ContratoOcorrenciaNotificacao_Cumprido));
            A302ContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtContratoOcorrenciaNotificacao_Protocolo_Internalname);
            n302ContratoOcorrenciaNotificacao_Protocolo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A302ContratoOcorrenciaNotificacao_Protocolo", A302ContratoOcorrenciaNotificacao_Protocolo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
            A303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
            A304ContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Nome_Internalname));
            n304ContratoOcorrenciaNotificacao_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A304ContratoOcorrenciaNotificacao_Nome", A304ContratoOcorrenciaNotificacao_Nome);
            A306ContratoOcorrenciaNotificacao_Docto = cgiGet( edtContratoOcorrenciaNotificacao_Docto_Internalname);
            n306ContratoOcorrenciaNotificacao_Docto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A306ContratoOcorrenciaNotificacao_Docto", A306ContratoOcorrenciaNotificacao_Docto);
            /* Read saved values. */
            wcpOA297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA297ContratoOcorrenciaNotificacao_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoOcorrenciaNotificacaoGeneral";
            A303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A303ContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoocorrencianotificacaogeneral:[SecurityCheckFailed value for]"+"ContratoOcorrenciaNotificacao_Responsavel:"+context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E117E2 */
         E117E2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E117E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E127E2( )
      {
         /* Load Routine */
         edtContratoOcorrenciaNotificacao_Nome_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A303ContratoOcorrenciaNotificacao_Responsavel) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Nome_Internalname, "Link", edtContratoOcorrenciaNotificacao_Nome_Link);
      }

      protected void E137E2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E147E2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoOcorrenciaNotificacao";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoOcorrenciaNotificacao_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoOcorrenciaNotificacao_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_7E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7E2( true) ;
         }
         else
         {
            wb_table2_8_7E2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_71_7E2( true) ;
         }
         else
         {
            wb_table3_71_7E2( false) ;
         }
         return  ;
      }

      protected void wb_table3_71_7E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7E2e( true) ;
         }
         else
         {
            wb_table1_2_7E2e( false) ;
         }
      }

      protected void wb_table3_71_7E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_71_7E2e( true) ;
         }
         else
         {
            wb_table3_71_7E2e( false) ;
         }
      }

      protected void wb_table2_8_7E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_codigo_Internalname, "da Notifica��o", "", "", lblTextblockcontratoocorrencianotificacao_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencia_codigo_Internalname, "C�digo da Ocorr�ncia", "", "", lblTextblockcontratoocorrencia_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N�mero do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_data_Internalname, "de emiss�o", "", "", lblTextblockcontratoocorrencianotificacao_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoOcorrenciaNotificacao_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Data_Internalname, context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"), context.localUtil.Format( A298ContratoOcorrenciaNotificacao_Data, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratoOcorrenciaNotificacao_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_prazo_Internalname, "de cumprimento", "", "", lblTextblockcontratoocorrencianotificacao_prazo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Prazo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoocorrencianotificacao_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Descricao_Internalname, A300ContratoOcorrenciaNotificacao_Descricao, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_cumprido_Internalname, "de cumprimento", "", "", lblTextblockcontratoocorrencianotificacao_cumprido_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoOcorrenciaNotificacao_Cumprido_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Cumprido_Internalname, context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), context.localUtil.Format( A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratoOcorrenciaNotificacao_Cumprido_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_protocolo_Internalname, "de Protocolo", "", "", lblTextblockcontratoocorrencianotificacao_protocolo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Protocolo_Internalname, A302ContratoOcorrenciaNotificacao_Protocolo, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_responsavel_Internalname, "pessoa respons�vel", "", "", lblTextblockcontratoocorrencianotificacao_responsavel_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_nome_Internalname, "do respons�vek", "", "", lblTextblockcontratoocorrencianotificacao_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Nome_Internalname, StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome), StringUtil.RTrim( context.localUtil.Format( A304ContratoOcorrenciaNotificacao_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratoOcorrenciaNotificacao_Nome_Link, "", "", "", edtContratoOcorrenciaNotificacao_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoocorrencianotificacao_docto_Internalname, "do respons�vel", "", "", lblTextblockcontratoocorrencianotificacao_docto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrenciaNotificacao_Docto_Internalname, A306ContratoOcorrenciaNotificacao_Docto, StringUtil.RTrim( context.localUtil.Format( A306ContratoOcorrenciaNotificacao_Docto, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrenciaNotificacao_Docto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoOcorrenciaNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7E2e( true) ;
         }
         else
         {
            wb_table2_8_7E2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A297ContratoOcorrenciaNotificacao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7E2( ) ;
         WS7E2( ) ;
         WE7E2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA297ContratoOcorrenciaNotificacao_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA7E2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoocorrencianotificacaogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA7E2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A297ContratoOcorrenciaNotificacao_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         }
         wcpOA297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA297ContratoOcorrenciaNotificacao_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A297ContratoOcorrenciaNotificacao_Codigo != wcpOA297ContratoOcorrenciaNotificacao_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA297ContratoOcorrenciaNotificacao_Codigo = A297ContratoOcorrenciaNotificacao_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA297ContratoOcorrenciaNotificacao_Codigo = cgiGet( sPrefix+"A297ContratoOcorrenciaNotificacao_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA297ContratoOcorrenciaNotificacao_Codigo) > 0 )
         {
            A297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA297ContratoOcorrenciaNotificacao_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         }
         else
         {
            A297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A297ContratoOcorrenciaNotificacao_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA7E2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS7E2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS7E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A297ContratoOcorrenciaNotificacao_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA297ContratoOcorrenciaNotificacao_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A297ContratoOcorrenciaNotificacao_Codigo_CTRL", StringUtil.RTrim( sCtrlA297ContratoOcorrenciaNotificacao_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE7E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311720733");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoocorrencianotificacaogeneral.js", "?2020311720733");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoocorrencianotificacao_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         lblTextblockcontratoocorrencia_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIA_CODIGO";
         edtContratoOcorrencia_Codigo_Internalname = sPrefix+"CONTRATOOCORRENCIA_CODIGO";
         lblTextblockcontrato_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATO_CODIGO";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         lblTextblockcontrato_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO";
         lblTextblockcontratoocorrencianotificacao_data_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtContratoOcorrenciaNotificacao_Data_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DATA";
         lblTextblockcontratoocorrencianotificacao_prazo_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         lblTextblockcontratoocorrencianotificacao_descricao_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         lblTextblockcontratoocorrencianotificacao_cumprido_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         lblTextblockcontratoocorrencianotificacao_protocolo_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         lblTextblockcontratoocorrencianotificacao_responsavel_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         lblTextblockcontratoocorrencianotificacao_nome_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtContratoOcorrenciaNotificacao_Nome_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_NOME";
         lblTextblockcontratoocorrencianotificacao_docto_Internalname = sPrefix+"TEXTBLOCKCONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         edtContratoOcorrenciaNotificacao_Docto_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoOcorrenciaNotificacao_Docto_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Nome_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Descricao_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Prazo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Data_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContratoOcorrencia_Codigo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Codigo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoOcorrenciaNotificacao_Nome_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E137E2',iparms:[{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E147E2',iparms:[{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H007E2_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         H007E2_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         H007E2_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         H007E2_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         H007E2_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         H007E2_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         H007E2_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         H007E2_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         H007E2_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         H007E2_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         H007E2_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         H007E2_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         H007E2_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         H007E2_A77Contrato_Numero = new String[] {""} ;
         H007E2_A74Contrato_Codigo = new int[1] ;
         H007E2_A294ContratoOcorrencia_Codigo = new int[1] ;
         A306ContratoOcorrenciaNotificacao_Docto = "";
         A304ContratoOcorrenciaNotificacao_Nome = "";
         A77Contrato_Numero = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_codigo_Jsonclick = "";
         lblTextblockcontratoocorrencia_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_data_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_prazo_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_descricao_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_cumprido_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_protocolo_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_responsavel_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_nome_Jsonclick = "";
         lblTextblockcontratoocorrencianotificacao_docto_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA297ContratoOcorrenciaNotificacao_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoocorrencianotificacaogeneral__default(),
            new Object[][] {
                new Object[] {
               H007E2_A297ContratoOcorrenciaNotificacao_Codigo, H007E2_A306ContratoOcorrenciaNotificacao_Docto, H007E2_n306ContratoOcorrenciaNotificacao_Docto, H007E2_A304ContratoOcorrenciaNotificacao_Nome, H007E2_n304ContratoOcorrenciaNotificacao_Nome, H007E2_A303ContratoOcorrenciaNotificacao_Responsavel, H007E2_A302ContratoOcorrenciaNotificacao_Protocolo, H007E2_n302ContratoOcorrenciaNotificacao_Protocolo, H007E2_A301ContratoOcorrenciaNotificacao_Cumprido, H007E2_n301ContratoOcorrenciaNotificacao_Cumprido,
               H007E2_A300ContratoOcorrenciaNotificacao_Descricao, H007E2_A299ContratoOcorrenciaNotificacao_Prazo, H007E2_A298ContratoOcorrenciaNotificacao_Data, H007E2_A77Contrato_Numero, H007E2_A74Contrato_Codigo, H007E2_A294ContratoOcorrencia_Codigo
               }
            }
         );
         AV14Pgmname = "ContratoOcorrenciaNotificacaoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoOcorrenciaNotificacaoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int wcpOA297ContratoOcorrenciaNotificacao_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int A74Contrato_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoOcorrenciaNotificacao_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String A77Contrato_Numero ;
      private String edtContratoOcorrencia_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Data_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Nome_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Docto_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Nome_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_codigo_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_codigo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Jsonclick ;
      private String lblTextblockcontratoocorrencia_codigo_Internalname ;
      private String lblTextblockcontratoocorrencia_codigo_Jsonclick ;
      private String edtContratoOcorrencia_Codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_data_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_data_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Data_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_prazo_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_prazo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_descricao_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_descricao_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_cumprido_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_cumprido_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_protocolo_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_protocolo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_responsavel_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_responsavel_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_nome_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_nome_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Nome_Jsonclick ;
      private String lblTextblockcontratoocorrencianotificacao_docto_Internalname ;
      private String lblTextblockcontratoocorrencianotificacao_docto_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Docto_Jsonclick ;
      private String sCtrlA297ContratoOcorrenciaNotificacao_Codigo ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n306ContratoOcorrenciaNotificacao_Docto ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool returnInSub ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A306ContratoOcorrenciaNotificacao_Docto ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H007E2_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] H007E2_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] H007E2_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] H007E2_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] H007E2_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] H007E2_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] H007E2_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] H007E2_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] H007E2_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] H007E2_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] H007E2_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] H007E2_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] H007E2_A298ContratoOcorrenciaNotificacao_Data ;
      private String[] H007E2_A77Contrato_Numero ;
      private int[] H007E2_A74Contrato_Codigo ;
      private int[] H007E2_A294ContratoOcorrencia_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoocorrencianotificacaogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007E2 ;
          prmH007E2 = new Object[] {
          new Object[] {"@ContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007E2", "SELECT T1.[ContratoOcorrenciaNotificacao_Codigo], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T1.[ContratoOcorrenciaNotificacao_Codigo] = @ContratoOcorrenciaNotificacao_Codigo ORDER BY T1.[ContratoOcorrenciaNotificacao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007E2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 20) ;
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
