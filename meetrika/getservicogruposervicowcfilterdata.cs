/*
               File: GetServicoGrupoServicoWCFilterData
        Description: Get Servico Grupo Servico WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:36.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getservicogruposervicowcfilterdata : GXProcedure
   {
      public getservicogruposervicowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getservicogruposervicowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getservicogruposervicowcfilterdata objgetservicogruposervicowcfilterdata;
         objgetservicogruposervicowcfilterdata = new getservicogruposervicowcfilterdata();
         objgetservicogruposervicowcfilterdata.AV17DDOName = aP0_DDOName;
         objgetservicogruposervicowcfilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetservicogruposervicowcfilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetservicogruposervicowcfilterdata.AV21OptionsJson = "" ;
         objgetservicogruposervicowcfilterdata.AV24OptionsDescJson = "" ;
         objgetservicogruposervicowcfilterdata.AV26OptionIndexesJson = "" ;
         objgetservicogruposervicowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetservicogruposervicowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetservicogruposervicowcfilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getservicogruposervicowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_SERVICO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("ServicoGrupoServicoWCGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ServicoGrupoServicoWCGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("ServicoGrupoServicoWCGridState"), "");
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "SERVICO_TIPOHIERARQUIA") == 0 )
            {
               AV33Servico_TipoHierarquia = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV10TFServico_Nome = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV11TFServico_Nome_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA") == 0 )
            {
               AV12TFServico_Sigla = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA_SEL") == 0 )
            {
               AV13TFServico_Sigla_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFSERVICO_ATIVO_SEL") == 0 )
            {
               AV14TFServico_Ativo_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "PARM_&SERVICOGRUPO_CODIGO") == 0 )
            {
               AV37ServicoGrupo_Codigo = (int)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV35Servico_Nome1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 )
            {
               AV36Servico_IsPublico1 = BooleanUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value);
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV10TFServico_Nome = AV15SearchTxt;
         AV11TFServico_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35Servico_Nome1 ,
                                              AV36Servico_IsPublico1 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServico_Sigla_Sel ,
                                              AV12TFServico_Sigla ,
                                              AV14TFServico_Ativo_Sel ,
                                              A608Servico_Nome ,
                                              A1635Servico_IsPublico ,
                                              A605Servico_Sigla ,
                                              A632Servico_Ativo ,
                                              A1530Servico_TipoHierarquia ,
                                              AV37ServicoGrupo_Codigo ,
                                              A157ServicoGrupo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV35Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV35Servico_Nome1), 50, "%");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFServico_Sigla), 15, "%");
         /* Using cursor P00UM2 */
         pr_default.execute(0, new Object[] {AV37ServicoGrupo_Codigo, lV35Servico_Nome1, AV36Servico_IsPublico1, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServico_Sigla, AV13TFServico_Sigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUM2 = false;
            A157ServicoGrupo_Codigo = P00UM2_A157ServicoGrupo_Codigo[0];
            A1530Servico_TipoHierarquia = P00UM2_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00UM2_n1530Servico_TipoHierarquia[0];
            A608Servico_Nome = P00UM2_A608Servico_Nome[0];
            A632Servico_Ativo = P00UM2_A632Servico_Ativo[0];
            A605Servico_Sigla = P00UM2_A605Servico_Sigla[0];
            A1635Servico_IsPublico = P00UM2_A1635Servico_IsPublico[0];
            A155Servico_Codigo = P00UM2_A155Servico_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UM2_A157ServicoGrupo_Codigo[0] == A157ServicoGrupo_Codigo ) && ( StringUtil.StrCmp(P00UM2_A608Servico_Nome[0], A608Servico_Nome) == 0 ) )
            {
               BRKUM2 = false;
               A155Servico_Codigo = P00UM2_A155Servico_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKUM2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV19Option = A608Servico_Nome;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUM2 )
            {
               BRKUM2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICO_SIGLAOPTIONS' Routine */
         AV12TFServico_Sigla = AV15SearchTxt;
         AV13TFServico_Sigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35Servico_Nome1 ,
                                              AV36Servico_IsPublico1 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServico_Sigla_Sel ,
                                              AV12TFServico_Sigla ,
                                              AV14TFServico_Ativo_Sel ,
                                              A608Servico_Nome ,
                                              A1635Servico_IsPublico ,
                                              A605Servico_Sigla ,
                                              A632Servico_Ativo ,
                                              A1530Servico_TipoHierarquia ,
                                              AV37ServicoGrupo_Codigo ,
                                              A157ServicoGrupo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV35Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV35Servico_Nome1), 50, "%");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFServico_Sigla), 15, "%");
         /* Using cursor P00UM3 */
         pr_default.execute(1, new Object[] {AV37ServicoGrupo_Codigo, lV35Servico_Nome1, AV36Servico_IsPublico1, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServico_Sigla, AV13TFServico_Sigla_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUM4 = false;
            A157ServicoGrupo_Codigo = P00UM3_A157ServicoGrupo_Codigo[0];
            A1530Servico_TipoHierarquia = P00UM3_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00UM3_n1530Servico_TipoHierarquia[0];
            A605Servico_Sigla = P00UM3_A605Servico_Sigla[0];
            A632Servico_Ativo = P00UM3_A632Servico_Ativo[0];
            A1635Servico_IsPublico = P00UM3_A1635Servico_IsPublico[0];
            A608Servico_Nome = P00UM3_A608Servico_Nome[0];
            A155Servico_Codigo = P00UM3_A155Servico_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UM3_A157ServicoGrupo_Codigo[0] == A157ServicoGrupo_Codigo ) && ( StringUtil.StrCmp(P00UM3_A605Servico_Sigla[0], A605Servico_Sigla) == 0 ) )
            {
               BRKUM4 = false;
               A155Servico_Codigo = P00UM3_A155Servico_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKUM4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
            {
               AV19Option = A605Servico_Sigla;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUM4 )
            {
               BRKUM4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV33Servico_TipoHierarquia = 1;
         AV10TFServico_Nome = "";
         AV11TFServico_Nome_Sel = "";
         AV12TFServico_Sigla = "";
         AV13TFServico_Sigla_Sel = "";
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35Servico_Nome1 = "";
         AV36Servico_IsPublico1 = false;
         scmdbuf = "";
         lV35Servico_Nome1 = "";
         lV10TFServico_Nome = "";
         lV12TFServico_Sigla = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         P00UM2_A157ServicoGrupo_Codigo = new int[1] ;
         P00UM2_A1530Servico_TipoHierarquia = new short[1] ;
         P00UM2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         P00UM2_A608Servico_Nome = new String[] {""} ;
         P00UM2_A632Servico_Ativo = new bool[] {false} ;
         P00UM2_A605Servico_Sigla = new String[] {""} ;
         P00UM2_A1635Servico_IsPublico = new bool[] {false} ;
         P00UM2_A155Servico_Codigo = new int[1] ;
         AV19Option = "";
         P00UM3_A157ServicoGrupo_Codigo = new int[1] ;
         P00UM3_A1530Servico_TipoHierarquia = new short[1] ;
         P00UM3_n1530Servico_TipoHierarquia = new bool[] {false} ;
         P00UM3_A605Servico_Sigla = new String[] {""} ;
         P00UM3_A632Servico_Ativo = new bool[] {false} ;
         P00UM3_A1635Servico_IsPublico = new bool[] {false} ;
         P00UM3_A608Servico_Nome = new String[] {""} ;
         P00UM3_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getservicogruposervicowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UM2_A157ServicoGrupo_Codigo, P00UM2_A1530Servico_TipoHierarquia, P00UM2_n1530Servico_TipoHierarquia, P00UM2_A608Servico_Nome, P00UM2_A632Servico_Ativo, P00UM2_A605Servico_Sigla, P00UM2_A1635Servico_IsPublico, P00UM2_A155Servico_Codigo
               }
               , new Object[] {
               P00UM3_A157ServicoGrupo_Codigo, P00UM3_A1530Servico_TipoHierarquia, P00UM3_n1530Servico_TipoHierarquia, P00UM3_A605Servico_Sigla, P00UM3_A632Servico_Ativo, P00UM3_A1635Servico_IsPublico, P00UM3_A608Servico_Nome, P00UM3_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33Servico_TipoHierarquia ;
      private short AV14TFServico_Ativo_Sel ;
      private short A1530Servico_TipoHierarquia ;
      private int AV40GXV1 ;
      private int AV37ServicoGrupo_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private long AV27count ;
      private String AV10TFServico_Nome ;
      private String AV11TFServico_Nome_Sel ;
      private String AV12TFServico_Sigla ;
      private String AV13TFServico_Sigla_Sel ;
      private String AV35Servico_Nome1 ;
      private String scmdbuf ;
      private String lV35Servico_Nome1 ;
      private String lV10TFServico_Nome ;
      private String lV12TFServico_Sigla ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private bool returnInSub ;
      private bool AV36Servico_IsPublico1 ;
      private bool A1635Servico_IsPublico ;
      private bool A632Servico_Ativo ;
      private bool BRKUM2 ;
      private bool n1530Servico_TipoHierarquia ;
      private bool BRKUM4 ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UM2_A157ServicoGrupo_Codigo ;
      private short[] P00UM2_A1530Servico_TipoHierarquia ;
      private bool[] P00UM2_n1530Servico_TipoHierarquia ;
      private String[] P00UM2_A608Servico_Nome ;
      private bool[] P00UM2_A632Servico_Ativo ;
      private String[] P00UM2_A605Servico_Sigla ;
      private bool[] P00UM2_A1635Servico_IsPublico ;
      private int[] P00UM2_A155Servico_Codigo ;
      private int[] P00UM3_A157ServicoGrupo_Codigo ;
      private short[] P00UM3_A1530Servico_TipoHierarquia ;
      private bool[] P00UM3_n1530Servico_TipoHierarquia ;
      private String[] P00UM3_A605Servico_Sigla ;
      private bool[] P00UM3_A632Servico_Ativo ;
      private bool[] P00UM3_A1635Servico_IsPublico ;
      private String[] P00UM3_A608Servico_Nome ;
      private int[] P00UM3_A155Servico_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getservicogruposervicowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UM2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35Servico_Nome1 ,
                                             bool AV36Servico_IsPublico1 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServico_Sigla_Sel ,
                                             String AV12TFServico_Sigla ,
                                             short AV14TFServico_Ativo_Sel ,
                                             String A608Servico_Nome ,
                                             bool A1635Servico_IsPublico ,
                                             String A605Servico_Sigla ,
                                             bool A632Servico_Ativo ,
                                             short A1530Servico_TipoHierarquia ,
                                             int AV37ServicoGrupo_Codigo ,
                                             int A157ServicoGrupo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ServicoGrupo_Codigo], [Servico_TipoHierarquia], [Servico_Nome], [Servico_Ativo], [Servico_Sigla], [Servico_IsPublico], [Servico_Codigo] FROM [Servico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoGrupo_Codigo] = @AV37ServicoGrupo_Codigo)";
         scmdbuf = scmdbuf + " and ([Servico_TipoHierarquia] = 1)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like '%' + @lV35Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 ) && ( ! (false==AV36Servico_IsPublico1) ) )
         {
            sWhereString = sWhereString + " and ([Servico_IsPublico] = @AV36Servico_IsPublico1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] like @lV12TFServico_Sigla)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] = @AV13TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV14TFServico_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 1)";
         }
         if ( AV14TFServico_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ServicoGrupo_Codigo], [Servico_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UM3( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35Servico_Nome1 ,
                                             bool AV36Servico_IsPublico1 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServico_Sigla_Sel ,
                                             String AV12TFServico_Sigla ,
                                             short AV14TFServico_Ativo_Sel ,
                                             String A608Servico_Nome ,
                                             bool A1635Servico_IsPublico ,
                                             String A605Servico_Sigla ,
                                             bool A632Servico_Ativo ,
                                             short A1530Servico_TipoHierarquia ,
                                             int AV37ServicoGrupo_Codigo ,
                                             int A157ServicoGrupo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ServicoGrupo_Codigo], [Servico_TipoHierarquia], [Servico_Sigla], [Servico_Ativo], [Servico_IsPublico], [Servico_Nome], [Servico_Codigo] FROM [Servico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoGrupo_Codigo] = @AV37ServicoGrupo_Codigo)";
         scmdbuf = scmdbuf + " and ([Servico_TipoHierarquia] = 1)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like '%' + @lV35Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SERVICO_ISPUBLICO") == 0 ) && ( ! (false==AV36Servico_IsPublico1) ) )
         {
            sWhereString = sWhereString + " and ([Servico_IsPublico] = @AV36Servico_IsPublico1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] like @lV12TFServico_Sigla)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Servico_Sigla] = @AV13TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV14TFServico_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 1)";
         }
         if ( AV14TFServico_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([Servico_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ServicoGrupo_Codigo], [Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UM2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (short)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
               case 1 :
                     return conditional_P00UM3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (short)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UM2 ;
          prmP00UM2 = new Object[] {
          new Object[] {"@AV37ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36Servico_IsPublico1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFServico_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00UM3 ;
          prmP00UM3 = new Object[] {
          new Object[] {"@AV37ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36Servico_IsPublico1",SqlDbType.Bit,4,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFServico_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UM2,100,0,true,false )
             ,new CursorDef("P00UM3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UM3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getservicogruposervicowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getservicogruposervicowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getservicogruposervicowcfilterdata") )
          {
             return  ;
          }
          getservicogruposervicowcfilterdata worker = new getservicogruposervicowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
