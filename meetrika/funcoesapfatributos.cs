/*
               File: FuncoesAPFAtributos
        Description: Atributos da Fun��o de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:26.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcoesapfatributos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A364FuncaoAPFAtributos_AtributosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A366FuncaoAPFAtributos_AtrTabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n366FuncaoAPFAtributos_AtrTabelaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A366FuncaoAPFAtributos_AtrTabelaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A378FuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n378FuncaoAPFAtributos_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A378FuncaoAPFAtributos_FuncaoDadosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A748FuncaoAPFAtributos_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n748FuncaoAPFAtributos_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A748FuncaoAPFAtributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A748FuncaoAPFAtributos_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
               AV8FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoAPFAtributos_AtributosCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkFuncoesAPFAtributos_Regra.Name = "FUNCOESAPFATRIBUTOS_REGRA";
         chkFuncoesAPFAtributos_Regra.WebTags = "";
         chkFuncoesAPFAtributos_Regra.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncoesAPFAtributos_Regra_Internalname, "TitleCaption", chkFuncoesAPFAtributos_Regra.Caption);
         chkFuncoesAPFAtributos_Regra.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Atributos da Fun��o de Transa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcoesapfatributos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcoesapfatributos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_FuncaoAPF_Codigo ,
                           int aP2_FuncaoAPFAtributos_AtributosCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7FuncaoAPF_Codigo = aP1_FuncaoAPF_Codigo;
         this.AV8FuncaoAPFAtributos_AtributosCod = aP2_FuncaoAPFAtributos_AtributosCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkFuncoesAPFAtributos_Regra = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1058( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1058e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPF_Codigo_Visible, edtFuncaoAPF_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtributosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtributosCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPFAtributos_AtributosCod_Visible, edtFuncaoAPFAtributos_AtributosCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPFAtributos_FuncaoDadosCod_Visible, edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributos.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtrTabelaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0, ",", "")), ((edtFuncaoAPFAtributos_AtrTabelaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPFAtributos_AtrTabelaCod_Visible, edtFuncaoAPFAtributos_AtrTabelaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributos.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1058( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1058( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1058e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_51_1058( true) ;
         }
         return  ;
      }

      protected void wb_table3_51_1058e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1058e( true) ;
         }
         else
         {
            wb_table1_2_1058e( false) ;
         }
      }

      protected void wb_table3_51_1058( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_51_1058e( true) ;
         }
         else
         {
            wb_table3_51_1058e( false) ;
         }
      }

      protected void wb_table2_5_1058( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1058( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1058e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1058e( true) ;
         }
         else
         {
            wb_table2_5_1058e( false) ;
         }
      }

      protected void wb_table4_13_1058( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_nome_Internalname, "Fun��o de Transa��o", "", "", lblTextblockfuncaoapf_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, edtFuncaoAPF_Nome_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_atributosnom_Internalname, "Atributo", "", "", lblTextblockfuncaoapfatributos_atributosnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtributosNom_Internalname, StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom), StringUtil.RTrim( context.localUtil.Format( A365FuncaoAPFAtributos_AtributosNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtributosNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPFAtributos_AtributosNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_atrtabelanom_Internalname, "Tabela", "", "", lblTextblockfuncaoapfatributos_atrtabelanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtrTabelaNom_Internalname, StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom), StringUtil.RTrim( context.localUtil.Format( A367FuncaoAPFAtributos_AtrTabelaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPFAtributos_AtrTabelaNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_regra_Internalname, "Regra de Neg�cio", "", "", lblTextblockfuncoesapfatributos_regra_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncoesAPFAtributos_Regra_Internalname, StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra), "", "", 1, chkFuncoesAPFAtributos_Regra.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(33, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_code_Internalname, "C�digo", "", "", lblTextblockfuncoesapfatributos_code_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncoesAPFAtributos_Code_Internalname, A383FuncoesAPFAtributos_Code, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncoesAPFAtributos_Code_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncoesAPFAtributos_Code_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_nome_Internalname, "Nome", "", "", lblTextblockfuncoesapfatributos_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncoesAPFAtributos_Nome_Internalname, StringUtil.RTrim( A384FuncoesAPFAtributos_Nome), StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncoesAPFAtributos_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncoesAPFAtributos_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncoesapfatributos_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncoesAPFAtributos_Descricao_Internalname, A385FuncoesAPFAtributos_Descricao, StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncoesAPFAtributos_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncoesAPFAtributos_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_FuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1058e( true) ;
         }
         else
         {
            wb_table4_13_1058e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11102 */
         E11102 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A365FuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtributosNom_Internalname));
               n365FuncaoAPFAtributos_AtributosNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
               A367FuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtrTabelaNom_Internalname));
               n367FuncaoAPFAtributos_AtrTabelaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
               A389FuncoesAPFAtributos_Regra = StringUtil.StrToBool( cgiGet( chkFuncoesAPFAtributos_Regra_Internalname));
               n389FuncoesAPFAtributos_Regra = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
               n389FuncoesAPFAtributos_Regra = ((false==A389FuncoesAPFAtributos_Regra) ? true : false);
               A383FuncoesAPFAtributos_Code = cgiGet( edtFuncoesAPFAtributos_Code_Internalname);
               n383FuncoesAPFAtributos_Code = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
               n383FuncoesAPFAtributos_Code = (String.IsNullOrEmpty(StringUtil.RTrim( A383FuncoesAPFAtributos_Code)) ? true : false);
               A384FuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Nome_Internalname));
               n384FuncoesAPFAtributos_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
               n384FuncoesAPFAtributos_Nome = (String.IsNullOrEmpty(StringUtil.RTrim( A384FuncoesAPFAtributos_Nome)) ? true : false);
               A385FuncoesAPFAtributos_Descricao = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Descricao_Internalname));
               n385FuncoesAPFAtributos_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A385FuncoesAPFAtributos_Descricao", A385FuncoesAPFAtributos_Descricao);
               n385FuncoesAPFAtributos_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A385FuncoesAPFAtributos_Descricao)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A165FuncaoAPF_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               }
               else
               {
                  A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFAtributos_AtributosCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A364FuncaoAPFAtributos_AtributosCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
               }
               else
               {
                  A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A378FuncaoAPFAtributos_FuncaoDadosCod = 0;
                  n378FuncaoAPFAtributos_FuncaoDadosCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
               }
               else
               {
                  A378FuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname), ",", "."));
                  n378FuncaoAPFAtributos_FuncaoDadosCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
               }
               n378FuncaoAPFAtributos_FuncaoDadosCod = ((0==A378FuncaoAPFAtributos_FuncaoDadosCod) ? true : false);
               A366FuncaoAPFAtributos_AtrTabelaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtrTabelaCod_Internalname), ",", "."));
               n366FuncaoAPFAtributos_AtrTabelaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
               /* Read saved values. */
               Z165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z165FuncaoAPF_Codigo"), ",", "."));
               Z364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( "Z364FuncaoAPFAtributos_AtributosCod"), ",", "."));
               Z389FuncoesAPFAtributos_Regra = StringUtil.StrToBool( cgiGet( "Z389FuncoesAPFAtributos_Regra"));
               n389FuncoesAPFAtributos_Regra = ((false==A389FuncoesAPFAtributos_Regra) ? true : false);
               Z383FuncoesAPFAtributos_Code = cgiGet( "Z383FuncoesAPFAtributos_Code");
               n383FuncoesAPFAtributos_Code = (String.IsNullOrEmpty(StringUtil.RTrim( A383FuncoesAPFAtributos_Code)) ? true : false);
               Z384FuncoesAPFAtributos_Nome = cgiGet( "Z384FuncoesAPFAtributos_Nome");
               n384FuncoesAPFAtributos_Nome = (String.IsNullOrEmpty(StringUtil.RTrim( A384FuncoesAPFAtributos_Nome)) ? true : false);
               Z385FuncoesAPFAtributos_Descricao = cgiGet( "Z385FuncoesAPFAtributos_Descricao");
               n385FuncoesAPFAtributos_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A385FuncoesAPFAtributos_Descricao)) ? true : false);
               Z751FuncaoAPFAtributos_Ativo = StringUtil.StrToBool( cgiGet( "Z751FuncaoAPFAtributos_Ativo"));
               n751FuncaoAPFAtributos_Ativo = ((false==A751FuncaoAPFAtributos_Ativo) ? true : false);
               Z748FuncaoAPFAtributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z748FuncaoAPFAtributos_MelhoraCod"), ",", "."));
               n748FuncaoAPFAtributos_MelhoraCod = ((0==A748FuncaoAPFAtributos_MelhoraCod) ? true : false);
               Z378FuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "Z378FuncaoAPFAtributos_FuncaoDadosCod"), ",", "."));
               n378FuncaoAPFAtributos_FuncaoDadosCod = ((0==A378FuncaoAPFAtributos_FuncaoDadosCod) ? true : false);
               A751FuncaoAPFAtributos_Ativo = StringUtil.StrToBool( cgiGet( "Z751FuncaoAPFAtributos_Ativo"));
               n751FuncaoAPFAtributos_Ativo = false;
               n751FuncaoAPFAtributos_Ativo = ((false==A751FuncaoAPFAtributos_Ativo) ? true : false);
               A748FuncaoAPFAtributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z748FuncaoAPFAtributos_MelhoraCod"), ",", "."));
               n748FuncaoAPFAtributos_MelhoraCod = false;
               n748FuncaoAPFAtributos_MelhoraCod = ((0==A748FuncaoAPFAtributos_MelhoraCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N378FuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "N378FuncaoAPFAtributos_FuncaoDadosCod"), ",", "."));
               n378FuncaoAPFAtributos_FuncaoDadosCod = ((0==A378FuncaoAPFAtributos_FuncaoDadosCod) ? true : false);
               N748FuncaoAPFAtributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "N748FuncaoAPFAtributos_MelhoraCod"), ",", "."));
               n748FuncaoAPFAtributos_MelhoraCod = ((0==A748FuncaoAPFAtributos_MelhoraCod) ? true : false);
               AV7FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFUNCAOAPF_CODIGO"), ",", "."));
               AV8FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( "vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD"), ",", "."));
               AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD"), ",", "."));
               AV14Insert_FuncaoAPFAtributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPFATRIBUTOS_MELHORACOD"), ",", "."));
               A748FuncaoAPFAtributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPFATRIBUTOS_MELHORACOD"), ",", "."));
               n748FuncaoAPFAtributos_MelhoraCod = ((0==A748FuncaoAPFAtributos_MelhoraCod) ? true : false);
               A751FuncaoAPFAtributos_Ativo = StringUtil.StrToBool( cgiGet( "FUNCAOAPFATRIBUTOS_ATIVO"));
               n751FuncaoAPFAtributos_Ativo = ((false==A751FuncaoAPFAtributos_Ativo) ? true : false);
               A412FuncaoAPFAtributos_FuncaoDadosNom = cgiGet( "FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM");
               n412FuncaoAPFAtributos_FuncaoDadosNom = false;
               A415FuncaoAPFAtributos_FuncaoDadosTip = cgiGet( "FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP");
               n415FuncaoAPFAtributos_FuncaoDadosTip = false;
               A393FuncaoAPFAtributos_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPFATRIBUTOS_SISTEMACOD"), ",", "."));
               n393FuncaoAPFAtributos_SistemaCod = false;
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "FuncoesAPFAtributos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A751FuncaoAPFAtributos_Ativo);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("funcoesapfatributos:[SecurityCheckFailed value for]"+"FuncaoAPFAtributos_MelhoraCod:"+context.localUtil.Format( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), "ZZZZZ9"));
                  GXUtil.WriteLog("funcoesapfatributos:[SecurityCheckFailed value for]"+"FuncaoAPFAtributos_Ativo:"+StringUtil.BoolToStr( A751FuncaoAPFAtributos_Ativo));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode58 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode58;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound58 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_100( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FUNCAOAPF_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11102 */
                           E11102 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12102 */
                           E12102 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12102 */
            E12102 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1058( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1058( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_100( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1058( ) ;
            }
            else
            {
               CheckExtendedTable1058( ) ;
               CloseExtendedTableCursors1058( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption100( )
      {
      }

      protected void E11102( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "FuncaoAPFAtributos_FuncaoDadosCod") == 0 )
               {
                  AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "FuncaoAPFAtributos_MelhoraCod") == 0 )
               {
                  AV14Insert_FuncaoAPFAtributos_MelhoraCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_FuncaoAPFAtributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_FuncaoAPFAtributos_MelhoraCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtFuncaoAPF_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Visible), 5, 0)));
         edtFuncaoAPFAtributos_AtributosCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Visible), 5, 0)));
         edtFuncaoAPFAtributos_FuncaoDadosCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_FuncaoDadosCod_Visible), 5, 0)));
         edtFuncaoAPFAtributos_AtrTabelaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtrTabelaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtrTabelaCod_Visible), 5, 0)));
      }

      protected void E12102( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwfuncoesapfatributos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1058( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z389FuncoesAPFAtributos_Regra = T00103_A389FuncoesAPFAtributos_Regra[0];
               Z383FuncoesAPFAtributos_Code = T00103_A383FuncoesAPFAtributos_Code[0];
               Z384FuncoesAPFAtributos_Nome = T00103_A384FuncoesAPFAtributos_Nome[0];
               Z385FuncoesAPFAtributos_Descricao = T00103_A385FuncoesAPFAtributos_Descricao[0];
               Z751FuncaoAPFAtributos_Ativo = T00103_A751FuncaoAPFAtributos_Ativo[0];
               Z748FuncaoAPFAtributos_MelhoraCod = T00103_A748FuncaoAPFAtributos_MelhoraCod[0];
               Z378FuncaoAPFAtributos_FuncaoDadosCod = T00103_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            }
            else
            {
               Z389FuncoesAPFAtributos_Regra = A389FuncoesAPFAtributos_Regra;
               Z383FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
               Z384FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
               Z385FuncoesAPFAtributos_Descricao = A385FuncoesAPFAtributos_Descricao;
               Z751FuncaoAPFAtributos_Ativo = A751FuncaoAPFAtributos_Ativo;
               Z748FuncaoAPFAtributos_MelhoraCod = A748FuncaoAPFAtributos_MelhoraCod;
               Z378FuncaoAPFAtributos_FuncaoDadosCod = A378FuncaoAPFAtributos_FuncaoDadosCod;
            }
         }
         if ( GX_JID == -14 )
         {
            Z389FuncoesAPFAtributos_Regra = A389FuncoesAPFAtributos_Regra;
            Z383FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
            Z384FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
            Z385FuncoesAPFAtributos_Descricao = A385FuncoesAPFAtributos_Descricao;
            Z751FuncaoAPFAtributos_Ativo = A751FuncaoAPFAtributos_Ativo;
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
            Z748FuncaoAPFAtributos_MelhoraCod = A748FuncaoAPFAtributos_MelhoraCod;
            Z378FuncaoAPFAtributos_FuncaoDadosCod = A378FuncaoAPFAtributos_FuncaoDadosCod;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            Z365FuncaoAPFAtributos_AtributosNom = A365FuncaoAPFAtributos_AtributosNom;
            Z366FuncaoAPFAtributos_AtrTabelaCod = A366FuncaoAPFAtributos_AtrTabelaCod;
            Z367FuncaoAPFAtributos_AtrTabelaNom = A367FuncaoAPFAtributos_AtrTabelaNom;
            Z393FuncaoAPFAtributos_SistemaCod = A393FuncaoAPFAtributos_SistemaCod;
            Z412FuncaoAPFAtributos_FuncaoDadosNom = A412FuncaoAPFAtributos_FuncaoDadosNom;
            Z415FuncaoAPFAtributos_FuncaoDadosTip = A415FuncaoAPFAtributos_FuncaoDadosTip;
         }
      }

      protected void standaloneNotModal( )
      {
         AV15Pgmname = "FuncoesAPFAtributos";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7FuncaoAPF_Codigo) )
         {
            A165FuncaoAPF_Codigo = AV7FuncaoAPF_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         if ( ! (0==AV7FuncaoAPF_Codigo) )
         {
            edtFuncaoAPF_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtFuncaoAPF_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7FuncaoAPF_Codigo) )
         {
            edtFuncaoAPF_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8FuncaoAPFAtributos_AtributosCod) )
         {
            A364FuncaoAPFAtributos_AtributosCod = AV8FuncaoAPFAtributos_AtributosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
         if ( ! (0==AV8FuncaoAPFAtributos_AtributosCod) )
         {
            edtFuncaoAPFAtributos_AtributosCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Enabled), 5, 0)));
         }
         else
         {
            edtFuncaoAPFAtributos_AtributosCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV8FuncaoAPFAtributos_AtributosCod) )
         {
            edtFuncaoAPFAtributos_AtributosCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod) )
         {
            edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled), 5, 0)));
         }
         else
         {
            edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_FuncaoAPFAtributos_MelhoraCod) )
         {
            A748FuncaoAPFAtributos_MelhoraCod = AV14Insert_FuncaoAPFAtributos_MelhoraCod;
            n748FuncaoAPFAtributos_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A748FuncaoAPFAtributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod) )
         {
            A378FuncaoAPFAtributos_FuncaoDadosCod = AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod;
            n378FuncaoAPFAtributos_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00104 */
            pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
            A166FuncaoAPF_Nome = T00104_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            pr_default.close(2);
            /* Using cursor T00105 */
            pr_default.execute(3, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
            A365FuncaoAPFAtributos_AtributosNom = T00105_A365FuncaoAPFAtributos_AtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
            n365FuncaoAPFAtributos_AtributosNom = T00105_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = T00105_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            n366FuncaoAPFAtributos_AtrTabelaCod = T00105_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            pr_default.close(3);
            /* Using cursor T00108 */
            pr_default.execute(6, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
            A367FuncaoAPFAtributos_AtrTabelaNom = T00108_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
            n367FuncaoAPFAtributos_AtrTabelaNom = T00108_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A393FuncaoAPFAtributos_SistemaCod = T00108_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = T00108_n393FuncaoAPFAtributos_SistemaCod[0];
            pr_default.close(6);
            /* Using cursor T00107 */
            pr_default.execute(5, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
            A412FuncaoAPFAtributos_FuncaoDadosNom = T00107_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = T00107_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = T00107_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = T00107_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            pr_default.close(5);
         }
      }

      protected void Load1058( )
      {
         /* Using cursor T00109 */
         pr_default.execute(7, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound58 = 1;
            A166FuncaoAPF_Nome = T00109_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A365FuncaoAPFAtributos_AtributosNom = T00109_A365FuncaoAPFAtributos_AtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
            n365FuncaoAPFAtributos_AtributosNom = T00109_n365FuncaoAPFAtributos_AtributosNom[0];
            A412FuncaoAPFAtributos_FuncaoDadosNom = T00109_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = T00109_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = T00109_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = T00109_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            A367FuncaoAPFAtributos_AtrTabelaNom = T00109_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
            n367FuncaoAPFAtributos_AtrTabelaNom = T00109_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A389FuncoesAPFAtributos_Regra = T00109_A389FuncoesAPFAtributos_Regra[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
            n389FuncoesAPFAtributos_Regra = T00109_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = T00109_A383FuncoesAPFAtributos_Code[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
            n383FuncoesAPFAtributos_Code = T00109_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = T00109_A384FuncoesAPFAtributos_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
            n384FuncoesAPFAtributos_Nome = T00109_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = T00109_A385FuncoesAPFAtributos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A385FuncoesAPFAtributos_Descricao", A385FuncoesAPFAtributos_Descricao);
            n385FuncoesAPFAtributos_Descricao = T00109_n385FuncoesAPFAtributos_Descricao[0];
            A751FuncaoAPFAtributos_Ativo = T00109_A751FuncaoAPFAtributos_Ativo[0];
            n751FuncaoAPFAtributos_Ativo = T00109_n751FuncaoAPFAtributos_Ativo[0];
            A748FuncaoAPFAtributos_MelhoraCod = T00109_A748FuncaoAPFAtributos_MelhoraCod[0];
            n748FuncaoAPFAtributos_MelhoraCod = T00109_n748FuncaoAPFAtributos_MelhoraCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = T00109_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            n378FuncaoAPFAtributos_FuncaoDadosCod = T00109_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = T00109_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            n366FuncaoAPFAtributos_AtrTabelaCod = T00109_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            A393FuncaoAPFAtributos_SistemaCod = T00109_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = T00109_n393FuncaoAPFAtributos_SistemaCod[0];
            ZM1058( -14) ;
         }
         pr_default.close(7);
         OnLoadActions1058( ) ;
      }

      protected void OnLoadActions1058( )
      {
      }

      protected void CheckExtendedTable1058( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00104 */
         pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A166FuncaoAPF_Nome = T00104_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         pr_default.close(2);
         /* Using cursor T00105 */
         pr_default.execute(3, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Atributos'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFAtributos_AtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A365FuncaoAPFAtributos_AtributosNom = T00105_A365FuncaoAPFAtributos_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
         n365FuncaoAPFAtributos_AtributosNom = T00105_n365FuncaoAPFAtributos_AtributosNom[0];
         A366FuncaoAPFAtributos_AtrTabelaCod = T00105_A366FuncaoAPFAtributos_AtrTabelaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
         n366FuncaoAPFAtributos_AtrTabelaCod = T00105_n366FuncaoAPFAtributos_AtrTabelaCod[0];
         pr_default.close(3);
         /* Using cursor T00108 */
         pr_default.execute(6, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A367FuncaoAPFAtributos_AtrTabelaNom = T00108_A367FuncaoAPFAtributos_AtrTabelaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
         n367FuncaoAPFAtributos_AtrTabelaNom = T00108_n367FuncaoAPFAtributos_AtrTabelaNom[0];
         A393FuncaoAPFAtributos_SistemaCod = T00108_A393FuncaoAPFAtributos_SistemaCod[0];
         n393FuncaoAPFAtributos_SistemaCod = T00108_n393FuncaoAPFAtributos_SistemaCod[0];
         pr_default.close(6);
         /* Using cursor T00107 */
         pr_default.execute(5, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A378FuncaoAPFAtributos_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'FuncaoAPFAtributos_FuncaoDadosTabela'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD");
               AnyError = 1;
               GX_FocusControl = edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A412FuncaoAPFAtributos_FuncaoDadosNom = T00107_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
         n412FuncaoAPFAtributos_FuncaoDadosNom = T00107_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
         A415FuncaoAPFAtributos_FuncaoDadosTip = T00107_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
         n415FuncaoAPFAtributos_FuncaoDadosTip = T00107_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
         pr_default.close(5);
         /* Using cursor T00106 */
         pr_default.execute(4, new Object[] {n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A748FuncaoAPFAtributos_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Funcao APFAtributosMelhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1058( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(6);
         pr_default.close(5);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( int A165FuncaoAPF_Codigo )
      {
         /* Using cursor T001010 */
         pr_default.execute(8, new Object[] {A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A166FuncaoAPF_Nome = T001010_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A166FuncaoAPF_Nome)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_16( int A364FuncaoAPFAtributos_AtributosCod )
      {
         /* Using cursor T001011 */
         pr_default.execute(9, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Atributos'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFAtributos_AtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A365FuncaoAPFAtributos_AtributosNom = T001011_A365FuncaoAPFAtributos_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
         n365FuncaoAPFAtributos_AtributosNom = T001011_n365FuncaoAPFAtributos_AtributosNom[0];
         A366FuncaoAPFAtributos_AtrTabelaCod = T001011_A366FuncaoAPFAtributos_AtrTabelaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
         n366FuncaoAPFAtributos_AtrTabelaCod = T001011_n366FuncaoAPFAtributos_AtrTabelaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_19( int A366FuncaoAPFAtributos_AtrTabelaCod )
      {
         /* Using cursor T001012 */
         pr_default.execute(10, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A367FuncaoAPFAtributos_AtrTabelaNom = T001012_A367FuncaoAPFAtributos_AtrTabelaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
         n367FuncaoAPFAtributos_AtrTabelaNom = T001012_n367FuncaoAPFAtributos_AtrTabelaNom[0];
         A393FuncaoAPFAtributos_SistemaCod = T001012_A393FuncaoAPFAtributos_SistemaCod[0];
         n393FuncaoAPFAtributos_SistemaCod = T001012_n393FuncaoAPFAtributos_SistemaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A393FuncaoAPFAtributos_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_18( int A378FuncaoAPFAtributos_FuncaoDadosCod )
      {
         /* Using cursor T001013 */
         pr_default.execute(11, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A378FuncaoAPFAtributos_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'FuncaoAPFAtributos_FuncaoDadosTabela'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD");
               AnyError = 1;
               GX_FocusControl = edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A412FuncaoAPFAtributos_FuncaoDadosNom = T001013_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
         n412FuncaoAPFAtributos_FuncaoDadosNom = T001013_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
         A415FuncaoAPFAtributos_FuncaoDadosTip = T001013_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
         n415FuncaoAPFAtributos_FuncaoDadosTip = T001013_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A412FuncaoAPFAtributos_FuncaoDadosNom)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A415FuncaoAPFAtributos_FuncaoDadosTip))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_17( int A748FuncaoAPFAtributos_MelhoraCod )
      {
         /* Using cursor T001014 */
         pr_default.execute(12, new Object[] {n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A748FuncaoAPFAtributos_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Funcao APFAtributosMelhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey1058( )
      {
         /* Using cursor T001015 */
         pr_default.execute(13, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound58 = 1;
         }
         else
         {
            RcdFound58 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00103 */
         pr_default.execute(1, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1058( 14) ;
            RcdFound58 = 1;
            A389FuncoesAPFAtributos_Regra = T00103_A389FuncoesAPFAtributos_Regra[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
            n389FuncoesAPFAtributos_Regra = T00103_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = T00103_A383FuncoesAPFAtributos_Code[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
            n383FuncoesAPFAtributos_Code = T00103_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = T00103_A384FuncoesAPFAtributos_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
            n384FuncoesAPFAtributos_Nome = T00103_n384FuncoesAPFAtributos_Nome[0];
            A385FuncoesAPFAtributos_Descricao = T00103_A385FuncoesAPFAtributos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A385FuncoesAPFAtributos_Descricao", A385FuncoesAPFAtributos_Descricao);
            n385FuncoesAPFAtributos_Descricao = T00103_n385FuncoesAPFAtributos_Descricao[0];
            A751FuncaoAPFAtributos_Ativo = T00103_A751FuncaoAPFAtributos_Ativo[0];
            n751FuncaoAPFAtributos_Ativo = T00103_n751FuncaoAPFAtributos_Ativo[0];
            A165FuncaoAPF_Codigo = T00103_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A364FuncaoAPFAtributos_AtributosCod = T00103_A364FuncaoAPFAtributos_AtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
            A748FuncaoAPFAtributos_MelhoraCod = T00103_A748FuncaoAPFAtributos_MelhoraCod[0];
            n748FuncaoAPFAtributos_MelhoraCod = T00103_n748FuncaoAPFAtributos_MelhoraCod[0];
            A378FuncaoAPFAtributos_FuncaoDadosCod = T00103_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            n378FuncaoAPFAtributos_FuncaoDadosCod = T00103_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
            sMode58 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1058( ) ;
            if ( AnyError == 1 )
            {
               RcdFound58 = 0;
               InitializeNonKey1058( ) ;
            }
            Gx_mode = sMode58;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound58 = 0;
            InitializeNonKey1058( ) ;
            sMode58 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode58;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1058( ) ;
         if ( RcdFound58 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound58 = 0;
         /* Using cursor T001016 */
         pr_default.execute(14, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T001016_A165FuncaoAPF_Codigo[0] < A165FuncaoAPF_Codigo ) || ( T001016_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( T001016_A364FuncaoAPFAtributos_AtributosCod[0] < A364FuncaoAPFAtributos_AtributosCod ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T001016_A165FuncaoAPF_Codigo[0] > A165FuncaoAPF_Codigo ) || ( T001016_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( T001016_A364FuncaoAPFAtributos_AtributosCod[0] > A364FuncaoAPFAtributos_AtributosCod ) ) )
            {
               A165FuncaoAPF_Codigo = T001016_A165FuncaoAPF_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A364FuncaoAPFAtributos_AtributosCod = T001016_A364FuncaoAPFAtributos_AtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
               RcdFound58 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound58 = 0;
         /* Using cursor T001017 */
         pr_default.execute(15, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T001017_A165FuncaoAPF_Codigo[0] > A165FuncaoAPF_Codigo ) || ( T001017_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( T001017_A364FuncaoAPFAtributos_AtributosCod[0] > A364FuncaoAPFAtributos_AtributosCod ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T001017_A165FuncaoAPF_Codigo[0] < A165FuncaoAPF_Codigo ) || ( T001017_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( T001017_A364FuncaoAPFAtributos_AtributosCod[0] < A364FuncaoAPFAtributos_AtributosCod ) ) )
            {
               A165FuncaoAPF_Codigo = T001017_A165FuncaoAPF_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A364FuncaoAPFAtributos_AtributosCod = T001017_A364FuncaoAPFAtributos_AtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
               RcdFound58 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1058( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1058( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound58 == 1 )
            {
               if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
               {
                  A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  A364FuncaoAPFAtributos_AtributosCod = Z364FuncaoAPFAtributos_AtributosCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAOAPF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1058( ) ;
                  GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
               {
                  /* Insert record */
                  GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1058( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAOAPF_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1058( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != Z364FuncaoAPFAtributos_AtributosCod ) )
         {
            A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A364FuncaoAPFAtributos_AtributosCod = Z364FuncaoAPFAtributos_AtributosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = chkFuncoesAPFAtributos_Regra_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00102 */
            pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesAPFAtributos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z389FuncoesAPFAtributos_Regra != T00102_A389FuncoesAPFAtributos_Regra[0] ) || ( StringUtil.StrCmp(Z383FuncoesAPFAtributos_Code, T00102_A383FuncoesAPFAtributos_Code[0]) != 0 ) || ( StringUtil.StrCmp(Z384FuncoesAPFAtributos_Nome, T00102_A384FuncoesAPFAtributos_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z385FuncoesAPFAtributos_Descricao, T00102_A385FuncoesAPFAtributos_Descricao[0]) != 0 ) || ( Z751FuncaoAPFAtributos_Ativo != T00102_A751FuncaoAPFAtributos_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z748FuncaoAPFAtributos_MelhoraCod != T00102_A748FuncaoAPFAtributos_MelhoraCod[0] ) || ( Z378FuncaoAPFAtributos_FuncaoDadosCod != T00102_A378FuncaoAPFAtributos_FuncaoDadosCod[0] ) )
            {
               if ( Z389FuncoesAPFAtributos_Regra != T00102_A389FuncoesAPFAtributos_Regra[0] )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncoesAPFAtributos_Regra");
                  GXUtil.WriteLogRaw("Old: ",Z389FuncoesAPFAtributos_Regra);
                  GXUtil.WriteLogRaw("Current: ",T00102_A389FuncoesAPFAtributos_Regra[0]);
               }
               if ( StringUtil.StrCmp(Z383FuncoesAPFAtributos_Code, T00102_A383FuncoesAPFAtributos_Code[0]) != 0 )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncoesAPFAtributos_Code");
                  GXUtil.WriteLogRaw("Old: ",Z383FuncoesAPFAtributos_Code);
                  GXUtil.WriteLogRaw("Current: ",T00102_A383FuncoesAPFAtributos_Code[0]);
               }
               if ( StringUtil.StrCmp(Z384FuncoesAPFAtributos_Nome, T00102_A384FuncoesAPFAtributos_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncoesAPFAtributos_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z384FuncoesAPFAtributos_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00102_A384FuncoesAPFAtributos_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z385FuncoesAPFAtributos_Descricao, T00102_A385FuncoesAPFAtributos_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncoesAPFAtributos_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z385FuncoesAPFAtributos_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T00102_A385FuncoesAPFAtributos_Descricao[0]);
               }
               if ( Z751FuncaoAPFAtributos_Ativo != T00102_A751FuncaoAPFAtributos_Ativo[0] )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncaoAPFAtributos_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z751FuncaoAPFAtributos_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00102_A751FuncaoAPFAtributos_Ativo[0]);
               }
               if ( Z748FuncaoAPFAtributos_MelhoraCod != T00102_A748FuncaoAPFAtributos_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncaoAPFAtributos_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z748FuncaoAPFAtributos_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T00102_A748FuncaoAPFAtributos_MelhoraCod[0]);
               }
               if ( Z378FuncaoAPFAtributos_FuncaoDadosCod != T00102_A378FuncaoAPFAtributos_FuncaoDadosCod[0] )
               {
                  GXUtil.WriteLog("funcoesapfatributos:[seudo value changed for attri]"+"FuncaoAPFAtributos_FuncaoDadosCod");
                  GXUtil.WriteLogRaw("Old: ",Z378FuncaoAPFAtributos_FuncaoDadosCod);
                  GXUtil.WriteLogRaw("Current: ",T00102_A378FuncaoAPFAtributos_FuncaoDadosCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncoesAPFAtributos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1058( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1058( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1058( 0) ;
            CheckOptimisticConcurrency1058( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1058( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1058( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001018 */
                     pr_default.execute(16, new Object[] {n389FuncoesAPFAtributos_Regra, A389FuncoesAPFAtributos_Regra, n383FuncoesAPFAtributos_Code, A383FuncoesAPFAtributos_Code, n384FuncoesAPFAtributos_Nome, A384FuncoesAPFAtributos_Nome, n385FuncoesAPFAtributos_Descricao, A385FuncoesAPFAtributos_Descricao, n751FuncaoAPFAtributos_Ativo, A751FuncaoAPFAtributos_Ativo, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod, n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption100( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1058( ) ;
            }
            EndLevel1058( ) ;
         }
         CloseExtendedTableCursors1058( ) ;
      }

      protected void Update1058( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1058( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1058( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1058( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1058( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001019 */
                     pr_default.execute(17, new Object[] {n389FuncoesAPFAtributos_Regra, A389FuncoesAPFAtributos_Regra, n383FuncoesAPFAtributos_Code, A383FuncoesAPFAtributos_Code, n384FuncoesAPFAtributos_Nome, A384FuncoesAPFAtributos_Nome, n385FuncoesAPFAtributos_Descricao, A385FuncoesAPFAtributos_Descricao, n751FuncaoAPFAtributos_Ativo, A751FuncaoAPFAtributos_Ativo, n748FuncaoAPFAtributos_MelhoraCod, A748FuncaoAPFAtributos_MelhoraCod, n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesAPFAtributos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1058( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1058( ) ;
         }
         CloseExtendedTableCursors1058( ) ;
      }

      protected void DeferredUpdate1058( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1058( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1058( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1058( ) ;
            AfterConfirm1058( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1058( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001020 */
                  pr_default.execute(18, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode58 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1058( ) ;
         Gx_mode = sMode58;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1058( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001021 */
            pr_default.execute(19, new Object[] {A165FuncaoAPF_Codigo});
            A166FuncaoAPF_Nome = T001021_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            pr_default.close(19);
            /* Using cursor T001022 */
            pr_default.execute(20, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
            A365FuncaoAPFAtributos_AtributosNom = T001022_A365FuncaoAPFAtributos_AtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
            n365FuncaoAPFAtributos_AtributosNom = T001022_n365FuncaoAPFAtributos_AtributosNom[0];
            A366FuncaoAPFAtributos_AtrTabelaCod = T001022_A366FuncaoAPFAtributos_AtrTabelaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            n366FuncaoAPFAtributos_AtrTabelaCod = T001022_n366FuncaoAPFAtributos_AtrTabelaCod[0];
            pr_default.close(20);
            /* Using cursor T001023 */
            pr_default.execute(21, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
            A367FuncaoAPFAtributos_AtrTabelaNom = T001023_A367FuncaoAPFAtributos_AtrTabelaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
            n367FuncaoAPFAtributos_AtrTabelaNom = T001023_n367FuncaoAPFAtributos_AtrTabelaNom[0];
            A393FuncaoAPFAtributos_SistemaCod = T001023_A393FuncaoAPFAtributos_SistemaCod[0];
            n393FuncaoAPFAtributos_SistemaCod = T001023_n393FuncaoAPFAtributos_SistemaCod[0];
            pr_default.close(21);
            /* Using cursor T001024 */
            pr_default.execute(22, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
            A412FuncaoAPFAtributos_FuncaoDadosNom = T001024_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
            n412FuncaoAPFAtributos_FuncaoDadosNom = T001024_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
            A415FuncaoAPFAtributos_FuncaoDadosTip = T001024_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
            n415FuncaoAPFAtributos_FuncaoDadosTip = T001024_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
            pr_default.close(22);
         }
      }

      protected void EndLevel1058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1058( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(22);
            pr_default.close(21);
            context.CommitDataStores( "FuncoesAPFAtributos");
            if ( AnyError == 0 )
            {
               ConfirmValues100( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(22);
            pr_default.close(21);
            context.RollbackDataStores( "FuncoesAPFAtributos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1058( )
      {
         /* Scan By routine */
         /* Using cursor T001025 */
         pr_default.execute(23);
         RcdFound58 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound58 = 1;
            A165FuncaoAPF_Codigo = T001025_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A364FuncaoAPFAtributos_AtributosCod = T001025_A364FuncaoAPFAtributos_AtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1058( )
      {
         /* Scan next routine */
         pr_default.readNext(23);
         RcdFound58 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound58 = 1;
            A165FuncaoAPF_Codigo = T001025_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A364FuncaoAPFAtributos_AtributosCod = T001025_A364FuncaoAPFAtributos_AtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
      }

      protected void ScanEnd1058( )
      {
         pr_default.close(23);
      }

      protected void AfterConfirm1058( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1058( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1058( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1058( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1058( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1058( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1058( )
      {
         edtFuncaoAPF_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Nome_Enabled), 5, 0)));
         edtFuncaoAPFAtributos_AtributosNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosNom_Enabled), 5, 0)));
         edtFuncaoAPFAtributos_AtrTabelaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtrTabelaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtrTabelaNom_Enabled), 5, 0)));
         chkFuncoesAPFAtributos_Regra.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncoesAPFAtributos_Regra_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncoesAPFAtributos_Regra.Enabled), 5, 0)));
         edtFuncoesAPFAtributos_Code_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncoesAPFAtributos_Code_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncoesAPFAtributos_Code_Enabled), 5, 0)));
         edtFuncoesAPFAtributos_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncoesAPFAtributos_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncoesAPFAtributos_Nome_Enabled), 5, 0)));
         edtFuncoesAPFAtributos_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncoesAPFAtributos_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncoesAPFAtributos_Descricao_Enabled), 5, 0)));
         edtFuncaoAPF_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         edtFuncaoAPFAtributos_AtributosCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Enabled), 5, 0)));
         edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled), 5, 0)));
         edtFuncaoAPFAtributos_AtrTabelaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtrTabelaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtrTabelaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues100( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117182841");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcoesapfatributos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoAPF_Codigo) + "," + UrlEncode("" +AV8FuncaoAPFAtributos_AtributosCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z389FuncoesAPFAtributos_Regra", Z389FuncoesAPFAtributos_Regra);
         GxWebStd.gx_hidden_field( context, "Z383FuncoesAPFAtributos_Code", Z383FuncoesAPFAtributos_Code);
         GxWebStd.gx_hidden_field( context, "Z384FuncoesAPFAtributos_Nome", StringUtil.RTrim( Z384FuncoesAPFAtributos_Nome));
         GxWebStd.gx_hidden_field( context, "Z385FuncoesAPFAtributos_Descricao", Z385FuncoesAPFAtributos_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, "Z751FuncaoAPFAtributos_Ativo", Z751FuncaoAPFAtributos_Ativo);
         GxWebStd.gx_hidden_field( context, "Z748FuncaoAPFAtributos_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z748FuncaoAPFAtributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N748FuncaoAPFAtributos_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPFATRIBUTOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_FuncaoAPFAtributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAOAPFATRIBUTOS_ATIVO", A751FuncaoAPFAtributos_Ativo);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM", A412FuncaoAPFAtributos_FuncaoDadosNom);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP", StringUtil.RTrim( A415FuncaoAPFAtributos_FuncaoDadosTip));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A393FuncaoAPFAtributos_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "FuncoesAPFAtributos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A751FuncaoAPFAtributos_Ativo);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("funcoesapfatributos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("funcoesapfatributos:[SendSecurityCheck value for]"+"FuncaoAPFAtributos_MelhoraCod:"+context.localUtil.Format( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), "ZZZZZ9"));
         GXUtil.WriteLog("funcoesapfatributos:[SendSecurityCheck value for]"+"FuncaoAPFAtributos_Ativo:"+StringUtil.BoolToStr( A751FuncaoAPFAtributos_Ativo));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcoesapfatributos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoAPF_Codigo) + "," + UrlEncode("" +AV8FuncaoAPFAtributos_AtributosCod) ;
      }

      public override String GetPgmname( )
      {
         return "FuncoesAPFAtributos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Atributos da Fun��o de Transa��o" ;
      }

      protected void InitializeNonKey1058( )
      {
         A378FuncaoAPFAtributos_FuncaoDadosCod = 0;
         n378FuncaoAPFAtributos_FuncaoDadosCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
         n378FuncaoAPFAtributos_FuncaoDadosCod = ((0==A378FuncaoAPFAtributos_FuncaoDadosCod) ? true : false);
         A748FuncaoAPFAtributos_MelhoraCod = 0;
         n748FuncaoAPFAtributos_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A748FuncaoAPFAtributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A748FuncaoAPFAtributos_MelhoraCod), 6, 0)));
         A166FuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         A365FuncaoAPFAtributos_AtributosNom = "";
         n365FuncaoAPFAtributos_AtributosNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         n412FuncaoAPFAtributos_FuncaoDadosNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A412FuncaoAPFAtributos_FuncaoDadosNom", A412FuncaoAPFAtributos_FuncaoDadosNom);
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         n415FuncaoAPFAtributos_FuncaoDadosTip = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A415FuncaoAPFAtributos_FuncaoDadosTip", A415FuncaoAPFAtributos_FuncaoDadosTip);
         A366FuncaoAPFAtributos_AtrTabelaCod = 0;
         n366FuncaoAPFAtributos_AtrTabelaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         n367FuncaoAPFAtributos_AtrTabelaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
         A393FuncaoAPFAtributos_SistemaCod = 0;
         n393FuncaoAPFAtributos_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A393FuncaoAPFAtributos_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A393FuncaoAPFAtributos_SistemaCod), 6, 0)));
         A389FuncoesAPFAtributos_Regra = false;
         n389FuncoesAPFAtributos_Regra = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
         n389FuncoesAPFAtributos_Regra = ((false==A389FuncoesAPFAtributos_Regra) ? true : false);
         A383FuncoesAPFAtributos_Code = "";
         n383FuncoesAPFAtributos_Code = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
         n383FuncoesAPFAtributos_Code = (String.IsNullOrEmpty(StringUtil.RTrim( A383FuncoesAPFAtributos_Code)) ? true : false);
         A384FuncoesAPFAtributos_Nome = "";
         n384FuncoesAPFAtributos_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
         n384FuncoesAPFAtributos_Nome = (String.IsNullOrEmpty(StringUtil.RTrim( A384FuncoesAPFAtributos_Nome)) ? true : false);
         A385FuncoesAPFAtributos_Descricao = "";
         n385FuncoesAPFAtributos_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A385FuncoesAPFAtributos_Descricao", A385FuncoesAPFAtributos_Descricao);
         n385FuncoesAPFAtributos_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A385FuncoesAPFAtributos_Descricao)) ? true : false);
         A751FuncaoAPFAtributos_Ativo = false;
         n751FuncaoAPFAtributos_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A751FuncaoAPFAtributos_Ativo", A751FuncaoAPFAtributos_Ativo);
         Z389FuncoesAPFAtributos_Regra = false;
         Z383FuncoesAPFAtributos_Code = "";
         Z384FuncoesAPFAtributos_Nome = "";
         Z385FuncoesAPFAtributos_Descricao = "";
         Z751FuncaoAPFAtributos_Ativo = false;
         Z748FuncaoAPFAtributos_MelhoraCod = 0;
         Z378FuncaoAPFAtributos_FuncaoDadosCod = 0;
      }

      protected void InitAll1058( )
      {
         A165FuncaoAPF_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A364FuncaoAPFAtributos_AtributosCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
         InitializeNonKey1058( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117182875");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcoesapfatributos.js", "?20203117182875");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapf_nome_Internalname = "TEXTBLOCKFUNCAOAPF_NOME";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         lblTextblockfuncaoapfatributos_atributosnom_Internalname = "TEXTBLOCKFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtFuncaoAPFAtributos_AtributosNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         lblTextblockfuncaoapfatributos_atrtabelanom_Internalname = "TEXTBLOCKFUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         lblTextblockfuncoesapfatributos_regra_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_REGRA";
         chkFuncoesAPFAtributos_Regra_Internalname = "FUNCOESAPFATRIBUTOS_REGRA";
         lblTextblockfuncoesapfatributos_code_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_CODE";
         edtFuncoesAPFAtributos_Code_Internalname = "FUNCOESAPFATRIBUTOS_CODE";
         lblTextblockfuncoesapfatributos_nome_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_NOME";
         edtFuncoesAPFAtributos_Nome_Internalname = "FUNCOESAPFATRIBUTOS_NOME";
         lblTextblockfuncoesapfatributos_descricao_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_DESCRICAO";
         edtFuncoesAPFAtributos_Descricao_Internalname = "FUNCOESAPFATRIBUTOS_DESCRICAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtFuncaoAPFAtributos_AtributosCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname = "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
         edtFuncaoAPFAtributos_AtrTabelaCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Atributos das Fun��es de APF";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Atributos da Fun��o de Transa��o";
         edtFuncoesAPFAtributos_Descricao_Jsonclick = "";
         edtFuncoesAPFAtributos_Descricao_Enabled = 1;
         edtFuncoesAPFAtributos_Nome_Jsonclick = "";
         edtFuncoesAPFAtributos_Nome_Enabled = 1;
         edtFuncoesAPFAtributos_Code_Jsonclick = "";
         edtFuncoesAPFAtributos_Code_Enabled = 1;
         chkFuncoesAPFAtributos_Regra.Enabled = 1;
         edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaNom_Enabled = 0;
         edtFuncaoAPFAtributos_AtributosNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Enabled = 0;
         edtFuncaoAPF_Nome_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaCod_Enabled = 0;
         edtFuncaoAPFAtributos_AtrTabelaCod_Visible = 1;
         edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick = "";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled = 1;
         edtFuncaoAPFAtributos_FuncaoDadosCod_Visible = 1;
         edtFuncaoAPFAtributos_AtributosCod_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosCod_Enabled = 1;
         edtFuncaoAPFAtributos_AtributosCod_Visible = 1;
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtFuncaoAPF_Codigo_Enabled = 1;
         edtFuncaoAPF_Codigo_Visible = 1;
         chkFuncoesAPFAtributos_Regra.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Funcaoapf_codigo( int GX_Parm1 ,
                                          String GX_Parm2 )
      {
         A165FuncaoAPF_Codigo = GX_Parm1;
         A166FuncaoAPF_Nome = GX_Parm2;
         /* Using cursor T001021 */
         pr_default.execute(19, new Object[] {A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Fun��es APF - An�lise de Ponto de Fun��o'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
         }
         A166FuncaoAPF_Nome = T001021_A166FuncaoAPF_Nome[0];
         pr_default.close(19);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A166FuncaoAPF_Nome = "";
         }
         isValidOutput.Add(A166FuncaoAPF_Nome);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaoapfatributos_atributoscod( int GX_Parm1 ,
                                                         int GX_Parm2 ,
                                                         String GX_Parm3 ,
                                                         String GX_Parm4 ,
                                                         int GX_Parm5 )
      {
         A364FuncaoAPFAtributos_AtributosCod = GX_Parm1;
         A366FuncaoAPFAtributos_AtrTabelaCod = GX_Parm2;
         n366FuncaoAPFAtributos_AtrTabelaCod = false;
         A365FuncaoAPFAtributos_AtributosNom = GX_Parm3;
         n365FuncaoAPFAtributos_AtributosNom = false;
         A367FuncaoAPFAtributos_AtrTabelaNom = GX_Parm4;
         n367FuncaoAPFAtributos_AtrTabelaNom = false;
         A393FuncaoAPFAtributos_SistemaCod = GX_Parm5;
         n393FuncaoAPFAtributos_SistemaCod = false;
         /* Using cursor T001022 */
         pr_default.execute(20, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFAtributos_Atributos'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFAtributos_AtributosCod_Internalname;
         }
         A365FuncaoAPFAtributos_AtributosNom = T001022_A365FuncaoAPFAtributos_AtributosNom[0];
         n365FuncaoAPFAtributos_AtributosNom = T001022_n365FuncaoAPFAtributos_AtributosNom[0];
         A366FuncaoAPFAtributos_AtrTabelaCod = T001022_A366FuncaoAPFAtributos_AtrTabelaCod[0];
         n366FuncaoAPFAtributos_AtrTabelaCod = T001022_n366FuncaoAPFAtributos_AtrTabelaCod[0];
         pr_default.close(20);
         /* Using cursor T001023 */
         pr_default.execute(21, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A367FuncaoAPFAtributos_AtrTabelaNom = T001023_A367FuncaoAPFAtributos_AtrTabelaNom[0];
         n367FuncaoAPFAtributos_AtrTabelaNom = T001023_n367FuncaoAPFAtributos_AtrTabelaNom[0];
         A393FuncaoAPFAtributos_SistemaCod = T001023_A393FuncaoAPFAtributos_SistemaCod[0];
         n393FuncaoAPFAtributos_SistemaCod = T001023_n393FuncaoAPFAtributos_SistemaCod[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A365FuncaoAPFAtributos_AtributosNom = "";
            n365FuncaoAPFAtributos_AtributosNom = false;
            A366FuncaoAPFAtributos_AtrTabelaCod = 0;
            n366FuncaoAPFAtributos_AtrTabelaCod = false;
            A367FuncaoAPFAtributos_AtrTabelaNom = "";
            n367FuncaoAPFAtributos_AtrTabelaNom = false;
            A393FuncaoAPFAtributos_SistemaCod = 0;
            n393FuncaoAPFAtributos_SistemaCod = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A393FuncaoAPFAtributos_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaoapfatributos_funcaodadoscod( int GX_Parm1 ,
                                                           String GX_Parm2 ,
                                                           String GX_Parm3 )
      {
         A378FuncaoAPFAtributos_FuncaoDadosCod = GX_Parm1;
         n378FuncaoAPFAtributos_FuncaoDadosCod = false;
         A412FuncaoAPFAtributos_FuncaoDadosNom = GX_Parm2;
         n412FuncaoAPFAtributos_FuncaoDadosNom = false;
         A415FuncaoAPFAtributos_FuncaoDadosTip = GX_Parm3;
         n415FuncaoAPFAtributos_FuncaoDadosTip = false;
         /* Using cursor T001024 */
         pr_default.execute(22, new Object[] {n378FuncaoAPFAtributos_FuncaoDadosCod, A378FuncaoAPFAtributos_FuncaoDadosCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            if ( ! ( (0==A378FuncaoAPFAtributos_FuncaoDadosCod) ) )
            {
               GX_msglist.addItem("N�o existe 'FuncaoAPFAtributos_FuncaoDadosTabela'.", "ForeignKeyNotFound", 1, "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD");
               AnyError = 1;
               GX_FocusControl = edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname;
            }
         }
         A412FuncaoAPFAtributos_FuncaoDadosNom = T001024_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
         n412FuncaoAPFAtributos_FuncaoDadosNom = T001024_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
         A415FuncaoAPFAtributos_FuncaoDadosTip = T001024_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
         n415FuncaoAPFAtributos_FuncaoDadosTip = T001024_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
         pr_default.close(22);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A412FuncaoAPFAtributos_FuncaoDadosNom = "";
            n412FuncaoAPFAtributos_FuncaoDadosNom = false;
            A415FuncaoAPFAtributos_FuncaoDadosTip = "";
            n415FuncaoAPFAtributos_FuncaoDadosTip = false;
         }
         isValidOutput.Add(A412FuncaoAPFAtributos_FuncaoDadosNom);
         isValidOutput.Add(StringUtil.RTrim( A415FuncaoAPFAtributos_FuncaoDadosTip));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12102',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(22);
         pr_default.close(21);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z383FuncoesAPFAtributos_Code = "";
         Z384FuncoesAPFAtributos_Nome = "";
         Z385FuncoesAPFAtributos_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockfuncaoapf_nome_Jsonclick = "";
         A166FuncaoAPF_Nome = "";
         lblTextblockfuncaoapfatributos_atributosnom_Jsonclick = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         lblTextblockfuncaoapfatributos_atrtabelanom_Jsonclick = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         lblTextblockfuncoesapfatributos_regra_Jsonclick = "";
         lblTextblockfuncoesapfatributos_code_Jsonclick = "";
         A383FuncoesAPFAtributos_Code = "";
         lblTextblockfuncoesapfatributos_nome_Jsonclick = "";
         A384FuncoesAPFAtributos_Nome = "";
         lblTextblockfuncoesapfatributos_descricao_Jsonclick = "";
         A385FuncoesAPFAtributos_Descricao = "";
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode58 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z166FuncaoAPF_Nome = "";
         Z365FuncaoAPFAtributos_AtributosNom = "";
         Z367FuncaoAPFAtributos_AtrTabelaNom = "";
         Z412FuncaoAPFAtributos_FuncaoDadosNom = "";
         Z415FuncaoAPFAtributos_FuncaoDadosTip = "";
         T00104_A166FuncaoAPF_Nome = new String[] {""} ;
         T00105_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         T00105_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         T00105_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         T00105_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         T00108_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         T00108_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         T00108_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         T00108_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         T00107_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         T00107_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         T00107_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         T00107_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         T00109_A166FuncaoAPF_Nome = new String[] {""} ;
         T00109_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         T00109_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         T00109_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         T00109_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         T00109_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         T00109_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         T00109_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         T00109_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         T00109_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         T00109_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         T00109_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         T00109_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         T00109_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         T00109_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         T00109_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         T00109_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         T00109_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         T00109_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         T00109_A165FuncaoAPF_Codigo = new int[1] ;
         T00109_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T00109_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         T00109_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         T00109_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         T00109_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         T00109_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         T00109_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         T00109_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         T00109_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         T00106_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         T00106_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         T001010_A166FuncaoAPF_Nome = new String[] {""} ;
         T001011_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         T001011_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         T001011_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         T001011_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         T001012_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         T001012_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         T001012_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         T001012_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         T001013_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         T001013_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         T001013_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         T001013_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         T001014_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         T001014_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         T001015_A165FuncaoAPF_Codigo = new int[1] ;
         T001015_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T00103_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         T00103_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         T00103_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         T00103_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         T00103_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         T00103_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         T00103_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         T00103_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         T00103_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         T00103_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         T00103_A165FuncaoAPF_Codigo = new int[1] ;
         T00103_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T00103_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         T00103_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         T00103_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         T00103_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         T001016_A165FuncaoAPF_Codigo = new int[1] ;
         T001016_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T001017_A165FuncaoAPF_Codigo = new int[1] ;
         T001017_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T00102_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         T00102_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         T00102_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         T00102_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         T00102_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         T00102_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         T00102_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         T00102_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         T00102_A751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         T00102_n751FuncaoAPFAtributos_Ativo = new bool[] {false} ;
         T00102_A165FuncaoAPF_Codigo = new int[1] ;
         T00102_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T00102_A748FuncaoAPFAtributos_MelhoraCod = new int[1] ;
         T00102_n748FuncaoAPFAtributos_MelhoraCod = new bool[] {false} ;
         T00102_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         T00102_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         T001021_A166FuncaoAPF_Nome = new String[] {""} ;
         T001022_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         T001022_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         T001022_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         T001022_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         T001023_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         T001023_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         T001023_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         T001023_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         T001024_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         T001024_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         T001024_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         T001024_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         T001025_A165FuncaoAPF_Codigo = new int[1] ;
         T001025_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcoesapfatributos__default(),
            new Object[][] {
                new Object[] {
               T00102_A389FuncoesAPFAtributos_Regra, T00102_n389FuncoesAPFAtributos_Regra, T00102_A383FuncoesAPFAtributos_Code, T00102_n383FuncoesAPFAtributos_Code, T00102_A384FuncoesAPFAtributos_Nome, T00102_n384FuncoesAPFAtributos_Nome, T00102_A385FuncoesAPFAtributos_Descricao, T00102_n385FuncoesAPFAtributos_Descricao, T00102_A751FuncaoAPFAtributos_Ativo, T00102_n751FuncaoAPFAtributos_Ativo,
               T00102_A165FuncaoAPF_Codigo, T00102_A364FuncaoAPFAtributos_AtributosCod, T00102_A748FuncaoAPFAtributos_MelhoraCod, T00102_n748FuncaoAPFAtributos_MelhoraCod, T00102_A378FuncaoAPFAtributos_FuncaoDadosCod, T00102_n378FuncaoAPFAtributos_FuncaoDadosCod
               }
               , new Object[] {
               T00103_A389FuncoesAPFAtributos_Regra, T00103_n389FuncoesAPFAtributos_Regra, T00103_A383FuncoesAPFAtributos_Code, T00103_n383FuncoesAPFAtributos_Code, T00103_A384FuncoesAPFAtributos_Nome, T00103_n384FuncoesAPFAtributos_Nome, T00103_A385FuncoesAPFAtributos_Descricao, T00103_n385FuncoesAPFAtributos_Descricao, T00103_A751FuncaoAPFAtributos_Ativo, T00103_n751FuncaoAPFAtributos_Ativo,
               T00103_A165FuncaoAPF_Codigo, T00103_A364FuncaoAPFAtributos_AtributosCod, T00103_A748FuncaoAPFAtributos_MelhoraCod, T00103_n748FuncaoAPFAtributos_MelhoraCod, T00103_A378FuncaoAPFAtributos_FuncaoDadosCod, T00103_n378FuncaoAPFAtributos_FuncaoDadosCod
               }
               , new Object[] {
               T00104_A166FuncaoAPF_Nome
               }
               , new Object[] {
               T00105_A365FuncaoAPFAtributos_AtributosNom, T00105_n365FuncaoAPFAtributos_AtributosNom, T00105_A366FuncaoAPFAtributos_AtrTabelaCod, T00105_n366FuncaoAPFAtributos_AtrTabelaCod
               }
               , new Object[] {
               T00106_A748FuncaoAPFAtributos_MelhoraCod
               }
               , new Object[] {
               T00107_A412FuncaoAPFAtributos_FuncaoDadosNom, T00107_n412FuncaoAPFAtributos_FuncaoDadosNom, T00107_A415FuncaoAPFAtributos_FuncaoDadosTip, T00107_n415FuncaoAPFAtributos_FuncaoDadosTip
               }
               , new Object[] {
               T00108_A367FuncaoAPFAtributos_AtrTabelaNom, T00108_n367FuncaoAPFAtributos_AtrTabelaNom, T00108_A393FuncaoAPFAtributos_SistemaCod, T00108_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               T00109_A166FuncaoAPF_Nome, T00109_A365FuncaoAPFAtributos_AtributosNom, T00109_n365FuncaoAPFAtributos_AtributosNom, T00109_A412FuncaoAPFAtributos_FuncaoDadosNom, T00109_n412FuncaoAPFAtributos_FuncaoDadosNom, T00109_A415FuncaoAPFAtributos_FuncaoDadosTip, T00109_n415FuncaoAPFAtributos_FuncaoDadosTip, T00109_A367FuncaoAPFAtributos_AtrTabelaNom, T00109_n367FuncaoAPFAtributos_AtrTabelaNom, T00109_A389FuncoesAPFAtributos_Regra,
               T00109_n389FuncoesAPFAtributos_Regra, T00109_A383FuncoesAPFAtributos_Code, T00109_n383FuncoesAPFAtributos_Code, T00109_A384FuncoesAPFAtributos_Nome, T00109_n384FuncoesAPFAtributos_Nome, T00109_A385FuncoesAPFAtributos_Descricao, T00109_n385FuncoesAPFAtributos_Descricao, T00109_A751FuncaoAPFAtributos_Ativo, T00109_n751FuncaoAPFAtributos_Ativo, T00109_A165FuncaoAPF_Codigo,
               T00109_A364FuncaoAPFAtributos_AtributosCod, T00109_A748FuncaoAPFAtributos_MelhoraCod, T00109_n748FuncaoAPFAtributos_MelhoraCod, T00109_A378FuncaoAPFAtributos_FuncaoDadosCod, T00109_n378FuncaoAPFAtributos_FuncaoDadosCod, T00109_A366FuncaoAPFAtributos_AtrTabelaCod, T00109_n366FuncaoAPFAtributos_AtrTabelaCod, T00109_A393FuncaoAPFAtributos_SistemaCod, T00109_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               T001010_A166FuncaoAPF_Nome
               }
               , new Object[] {
               T001011_A365FuncaoAPFAtributos_AtributosNom, T001011_n365FuncaoAPFAtributos_AtributosNom, T001011_A366FuncaoAPFAtributos_AtrTabelaCod, T001011_n366FuncaoAPFAtributos_AtrTabelaCod
               }
               , new Object[] {
               T001012_A367FuncaoAPFAtributos_AtrTabelaNom, T001012_n367FuncaoAPFAtributos_AtrTabelaNom, T001012_A393FuncaoAPFAtributos_SistemaCod, T001012_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               T001013_A412FuncaoAPFAtributos_FuncaoDadosNom, T001013_n412FuncaoAPFAtributos_FuncaoDadosNom, T001013_A415FuncaoAPFAtributos_FuncaoDadosTip, T001013_n415FuncaoAPFAtributos_FuncaoDadosTip
               }
               , new Object[] {
               T001014_A748FuncaoAPFAtributos_MelhoraCod
               }
               , new Object[] {
               T001015_A165FuncaoAPF_Codigo, T001015_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T001016_A165FuncaoAPF_Codigo, T001016_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T001017_A165FuncaoAPF_Codigo, T001017_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001021_A166FuncaoAPF_Nome
               }
               , new Object[] {
               T001022_A365FuncaoAPFAtributos_AtributosNom, T001022_n365FuncaoAPFAtributos_AtributosNom, T001022_A366FuncaoAPFAtributos_AtrTabelaCod, T001022_n366FuncaoAPFAtributos_AtrTabelaCod
               }
               , new Object[] {
               T001023_A367FuncaoAPFAtributos_AtrTabelaNom, T001023_n367FuncaoAPFAtributos_AtrTabelaNom, T001023_A393FuncaoAPFAtributos_SistemaCod, T001023_n393FuncaoAPFAtributos_SistemaCod
               }
               , new Object[] {
               T001024_A412FuncaoAPFAtributos_FuncaoDadosNom, T001024_n412FuncaoAPFAtributos_FuncaoDadosNom, T001024_A415FuncaoAPFAtributos_FuncaoDadosTip, T001024_n415FuncaoAPFAtributos_FuncaoDadosTip
               }
               , new Object[] {
               T001025_A165FuncaoAPF_Codigo, T001025_A364FuncaoAPFAtributos_AtributosCod
               }
            }
         );
         AV15Pgmname = "FuncoesAPFAtributos";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound58 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7FuncaoAPF_Codigo ;
      private int wcpOAV8FuncaoAPFAtributos_AtributosCod ;
      private int Z165FuncaoAPF_Codigo ;
      private int Z364FuncaoAPFAtributos_AtributosCod ;
      private int Z748FuncaoAPFAtributos_MelhoraCod ;
      private int Z378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int N378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int N748FuncaoAPFAtributos_MelhoraCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A748FuncaoAPFAtributos_MelhoraCod ;
      private int AV7FuncaoAPF_Codigo ;
      private int AV8FuncaoAPFAtributos_AtributosCod ;
      private int trnEnded ;
      private int edtFuncaoAPF_Codigo_Visible ;
      private int edtFuncaoAPF_Codigo_Enabled ;
      private int edtFuncaoAPFAtributos_AtributosCod_Visible ;
      private int edtFuncaoAPFAtributos_AtributosCod_Enabled ;
      private int edtFuncaoAPFAtributos_FuncaoDadosCod_Visible ;
      private int edtFuncaoAPFAtributos_FuncaoDadosCod_Enabled ;
      private int edtFuncaoAPFAtributos_AtrTabelaCod_Enabled ;
      private int edtFuncaoAPFAtributos_AtrTabelaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtFuncaoAPF_Nome_Enabled ;
      private int edtFuncaoAPFAtributos_AtributosNom_Enabled ;
      private int edtFuncaoAPFAtributos_AtrTabelaNom_Enabled ;
      private int edtFuncoesAPFAtributos_Code_Enabled ;
      private int edtFuncoesAPFAtributos_Nome_Enabled ;
      private int edtFuncoesAPFAtributos_Descricao_Enabled ;
      private int AV12Insert_FuncaoAPFAtributos_FuncaoDadosCod ;
      private int AV14Insert_FuncaoAPFAtributos_MelhoraCod ;
      private int A393FuncaoAPFAtributos_SistemaCod ;
      private int AV16GXV1 ;
      private int Z366FuncaoAPFAtributos_AtrTabelaCod ;
      private int Z393FuncaoAPFAtributos_SistemaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z384FuncoesAPFAtributos_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkFuncoesAPFAtributos_Regra_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapf_nome_Internalname ;
      private String lblTextblockfuncaoapf_nome_Jsonclick ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String lblTextblockfuncaoapfatributos_atributosnom_Internalname ;
      private String lblTextblockfuncaoapfatributos_atributosnom_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosNom_Internalname ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String edtFuncaoAPFAtributos_AtributosNom_Jsonclick ;
      private String lblTextblockfuncaoapfatributos_atrtabelanom_Internalname ;
      private String lblTextblockfuncaoapfatributos_atrtabelanom_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Internalname ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_regra_Internalname ;
      private String lblTextblockfuncoesapfatributos_regra_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_code_Internalname ;
      private String lblTextblockfuncoesapfatributos_code_Jsonclick ;
      private String edtFuncoesAPFAtributos_Code_Internalname ;
      private String edtFuncoesAPFAtributos_Code_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_nome_Internalname ;
      private String lblTextblockfuncoesapfatributos_nome_Jsonclick ;
      private String edtFuncoesAPFAtributos_Nome_Internalname ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String edtFuncoesAPFAtributos_Nome_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_descricao_Internalname ;
      private String lblTextblockfuncoesapfatributos_descricao_Jsonclick ;
      private String edtFuncoesAPFAtributos_Descricao_Internalname ;
      private String edtFuncoesAPFAtributos_Descricao_Jsonclick ;
      private String A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode58 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z365FuncaoAPFAtributos_AtributosNom ;
      private String Z367FuncaoAPFAtributos_AtrTabelaNom ;
      private String Z415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z389FuncoesAPFAtributos_Regra ;
      private bool Z751FuncaoAPFAtributos_Ativo ;
      private bool entryPointCalled ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n748FuncaoAPFAtributos_MelhoraCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool n751FuncaoAPFAtributos_Ativo ;
      private bool A751FuncaoAPFAtributos_Ativo ;
      private bool n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool n393FuncaoAPFAtributos_SistemaCod ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z383FuncoesAPFAtributos_Code ;
      private String Z385FuncoesAPFAtributos_Descricao ;
      private String A166FuncaoAPF_Nome ;
      private String A383FuncoesAPFAtributos_Code ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private String A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String Z166FuncaoAPF_Nome ;
      private String Z412FuncaoAPFAtributos_FuncaoDadosNom ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkFuncoesAPFAtributos_Regra ;
      private IDataStoreProvider pr_default ;
      private String[] T00104_A166FuncaoAPF_Nome ;
      private String[] T00105_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] T00105_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] T00105_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] T00105_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] T00108_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] T00108_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] T00108_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] T00108_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] T00107_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] T00107_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] T00107_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] T00107_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String[] T00109_A166FuncaoAPF_Nome ;
      private String[] T00109_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] T00109_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] T00109_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] T00109_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] T00109_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] T00109_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String[] T00109_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] T00109_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] T00109_A389FuncoesAPFAtributos_Regra ;
      private bool[] T00109_n389FuncoesAPFAtributos_Regra ;
      private String[] T00109_A383FuncoesAPFAtributos_Code ;
      private bool[] T00109_n383FuncoesAPFAtributos_Code ;
      private String[] T00109_A384FuncoesAPFAtributos_Nome ;
      private bool[] T00109_n384FuncoesAPFAtributos_Nome ;
      private String[] T00109_A385FuncoesAPFAtributos_Descricao ;
      private bool[] T00109_n385FuncoesAPFAtributos_Descricao ;
      private bool[] T00109_A751FuncaoAPFAtributos_Ativo ;
      private bool[] T00109_n751FuncaoAPFAtributos_Ativo ;
      private int[] T00109_A165FuncaoAPF_Codigo ;
      private int[] T00109_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T00109_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] T00109_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] T00109_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] T00109_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] T00109_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] T00109_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] T00109_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] T00109_n393FuncaoAPFAtributos_SistemaCod ;
      private int[] T00106_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] T00106_n748FuncaoAPFAtributos_MelhoraCod ;
      private String[] T001010_A166FuncaoAPF_Nome ;
      private String[] T001011_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] T001011_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] T001011_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] T001011_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] T001012_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] T001012_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] T001012_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] T001012_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] T001013_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] T001013_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] T001013_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] T001013_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private int[] T001014_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] T001014_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] T001015_A165FuncaoAPF_Codigo ;
      private int[] T001015_A364FuncaoAPFAtributos_AtributosCod ;
      private bool[] T00103_A389FuncoesAPFAtributos_Regra ;
      private bool[] T00103_n389FuncoesAPFAtributos_Regra ;
      private String[] T00103_A383FuncoesAPFAtributos_Code ;
      private bool[] T00103_n383FuncoesAPFAtributos_Code ;
      private String[] T00103_A384FuncoesAPFAtributos_Nome ;
      private bool[] T00103_n384FuncoesAPFAtributos_Nome ;
      private String[] T00103_A385FuncoesAPFAtributos_Descricao ;
      private bool[] T00103_n385FuncoesAPFAtributos_Descricao ;
      private bool[] T00103_A751FuncaoAPFAtributos_Ativo ;
      private bool[] T00103_n751FuncaoAPFAtributos_Ativo ;
      private int[] T00103_A165FuncaoAPF_Codigo ;
      private int[] T00103_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T00103_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] T00103_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] T00103_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] T00103_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] T001016_A165FuncaoAPF_Codigo ;
      private int[] T001016_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T001017_A165FuncaoAPF_Codigo ;
      private int[] T001017_A364FuncaoAPFAtributos_AtributosCod ;
      private bool[] T00102_A389FuncoesAPFAtributos_Regra ;
      private bool[] T00102_n389FuncoesAPFAtributos_Regra ;
      private String[] T00102_A383FuncoesAPFAtributos_Code ;
      private bool[] T00102_n383FuncoesAPFAtributos_Code ;
      private String[] T00102_A384FuncoesAPFAtributos_Nome ;
      private bool[] T00102_n384FuncoesAPFAtributos_Nome ;
      private String[] T00102_A385FuncoesAPFAtributos_Descricao ;
      private bool[] T00102_n385FuncoesAPFAtributos_Descricao ;
      private bool[] T00102_A751FuncaoAPFAtributos_Ativo ;
      private bool[] T00102_n751FuncaoAPFAtributos_Ativo ;
      private int[] T00102_A165FuncaoAPF_Codigo ;
      private int[] T00102_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T00102_A748FuncaoAPFAtributos_MelhoraCod ;
      private bool[] T00102_n748FuncaoAPFAtributos_MelhoraCod ;
      private int[] T00102_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] T00102_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] T001021_A166FuncaoAPF_Nome ;
      private String[] T001022_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] T001022_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] T001022_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] T001022_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] T001023_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] T001023_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] T001023_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] T001023_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] T001024_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] T001024_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] T001024_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] T001024_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private int[] T001025_A165FuncaoAPF_Codigo ;
      private int[] T001025_A364FuncaoAPFAtributos_AtributosCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class funcoesapfatributos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00109 ;
          prmT00109 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00104 ;
          prmT00104 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00105 ;
          prmT00105 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00108 ;
          prmT00108 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00107 ;
          prmT00107 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00106 ;
          prmT00106 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001010 ;
          prmT001010 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001011 ;
          prmT001011 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001012 ;
          prmT001012 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001013 ;
          prmT001013 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001014 ;
          prmT001014 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001015 ;
          prmT001015 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00103 ;
          prmT00103 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001016 ;
          prmT001016 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001017 ;
          prmT001017 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00102 ;
          prmT00102 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001018 ;
          prmT001018 = new Object[] {
          new Object[] {"@FuncoesAPFAtributos_Regra",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@FuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@FuncaoAPFAtributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001019 ;
          prmT001019 = new Object[] {
          new Object[] {"@FuncoesAPFAtributos_Regra",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@FuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@FuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@FuncaoAPFAtributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPFAtributos_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001020 ;
          prmT001020 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001025 ;
          prmT001025 = new Object[] {
          } ;
          Object[] prmT001021 ;
          prmT001021 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001022 ;
          prmT001022 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001023 ;
          prmT001023 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001024 ;
          prmT001024 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00102", "SELECT [FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_Ativo], [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, [FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, [FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod FROM [FuncoesAPFAtributos] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00102,1,0,true,false )
             ,new CursorDef("T00103", "SELECT [FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_Ativo], [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, [FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, [FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00103,1,0,true,false )
             ,new CursorDef("T00104", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00104,1,0,true,false )
             ,new CursorDef("T00105", "SELECT [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00105,1,0,true,false )
             ,new CursorDef("T00106", "SELECT [Atributos_Codigo] AS FuncaoAPFAtributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00106,1,0,true,false )
             ,new CursorDef("T00107", "SELECT [FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, [FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoAPFAtributos_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00107,1,0,true,false )
             ,new CursorDef("T00108", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, [Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00108,1,0,true,false )
             ,new CursorDef("T00109", "SELECT T2.[FuncaoAPF_Nome], T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T5.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T5.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip, T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, TM1.[FuncoesAPFAtributos_Regra], TM1.[FuncoesAPFAtributos_Code], TM1.[FuncoesAPFAtributos_Nome], TM1.[FuncoesAPFAtributos_Descricao], TM1.[FuncaoAPFAtributos_Ativo], TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, TM1.[FuncaoAPFAtributos_MelhoraCod] AS FuncaoAPFAtributos_MelhoraCod, TM1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T4.[Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM (((([FuncoesAPFAtributos] TM1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_Codigo]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = TM1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod]) LEFT JOIN [FuncaoDados] T5 WITH (NOLOCK) ON T5.[FuncaoDados_Codigo] = TM1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and TM1.[FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ORDER BY TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPFAtributos_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00109,100,0,true,false )
             ,new CursorDef("T001010", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001010,1,0,true,false )
             ,new CursorDef("T001011", "SELECT [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001011,1,0,true,false )
             ,new CursorDef("T001012", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, [Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001012,1,0,true,false )
             ,new CursorDef("T001013", "SELECT [FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, [FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoAPFAtributos_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001013,1,0,true,false )
             ,new CursorDef("T001014", "SELECT [Atributos_Codigo] AS FuncaoAPFAtributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001014,1,0,true,false )
             ,new CursorDef("T001015", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001015,1,0,true,false )
             ,new CursorDef("T001016", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE ( [FuncaoAPF_Codigo] > @FuncaoAPF_Codigo or [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and [FuncaoAPFAtributos_AtributosCod] > @FuncaoAPFAtributos_AtributosCod) ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001016,1,0,true,true )
             ,new CursorDef("T001017", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE ( [FuncaoAPF_Codigo] < @FuncaoAPF_Codigo or [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and [FuncaoAPFAtributos_AtributosCod] < @FuncaoAPFAtributos_AtributosCod) ORDER BY [FuncaoAPF_Codigo] DESC, [FuncaoAPFAtributos_AtributosCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001017,1,0,true,true )
             ,new CursorDef("T001018", "INSERT INTO [FuncoesAPFAtributos]([FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome], [FuncoesAPFAtributos_Descricao], [FuncaoAPFAtributos_Ativo], [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod], [FuncaoAPFAtributos_MelhoraCod], [FuncaoAPFAtributos_FuncaoDadosCod]) VALUES(@FuncoesAPFAtributos_Regra, @FuncoesAPFAtributos_Code, @FuncoesAPFAtributos_Nome, @FuncoesAPFAtributos_Descricao, @FuncaoAPFAtributos_Ativo, @FuncaoAPF_Codigo, @FuncaoAPFAtributos_AtributosCod, @FuncaoAPFAtributos_MelhoraCod, @FuncaoAPFAtributos_FuncaoDadosCod)", GxErrorMask.GX_NOMASK,prmT001018)
             ,new CursorDef("T001019", "UPDATE [FuncoesAPFAtributos] SET [FuncoesAPFAtributos_Regra]=@FuncoesAPFAtributos_Regra, [FuncoesAPFAtributos_Code]=@FuncoesAPFAtributos_Code, [FuncoesAPFAtributos_Nome]=@FuncoesAPFAtributos_Nome, [FuncoesAPFAtributos_Descricao]=@FuncoesAPFAtributos_Descricao, [FuncaoAPFAtributos_Ativo]=@FuncaoAPFAtributos_Ativo, [FuncaoAPFAtributos_MelhoraCod]=@FuncaoAPFAtributos_MelhoraCod, [FuncaoAPFAtributos_FuncaoDadosCod]=@FuncaoAPFAtributos_FuncaoDadosCod  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK,prmT001019)
             ,new CursorDef("T001020", "DELETE FROM [FuncoesAPFAtributos]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo AND [FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod", GxErrorMask.GX_NOMASK,prmT001020)
             ,new CursorDef("T001021", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001021,1,0,true,false )
             ,new CursorDef("T001022", "SELECT [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001022,1,0,true,false )
             ,new CursorDef("T001023", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, [Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001023,1,0,true,false )
             ,new CursorDef("T001024", "SELECT [FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, [FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoAPFAtributos_FuncaoDadosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001024,1,0,true,false )
             ,new CursorDef("T001025", "SELECT [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod FROM [FuncoesAPFAtributos] WITH (NOLOCK) ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001025,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                stmt.SetParameter(9, (int)parms[15]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
