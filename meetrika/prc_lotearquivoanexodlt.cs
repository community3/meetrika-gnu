/*
               File: PRC_LoteArquivoAnexoDLT
        Description: DLT Arquivo Anexo do Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:37.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_lotearquivoanexodlt : GXProcedure
   {
      public prc_lotearquivoanexodlt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_lotearquivoanexodlt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_LoteArquivoAnexo_LoteCod ,
                           ref DateTime aP1_LoteArquivoAnexo_Data ,
                           ref String aP2_LoteArquivoAnexo_NomeArq )
      {
         this.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         this.AV8LoteArquivoAnexo_Data = aP1_LoteArquivoAnexo_Data;
         this.A839LoteArquivoAnexo_NomeArq = aP2_LoteArquivoAnexo_NomeArq;
         initialize();
         executePrivate();
         aP0_LoteArquivoAnexo_LoteCod=this.A841LoteArquivoAnexo_LoteCod;
         aP1_LoteArquivoAnexo_Data=this.AV8LoteArquivoAnexo_Data;
         aP2_LoteArquivoAnexo_NomeArq=this.A839LoteArquivoAnexo_NomeArq;
      }

      public String executeUdp( ref int aP0_LoteArquivoAnexo_LoteCod ,
                                ref DateTime aP1_LoteArquivoAnexo_Data )
      {
         this.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         this.AV8LoteArquivoAnexo_Data = aP1_LoteArquivoAnexo_Data;
         this.A839LoteArquivoAnexo_NomeArq = aP2_LoteArquivoAnexo_NomeArq;
         initialize();
         executePrivate();
         aP0_LoteArquivoAnexo_LoteCod=this.A841LoteArquivoAnexo_LoteCod;
         aP1_LoteArquivoAnexo_Data=this.AV8LoteArquivoAnexo_Data;
         aP2_LoteArquivoAnexo_NomeArq=this.A839LoteArquivoAnexo_NomeArq;
         return A839LoteArquivoAnexo_NomeArq ;
      }

      public void executeSubmit( ref int aP0_LoteArquivoAnexo_LoteCod ,
                                 ref DateTime aP1_LoteArquivoAnexo_Data ,
                                 ref String aP2_LoteArquivoAnexo_NomeArq )
      {
         prc_lotearquivoanexodlt objprc_lotearquivoanexodlt;
         objprc_lotearquivoanexodlt = new prc_lotearquivoanexodlt();
         objprc_lotearquivoanexodlt.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         objprc_lotearquivoanexodlt.AV8LoteArquivoAnexo_Data = aP1_LoteArquivoAnexo_Data;
         objprc_lotearquivoanexodlt.A839LoteArquivoAnexo_NomeArq = aP2_LoteArquivoAnexo_NomeArq;
         objprc_lotearquivoanexodlt.context.SetSubmitInitialConfig(context);
         objprc_lotearquivoanexodlt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_lotearquivoanexodlt);
         aP0_LoteArquivoAnexo_LoteCod=this.A841LoteArquivoAnexo_LoteCod;
         aP1_LoteArquivoAnexo_Data=this.AV8LoteArquivoAnexo_Data;
         aP2_LoteArquivoAnexo_NomeArq=this.A839LoteArquivoAnexo_NomeArq;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_lotearquivoanexodlt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P006Y2 */
         pr_default.execute(0, new Object[] {A841LoteArquivoAnexo_LoteCod, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A836LoteArquivoAnexo_Data = P006Y2_A836LoteArquivoAnexo_Data[0];
            if ( DateTimeUtil.ResetTime( A836LoteArquivoAnexo_Data) == DateTimeUtil.ResetTime( AV8LoteArquivoAnexo_Data) )
            {
               if ( DateTimeUtil.Hour( A836LoteArquivoAnexo_Data) == DateTimeUtil.Hour( AV8LoteArquivoAnexo_Data) )
               {
                  if ( DateTimeUtil.Minute( A836LoteArquivoAnexo_Data) == DateTimeUtil.Minute( AV8LoteArquivoAnexo_Data) )
                  {
                     /* Using cursor P006Y3 */
                     pr_default.execute(1, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
                     pr_default.close(1);
                     dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_LoteArquivoAnexoDLT");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006Y2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         P006Y2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P006Y2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P006Y2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_lotearquivoanexodlt__default(),
            new Object[][] {
                new Object[] {
               P006Y2_A841LoteArquivoAnexo_LoteCod, P006Y2_A839LoteArquivoAnexo_NomeArq, P006Y2_n839LoteArquivoAnexo_NomeArq, P006Y2_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A841LoteArquivoAnexo_LoteCod ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String scmdbuf ;
      private DateTime AV8LoteArquivoAnexo_Data ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_LoteArquivoAnexo_LoteCod ;
      private DateTime aP1_LoteArquivoAnexo_Data ;
      private String aP2_LoteArquivoAnexo_NomeArq ;
      private IDataStoreProvider pr_default ;
      private int[] P006Y2_A841LoteArquivoAnexo_LoteCod ;
      private String[] P006Y2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P006Y2_n839LoteArquivoAnexo_NomeArq ;
      private DateTime[] P006Y2_A836LoteArquivoAnexo_Data ;
   }

   public class prc_lotearquivoanexodlt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006Y2 ;
          prmP006Y2 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0}
          } ;
          Object[] prmP006Y3 ;
          prmP006Y3 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006Y2", "SELECT [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_NomeArq], [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (UPDLOCK) WHERE ([LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod) AND ([LoteArquivoAnexo_NomeArq] = @LoteArquivoAnexo_NomeArq) ORDER BY [LoteArquivoAnexo_LoteCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Y2,1,0,true,false )
             ,new CursorDef("P006Y3", "DELETE FROM [LoteArquivoAnexo]  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Y3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
       }
    }

 }

}
