/*
               File: Contratante
        Description: Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:14:49.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratante : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvESTADO_UF05124( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"MUNICIPIO_CODIGO") == 0 )
         {
            AV15Estado_UF = GetNextPar( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAMUNICIPIO_CODIGO05124( AV15Estado_UF) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATANTE_SERVICOSSPADRAO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATANTE_SERVICOSSPADRAO05124( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_37") == 0 )
         {
            A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_37( A25Municipio_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_38") == 0 )
         {
            A335Contratante_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_38( A335Contratante_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratante_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contratante_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratante_UsaOSistema.Name = "CONTRATANTE_USAOSISTEMA";
         cmbContratante_UsaOSistema.WebTags = "";
         cmbContratante_UsaOSistema.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_UsaOSistema.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_UsaOSistema.ItemCount > 0 )
         {
            if ( (false==A1822Contratante_UsaOSistema) )
            {
               A1822Contratante_UsaOSistema = false;
               n1822Contratante_UsaOSistema = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
            }
            A1822Contratante_UsaOSistema = StringUtil.StrToBool( cmbContratante_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1822Contratante_UsaOSistema)));
            n1822Contratante_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
         }
         cmbavRedmine_secure.Name = "vREDMINE_SECURE";
         cmbavRedmine_secure.WebTags = "";
         cmbavRedmine_secure.addItem("0", "http://", 0);
         cmbavRedmine_secure.addItem("1", "https://", 0);
         if ( cmbavRedmine_secure.ItemCount > 0 )
         {
            AV30Redmine_Secure = (short)(NumberUtil.Val( cmbavRedmine_secure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Redmine_Secure", StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0));
         }
         cmbavRedmine_versao.Name = "vREDMINE_VERSAO";
         cmbavRedmine_versao.WebTags = "";
         cmbavRedmine_versao.addItem("144", "1.4.4", 0);
         cmbavRedmine_versao.addItem("223", "2.2.3", 0);
         if ( cmbavRedmine_versao.ItemCount > 0 )
         {
            AV28Redmine_Versao = cmbavRedmine_versao.getValidValue(AV28Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Redmine_Versao", AV28Redmine_Versao);
         }
         cmbContratante_EmailSdaAut.Name = "CONTRATANTE_EMAILSDAAUT";
         cmbContratante_EmailSdaAut.WebTags = "";
         cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_EmailSdaAut.ItemCount > 0 )
         {
            if ( (false==A551Contratante_EmailSdaAut) )
            {
               A551Contratante_EmailSdaAut = false;
               n551Contratante_EmailSdaAut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
            }
            A551Contratante_EmailSdaAut = StringUtil.StrToBool( cmbContratante_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A551Contratante_EmailSdaAut)));
            n551Contratante_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
         }
         cmbContratante_EmailSdaSec.Name = "CONTRATANTE_EMAILSDASEC";
         cmbContratante_EmailSdaSec.WebTags = "";
         cmbContratante_EmailSdaSec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
         cmbContratante_EmailSdaSec.addItem("1", "SSL e TLS", 0);
         if ( cmbContratante_EmailSdaSec.ItemCount > 0 )
         {
            A1048Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbContratante_EmailSdaSec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0))), "."));
            n1048Contratante_EmailSdaSec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
         }
         dynavEstado_uf.Name = "vESTADO_UF";
         dynavEstado_uf.WebTags = "";
         dynMunicipio_Codigo.Name = "MUNICIPIO_CODIGO";
         dynMunicipio_Codigo.WebTags = "";
         chkContratante_Ativo.Name = "CONTRATANTE_ATIVO";
         chkContratante_Ativo.WebTags = "";
         chkContratante_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratante_Ativo_Internalname, "TitleCaption", chkContratante_Ativo.Caption);
         chkContratante_Ativo.CheckedValue = "false";
         cmbContratante_SSAutomatica.Name = "CONTRATANTE_SSAUTOMATICA";
         cmbContratante_SSAutomatica.WebTags = "";
         cmbContratante_SSAutomatica.addItem(StringUtil.BoolToStr( false), "Manual", 0);
         cmbContratante_SSAutomatica.addItem(StringUtil.BoolToStr( true), "Autom�tica", 0);
         if ( cmbContratante_SSAutomatica.ItemCount > 0 )
         {
            A1652Contratante_SSAutomatica = StringUtil.StrToBool( cmbContratante_SSAutomatica.getValidValue(StringUtil.BoolToStr( A1652Contratante_SSAutomatica)));
            n1652Contratante_SSAutomatica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
         }
         cmbContratante_SSPrestadoraUnica.Name = "CONTRATANTE_SSPRESTADORAUNICA";
         cmbContratante_SSPrestadoraUnica.WebTags = "";
         cmbContratante_SSPrestadoraUnica.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_SSPrestadoraUnica.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_SSPrestadoraUnica.ItemCount > 0 )
         {
            A1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cmbContratante_SSPrestadoraUnica.getValidValue(StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica)));
            n1733Contratante_SSPrestadoraUnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
         }
         cmbContratante_SSStatusPlanejamento.Name = "CONTRATANTE_SSSTATUSPLANEJAMENTO";
         cmbContratante_SSStatusPlanejamento.WebTags = "";
         cmbContratante_SSStatusPlanejamento.addItem("", "Stand by", 0);
         cmbContratante_SSStatusPlanejamento.addItem("B", "Stand by", 0);
         cmbContratante_SSStatusPlanejamento.addItem("S", "Solicitada", 0);
         cmbContratante_SSStatusPlanejamento.addItem("E", "Em An�lise", 0);
         cmbContratante_SSStatusPlanejamento.addItem("A", "Em execu��o", 0);
         cmbContratante_SSStatusPlanejamento.addItem("R", "Resolvida", 0);
         cmbContratante_SSStatusPlanejamento.addItem("C", "Conferida", 0);
         cmbContratante_SSStatusPlanejamento.addItem("D", "Rejeitada", 0);
         cmbContratante_SSStatusPlanejamento.addItem("H", "Homologada", 0);
         cmbContratante_SSStatusPlanejamento.addItem("O", "Aceite", 0);
         cmbContratante_SSStatusPlanejamento.addItem("P", "A Pagar", 0);
         cmbContratante_SSStatusPlanejamento.addItem("L", "Liquidada", 0);
         cmbContratante_SSStatusPlanejamento.addItem("X", "Cancelada", 0);
         cmbContratante_SSStatusPlanejamento.addItem("N", "N�o Faturada", 0);
         cmbContratante_SSStatusPlanejamento.addItem("J", "Planejamento", 0);
         cmbContratante_SSStatusPlanejamento.addItem("I", "An�lise Planejamento", 0);
         cmbContratante_SSStatusPlanejamento.addItem("T", "Validacao T�cnica", 0);
         cmbContratante_SSStatusPlanejamento.addItem("Q", "Validacao Qualidade", 0);
         cmbContratante_SSStatusPlanejamento.addItem("G", "Em Homologa��o", 0);
         cmbContratante_SSStatusPlanejamento.addItem("M", "Valida��o Mensura��o", 0);
         cmbContratante_SSStatusPlanejamento.addItem("U", "Rascunho", 0);
         if ( cmbContratante_SSStatusPlanejamento.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) )
            {
               A1732Contratante_SSStatusPlanejamento = "B";
               n1732Contratante_SSStatusPlanejamento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
            }
            A1732Contratante_SSStatusPlanejamento = cmbContratante_SSStatusPlanejamento.getValidValue(A1732Contratante_SSStatusPlanejamento);
            n1732Contratante_SSStatusPlanejamento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
         }
         dynContratante_ServicoSSPadrao.Name = "CONTRATANTE_SERVICOSSPADRAO";
         dynContratante_ServicoSSPadrao.WebTags = "";
         cmbContratante_RetornaSS.Name = "CONTRATANTE_RETORNASS";
         cmbContratante_RetornaSS.WebTags = "";
         cmbContratante_RetornaSS.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_RetornaSS.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_RetornaSS.ItemCount > 0 )
         {
            A1729Contratante_RetornaSS = StringUtil.StrToBool( cmbContratante_RetornaSS.getValidValue(StringUtil.BoolToStr( A1729Contratante_RetornaSS)));
            n1729Contratante_RetornaSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
         }
         cmbContratante_PropostaRqrSrv.Name = "CONTRATANTE_PROPOSTARQRSRV";
         cmbContratante_PropostaRqrSrv.WebTags = "";
         cmbContratante_PropostaRqrSrv.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_PropostaRqrSrv.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_PropostaRqrSrv.ItemCount > 0 )
         {
            if ( (false==A1738Contratante_PropostaRqrSrv) )
            {
               A1738Contratante_PropostaRqrSrv = true;
               n1738Contratante_PropostaRqrSrv = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
            }
            A1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cmbContratante_PropostaRqrSrv.getValidValue(StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv)));
            n1738Contratante_PropostaRqrSrv = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
         }
         cmbContratante_PropostaRqrPrz.Name = "CONTRATANTE_PROPOSTARQRPRZ";
         cmbContratante_PropostaRqrPrz.WebTags = "";
         cmbContratante_PropostaRqrPrz.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_PropostaRqrPrz.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_PropostaRqrPrz.ItemCount > 0 )
         {
            A1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cmbContratante_PropostaRqrPrz.getValidValue(StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz)));
            n1739Contratante_PropostaRqrPrz = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
         }
         cmbContratante_PropostaRqrEsf.Name = "CONTRATANTE_PROPOSTARQRESF";
         cmbContratante_PropostaRqrEsf.WebTags = "";
         cmbContratante_PropostaRqrEsf.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_PropostaRqrEsf.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_PropostaRqrEsf.ItemCount > 0 )
         {
            A1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cmbContratante_PropostaRqrEsf.getValidValue(StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf)));
            n1740Contratante_PropostaRqrEsf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
         }
         cmbContratante_PropostaRqrCnt.Name = "CONTRATANTE_PROPOSTARQRCNT";
         cmbContratante_PropostaRqrCnt.WebTags = "";
         cmbContratante_PropostaRqrCnt.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_PropostaRqrCnt.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_PropostaRqrCnt.ItemCount > 0 )
         {
            A1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cmbContratante_PropostaRqrCnt.getValidValue(StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt)));
            n1741Contratante_PropostaRqrCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
         }
         cmbContratante_PropostaNvlCnt.Name = "CONTRATANTE_PROPOSTANVLCNT";
         cmbContratante_PropostaNvlCnt.WebTags = "";
         cmbContratante_PropostaNvlCnt.addItem("0", "Unica", 0);
         cmbContratante_PropostaNvlCnt.addItem("1", "Estimada", 0);
         cmbContratante_PropostaNvlCnt.addItem("2", "Detalhada", 0);
         if ( cmbContratante_PropostaNvlCnt.ItemCount > 0 )
         {
            if ( (0==A1742Contratante_PropostaNvlCnt) )
            {
               A1742Contratante_PropostaNvlCnt = 0;
               n1742Contratante_PropostaNvlCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
            }
            A1742Contratante_PropostaNvlCnt = (short)(NumberUtil.Val( cmbContratante_PropostaNvlCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0))), "."));
            n1742Contratante_PropostaNvlCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
         }
         cmbContratante_OSAutomatica.Name = "CONTRATANTE_OSAUTOMATICA";
         cmbContratante_OSAutomatica.WebTags = "";
         cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( true), "Autom�tica", 0);
         cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( false), "Manual", 0);
         if ( cmbContratante_OSAutomatica.ItemCount > 0 )
         {
            A593Contratante_OSAutomatica = StringUtil.StrToBool( cmbContratante_OSAutomatica.getValidValue(StringUtil.BoolToStr( A593Contratante_OSAutomatica)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
         }
         cmbContratante_OSGeraOS.Name = "CONTRATANTE_OSGERAOS";
         cmbContratante_OSGeraOS.WebTags = "";
         cmbContratante_OSGeraOS.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_OSGeraOS.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_OSGeraOS.ItemCount > 0 )
         {
            A1757Contratante_OSGeraOS = StringUtil.StrToBool( cmbContratante_OSGeraOS.getValidValue(StringUtil.BoolToStr( A1757Contratante_OSGeraOS)));
            n1757Contratante_OSGeraOS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
         }
         cmbContratante_OSGeraTRP.Name = "CONTRATANTE_OSGERATRP";
         cmbContratante_OSGeraTRP.WebTags = "";
         cmbContratante_OSGeraTRP.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_OSGeraTRP.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_OSGeraTRP.ItemCount > 0 )
         {
            A1758Contratante_OSGeraTRP = StringUtil.StrToBool( cmbContratante_OSGeraTRP.getValidValue(StringUtil.BoolToStr( A1758Contratante_OSGeraTRP)));
            n1758Contratante_OSGeraTRP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
         }
         cmbContratante_OSGeraTRD.Name = "CONTRATANTE_OSGERATRD";
         cmbContratante_OSGeraTRD.WebTags = "";
         cmbContratante_OSGeraTRD.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_OSGeraTRD.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_OSGeraTRD.ItemCount > 0 )
         {
            A1760Contratante_OSGeraTRD = StringUtil.StrToBool( cmbContratante_OSGeraTRD.getValidValue(StringUtil.BoolToStr( A1760Contratante_OSGeraTRD)));
            n1760Contratante_OSGeraTRD = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
         }
         cmbContratante_OSGeraTH.Name = "CONTRATANTE_OSGERATH";
         cmbContratante_OSGeraTH.WebTags = "";
         cmbContratante_OSGeraTH.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_OSGeraTH.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_OSGeraTH.ItemCount > 0 )
         {
            A1759Contratante_OSGeraTH = StringUtil.StrToBool( cmbContratante_OSGeraTH.getValidValue(StringUtil.BoolToStr( A1759Contratante_OSGeraTH)));
            n1759Contratante_OSGeraTH = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
         }
         cmbContratante_OSGeraTR.Name = "CONTRATANTE_OSGERATR";
         cmbContratante_OSGeraTR.WebTags = "";
         cmbContratante_OSGeraTR.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_OSGeraTR.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_OSGeraTR.ItemCount > 0 )
         {
            A1761Contratante_OSGeraTR = StringUtil.StrToBool( cmbContratante_OSGeraTR.getValidValue(StringUtil.BoolToStr( A1761Contratante_OSGeraTR)));
            n1761Contratante_OSGeraTR = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
         }
         cmbContratante_OSHmlgComPnd.Name = "CONTRATANTE_OSHMLGCOMPND";
         cmbContratante_OSHmlgComPnd.WebTags = "";
         cmbContratante_OSHmlgComPnd.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_OSHmlgComPnd.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_OSHmlgComPnd.ItemCount > 0 )
         {
            A1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cmbContratante_OSHmlgComPnd.getValidValue(StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd)));
            n1803Contratante_OSHmlgComPnd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
         }
         cmbContratante_SelecionaResponsavelOS.Name = "CONTRATANTE_SELECIONARESPONSAVELOS";
         cmbContratante_SelecionaResponsavelOS.WebTags = "";
         cmbContratante_SelecionaResponsavelOS.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_SelecionaResponsavelOS.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_SelecionaResponsavelOS.ItemCount > 0 )
         {
            A2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cmbContratante_SelecionaResponsavelOS.getValidValue(StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
         }
         cmbContratante_ExibePF.Name = "CONTRATANTE_EXIBEPF";
         cmbContratante_ExibePF.WebTags = "";
         cmbContratante_ExibePF.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContratante_ExibePF.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContratante_ExibePF.ItemCount > 0 )
         {
            A2090Contratante_ExibePF = StringUtil.StrToBool( cmbContratante_ExibePF.getValidValue(StringUtil.BoolToStr( A2090Contratante_ExibePF)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contratante", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratante_NomeFantasia_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contratante_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Contratante_Codigo = aP1_Contratante_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratante_UsaOSistema = new GXCombobox();
         cmbavRedmine_secure = new GXCombobox();
         cmbavRedmine_versao = new GXCombobox();
         cmbContratante_EmailSdaAut = new GXCombobox();
         cmbContratante_EmailSdaSec = new GXCombobox();
         dynavEstado_uf = new GXCombobox();
         dynMunicipio_Codigo = new GXCombobox();
         chkContratante_Ativo = new GXCheckbox();
         cmbContratante_SSAutomatica = new GXCombobox();
         cmbContratante_SSPrestadoraUnica = new GXCombobox();
         cmbContratante_SSStatusPlanejamento = new GXCombobox();
         dynContratante_ServicoSSPadrao = new GXCombobox();
         cmbContratante_RetornaSS = new GXCombobox();
         cmbContratante_PropostaRqrSrv = new GXCombobox();
         cmbContratante_PropostaRqrPrz = new GXCombobox();
         cmbContratante_PropostaRqrEsf = new GXCombobox();
         cmbContratante_PropostaRqrCnt = new GXCombobox();
         cmbContratante_PropostaNvlCnt = new GXCombobox();
         cmbContratante_OSAutomatica = new GXCombobox();
         cmbContratante_OSGeraOS = new GXCombobox();
         cmbContratante_OSGeraTRP = new GXCombobox();
         cmbContratante_OSGeraTRD = new GXCombobox();
         cmbContratante_OSGeraTH = new GXCombobox();
         cmbContratante_OSGeraTR = new GXCombobox();
         cmbContratante_OSHmlgComPnd = new GXCombobox();
         cmbContratante_SelecionaResponsavelOS = new GXCombobox();
         cmbContratante_ExibePF = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratante_UsaOSistema.ItemCount > 0 )
         {
            A1822Contratante_UsaOSistema = StringUtil.StrToBool( cmbContratante_UsaOSistema.getValidValue(StringUtil.BoolToStr( A1822Contratante_UsaOSistema)));
            n1822Contratante_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
         }
         if ( cmbavRedmine_secure.ItemCount > 0 )
         {
            AV30Redmine_Secure = (short)(NumberUtil.Val( cmbavRedmine_secure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Redmine_Secure", StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0));
         }
         if ( cmbavRedmine_versao.ItemCount > 0 )
         {
            AV28Redmine_Versao = cmbavRedmine_versao.getValidValue(AV28Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Redmine_Versao", AV28Redmine_Versao);
         }
         if ( cmbContratante_EmailSdaAut.ItemCount > 0 )
         {
            A551Contratante_EmailSdaAut = StringUtil.StrToBool( cmbContratante_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A551Contratante_EmailSdaAut)));
            n551Contratante_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
         }
         if ( cmbContratante_EmailSdaSec.ItemCount > 0 )
         {
            A1048Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbContratante_EmailSdaSec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0))), "."));
            n1048Contratante_EmailSdaSec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
         }
         if ( dynavEstado_uf.ItemCount > 0 )
         {
            AV15Estado_UF = dynavEstado_uf.getValidValue(AV15Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
         }
         if ( dynMunicipio_Codigo.ItemCount > 0 )
         {
            A25Municipio_Codigo = (int)(NumberUtil.Val( dynMunicipio_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0))), "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         }
         if ( cmbContratante_SSAutomatica.ItemCount > 0 )
         {
            A1652Contratante_SSAutomatica = StringUtil.StrToBool( cmbContratante_SSAutomatica.getValidValue(StringUtil.BoolToStr( A1652Contratante_SSAutomatica)));
            n1652Contratante_SSAutomatica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
         }
         if ( cmbContratante_SSPrestadoraUnica.ItemCount > 0 )
         {
            A1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cmbContratante_SSPrestadoraUnica.getValidValue(StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica)));
            n1733Contratante_SSPrestadoraUnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
         }
         if ( cmbContratante_SSStatusPlanejamento.ItemCount > 0 )
         {
            A1732Contratante_SSStatusPlanejamento = cmbContratante_SSStatusPlanejamento.getValidValue(A1732Contratante_SSStatusPlanejamento);
            n1732Contratante_SSStatusPlanejamento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
         }
         if ( dynContratante_ServicoSSPadrao.ItemCount > 0 )
         {
            A1727Contratante_ServicoSSPadrao = (int)(NumberUtil.Val( dynContratante_ServicoSSPadrao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0))), "."));
            n1727Contratante_ServicoSSPadrao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
         }
         if ( cmbContratante_RetornaSS.ItemCount > 0 )
         {
            A1729Contratante_RetornaSS = StringUtil.StrToBool( cmbContratante_RetornaSS.getValidValue(StringUtil.BoolToStr( A1729Contratante_RetornaSS)));
            n1729Contratante_RetornaSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
         }
         if ( cmbContratante_PropostaRqrSrv.ItemCount > 0 )
         {
            A1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cmbContratante_PropostaRqrSrv.getValidValue(StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv)));
            n1738Contratante_PropostaRqrSrv = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
         }
         if ( cmbContratante_PropostaRqrPrz.ItemCount > 0 )
         {
            A1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cmbContratante_PropostaRqrPrz.getValidValue(StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz)));
            n1739Contratante_PropostaRqrPrz = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
         }
         if ( cmbContratante_PropostaRqrEsf.ItemCount > 0 )
         {
            A1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cmbContratante_PropostaRqrEsf.getValidValue(StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf)));
            n1740Contratante_PropostaRqrEsf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
         }
         if ( cmbContratante_PropostaRqrCnt.ItemCount > 0 )
         {
            A1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cmbContratante_PropostaRqrCnt.getValidValue(StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt)));
            n1741Contratante_PropostaRqrCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
         }
         if ( cmbContratante_PropostaNvlCnt.ItemCount > 0 )
         {
            A1742Contratante_PropostaNvlCnt = (short)(NumberUtil.Val( cmbContratante_PropostaNvlCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0))), "."));
            n1742Contratante_PropostaNvlCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
         }
         if ( cmbContratante_OSAutomatica.ItemCount > 0 )
         {
            A593Contratante_OSAutomatica = StringUtil.StrToBool( cmbContratante_OSAutomatica.getValidValue(StringUtil.BoolToStr( A593Contratante_OSAutomatica)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
         }
         if ( cmbContratante_OSGeraOS.ItemCount > 0 )
         {
            A1757Contratante_OSGeraOS = StringUtil.StrToBool( cmbContratante_OSGeraOS.getValidValue(StringUtil.BoolToStr( A1757Contratante_OSGeraOS)));
            n1757Contratante_OSGeraOS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
         }
         if ( cmbContratante_OSGeraTRP.ItemCount > 0 )
         {
            A1758Contratante_OSGeraTRP = StringUtil.StrToBool( cmbContratante_OSGeraTRP.getValidValue(StringUtil.BoolToStr( A1758Contratante_OSGeraTRP)));
            n1758Contratante_OSGeraTRP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
         }
         if ( cmbContratante_OSGeraTRD.ItemCount > 0 )
         {
            A1760Contratante_OSGeraTRD = StringUtil.StrToBool( cmbContratante_OSGeraTRD.getValidValue(StringUtil.BoolToStr( A1760Contratante_OSGeraTRD)));
            n1760Contratante_OSGeraTRD = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
         }
         if ( cmbContratante_OSGeraTH.ItemCount > 0 )
         {
            A1759Contratante_OSGeraTH = StringUtil.StrToBool( cmbContratante_OSGeraTH.getValidValue(StringUtil.BoolToStr( A1759Contratante_OSGeraTH)));
            n1759Contratante_OSGeraTH = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
         }
         if ( cmbContratante_OSGeraTR.ItemCount > 0 )
         {
            A1761Contratante_OSGeraTR = StringUtil.StrToBool( cmbContratante_OSGeraTR.getValidValue(StringUtil.BoolToStr( A1761Contratante_OSGeraTR)));
            n1761Contratante_OSGeraTR = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
         }
         if ( cmbContratante_OSHmlgComPnd.ItemCount > 0 )
         {
            A1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cmbContratante_OSHmlgComPnd.getValidValue(StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd)));
            n1803Contratante_OSHmlgComPnd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
         }
         if ( cmbContratante_SelecionaResponsavelOS.ItemCount > 0 )
         {
            A2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cmbContratante_SelecionaResponsavelOS.getValidValue(StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
         }
         if ( cmbContratante_ExibePF.ItemCount > 0 )
         {
            A2090Contratante_ExibePF = StringUtil.StrToBool( cmbContratante_ExibePF.getValidValue(StringUtil.BoolToStr( A2090Contratante_ExibePF)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_05124( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")), ((edtContratante_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratante_Codigo_Visible, edtContratante_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratante.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_LogoNomeArq_Internalname, StringUtil.RTrim( A1125Contratante_LogoNomeArq), StringUtil.RTrim( context.localUtil.Format( A1125Contratante_LogoNomeArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_LogoNomeArq_Jsonclick, 0, "Attribute", "", "", "", edtContratante_LogoNomeArq_Visible, edtContratante_LogoNomeArq_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_Contratante.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_LogoTipoArq_Internalname, StringUtil.RTrim( A1126Contratante_LogoTipoArq), StringUtil.RTrim( context.localUtil.Format( A1126Contratante_LogoTipoArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_LogoTipoArq_Jsonclick, 0, "Attribute", "", "", "", edtContratante_LogoTipoArq_Visible, edtContratante_LogoTipoArq_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_Contratante.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_05124( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_383_05124( true) ;
         }
         return  ;
      }

      protected void wb_table3_383_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_05124e( true) ;
         }
         else
         {
            wb_table1_2_05124e( false) ;
         }
      }

      protected void wb_table3_383_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 386,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 388,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 390,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_383_05124e( true) ;
         }
         else
         {
            wb_table3_383_05124e( false) ;
         }
      }

      protected void wb_table2_5_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_14_05124( true) ;
         }
         return  ;
      }

      protected void wb_table4_14_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_05124e( true) ;
         }
         else
         {
            wb_table2_5_05124e( false) ;
         }
      }

      protected void wb_table4_14_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleDados"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Dados") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"Dados"+"\" style=\"display:none;\">") ;
            wb_table5_20_05124( true) ;
         }
         return  ;
      }

      protected void wb_table5_20_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleSSOS"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "SS / OS") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"SSOS"+"\" style=\"display:none;\">") ;
            wb_table6_239_05124( true) ;
         }
         return  ;
      }

      protected void wb_table6_239_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"TitleFinanceiro"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Financeiro") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSContainer"+"Financeiro"+"\" style=\"display:none;\">") ;
            wb_table7_354_05124( true) ;
         }
         return  ;
      }

      protected void wb_table7_354_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_05124e( true) ;
         }
         else
         {
            wb_table4_14_05124e( false) ;
         }
      }

      protected void wb_table7_354_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ativocirculante_Internalname, "Ativo Circulante", "", "", lblTextblockcontratante_ativocirculante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 359,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_AtivoCirculante_Internalname, StringUtil.LTrim( StringUtil.NToC( A1805Contratante_AtivoCirculante, 18, 5, ",", "")), ((edtContratante_AtivoCirculante_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1805Contratante_AtivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1805Contratante_AtivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,359);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_AtivoCirculante_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_AtivoCirculante_Enabled, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_passivocirculante_Internalname, "Passivo Circulante", "", "", lblTextblockcontratante_passivocirculante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 364,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_PassivoCirculante_Internalname, StringUtil.LTrim( StringUtil.NToC( A1806Contratante_PassivoCirculante, 18, 5, ",", "")), ((edtContratante_PassivoCirculante_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1806Contratante_PassivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1806Contratante_PassivoCirculante, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,364);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_PassivoCirculante_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_PassivoCirculante_Enabled, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_patrimonioliquido_Internalname, "Patrimonio Liquido", "", "", lblTextblockcontratante_patrimonioliquido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 369,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_PatrimonioLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( A1807Contratante_PatrimonioLiquido, 18, 5, ",", "")), ((edtContratante_PatrimonioLiquido_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1807Contratante_PatrimonioLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1807Contratante_PatrimonioLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,369);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_PatrimonioLiquido_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_PatrimonioLiquido_Enabled, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_receitabruta_Internalname, "Receita Bruta", "", "", lblTextblockcontratante_receitabruta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 374,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_ReceitaBruta_Internalname, StringUtil.LTrim( StringUtil.NToC( A1808Contratante_ReceitaBruta, 18, 5, ",", "")), ((edtContratante_ReceitaBruta_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1808Contratante_ReceitaBruta, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1808Contratante_ReceitaBruta, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,374);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_ReceitaBruta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_ReceitaBruta_Enabled, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ttlrltgerencial_Internalname, "Titulo relat�rio gerencial", "", "", lblTextblockcontratante_ttlrltgerencial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 379,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_TtlRltGerencial_Internalname, A2034Contratante_TtlRltGerencial, StringUtil.RTrim( context.localUtil.Format( A2034Contratante_TtlRltGerencial, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,379);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_TtlRltGerencial_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContratante_TtlRltGerencial_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_354_05124e( true) ;
         }
         else
         {
            wb_table7_354_05124e( false) ;
         }
      }

      protected void wb_table6_239_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "SS", 1, 0, "px", 0, "px", "Group", "", "HLP_Contratante.htm");
            wb_table8_243_05124( true) ;
         }
         return  ;
      }

      protected void wb_table8_243_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup6_Internalname, "Proposta", 1, 0, "px", 0, "px", "Group", "", "HLP_Contratante.htm");
            wb_table9_273_05124( true) ;
         }
         return  ;
      }

      protected void wb_table9_273_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup8_Internalname, "OS", 1, 0, "px", 0, "px", "Group", "", "HLP_Contratante.htm");
            wb_table10_303_05124( true) ;
         }
         return  ;
      }

      protected void wb_table10_303_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_239_05124e( true) ;
         }
         else
         {
            wb_table6_239_05124e( false) ;
         }
      }

      protected void wb_table10_303_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osautomatica_Internalname, "Numera��o", "", "", lblTextblockcontratante_osautomatica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 308,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSAutomatica, cmbContratante_OSAutomatica_Internalname, StringUtil.BoolToStr( A593Contratante_OSAutomatica), 1, cmbContratante_OSAutomatica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSAutomatica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,308);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSAutomatica.CurrentValue = StringUtil.BoolToStr( A593Contratante_OSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSAutomatica_Internalname, "Values", (String)(cmbContratante_OSAutomatica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeraos_Internalname, "Gera OS?", "", "", lblTextblockcontratante_osgeraos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 317,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraOS, cmbContratante_OSGeraOS_Internalname, StringUtil.BoolToStr( A1757Contratante_OSGeraOS), 1, cmbContratante_OSGeraOS_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSGeraOS.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,317);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSGeraOS.CurrentValue = StringUtil.BoolToStr( A1757Contratante_OSGeraOS);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraOS_Internalname, "Values", (String)(cmbContratante_OSGeraOS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeratrp_Internalname, "Termo de Recebimento Provis�rio?", "", "", lblTextblockcontratante_osgeratrp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 321,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTRP, cmbContratante_OSGeraTRP_Internalname, StringUtil.BoolToStr( A1758Contratante_OSGeraTRP), 1, cmbContratante_OSGeraTRP_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSGeraTRP.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,321);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSGeraTRP.CurrentValue = StringUtil.BoolToStr( A1758Contratante_OSGeraTRP);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTRP_Internalname, "Values", (String)(cmbContratante_OSGeraTRP.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeratrd_Internalname, "Termo de Recebimento Definitivo?", "", "", lblTextblockcontratante_osgeratrd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 325,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTRD, cmbContratante_OSGeraTRD_Internalname, StringUtil.BoolToStr( A1760Contratante_OSGeraTRD), 1, cmbContratante_OSGeraTRD_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSGeraTRD.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,325);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSGeraTRD.CurrentValue = StringUtil.BoolToStr( A1760Contratante_OSGeraTRD);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTRD_Internalname, "Values", (String)(cmbContratante_OSGeraTRD.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgerath_Internalname, "Termo de Homologa��o?", "", "", lblTextblockcontratante_osgerath_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 330,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTH, cmbContratante_OSGeraTH_Internalname, StringUtil.BoolToStr( A1759Contratante_OSGeraTH), 1, cmbContratante_OSGeraTH_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSGeraTH.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,330);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSGeraTH.CurrentValue = StringUtil.BoolToStr( A1759Contratante_OSGeraTH);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTH_Internalname, "Values", (String)(cmbContratante_OSGeraTH.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_osgeratr_Internalname, "Termo de Recusa?", "", "", lblTextblockcontratante_osgeratr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 334,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSGeraTR, cmbContratante_OSGeraTR_Internalname, StringUtil.BoolToStr( A1761Contratante_OSGeraTR), 1, cmbContratante_OSGeraTR_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSGeraTR.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,334);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSGeraTR.CurrentValue = StringUtil.BoolToStr( A1761Contratante_OSGeraTR);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTR_Internalname, "Values", (String)(cmbContratante_OSGeraTR.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_oshmlgcompnd_Internalname, "Homologa com pend�ncias", "", "", lblTextblockcontratante_oshmlgcompnd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 338,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_OSHmlgComPnd, cmbContratante_OSHmlgComPnd_Internalname, StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd), 1, cmbContratante_OSHmlgComPnd_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_OSHmlgComPnd.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,338);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_OSHmlgComPnd.CurrentValue = StringUtil.BoolToStr( A1803Contratante_OSHmlgComPnd);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSHmlgComPnd_Internalname, "Values", (String)(cmbContratante_OSHmlgComPnd.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_selecionaresponsavelos_Internalname, "Seleciona o Profissional respons�vel pela OS?", "", "", lblTextblockcontratante_selecionaresponsavelos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 343,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SelecionaResponsavelOS, cmbContratante_SelecionaResponsavelOS_Internalname, StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS), 1, cmbContratante_SelecionaResponsavelOS_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_SelecionaResponsavelOS.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,343);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_SelecionaResponsavelOS.CurrentValue = StringUtil.BoolToStr( A2089Contratante_SelecionaResponsavelOS);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SelecionaResponsavelOS_Internalname, "Values", (String)(cmbContratante_SelecionaResponsavelOS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_exibepf_Internalname, "Exibir PF Bruto e L�quido?", "", "", lblTextblockcontratante_exibepf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 347,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_ExibePF, cmbContratante_ExibePF_Internalname, StringUtil.BoolToStr( A2090Contratante_ExibePF), 1, cmbContratante_ExibePF_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_ExibePF.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,347);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_ExibePF.CurrentValue = StringUtil.BoolToStr( A2090Contratante_ExibePF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_ExibePF_Internalname, "Values", (String)(cmbContratante_ExibePF.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_303_05124e( true) ;
         }
         else
         {
            wb_table10_303_05124e( false) ;
         }
      }

      protected void wb_table9_273_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqrsrv_Internalname, "Requer servi�o?", "", "", lblTextblockcontratante_propostarqrsrv_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 278,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrSrv, cmbContratante_PropostaRqrSrv_Internalname, StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv), 1, cmbContratante_PropostaRqrSrv_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_PropostaRqrSrv.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,278);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_PropostaRqrSrv.CurrentValue = StringUtil.BoolToStr( A1738Contratante_PropostaRqrSrv);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrSrv_Internalname, "Values", (String)(cmbContratante_PropostaRqrSrv.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqrprz_Internalname, "Requer prazo?", "", "", lblTextblockcontratante_propostarqrprz_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 282,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrPrz, cmbContratante_PropostaRqrPrz_Internalname, StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz), 1, cmbContratante_PropostaRqrPrz_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_PropostaRqrPrz.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,282);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_PropostaRqrPrz.CurrentValue = StringUtil.BoolToStr( A1739Contratante_PropostaRqrPrz);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrPrz_Internalname, "Values", (String)(cmbContratante_PropostaRqrPrz.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqresf_Internalname, "Requer esfor�o?", "", "", lblTextblockcontratante_propostarqresf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 286,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrEsf, cmbContratante_PropostaRqrEsf_Internalname, StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf), 1, cmbContratante_PropostaRqrEsf_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_PropostaRqrEsf.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,286);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_PropostaRqrEsf.CurrentValue = StringUtil.BoolToStr( A1740Contratante_PropostaRqrEsf);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrEsf_Internalname, "Values", (String)(cmbContratante_PropostaRqrEsf.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostarqrcnt_Internalname, "Requer valores?", "", "", lblTextblockcontratante_propostarqrcnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 291,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaRqrCnt, cmbContratante_PropostaRqrCnt_Internalname, StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt), 1, cmbContratante_PropostaRqrCnt_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_PropostaRqrCnt.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,291);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_PropostaRqrCnt.CurrentValue = StringUtil.BoolToStr( A1741Contratante_PropostaRqrCnt);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrCnt_Internalname, "Values", (String)(cmbContratante_PropostaRqrCnt.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_propostanvlcnt_Internalname, "Nivel dos valores?", "", "", lblTextblockcontratante_propostanvlcnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 295,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_PropostaNvlCnt, cmbContratante_PropostaNvlCnt_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)), 1, cmbContratante_PropostaNvlCnt_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratante_PropostaNvlCnt.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,295);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_PropostaNvlCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaNvlCnt_Internalname, "Values", (String)(cmbContratante_PropostaNvlCnt.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_273_05124e( true) ;
         }
         else
         {
            wb_table9_273_05124e( false) ;
         }
      }

      protected void wb_table8_243_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ssautomatica_Internalname, "Numera��o", "", "", lblTextblockcontratante_ssautomatica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 248,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SSAutomatica, cmbContratante_SSAutomatica_Internalname, StringUtil.BoolToStr( A1652Contratante_SSAutomatica), 1, cmbContratante_SSAutomatica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_SSAutomatica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,248);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_SSAutomatica.CurrentValue = StringUtil.BoolToStr( A1652Contratante_SSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SSAutomatica_Internalname, "Values", (String)(cmbContratante_SSAutomatica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ssprestadoraunica_Internalname, "Prestadora �nica?", "", "", lblTextblockcontratante_ssprestadoraunica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 252,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SSPrestadoraUnica, cmbContratante_SSPrestadoraUnica_Internalname, StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica), 1, cmbContratante_SSPrestadoraUnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_SSPrestadoraUnica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,252);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_SSPrestadoraUnica.CurrentValue = StringUtil.BoolToStr( A1733Contratante_SSPrestadoraUnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SSPrestadoraUnica_Internalname, "Values", (String)(cmbContratante_SSPrestadoraUnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ssstatusplanejamento_Internalname, "Status em planejamento", "", "", lblTextblockcontratante_ssstatusplanejamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 256,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_SSStatusPlanejamento, cmbContratante_SSStatusPlanejamento_Internalname, StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento), 1, cmbContratante_SSStatusPlanejamento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratante_SSStatusPlanejamento.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,256);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_SSStatusPlanejamento.CurrentValue = StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SSStatusPlanejamento_Internalname, "Values", (String)(cmbContratante_SSStatusPlanejamento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_servicosspadrao_Internalname, "Servico SS padr�o", "", "", lblTextblockcontratante_servicosspadrao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 261,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratante_ServicoSSPadrao, dynContratante_ServicoSSPadrao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)), 1, dynContratante_ServicoSSPadrao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratante_ServicoSSPadrao.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,261);\"", "", true, "HLP_Contratante.htm");
            dynContratante_ServicoSSPadrao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratante_ServicoSSPadrao_Internalname, "Values", (String)(dynContratante_ServicoSSPadrao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_retornass_Internalname, "Retorna SS?", "", "", lblTextblockcontratante_retornass_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 265,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_RetornaSS, cmbContratante_RetornaSS_Internalname, StringUtil.BoolToStr( A1729Contratante_RetornaSS), 1, cmbContratante_RetornaSS_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_RetornaSS.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,265);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_RetornaSS.CurrentValue = StringUtil.BoolToStr( A1729Contratante_RetornaSS);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_RetornaSS_Internalname, "Values", (String)(cmbContratante_RetornaSS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_243_05124e( true) ;
         }
         else
         {
            wb_table8_243_05124e( false) ;
         }
      }

      protected void wb_table5_20_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable9_Internalname, tblUnnamedtable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_razaosocial_Internalname, "Raz�o Social", "", "", lblTextblockcontratante_razaosocial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_RazaoSocial_Internalname, StringUtil.RTrim( A9Contratante_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_RazaoSocial_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_RazaoSocial_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_nomefantasia_Internalname, "Nome Fantasia", "", "", lblTextblockcontratante_nomefantasia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_NomeFantasia_Internalname, StringUtil.RTrim( A10Contratante_NomeFantasia), StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_NomeFantasia_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContratante_NomeFantasia_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_cnpj_Internalname, "CNPJ", "", "", lblTextblockcontratante_cnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratante_CNPJ_Internalname, A12Contratante_CNPJ, StringUtil.RTrim( context.localUtil.Format( A12Contratante_CNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_CNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_CNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ie_Internalname, "Insc. Estadual", "", "", lblTextblockcontratante_ie_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_IE_Internalname, StringUtil.RTrim( A11Contratante_IE), StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_IE_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_IE_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "IE", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_telefone_Internalname, "Telefone", "", "", lblTextblockcontratante_telefone_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_Telefone_Internalname, StringUtil.RTrim( A31Contratante_Telefone), StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtContratante_Telefone_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_Telefone_Enabled, 0, "tel", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "Phone", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ramal_Internalname, "Ramal", "", "", lblTextblockcontratante_ramal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_Ramal_Internalname, StringUtil.RTrim( A32Contratante_Ramal), StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_Ramal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_Ramal_Enabled, 0, "text", "", 100, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Ramal", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_fax_Internalname, "Fax", "", "", lblTextblockcontratante_fax_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A33Contratante_Fax);
            }
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_Fax_Internalname, StringUtil.RTrim( A33Contratante_Fax), StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtContratante_Fax_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_Fax_Enabled, 0, "tel", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "Phone", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_website_Internalname, "Site", "", "", lblTextblockcontratante_website_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_WebSite_Internalname, A13Contratante_WebSite, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_WebSite_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_WebSite_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_email_Internalname, "Email", "", "", lblTextblockcontratante_email_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_Email_Internalname, A14Contratante_Email, StringUtil.RTrim( context.localUtil.Format( A14Contratante_Email, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "mailto:"+A14Contratante_Email, "", "", "", edtContratante_Email_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_Email_Enabled, 0, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "Email", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_banconome_Internalname, "Banco", "", "", lblTextblockcontratante_banconome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_BancoNome_Internalname, StringUtil.RTrim( A338Contratante_BancoNome), StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_BancoNome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_BancoNome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_banconro_Internalname, "N�mero", "", "", lblTextblockcontratante_banconro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_BancoNro_Internalname, StringUtil.RTrim( A339Contratante_BancoNro), StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_BancoNro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_BancoNro_Enabled, 0, "text", "", 80, "px", 1, "row", 6, 0, 0, 0, 1, -1, -1, true, "NumeroBanco", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_agencianome_Internalname, "Ag�ncia", "", "", lblTextblockcontratante_agencianome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_AgenciaNome_Internalname, StringUtil.RTrim( A336Contratante_AgenciaNome), StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_AgenciaNome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_AgenciaNome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_agencianro_Internalname, "N�mero", "", "", lblTextblockcontratante_agencianro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_AgenciaNro_Internalname, StringUtil.RTrim( A337Contratante_AgenciaNro), StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_AgenciaNro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_AgenciaNro_Enabled, 0, "text", "", 80, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Agencia", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_contacorrente_Internalname, "Conta Corrente", "", "", lblTextblockcontratante_contacorrente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_ContaCorrente_Internalname, StringUtil.RTrim( A16Contratante_ContaCorrente), StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_ContaCorrente_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_ContaCorrente_Enabled, 0, "text", "", 80, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "ContaCorrente", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_iniciodoexpediente_Internalname, "Expediente", "", "", lblTextblockcontratante_iniciodoexpediente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            wb_table11_94_05124( true) ;
         }
         return  ;
      }

      protected void wb_table11_94_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_logoarquivo_Internalname, "Logomarca", "", "", lblTextblockcontratante_logoarquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'',0)\"";
            edtContratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            edtContratante_LogoArquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Filetype", edtContratante_LogoArquivo_Filetype);
            edtContratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Filetype", edtContratante_LogoArquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1124Contratante_LogoArquivo)) )
            {
               gxblobfileaux.Source = A1124Contratante_LogoArquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContratante_LogoArquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContratante_LogoArquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A1124Contratante_LogoArquivo = gxblobfileaux.GetAbsoluteName();
                  n1124Contratante_LogoArquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1124Contratante_LogoArquivo", A1124Contratante_LogoArquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1124Contratante_LogoArquivo));
                  edtContratante_LogoArquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Filetype", edtContratante_LogoArquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1124Contratante_LogoArquivo));
            }
            GxWebStd.gx_blob_field( context, edtContratante_LogoArquivo_Internalname, StringUtil.RTrim( A1124Contratante_LogoArquivo), context.PathToRelativeUrl( A1124Contratante_LogoArquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtContratante_LogoArquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContratante_LogoArquivo_Filetype)) ? A1124Contratante_LogoArquivo : edtContratante_LogoArquivo_Filetype)) : edtContratante_LogoArquivo_Contenttype), true, edtContratante_LogoArquivo_Linktarget, edtContratante_LogoArquivo_Parameters, edtContratante_LogoArquivo_Display, edtContratante_LogoArquivo_Enabled, 1, "", edtContratante_LogoArquivo_Tooltiptext, 0, -1, 250, "px", 60, "px", 0, 0, 0, edtContratante_LogoArquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "", "", "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_usaosistema_Internalname, "Usa o Sistema", "", "", lblTextblockcontratante_usaosistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_UsaOSistema, cmbContratante_UsaOSistema_Internalname, StringUtil.BoolToStr( A1822Contratante_UsaOSistema), 1, cmbContratante_UsaOSistema_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_UsaOSistema.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_UsaOSistema.CurrentValue = StringUtil.BoolToStr( A1822Contratante_UsaOSistema);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_UsaOSistema_Internalname, "Values", (String)(cmbContratante_UsaOSistema.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" rowspan=\"3\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup10_Internalname, "Redmine", 1, 0, "px", 0, "px", "Group", "", "HLP_Contratante.htm");
            wb_table12_123_05124( true) ;
         }
         return  ;
      }

      protected void wb_table12_123_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" rowspan=\"3\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup11_Internalname, "Servidor de E-mail:", 1, 0, "px", 0, "px", "Group", "", "HLP_Contratante.htm");
            wb_table13_181_05124( true) ;
         }
         return  ;
      }

      protected void wb_table13_181_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockestado_uf_Internalname, "UF", "", "", lblTextblockestado_uf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 228,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf, dynavEstado_uf_Internalname, StringUtil.RTrim( AV15Estado_UF), 1, dynavEstado_uf_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, dynavEstado_uf.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,228);\"", "", true, "HLP_Contratante.htm");
            dynavEstado_uf.CurrentValue = StringUtil.RTrim( AV15Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Values", (String)(dynavEstado_uf.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_codigo_Internalname, "Municipio", "", "", lblTextblockmunicipio_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 232,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynMunicipio_Codigo, dynMunicipio_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)), 1, dynMunicipio_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynMunicipio_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,232);\"", "", true, "HLP_Contratante.htm");
            dynMunicipio_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMunicipio_Codigo_Internalname, "Values", (String)(dynMunicipio_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_ativo_Internalname, "Ativo?", "", "", lblTextblockcontratante_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratante_ativo_Visible, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratante_Ativo_Internalname, StringUtil.BoolToStr( A30Contratante_Ativo), "", "", chkContratante_Ativo.Visible, chkContratante_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(236, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,236);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_05124e( true) ;
         }
         else
         {
            wb_table5_20_05124e( false) ;
         }
      }

      protected void wb_table13_181_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblEmailsaida_Internalname, tblEmailsaida_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdahost_Internalname, "Host SMTP", "", "", lblTextblockcontratante_emailsdahost_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_EmailSdaHost_Internalname, A547Contratante_EmailSdaHost, StringUtil.RTrim( context.localUtil.Format( A547Contratante_EmailSdaHost, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,186);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_EmailSdaHost_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_EmailSdaHost_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdauser_Internalname, "Usu�rio", "", "", lblTextblockcontratante_emailsdauser_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 190,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_EmailSdaUser_Internalname, A548Contratante_EmailSdaUser, StringUtil.RTrim( context.localUtil.Format( A548Contratante_EmailSdaUser, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,190);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_EmailSdaUser_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_EmailSdaUser_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdapass_Internalname, "Senha", "", "", lblTextblockcontratante_emailsdapass_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_EmailSdaPass_Internalname, A549Contratante_EmailSdaPass, StringUtil.RTrim( context.localUtil.Format( A549Contratante_EmailSdaPass, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,194);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_EmailSdaPass_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_EmailSdaPass_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, -1, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdaport_Internalname, "Porta", "", "", lblTextblockcontratante_emailsdaport_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 200,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_EmailSdaPort_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ",", "")), ((edtContratante_EmailSdaPort_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A552Contratante_EmailSdaPort), "ZZZ9")) : context.localUtil.Format( (decimal)(A552Contratante_EmailSdaPort), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,200);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_EmailSdaPort_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratante_EmailSdaPort_Enabled, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdaaut_Internalname, "Autentica��o", "", "", lblTextblockcontratante_emailsdaaut_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 204,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_EmailSdaAut, cmbContratante_EmailSdaAut_Internalname, StringUtil.BoolToStr( A551Contratante_EmailSdaAut), 1, cmbContratante_EmailSdaAut_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContratante_EmailSdaAut.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,204);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_EmailSdaAut.CurrentValue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_EmailSdaAut_Internalname, "Values", (String)(cmbContratante_EmailSdaAut.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_emailsdasec_Internalname, "Seguran�a", "", "", lblTextblockcontratante_emailsdasec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 208,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratante_EmailSdaSec, cmbContratante_EmailSdaSec_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)), 1, cmbContratante_EmailSdaSec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratante_EmailSdaSec.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,208);\"", "", true, "HLP_Contratante.htm");
            cmbContratante_EmailSdaSec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_EmailSdaSec_Internalname, "Values", (String)(cmbContratante_EmailSdaSec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 210,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntestaremail_Internalname, "", "Testar envio para usu�rio logado", bttBtntestaremail_Jsonclick, 5, "Envia um email de teste usando os par�metros gravados", "", StyleString, ClassString, bttBtntestaremail_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOTESTAREMAIL\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"8\" >") ;
            wb_table14_214_05124( true) ;
         }
         return  ;
      }

      protected void wb_table14_214_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_181_05124e( true) ;
         }
         else
         {
            wb_table13_181_05124e( false) ;
         }
      }

      protected void wb_table14_214_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtestecsharp_Internalname, tblTablemergedtestecsharp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 217,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntestecsharp_Internalname, "", "Teste C# para:", bttBtntestecsharp_Jsonclick, 5, "Teste alternativo em C# usando os par�metros na tela", "", StyleString, ClassString, bttBtntestecsharp_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOTESTECSHARP\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemailto_Internalname, "", "", "", lblTextblockemailto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 221,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmailto_Internalname, StringUtil.RTrim( AV44EmailTo), StringUtil.RTrim( context.localUtil.Format( AV44EmailTo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,221);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "seuemail@servidor.com", edtavEmailto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavEmailto_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_214_05124e( true) ;
         }
         else
         {
            wb_table14_214_05124e( false) ;
         }
      }

      protected void wb_table12_123_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblRedmine_Internalname, tblRedmine_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_user_Internalname, "Usu�rio", "", "", lblTextblockredmine_user_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRedmine_user_Internalname, StringUtil.RTrim( AV31Redmine_User), StringUtil.RTrim( context.localUtil.Format( AV31Redmine_User, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRedmine_user_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRedmine_user_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_key_Internalname, "Key", "", "", lblTextblockredmine_key_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRedmine_key_Internalname, StringUtil.RTrim( AV26Redmine_Key), StringUtil.RTrim( context.localUtil.Format( AV26Redmine_Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRedmine_key_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRedmine_key_Enabled, 0, "text", "", 250, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_secure_Internalname, "Host", "", "", lblTextblockredmine_secure_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table15_143_05124( true) ;
         }
         return  ;
      }

      protected void wb_table15_143_05124e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_url_Internalname, "Url", "", "", lblTextblockredmine_url_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRedmine_url_Internalname, StringUtil.RTrim( AV25Redmine_Url), StringUtil.RTrim( context.localUtil.Format( AV25Redmine_Url, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRedmine_url_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRedmine_url_Enabled, 0, "text", "", 250, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_versao_Internalname, "Vers�o", "", "", lblTextblockredmine_versao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRedmine_versao, cmbavRedmine_versao_Internalname, StringUtil.RTrim( AV28Redmine_Versao), 1, cmbavRedmine_versao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavRedmine_versao.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,156);\"", "", true, "HLP_Contratante.htm");
            cmbavRedmine_versao.CurrentValue = StringUtil.RTrim( AV28Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRedmine_versao_Internalname, "Values", (String)(cmbavRedmine_versao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_camposistema_Internalname, "Campo de Sistema", "", "", lblTextblockredmine_camposistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRedmine_camposistema_Internalname, StringUtil.RTrim( AV32Redmine_CampoSistema), StringUtil.RTrim( context.localUtil.Format( AV32Redmine_CampoSistema, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRedmine_camposistema_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRedmine_camposistema_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_camposervico_Internalname, "Campo de Servi�o", "", "", lblTextblockredmine_camposervico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRedmine_camposervico_Internalname, StringUtil.RTrim( AV33Redmine_CampoServico), StringUtil.RTrim( context.localUtil.Format( AV33Redmine_CampoServico, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRedmine_camposervico_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRedmine_camposervico_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_123_05124e( true) ;
         }
         else
         {
            wb_table12_123_05124e( false) ;
         }
      }

      protected void wb_table15_143_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedredmine_secure_Internalname, tblTablemergedredmine_secure_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRedmine_secure, cmbavRedmine_secure_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0)), 1, cmbavRedmine_secure_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavRedmine_secure.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", "", true, "HLP_Contratante.htm");
            cmbavRedmine_secure.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRedmine_secure_Internalname, "Values", (String)(cmbavRedmine_secure.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRedmine_host_Internalname, StringUtil.RTrim( AV24Redmine_Host), StringUtil.RTrim( context.localUtil.Format( AV24Redmine_Host, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRedmine_host_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRedmine_host_Enabled, 0, "text", "", 250, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_143_05124e( true) ;
         }
         else
         {
            wb_table15_143_05124e( false) ;
         }
      }

      protected void wb_table11_94_05124( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratante_iniciodoexpediente_Internalname, tblTablemergedcontratante_iniciodoexpediente_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_InicioDoExpediente_Internalname, context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1448Contratante_InicioDoExpediente, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_InicioDoExpediente_Jsonclick, 0, "BootstrapAttributeDateDateTime", "", "", "", 1, edtContratante_InicioDoExpediente_Enabled, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "Time", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratante_fimdoexpediente_Internalname, "�s", "", "", lblTextblockcontratante_fimdoexpediente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratante_FimDoExpediente_Internalname, context.localUtil.TToC( A1192Contratante_FimDoExpediente, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1192Contratante_FimDoExpediente, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratante_FimDoExpediente_Jsonclick, 0, "BootstrapAttributeDateDateTime", "", "", "", 1, edtContratante_FimDoExpediente_Enabled, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "Time", "right", false, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratante_fimdoexpediente_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratante_fimdoexpediente_righttext_Internalname, "��hs.", "", "", lblContratante_fimdoexpediente_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_94_05124e( true) ;
         }
         else
         {
            wb_table11_94_05124e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11052 */
         E11052 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
               n9Contratante_RazaoSocial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
               A10Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtContratante_NomeFantasia_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
               A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
               n12Contratante_CNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
               A11Contratante_IE = cgiGet( edtContratante_IE_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Contratante_IE", A11Contratante_IE);
               A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31Contratante_Telefone", A31Contratante_Telefone);
               A32Contratante_Ramal = cgiGet( edtContratante_Ramal_Internalname);
               n32Contratante_Ramal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32Contratante_Ramal", A32Contratante_Ramal);
               n32Contratante_Ramal = (String.IsNullOrEmpty(StringUtil.RTrim( A32Contratante_Ramal)) ? true : false);
               A33Contratante_Fax = cgiGet( edtContratante_Fax_Internalname);
               n33Contratante_Fax = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33Contratante_Fax", A33Contratante_Fax);
               n33Contratante_Fax = (String.IsNullOrEmpty(StringUtil.RTrim( A33Contratante_Fax)) ? true : false);
               A13Contratante_WebSite = cgiGet( edtContratante_WebSite_Internalname);
               n13Contratante_WebSite = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Contratante_WebSite", A13Contratante_WebSite);
               n13Contratante_WebSite = (String.IsNullOrEmpty(StringUtil.RTrim( A13Contratante_WebSite)) ? true : false);
               A14Contratante_Email = cgiGet( edtContratante_Email_Internalname);
               n14Contratante_Email = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Contratante_Email", A14Contratante_Email);
               n14Contratante_Email = (String.IsNullOrEmpty(StringUtil.RTrim( A14Contratante_Email)) ? true : false);
               A338Contratante_BancoNome = StringUtil.Upper( cgiGet( edtContratante_BancoNome_Internalname));
               n338Contratante_BancoNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A338Contratante_BancoNome", A338Contratante_BancoNome);
               n338Contratante_BancoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A338Contratante_BancoNome)) ? true : false);
               A339Contratante_BancoNro = cgiGet( edtContratante_BancoNro_Internalname);
               n339Contratante_BancoNro = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A339Contratante_BancoNro", A339Contratante_BancoNro);
               n339Contratante_BancoNro = (String.IsNullOrEmpty(StringUtil.RTrim( A339Contratante_BancoNro)) ? true : false);
               A336Contratante_AgenciaNome = StringUtil.Upper( cgiGet( edtContratante_AgenciaNome_Internalname));
               n336Contratante_AgenciaNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A336Contratante_AgenciaNome", A336Contratante_AgenciaNome);
               n336Contratante_AgenciaNome = (String.IsNullOrEmpty(StringUtil.RTrim( A336Contratante_AgenciaNome)) ? true : false);
               A337Contratante_AgenciaNro = cgiGet( edtContratante_AgenciaNro_Internalname);
               n337Contratante_AgenciaNro = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A337Contratante_AgenciaNro", A337Contratante_AgenciaNro);
               n337Contratante_AgenciaNro = (String.IsNullOrEmpty(StringUtil.RTrim( A337Contratante_AgenciaNro)) ? true : false);
               A16Contratante_ContaCorrente = cgiGet( edtContratante_ContaCorrente_Internalname);
               n16Contratante_ContaCorrente = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16Contratante_ContaCorrente", A16Contratante_ContaCorrente);
               n16Contratante_ContaCorrente = (String.IsNullOrEmpty(StringUtil.RTrim( A16Contratante_ContaCorrente)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratante_InicioDoExpediente_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"Inicio do Expediente"}), 1, "CONTRATANTE_INICIODOEXPEDIENTE");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_InicioDoExpediente_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
                  n1448Contratante_InicioDoExpediente = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1448Contratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContratante_InicioDoExpediente_Internalname)));
                  n1448Contratante_InicioDoExpediente = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               }
               n1448Contratante_InicioDoExpediente = ((DateTime.MinValue==A1448Contratante_InicioDoExpediente) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratante_FimDoExpediente_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"Fim do Expediente"}), 1, "CONTRATANTE_FIMDOEXPEDIENTE");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_FimDoExpediente_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
                  n1192Contratante_FimDoExpediente = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1192Contratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContratante_FimDoExpediente_Internalname)));
                  n1192Contratante_FimDoExpediente = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               }
               n1192Contratante_FimDoExpediente = ((DateTime.MinValue==A1192Contratante_FimDoExpediente) ? true : false);
               A1124Contratante_LogoArquivo = cgiGet( edtContratante_LogoArquivo_Internalname);
               n1124Contratante_LogoArquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1124Contratante_LogoArquivo", A1124Contratante_LogoArquivo);
               n1124Contratante_LogoArquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1124Contratante_LogoArquivo)) ? true : false);
               cmbContratante_UsaOSistema.CurrentValue = cgiGet( cmbContratante_UsaOSistema_Internalname);
               A1822Contratante_UsaOSistema = StringUtil.StrToBool( cgiGet( cmbContratante_UsaOSistema_Internalname));
               n1822Contratante_UsaOSistema = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
               n1822Contratante_UsaOSistema = ((false==A1822Contratante_UsaOSistema) ? true : false);
               AV31Redmine_User = cgiGet( edtavRedmine_user_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Redmine_User", AV31Redmine_User);
               AV26Redmine_Key = cgiGet( edtavRedmine_key_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Redmine_Key", AV26Redmine_Key);
               cmbavRedmine_secure.CurrentValue = cgiGet( cmbavRedmine_secure_Internalname);
               AV30Redmine_Secure = (short)(NumberUtil.Val( cgiGet( cmbavRedmine_secure_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Redmine_Secure", StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0));
               AV24Redmine_Host = cgiGet( edtavRedmine_host_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Redmine_Host", AV24Redmine_Host);
               AV25Redmine_Url = cgiGet( edtavRedmine_url_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Redmine_Url", AV25Redmine_Url);
               cmbavRedmine_versao.CurrentValue = cgiGet( cmbavRedmine_versao_Internalname);
               AV28Redmine_Versao = cgiGet( cmbavRedmine_versao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Redmine_Versao", AV28Redmine_Versao);
               AV32Redmine_CampoSistema = cgiGet( edtavRedmine_camposistema_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Redmine_CampoSistema", AV32Redmine_CampoSistema);
               AV33Redmine_CampoServico = cgiGet( edtavRedmine_camposervico_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Redmine_CampoServico", AV33Redmine_CampoServico);
               A547Contratante_EmailSdaHost = cgiGet( edtContratante_EmailSdaHost_Internalname);
               n547Contratante_EmailSdaHost = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A547Contratante_EmailSdaHost", A547Contratante_EmailSdaHost);
               n547Contratante_EmailSdaHost = (String.IsNullOrEmpty(StringUtil.RTrim( A547Contratante_EmailSdaHost)) ? true : false);
               A548Contratante_EmailSdaUser = cgiGet( edtContratante_EmailSdaUser_Internalname);
               n548Contratante_EmailSdaUser = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A548Contratante_EmailSdaUser", A548Contratante_EmailSdaUser);
               n548Contratante_EmailSdaUser = (String.IsNullOrEmpty(StringUtil.RTrim( A548Contratante_EmailSdaUser)) ? true : false);
               A549Contratante_EmailSdaPass = cgiGet( edtContratante_EmailSdaPass_Internalname);
               n549Contratante_EmailSdaPass = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
               n549Contratante_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratante_EmailSdaPort_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratante_EmailSdaPort_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTE_EMAILSDAPORT");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_EmailSdaPort_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A552Contratante_EmailSdaPort = 0;
                  n552Contratante_EmailSdaPort = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
               }
               else
               {
                  A552Contratante_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( edtContratante_EmailSdaPort_Internalname), ",", "."));
                  n552Contratante_EmailSdaPort = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
               }
               n552Contratante_EmailSdaPort = ((0==A552Contratante_EmailSdaPort) ? true : false);
               cmbContratante_EmailSdaAut.CurrentValue = cgiGet( cmbContratante_EmailSdaAut_Internalname);
               A551Contratante_EmailSdaAut = StringUtil.StrToBool( cgiGet( cmbContratante_EmailSdaAut_Internalname));
               n551Contratante_EmailSdaAut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
               n551Contratante_EmailSdaAut = ((false==A551Contratante_EmailSdaAut) ? true : false);
               cmbContratante_EmailSdaSec.CurrentValue = cgiGet( cmbContratante_EmailSdaSec_Internalname);
               A1048Contratante_EmailSdaSec = (short)(NumberUtil.Val( cgiGet( cmbContratante_EmailSdaSec_Internalname), "."));
               n1048Contratante_EmailSdaSec = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
               n1048Contratante_EmailSdaSec = ((0==A1048Contratante_EmailSdaSec) ? true : false);
               AV44EmailTo = cgiGet( edtavEmailto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44EmailTo", AV44EmailTo);
               dynavEstado_uf.CurrentValue = cgiGet( dynavEstado_uf_Internalname);
               AV15Estado_UF = cgiGet( dynavEstado_uf_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
               dynMunicipio_Codigo.CurrentValue = cgiGet( dynMunicipio_Codigo_Internalname);
               A25Municipio_Codigo = (int)(NumberUtil.Val( cgiGet( dynMunicipio_Codigo_Internalname), "."));
               n25Municipio_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               n25Municipio_Codigo = ((0==A25Municipio_Codigo) ? true : false);
               A30Contratante_Ativo = StringUtil.StrToBool( cgiGet( chkContratante_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30Contratante_Ativo", A30Contratante_Ativo);
               cmbContratante_SSAutomatica.CurrentValue = cgiGet( cmbContratante_SSAutomatica_Internalname);
               A1652Contratante_SSAutomatica = StringUtil.StrToBool( cgiGet( cmbContratante_SSAutomatica_Internalname));
               n1652Contratante_SSAutomatica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
               n1652Contratante_SSAutomatica = ((false==A1652Contratante_SSAutomatica) ? true : false);
               cmbContratante_SSPrestadoraUnica.CurrentValue = cgiGet( cmbContratante_SSPrestadoraUnica_Internalname);
               A1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cgiGet( cmbContratante_SSPrestadoraUnica_Internalname));
               n1733Contratante_SSPrestadoraUnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
               n1733Contratante_SSPrestadoraUnica = ((false==A1733Contratante_SSPrestadoraUnica) ? true : false);
               cmbContratante_SSStatusPlanejamento.CurrentValue = cgiGet( cmbContratante_SSStatusPlanejamento_Internalname);
               A1732Contratante_SSStatusPlanejamento = cgiGet( cmbContratante_SSStatusPlanejamento_Internalname);
               n1732Contratante_SSStatusPlanejamento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
               n1732Contratante_SSStatusPlanejamento = (String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) ? true : false);
               dynContratante_ServicoSSPadrao.CurrentValue = cgiGet( dynContratante_ServicoSSPadrao_Internalname);
               A1727Contratante_ServicoSSPadrao = (int)(NumberUtil.Val( cgiGet( dynContratante_ServicoSSPadrao_Internalname), "."));
               n1727Contratante_ServicoSSPadrao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
               n1727Contratante_ServicoSSPadrao = ((0==A1727Contratante_ServicoSSPadrao) ? true : false);
               cmbContratante_RetornaSS.CurrentValue = cgiGet( cmbContratante_RetornaSS_Internalname);
               A1729Contratante_RetornaSS = StringUtil.StrToBool( cgiGet( cmbContratante_RetornaSS_Internalname));
               n1729Contratante_RetornaSS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
               n1729Contratante_RetornaSS = ((false==A1729Contratante_RetornaSS) ? true : false);
               cmbContratante_PropostaRqrSrv.CurrentValue = cgiGet( cmbContratante_PropostaRqrSrv_Internalname);
               A1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrSrv_Internalname));
               n1738Contratante_PropostaRqrSrv = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
               n1738Contratante_PropostaRqrSrv = ((false==A1738Contratante_PropostaRqrSrv) ? true : false);
               cmbContratante_PropostaRqrPrz.CurrentValue = cgiGet( cmbContratante_PropostaRqrPrz_Internalname);
               A1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrPrz_Internalname));
               n1739Contratante_PropostaRqrPrz = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
               n1739Contratante_PropostaRqrPrz = ((false==A1739Contratante_PropostaRqrPrz) ? true : false);
               cmbContratante_PropostaRqrEsf.CurrentValue = cgiGet( cmbContratante_PropostaRqrEsf_Internalname);
               A1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrEsf_Internalname));
               n1740Contratante_PropostaRqrEsf = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
               n1740Contratante_PropostaRqrEsf = ((false==A1740Contratante_PropostaRqrEsf) ? true : false);
               cmbContratante_PropostaRqrCnt.CurrentValue = cgiGet( cmbContratante_PropostaRqrCnt_Internalname);
               A1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cgiGet( cmbContratante_PropostaRqrCnt_Internalname));
               n1741Contratante_PropostaRqrCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
               n1741Contratante_PropostaRqrCnt = ((false==A1741Contratante_PropostaRqrCnt) ? true : false);
               cmbContratante_PropostaNvlCnt.CurrentValue = cgiGet( cmbContratante_PropostaNvlCnt_Internalname);
               A1742Contratante_PropostaNvlCnt = (short)(NumberUtil.Val( cgiGet( cmbContratante_PropostaNvlCnt_Internalname), "."));
               n1742Contratante_PropostaNvlCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
               n1742Contratante_PropostaNvlCnt = ((0==A1742Contratante_PropostaNvlCnt) ? true : false);
               cmbContratante_OSAutomatica.CurrentValue = cgiGet( cmbContratante_OSAutomatica_Internalname);
               A593Contratante_OSAutomatica = StringUtil.StrToBool( cgiGet( cmbContratante_OSAutomatica_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
               cmbContratante_OSGeraOS.CurrentValue = cgiGet( cmbContratante_OSGeraOS_Internalname);
               A1757Contratante_OSGeraOS = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraOS_Internalname));
               n1757Contratante_OSGeraOS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
               n1757Contratante_OSGeraOS = ((false==A1757Contratante_OSGeraOS) ? true : false);
               cmbContratante_OSGeraTRP.CurrentValue = cgiGet( cmbContratante_OSGeraTRP_Internalname);
               A1758Contratante_OSGeraTRP = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTRP_Internalname));
               n1758Contratante_OSGeraTRP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
               n1758Contratante_OSGeraTRP = ((false==A1758Contratante_OSGeraTRP) ? true : false);
               cmbContratante_OSGeraTRD.CurrentValue = cgiGet( cmbContratante_OSGeraTRD_Internalname);
               A1760Contratante_OSGeraTRD = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTRD_Internalname));
               n1760Contratante_OSGeraTRD = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
               n1760Contratante_OSGeraTRD = ((false==A1760Contratante_OSGeraTRD) ? true : false);
               cmbContratante_OSGeraTH.CurrentValue = cgiGet( cmbContratante_OSGeraTH_Internalname);
               A1759Contratante_OSGeraTH = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTH_Internalname));
               n1759Contratante_OSGeraTH = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
               n1759Contratante_OSGeraTH = ((false==A1759Contratante_OSGeraTH) ? true : false);
               cmbContratante_OSGeraTR.CurrentValue = cgiGet( cmbContratante_OSGeraTR_Internalname);
               A1761Contratante_OSGeraTR = StringUtil.StrToBool( cgiGet( cmbContratante_OSGeraTR_Internalname));
               n1761Contratante_OSGeraTR = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
               n1761Contratante_OSGeraTR = ((false==A1761Contratante_OSGeraTR) ? true : false);
               cmbContratante_OSHmlgComPnd.CurrentValue = cgiGet( cmbContratante_OSHmlgComPnd_Internalname);
               A1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cgiGet( cmbContratante_OSHmlgComPnd_Internalname));
               n1803Contratante_OSHmlgComPnd = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
               n1803Contratante_OSHmlgComPnd = ((false==A1803Contratante_OSHmlgComPnd) ? true : false);
               cmbContratante_SelecionaResponsavelOS.CurrentValue = cgiGet( cmbContratante_SelecionaResponsavelOS_Internalname);
               A2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cgiGet( cmbContratante_SelecionaResponsavelOS_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
               cmbContratante_ExibePF.CurrentValue = cgiGet( cmbContratante_ExibePF_Internalname);
               A2090Contratante_ExibePF = StringUtil.StrToBool( cgiGet( cmbContratante_ExibePF_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratante_AtivoCirculante_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratante_AtivoCirculante_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTE_ATIVOCIRCULANTE");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_AtivoCirculante_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1805Contratante_AtivoCirculante = 0;
                  n1805Contratante_AtivoCirculante = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
               }
               else
               {
                  A1805Contratante_AtivoCirculante = context.localUtil.CToN( cgiGet( edtContratante_AtivoCirculante_Internalname), ",", ".");
                  n1805Contratante_AtivoCirculante = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
               }
               n1805Contratante_AtivoCirculante = ((Convert.ToDecimal(0)==A1805Contratante_AtivoCirculante) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratante_PassivoCirculante_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratante_PassivoCirculante_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTE_PASSIVOCIRCULANTE");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_PassivoCirculante_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1806Contratante_PassivoCirculante = 0;
                  n1806Contratante_PassivoCirculante = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
               }
               else
               {
                  A1806Contratante_PassivoCirculante = context.localUtil.CToN( cgiGet( edtContratante_PassivoCirculante_Internalname), ",", ".");
                  n1806Contratante_PassivoCirculante = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
               }
               n1806Contratante_PassivoCirculante = ((Convert.ToDecimal(0)==A1806Contratante_PassivoCirculante) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratante_PatrimonioLiquido_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratante_PatrimonioLiquido_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTE_PATRIMONIOLIQUIDO");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_PatrimonioLiquido_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1807Contratante_PatrimonioLiquido = 0;
                  n1807Contratante_PatrimonioLiquido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
               }
               else
               {
                  A1807Contratante_PatrimonioLiquido = context.localUtil.CToN( cgiGet( edtContratante_PatrimonioLiquido_Internalname), ",", ".");
                  n1807Contratante_PatrimonioLiquido = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
               }
               n1807Contratante_PatrimonioLiquido = ((Convert.ToDecimal(0)==A1807Contratante_PatrimonioLiquido) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratante_ReceitaBruta_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratante_ReceitaBruta_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTE_RECEITABRUTA");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_ReceitaBruta_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1808Contratante_ReceitaBruta = 0;
                  n1808Contratante_ReceitaBruta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
               }
               else
               {
                  A1808Contratante_ReceitaBruta = context.localUtil.CToN( cgiGet( edtContratante_ReceitaBruta_Internalname), ",", ".");
                  n1808Contratante_ReceitaBruta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
               }
               n1808Contratante_ReceitaBruta = ((Convert.ToDecimal(0)==A1808Contratante_ReceitaBruta) ? true : false);
               A2034Contratante_TtlRltGerencial = cgiGet( edtContratante_TtlRltGerencial_Internalname);
               n2034Contratante_TtlRltGerencial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
               n2034Contratante_TtlRltGerencial = (String.IsNullOrEmpty(StringUtil.RTrim( A2034Contratante_TtlRltGerencial)) ? true : false);
               A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               /* Read saved values. */
               Z29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z29Contratante_Codigo"), ",", "."));
               Z550Contratante_EmailSdaKey = cgiGet( "Z550Contratante_EmailSdaKey");
               n550Contratante_EmailSdaKey = (String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) ? true : false);
               Z549Contratante_EmailSdaPass = cgiGet( "Z549Contratante_EmailSdaPass");
               n549Contratante_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) ? true : false);
               Z10Contratante_NomeFantasia = cgiGet( "Z10Contratante_NomeFantasia");
               Z11Contratante_IE = cgiGet( "Z11Contratante_IE");
               Z13Contratante_WebSite = cgiGet( "Z13Contratante_WebSite");
               n13Contratante_WebSite = (String.IsNullOrEmpty(StringUtil.RTrim( A13Contratante_WebSite)) ? true : false);
               Z14Contratante_Email = cgiGet( "Z14Contratante_Email");
               n14Contratante_Email = (String.IsNullOrEmpty(StringUtil.RTrim( A14Contratante_Email)) ? true : false);
               Z31Contratante_Telefone = cgiGet( "Z31Contratante_Telefone");
               Z32Contratante_Ramal = cgiGet( "Z32Contratante_Ramal");
               n32Contratante_Ramal = (String.IsNullOrEmpty(StringUtil.RTrim( A32Contratante_Ramal)) ? true : false);
               Z33Contratante_Fax = cgiGet( "Z33Contratante_Fax");
               n33Contratante_Fax = (String.IsNullOrEmpty(StringUtil.RTrim( A33Contratante_Fax)) ? true : false);
               Z336Contratante_AgenciaNome = cgiGet( "Z336Contratante_AgenciaNome");
               n336Contratante_AgenciaNome = (String.IsNullOrEmpty(StringUtil.RTrim( A336Contratante_AgenciaNome)) ? true : false);
               Z337Contratante_AgenciaNro = cgiGet( "Z337Contratante_AgenciaNro");
               n337Contratante_AgenciaNro = (String.IsNullOrEmpty(StringUtil.RTrim( A337Contratante_AgenciaNro)) ? true : false);
               Z338Contratante_BancoNome = cgiGet( "Z338Contratante_BancoNome");
               n338Contratante_BancoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A338Contratante_BancoNome)) ? true : false);
               Z339Contratante_BancoNro = cgiGet( "Z339Contratante_BancoNro");
               n339Contratante_BancoNro = (String.IsNullOrEmpty(StringUtil.RTrim( A339Contratante_BancoNro)) ? true : false);
               Z16Contratante_ContaCorrente = cgiGet( "Z16Contratante_ContaCorrente");
               n16Contratante_ContaCorrente = (String.IsNullOrEmpty(StringUtil.RTrim( A16Contratante_ContaCorrente)) ? true : false);
               Z30Contratante_Ativo = StringUtil.StrToBool( cgiGet( "Z30Contratante_Ativo"));
               Z547Contratante_EmailSdaHost = cgiGet( "Z547Contratante_EmailSdaHost");
               n547Contratante_EmailSdaHost = (String.IsNullOrEmpty(StringUtil.RTrim( A547Contratante_EmailSdaHost)) ? true : false);
               Z548Contratante_EmailSdaUser = cgiGet( "Z548Contratante_EmailSdaUser");
               n548Contratante_EmailSdaUser = (String.IsNullOrEmpty(StringUtil.RTrim( A548Contratante_EmailSdaUser)) ? true : false);
               Z551Contratante_EmailSdaAut = StringUtil.StrToBool( cgiGet( "Z551Contratante_EmailSdaAut"));
               n551Contratante_EmailSdaAut = ((false==A551Contratante_EmailSdaAut) ? true : false);
               Z552Contratante_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( "Z552Contratante_EmailSdaPort"), ",", "."));
               n552Contratante_EmailSdaPort = ((0==A552Contratante_EmailSdaPort) ? true : false);
               Z1048Contratante_EmailSdaSec = (short)(context.localUtil.CToN( cgiGet( "Z1048Contratante_EmailSdaSec"), ",", "."));
               n1048Contratante_EmailSdaSec = ((0==A1048Contratante_EmailSdaSec) ? true : false);
               Z593Contratante_OSAutomatica = StringUtil.StrToBool( cgiGet( "Z593Contratante_OSAutomatica"));
               Z1652Contratante_SSAutomatica = StringUtil.StrToBool( cgiGet( "Z1652Contratante_SSAutomatica"));
               n1652Contratante_SSAutomatica = ((false==A1652Contratante_SSAutomatica) ? true : false);
               Z594Contratante_ExisteConferencia = StringUtil.StrToBool( cgiGet( "Z594Contratante_ExisteConferencia"));
               Z1448Contratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "Z1448Contratante_InicioDoExpediente"), 0));
               n1448Contratante_InicioDoExpediente = ((DateTime.MinValue==A1448Contratante_InicioDoExpediente) ? true : false);
               Z1192Contratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "Z1192Contratante_FimDoExpediente"), 0));
               n1192Contratante_FimDoExpediente = ((DateTime.MinValue==A1192Contratante_FimDoExpediente) ? true : false);
               Z1727Contratante_ServicoSSPadrao = (int)(context.localUtil.CToN( cgiGet( "Z1727Contratante_ServicoSSPadrao"), ",", "."));
               n1727Contratante_ServicoSSPadrao = ((0==A1727Contratante_ServicoSSPadrao) ? true : false);
               Z1729Contratante_RetornaSS = StringUtil.StrToBool( cgiGet( "Z1729Contratante_RetornaSS"));
               n1729Contratante_RetornaSS = ((false==A1729Contratante_RetornaSS) ? true : false);
               Z1732Contratante_SSStatusPlanejamento = cgiGet( "Z1732Contratante_SSStatusPlanejamento");
               n1732Contratante_SSStatusPlanejamento = (String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) ? true : false);
               Z1733Contratante_SSPrestadoraUnica = StringUtil.StrToBool( cgiGet( "Z1733Contratante_SSPrestadoraUnica"));
               n1733Contratante_SSPrestadoraUnica = ((false==A1733Contratante_SSPrestadoraUnica) ? true : false);
               Z1738Contratante_PropostaRqrSrv = StringUtil.StrToBool( cgiGet( "Z1738Contratante_PropostaRqrSrv"));
               n1738Contratante_PropostaRqrSrv = ((false==A1738Contratante_PropostaRqrSrv) ? true : false);
               Z1739Contratante_PropostaRqrPrz = StringUtil.StrToBool( cgiGet( "Z1739Contratante_PropostaRqrPrz"));
               n1739Contratante_PropostaRqrPrz = ((false==A1739Contratante_PropostaRqrPrz) ? true : false);
               Z1740Contratante_PropostaRqrEsf = StringUtil.StrToBool( cgiGet( "Z1740Contratante_PropostaRqrEsf"));
               n1740Contratante_PropostaRqrEsf = ((false==A1740Contratante_PropostaRqrEsf) ? true : false);
               Z1741Contratante_PropostaRqrCnt = StringUtil.StrToBool( cgiGet( "Z1741Contratante_PropostaRqrCnt"));
               n1741Contratante_PropostaRqrCnt = ((false==A1741Contratante_PropostaRqrCnt) ? true : false);
               Z1742Contratante_PropostaNvlCnt = (short)(context.localUtil.CToN( cgiGet( "Z1742Contratante_PropostaNvlCnt"), ",", "."));
               n1742Contratante_PropostaNvlCnt = ((0==A1742Contratante_PropostaNvlCnt) ? true : false);
               Z1757Contratante_OSGeraOS = StringUtil.StrToBool( cgiGet( "Z1757Contratante_OSGeraOS"));
               n1757Contratante_OSGeraOS = ((false==A1757Contratante_OSGeraOS) ? true : false);
               Z1758Contratante_OSGeraTRP = StringUtil.StrToBool( cgiGet( "Z1758Contratante_OSGeraTRP"));
               n1758Contratante_OSGeraTRP = ((false==A1758Contratante_OSGeraTRP) ? true : false);
               Z1759Contratante_OSGeraTH = StringUtil.StrToBool( cgiGet( "Z1759Contratante_OSGeraTH"));
               n1759Contratante_OSGeraTH = ((false==A1759Contratante_OSGeraTH) ? true : false);
               Z1760Contratante_OSGeraTRD = StringUtil.StrToBool( cgiGet( "Z1760Contratante_OSGeraTRD"));
               n1760Contratante_OSGeraTRD = ((false==A1760Contratante_OSGeraTRD) ? true : false);
               Z1761Contratante_OSGeraTR = StringUtil.StrToBool( cgiGet( "Z1761Contratante_OSGeraTR"));
               n1761Contratante_OSGeraTR = ((false==A1761Contratante_OSGeraTR) ? true : false);
               Z1803Contratante_OSHmlgComPnd = StringUtil.StrToBool( cgiGet( "Z1803Contratante_OSHmlgComPnd"));
               n1803Contratante_OSHmlgComPnd = ((false==A1803Contratante_OSHmlgComPnd) ? true : false);
               Z1805Contratante_AtivoCirculante = context.localUtil.CToN( cgiGet( "Z1805Contratante_AtivoCirculante"), ",", ".");
               n1805Contratante_AtivoCirculante = ((Convert.ToDecimal(0)==A1805Contratante_AtivoCirculante) ? true : false);
               Z1806Contratante_PassivoCirculante = context.localUtil.CToN( cgiGet( "Z1806Contratante_PassivoCirculante"), ",", ".");
               n1806Contratante_PassivoCirculante = ((Convert.ToDecimal(0)==A1806Contratante_PassivoCirculante) ? true : false);
               Z1807Contratante_PatrimonioLiquido = context.localUtil.CToN( cgiGet( "Z1807Contratante_PatrimonioLiquido"), ",", ".");
               n1807Contratante_PatrimonioLiquido = ((Convert.ToDecimal(0)==A1807Contratante_PatrimonioLiquido) ? true : false);
               Z1808Contratante_ReceitaBruta = context.localUtil.CToN( cgiGet( "Z1808Contratante_ReceitaBruta"), ",", ".");
               n1808Contratante_ReceitaBruta = ((Convert.ToDecimal(0)==A1808Contratante_ReceitaBruta) ? true : false);
               Z1822Contratante_UsaOSistema = StringUtil.StrToBool( cgiGet( "Z1822Contratante_UsaOSistema"));
               n1822Contratante_UsaOSistema = ((false==A1822Contratante_UsaOSistema) ? true : false);
               Z2034Contratante_TtlRltGerencial = cgiGet( "Z2034Contratante_TtlRltGerencial");
               n2034Contratante_TtlRltGerencial = (String.IsNullOrEmpty(StringUtil.RTrim( A2034Contratante_TtlRltGerencial)) ? true : false);
               Z2084Contratante_PrzAoRtr = (short)(context.localUtil.CToN( cgiGet( "Z2084Contratante_PrzAoRtr"), ",", "."));
               n2084Contratante_PrzAoRtr = ((0==A2084Contratante_PrzAoRtr) ? true : false);
               Z2083Contratante_PrzActRtr = (short)(context.localUtil.CToN( cgiGet( "Z2083Contratante_PrzActRtr"), ",", "."));
               n2083Contratante_PrzActRtr = ((0==A2083Contratante_PrzActRtr) ? true : false);
               Z2085Contratante_RequerOrigem = StringUtil.StrToBool( cgiGet( "Z2085Contratante_RequerOrigem"));
               n2085Contratante_RequerOrigem = ((false==A2085Contratante_RequerOrigem) ? true : false);
               Z2089Contratante_SelecionaResponsavelOS = StringUtil.StrToBool( cgiGet( "Z2089Contratante_SelecionaResponsavelOS"));
               Z2090Contratante_ExibePF = StringUtil.StrToBool( cgiGet( "Z2090Contratante_ExibePF"));
               Z25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z25Municipio_Codigo"), ",", "."));
               n25Municipio_Codigo = ((0==A25Municipio_Codigo) ? true : false);
               Z335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "Z335Contratante_PessoaCod"), ",", "."));
               A550Contratante_EmailSdaKey = cgiGet( "Z550Contratante_EmailSdaKey");
               n550Contratante_EmailSdaKey = false;
               n550Contratante_EmailSdaKey = (String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) ? true : false);
               A594Contratante_ExisteConferencia = StringUtil.StrToBool( cgiGet( "Z594Contratante_ExisteConferencia"));
               A2084Contratante_PrzAoRtr = (short)(context.localUtil.CToN( cgiGet( "Z2084Contratante_PrzAoRtr"), ",", "."));
               n2084Contratante_PrzAoRtr = false;
               n2084Contratante_PrzAoRtr = ((0==A2084Contratante_PrzAoRtr) ? true : false);
               A2083Contratante_PrzActRtr = (short)(context.localUtil.CToN( cgiGet( "Z2083Contratante_PrzActRtr"), ",", "."));
               n2083Contratante_PrzActRtr = false;
               n2083Contratante_PrzActRtr = ((0==A2083Contratante_PrzActRtr) ? true : false);
               A2085Contratante_RequerOrigem = StringUtil.StrToBool( cgiGet( "Z2085Contratante_RequerOrigem"));
               n2085Contratante_RequerOrigem = false;
               n2085Contratante_RequerOrigem = ((false==A2085Contratante_RequerOrigem) ? true : false);
               A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "Z335Contratante_PessoaCod"), ",", "."));
               O549Contratante_EmailSdaPass = cgiGet( "O549Contratante_EmailSdaPass");
               n549Contratante_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "N335Contratante_PessoaCod"), ",", "."));
               N25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "N25Municipio_Codigo"), ",", "."));
               n25Municipio_Codigo = ((0==A25Municipio_Codigo) ? true : false);
               AV7Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATANTE_CODIGO"), ",", "."));
               AV16Insert_Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATANTE_PESSOACOD"), ",", "."));
               A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATANTE_PESSOACOD"), ",", "."));
               AV17Insert_Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_MUNICIPIO_CODIGO"), ",", "."));
               A23Estado_UF = cgiGet( "ESTADO_UF");
               A550Contratante_EmailSdaKey = cgiGet( "CONTRATANTE_EMAILSDAKEY");
               n550Contratante_EmailSdaKey = (String.IsNullOrEmpty(StringUtil.RTrim( A550Contratante_EmailSdaKey)) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A2085Contratante_RequerOrigem = StringUtil.StrToBool( cgiGet( "CONTRATANTE_REQUERORIGEM"));
               n2085Contratante_RequerOrigem = ((false==A2085Contratante_RequerOrigem) ? true : false);
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV29AuditingObject);
               A594Contratante_ExisteConferencia = StringUtil.StrToBool( cgiGet( "CONTRATANTE_EXISTECONFERENCIA"));
               A2084Contratante_PrzAoRtr = (short)(context.localUtil.CToN( cgiGet( "CONTRATANTE_PRZAORTR"), ",", "."));
               n2084Contratante_PrzAoRtr = ((0==A2084Contratante_PrzAoRtr) ? true : false);
               A2083Contratante_PrzActRtr = (short)(context.localUtil.CToN( cgiGet( "CONTRATANTE_PRZACTRTR"), ",", "."));
               n2083Contratante_PrzActRtr = ((0==A2083Contratante_PrzActRtr) ? true : false);
               A26Municipio_Nome = cgiGet( "MUNICIPIO_NOME");
               AV45Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Gxuitabspanel_tabs_Width = cgiGet( "GXUITABSPANEL_TABS_Width");
               Gxuitabspanel_tabs_Height = cgiGet( "GXUITABSPANEL_TABS_Height");
               Gxuitabspanel_tabs_Cls = cgiGet( "GXUITABSPANEL_TABS_Cls");
               Gxuitabspanel_tabs_Enabled = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Enabled"));
               Gxuitabspanel_tabs_Class = cgiGet( "GXUITABSPANEL_TABS_Class");
               Gxuitabspanel_tabs_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autowidth"));
               Gxuitabspanel_tabs_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autoheight"));
               Gxuitabspanel_tabs_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Autoscroll"));
               Gxuitabspanel_tabs_Activetabid = cgiGet( "GXUITABSPANEL_TABS_Activetabid");
               Gxuitabspanel_tabs_Designtimetabs = cgiGet( "GXUITABSPANEL_TABS_Designtimetabs");
               Gxuitabspanel_tabs_Selectedtabindex = (int)(context.localUtil.CToN( cgiGet( "GXUITABSPANEL_TABS_Selectedtabindex"), ",", "."));
               Gxuitabspanel_tabs_Visible = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABS_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtContratante_LogoArquivo_Filetype = cgiGet( "CONTRATANTE_LOGOARQUIVO_Filetype");
               edtContratante_LogoArquivo_Filename = cgiGet( "CONTRATANTE_LOGOARQUIVO_Filename");
               edtContratante_LogoArquivo_Filename = cgiGet( "CONTRATANTE_LOGOARQUIVO_Filename");
               edtContratante_LogoArquivo_Filetype = cgiGet( "CONTRATANTE_LOGOARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1124Contratante_LogoArquivo)) )
               {
                  edtContratante_LogoArquivo_Filename = (String)(CGIGetFileName(edtContratante_LogoArquivo_Internalname));
                  edtContratante_LogoArquivo_Filetype = (String)(CGIGetFileType(edtContratante_LogoArquivo_Internalname));
               }
               A1126Contratante_LogoTipoArq = edtContratante_LogoArquivo_Filetype;
               n1126Contratante_LogoTipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1126Contratante_LogoTipoArq", A1126Contratante_LogoTipoArq);
               A1125Contratante_LogoNomeArq = edtContratante_LogoArquivo_Filename;
               n1125Contratante_LogoNomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1125Contratante_LogoNomeArq", A1125Contratante_LogoNomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A1124Contratante_LogoArquivo)) )
               {
                  GXCCtlgxBlob = "CONTRATANTE_LOGOARQUIVO" + "_gxBlob";
                  A1124Contratante_LogoArquivo = cgiGet( GXCCtlgxBlob);
                  n1124Contratante_LogoArquivo = false;
                  n1124Contratante_LogoArquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1124Contratante_LogoArquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Contratante";
               A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
               n29Contratante_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV45Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A594Contratante_ExisteConferencia);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2084Contratante_PrzAoRtr), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2083Contratante_PrzActRtr), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2085Contratante_RequerOrigem);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A29Contratante_Codigo != Z29Contratante_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Contratante_Codigo:"+context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Contratante_PessoaCod:"+context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV45Pgmname, "")));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Contratante_ExisteConferencia:"+StringUtil.BoolToStr( A594Contratante_ExisteConferencia));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Contratante_PrzAoRtr:"+context.localUtil.Format( (decimal)(A2084Contratante_PrzAoRtr), "ZZZ9"));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Contratante_PrzActRtr:"+context.localUtil.Format( (decimal)(A2083Contratante_PrzActRtr), "ZZZ9"));
                  GXUtil.WriteLog("contratante:[SecurityCheckFailed value for]"+"Contratante_RequerOrigem:"+StringUtil.BoolToStr( A2085Contratante_RequerOrigem));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n29Contratante_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode124 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode124;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound124 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_050( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATANTE_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContratante_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11052 */
                           E11052 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12052 */
                           E12052 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOTESTAREMAIL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13052 */
                           E13052 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOTESTECSHARP'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14052 */
                           E14052 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOTESTAREMAIL2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15052 */
                           E15052 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12052 */
            E12052 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll05124( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes05124( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_user_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_user_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_key_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_key_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRedmine_secure_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRedmine_secure.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_host_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_host_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_url_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_url_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRedmine_versao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRedmine_versao.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_camposistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_camposistema_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_camposervico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_camposervico_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmailto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmailto_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_050( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls05124( ) ;
            }
            else
            {
               CheckExtendedTable05124( ) ;
               CloseExtendedTableCursors05124( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption050( )
      {
      }

      protected void E11052( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV45Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV46GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46GXV1), 8, 0)));
            while ( AV46GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV46GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contratante_PessoaCod") == 0 )
               {
                  AV16Insert_Contratante_PessoaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_Contratante_PessoaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Municipio_Codigo") == 0 )
               {
                  AV17Insert_Municipio_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Insert_Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Insert_Municipio_Codigo), 6, 0)));
               }
               AV46GXV1 = (int)(AV46GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46GXV1), 8, 0)));
            }
         }
         edtContratante_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Visible), 5, 0)));
         edtContratante_LogoNomeArq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoNomeArq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoNomeArq_Visible), 5, 0)));
         edtContratante_LogoTipoArq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoTipoArq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoTipoArq_Visible), 5, 0)));
         lblTextblockcontratante_ativo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratante_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratante_ativo_Visible), 5, 0)));
         chkContratante_Ativo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratante_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratante_Ativo.Visible), 5, 0)));
         edtContratante_LogoArquivo_Display = 1;
         edtContratante_LogoArquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Linktarget", edtContratante_LogoArquivo_Linktarget);
         edtContratante_LogoArquivo_Tooltiptext = "Ver logomarca";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Tooltiptext", edtContratante_LogoArquivo_Tooltiptext);
         cmbContratante_UsaOSistema.Enabled = (AV8WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_UsaOSistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_UsaOSistema.Enabled), 5, 0)));
         new prc_getredmine(context ).execute( ref  AV7Contratante_Codigo, out  AV31Redmine_User, out  AV30Redmine_Secure, out  AV24Redmine_Host, out  AV25Redmine_Url, out  AV26Redmine_Key, out  AV28Redmine_Versao, out  AV32Redmine_CampoSistema, out  AV33Redmine_CampoServico) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contratante_Codigo), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Redmine_User", AV31Redmine_User);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Redmine_Secure", StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Redmine_Host", AV24Redmine_Host);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Redmine_Url", AV25Redmine_Url);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Redmine_Key", AV26Redmine_Key);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Redmine_Versao", AV28Redmine_Versao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Redmine_CampoSistema", AV32Redmine_CampoSistema);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Redmine_CampoServico", AV33Redmine_CampoServico);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> document.getElementById(\"GXUITABSPANEL_TABSContainer\").setAttribute(\"class\",\"gx_usercontrol tab-container\");</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E12052( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV45Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pgmname", AV45Pgmname);
         new prc_updredmine(context ).execute( ref  AV7Contratante_Codigo,  AV31Redmine_User,  AV30Redmine_Secure,  AV24Redmine_Host,  AV25Redmine_Url,  AV26Redmine_Key,  AV28Redmine_Versao,  AV32Redmine_CampoSistema,  AV33Redmine_CampoServico) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contratante_Codigo), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Redmine_User", AV31Redmine_User);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Redmine_Secure", StringUtil.Str( (decimal)(AV30Redmine_Secure), 1, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Redmine_Host", AV24Redmine_Host);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Redmine_Url", AV25Redmine_Url);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Redmine_Key", AV26Redmine_Key);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Redmine_Versao", AV28Redmine_Versao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Redmine_CampoSistema", AV32Redmine_CampoSistema);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Redmine_CampoServico", AV33Redmine_CampoServico);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContratanteUsuario"));
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV45Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pgmname", AV45Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratante.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E13052( )
      {
         /* 'DoTestarEmail' Routine */
         AV37EmailText = "Teste de envio de email";
         AV38Subject = " - " + "Teste de envio de email (No reply)";
         AV34Usuarios.Clear();
         AV34Usuarios.Add(AV8WWPContext.gxTpr_Userid, 0);
         new prc_enviaremail(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo,  AV34Usuarios,  AV38Subject,  AV37EmailText,  AV35Attachments, ref  AV36Resultado) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Resultado", AV36Resultado);
         GX_msglist.addItem(AV36Resultado);
      }

      protected void E14052( )
      {
         /* 'DoTesteCSharp' Routine */
         GXt_int1 = AV43retorno;
         new prc_enviaremailcsharp(context ).execute(  A547Contratante_EmailSdaHost,  A552Contratante_EmailSdaPort,  A1048Contratante_EmailSdaSec,  A548Contratante_EmailSdaUser,  A549Contratante_EmailSdaPass,  AV44EmailTo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A547Contratante_EmailSdaHost", A547Contratante_EmailSdaHost);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A548Contratante_EmailSdaUser", A548Contratante_EmailSdaUser);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44EmailTo", AV44EmailTo);
         AV43retorno = GXt_int1;
         if ( (0==AV43retorno) )
         {
            GX_msglist.addItem("Teste N�O conlu�do!");
         }
         else
         {
            GX_msglist.addItem("Envio com sucesso!");
         }
      }

      protected void E15052( )
      {
         /* 'DoTestarEmail2' Routine */
         AV41SMTPSession.ErrDisplay = 1;
         AV19MailRecipient.Name = "daniel.rodriguez@efor.com.br";
         AV19MailRecipient.Address = "daniel.rodriguez@efor.com.br";
         AV39Email.Clear();
         AV39Email.To.Add(AV19MailRecipient) ;
         AV39Email.Subject = "Teste de envio de email";
         AV39Email.Text = "Teste de envio de email";
         AV41SMTPSession.Sender.Name = "meetrika@eficaciaorganizacao.com.br";
         AV41SMTPSession.Sender.Address = "meetrika@eficaciaorganizacao.com.br";
         AV41SMTPSession.Host = "192.168.11.25";
         AV41SMTPSession.UserName = "meetrika@eficaciaorganizacao.com.br";
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 ) )
         {
            AV41SMTPSession.Password = StringUtil.Trim( A549Contratante_EmailSdaPass);
         }
         else
         {
            AV41SMTPSession.Password = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
         }
         AV41SMTPSession.Port = 25;
         AV41SMTPSession.Authentication = 0;
         AV41SMTPSession.Secure = 0;
         AV41SMTPSession.Login();
         if ( AV41SMTPSession.ErrCode != 0 )
         {
            GX_msglist.addItem("Retorno do Login: "+AV41SMTPSession.ErrDescription);
         }
         else
         {
            GX_msglist.addItem("Login bem sucedido!");
         }
         AV41SMTPSession.Send(AV39Email);
         if ( AV41SMTPSession.ErrCode != 0 )
         {
            GX_msglist.addItem("Retorno do Send: "+AV41SMTPSession.ErrDescription+": "+A548Contratante_EmailSdaUser);
         }
         else
         {
            GX_msglist.addItem("Mensagem enviada com sucesso!");
         }
         AV41SMTPSession.Logout();
      }

      protected void ZM05124( short GX_JID )
      {
         if ( ( GX_JID == 36 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z550Contratante_EmailSdaKey = T00053_A550Contratante_EmailSdaKey[0];
               Z549Contratante_EmailSdaPass = T00053_A549Contratante_EmailSdaPass[0];
               Z10Contratante_NomeFantasia = T00053_A10Contratante_NomeFantasia[0];
               Z11Contratante_IE = T00053_A11Contratante_IE[0];
               Z13Contratante_WebSite = T00053_A13Contratante_WebSite[0];
               Z14Contratante_Email = T00053_A14Contratante_Email[0];
               Z31Contratante_Telefone = T00053_A31Contratante_Telefone[0];
               Z32Contratante_Ramal = T00053_A32Contratante_Ramal[0];
               Z33Contratante_Fax = T00053_A33Contratante_Fax[0];
               Z336Contratante_AgenciaNome = T00053_A336Contratante_AgenciaNome[0];
               Z337Contratante_AgenciaNro = T00053_A337Contratante_AgenciaNro[0];
               Z338Contratante_BancoNome = T00053_A338Contratante_BancoNome[0];
               Z339Contratante_BancoNro = T00053_A339Contratante_BancoNro[0];
               Z16Contratante_ContaCorrente = T00053_A16Contratante_ContaCorrente[0];
               Z30Contratante_Ativo = T00053_A30Contratante_Ativo[0];
               Z547Contratante_EmailSdaHost = T00053_A547Contratante_EmailSdaHost[0];
               Z548Contratante_EmailSdaUser = T00053_A548Contratante_EmailSdaUser[0];
               Z551Contratante_EmailSdaAut = T00053_A551Contratante_EmailSdaAut[0];
               Z552Contratante_EmailSdaPort = T00053_A552Contratante_EmailSdaPort[0];
               Z1048Contratante_EmailSdaSec = T00053_A1048Contratante_EmailSdaSec[0];
               Z593Contratante_OSAutomatica = T00053_A593Contratante_OSAutomatica[0];
               Z1652Contratante_SSAutomatica = T00053_A1652Contratante_SSAutomatica[0];
               Z594Contratante_ExisteConferencia = T00053_A594Contratante_ExisteConferencia[0];
               Z1448Contratante_InicioDoExpediente = T00053_A1448Contratante_InicioDoExpediente[0];
               Z1192Contratante_FimDoExpediente = T00053_A1192Contratante_FimDoExpediente[0];
               Z1727Contratante_ServicoSSPadrao = T00053_A1727Contratante_ServicoSSPadrao[0];
               Z1729Contratante_RetornaSS = T00053_A1729Contratante_RetornaSS[0];
               Z1732Contratante_SSStatusPlanejamento = T00053_A1732Contratante_SSStatusPlanejamento[0];
               Z1733Contratante_SSPrestadoraUnica = T00053_A1733Contratante_SSPrestadoraUnica[0];
               Z1738Contratante_PropostaRqrSrv = T00053_A1738Contratante_PropostaRqrSrv[0];
               Z1739Contratante_PropostaRqrPrz = T00053_A1739Contratante_PropostaRqrPrz[0];
               Z1740Contratante_PropostaRqrEsf = T00053_A1740Contratante_PropostaRqrEsf[0];
               Z1741Contratante_PropostaRqrCnt = T00053_A1741Contratante_PropostaRqrCnt[0];
               Z1742Contratante_PropostaNvlCnt = T00053_A1742Contratante_PropostaNvlCnt[0];
               Z1757Contratante_OSGeraOS = T00053_A1757Contratante_OSGeraOS[0];
               Z1758Contratante_OSGeraTRP = T00053_A1758Contratante_OSGeraTRP[0];
               Z1759Contratante_OSGeraTH = T00053_A1759Contratante_OSGeraTH[0];
               Z1760Contratante_OSGeraTRD = T00053_A1760Contratante_OSGeraTRD[0];
               Z1761Contratante_OSGeraTR = T00053_A1761Contratante_OSGeraTR[0];
               Z1803Contratante_OSHmlgComPnd = T00053_A1803Contratante_OSHmlgComPnd[0];
               Z1805Contratante_AtivoCirculante = T00053_A1805Contratante_AtivoCirculante[0];
               Z1806Contratante_PassivoCirculante = T00053_A1806Contratante_PassivoCirculante[0];
               Z1807Contratante_PatrimonioLiquido = T00053_A1807Contratante_PatrimonioLiquido[0];
               Z1808Contratante_ReceitaBruta = T00053_A1808Contratante_ReceitaBruta[0];
               Z1822Contratante_UsaOSistema = T00053_A1822Contratante_UsaOSistema[0];
               Z2034Contratante_TtlRltGerencial = T00053_A2034Contratante_TtlRltGerencial[0];
               Z2084Contratante_PrzAoRtr = T00053_A2084Contratante_PrzAoRtr[0];
               Z2083Contratante_PrzActRtr = T00053_A2083Contratante_PrzActRtr[0];
               Z2085Contratante_RequerOrigem = T00053_A2085Contratante_RequerOrigem[0];
               Z2089Contratante_SelecionaResponsavelOS = T00053_A2089Contratante_SelecionaResponsavelOS[0];
               Z2090Contratante_ExibePF = T00053_A2090Contratante_ExibePF[0];
               Z25Municipio_Codigo = T00053_A25Municipio_Codigo[0];
               Z335Contratante_PessoaCod = T00053_A335Contratante_PessoaCod[0];
            }
            else
            {
               Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
               Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
               Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
               Z11Contratante_IE = A11Contratante_IE;
               Z13Contratante_WebSite = A13Contratante_WebSite;
               Z14Contratante_Email = A14Contratante_Email;
               Z31Contratante_Telefone = A31Contratante_Telefone;
               Z32Contratante_Ramal = A32Contratante_Ramal;
               Z33Contratante_Fax = A33Contratante_Fax;
               Z336Contratante_AgenciaNome = A336Contratante_AgenciaNome;
               Z337Contratante_AgenciaNro = A337Contratante_AgenciaNro;
               Z338Contratante_BancoNome = A338Contratante_BancoNome;
               Z339Contratante_BancoNro = A339Contratante_BancoNro;
               Z16Contratante_ContaCorrente = A16Contratante_ContaCorrente;
               Z30Contratante_Ativo = A30Contratante_Ativo;
               Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
               Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
               Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
               Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
               Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
               Z593Contratante_OSAutomatica = A593Contratante_OSAutomatica;
               Z1652Contratante_SSAutomatica = A1652Contratante_SSAutomatica;
               Z594Contratante_ExisteConferencia = A594Contratante_ExisteConferencia;
               Z1448Contratante_InicioDoExpediente = A1448Contratante_InicioDoExpediente;
               Z1192Contratante_FimDoExpediente = A1192Contratante_FimDoExpediente;
               Z1727Contratante_ServicoSSPadrao = A1727Contratante_ServicoSSPadrao;
               Z1729Contratante_RetornaSS = A1729Contratante_RetornaSS;
               Z1732Contratante_SSStatusPlanejamento = A1732Contratante_SSStatusPlanejamento;
               Z1733Contratante_SSPrestadoraUnica = A1733Contratante_SSPrestadoraUnica;
               Z1738Contratante_PropostaRqrSrv = A1738Contratante_PropostaRqrSrv;
               Z1739Contratante_PropostaRqrPrz = A1739Contratante_PropostaRqrPrz;
               Z1740Contratante_PropostaRqrEsf = A1740Contratante_PropostaRqrEsf;
               Z1741Contratante_PropostaRqrCnt = A1741Contratante_PropostaRqrCnt;
               Z1742Contratante_PropostaNvlCnt = A1742Contratante_PropostaNvlCnt;
               Z1757Contratante_OSGeraOS = A1757Contratante_OSGeraOS;
               Z1758Contratante_OSGeraTRP = A1758Contratante_OSGeraTRP;
               Z1759Contratante_OSGeraTH = A1759Contratante_OSGeraTH;
               Z1760Contratante_OSGeraTRD = A1760Contratante_OSGeraTRD;
               Z1761Contratante_OSGeraTR = A1761Contratante_OSGeraTR;
               Z1803Contratante_OSHmlgComPnd = A1803Contratante_OSHmlgComPnd;
               Z1805Contratante_AtivoCirculante = A1805Contratante_AtivoCirculante;
               Z1806Contratante_PassivoCirculante = A1806Contratante_PassivoCirculante;
               Z1807Contratante_PatrimonioLiquido = A1807Contratante_PatrimonioLiquido;
               Z1808Contratante_ReceitaBruta = A1808Contratante_ReceitaBruta;
               Z1822Contratante_UsaOSistema = A1822Contratante_UsaOSistema;
               Z2034Contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
               Z2084Contratante_PrzAoRtr = A2084Contratante_PrzAoRtr;
               Z2083Contratante_PrzActRtr = A2083Contratante_PrzActRtr;
               Z2085Contratante_RequerOrigem = A2085Contratante_RequerOrigem;
               Z2089Contratante_SelecionaResponsavelOS = A2089Contratante_SelecionaResponsavelOS;
               Z2090Contratante_ExibePF = A2090Contratante_ExibePF;
               Z25Municipio_Codigo = A25Municipio_Codigo;
               Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
            }
         }
         if ( GX_JID == -36 )
         {
            Z29Contratante_Codigo = A29Contratante_Codigo;
            Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
            Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            Z11Contratante_IE = A11Contratante_IE;
            Z13Contratante_WebSite = A13Contratante_WebSite;
            Z14Contratante_Email = A14Contratante_Email;
            Z31Contratante_Telefone = A31Contratante_Telefone;
            Z32Contratante_Ramal = A32Contratante_Ramal;
            Z33Contratante_Fax = A33Contratante_Fax;
            Z336Contratante_AgenciaNome = A336Contratante_AgenciaNome;
            Z337Contratante_AgenciaNro = A337Contratante_AgenciaNro;
            Z338Contratante_BancoNome = A338Contratante_BancoNome;
            Z339Contratante_BancoNro = A339Contratante_BancoNro;
            Z16Contratante_ContaCorrente = A16Contratante_ContaCorrente;
            Z30Contratante_Ativo = A30Contratante_Ativo;
            Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            Z593Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            Z1652Contratante_SSAutomatica = A1652Contratante_SSAutomatica;
            Z594Contratante_ExisteConferencia = A594Contratante_ExisteConferencia;
            Z1124Contratante_LogoArquivo = A1124Contratante_LogoArquivo;
            Z1448Contratante_InicioDoExpediente = A1448Contratante_InicioDoExpediente;
            Z1192Contratante_FimDoExpediente = A1192Contratante_FimDoExpediente;
            Z1727Contratante_ServicoSSPadrao = A1727Contratante_ServicoSSPadrao;
            Z1729Contratante_RetornaSS = A1729Contratante_RetornaSS;
            Z1732Contratante_SSStatusPlanejamento = A1732Contratante_SSStatusPlanejamento;
            Z1733Contratante_SSPrestadoraUnica = A1733Contratante_SSPrestadoraUnica;
            Z1738Contratante_PropostaRqrSrv = A1738Contratante_PropostaRqrSrv;
            Z1739Contratante_PropostaRqrPrz = A1739Contratante_PropostaRqrPrz;
            Z1740Contratante_PropostaRqrEsf = A1740Contratante_PropostaRqrEsf;
            Z1741Contratante_PropostaRqrCnt = A1741Contratante_PropostaRqrCnt;
            Z1742Contratante_PropostaNvlCnt = A1742Contratante_PropostaNvlCnt;
            Z1757Contratante_OSGeraOS = A1757Contratante_OSGeraOS;
            Z1758Contratante_OSGeraTRP = A1758Contratante_OSGeraTRP;
            Z1759Contratante_OSGeraTH = A1759Contratante_OSGeraTH;
            Z1760Contratante_OSGeraTRD = A1760Contratante_OSGeraTRD;
            Z1761Contratante_OSGeraTR = A1761Contratante_OSGeraTR;
            Z1803Contratante_OSHmlgComPnd = A1803Contratante_OSHmlgComPnd;
            Z1805Contratante_AtivoCirculante = A1805Contratante_AtivoCirculante;
            Z1806Contratante_PassivoCirculante = A1806Contratante_PassivoCirculante;
            Z1807Contratante_PatrimonioLiquido = A1807Contratante_PatrimonioLiquido;
            Z1808Contratante_ReceitaBruta = A1808Contratante_ReceitaBruta;
            Z1822Contratante_UsaOSistema = A1822Contratante_UsaOSistema;
            Z2034Contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
            Z2084Contratante_PrzAoRtr = A2084Contratante_PrzAoRtr;
            Z2083Contratante_PrzActRtr = A2083Contratante_PrzActRtr;
            Z2085Contratante_RequerOrigem = A2085Contratante_RequerOrigem;
            Z2089Contratante_SelecionaResponsavelOS = A2089Contratante_SelecionaResponsavelOS;
            Z2090Contratante_ExibePF = A2090Contratante_ExibePF;
            Z1126Contratante_LogoTipoArq = A1126Contratante_LogoTipoArq;
            Z1125Contratante_LogoNomeArq = A1125Contratante_LogoNomeArq;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
            Z12Contratante_CNPJ = A12Contratante_CNPJ;
            Z9Contratante_RazaoSocial = A9Contratante_RazaoSocial;
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvESTADO_UF_html05124( ) ;
         GXACONTRATANTE_SERVICOSSPADRAO_html05124( ) ;
         edtContratante_LogoNomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoNomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoNomeArq_Enabled), 5, 0)));
         edtContratante_LogoTipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoTipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoTipoArq_Enabled), 5, 0)));
         edtContratante_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV45Pgmname = "Contratante";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Pgmname", AV45Pgmname);
         edtContratante_LogoNomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoNomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoNomeArq_Enabled), 5, 0)));
         edtContratante_LogoTipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoTipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoTipoArq_Enabled), 5, 0)));
         edtContratante_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Contratante_Codigo) )
         {
            A29Contratante_Codigo = AV7Contratante_Codigo;
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV17Insert_Municipio_Codigo) )
         {
            dynMunicipio_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynMunicipio_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynMunicipio_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynMunicipio_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV17Insert_Municipio_Codigo) )
         {
            A25Municipio_Codigo = AV17Insert_Municipio_Codigo;
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_Contratante_PessoaCod) )
         {
            A335Contratante_PessoaCod = AV16Insert_Contratante_PessoaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2085Contratante_RequerOrigem) && ( Gx_BScreen == 0 ) )
         {
            A2085Contratante_RequerOrigem = true;
            n2085Contratante_RequerOrigem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2085Contratante_RequerOrigem", A2085Contratante_RequerOrigem);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A2034Contratante_TtlRltGerencial)) && ( Gx_BScreen == 0 ) )
         {
            A2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
            n2034Contratante_TtlRltGerencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1822Contratante_UsaOSistema) && ( Gx_BScreen == 0 ) )
         {
            A1822Contratante_UsaOSistema = false;
            n1822Contratante_UsaOSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1742Contratante_PropostaNvlCnt) && ( Gx_BScreen == 0 ) )
         {
            A1742Contratante_PropostaNvlCnt = 0;
            n1742Contratante_PropostaNvlCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1738Contratante_PropostaRqrSrv) && ( Gx_BScreen == 0 ) )
         {
            A1738Contratante_PropostaRqrSrv = true;
            n1738Contratante_PropostaRqrSrv = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) && ( Gx_BScreen == 0 ) )
         {
            A1732Contratante_SSStatusPlanejamento = "B";
            n1732Contratante_SSStatusPlanejamento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A551Contratante_EmailSdaAut) && ( Gx_BScreen == 0 ) )
         {
            A551Contratante_EmailSdaAut = false;
            n551Contratante_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A30Contratante_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A30Contratante_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30Contratante_Ativo", A30Contratante_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            GXAMUNICIPIO_CODIGO_html05124( AV15Estado_UF) ;
            /* Using cursor T00054 */
            pr_default.execute(2, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            A26Municipio_Nome = T00054_A26Municipio_Nome[0];
            A23Estado_UF = T00054_A23Estado_UF[0];
            pr_default.close(2);
            /* Using cursor T00055 */
            pr_default.execute(3, new Object[] {A335Contratante_PessoaCod});
            A12Contratante_CNPJ = T00055_A12Contratante_CNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
            n12Contratante_CNPJ = T00055_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = T00055_A9Contratante_RazaoSocial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
            n9Contratante_RazaoSocial = T00055_n9Contratante_RazaoSocial[0];
            pr_default.close(3);
         }
      }

      protected void Load05124( )
      {
         /* Using cursor T00056 */
         pr_default.execute(4, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound124 = 1;
            A550Contratante_EmailSdaKey = T00056_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = T00056_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = T00056_A549Contratante_EmailSdaPass[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
            n549Contratante_EmailSdaPass = T00056_n549Contratante_EmailSdaPass[0];
            A12Contratante_CNPJ = T00056_A12Contratante_CNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
            n12Contratante_CNPJ = T00056_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = T00056_A9Contratante_RazaoSocial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
            n9Contratante_RazaoSocial = T00056_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = T00056_A10Contratante_NomeFantasia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
            A11Contratante_IE = T00056_A11Contratante_IE[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Contratante_IE", A11Contratante_IE);
            A13Contratante_WebSite = T00056_A13Contratante_WebSite[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Contratante_WebSite", A13Contratante_WebSite);
            n13Contratante_WebSite = T00056_n13Contratante_WebSite[0];
            A14Contratante_Email = T00056_A14Contratante_Email[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Contratante_Email", A14Contratante_Email);
            n14Contratante_Email = T00056_n14Contratante_Email[0];
            A31Contratante_Telefone = T00056_A31Contratante_Telefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31Contratante_Telefone", A31Contratante_Telefone);
            A32Contratante_Ramal = T00056_A32Contratante_Ramal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32Contratante_Ramal", A32Contratante_Ramal);
            n32Contratante_Ramal = T00056_n32Contratante_Ramal[0];
            A33Contratante_Fax = T00056_A33Contratante_Fax[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33Contratante_Fax", A33Contratante_Fax);
            n33Contratante_Fax = T00056_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = T00056_A336Contratante_AgenciaNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A336Contratante_AgenciaNome", A336Contratante_AgenciaNome);
            n336Contratante_AgenciaNome = T00056_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = T00056_A337Contratante_AgenciaNro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A337Contratante_AgenciaNro", A337Contratante_AgenciaNro);
            n337Contratante_AgenciaNro = T00056_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = T00056_A338Contratante_BancoNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A338Contratante_BancoNome", A338Contratante_BancoNome);
            n338Contratante_BancoNome = T00056_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = T00056_A339Contratante_BancoNro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A339Contratante_BancoNro", A339Contratante_BancoNro);
            n339Contratante_BancoNro = T00056_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = T00056_A16Contratante_ContaCorrente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16Contratante_ContaCorrente", A16Contratante_ContaCorrente);
            n16Contratante_ContaCorrente = T00056_n16Contratante_ContaCorrente[0];
            A26Municipio_Nome = T00056_A26Municipio_Nome[0];
            A30Contratante_Ativo = T00056_A30Contratante_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30Contratante_Ativo", A30Contratante_Ativo);
            A547Contratante_EmailSdaHost = T00056_A547Contratante_EmailSdaHost[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A547Contratante_EmailSdaHost", A547Contratante_EmailSdaHost);
            n547Contratante_EmailSdaHost = T00056_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = T00056_A548Contratante_EmailSdaUser[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A548Contratante_EmailSdaUser", A548Contratante_EmailSdaUser);
            n548Contratante_EmailSdaUser = T00056_n548Contratante_EmailSdaUser[0];
            A551Contratante_EmailSdaAut = T00056_A551Contratante_EmailSdaAut[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
            n551Contratante_EmailSdaAut = T00056_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = T00056_A552Contratante_EmailSdaPort[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
            n552Contratante_EmailSdaPort = T00056_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = T00056_A1048Contratante_EmailSdaSec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
            n1048Contratante_EmailSdaSec = T00056_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = T00056_A593Contratante_OSAutomatica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
            A1652Contratante_SSAutomatica = T00056_A1652Contratante_SSAutomatica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
            n1652Contratante_SSAutomatica = T00056_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = T00056_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = T00056_A1448Contratante_InicioDoExpediente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            n1448Contratante_InicioDoExpediente = T00056_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = T00056_A1192Contratante_FimDoExpediente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            n1192Contratante_FimDoExpediente = T00056_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = T00056_A1727Contratante_ServicoSSPadrao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
            n1727Contratante_ServicoSSPadrao = T00056_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = T00056_A1729Contratante_RetornaSS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
            n1729Contratante_RetornaSS = T00056_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = T00056_A1732Contratante_SSStatusPlanejamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
            n1732Contratante_SSStatusPlanejamento = T00056_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = T00056_A1733Contratante_SSPrestadoraUnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
            n1733Contratante_SSPrestadoraUnica = T00056_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = T00056_A1738Contratante_PropostaRqrSrv[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
            n1738Contratante_PropostaRqrSrv = T00056_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = T00056_A1739Contratante_PropostaRqrPrz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
            n1739Contratante_PropostaRqrPrz = T00056_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = T00056_A1740Contratante_PropostaRqrEsf[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
            n1740Contratante_PropostaRqrEsf = T00056_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = T00056_A1741Contratante_PropostaRqrCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
            n1741Contratante_PropostaRqrCnt = T00056_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = T00056_A1742Contratante_PropostaNvlCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
            n1742Contratante_PropostaNvlCnt = T00056_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = T00056_A1757Contratante_OSGeraOS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
            n1757Contratante_OSGeraOS = T00056_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = T00056_A1758Contratante_OSGeraTRP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
            n1758Contratante_OSGeraTRP = T00056_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = T00056_A1759Contratante_OSGeraTH[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
            n1759Contratante_OSGeraTH = T00056_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = T00056_A1760Contratante_OSGeraTRD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
            n1760Contratante_OSGeraTRD = T00056_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = T00056_A1761Contratante_OSGeraTR[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
            n1761Contratante_OSGeraTR = T00056_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = T00056_A1803Contratante_OSHmlgComPnd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
            n1803Contratante_OSHmlgComPnd = T00056_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = T00056_A1805Contratante_AtivoCirculante[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
            n1805Contratante_AtivoCirculante = T00056_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = T00056_A1806Contratante_PassivoCirculante[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
            n1806Contratante_PassivoCirculante = T00056_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = T00056_A1807Contratante_PatrimonioLiquido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
            n1807Contratante_PatrimonioLiquido = T00056_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = T00056_A1808Contratante_ReceitaBruta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
            n1808Contratante_ReceitaBruta = T00056_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = T00056_A1822Contratante_UsaOSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
            n1822Contratante_UsaOSistema = T00056_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = T00056_A2034Contratante_TtlRltGerencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
            n2034Contratante_TtlRltGerencial = T00056_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = T00056_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = T00056_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = T00056_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = T00056_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = T00056_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = T00056_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = T00056_A2089Contratante_SelecionaResponsavelOS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
            A2090Contratante_ExibePF = T00056_A2090Contratante_ExibePF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
            A1126Contratante_LogoTipoArq = T00056_A1126Contratante_LogoTipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1126Contratante_LogoTipoArq", A1126Contratante_LogoTipoArq);
            n1126Contratante_LogoTipoArq = T00056_n1126Contratante_LogoTipoArq[0];
            edtContratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Filetype", edtContratante_LogoArquivo_Filetype);
            A1125Contratante_LogoNomeArq = T00056_A1125Contratante_LogoNomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1125Contratante_LogoNomeArq", A1125Contratante_LogoNomeArq);
            n1125Contratante_LogoNomeArq = T00056_n1125Contratante_LogoNomeArq[0];
            edtContratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A25Municipio_Codigo = T00056_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = T00056_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = T00056_A335Contratante_PessoaCod[0];
            A23Estado_UF = T00056_A23Estado_UF[0];
            A1124Contratante_LogoArquivo = T00056_A1124Contratante_LogoArquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1124Contratante_LogoArquivo", A1124Contratante_LogoArquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1124Contratante_LogoArquivo));
            n1124Contratante_LogoArquivo = T00056_n1124Contratante_LogoArquivo[0];
            ZM05124( -36) ;
         }
         pr_default.close(4);
         OnLoadActions05124( ) ;
      }

      protected void OnLoadActions05124( )
      {
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Estado_UF = A23Estado_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
         }
         GXAMUNICIPIO_CODIGO_html05124( AV15Estado_UF) ;
      }

      protected void CheckExtendedTable05124( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( GxRegex.IsMatch(A14Contratante_Email,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A14Contratante_Email)) ) )
         {
            GX_msglist.addItem("O valor de Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "CONTRATANTE_EMAIL");
            AnyError = 1;
            GX_FocusControl = edtContratante_Email_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00054 */
         pr_default.execute(2, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynMunicipio_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A26Municipio_Nome = T00054_A26Municipio_Nome[0];
         A23Estado_UF = T00054_A23Estado_UF[0];
         pr_default.close(2);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Estado_UF = A23Estado_UF;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
         }
         if ( ! ( ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "B") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "S") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "E") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "A") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "R") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "C") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "D") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "H") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "O") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "P") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "L") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "X") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "N") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "J") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "I") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "T") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "Q") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "G") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "M") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) ) )
         {
            GX_msglist.addItem("Campo Status em planejamento fora do intervalo", "OutOfRange", 1, "CONTRATANTE_SSSTATUSPLANEJAMENTO");
            AnyError = 1;
            GX_FocusControl = cmbContratante_SSStatusPlanejamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A1742Contratante_PropostaNvlCnt == 0 ) || ( A1742Contratante_PropostaNvlCnt == 1 ) || ( A1742Contratante_PropostaNvlCnt == 2 ) || (0==A1742Contratante_PropostaNvlCnt) ) )
         {
            GX_msglist.addItem("Campo Nivel dos valores? fora do intervalo", "OutOfRange", 1, "CONTRATANTE_PROPOSTANVLCNT");
            AnyError = 1;
            GX_FocusControl = cmbContratante_PropostaNvlCnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GXAMUNICIPIO_CODIGO_html05124( AV15Estado_UF) ;
         /* Using cursor T00055 */
         pr_default.execute(3, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A12Contratante_CNPJ = T00055_A12Contratante_CNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
         n12Contratante_CNPJ = T00055_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = T00055_A9Contratante_RazaoSocial[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
         n9Contratante_RazaoSocial = T00055_n9Contratante_RazaoSocial[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors05124( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_37( int A25Municipio_Codigo )
      {
         /* Using cursor T00057 */
         pr_default.execute(5, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynMunicipio_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A26Municipio_Nome = T00057_A26Municipio_Nome[0];
         A23Estado_UF = T00057_A23Estado_UF[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A26Municipio_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A23Estado_UF))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_38( int A335Contratante_PessoaCod )
      {
         /* Using cursor T00058 */
         pr_default.execute(6, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A12Contratante_CNPJ = T00058_A12Contratante_CNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
         n12Contratante_CNPJ = T00058_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = T00058_A9Contratante_RazaoSocial[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
         n9Contratante_RazaoSocial = T00058_n9Contratante_RazaoSocial[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A12Contratante_CNPJ)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A9Contratante_RazaoSocial))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey05124( )
      {
         /* Using cursor T00059 */
         pr_default.execute(7, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound124 = 1;
         }
         else
         {
            RcdFound124 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00053 */
         pr_default.execute(1, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM05124( 36) ;
            RcdFound124 = 1;
            A29Contratante_Codigo = T00053_A29Contratante_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            n29Contratante_Codigo = T00053_n29Contratante_Codigo[0];
            A550Contratante_EmailSdaKey = T00053_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = T00053_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = T00053_A549Contratante_EmailSdaPass[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
            n549Contratante_EmailSdaPass = T00053_n549Contratante_EmailSdaPass[0];
            A10Contratante_NomeFantasia = T00053_A10Contratante_NomeFantasia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
            A11Contratante_IE = T00053_A11Contratante_IE[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Contratante_IE", A11Contratante_IE);
            A13Contratante_WebSite = T00053_A13Contratante_WebSite[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Contratante_WebSite", A13Contratante_WebSite);
            n13Contratante_WebSite = T00053_n13Contratante_WebSite[0];
            A14Contratante_Email = T00053_A14Contratante_Email[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Contratante_Email", A14Contratante_Email);
            n14Contratante_Email = T00053_n14Contratante_Email[0];
            A31Contratante_Telefone = T00053_A31Contratante_Telefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31Contratante_Telefone", A31Contratante_Telefone);
            A32Contratante_Ramal = T00053_A32Contratante_Ramal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32Contratante_Ramal", A32Contratante_Ramal);
            n32Contratante_Ramal = T00053_n32Contratante_Ramal[0];
            A33Contratante_Fax = T00053_A33Contratante_Fax[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33Contratante_Fax", A33Contratante_Fax);
            n33Contratante_Fax = T00053_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = T00053_A336Contratante_AgenciaNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A336Contratante_AgenciaNome", A336Contratante_AgenciaNome);
            n336Contratante_AgenciaNome = T00053_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = T00053_A337Contratante_AgenciaNro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A337Contratante_AgenciaNro", A337Contratante_AgenciaNro);
            n337Contratante_AgenciaNro = T00053_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = T00053_A338Contratante_BancoNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A338Contratante_BancoNome", A338Contratante_BancoNome);
            n338Contratante_BancoNome = T00053_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = T00053_A339Contratante_BancoNro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A339Contratante_BancoNro", A339Contratante_BancoNro);
            n339Contratante_BancoNro = T00053_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = T00053_A16Contratante_ContaCorrente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16Contratante_ContaCorrente", A16Contratante_ContaCorrente);
            n16Contratante_ContaCorrente = T00053_n16Contratante_ContaCorrente[0];
            A30Contratante_Ativo = T00053_A30Contratante_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30Contratante_Ativo", A30Contratante_Ativo);
            A547Contratante_EmailSdaHost = T00053_A547Contratante_EmailSdaHost[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A547Contratante_EmailSdaHost", A547Contratante_EmailSdaHost);
            n547Contratante_EmailSdaHost = T00053_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = T00053_A548Contratante_EmailSdaUser[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A548Contratante_EmailSdaUser", A548Contratante_EmailSdaUser);
            n548Contratante_EmailSdaUser = T00053_n548Contratante_EmailSdaUser[0];
            A551Contratante_EmailSdaAut = T00053_A551Contratante_EmailSdaAut[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
            n551Contratante_EmailSdaAut = T00053_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = T00053_A552Contratante_EmailSdaPort[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
            n552Contratante_EmailSdaPort = T00053_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = T00053_A1048Contratante_EmailSdaSec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
            n1048Contratante_EmailSdaSec = T00053_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = T00053_A593Contratante_OSAutomatica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
            A1652Contratante_SSAutomatica = T00053_A1652Contratante_SSAutomatica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
            n1652Contratante_SSAutomatica = T00053_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = T00053_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = T00053_A1448Contratante_InicioDoExpediente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            n1448Contratante_InicioDoExpediente = T00053_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = T00053_A1192Contratante_FimDoExpediente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            n1192Contratante_FimDoExpediente = T00053_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = T00053_A1727Contratante_ServicoSSPadrao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
            n1727Contratante_ServicoSSPadrao = T00053_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = T00053_A1729Contratante_RetornaSS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
            n1729Contratante_RetornaSS = T00053_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = T00053_A1732Contratante_SSStatusPlanejamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
            n1732Contratante_SSStatusPlanejamento = T00053_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = T00053_A1733Contratante_SSPrestadoraUnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
            n1733Contratante_SSPrestadoraUnica = T00053_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = T00053_A1738Contratante_PropostaRqrSrv[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
            n1738Contratante_PropostaRqrSrv = T00053_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = T00053_A1739Contratante_PropostaRqrPrz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
            n1739Contratante_PropostaRqrPrz = T00053_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = T00053_A1740Contratante_PropostaRqrEsf[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
            n1740Contratante_PropostaRqrEsf = T00053_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = T00053_A1741Contratante_PropostaRqrCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
            n1741Contratante_PropostaRqrCnt = T00053_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = T00053_A1742Contratante_PropostaNvlCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
            n1742Contratante_PropostaNvlCnt = T00053_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = T00053_A1757Contratante_OSGeraOS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
            n1757Contratante_OSGeraOS = T00053_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = T00053_A1758Contratante_OSGeraTRP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
            n1758Contratante_OSGeraTRP = T00053_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = T00053_A1759Contratante_OSGeraTH[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
            n1759Contratante_OSGeraTH = T00053_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = T00053_A1760Contratante_OSGeraTRD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
            n1760Contratante_OSGeraTRD = T00053_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = T00053_A1761Contratante_OSGeraTR[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
            n1761Contratante_OSGeraTR = T00053_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = T00053_A1803Contratante_OSHmlgComPnd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
            n1803Contratante_OSHmlgComPnd = T00053_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = T00053_A1805Contratante_AtivoCirculante[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
            n1805Contratante_AtivoCirculante = T00053_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = T00053_A1806Contratante_PassivoCirculante[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
            n1806Contratante_PassivoCirculante = T00053_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = T00053_A1807Contratante_PatrimonioLiquido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
            n1807Contratante_PatrimonioLiquido = T00053_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = T00053_A1808Contratante_ReceitaBruta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
            n1808Contratante_ReceitaBruta = T00053_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = T00053_A1822Contratante_UsaOSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
            n1822Contratante_UsaOSistema = T00053_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = T00053_A2034Contratante_TtlRltGerencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
            n2034Contratante_TtlRltGerencial = T00053_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = T00053_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = T00053_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = T00053_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = T00053_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = T00053_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = T00053_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = T00053_A2089Contratante_SelecionaResponsavelOS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
            A2090Contratante_ExibePF = T00053_A2090Contratante_ExibePF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
            A1126Contratante_LogoTipoArq = T00053_A1126Contratante_LogoTipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1126Contratante_LogoTipoArq", A1126Contratante_LogoTipoArq);
            n1126Contratante_LogoTipoArq = T00053_n1126Contratante_LogoTipoArq[0];
            edtContratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Filetype", edtContratante_LogoArquivo_Filetype);
            A1125Contratante_LogoNomeArq = T00053_A1125Contratante_LogoNomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1125Contratante_LogoNomeArq", A1125Contratante_LogoNomeArq);
            n1125Contratante_LogoNomeArq = T00053_n1125Contratante_LogoNomeArq[0];
            edtContratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A25Municipio_Codigo = T00053_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = T00053_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = T00053_A335Contratante_PessoaCod[0];
            A1124Contratante_LogoArquivo = T00053_A1124Contratante_LogoArquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1124Contratante_LogoArquivo", A1124Contratante_LogoArquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1124Contratante_LogoArquivo));
            n1124Contratante_LogoArquivo = T00053_n1124Contratante_LogoArquivo[0];
            O549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            n549Contratante_EmailSdaPass = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
            Z29Contratante_Codigo = A29Contratante_Codigo;
            sMode124 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load05124( ) ;
            if ( AnyError == 1 )
            {
               RcdFound124 = 0;
               InitializeNonKey05124( ) ;
            }
            Gx_mode = sMode124;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound124 = 0;
            InitializeNonKey05124( ) ;
            sMode124 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode124;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey05124( ) ;
         if ( RcdFound124 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound124 = 0;
         /* Using cursor T000510 */
         pr_default.execute(8, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000510_A29Contratante_Codigo[0] < A29Contratante_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000510_A29Contratante_Codigo[0] > A29Contratante_Codigo ) ) )
            {
               A29Contratante_Codigo = T000510_A29Contratante_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               n29Contratante_Codigo = T000510_n29Contratante_Codigo[0];
               RcdFound124 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound124 = 0;
         /* Using cursor T000511 */
         pr_default.execute(9, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T000511_A29Contratante_Codigo[0] > A29Contratante_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T000511_A29Contratante_Codigo[0] < A29Contratante_Codigo ) ) )
            {
               A29Contratante_Codigo = T000511_A29Contratante_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
               n29Contratante_Codigo = T000511_n29Contratante_Codigo[0];
               RcdFound124 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey05124( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratante_NomeFantasia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert05124( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound124 == 1 )
            {
               if ( A29Contratante_Codigo != Z29Contratante_Codigo )
               {
                  A29Contratante_Codigo = Z29Contratante_Codigo;
                  n29Contratante_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATANTE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratante_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratante_NomeFantasia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update05124( ) ;
                  GX_FocusControl = edtContratante_NomeFantasia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A29Contratante_Codigo != Z29Contratante_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratante_NomeFantasia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert05124( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATANTE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratante_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratante_NomeFantasia_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert05124( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A29Contratante_Codigo != Z29Contratante_Codigo )
         {
            A29Contratante_Codigo = Z29Contratante_Codigo;
            n29Contratante_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATANTE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratante_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratante_NomeFantasia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency05124( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00052 */
            pr_default.execute(0, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratante"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z550Contratante_EmailSdaKey, T00052_A550Contratante_EmailSdaKey[0]) != 0 ) || ( StringUtil.StrCmp(Z549Contratante_EmailSdaPass, T00052_A549Contratante_EmailSdaPass[0]) != 0 ) || ( StringUtil.StrCmp(Z10Contratante_NomeFantasia, T00052_A10Contratante_NomeFantasia[0]) != 0 ) || ( StringUtil.StrCmp(Z11Contratante_IE, T00052_A11Contratante_IE[0]) != 0 ) || ( StringUtil.StrCmp(Z13Contratante_WebSite, T00052_A13Contratante_WebSite[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z14Contratante_Email, T00052_A14Contratante_Email[0]) != 0 ) || ( StringUtil.StrCmp(Z31Contratante_Telefone, T00052_A31Contratante_Telefone[0]) != 0 ) || ( StringUtil.StrCmp(Z32Contratante_Ramal, T00052_A32Contratante_Ramal[0]) != 0 ) || ( StringUtil.StrCmp(Z33Contratante_Fax, T00052_A33Contratante_Fax[0]) != 0 ) || ( StringUtil.StrCmp(Z336Contratante_AgenciaNome, T00052_A336Contratante_AgenciaNome[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z337Contratante_AgenciaNro, T00052_A337Contratante_AgenciaNro[0]) != 0 ) || ( StringUtil.StrCmp(Z338Contratante_BancoNome, T00052_A338Contratante_BancoNome[0]) != 0 ) || ( StringUtil.StrCmp(Z339Contratante_BancoNro, T00052_A339Contratante_BancoNro[0]) != 0 ) || ( StringUtil.StrCmp(Z16Contratante_ContaCorrente, T00052_A16Contratante_ContaCorrente[0]) != 0 ) || ( Z30Contratante_Ativo != T00052_A30Contratante_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z547Contratante_EmailSdaHost, T00052_A547Contratante_EmailSdaHost[0]) != 0 ) || ( StringUtil.StrCmp(Z548Contratante_EmailSdaUser, T00052_A548Contratante_EmailSdaUser[0]) != 0 ) || ( Z551Contratante_EmailSdaAut != T00052_A551Contratante_EmailSdaAut[0] ) || ( Z552Contratante_EmailSdaPort != T00052_A552Contratante_EmailSdaPort[0] ) || ( Z1048Contratante_EmailSdaSec != T00052_A1048Contratante_EmailSdaSec[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z593Contratante_OSAutomatica != T00052_A593Contratante_OSAutomatica[0] ) || ( Z1652Contratante_SSAutomatica != T00052_A1652Contratante_SSAutomatica[0] ) || ( Z594Contratante_ExisteConferencia != T00052_A594Contratante_ExisteConferencia[0] ) || ( Z1448Contratante_InicioDoExpediente != T00052_A1448Contratante_InicioDoExpediente[0] ) || ( Z1192Contratante_FimDoExpediente != T00052_A1192Contratante_FimDoExpediente[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1727Contratante_ServicoSSPadrao != T00052_A1727Contratante_ServicoSSPadrao[0] ) || ( Z1729Contratante_RetornaSS != T00052_A1729Contratante_RetornaSS[0] ) || ( StringUtil.StrCmp(Z1732Contratante_SSStatusPlanejamento, T00052_A1732Contratante_SSStatusPlanejamento[0]) != 0 ) || ( Z1733Contratante_SSPrestadoraUnica != T00052_A1733Contratante_SSPrestadoraUnica[0] ) || ( Z1738Contratante_PropostaRqrSrv != T00052_A1738Contratante_PropostaRqrSrv[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1739Contratante_PropostaRqrPrz != T00052_A1739Contratante_PropostaRqrPrz[0] ) || ( Z1740Contratante_PropostaRqrEsf != T00052_A1740Contratante_PropostaRqrEsf[0] ) || ( Z1741Contratante_PropostaRqrCnt != T00052_A1741Contratante_PropostaRqrCnt[0] ) || ( Z1742Contratante_PropostaNvlCnt != T00052_A1742Contratante_PropostaNvlCnt[0] ) || ( Z1757Contratante_OSGeraOS != T00052_A1757Contratante_OSGeraOS[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1758Contratante_OSGeraTRP != T00052_A1758Contratante_OSGeraTRP[0] ) || ( Z1759Contratante_OSGeraTH != T00052_A1759Contratante_OSGeraTH[0] ) || ( Z1760Contratante_OSGeraTRD != T00052_A1760Contratante_OSGeraTRD[0] ) || ( Z1761Contratante_OSGeraTR != T00052_A1761Contratante_OSGeraTR[0] ) || ( Z1803Contratante_OSHmlgComPnd != T00052_A1803Contratante_OSHmlgComPnd[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1805Contratante_AtivoCirculante != T00052_A1805Contratante_AtivoCirculante[0] ) || ( Z1806Contratante_PassivoCirculante != T00052_A1806Contratante_PassivoCirculante[0] ) || ( Z1807Contratante_PatrimonioLiquido != T00052_A1807Contratante_PatrimonioLiquido[0] ) || ( Z1808Contratante_ReceitaBruta != T00052_A1808Contratante_ReceitaBruta[0] ) || ( Z1822Contratante_UsaOSistema != T00052_A1822Contratante_UsaOSistema[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z2034Contratante_TtlRltGerencial, T00052_A2034Contratante_TtlRltGerencial[0]) != 0 ) || ( Z2084Contratante_PrzAoRtr != T00052_A2084Contratante_PrzAoRtr[0] ) || ( Z2083Contratante_PrzActRtr != T00052_A2083Contratante_PrzActRtr[0] ) || ( Z2085Contratante_RequerOrigem != T00052_A2085Contratante_RequerOrigem[0] ) || ( Z2089Contratante_SelecionaResponsavelOS != T00052_A2089Contratante_SelecionaResponsavelOS[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2090Contratante_ExibePF != T00052_A2090Contratante_ExibePF[0] ) || ( Z25Municipio_Codigo != T00052_A25Municipio_Codigo[0] ) || ( Z335Contratante_PessoaCod != T00052_A335Contratante_PessoaCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z550Contratante_EmailSdaKey, T00052_A550Contratante_EmailSdaKey[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaKey");
                  GXUtil.WriteLogRaw("Old: ",Z550Contratante_EmailSdaKey);
                  GXUtil.WriteLogRaw("Current: ",T00052_A550Contratante_EmailSdaKey[0]);
               }
               if ( StringUtil.StrCmp(Z549Contratante_EmailSdaPass, T00052_A549Contratante_EmailSdaPass[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaPass");
                  GXUtil.WriteLogRaw("Old: ",Z549Contratante_EmailSdaPass);
                  GXUtil.WriteLogRaw("Current: ",T00052_A549Contratante_EmailSdaPass[0]);
               }
               if ( StringUtil.StrCmp(Z10Contratante_NomeFantasia, T00052_A10Contratante_NomeFantasia[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_NomeFantasia");
                  GXUtil.WriteLogRaw("Old: ",Z10Contratante_NomeFantasia);
                  GXUtil.WriteLogRaw("Current: ",T00052_A10Contratante_NomeFantasia[0]);
               }
               if ( StringUtil.StrCmp(Z11Contratante_IE, T00052_A11Contratante_IE[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_IE");
                  GXUtil.WriteLogRaw("Old: ",Z11Contratante_IE);
                  GXUtil.WriteLogRaw("Current: ",T00052_A11Contratante_IE[0]);
               }
               if ( StringUtil.StrCmp(Z13Contratante_WebSite, T00052_A13Contratante_WebSite[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_WebSite");
                  GXUtil.WriteLogRaw("Old: ",Z13Contratante_WebSite);
                  GXUtil.WriteLogRaw("Current: ",T00052_A13Contratante_WebSite[0]);
               }
               if ( StringUtil.StrCmp(Z14Contratante_Email, T00052_A14Contratante_Email[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_Email");
                  GXUtil.WriteLogRaw("Old: ",Z14Contratante_Email);
                  GXUtil.WriteLogRaw("Current: ",T00052_A14Contratante_Email[0]);
               }
               if ( StringUtil.StrCmp(Z31Contratante_Telefone, T00052_A31Contratante_Telefone[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_Telefone");
                  GXUtil.WriteLogRaw("Old: ",Z31Contratante_Telefone);
                  GXUtil.WriteLogRaw("Current: ",T00052_A31Contratante_Telefone[0]);
               }
               if ( StringUtil.StrCmp(Z32Contratante_Ramal, T00052_A32Contratante_Ramal[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_Ramal");
                  GXUtil.WriteLogRaw("Old: ",Z32Contratante_Ramal);
                  GXUtil.WriteLogRaw("Current: ",T00052_A32Contratante_Ramal[0]);
               }
               if ( StringUtil.StrCmp(Z33Contratante_Fax, T00052_A33Contratante_Fax[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_Fax");
                  GXUtil.WriteLogRaw("Old: ",Z33Contratante_Fax);
                  GXUtil.WriteLogRaw("Current: ",T00052_A33Contratante_Fax[0]);
               }
               if ( StringUtil.StrCmp(Z336Contratante_AgenciaNome, T00052_A336Contratante_AgenciaNome[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_AgenciaNome");
                  GXUtil.WriteLogRaw("Old: ",Z336Contratante_AgenciaNome);
                  GXUtil.WriteLogRaw("Current: ",T00052_A336Contratante_AgenciaNome[0]);
               }
               if ( StringUtil.StrCmp(Z337Contratante_AgenciaNro, T00052_A337Contratante_AgenciaNro[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_AgenciaNro");
                  GXUtil.WriteLogRaw("Old: ",Z337Contratante_AgenciaNro);
                  GXUtil.WriteLogRaw("Current: ",T00052_A337Contratante_AgenciaNro[0]);
               }
               if ( StringUtil.StrCmp(Z338Contratante_BancoNome, T00052_A338Contratante_BancoNome[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_BancoNome");
                  GXUtil.WriteLogRaw("Old: ",Z338Contratante_BancoNome);
                  GXUtil.WriteLogRaw("Current: ",T00052_A338Contratante_BancoNome[0]);
               }
               if ( StringUtil.StrCmp(Z339Contratante_BancoNro, T00052_A339Contratante_BancoNro[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_BancoNro");
                  GXUtil.WriteLogRaw("Old: ",Z339Contratante_BancoNro);
                  GXUtil.WriteLogRaw("Current: ",T00052_A339Contratante_BancoNro[0]);
               }
               if ( StringUtil.StrCmp(Z16Contratante_ContaCorrente, T00052_A16Contratante_ContaCorrente[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_ContaCorrente");
                  GXUtil.WriteLogRaw("Old: ",Z16Contratante_ContaCorrente);
                  GXUtil.WriteLogRaw("Current: ",T00052_A16Contratante_ContaCorrente[0]);
               }
               if ( Z30Contratante_Ativo != T00052_A30Contratante_Ativo[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z30Contratante_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00052_A30Contratante_Ativo[0]);
               }
               if ( StringUtil.StrCmp(Z547Contratante_EmailSdaHost, T00052_A547Contratante_EmailSdaHost[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaHost");
                  GXUtil.WriteLogRaw("Old: ",Z547Contratante_EmailSdaHost);
                  GXUtil.WriteLogRaw("Current: ",T00052_A547Contratante_EmailSdaHost[0]);
               }
               if ( StringUtil.StrCmp(Z548Contratante_EmailSdaUser, T00052_A548Contratante_EmailSdaUser[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaUser");
                  GXUtil.WriteLogRaw("Old: ",Z548Contratante_EmailSdaUser);
                  GXUtil.WriteLogRaw("Current: ",T00052_A548Contratante_EmailSdaUser[0]);
               }
               if ( Z551Contratante_EmailSdaAut != T00052_A551Contratante_EmailSdaAut[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaAut");
                  GXUtil.WriteLogRaw("Old: ",Z551Contratante_EmailSdaAut);
                  GXUtil.WriteLogRaw("Current: ",T00052_A551Contratante_EmailSdaAut[0]);
               }
               if ( Z552Contratante_EmailSdaPort != T00052_A552Contratante_EmailSdaPort[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaPort");
                  GXUtil.WriteLogRaw("Old: ",Z552Contratante_EmailSdaPort);
                  GXUtil.WriteLogRaw("Current: ",T00052_A552Contratante_EmailSdaPort[0]);
               }
               if ( Z1048Contratante_EmailSdaSec != T00052_A1048Contratante_EmailSdaSec[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_EmailSdaSec");
                  GXUtil.WriteLogRaw("Old: ",Z1048Contratante_EmailSdaSec);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1048Contratante_EmailSdaSec[0]);
               }
               if ( Z593Contratante_OSAutomatica != T00052_A593Contratante_OSAutomatica[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSAutomatica");
                  GXUtil.WriteLogRaw("Old: ",Z593Contratante_OSAutomatica);
                  GXUtil.WriteLogRaw("Current: ",T00052_A593Contratante_OSAutomatica[0]);
               }
               if ( Z1652Contratante_SSAutomatica != T00052_A1652Contratante_SSAutomatica[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_SSAutomatica");
                  GXUtil.WriteLogRaw("Old: ",Z1652Contratante_SSAutomatica);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1652Contratante_SSAutomatica[0]);
               }
               if ( Z594Contratante_ExisteConferencia != T00052_A594Contratante_ExisteConferencia[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_ExisteConferencia");
                  GXUtil.WriteLogRaw("Old: ",Z594Contratante_ExisteConferencia);
                  GXUtil.WriteLogRaw("Current: ",T00052_A594Contratante_ExisteConferencia[0]);
               }
               if ( Z1448Contratante_InicioDoExpediente != T00052_A1448Contratante_InicioDoExpediente[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_InicioDoExpediente");
                  GXUtil.WriteLogRaw("Old: ",Z1448Contratante_InicioDoExpediente);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1448Contratante_InicioDoExpediente[0]);
               }
               if ( Z1192Contratante_FimDoExpediente != T00052_A1192Contratante_FimDoExpediente[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_FimDoExpediente");
                  GXUtil.WriteLogRaw("Old: ",Z1192Contratante_FimDoExpediente);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1192Contratante_FimDoExpediente[0]);
               }
               if ( Z1727Contratante_ServicoSSPadrao != T00052_A1727Contratante_ServicoSSPadrao[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_ServicoSSPadrao");
                  GXUtil.WriteLogRaw("Old: ",Z1727Contratante_ServicoSSPadrao);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1727Contratante_ServicoSSPadrao[0]);
               }
               if ( Z1729Contratante_RetornaSS != T00052_A1729Contratante_RetornaSS[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_RetornaSS");
                  GXUtil.WriteLogRaw("Old: ",Z1729Contratante_RetornaSS);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1729Contratante_RetornaSS[0]);
               }
               if ( StringUtil.StrCmp(Z1732Contratante_SSStatusPlanejamento, T00052_A1732Contratante_SSStatusPlanejamento[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_SSStatusPlanejamento");
                  GXUtil.WriteLogRaw("Old: ",Z1732Contratante_SSStatusPlanejamento);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1732Contratante_SSStatusPlanejamento[0]);
               }
               if ( Z1733Contratante_SSPrestadoraUnica != T00052_A1733Contratante_SSPrestadoraUnica[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_SSPrestadoraUnica");
                  GXUtil.WriteLogRaw("Old: ",Z1733Contratante_SSPrestadoraUnica);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1733Contratante_SSPrestadoraUnica[0]);
               }
               if ( Z1738Contratante_PropostaRqrSrv != T00052_A1738Contratante_PropostaRqrSrv[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PropostaRqrSrv");
                  GXUtil.WriteLogRaw("Old: ",Z1738Contratante_PropostaRqrSrv);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1738Contratante_PropostaRqrSrv[0]);
               }
               if ( Z1739Contratante_PropostaRqrPrz != T00052_A1739Contratante_PropostaRqrPrz[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PropostaRqrPrz");
                  GXUtil.WriteLogRaw("Old: ",Z1739Contratante_PropostaRqrPrz);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1739Contratante_PropostaRqrPrz[0]);
               }
               if ( Z1740Contratante_PropostaRqrEsf != T00052_A1740Contratante_PropostaRqrEsf[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PropostaRqrEsf");
                  GXUtil.WriteLogRaw("Old: ",Z1740Contratante_PropostaRqrEsf);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1740Contratante_PropostaRqrEsf[0]);
               }
               if ( Z1741Contratante_PropostaRqrCnt != T00052_A1741Contratante_PropostaRqrCnt[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PropostaRqrCnt");
                  GXUtil.WriteLogRaw("Old: ",Z1741Contratante_PropostaRqrCnt);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1741Contratante_PropostaRqrCnt[0]);
               }
               if ( Z1742Contratante_PropostaNvlCnt != T00052_A1742Contratante_PropostaNvlCnt[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PropostaNvlCnt");
                  GXUtil.WriteLogRaw("Old: ",Z1742Contratante_PropostaNvlCnt);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1742Contratante_PropostaNvlCnt[0]);
               }
               if ( Z1757Contratante_OSGeraOS != T00052_A1757Contratante_OSGeraOS[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSGeraOS");
                  GXUtil.WriteLogRaw("Old: ",Z1757Contratante_OSGeraOS);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1757Contratante_OSGeraOS[0]);
               }
               if ( Z1758Contratante_OSGeraTRP != T00052_A1758Contratante_OSGeraTRP[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSGeraTRP");
                  GXUtil.WriteLogRaw("Old: ",Z1758Contratante_OSGeraTRP);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1758Contratante_OSGeraTRP[0]);
               }
               if ( Z1759Contratante_OSGeraTH != T00052_A1759Contratante_OSGeraTH[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSGeraTH");
                  GXUtil.WriteLogRaw("Old: ",Z1759Contratante_OSGeraTH);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1759Contratante_OSGeraTH[0]);
               }
               if ( Z1760Contratante_OSGeraTRD != T00052_A1760Contratante_OSGeraTRD[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSGeraTRD");
                  GXUtil.WriteLogRaw("Old: ",Z1760Contratante_OSGeraTRD);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1760Contratante_OSGeraTRD[0]);
               }
               if ( Z1761Contratante_OSGeraTR != T00052_A1761Contratante_OSGeraTR[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSGeraTR");
                  GXUtil.WriteLogRaw("Old: ",Z1761Contratante_OSGeraTR);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1761Contratante_OSGeraTR[0]);
               }
               if ( Z1803Contratante_OSHmlgComPnd != T00052_A1803Contratante_OSHmlgComPnd[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_OSHmlgComPnd");
                  GXUtil.WriteLogRaw("Old: ",Z1803Contratante_OSHmlgComPnd);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1803Contratante_OSHmlgComPnd[0]);
               }
               if ( Z1805Contratante_AtivoCirculante != T00052_A1805Contratante_AtivoCirculante[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_AtivoCirculante");
                  GXUtil.WriteLogRaw("Old: ",Z1805Contratante_AtivoCirculante);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1805Contratante_AtivoCirculante[0]);
               }
               if ( Z1806Contratante_PassivoCirculante != T00052_A1806Contratante_PassivoCirculante[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PassivoCirculante");
                  GXUtil.WriteLogRaw("Old: ",Z1806Contratante_PassivoCirculante);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1806Contratante_PassivoCirculante[0]);
               }
               if ( Z1807Contratante_PatrimonioLiquido != T00052_A1807Contratante_PatrimonioLiquido[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PatrimonioLiquido");
                  GXUtil.WriteLogRaw("Old: ",Z1807Contratante_PatrimonioLiquido);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1807Contratante_PatrimonioLiquido[0]);
               }
               if ( Z1808Contratante_ReceitaBruta != T00052_A1808Contratante_ReceitaBruta[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_ReceitaBruta");
                  GXUtil.WriteLogRaw("Old: ",Z1808Contratante_ReceitaBruta);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1808Contratante_ReceitaBruta[0]);
               }
               if ( Z1822Contratante_UsaOSistema != T00052_A1822Contratante_UsaOSistema[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_UsaOSistema");
                  GXUtil.WriteLogRaw("Old: ",Z1822Contratante_UsaOSistema);
                  GXUtil.WriteLogRaw("Current: ",T00052_A1822Contratante_UsaOSistema[0]);
               }
               if ( StringUtil.StrCmp(Z2034Contratante_TtlRltGerencial, T00052_A2034Contratante_TtlRltGerencial[0]) != 0 )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_TtlRltGerencial");
                  GXUtil.WriteLogRaw("Old: ",Z2034Contratante_TtlRltGerencial);
                  GXUtil.WriteLogRaw("Current: ",T00052_A2034Contratante_TtlRltGerencial[0]);
               }
               if ( Z2084Contratante_PrzAoRtr != T00052_A2084Contratante_PrzAoRtr[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PrzAoRtr");
                  GXUtil.WriteLogRaw("Old: ",Z2084Contratante_PrzAoRtr);
                  GXUtil.WriteLogRaw("Current: ",T00052_A2084Contratante_PrzAoRtr[0]);
               }
               if ( Z2083Contratante_PrzActRtr != T00052_A2083Contratante_PrzActRtr[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PrzActRtr");
                  GXUtil.WriteLogRaw("Old: ",Z2083Contratante_PrzActRtr);
                  GXUtil.WriteLogRaw("Current: ",T00052_A2083Contratante_PrzActRtr[0]);
               }
               if ( Z2085Contratante_RequerOrigem != T00052_A2085Contratante_RequerOrigem[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_RequerOrigem");
                  GXUtil.WriteLogRaw("Old: ",Z2085Contratante_RequerOrigem);
                  GXUtil.WriteLogRaw("Current: ",T00052_A2085Contratante_RequerOrigem[0]);
               }
               if ( Z2089Contratante_SelecionaResponsavelOS != T00052_A2089Contratante_SelecionaResponsavelOS[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_SelecionaResponsavelOS");
                  GXUtil.WriteLogRaw("Old: ",Z2089Contratante_SelecionaResponsavelOS);
                  GXUtil.WriteLogRaw("Current: ",T00052_A2089Contratante_SelecionaResponsavelOS[0]);
               }
               if ( Z2090Contratante_ExibePF != T00052_A2090Contratante_ExibePF[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_ExibePF");
                  GXUtil.WriteLogRaw("Old: ",Z2090Contratante_ExibePF);
                  GXUtil.WriteLogRaw("Current: ",T00052_A2090Contratante_ExibePF[0]);
               }
               if ( Z25Municipio_Codigo != T00052_A25Municipio_Codigo[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Municipio_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z25Municipio_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00052_A25Municipio_Codigo[0]);
               }
               if ( Z335Contratante_PessoaCod != T00052_A335Contratante_PessoaCod[0] )
               {
                  GXUtil.WriteLog("contratante:[seudo value changed for attri]"+"Contratante_PessoaCod");
                  GXUtil.WriteLogRaw("Old: ",Z335Contratante_PessoaCod);
                  GXUtil.WriteLogRaw("Current: ",T00052_A335Contratante_PessoaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contratante"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert05124( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable05124( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM05124( 0) ;
            CheckOptimisticConcurrency05124( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm05124( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert05124( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000512 */
                     A1126Contratante_LogoTipoArq = edtContratante_LogoArquivo_Filetype;
                     n1126Contratante_LogoTipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1126Contratante_LogoTipoArq", A1126Contratante_LogoTipoArq);
                     A1125Contratante_LogoNomeArq = edtContratante_LogoArquivo_Filename;
                     n1125Contratante_LogoNomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1125Contratante_LogoNomeArq", A1125Contratante_LogoNomeArq);
                     pr_default.execute(10, new Object[] {n550Contratante_EmailSdaKey, A550Contratante_EmailSdaKey, n549Contratante_EmailSdaPass, A549Contratante_EmailSdaPass, A10Contratante_NomeFantasia, A11Contratante_IE, n13Contratante_WebSite, A13Contratante_WebSite, n14Contratante_Email, A14Contratante_Email, A31Contratante_Telefone, n32Contratante_Ramal, A32Contratante_Ramal, n33Contratante_Fax, A33Contratante_Fax, n336Contratante_AgenciaNome, A336Contratante_AgenciaNome, n337Contratante_AgenciaNro, A337Contratante_AgenciaNro, n338Contratante_BancoNome, A338Contratante_BancoNome, n339Contratante_BancoNro, A339Contratante_BancoNro, n16Contratante_ContaCorrente, A16Contratante_ContaCorrente, A30Contratante_Ativo, n547Contratante_EmailSdaHost, A547Contratante_EmailSdaHost, n548Contratante_EmailSdaUser, A548Contratante_EmailSdaUser, n551Contratante_EmailSdaAut, A551Contratante_EmailSdaAut, n552Contratante_EmailSdaPort, A552Contratante_EmailSdaPort, n1048Contratante_EmailSdaSec, A1048Contratante_EmailSdaSec, A593Contratante_OSAutomatica, n1652Contratante_SSAutomatica, A1652Contratante_SSAutomatica, A594Contratante_ExisteConferencia, n1124Contratante_LogoArquivo, A1124Contratante_LogoArquivo, n1448Contratante_InicioDoExpediente, A1448Contratante_InicioDoExpediente, n1192Contratante_FimDoExpediente, A1192Contratante_FimDoExpediente, n1727Contratante_ServicoSSPadrao, A1727Contratante_ServicoSSPadrao, n1729Contratante_RetornaSS, A1729Contratante_RetornaSS, n1732Contratante_SSStatusPlanejamento, A1732Contratante_SSStatusPlanejamento, n1733Contratante_SSPrestadoraUnica, A1733Contratante_SSPrestadoraUnica, n1738Contratante_PropostaRqrSrv, A1738Contratante_PropostaRqrSrv, n1739Contratante_PropostaRqrPrz, A1739Contratante_PropostaRqrPrz, n1740Contratante_PropostaRqrEsf, A1740Contratante_PropostaRqrEsf, n1741Contratante_PropostaRqrCnt, A1741Contratante_PropostaRqrCnt, n1742Contratante_PropostaNvlCnt, A1742Contratante_PropostaNvlCnt, n1757Contratante_OSGeraOS, A1757Contratante_OSGeraOS, n1758Contratante_OSGeraTRP, A1758Contratante_OSGeraTRP, n1759Contratante_OSGeraTH, A1759Contratante_OSGeraTH, n1760Contratante_OSGeraTRD, A1760Contratante_OSGeraTRD, n1761Contratante_OSGeraTR, A1761Contratante_OSGeraTR, n1803Contratante_OSHmlgComPnd, A1803Contratante_OSHmlgComPnd, n1805Contratante_AtivoCirculante, A1805Contratante_AtivoCirculante, n1806Contratante_PassivoCirculante, A1806Contratante_PassivoCirculante, n1807Contratante_PatrimonioLiquido, A1807Contratante_PatrimonioLiquido, n1808Contratante_ReceitaBruta, A1808Contratante_ReceitaBruta, n1822Contratante_UsaOSistema, A1822Contratante_UsaOSistema, n2034Contratante_TtlRltGerencial, A2034Contratante_TtlRltGerencial, n2084Contratante_PrzAoRtr, A2084Contratante_PrzAoRtr, n2083Contratante_PrzActRtr, A2083Contratante_PrzActRtr, n2085Contratante_RequerOrigem, A2085Contratante_RequerOrigem, A2089Contratante_SelecionaResponsavelOS, A2090Contratante_ExibePF, n1126Contratante_LogoTipoArq, A1126Contratante_LogoTipoArq, n1125Contratante_LogoNomeArq, A1125Contratante_LogoNomeArq, n25Municipio_Codigo, A25Municipio_Codigo, A335Contratante_PessoaCod});
                     A29Contratante_Codigo = T000512_A29Contratante_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
                     n29Contratante_Codigo = T000512_n29Contratante_Codigo[0];
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption050( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load05124( ) ;
            }
            EndLevel05124( ) ;
         }
         CloseExtendedTableCursors05124( ) ;
      }

      protected void Update05124( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable05124( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency05124( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm05124( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate05124( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000513 */
                     A1126Contratante_LogoTipoArq = edtContratante_LogoArquivo_Filetype;
                     n1126Contratante_LogoTipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1126Contratante_LogoTipoArq", A1126Contratante_LogoTipoArq);
                     A1125Contratante_LogoNomeArq = edtContratante_LogoArquivo_Filename;
                     n1125Contratante_LogoNomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1125Contratante_LogoNomeArq", A1125Contratante_LogoNomeArq);
                     pr_default.execute(11, new Object[] {n550Contratante_EmailSdaKey, A550Contratante_EmailSdaKey, n549Contratante_EmailSdaPass, A549Contratante_EmailSdaPass, A10Contratante_NomeFantasia, A11Contratante_IE, n13Contratante_WebSite, A13Contratante_WebSite, n14Contratante_Email, A14Contratante_Email, A31Contratante_Telefone, n32Contratante_Ramal, A32Contratante_Ramal, n33Contratante_Fax, A33Contratante_Fax, n336Contratante_AgenciaNome, A336Contratante_AgenciaNome, n337Contratante_AgenciaNro, A337Contratante_AgenciaNro, n338Contratante_BancoNome, A338Contratante_BancoNome, n339Contratante_BancoNro, A339Contratante_BancoNro, n16Contratante_ContaCorrente, A16Contratante_ContaCorrente, A30Contratante_Ativo, n547Contratante_EmailSdaHost, A547Contratante_EmailSdaHost, n548Contratante_EmailSdaUser, A548Contratante_EmailSdaUser, n551Contratante_EmailSdaAut, A551Contratante_EmailSdaAut, n552Contratante_EmailSdaPort, A552Contratante_EmailSdaPort, n1048Contratante_EmailSdaSec, A1048Contratante_EmailSdaSec, A593Contratante_OSAutomatica, n1652Contratante_SSAutomatica, A1652Contratante_SSAutomatica, A594Contratante_ExisteConferencia, n1448Contratante_InicioDoExpediente, A1448Contratante_InicioDoExpediente, n1192Contratante_FimDoExpediente, A1192Contratante_FimDoExpediente, n1727Contratante_ServicoSSPadrao, A1727Contratante_ServicoSSPadrao, n1729Contratante_RetornaSS, A1729Contratante_RetornaSS, n1732Contratante_SSStatusPlanejamento, A1732Contratante_SSStatusPlanejamento, n1733Contratante_SSPrestadoraUnica, A1733Contratante_SSPrestadoraUnica, n1738Contratante_PropostaRqrSrv, A1738Contratante_PropostaRqrSrv, n1739Contratante_PropostaRqrPrz, A1739Contratante_PropostaRqrPrz, n1740Contratante_PropostaRqrEsf, A1740Contratante_PropostaRqrEsf, n1741Contratante_PropostaRqrCnt, A1741Contratante_PropostaRqrCnt, n1742Contratante_PropostaNvlCnt, A1742Contratante_PropostaNvlCnt, n1757Contratante_OSGeraOS, A1757Contratante_OSGeraOS, n1758Contratante_OSGeraTRP, A1758Contratante_OSGeraTRP, n1759Contratante_OSGeraTH, A1759Contratante_OSGeraTH, n1760Contratante_OSGeraTRD, A1760Contratante_OSGeraTRD, n1761Contratante_OSGeraTR, A1761Contratante_OSGeraTR, n1803Contratante_OSHmlgComPnd, A1803Contratante_OSHmlgComPnd, n1805Contratante_AtivoCirculante, A1805Contratante_AtivoCirculante, n1806Contratante_PassivoCirculante, A1806Contratante_PassivoCirculante, n1807Contratante_PatrimonioLiquido, A1807Contratante_PatrimonioLiquido, n1808Contratante_ReceitaBruta, A1808Contratante_ReceitaBruta, n1822Contratante_UsaOSistema, A1822Contratante_UsaOSistema, n2034Contratante_TtlRltGerencial, A2034Contratante_TtlRltGerencial, n2084Contratante_PrzAoRtr, A2084Contratante_PrzAoRtr, n2083Contratante_PrzActRtr, A2083Contratante_PrzActRtr, n2085Contratante_RequerOrigem, A2085Contratante_RequerOrigem, A2089Contratante_SelecionaResponsavelOS, A2090Contratante_ExibePF, n1126Contratante_LogoTipoArq, A1126Contratante_LogoTipoArq, n1125Contratante_LogoNomeArq, A1125Contratante_LogoNomeArq, n25Municipio_Codigo, A25Municipio_Codigo, A335Contratante_PessoaCod, n29Contratante_Codigo, A29Contratante_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratante"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate05124( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel05124( ) ;
         }
         CloseExtendedTableCursors05124( ) ;
      }

      protected void DeferredUpdate05124( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000514 */
            pr_default.execute(12, new Object[] {n1124Contratante_LogoArquivo, A1124Contratante_LogoArquivo, n29Contratante_Codigo, A29Contratante_Codigo});
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency05124( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls05124( ) ;
            AfterConfirm05124( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete05124( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000515 */
                  pr_default.execute(13, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode124 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel05124( ) ;
         Gx_mode = sMode124;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls05124( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000516 */
            pr_default.execute(14, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            A26Municipio_Nome = T000516_A26Municipio_Nome[0];
            A23Estado_UF = T000516_A23Estado_UF[0];
            pr_default.close(14);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV15Estado_UF = A23Estado_UF;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
            }
            GXAMUNICIPIO_CODIGO_html05124( AV15Estado_UF) ;
            /* Using cursor T000517 */
            pr_default.execute(15, new Object[] {A335Contratante_PessoaCod});
            A12Contratante_CNPJ = T000517_A12Contratante_CNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
            n12Contratante_CNPJ = T000517_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = T000517_A9Contratante_RazaoSocial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
            n9Contratante_RazaoSocial = T000517_n9Contratante_RazaoSocial[0];
            pr_default.close(15);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000518 */
            pr_default.execute(16, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Redmine"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T000519 */
            pr_default.execute(17, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T000520 */
            pr_default.execute(18, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area de Trabalho"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
         }
      }

      protected void EndLevel05124( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete05124( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.CommitDataStores( "Contratante");
            if ( AnyError == 0 )
            {
               ConfirmValues050( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.RollbackDataStores( "Contratante");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart05124( )
      {
         /* Scan By routine */
         /* Using cursor T000521 */
         pr_default.execute(19);
         RcdFound124 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound124 = 1;
            A29Contratante_Codigo = T000521_A29Contratante_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            n29Contratante_Codigo = T000521_n29Contratante_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext05124( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound124 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound124 = 1;
            A29Contratante_Codigo = T000521_A29Contratante_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            n29Contratante_Codigo = T000521_n29Contratante_Codigo[0];
         }
      }

      protected void ScanEnd05124( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm05124( )
      {
         /* After Confirm Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 ) )
         {
            A550Contratante_EmailSdaKey = Crypto.GetEncryptionKey( );
            n550Contratante_EmailSdaKey = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A550Contratante_EmailSdaKey", A550Contratante_EmailSdaKey);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 ) )
         {
            A549Contratante_EmailSdaPass = Crypto.Encrypt64( O549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
            n549Contratante_EmailSdaPass = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
         }
      }

      protected void BeforeInsert05124( )
      {
         /* Before Insert Rules */
         if ( (0==A25Municipio_Codigo) )
         {
            A25Municipio_Codigo = 0;
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         }
      }

      protected void BeforeUpdate05124( )
      {
         /* Before Update Rules */
         new loadauditcontratante(context ).execute(  "Y", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         if ( (0==A25Municipio_Codigo) )
         {
            A25Municipio_Codigo = 0;
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            n25Municipio_Codigo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         }
      }

      protected void BeforeDelete05124( )
      {
         /* Before Delete Rules */
         new loadauditcontratante(context ).execute(  "Y", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete05124( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratante(context ).execute(  "N", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratante(context ).execute(  "N", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate05124( )
      {
         /* Before Validate Rules */
         if ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 )
         {
            A550Contratante_EmailSdaKey = Crypto.GetEncryptionKey( );
            n550Contratante_EmailSdaKey = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A550Contratante_EmailSdaKey", A550Contratante_EmailSdaKey);
         }
         if ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 )
         {
            A549Contratante_EmailSdaPass = Crypto.Encrypt64( StringUtil.Trim( A549Contratante_EmailSdaPass), A550Contratante_EmailSdaKey);
            n549Contratante_EmailSdaPass = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
         }
      }

      protected void DisableAttributes05124( )
      {
         edtContratante_RazaoSocial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_RazaoSocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_RazaoSocial_Enabled), 5, 0)));
         edtContratante_NomeFantasia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_NomeFantasia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_NomeFantasia_Enabled), 5, 0)));
         edtContratante_CNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_CNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_CNPJ_Enabled), 5, 0)));
         edtContratante_IE_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_IE_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_IE_Enabled), 5, 0)));
         edtContratante_Telefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Telefone_Enabled), 5, 0)));
         edtContratante_Ramal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Ramal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Ramal_Enabled), 5, 0)));
         edtContratante_Fax_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Fax_Enabled), 5, 0)));
         edtContratante_WebSite_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_WebSite_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_WebSite_Enabled), 5, 0)));
         edtContratante_Email_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Email_Enabled), 5, 0)));
         edtContratante_BancoNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_BancoNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_BancoNome_Enabled), 5, 0)));
         edtContratante_BancoNro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_BancoNro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_BancoNro_Enabled), 5, 0)));
         edtContratante_AgenciaNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_AgenciaNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_AgenciaNome_Enabled), 5, 0)));
         edtContratante_AgenciaNro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_AgenciaNro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_AgenciaNro_Enabled), 5, 0)));
         edtContratante_ContaCorrente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_ContaCorrente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_ContaCorrente_Enabled), 5, 0)));
         edtContratante_InicioDoExpediente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_InicioDoExpediente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_InicioDoExpediente_Enabled), 5, 0)));
         edtContratante_FimDoExpediente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_FimDoExpediente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_FimDoExpediente_Enabled), 5, 0)));
         edtContratante_LogoArquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoArquivo_Enabled), 5, 0)));
         cmbContratante_UsaOSistema.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_UsaOSistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_UsaOSistema.Enabled), 5, 0)));
         edtavRedmine_user_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_user_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_user_Enabled), 5, 0)));
         edtavRedmine_key_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_key_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_key_Enabled), 5, 0)));
         cmbavRedmine_secure.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRedmine_secure_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRedmine_secure.Enabled), 5, 0)));
         edtavRedmine_host_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_host_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_host_Enabled), 5, 0)));
         edtavRedmine_url_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_url_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_url_Enabled), 5, 0)));
         cmbavRedmine_versao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRedmine_versao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRedmine_versao.Enabled), 5, 0)));
         edtavRedmine_camposistema_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_camposistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_camposistema_Enabled), 5, 0)));
         edtavRedmine_camposervico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRedmine_camposervico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRedmine_camposervico_Enabled), 5, 0)));
         edtContratante_EmailSdaHost_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaHost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_EmailSdaHost_Enabled), 5, 0)));
         edtContratante_EmailSdaUser_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaUser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_EmailSdaUser_Enabled), 5, 0)));
         edtContratante_EmailSdaPass_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaPass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_EmailSdaPass_Enabled), 5, 0)));
         edtContratante_EmailSdaPort_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_EmailSdaPort_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_EmailSdaPort_Enabled), 5, 0)));
         cmbContratante_EmailSdaAut.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_EmailSdaAut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_EmailSdaAut.Enabled), 5, 0)));
         cmbContratante_EmailSdaSec.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_EmailSdaSec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_EmailSdaSec.Enabled), 5, 0)));
         edtavEmailto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmailto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmailto_Enabled), 5, 0)));
         dynavEstado_uf.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf.Enabled), 5, 0)));
         dynMunicipio_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynMunicipio_Codigo.Enabled), 5, 0)));
         chkContratante_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratante_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratante_Ativo.Enabled), 5, 0)));
         cmbContratante_SSAutomatica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SSAutomatica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_SSAutomatica.Enabled), 5, 0)));
         cmbContratante_SSPrestadoraUnica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SSPrestadoraUnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_SSPrestadoraUnica.Enabled), 5, 0)));
         cmbContratante_SSStatusPlanejamento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SSStatusPlanejamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_SSStatusPlanejamento.Enabled), 5, 0)));
         dynContratante_ServicoSSPadrao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratante_ServicoSSPadrao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratante_ServicoSSPadrao.Enabled), 5, 0)));
         cmbContratante_RetornaSS.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_RetornaSS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_RetornaSS.Enabled), 5, 0)));
         cmbContratante_PropostaRqrSrv.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrSrv_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_PropostaRqrSrv.Enabled), 5, 0)));
         cmbContratante_PropostaRqrPrz.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrPrz_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_PropostaRqrPrz.Enabled), 5, 0)));
         cmbContratante_PropostaRqrEsf.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrEsf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_PropostaRqrEsf.Enabled), 5, 0)));
         cmbContratante_PropostaRqrCnt.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaRqrCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_PropostaRqrCnt.Enabled), 5, 0)));
         cmbContratante_PropostaNvlCnt.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_PropostaNvlCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_PropostaNvlCnt.Enabled), 5, 0)));
         cmbContratante_OSAutomatica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSAutomatica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSAutomatica.Enabled), 5, 0)));
         cmbContratante_OSGeraOS.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraOS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSGeraOS.Enabled), 5, 0)));
         cmbContratante_OSGeraTRP.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTRP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSGeraTRP.Enabled), 5, 0)));
         cmbContratante_OSGeraTRD.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTRD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSGeraTRD.Enabled), 5, 0)));
         cmbContratante_OSGeraTH.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTH_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSGeraTH.Enabled), 5, 0)));
         cmbContratante_OSGeraTR.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSGeraTR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSGeraTR.Enabled), 5, 0)));
         cmbContratante_OSHmlgComPnd.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_OSHmlgComPnd_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_OSHmlgComPnd.Enabled), 5, 0)));
         cmbContratante_SelecionaResponsavelOS.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_SelecionaResponsavelOS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_SelecionaResponsavelOS.Enabled), 5, 0)));
         cmbContratante_ExibePF.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratante_ExibePF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratante_ExibePF.Enabled), 5, 0)));
         edtContratante_AtivoCirculante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_AtivoCirculante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_AtivoCirculante_Enabled), 5, 0)));
         edtContratante_PassivoCirculante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_PassivoCirculante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_PassivoCirculante_Enabled), 5, 0)));
         edtContratante_PatrimonioLiquido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_PatrimonioLiquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_PatrimonioLiquido_Enabled), 5, 0)));
         edtContratante_ReceitaBruta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_ReceitaBruta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_ReceitaBruta_Enabled), 5, 0)));
         edtContratante_TtlRltGerencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_TtlRltGerencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_TtlRltGerencial_Enabled), 5, 0)));
         edtContratante_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_Codigo_Enabled), 5, 0)));
         edtContratante_LogoNomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoNomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoNomeArq_Enabled), 5, 0)));
         edtContratante_LogoTipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoTipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratante_LogoTipoArq_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues050( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311715843");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contratante_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z29Contratante_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z550Contratante_EmailSdaKey", StringUtil.RTrim( Z550Contratante_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, "Z549Contratante_EmailSdaPass", Z549Contratante_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "Z10Contratante_NomeFantasia", StringUtil.RTrim( Z10Contratante_NomeFantasia));
         GxWebStd.gx_hidden_field( context, "Z11Contratante_IE", StringUtil.RTrim( Z11Contratante_IE));
         GxWebStd.gx_hidden_field( context, "Z13Contratante_WebSite", Z13Contratante_WebSite);
         GxWebStd.gx_hidden_field( context, "Z14Contratante_Email", Z14Contratante_Email);
         GxWebStd.gx_hidden_field( context, "Z31Contratante_Telefone", StringUtil.RTrim( Z31Contratante_Telefone));
         GxWebStd.gx_hidden_field( context, "Z32Contratante_Ramal", StringUtil.RTrim( Z32Contratante_Ramal));
         GxWebStd.gx_hidden_field( context, "Z33Contratante_Fax", StringUtil.RTrim( Z33Contratante_Fax));
         GxWebStd.gx_hidden_field( context, "Z336Contratante_AgenciaNome", StringUtil.RTrim( Z336Contratante_AgenciaNome));
         GxWebStd.gx_hidden_field( context, "Z337Contratante_AgenciaNro", StringUtil.RTrim( Z337Contratante_AgenciaNro));
         GxWebStd.gx_hidden_field( context, "Z338Contratante_BancoNome", StringUtil.RTrim( Z338Contratante_BancoNome));
         GxWebStd.gx_hidden_field( context, "Z339Contratante_BancoNro", StringUtil.RTrim( Z339Contratante_BancoNro));
         GxWebStd.gx_hidden_field( context, "Z16Contratante_ContaCorrente", StringUtil.RTrim( Z16Contratante_ContaCorrente));
         GxWebStd.gx_boolean_hidden_field( context, "Z30Contratante_Ativo", Z30Contratante_Ativo);
         GxWebStd.gx_hidden_field( context, "Z547Contratante_EmailSdaHost", Z547Contratante_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, "Z548Contratante_EmailSdaUser", Z548Contratante_EmailSdaUser);
         GxWebStd.gx_boolean_hidden_field( context, "Z551Contratante_EmailSdaAut", Z551Contratante_EmailSdaAut);
         GxWebStd.gx_hidden_field( context, "Z552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z552Contratante_EmailSdaPort), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1048Contratante_EmailSdaSec), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z593Contratante_OSAutomatica", Z593Contratante_OSAutomatica);
         GxWebStd.gx_boolean_hidden_field( context, "Z1652Contratante_SSAutomatica", Z1652Contratante_SSAutomatica);
         GxWebStd.gx_boolean_hidden_field( context, "Z594Contratante_ExisteConferencia", Z594Contratante_ExisteConferencia);
         GxWebStd.gx_hidden_field( context, "Z1448Contratante_InicioDoExpediente", context.localUtil.TToC( Z1448Contratante_InicioDoExpediente, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1192Contratante_FimDoExpediente", context.localUtil.TToC( Z1192Contratante_FimDoExpediente, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1727Contratante_ServicoSSPadrao), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1729Contratante_RetornaSS", Z1729Contratante_RetornaSS);
         GxWebStd.gx_hidden_field( context, "Z1732Contratante_SSStatusPlanejamento", StringUtil.RTrim( Z1732Contratante_SSStatusPlanejamento));
         GxWebStd.gx_boolean_hidden_field( context, "Z1733Contratante_SSPrestadoraUnica", Z1733Contratante_SSPrestadoraUnica);
         GxWebStd.gx_boolean_hidden_field( context, "Z1738Contratante_PropostaRqrSrv", Z1738Contratante_PropostaRqrSrv);
         GxWebStd.gx_boolean_hidden_field( context, "Z1739Contratante_PropostaRqrPrz", Z1739Contratante_PropostaRqrPrz);
         GxWebStd.gx_boolean_hidden_field( context, "Z1740Contratante_PropostaRqrEsf", Z1740Contratante_PropostaRqrEsf);
         GxWebStd.gx_boolean_hidden_field( context, "Z1741Contratante_PropostaRqrCnt", Z1741Contratante_PropostaRqrCnt);
         GxWebStd.gx_hidden_field( context, "Z1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1742Contratante_PropostaNvlCnt), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1757Contratante_OSGeraOS", Z1757Contratante_OSGeraOS);
         GxWebStd.gx_boolean_hidden_field( context, "Z1758Contratante_OSGeraTRP", Z1758Contratante_OSGeraTRP);
         GxWebStd.gx_boolean_hidden_field( context, "Z1759Contratante_OSGeraTH", Z1759Contratante_OSGeraTH);
         GxWebStd.gx_boolean_hidden_field( context, "Z1760Contratante_OSGeraTRD", Z1760Contratante_OSGeraTRD);
         GxWebStd.gx_boolean_hidden_field( context, "Z1761Contratante_OSGeraTR", Z1761Contratante_OSGeraTR);
         GxWebStd.gx_boolean_hidden_field( context, "Z1803Contratante_OSHmlgComPnd", Z1803Contratante_OSHmlgComPnd);
         GxWebStd.gx_hidden_field( context, "Z1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.NToC( Z1805Contratante_AtivoCirculante, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.NToC( Z1806Contratante_PassivoCirculante, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.NToC( Z1807Contratante_PatrimonioLiquido, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.NToC( Z1808Contratante_ReceitaBruta, 18, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1822Contratante_UsaOSistema", Z1822Contratante_UsaOSistema);
         GxWebStd.gx_hidden_field( context, "Z2034Contratante_TtlRltGerencial", Z2034Contratante_TtlRltGerencial);
         GxWebStd.gx_hidden_field( context, "Z2084Contratante_PrzAoRtr", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2084Contratante_PrzAoRtr), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2083Contratante_PrzActRtr", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2083Contratante_PrzActRtr), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2085Contratante_RequerOrigem", Z2085Contratante_RequerOrigem);
         GxWebStd.gx_boolean_hidden_field( context, "Z2089Contratante_SelecionaResponsavelOS", Z2089Contratante_SelecionaResponsavelOS);
         GxWebStd.gx_boolean_hidden_field( context, "Z2090Contratante_ExibePF", Z2090Contratante_ExibePF);
         GxWebStd.gx_hidden_field( context, "Z25Municipio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z335Contratante_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O549Contratante_EmailSdaPass", O549Contratante_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N25Municipio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATTACHMENTS", AV35Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATTACHMENTS", AV35Attachments);
         }
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV36Resultado));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATANTE_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Insert_Contratante_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Insert_Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAKEY", StringUtil.RTrim( A550Contratante_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATANTE_REQUERORIGEM", A2085Contratante_RequerOrigem);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV29AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV29AuditingObject);
         }
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATANTE_EXISTECONFERENCIA", A594Contratante_ExisteConferencia);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_PRZAORTR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2084Contratante_PrzAoRtr), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_PRZACTRTR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2083Contratante_PrzActRtr), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_NOME", StringUtil.RTrim( A26Municipio_Nome));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV45Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Contratante_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "CONTRATANTE_LOGOARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A1124Contratante_LogoArquivo);
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Width", StringUtil.RTrim( Gxuitabspanel_tabs_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Cls", StringUtil.RTrim( Gxuitabspanel_tabs_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Enabled", StringUtil.BoolToStr( Gxuitabspanel_tabs_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabs_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABS_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabs_Designtimetabs));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_LOGOARQUIVO_Filetype", StringUtil.RTrim( edtContratante_LogoArquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_LOGOARQUIVO_Filename", StringUtil.RTrim( edtContratante_LogoArquivo_Filename));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_LOGOARQUIVO_Filename", StringUtil.RTrim( edtContratante_LogoArquivo_Filename));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_LOGOARQUIVO_Filetype", StringUtil.RTrim( edtContratante_LogoArquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Contratante";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV45Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A594Contratante_ExisteConferencia);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2084Contratante_PrzAoRtr), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2083Contratante_PrzActRtr), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2085Contratante_RequerOrigem);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Contratante_Codigo:"+context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Contratante_PessoaCod:"+context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV45Pgmname, "")));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Contratante_ExisteConferencia:"+StringUtil.BoolToStr( A594Contratante_ExisteConferencia));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Contratante_PrzAoRtr:"+context.localUtil.Format( (decimal)(A2084Contratante_PrzAoRtr), "ZZZ9"));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Contratante_PrzActRtr:"+context.localUtil.Format( (decimal)(A2083Contratante_PrzActRtr), "ZZZ9"));
         GXUtil.WriteLog("contratante:[SendSecurityCheck value for]"+"Contratante_RequerOrigem:"+StringUtil.BoolToStr( A2085Contratante_RequerOrigem));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Contratante_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Contratante" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratante" ;
      }

      protected void InitializeNonKey05124( )
      {
         A335Contratante_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A335Contratante_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A335Contratante_PessoaCod), 6, 0)));
         A25Municipio_Codigo = 0;
         n25Municipio_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         n25Municipio_Codigo = ((0==A25Municipio_Codigo) ? true : false);
         AV15Estado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Estado_UF", AV15Estado_UF);
         AV44EmailTo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44EmailTo", AV44EmailTo);
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A550Contratante_EmailSdaKey = "";
         n550Contratante_EmailSdaKey = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A550Contratante_EmailSdaKey", A550Contratante_EmailSdaKey);
         A549Contratante_EmailSdaPass = "";
         n549Contratante_EmailSdaPass = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
         n549Contratante_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) ? true : false);
         A12Contratante_CNPJ = "";
         n12Contratante_CNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Contratante_CNPJ", A12Contratante_CNPJ);
         A9Contratante_RazaoSocial = "";
         n9Contratante_RazaoSocial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
         A10Contratante_NomeFantasia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A10Contratante_NomeFantasia", A10Contratante_NomeFantasia);
         A11Contratante_IE = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Contratante_IE", A11Contratante_IE);
         A13Contratante_WebSite = "";
         n13Contratante_WebSite = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Contratante_WebSite", A13Contratante_WebSite);
         n13Contratante_WebSite = (String.IsNullOrEmpty(StringUtil.RTrim( A13Contratante_WebSite)) ? true : false);
         A14Contratante_Email = "";
         n14Contratante_Email = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Contratante_Email", A14Contratante_Email);
         n14Contratante_Email = (String.IsNullOrEmpty(StringUtil.RTrim( A14Contratante_Email)) ? true : false);
         A31Contratante_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31Contratante_Telefone", A31Contratante_Telefone);
         A32Contratante_Ramal = "";
         n32Contratante_Ramal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32Contratante_Ramal", A32Contratante_Ramal);
         n32Contratante_Ramal = (String.IsNullOrEmpty(StringUtil.RTrim( A32Contratante_Ramal)) ? true : false);
         A33Contratante_Fax = "";
         n33Contratante_Fax = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33Contratante_Fax", A33Contratante_Fax);
         n33Contratante_Fax = (String.IsNullOrEmpty(StringUtil.RTrim( A33Contratante_Fax)) ? true : false);
         A336Contratante_AgenciaNome = "";
         n336Contratante_AgenciaNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A336Contratante_AgenciaNome", A336Contratante_AgenciaNome);
         n336Contratante_AgenciaNome = (String.IsNullOrEmpty(StringUtil.RTrim( A336Contratante_AgenciaNome)) ? true : false);
         A337Contratante_AgenciaNro = "";
         n337Contratante_AgenciaNro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A337Contratante_AgenciaNro", A337Contratante_AgenciaNro);
         n337Contratante_AgenciaNro = (String.IsNullOrEmpty(StringUtil.RTrim( A337Contratante_AgenciaNro)) ? true : false);
         A338Contratante_BancoNome = "";
         n338Contratante_BancoNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A338Contratante_BancoNome", A338Contratante_BancoNome);
         n338Contratante_BancoNome = (String.IsNullOrEmpty(StringUtil.RTrim( A338Contratante_BancoNome)) ? true : false);
         A339Contratante_BancoNro = "";
         n339Contratante_BancoNro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A339Contratante_BancoNro", A339Contratante_BancoNro);
         n339Contratante_BancoNro = (String.IsNullOrEmpty(StringUtil.RTrim( A339Contratante_BancoNro)) ? true : false);
         A16Contratante_ContaCorrente = "";
         n16Contratante_ContaCorrente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16Contratante_ContaCorrente", A16Contratante_ContaCorrente);
         n16Contratante_ContaCorrente = (String.IsNullOrEmpty(StringUtil.RTrim( A16Contratante_ContaCorrente)) ? true : false);
         A26Municipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
         A23Estado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
         A547Contratante_EmailSdaHost = "";
         n547Contratante_EmailSdaHost = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A547Contratante_EmailSdaHost", A547Contratante_EmailSdaHost);
         n547Contratante_EmailSdaHost = (String.IsNullOrEmpty(StringUtil.RTrim( A547Contratante_EmailSdaHost)) ? true : false);
         A548Contratante_EmailSdaUser = "";
         n548Contratante_EmailSdaUser = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A548Contratante_EmailSdaUser", A548Contratante_EmailSdaUser);
         n548Contratante_EmailSdaUser = (String.IsNullOrEmpty(StringUtil.RTrim( A548Contratante_EmailSdaUser)) ? true : false);
         A552Contratante_EmailSdaPort = 0;
         n552Contratante_EmailSdaPort = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A552Contratante_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0)));
         n552Contratante_EmailSdaPort = ((0==A552Contratante_EmailSdaPort) ? true : false);
         A1048Contratante_EmailSdaSec = 0;
         n1048Contratante_EmailSdaSec = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1048Contratante_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)));
         n1048Contratante_EmailSdaSec = ((0==A1048Contratante_EmailSdaSec) ? true : false);
         A593Contratante_OSAutomatica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A593Contratante_OSAutomatica", A593Contratante_OSAutomatica);
         A1652Contratante_SSAutomatica = false;
         n1652Contratante_SSAutomatica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1652Contratante_SSAutomatica", A1652Contratante_SSAutomatica);
         n1652Contratante_SSAutomatica = ((false==A1652Contratante_SSAutomatica) ? true : false);
         A594Contratante_ExisteConferencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A594Contratante_ExisteConferencia", A594Contratante_ExisteConferencia);
         A1124Contratante_LogoArquivo = "";
         n1124Contratante_LogoArquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1124Contratante_LogoArquivo", A1124Contratante_LogoArquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1124Contratante_LogoArquivo));
         n1124Contratante_LogoArquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1124Contratante_LogoArquivo)) ? true : false);
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         n1448Contratante_InicioDoExpediente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1448Contratante_InicioDoExpediente", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         n1448Contratante_InicioDoExpediente = ((DateTime.MinValue==A1448Contratante_InicioDoExpediente) ? true : false);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         n1192Contratante_FimDoExpediente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1192Contratante_FimDoExpediente", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         n1192Contratante_FimDoExpediente = ((DateTime.MinValue==A1192Contratante_FimDoExpediente) ? true : false);
         A1727Contratante_ServicoSSPadrao = 0;
         n1727Contratante_ServicoSSPadrao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1727Contratante_ServicoSSPadrao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1727Contratante_ServicoSSPadrao), 6, 0)));
         n1727Contratante_ServicoSSPadrao = ((0==A1727Contratante_ServicoSSPadrao) ? true : false);
         A1729Contratante_RetornaSS = false;
         n1729Contratante_RetornaSS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1729Contratante_RetornaSS", A1729Contratante_RetornaSS);
         n1729Contratante_RetornaSS = ((false==A1729Contratante_RetornaSS) ? true : false);
         A1733Contratante_SSPrestadoraUnica = false;
         n1733Contratante_SSPrestadoraUnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1733Contratante_SSPrestadoraUnica", A1733Contratante_SSPrestadoraUnica);
         n1733Contratante_SSPrestadoraUnica = ((false==A1733Contratante_SSPrestadoraUnica) ? true : false);
         A1739Contratante_PropostaRqrPrz = false;
         n1739Contratante_PropostaRqrPrz = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1739Contratante_PropostaRqrPrz", A1739Contratante_PropostaRqrPrz);
         n1739Contratante_PropostaRqrPrz = ((false==A1739Contratante_PropostaRqrPrz) ? true : false);
         A1740Contratante_PropostaRqrEsf = false;
         n1740Contratante_PropostaRqrEsf = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1740Contratante_PropostaRqrEsf", A1740Contratante_PropostaRqrEsf);
         n1740Contratante_PropostaRqrEsf = ((false==A1740Contratante_PropostaRqrEsf) ? true : false);
         A1741Contratante_PropostaRqrCnt = false;
         n1741Contratante_PropostaRqrCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1741Contratante_PropostaRqrCnt", A1741Contratante_PropostaRqrCnt);
         n1741Contratante_PropostaRqrCnt = ((false==A1741Contratante_PropostaRqrCnt) ? true : false);
         A1757Contratante_OSGeraOS = false;
         n1757Contratante_OSGeraOS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1757Contratante_OSGeraOS", A1757Contratante_OSGeraOS);
         n1757Contratante_OSGeraOS = ((false==A1757Contratante_OSGeraOS) ? true : false);
         A1758Contratante_OSGeraTRP = false;
         n1758Contratante_OSGeraTRP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1758Contratante_OSGeraTRP", A1758Contratante_OSGeraTRP);
         n1758Contratante_OSGeraTRP = ((false==A1758Contratante_OSGeraTRP) ? true : false);
         A1759Contratante_OSGeraTH = false;
         n1759Contratante_OSGeraTH = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1759Contratante_OSGeraTH", A1759Contratante_OSGeraTH);
         n1759Contratante_OSGeraTH = ((false==A1759Contratante_OSGeraTH) ? true : false);
         A1760Contratante_OSGeraTRD = false;
         n1760Contratante_OSGeraTRD = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1760Contratante_OSGeraTRD", A1760Contratante_OSGeraTRD);
         n1760Contratante_OSGeraTRD = ((false==A1760Contratante_OSGeraTRD) ? true : false);
         A1761Contratante_OSGeraTR = false;
         n1761Contratante_OSGeraTR = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1761Contratante_OSGeraTR", A1761Contratante_OSGeraTR);
         n1761Contratante_OSGeraTR = ((false==A1761Contratante_OSGeraTR) ? true : false);
         A1803Contratante_OSHmlgComPnd = false;
         n1803Contratante_OSHmlgComPnd = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1803Contratante_OSHmlgComPnd", A1803Contratante_OSHmlgComPnd);
         n1803Contratante_OSHmlgComPnd = ((false==A1803Contratante_OSHmlgComPnd) ? true : false);
         A1805Contratante_AtivoCirculante = 0;
         n1805Contratante_AtivoCirculante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1805Contratante_AtivoCirculante", StringUtil.LTrim( StringUtil.Str( A1805Contratante_AtivoCirculante, 18, 5)));
         n1805Contratante_AtivoCirculante = ((Convert.ToDecimal(0)==A1805Contratante_AtivoCirculante) ? true : false);
         A1806Contratante_PassivoCirculante = 0;
         n1806Contratante_PassivoCirculante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1806Contratante_PassivoCirculante", StringUtil.LTrim( StringUtil.Str( A1806Contratante_PassivoCirculante, 18, 5)));
         n1806Contratante_PassivoCirculante = ((Convert.ToDecimal(0)==A1806Contratante_PassivoCirculante) ? true : false);
         A1807Contratante_PatrimonioLiquido = 0;
         n1807Contratante_PatrimonioLiquido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1807Contratante_PatrimonioLiquido", StringUtil.LTrim( StringUtil.Str( A1807Contratante_PatrimonioLiquido, 18, 5)));
         n1807Contratante_PatrimonioLiquido = ((Convert.ToDecimal(0)==A1807Contratante_PatrimonioLiquido) ? true : false);
         A1808Contratante_ReceitaBruta = 0;
         n1808Contratante_ReceitaBruta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1808Contratante_ReceitaBruta", StringUtil.LTrim( StringUtil.Str( A1808Contratante_ReceitaBruta, 18, 5)));
         n1808Contratante_ReceitaBruta = ((Convert.ToDecimal(0)==A1808Contratante_ReceitaBruta) ? true : false);
         A2084Contratante_PrzAoRtr = 0;
         n2084Contratante_PrzAoRtr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2084Contratante_PrzAoRtr", StringUtil.LTrim( StringUtil.Str( (decimal)(A2084Contratante_PrzAoRtr), 4, 0)));
         A2083Contratante_PrzActRtr = 0;
         n2083Contratante_PrzActRtr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2083Contratante_PrzActRtr", StringUtil.LTrim( StringUtil.Str( (decimal)(A2083Contratante_PrzActRtr), 4, 0)));
         A2089Contratante_SelecionaResponsavelOS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2089Contratante_SelecionaResponsavelOS", A2089Contratante_SelecionaResponsavelOS);
         A2090Contratante_ExibePF = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2090Contratante_ExibePF", A2090Contratante_ExibePF);
         A1126Contratante_LogoTipoArq = "";
         n1126Contratante_LogoTipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1126Contratante_LogoTipoArq", A1126Contratante_LogoTipoArq);
         n1126Contratante_LogoTipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1126Contratante_LogoTipoArq)) ? true : false);
         A1125Contratante_LogoNomeArq = "";
         n1125Contratante_LogoNomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1125Contratante_LogoNomeArq", A1125Contratante_LogoNomeArq);
         n1125Contratante_LogoNomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1125Contratante_LogoNomeArq)) ? true : false);
         A30Contratante_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30Contratante_Ativo", A30Contratante_Ativo);
         A551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
         A1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
         A1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
         A1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
         A1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
         A2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
         A2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2085Contratante_RequerOrigem", A2085Contratante_RequerOrigem);
         O549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
         n549Contratante_EmailSdaPass = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A549Contratante_EmailSdaPass", A549Contratante_EmailSdaPass);
         Z550Contratante_EmailSdaKey = "";
         Z549Contratante_EmailSdaPass = "";
         Z10Contratante_NomeFantasia = "";
         Z11Contratante_IE = "";
         Z13Contratante_WebSite = "";
         Z14Contratante_Email = "";
         Z31Contratante_Telefone = "";
         Z32Contratante_Ramal = "";
         Z33Contratante_Fax = "";
         Z336Contratante_AgenciaNome = "";
         Z337Contratante_AgenciaNro = "";
         Z338Contratante_BancoNome = "";
         Z339Contratante_BancoNro = "";
         Z16Contratante_ContaCorrente = "";
         Z30Contratante_Ativo = false;
         Z547Contratante_EmailSdaHost = "";
         Z548Contratante_EmailSdaUser = "";
         Z551Contratante_EmailSdaAut = false;
         Z552Contratante_EmailSdaPort = 0;
         Z1048Contratante_EmailSdaSec = 0;
         Z593Contratante_OSAutomatica = false;
         Z1652Contratante_SSAutomatica = false;
         Z594Contratante_ExisteConferencia = false;
         Z1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         Z1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         Z1727Contratante_ServicoSSPadrao = 0;
         Z1729Contratante_RetornaSS = false;
         Z1732Contratante_SSStatusPlanejamento = "";
         Z1733Contratante_SSPrestadoraUnica = false;
         Z1738Contratante_PropostaRqrSrv = false;
         Z1739Contratante_PropostaRqrPrz = false;
         Z1740Contratante_PropostaRqrEsf = false;
         Z1741Contratante_PropostaRqrCnt = false;
         Z1742Contratante_PropostaNvlCnt = 0;
         Z1757Contratante_OSGeraOS = false;
         Z1758Contratante_OSGeraTRP = false;
         Z1759Contratante_OSGeraTH = false;
         Z1760Contratante_OSGeraTRD = false;
         Z1761Contratante_OSGeraTR = false;
         Z1803Contratante_OSHmlgComPnd = false;
         Z1805Contratante_AtivoCirculante = 0;
         Z1806Contratante_PassivoCirculante = 0;
         Z1807Contratante_PatrimonioLiquido = 0;
         Z1808Contratante_ReceitaBruta = 0;
         Z1822Contratante_UsaOSistema = false;
         Z2034Contratante_TtlRltGerencial = "";
         Z2084Contratante_PrzAoRtr = 0;
         Z2083Contratante_PrzActRtr = 0;
         Z2085Contratante_RequerOrigem = false;
         Z2089Contratante_SelecionaResponsavelOS = false;
         Z2090Contratante_ExibePF = false;
         Z25Municipio_Codigo = 0;
         Z335Contratante_PessoaCod = 0;
      }

      protected void InitAll05124( )
      {
         A29Contratante_Codigo = 0;
         n29Contratante_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         InitializeNonKey05124( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2085Contratante_RequerOrigem = i2085Contratante_RequerOrigem;
         n2085Contratante_RequerOrigem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2085Contratante_RequerOrigem", A2085Contratante_RequerOrigem);
         A2034Contratante_TtlRltGerencial = i2034Contratante_TtlRltGerencial;
         n2034Contratante_TtlRltGerencial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2034Contratante_TtlRltGerencial", A2034Contratante_TtlRltGerencial);
         A1822Contratante_UsaOSistema = i1822Contratante_UsaOSistema;
         n1822Contratante_UsaOSistema = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1822Contratante_UsaOSistema", A1822Contratante_UsaOSistema);
         A1742Contratante_PropostaNvlCnt = i1742Contratante_PropostaNvlCnt;
         n1742Contratante_PropostaNvlCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1742Contratante_PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A1742Contratante_PropostaNvlCnt), 4, 0)));
         A1738Contratante_PropostaRqrSrv = i1738Contratante_PropostaRqrSrv;
         n1738Contratante_PropostaRqrSrv = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1738Contratante_PropostaRqrSrv", A1738Contratante_PropostaRqrSrv);
         A1732Contratante_SSStatusPlanejamento = i1732Contratante_SSStatusPlanejamento;
         n1732Contratante_SSStatusPlanejamento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1732Contratante_SSStatusPlanejamento", A1732Contratante_SSStatusPlanejamento);
         A551Contratante_EmailSdaAut = i551Contratante_EmailSdaAut;
         n551Contratante_EmailSdaAut = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A551Contratante_EmailSdaAut", A551Contratante_EmailSdaAut);
         A30Contratante_Ativo = i30Contratante_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30Contratante_Ativo", A30Contratante_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117151057");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratante.js", "?20203117151058");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratante_razaosocial_Internalname = "TEXTBLOCKCONTRATANTE_RAZAOSOCIAL";
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL";
         lblTextblockcontratante_nomefantasia_Internalname = "TEXTBLOCKCONTRATANTE_NOMEFANTASIA";
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA";
         lblTextblockcontratante_cnpj_Internalname = "TEXTBLOCKCONTRATANTE_CNPJ";
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ";
         lblTextblockcontratante_ie_Internalname = "TEXTBLOCKCONTRATANTE_IE";
         edtContratante_IE_Internalname = "CONTRATANTE_IE";
         lblTextblockcontratante_telefone_Internalname = "TEXTBLOCKCONTRATANTE_TELEFONE";
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE";
         lblTextblockcontratante_ramal_Internalname = "TEXTBLOCKCONTRATANTE_RAMAL";
         edtContratante_Ramal_Internalname = "CONTRATANTE_RAMAL";
         lblTextblockcontratante_fax_Internalname = "TEXTBLOCKCONTRATANTE_FAX";
         edtContratante_Fax_Internalname = "CONTRATANTE_FAX";
         lblTextblockcontratante_website_Internalname = "TEXTBLOCKCONTRATANTE_WEBSITE";
         edtContratante_WebSite_Internalname = "CONTRATANTE_WEBSITE";
         lblTextblockcontratante_email_Internalname = "TEXTBLOCKCONTRATANTE_EMAIL";
         edtContratante_Email_Internalname = "CONTRATANTE_EMAIL";
         lblTextblockcontratante_banconome_Internalname = "TEXTBLOCKCONTRATANTE_BANCONOME";
         edtContratante_BancoNome_Internalname = "CONTRATANTE_BANCONOME";
         lblTextblockcontratante_banconro_Internalname = "TEXTBLOCKCONTRATANTE_BANCONRO";
         edtContratante_BancoNro_Internalname = "CONTRATANTE_BANCONRO";
         lblTextblockcontratante_agencianome_Internalname = "TEXTBLOCKCONTRATANTE_AGENCIANOME";
         edtContratante_AgenciaNome_Internalname = "CONTRATANTE_AGENCIANOME";
         lblTextblockcontratante_agencianro_Internalname = "TEXTBLOCKCONTRATANTE_AGENCIANRO";
         edtContratante_AgenciaNro_Internalname = "CONTRATANTE_AGENCIANRO";
         lblTextblockcontratante_contacorrente_Internalname = "TEXTBLOCKCONTRATANTE_CONTACORRENTE";
         edtContratante_ContaCorrente_Internalname = "CONTRATANTE_CONTACORRENTE";
         lblTextblockcontratante_iniciodoexpediente_Internalname = "TEXTBLOCKCONTRATANTE_INICIODOEXPEDIENTE";
         edtContratante_InicioDoExpediente_Internalname = "CONTRATANTE_INICIODOEXPEDIENTE";
         lblTextblockcontratante_fimdoexpediente_Internalname = "TEXTBLOCKCONTRATANTE_FIMDOEXPEDIENTE";
         edtContratante_FimDoExpediente_Internalname = "CONTRATANTE_FIMDOEXPEDIENTE";
         lblContratante_fimdoexpediente_righttext_Internalname = "CONTRATANTE_FIMDOEXPEDIENTE_RIGHTTEXT";
         cellContratante_fimdoexpediente_righttext_cell_Internalname = "CONTRATANTE_FIMDOEXPEDIENTE_RIGHTTEXT_CELL";
         tblTablemergedcontratante_iniciodoexpediente_Internalname = "TABLEMERGEDCONTRATANTE_INICIODOEXPEDIENTE";
         lblTextblockcontratante_logoarquivo_Internalname = "TEXTBLOCKCONTRATANTE_LOGOARQUIVO";
         edtContratante_LogoArquivo_Internalname = "CONTRATANTE_LOGOARQUIVO";
         lblTextblockcontratante_usaosistema_Internalname = "TEXTBLOCKCONTRATANTE_USAOSISTEMA";
         cmbContratante_UsaOSistema_Internalname = "CONTRATANTE_USAOSISTEMA";
         lblTextblockredmine_user_Internalname = "TEXTBLOCKREDMINE_USER";
         edtavRedmine_user_Internalname = "vREDMINE_USER";
         lblTextblockredmine_key_Internalname = "TEXTBLOCKREDMINE_KEY";
         edtavRedmine_key_Internalname = "vREDMINE_KEY";
         lblTextblockredmine_secure_Internalname = "TEXTBLOCKREDMINE_SECURE";
         cmbavRedmine_secure_Internalname = "vREDMINE_SECURE";
         edtavRedmine_host_Internalname = "vREDMINE_HOST";
         tblTablemergedredmine_secure_Internalname = "TABLEMERGEDREDMINE_SECURE";
         lblTextblockredmine_url_Internalname = "TEXTBLOCKREDMINE_URL";
         edtavRedmine_url_Internalname = "vREDMINE_URL";
         lblTextblockredmine_versao_Internalname = "TEXTBLOCKREDMINE_VERSAO";
         cmbavRedmine_versao_Internalname = "vREDMINE_VERSAO";
         lblTextblockredmine_camposistema_Internalname = "TEXTBLOCKREDMINE_CAMPOSISTEMA";
         edtavRedmine_camposistema_Internalname = "vREDMINE_CAMPOSISTEMA";
         lblTextblockredmine_camposervico_Internalname = "TEXTBLOCKREDMINE_CAMPOSERVICO";
         edtavRedmine_camposervico_Internalname = "vREDMINE_CAMPOSERVICO";
         tblRedmine_Internalname = "REDMINE";
         grpUnnamedgroup10_Internalname = "UNNAMEDGROUP10";
         lblTextblockcontratante_emailsdahost_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAHOST";
         edtContratante_EmailSdaHost_Internalname = "CONTRATANTE_EMAILSDAHOST";
         lblTextblockcontratante_emailsdauser_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAUSER";
         edtContratante_EmailSdaUser_Internalname = "CONTRATANTE_EMAILSDAUSER";
         lblTextblockcontratante_emailsdapass_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAPASS";
         edtContratante_EmailSdaPass_Internalname = "CONTRATANTE_EMAILSDAPASS";
         lblTextblockcontratante_emailsdaport_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAPORT";
         edtContratante_EmailSdaPort_Internalname = "CONTRATANTE_EMAILSDAPORT";
         lblTextblockcontratante_emailsdaaut_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDAAUT";
         cmbContratante_EmailSdaAut_Internalname = "CONTRATANTE_EMAILSDAAUT";
         lblTextblockcontratante_emailsdasec_Internalname = "TEXTBLOCKCONTRATANTE_EMAILSDASEC";
         cmbContratante_EmailSdaSec_Internalname = "CONTRATANTE_EMAILSDASEC";
         bttBtntestaremail_Internalname = "BTNTESTAREMAIL";
         bttBtntestecsharp_Internalname = "BTNTESTECSHARP";
         lblTextblockemailto_Internalname = "TEXTBLOCKEMAILTO";
         edtavEmailto_Internalname = "vEMAILTO";
         tblTablemergedtestecsharp_Internalname = "TABLEMERGEDTESTECSHARP";
         tblEmailsaida_Internalname = "EMAILSAIDA";
         grpUnnamedgroup11_Internalname = "UNNAMEDGROUP11";
         lblTextblockestado_uf_Internalname = "TEXTBLOCKESTADO_UF";
         dynavEstado_uf_Internalname = "vESTADO_UF";
         lblTextblockmunicipio_codigo_Internalname = "TEXTBLOCKMUNICIPIO_CODIGO";
         dynMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         lblTextblockcontratante_ativo_Internalname = "TEXTBLOCKCONTRATANTE_ATIVO";
         chkContratante_Ativo_Internalname = "CONTRATANTE_ATIVO";
         tblUnnamedtable9_Internalname = "UNNAMEDTABLE9";
         lblTextblockcontratante_ssautomatica_Internalname = "TEXTBLOCKCONTRATANTE_SSAUTOMATICA";
         cmbContratante_SSAutomatica_Internalname = "CONTRATANTE_SSAUTOMATICA";
         lblTextblockcontratante_ssprestadoraunica_Internalname = "TEXTBLOCKCONTRATANTE_SSPRESTADORAUNICA";
         cmbContratante_SSPrestadoraUnica_Internalname = "CONTRATANTE_SSPRESTADORAUNICA";
         lblTextblockcontratante_ssstatusplanejamento_Internalname = "TEXTBLOCKCONTRATANTE_SSSTATUSPLANEJAMENTO";
         cmbContratante_SSStatusPlanejamento_Internalname = "CONTRATANTE_SSSTATUSPLANEJAMENTO";
         lblTextblockcontratante_servicosspadrao_Internalname = "TEXTBLOCKCONTRATANTE_SERVICOSSPADRAO";
         dynContratante_ServicoSSPadrao_Internalname = "CONTRATANTE_SERVICOSSPADRAO";
         lblTextblockcontratante_retornass_Internalname = "TEXTBLOCKCONTRATANTE_RETORNASS";
         cmbContratante_RetornaSS_Internalname = "CONTRATANTE_RETORNASS";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         grpUnnamedgroup4_Internalname = "UNNAMEDGROUP4";
         lblTextblockcontratante_propostarqrsrv_Internalname = "TEXTBLOCKCONTRATANTE_PROPOSTARQRSRV";
         cmbContratante_PropostaRqrSrv_Internalname = "CONTRATANTE_PROPOSTARQRSRV";
         lblTextblockcontratante_propostarqrprz_Internalname = "TEXTBLOCKCONTRATANTE_PROPOSTARQRPRZ";
         cmbContratante_PropostaRqrPrz_Internalname = "CONTRATANTE_PROPOSTARQRPRZ";
         lblTextblockcontratante_propostarqresf_Internalname = "TEXTBLOCKCONTRATANTE_PROPOSTARQRESF";
         cmbContratante_PropostaRqrEsf_Internalname = "CONTRATANTE_PROPOSTARQRESF";
         lblTextblockcontratante_propostarqrcnt_Internalname = "TEXTBLOCKCONTRATANTE_PROPOSTARQRCNT";
         cmbContratante_PropostaRqrCnt_Internalname = "CONTRATANTE_PROPOSTARQRCNT";
         lblTextblockcontratante_propostanvlcnt_Internalname = "TEXTBLOCKCONTRATANTE_PROPOSTANVLCNT";
         cmbContratante_PropostaNvlCnt_Internalname = "CONTRATANTE_PROPOSTANVLCNT";
         tblUnnamedtable5_Internalname = "UNNAMEDTABLE5";
         grpUnnamedgroup6_Internalname = "UNNAMEDGROUP6";
         lblTextblockcontratante_osautomatica_Internalname = "TEXTBLOCKCONTRATANTE_OSAUTOMATICA";
         cmbContratante_OSAutomatica_Internalname = "CONTRATANTE_OSAUTOMATICA";
         lblTextblockcontratante_osgeraos_Internalname = "TEXTBLOCKCONTRATANTE_OSGERAOS";
         cmbContratante_OSGeraOS_Internalname = "CONTRATANTE_OSGERAOS";
         lblTextblockcontratante_osgeratrp_Internalname = "TEXTBLOCKCONTRATANTE_OSGERATRP";
         cmbContratante_OSGeraTRP_Internalname = "CONTRATANTE_OSGERATRP";
         lblTextblockcontratante_osgeratrd_Internalname = "TEXTBLOCKCONTRATANTE_OSGERATRD";
         cmbContratante_OSGeraTRD_Internalname = "CONTRATANTE_OSGERATRD";
         lblTextblockcontratante_osgerath_Internalname = "TEXTBLOCKCONTRATANTE_OSGERATH";
         cmbContratante_OSGeraTH_Internalname = "CONTRATANTE_OSGERATH";
         lblTextblockcontratante_osgeratr_Internalname = "TEXTBLOCKCONTRATANTE_OSGERATR";
         cmbContratante_OSGeraTR_Internalname = "CONTRATANTE_OSGERATR";
         lblTextblockcontratante_oshmlgcompnd_Internalname = "TEXTBLOCKCONTRATANTE_OSHMLGCOMPND";
         cmbContratante_OSHmlgComPnd_Internalname = "CONTRATANTE_OSHMLGCOMPND";
         lblTextblockcontratante_selecionaresponsavelos_Internalname = "TEXTBLOCKCONTRATANTE_SELECIONARESPONSAVELOS";
         cmbContratante_SelecionaResponsavelOS_Internalname = "CONTRATANTE_SELECIONARESPONSAVELOS";
         lblTextblockcontratante_exibepf_Internalname = "TEXTBLOCKCONTRATANTE_EXIBEPF";
         cmbContratante_ExibePF_Internalname = "CONTRATANTE_EXIBEPF";
         tblUnnamedtable7_Internalname = "UNNAMEDTABLE7";
         grpUnnamedgroup8_Internalname = "UNNAMEDGROUP8";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         lblTextblockcontratante_ativocirculante_Internalname = "TEXTBLOCKCONTRATANTE_ATIVOCIRCULANTE";
         edtContratante_AtivoCirculante_Internalname = "CONTRATANTE_ATIVOCIRCULANTE";
         lblTextblockcontratante_passivocirculante_Internalname = "TEXTBLOCKCONTRATANTE_PASSIVOCIRCULANTE";
         edtContratante_PassivoCirculante_Internalname = "CONTRATANTE_PASSIVOCIRCULANTE";
         lblTextblockcontratante_patrimonioliquido_Internalname = "TEXTBLOCKCONTRATANTE_PATRIMONIOLIQUIDO";
         edtContratante_PatrimonioLiquido_Internalname = "CONTRATANTE_PATRIMONIOLIQUIDO";
         lblTextblockcontratante_receitabruta_Internalname = "TEXTBLOCKCONTRATANTE_RECEITABRUTA";
         edtContratante_ReceitaBruta_Internalname = "CONTRATANTE_RECEITABRUTA";
         lblTextblockcontratante_ttlrltgerencial_Internalname = "TEXTBLOCKCONTRATANTE_TTLRLTGERENCIAL";
         edtContratante_TtlRltGerencial_Internalname = "CONTRATANTE_TTLRLTGERENCIAL";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Gxuitabspanel_tabs_Internalname = "GXUITABSPANEL_TABS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO";
         edtContratante_LogoNomeArq_Internalname = "CONTRATANTE_LOGONOMEARQ";
         edtContratante_LogoTipoArq_Internalname = "CONTRATANTE_LOGOTIPOARQ";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Contratante";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Gxuitabspanel_tabs_Designtimetabs = "[{\"id\":\"Dados\"},{\"id\":\"SSOS\"},{\"id\":\"Financeiro\"}]";
         Gxuitabspanel_tabs_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabs_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabs_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tabs_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contratante";
         edtContratante_LogoArquivo_Filename = "";
         edtContratante_FimDoExpediente_Jsonclick = "";
         edtContratante_FimDoExpediente_Enabled = 1;
         edtContratante_InicioDoExpediente_Jsonclick = "";
         edtContratante_InicioDoExpediente_Enabled = 1;
         edtavRedmine_host_Jsonclick = "";
         edtavRedmine_host_Enabled = 1;
         cmbavRedmine_secure_Jsonclick = "";
         cmbavRedmine_secure.Enabled = 1;
         edtavRedmine_camposervico_Jsonclick = "";
         edtavRedmine_camposervico_Enabled = 1;
         edtavRedmine_camposistema_Jsonclick = "";
         edtavRedmine_camposistema_Enabled = 1;
         cmbavRedmine_versao_Jsonclick = "";
         cmbavRedmine_versao.Enabled = 1;
         edtavRedmine_url_Jsonclick = "";
         edtavRedmine_url_Enabled = 1;
         edtavRedmine_key_Jsonclick = "";
         edtavRedmine_key_Enabled = 1;
         edtavRedmine_user_Jsonclick = "";
         edtavRedmine_user_Enabled = 1;
         edtavEmailto_Jsonclick = "";
         edtavEmailto_Enabled = 1;
         bttBtntestecsharp_Visible = 1;
         bttBtntestaremail_Visible = 1;
         cmbContratante_EmailSdaSec_Jsonclick = "";
         cmbContratante_EmailSdaSec.Enabled = 1;
         cmbContratante_EmailSdaAut_Jsonclick = "";
         cmbContratante_EmailSdaAut.Enabled = 1;
         edtContratante_EmailSdaPort_Jsonclick = "";
         edtContratante_EmailSdaPort_Enabled = 1;
         edtContratante_EmailSdaPass_Jsonclick = "";
         edtContratante_EmailSdaPass_Enabled = 1;
         edtContratante_EmailSdaUser_Jsonclick = "";
         edtContratante_EmailSdaUser_Enabled = 1;
         edtContratante_EmailSdaHost_Jsonclick = "";
         edtContratante_EmailSdaHost_Enabled = 1;
         chkContratante_Ativo.Enabled = 1;
         chkContratante_Ativo.Visible = 1;
         lblTextblockcontratante_ativo_Visible = 1;
         dynMunicipio_Codigo_Jsonclick = "";
         dynMunicipio_Codigo.Enabled = 1;
         dynavEstado_uf_Jsonclick = "";
         dynavEstado_uf.Enabled = 1;
         cmbContratante_UsaOSistema_Jsonclick = "";
         cmbContratante_UsaOSistema.Enabled = 1;
         edtContratante_LogoArquivo_Jsonclick = "";
         edtContratante_LogoArquivo_Parameters = "";
         edtContratante_LogoArquivo_Contenttype = "";
         edtContratante_LogoArquivo_Filetype = "";
         edtContratante_LogoArquivo_Linktarget = "";
         edtContratante_LogoArquivo_Tooltiptext = "";
         edtContratante_LogoArquivo_Display = 0;
         edtContratante_LogoArquivo_Enabled = 1;
         edtContratante_ContaCorrente_Jsonclick = "";
         edtContratante_ContaCorrente_Enabled = 1;
         edtContratante_AgenciaNro_Jsonclick = "";
         edtContratante_AgenciaNro_Enabled = 1;
         edtContratante_AgenciaNome_Jsonclick = "";
         edtContratante_AgenciaNome_Enabled = 1;
         edtContratante_BancoNro_Jsonclick = "";
         edtContratante_BancoNro_Enabled = 1;
         edtContratante_BancoNome_Jsonclick = "";
         edtContratante_BancoNome_Enabled = 1;
         edtContratante_Email_Jsonclick = "";
         edtContratante_Email_Enabled = 1;
         edtContratante_WebSite_Jsonclick = "";
         edtContratante_WebSite_Enabled = 1;
         edtContratante_Fax_Jsonclick = "";
         edtContratante_Fax_Enabled = 1;
         edtContratante_Ramal_Jsonclick = "";
         edtContratante_Ramal_Enabled = 1;
         edtContratante_Telefone_Jsonclick = "";
         edtContratante_Telefone_Enabled = 1;
         edtContratante_IE_Jsonclick = "";
         edtContratante_IE_Enabled = 1;
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_CNPJ_Enabled = 0;
         edtContratante_NomeFantasia_Jsonclick = "";
         edtContratante_NomeFantasia_Enabled = 1;
         edtContratante_RazaoSocial_Jsonclick = "";
         edtContratante_RazaoSocial_Enabled = 0;
         cmbContratante_RetornaSS_Jsonclick = "";
         cmbContratante_RetornaSS.Enabled = 1;
         dynContratante_ServicoSSPadrao_Jsonclick = "";
         dynContratante_ServicoSSPadrao.Enabled = 1;
         cmbContratante_SSStatusPlanejamento_Jsonclick = "";
         cmbContratante_SSStatusPlanejamento.Enabled = 1;
         cmbContratante_SSPrestadoraUnica_Jsonclick = "";
         cmbContratante_SSPrestadoraUnica.Enabled = 1;
         cmbContratante_SSAutomatica_Jsonclick = "";
         cmbContratante_SSAutomatica.Enabled = 1;
         cmbContratante_PropostaNvlCnt_Jsonclick = "";
         cmbContratante_PropostaNvlCnt.Enabled = 1;
         cmbContratante_PropostaRqrCnt_Jsonclick = "";
         cmbContratante_PropostaRqrCnt.Enabled = 1;
         cmbContratante_PropostaRqrEsf_Jsonclick = "";
         cmbContratante_PropostaRqrEsf.Enabled = 1;
         cmbContratante_PropostaRqrPrz_Jsonclick = "";
         cmbContratante_PropostaRqrPrz.Enabled = 1;
         cmbContratante_PropostaRqrSrv_Jsonclick = "";
         cmbContratante_PropostaRqrSrv.Enabled = 1;
         cmbContratante_ExibePF_Jsonclick = "";
         cmbContratante_ExibePF.Enabled = 1;
         cmbContratante_SelecionaResponsavelOS_Jsonclick = "";
         cmbContratante_SelecionaResponsavelOS.Enabled = 1;
         cmbContratante_OSHmlgComPnd_Jsonclick = "";
         cmbContratante_OSHmlgComPnd.Enabled = 1;
         cmbContratante_OSGeraTR_Jsonclick = "";
         cmbContratante_OSGeraTR.Enabled = 1;
         cmbContratante_OSGeraTH_Jsonclick = "";
         cmbContratante_OSGeraTH.Enabled = 1;
         cmbContratante_OSGeraTRD_Jsonclick = "";
         cmbContratante_OSGeraTRD.Enabled = 1;
         cmbContratante_OSGeraTRP_Jsonclick = "";
         cmbContratante_OSGeraTRP.Enabled = 1;
         cmbContratante_OSGeraOS_Jsonclick = "";
         cmbContratante_OSGeraOS.Enabled = 1;
         cmbContratante_OSAutomatica_Jsonclick = "";
         cmbContratante_OSAutomatica.Enabled = 1;
         edtContratante_TtlRltGerencial_Jsonclick = "";
         edtContratante_TtlRltGerencial_Enabled = 1;
         edtContratante_ReceitaBruta_Jsonclick = "";
         edtContratante_ReceitaBruta_Enabled = 1;
         edtContratante_PatrimonioLiquido_Jsonclick = "";
         edtContratante_PatrimonioLiquido_Enabled = 1;
         edtContratante_PassivoCirculante_Jsonclick = "";
         edtContratante_PassivoCirculante_Enabled = 1;
         edtContratante_AtivoCirculante_Jsonclick = "";
         edtContratante_AtivoCirculante_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtContratante_LogoTipoArq_Jsonclick = "";
         edtContratante_LogoTipoArq_Enabled = 0;
         edtContratante_LogoTipoArq_Visible = 1;
         edtContratante_LogoNomeArq_Jsonclick = "";
         edtContratante_LogoNomeArq_Enabled = 0;
         edtContratante_LogoNomeArq_Visible = 1;
         edtContratante_Codigo_Jsonclick = "";
         edtContratante_Codigo_Enabled = 0;
         edtContratante_Codigo_Visible = 1;
         chkContratante_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXAMUNICIPIO_CODIGO_html05124( AV15Estado_UF) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvESTADO_UF05124( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF_data05124( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF_html05124( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF_data05124( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvESTADO_UF_data05124( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000522 */
         pr_default.execute(20);
         while ( (pr_default.getStatus(20) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( T000522_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000522_A24Estado_Nome[0]));
            pr_default.readNext(20);
         }
         pr_default.close(20);
      }

      protected void GXDLAMUNICIPIO_CODIGO05124( String AV15Estado_UF )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAMUNICIPIO_CODIGO_data05124( AV15Estado_UF) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAMUNICIPIO_CODIGO_html05124( String AV15Estado_UF )
      {
         int gxdynajaxvalue ;
         GXDLAMUNICIPIO_CODIGO_data05124( AV15Estado_UF) ;
         gxdynajaxindex = 1;
         dynMunicipio_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynMunicipio_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAMUNICIPIO_CODIGO_data05124( String AV15Estado_UF )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000523 */
         pr_default.execute(21, new Object[] {AV15Estado_UF});
         while ( (pr_default.getStatus(21) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000523_A25Municipio_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000523_A26Municipio_Nome[0]));
            pr_default.readNext(21);
         }
         pr_default.close(21);
      }

      protected void GXDLACONTRATANTE_SERVICOSSPADRAO05124( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATANTE_SERVICOSSPADRAO_data05124( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATANTE_SERVICOSSPADRAO_html05124( )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATANTE_SERVICOSSPADRAO_data05124( ) ;
         gxdynajaxindex = 1;
         dynContratante_ServicoSSPadrao.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratante_ServicoSSPadrao.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATANTE_SERVICOSSPADRAO_data05124( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000524 */
         pr_default.execute(22);
         while ( (pr_default.getStatus(22) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000524_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000524_A605Servico_Sigla[0]));
            pr_default.readNext(22);
         }
         pr_default.close(22);
      }

      protected void XC_32_05124( wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                  int A29Contratante_Codigo ,
                                  String Gx_mode )
      {
         new loadauditcontratante(context ).execute(  "Y", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_33_05124( wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                  int A29Contratante_Codigo ,
                                  String Gx_mode )
      {
         new loadauditcontratante(context ).execute(  "Y", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_34_05124( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                  int A29Contratante_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratante(context ).execute(  "N", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_35_05124( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV29AuditingObject ,
                                  int A29Contratante_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratante(context ).execute(  "N", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A29Contratante_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV29AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_Meetrika")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Validv_Estado_uf( GXCombobox dynGX_Parm1 ,
                                    GXCombobox dynGX_Parm2 )
      {
         dynavEstado_uf = dynGX_Parm1;
         AV15Estado_UF = dynavEstado_uf.CurrentValue;
         dynMunicipio_Codigo = dynGX_Parm2;
         A25Municipio_Codigo = (int)(NumberUtil.Val( dynMunicipio_Codigo.CurrentValue, "."));
         n25Municipio_Codigo = false;
         GXAMUNICIPIO_CODIGO_html05124( AV15Estado_UF) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynMunicipio_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0));
         if ( dynMunicipio_Codigo.ItemCount > 0 )
         {
            A25Municipio_Codigo = (int)(NumberUtil.Val( dynMunicipio_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0))), "."));
            n25Municipio_Codigo = false;
         }
         dynMunicipio_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0));
         isValidOutput.Add(dynMunicipio_Codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Municipio_codigo( String GX_Parm1 ,
                                          GXCombobox dynGX_Parm2 ,
                                          String GX_Parm3 ,
                                          String GX_Parm4 ,
                                          GXCombobox dynGX_Parm5 )
      {
         Gx_mode = GX_Parm1;
         dynMunicipio_Codigo = dynGX_Parm2;
         A25Municipio_Codigo = (int)(NumberUtil.Val( dynMunicipio_Codigo.CurrentValue, "."));
         n25Municipio_Codigo = false;
         A23Estado_UF = GX_Parm3;
         A26Municipio_Nome = GX_Parm4;
         dynavEstado_uf = dynGX_Parm5;
         AV15Estado_UF = dynavEstado_uf.CurrentValue;
         /* Using cursor T000525 */
         pr_default.execute(23, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynMunicipio_Codigo_Internalname;
            }
         }
         A26Municipio_Nome = T000525_A26Municipio_Nome[0];
         A23Estado_UF = T000525_A23Estado_UF[0];
         pr_default.close(23);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Estado_UF = A23Estado_UF;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A26Municipio_Nome = "";
            A23Estado_UF = "";
         }
         GXVvESTADO_UF_html05124( ) ;
         dynavEstado_uf.CurrentValue = AV15Estado_UF;
         isValidOutput.Add(StringUtil.RTrim( A26Municipio_Nome));
         isValidOutput.Add(StringUtil.RTrim( A23Estado_UF));
         if ( dynavEstado_uf.ItemCount > 0 )
         {
            AV15Estado_UF = dynavEstado_uf.getValidValue(AV15Estado_UF);
         }
         dynavEstado_uf.CurrentValue = AV15Estado_UF;
         isValidOutput.Add(dynavEstado_uf);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12052',iparms:[{av:'AV29AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV45Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV7Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV31Redmine_User',fld:'vREDMINE_USER',pic:'',nv:''},{av:'AV30Redmine_Secure',fld:'vREDMINE_SECURE',pic:'9',nv:0},{av:'AV24Redmine_Host',fld:'vREDMINE_HOST',pic:'',nv:''},{av:'AV25Redmine_Url',fld:'vREDMINE_URL',pic:'',nv:''},{av:'AV26Redmine_Key',fld:'vREDMINE_KEY',pic:'',nv:''},{av:'AV28Redmine_Versao',fld:'vREDMINE_VERSAO',pic:'',nv:''},{av:'AV32Redmine_CampoSistema',fld:'vREDMINE_CAMPOSISTEMA',pic:'',nv:''},{av:'AV33Redmine_CampoServico',fld:'vREDMINE_CAMPOSERVICO',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV7Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}]}");
         setEventMetadata("'DOTESTAREMAIL'","{handler:'E13052',iparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV35Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV36Resultado',fld:'vRESULTADO',pic:'',nv:''}],oparms:[{av:'AV36Resultado',fld:'vRESULTADO',pic:'',nv:''}]}");
         setEventMetadata("'DOTESTECSHARP'","{handler:'E14052',iparms:[{av:'A547Contratante_EmailSdaHost',fld:'CONTRATANTE_EMAILSDAHOST',pic:'',nv:''},{av:'A552Contratante_EmailSdaPort',fld:'CONTRATANTE_EMAILSDAPORT',pic:'ZZZ9',nv:0},{av:'A1048Contratante_EmailSdaSec',fld:'CONTRATANTE_EMAILSDASEC',pic:'ZZZ9',nv:0},{av:'A548Contratante_EmailSdaUser',fld:'CONTRATANTE_EMAILSDAUSER',pic:'',nv:''},{av:'A549Contratante_EmailSdaPass',fld:'CONTRATANTE_EMAILSDAPASS',pic:'',nv:''},{av:'AV44EmailTo',fld:'vEMAILTO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOTESTAREMAIL2'","{handler:'E15052',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A549Contratante_EmailSdaPass',fld:'CONTRATANTE_EMAILSDAPASS',pic:'',nv:''},{av:'A550Contratante_EmailSdaKey',fld:'CONTRATANTE_EMAILSDAKEY',pic:'',nv:''},{av:'A548Contratante_EmailSdaUser',fld:'CONTRATANTE_EMAILSDAUSER',pic:'',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(23);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z550Contratante_EmailSdaKey = "";
         Z549Contratante_EmailSdaPass = "";
         Z10Contratante_NomeFantasia = "";
         Z11Contratante_IE = "";
         Z13Contratante_WebSite = "";
         Z14Contratante_Email = "";
         Z31Contratante_Telefone = "";
         Z32Contratante_Ramal = "";
         Z33Contratante_Fax = "";
         Z336Contratante_AgenciaNome = "";
         Z337Contratante_AgenciaNro = "";
         Z338Contratante_BancoNome = "";
         Z339Contratante_BancoNro = "";
         Z16Contratante_ContaCorrente = "";
         Z547Contratante_EmailSdaHost = "";
         Z548Contratante_EmailSdaUser = "";
         Z1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         Z1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         Z1732Contratante_SSStatusPlanejamento = "";
         Z2034Contratante_TtlRltGerencial = "";
         O549Contratante_EmailSdaPass = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15Estado_UF = "";
         GXKey = "";
         AV30Redmine_Secure = 0;
         AV28Redmine_Versao = "";
         A1732Contratante_SSStatusPlanejamento = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         A1125Contratante_LogoNomeArq = "";
         A1126Contratante_LogoTipoArq = "";
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratante_ativocirculante_Jsonclick = "";
         lblTextblockcontratante_passivocirculante_Jsonclick = "";
         lblTextblockcontratante_patrimonioliquido_Jsonclick = "";
         lblTextblockcontratante_receitabruta_Jsonclick = "";
         lblTextblockcontratante_ttlrltgerencial_Jsonclick = "";
         A2034Contratante_TtlRltGerencial = "";
         lblTextblockcontratante_osautomatica_Jsonclick = "";
         lblTextblockcontratante_osgeraos_Jsonclick = "";
         lblTextblockcontratante_osgeratrp_Jsonclick = "";
         lblTextblockcontratante_osgeratrd_Jsonclick = "";
         lblTextblockcontratante_osgerath_Jsonclick = "";
         lblTextblockcontratante_osgeratr_Jsonclick = "";
         lblTextblockcontratante_oshmlgcompnd_Jsonclick = "";
         lblTextblockcontratante_selecionaresponsavelos_Jsonclick = "";
         lblTextblockcontratante_exibepf_Jsonclick = "";
         lblTextblockcontratante_propostarqrsrv_Jsonclick = "";
         lblTextblockcontratante_propostarqrprz_Jsonclick = "";
         lblTextblockcontratante_propostarqresf_Jsonclick = "";
         lblTextblockcontratante_propostarqrcnt_Jsonclick = "";
         lblTextblockcontratante_propostanvlcnt_Jsonclick = "";
         lblTextblockcontratante_ssautomatica_Jsonclick = "";
         lblTextblockcontratante_ssprestadoraunica_Jsonclick = "";
         lblTextblockcontratante_ssstatusplanejamento_Jsonclick = "";
         lblTextblockcontratante_servicosspadrao_Jsonclick = "";
         lblTextblockcontratante_retornass_Jsonclick = "";
         lblTextblockcontratante_razaosocial_Jsonclick = "";
         A9Contratante_RazaoSocial = "";
         lblTextblockcontratante_nomefantasia_Jsonclick = "";
         A10Contratante_NomeFantasia = "";
         lblTextblockcontratante_cnpj_Jsonclick = "";
         A12Contratante_CNPJ = "";
         lblTextblockcontratante_ie_Jsonclick = "";
         A11Contratante_IE = "";
         lblTextblockcontratante_telefone_Jsonclick = "";
         gxphoneLink = "";
         A31Contratante_Telefone = "";
         lblTextblockcontratante_ramal_Jsonclick = "";
         A32Contratante_Ramal = "";
         lblTextblockcontratante_fax_Jsonclick = "";
         A33Contratante_Fax = "";
         lblTextblockcontratante_website_Jsonclick = "";
         A13Contratante_WebSite = "";
         lblTextblockcontratante_email_Jsonclick = "";
         A14Contratante_Email = "";
         lblTextblockcontratante_banconome_Jsonclick = "";
         A338Contratante_BancoNome = "";
         lblTextblockcontratante_banconro_Jsonclick = "";
         A339Contratante_BancoNro = "";
         lblTextblockcontratante_agencianome_Jsonclick = "";
         A336Contratante_AgenciaNome = "";
         lblTextblockcontratante_agencianro_Jsonclick = "";
         A337Contratante_AgenciaNro = "";
         lblTextblockcontratante_contacorrente_Jsonclick = "";
         A16Contratante_ContaCorrente = "";
         lblTextblockcontratante_iniciodoexpediente_Jsonclick = "";
         lblTextblockcontratante_logoarquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A1124Contratante_LogoArquivo = "";
         lblTextblockcontratante_usaosistema_Jsonclick = "";
         lblTextblockestado_uf_Jsonclick = "";
         lblTextblockmunicipio_codigo_Jsonclick = "";
         lblTextblockcontratante_ativo_Jsonclick = "";
         lblTextblockcontratante_emailsdahost_Jsonclick = "";
         A547Contratante_EmailSdaHost = "";
         lblTextblockcontratante_emailsdauser_Jsonclick = "";
         A548Contratante_EmailSdaUser = "";
         lblTextblockcontratante_emailsdapass_Jsonclick = "";
         A549Contratante_EmailSdaPass = "";
         lblTextblockcontratante_emailsdaport_Jsonclick = "";
         lblTextblockcontratante_emailsdaaut_Jsonclick = "";
         lblTextblockcontratante_emailsdasec_Jsonclick = "";
         bttBtntestaremail_Jsonclick = "";
         bttBtntestecsharp_Jsonclick = "";
         lblTextblockemailto_Jsonclick = "";
         AV44EmailTo = "";
         lblTextblockredmine_user_Jsonclick = "";
         AV31Redmine_User = "";
         lblTextblockredmine_key_Jsonclick = "";
         AV26Redmine_Key = "";
         lblTextblockredmine_secure_Jsonclick = "";
         lblTextblockredmine_url_Jsonclick = "";
         AV25Redmine_Url = "";
         lblTextblockredmine_versao_Jsonclick = "";
         lblTextblockredmine_camposistema_Jsonclick = "";
         AV32Redmine_CampoSistema = "";
         lblTextblockredmine_camposervico_Jsonclick = "";
         AV33Redmine_CampoServico = "";
         AV24Redmine_Host = "";
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         lblTextblockcontratante_fimdoexpediente_Jsonclick = "";
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         lblContratante_fimdoexpediente_righttext_Jsonclick = "";
         A550Contratante_EmailSdaKey = "";
         A23Estado_UF = "";
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A26Municipio_Nome = "";
         AV45Pgmname = "";
         Gxuitabspanel_tabs_Height = "";
         Gxuitabspanel_tabs_Class = "";
         Gxuitabspanel_tabs_Activetabid = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode124 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV37EmailText = "";
         AV38Subject = "";
         AV34Usuarios = new GxSimpleCollection();
         AV35Attachments = new GxSimpleCollection();
         AV36Resultado = "";
         AV41SMTPSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         AV19MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV39Email = new GeneXus.Mail.GXMailMessage();
         Z1124Contratante_LogoArquivo = "";
         Z1126Contratante_LogoTipoArq = "";
         Z1125Contratante_LogoNomeArq = "";
         Z12Contratante_CNPJ = "";
         Z9Contratante_RazaoSocial = "";
         Z26Municipio_Nome = "";
         Z23Estado_UF = "";
         T00054_A26Municipio_Nome = new String[] {""} ;
         T00054_A23Estado_UF = new String[] {""} ;
         T00055_A12Contratante_CNPJ = new String[] {""} ;
         T00055_n12Contratante_CNPJ = new bool[] {false} ;
         T00055_A9Contratante_RazaoSocial = new String[] {""} ;
         T00055_n9Contratante_RazaoSocial = new bool[] {false} ;
         T00056_A29Contratante_Codigo = new int[1] ;
         T00056_n29Contratante_Codigo = new bool[] {false} ;
         T00056_A550Contratante_EmailSdaKey = new String[] {""} ;
         T00056_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T00056_A549Contratante_EmailSdaPass = new String[] {""} ;
         T00056_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T00056_A12Contratante_CNPJ = new String[] {""} ;
         T00056_n12Contratante_CNPJ = new bool[] {false} ;
         T00056_A9Contratante_RazaoSocial = new String[] {""} ;
         T00056_n9Contratante_RazaoSocial = new bool[] {false} ;
         T00056_A10Contratante_NomeFantasia = new String[] {""} ;
         T00056_A11Contratante_IE = new String[] {""} ;
         T00056_A13Contratante_WebSite = new String[] {""} ;
         T00056_n13Contratante_WebSite = new bool[] {false} ;
         T00056_A14Contratante_Email = new String[] {""} ;
         T00056_n14Contratante_Email = new bool[] {false} ;
         T00056_A31Contratante_Telefone = new String[] {""} ;
         T00056_A32Contratante_Ramal = new String[] {""} ;
         T00056_n32Contratante_Ramal = new bool[] {false} ;
         T00056_A33Contratante_Fax = new String[] {""} ;
         T00056_n33Contratante_Fax = new bool[] {false} ;
         T00056_A336Contratante_AgenciaNome = new String[] {""} ;
         T00056_n336Contratante_AgenciaNome = new bool[] {false} ;
         T00056_A337Contratante_AgenciaNro = new String[] {""} ;
         T00056_n337Contratante_AgenciaNro = new bool[] {false} ;
         T00056_A338Contratante_BancoNome = new String[] {""} ;
         T00056_n338Contratante_BancoNome = new bool[] {false} ;
         T00056_A339Contratante_BancoNro = new String[] {""} ;
         T00056_n339Contratante_BancoNro = new bool[] {false} ;
         T00056_A16Contratante_ContaCorrente = new String[] {""} ;
         T00056_n16Contratante_ContaCorrente = new bool[] {false} ;
         T00056_A26Municipio_Nome = new String[] {""} ;
         T00056_A30Contratante_Ativo = new bool[] {false} ;
         T00056_A547Contratante_EmailSdaHost = new String[] {""} ;
         T00056_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T00056_A548Contratante_EmailSdaUser = new String[] {""} ;
         T00056_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T00056_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T00056_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T00056_A552Contratante_EmailSdaPort = new short[1] ;
         T00056_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T00056_A1048Contratante_EmailSdaSec = new short[1] ;
         T00056_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T00056_A593Contratante_OSAutomatica = new bool[] {false} ;
         T00056_A1652Contratante_SSAutomatica = new bool[] {false} ;
         T00056_n1652Contratante_SSAutomatica = new bool[] {false} ;
         T00056_A594Contratante_ExisteConferencia = new bool[] {false} ;
         T00056_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         T00056_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         T00056_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         T00056_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         T00056_A1727Contratante_ServicoSSPadrao = new int[1] ;
         T00056_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         T00056_A1729Contratante_RetornaSS = new bool[] {false} ;
         T00056_n1729Contratante_RetornaSS = new bool[] {false} ;
         T00056_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         T00056_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         T00056_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         T00056_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         T00056_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         T00056_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         T00056_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         T00056_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         T00056_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         T00056_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         T00056_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         T00056_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         T00056_A1742Contratante_PropostaNvlCnt = new short[1] ;
         T00056_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         T00056_A1757Contratante_OSGeraOS = new bool[] {false} ;
         T00056_n1757Contratante_OSGeraOS = new bool[] {false} ;
         T00056_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         T00056_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         T00056_A1759Contratante_OSGeraTH = new bool[] {false} ;
         T00056_n1759Contratante_OSGeraTH = new bool[] {false} ;
         T00056_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         T00056_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         T00056_A1761Contratante_OSGeraTR = new bool[] {false} ;
         T00056_n1761Contratante_OSGeraTR = new bool[] {false} ;
         T00056_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         T00056_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         T00056_A1805Contratante_AtivoCirculante = new decimal[1] ;
         T00056_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         T00056_A1806Contratante_PassivoCirculante = new decimal[1] ;
         T00056_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         T00056_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         T00056_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         T00056_A1808Contratante_ReceitaBruta = new decimal[1] ;
         T00056_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         T00056_A1822Contratante_UsaOSistema = new bool[] {false} ;
         T00056_n1822Contratante_UsaOSistema = new bool[] {false} ;
         T00056_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         T00056_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         T00056_A2084Contratante_PrzAoRtr = new short[1] ;
         T00056_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         T00056_A2083Contratante_PrzActRtr = new short[1] ;
         T00056_n2083Contratante_PrzActRtr = new bool[] {false} ;
         T00056_A2085Contratante_RequerOrigem = new bool[] {false} ;
         T00056_n2085Contratante_RequerOrigem = new bool[] {false} ;
         T00056_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         T00056_A2090Contratante_ExibePF = new bool[] {false} ;
         T00056_A1126Contratante_LogoTipoArq = new String[] {""} ;
         T00056_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         T00056_A1125Contratante_LogoNomeArq = new String[] {""} ;
         T00056_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         T00056_A25Municipio_Codigo = new int[1] ;
         T00056_n25Municipio_Codigo = new bool[] {false} ;
         T00056_A335Contratante_PessoaCod = new int[1] ;
         T00056_A23Estado_UF = new String[] {""} ;
         T00056_A1124Contratante_LogoArquivo = new String[] {""} ;
         T00056_n1124Contratante_LogoArquivo = new bool[] {false} ;
         T00057_A26Municipio_Nome = new String[] {""} ;
         T00057_A23Estado_UF = new String[] {""} ;
         T00058_A12Contratante_CNPJ = new String[] {""} ;
         T00058_n12Contratante_CNPJ = new bool[] {false} ;
         T00058_A9Contratante_RazaoSocial = new String[] {""} ;
         T00058_n9Contratante_RazaoSocial = new bool[] {false} ;
         T00059_A29Contratante_Codigo = new int[1] ;
         T00059_n29Contratante_Codigo = new bool[] {false} ;
         T00053_A29Contratante_Codigo = new int[1] ;
         T00053_n29Contratante_Codigo = new bool[] {false} ;
         T00053_A550Contratante_EmailSdaKey = new String[] {""} ;
         T00053_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T00053_A549Contratante_EmailSdaPass = new String[] {""} ;
         T00053_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T00053_A10Contratante_NomeFantasia = new String[] {""} ;
         T00053_A11Contratante_IE = new String[] {""} ;
         T00053_A13Contratante_WebSite = new String[] {""} ;
         T00053_n13Contratante_WebSite = new bool[] {false} ;
         T00053_A14Contratante_Email = new String[] {""} ;
         T00053_n14Contratante_Email = new bool[] {false} ;
         T00053_A31Contratante_Telefone = new String[] {""} ;
         T00053_A32Contratante_Ramal = new String[] {""} ;
         T00053_n32Contratante_Ramal = new bool[] {false} ;
         T00053_A33Contratante_Fax = new String[] {""} ;
         T00053_n33Contratante_Fax = new bool[] {false} ;
         T00053_A336Contratante_AgenciaNome = new String[] {""} ;
         T00053_n336Contratante_AgenciaNome = new bool[] {false} ;
         T00053_A337Contratante_AgenciaNro = new String[] {""} ;
         T00053_n337Contratante_AgenciaNro = new bool[] {false} ;
         T00053_A338Contratante_BancoNome = new String[] {""} ;
         T00053_n338Contratante_BancoNome = new bool[] {false} ;
         T00053_A339Contratante_BancoNro = new String[] {""} ;
         T00053_n339Contratante_BancoNro = new bool[] {false} ;
         T00053_A16Contratante_ContaCorrente = new String[] {""} ;
         T00053_n16Contratante_ContaCorrente = new bool[] {false} ;
         T00053_A30Contratante_Ativo = new bool[] {false} ;
         T00053_A547Contratante_EmailSdaHost = new String[] {""} ;
         T00053_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T00053_A548Contratante_EmailSdaUser = new String[] {""} ;
         T00053_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T00053_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T00053_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T00053_A552Contratante_EmailSdaPort = new short[1] ;
         T00053_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T00053_A1048Contratante_EmailSdaSec = new short[1] ;
         T00053_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T00053_A593Contratante_OSAutomatica = new bool[] {false} ;
         T00053_A1652Contratante_SSAutomatica = new bool[] {false} ;
         T00053_n1652Contratante_SSAutomatica = new bool[] {false} ;
         T00053_A594Contratante_ExisteConferencia = new bool[] {false} ;
         T00053_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         T00053_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         T00053_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         T00053_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         T00053_A1727Contratante_ServicoSSPadrao = new int[1] ;
         T00053_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         T00053_A1729Contratante_RetornaSS = new bool[] {false} ;
         T00053_n1729Contratante_RetornaSS = new bool[] {false} ;
         T00053_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         T00053_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         T00053_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         T00053_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         T00053_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         T00053_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         T00053_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         T00053_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         T00053_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         T00053_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         T00053_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         T00053_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         T00053_A1742Contratante_PropostaNvlCnt = new short[1] ;
         T00053_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         T00053_A1757Contratante_OSGeraOS = new bool[] {false} ;
         T00053_n1757Contratante_OSGeraOS = new bool[] {false} ;
         T00053_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         T00053_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         T00053_A1759Contratante_OSGeraTH = new bool[] {false} ;
         T00053_n1759Contratante_OSGeraTH = new bool[] {false} ;
         T00053_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         T00053_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         T00053_A1761Contratante_OSGeraTR = new bool[] {false} ;
         T00053_n1761Contratante_OSGeraTR = new bool[] {false} ;
         T00053_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         T00053_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         T00053_A1805Contratante_AtivoCirculante = new decimal[1] ;
         T00053_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         T00053_A1806Contratante_PassivoCirculante = new decimal[1] ;
         T00053_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         T00053_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         T00053_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         T00053_A1808Contratante_ReceitaBruta = new decimal[1] ;
         T00053_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         T00053_A1822Contratante_UsaOSistema = new bool[] {false} ;
         T00053_n1822Contratante_UsaOSistema = new bool[] {false} ;
         T00053_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         T00053_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         T00053_A2084Contratante_PrzAoRtr = new short[1] ;
         T00053_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         T00053_A2083Contratante_PrzActRtr = new short[1] ;
         T00053_n2083Contratante_PrzActRtr = new bool[] {false} ;
         T00053_A2085Contratante_RequerOrigem = new bool[] {false} ;
         T00053_n2085Contratante_RequerOrigem = new bool[] {false} ;
         T00053_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         T00053_A2090Contratante_ExibePF = new bool[] {false} ;
         T00053_A1126Contratante_LogoTipoArq = new String[] {""} ;
         T00053_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         T00053_A1125Contratante_LogoNomeArq = new String[] {""} ;
         T00053_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         T00053_A25Municipio_Codigo = new int[1] ;
         T00053_n25Municipio_Codigo = new bool[] {false} ;
         T00053_A335Contratante_PessoaCod = new int[1] ;
         T00053_A1124Contratante_LogoArquivo = new String[] {""} ;
         T00053_n1124Contratante_LogoArquivo = new bool[] {false} ;
         T000510_A29Contratante_Codigo = new int[1] ;
         T000510_n29Contratante_Codigo = new bool[] {false} ;
         T000511_A29Contratante_Codigo = new int[1] ;
         T000511_n29Contratante_Codigo = new bool[] {false} ;
         T00052_A29Contratante_Codigo = new int[1] ;
         T00052_n29Contratante_Codigo = new bool[] {false} ;
         T00052_A550Contratante_EmailSdaKey = new String[] {""} ;
         T00052_n550Contratante_EmailSdaKey = new bool[] {false} ;
         T00052_A549Contratante_EmailSdaPass = new String[] {""} ;
         T00052_n549Contratante_EmailSdaPass = new bool[] {false} ;
         T00052_A10Contratante_NomeFantasia = new String[] {""} ;
         T00052_A11Contratante_IE = new String[] {""} ;
         T00052_A13Contratante_WebSite = new String[] {""} ;
         T00052_n13Contratante_WebSite = new bool[] {false} ;
         T00052_A14Contratante_Email = new String[] {""} ;
         T00052_n14Contratante_Email = new bool[] {false} ;
         T00052_A31Contratante_Telefone = new String[] {""} ;
         T00052_A32Contratante_Ramal = new String[] {""} ;
         T00052_n32Contratante_Ramal = new bool[] {false} ;
         T00052_A33Contratante_Fax = new String[] {""} ;
         T00052_n33Contratante_Fax = new bool[] {false} ;
         T00052_A336Contratante_AgenciaNome = new String[] {""} ;
         T00052_n336Contratante_AgenciaNome = new bool[] {false} ;
         T00052_A337Contratante_AgenciaNro = new String[] {""} ;
         T00052_n337Contratante_AgenciaNro = new bool[] {false} ;
         T00052_A338Contratante_BancoNome = new String[] {""} ;
         T00052_n338Contratante_BancoNome = new bool[] {false} ;
         T00052_A339Contratante_BancoNro = new String[] {""} ;
         T00052_n339Contratante_BancoNro = new bool[] {false} ;
         T00052_A16Contratante_ContaCorrente = new String[] {""} ;
         T00052_n16Contratante_ContaCorrente = new bool[] {false} ;
         T00052_A30Contratante_Ativo = new bool[] {false} ;
         T00052_A547Contratante_EmailSdaHost = new String[] {""} ;
         T00052_n547Contratante_EmailSdaHost = new bool[] {false} ;
         T00052_A548Contratante_EmailSdaUser = new String[] {""} ;
         T00052_n548Contratante_EmailSdaUser = new bool[] {false} ;
         T00052_A551Contratante_EmailSdaAut = new bool[] {false} ;
         T00052_n551Contratante_EmailSdaAut = new bool[] {false} ;
         T00052_A552Contratante_EmailSdaPort = new short[1] ;
         T00052_n552Contratante_EmailSdaPort = new bool[] {false} ;
         T00052_A1048Contratante_EmailSdaSec = new short[1] ;
         T00052_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         T00052_A593Contratante_OSAutomatica = new bool[] {false} ;
         T00052_A1652Contratante_SSAutomatica = new bool[] {false} ;
         T00052_n1652Contratante_SSAutomatica = new bool[] {false} ;
         T00052_A594Contratante_ExisteConferencia = new bool[] {false} ;
         T00052_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         T00052_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         T00052_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         T00052_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         T00052_A1727Contratante_ServicoSSPadrao = new int[1] ;
         T00052_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         T00052_A1729Contratante_RetornaSS = new bool[] {false} ;
         T00052_n1729Contratante_RetornaSS = new bool[] {false} ;
         T00052_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         T00052_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         T00052_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         T00052_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         T00052_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         T00052_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         T00052_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         T00052_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         T00052_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         T00052_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         T00052_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         T00052_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         T00052_A1742Contratante_PropostaNvlCnt = new short[1] ;
         T00052_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         T00052_A1757Contratante_OSGeraOS = new bool[] {false} ;
         T00052_n1757Contratante_OSGeraOS = new bool[] {false} ;
         T00052_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         T00052_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         T00052_A1759Contratante_OSGeraTH = new bool[] {false} ;
         T00052_n1759Contratante_OSGeraTH = new bool[] {false} ;
         T00052_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         T00052_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         T00052_A1761Contratante_OSGeraTR = new bool[] {false} ;
         T00052_n1761Contratante_OSGeraTR = new bool[] {false} ;
         T00052_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         T00052_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         T00052_A1805Contratante_AtivoCirculante = new decimal[1] ;
         T00052_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         T00052_A1806Contratante_PassivoCirculante = new decimal[1] ;
         T00052_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         T00052_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         T00052_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         T00052_A1808Contratante_ReceitaBruta = new decimal[1] ;
         T00052_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         T00052_A1822Contratante_UsaOSistema = new bool[] {false} ;
         T00052_n1822Contratante_UsaOSistema = new bool[] {false} ;
         T00052_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         T00052_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         T00052_A2084Contratante_PrzAoRtr = new short[1] ;
         T00052_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         T00052_A2083Contratante_PrzActRtr = new short[1] ;
         T00052_n2083Contratante_PrzActRtr = new bool[] {false} ;
         T00052_A2085Contratante_RequerOrigem = new bool[] {false} ;
         T00052_n2085Contratante_RequerOrigem = new bool[] {false} ;
         T00052_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         T00052_A2090Contratante_ExibePF = new bool[] {false} ;
         T00052_A1126Contratante_LogoTipoArq = new String[] {""} ;
         T00052_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         T00052_A1125Contratante_LogoNomeArq = new String[] {""} ;
         T00052_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         T00052_A25Municipio_Codigo = new int[1] ;
         T00052_n25Municipio_Codigo = new bool[] {false} ;
         T00052_A335Contratante_PessoaCod = new int[1] ;
         T00052_A1124Contratante_LogoArquivo = new String[] {""} ;
         T00052_n1124Contratante_LogoArquivo = new bool[] {false} ;
         T000512_A29Contratante_Codigo = new int[1] ;
         T000512_n29Contratante_Codigo = new bool[] {false} ;
         T000516_A26Municipio_Nome = new String[] {""} ;
         T000516_A23Estado_UF = new String[] {""} ;
         T000517_A12Contratante_CNPJ = new String[] {""} ;
         T000517_n12Contratante_CNPJ = new bool[] {false} ;
         T000517_A9Contratante_RazaoSocial = new String[] {""} ;
         T000517_n9Contratante_RazaoSocial = new bool[] {false} ;
         T000518_A1380Redmine_Codigo = new int[1] ;
         T000519_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000519_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000520_A5AreaTrabalho_Codigo = new int[1] ;
         T000521_A29Contratante_Codigo = new int[1] ;
         T000521_n29Contratante_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i2034Contratante_TtlRltGerencial = "";
         i1732Contratante_SSStatusPlanejamento = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000522_A23Estado_UF = new String[] {""} ;
         T000522_A24Estado_Nome = new String[] {""} ;
         T000523_A25Municipio_Codigo = new int[1] ;
         T000523_n25Municipio_Codigo = new bool[] {false} ;
         T000523_A26Municipio_Nome = new String[] {""} ;
         T000523_A23Estado_UF = new String[] {""} ;
         T000524_A155Servico_Codigo = new int[1] ;
         T000524_A605Servico_Sigla = new String[] {""} ;
         T000524_A1635Servico_IsPublico = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000525_A26Municipio_Nome = new String[] {""} ;
         T000525_A23Estado_UF = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratante__default(),
            new Object[][] {
                new Object[] {
               T00052_A29Contratante_Codigo, T00052_A550Contratante_EmailSdaKey, T00052_n550Contratante_EmailSdaKey, T00052_A549Contratante_EmailSdaPass, T00052_n549Contratante_EmailSdaPass, T00052_A10Contratante_NomeFantasia, T00052_A11Contratante_IE, T00052_A13Contratante_WebSite, T00052_n13Contratante_WebSite, T00052_A14Contratante_Email,
               T00052_n14Contratante_Email, T00052_A31Contratante_Telefone, T00052_A32Contratante_Ramal, T00052_n32Contratante_Ramal, T00052_A33Contratante_Fax, T00052_n33Contratante_Fax, T00052_A336Contratante_AgenciaNome, T00052_n336Contratante_AgenciaNome, T00052_A337Contratante_AgenciaNro, T00052_n337Contratante_AgenciaNro,
               T00052_A338Contratante_BancoNome, T00052_n338Contratante_BancoNome, T00052_A339Contratante_BancoNro, T00052_n339Contratante_BancoNro, T00052_A16Contratante_ContaCorrente, T00052_n16Contratante_ContaCorrente, T00052_A30Contratante_Ativo, T00052_A547Contratante_EmailSdaHost, T00052_n547Contratante_EmailSdaHost, T00052_A548Contratante_EmailSdaUser,
               T00052_n548Contratante_EmailSdaUser, T00052_A551Contratante_EmailSdaAut, T00052_n551Contratante_EmailSdaAut, T00052_A552Contratante_EmailSdaPort, T00052_n552Contratante_EmailSdaPort, T00052_A1048Contratante_EmailSdaSec, T00052_n1048Contratante_EmailSdaSec, T00052_A593Contratante_OSAutomatica, T00052_A1652Contratante_SSAutomatica, T00052_n1652Contratante_SSAutomatica,
               T00052_A594Contratante_ExisteConferencia, T00052_A1448Contratante_InicioDoExpediente, T00052_n1448Contratante_InicioDoExpediente, T00052_A1192Contratante_FimDoExpediente, T00052_n1192Contratante_FimDoExpediente, T00052_A1727Contratante_ServicoSSPadrao, T00052_n1727Contratante_ServicoSSPadrao, T00052_A1729Contratante_RetornaSS, T00052_n1729Contratante_RetornaSS, T00052_A1732Contratante_SSStatusPlanejamento,
               T00052_n1732Contratante_SSStatusPlanejamento, T00052_A1733Contratante_SSPrestadoraUnica, T00052_n1733Contratante_SSPrestadoraUnica, T00052_A1738Contratante_PropostaRqrSrv, T00052_n1738Contratante_PropostaRqrSrv, T00052_A1739Contratante_PropostaRqrPrz, T00052_n1739Contratante_PropostaRqrPrz, T00052_A1740Contratante_PropostaRqrEsf, T00052_n1740Contratante_PropostaRqrEsf, T00052_A1741Contratante_PropostaRqrCnt,
               T00052_n1741Contratante_PropostaRqrCnt, T00052_A1742Contratante_PropostaNvlCnt, T00052_n1742Contratante_PropostaNvlCnt, T00052_A1757Contratante_OSGeraOS, T00052_n1757Contratante_OSGeraOS, T00052_A1758Contratante_OSGeraTRP, T00052_n1758Contratante_OSGeraTRP, T00052_A1759Contratante_OSGeraTH, T00052_n1759Contratante_OSGeraTH, T00052_A1760Contratante_OSGeraTRD,
               T00052_n1760Contratante_OSGeraTRD, T00052_A1761Contratante_OSGeraTR, T00052_n1761Contratante_OSGeraTR, T00052_A1803Contratante_OSHmlgComPnd, T00052_n1803Contratante_OSHmlgComPnd, T00052_A1805Contratante_AtivoCirculante, T00052_n1805Contratante_AtivoCirculante, T00052_A1806Contratante_PassivoCirculante, T00052_n1806Contratante_PassivoCirculante, T00052_A1807Contratante_PatrimonioLiquido,
               T00052_n1807Contratante_PatrimonioLiquido, T00052_A1808Contratante_ReceitaBruta, T00052_n1808Contratante_ReceitaBruta, T00052_A1822Contratante_UsaOSistema, T00052_n1822Contratante_UsaOSistema, T00052_A2034Contratante_TtlRltGerencial, T00052_n2034Contratante_TtlRltGerencial, T00052_A2084Contratante_PrzAoRtr, T00052_n2084Contratante_PrzAoRtr, T00052_A2083Contratante_PrzActRtr,
               T00052_n2083Contratante_PrzActRtr, T00052_A2085Contratante_RequerOrigem, T00052_n2085Contratante_RequerOrigem, T00052_A2089Contratante_SelecionaResponsavelOS, T00052_A2090Contratante_ExibePF, T00052_A1126Contratante_LogoTipoArq, T00052_n1126Contratante_LogoTipoArq, T00052_A1125Contratante_LogoNomeArq, T00052_n1125Contratante_LogoNomeArq, T00052_A25Municipio_Codigo,
               T00052_n25Municipio_Codigo, T00052_A335Contratante_PessoaCod, T00052_A1124Contratante_LogoArquivo, T00052_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               T00053_A29Contratante_Codigo, T00053_A550Contratante_EmailSdaKey, T00053_n550Contratante_EmailSdaKey, T00053_A549Contratante_EmailSdaPass, T00053_n549Contratante_EmailSdaPass, T00053_A10Contratante_NomeFantasia, T00053_A11Contratante_IE, T00053_A13Contratante_WebSite, T00053_n13Contratante_WebSite, T00053_A14Contratante_Email,
               T00053_n14Contratante_Email, T00053_A31Contratante_Telefone, T00053_A32Contratante_Ramal, T00053_n32Contratante_Ramal, T00053_A33Contratante_Fax, T00053_n33Contratante_Fax, T00053_A336Contratante_AgenciaNome, T00053_n336Contratante_AgenciaNome, T00053_A337Contratante_AgenciaNro, T00053_n337Contratante_AgenciaNro,
               T00053_A338Contratante_BancoNome, T00053_n338Contratante_BancoNome, T00053_A339Contratante_BancoNro, T00053_n339Contratante_BancoNro, T00053_A16Contratante_ContaCorrente, T00053_n16Contratante_ContaCorrente, T00053_A30Contratante_Ativo, T00053_A547Contratante_EmailSdaHost, T00053_n547Contratante_EmailSdaHost, T00053_A548Contratante_EmailSdaUser,
               T00053_n548Contratante_EmailSdaUser, T00053_A551Contratante_EmailSdaAut, T00053_n551Contratante_EmailSdaAut, T00053_A552Contratante_EmailSdaPort, T00053_n552Contratante_EmailSdaPort, T00053_A1048Contratante_EmailSdaSec, T00053_n1048Contratante_EmailSdaSec, T00053_A593Contratante_OSAutomatica, T00053_A1652Contratante_SSAutomatica, T00053_n1652Contratante_SSAutomatica,
               T00053_A594Contratante_ExisteConferencia, T00053_A1448Contratante_InicioDoExpediente, T00053_n1448Contratante_InicioDoExpediente, T00053_A1192Contratante_FimDoExpediente, T00053_n1192Contratante_FimDoExpediente, T00053_A1727Contratante_ServicoSSPadrao, T00053_n1727Contratante_ServicoSSPadrao, T00053_A1729Contratante_RetornaSS, T00053_n1729Contratante_RetornaSS, T00053_A1732Contratante_SSStatusPlanejamento,
               T00053_n1732Contratante_SSStatusPlanejamento, T00053_A1733Contratante_SSPrestadoraUnica, T00053_n1733Contratante_SSPrestadoraUnica, T00053_A1738Contratante_PropostaRqrSrv, T00053_n1738Contratante_PropostaRqrSrv, T00053_A1739Contratante_PropostaRqrPrz, T00053_n1739Contratante_PropostaRqrPrz, T00053_A1740Contratante_PropostaRqrEsf, T00053_n1740Contratante_PropostaRqrEsf, T00053_A1741Contratante_PropostaRqrCnt,
               T00053_n1741Contratante_PropostaRqrCnt, T00053_A1742Contratante_PropostaNvlCnt, T00053_n1742Contratante_PropostaNvlCnt, T00053_A1757Contratante_OSGeraOS, T00053_n1757Contratante_OSGeraOS, T00053_A1758Contratante_OSGeraTRP, T00053_n1758Contratante_OSGeraTRP, T00053_A1759Contratante_OSGeraTH, T00053_n1759Contratante_OSGeraTH, T00053_A1760Contratante_OSGeraTRD,
               T00053_n1760Contratante_OSGeraTRD, T00053_A1761Contratante_OSGeraTR, T00053_n1761Contratante_OSGeraTR, T00053_A1803Contratante_OSHmlgComPnd, T00053_n1803Contratante_OSHmlgComPnd, T00053_A1805Contratante_AtivoCirculante, T00053_n1805Contratante_AtivoCirculante, T00053_A1806Contratante_PassivoCirculante, T00053_n1806Contratante_PassivoCirculante, T00053_A1807Contratante_PatrimonioLiquido,
               T00053_n1807Contratante_PatrimonioLiquido, T00053_A1808Contratante_ReceitaBruta, T00053_n1808Contratante_ReceitaBruta, T00053_A1822Contratante_UsaOSistema, T00053_n1822Contratante_UsaOSistema, T00053_A2034Contratante_TtlRltGerencial, T00053_n2034Contratante_TtlRltGerencial, T00053_A2084Contratante_PrzAoRtr, T00053_n2084Contratante_PrzAoRtr, T00053_A2083Contratante_PrzActRtr,
               T00053_n2083Contratante_PrzActRtr, T00053_A2085Contratante_RequerOrigem, T00053_n2085Contratante_RequerOrigem, T00053_A2089Contratante_SelecionaResponsavelOS, T00053_A2090Contratante_ExibePF, T00053_A1126Contratante_LogoTipoArq, T00053_n1126Contratante_LogoTipoArq, T00053_A1125Contratante_LogoNomeArq, T00053_n1125Contratante_LogoNomeArq, T00053_A25Municipio_Codigo,
               T00053_n25Municipio_Codigo, T00053_A335Contratante_PessoaCod, T00053_A1124Contratante_LogoArquivo, T00053_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               T00054_A26Municipio_Nome, T00054_A23Estado_UF
               }
               , new Object[] {
               T00055_A12Contratante_CNPJ, T00055_n12Contratante_CNPJ, T00055_A9Contratante_RazaoSocial, T00055_n9Contratante_RazaoSocial
               }
               , new Object[] {
               T00056_A29Contratante_Codigo, T00056_A550Contratante_EmailSdaKey, T00056_n550Contratante_EmailSdaKey, T00056_A549Contratante_EmailSdaPass, T00056_n549Contratante_EmailSdaPass, T00056_A12Contratante_CNPJ, T00056_n12Contratante_CNPJ, T00056_A9Contratante_RazaoSocial, T00056_n9Contratante_RazaoSocial, T00056_A10Contratante_NomeFantasia,
               T00056_A11Contratante_IE, T00056_A13Contratante_WebSite, T00056_n13Contratante_WebSite, T00056_A14Contratante_Email, T00056_n14Contratante_Email, T00056_A31Contratante_Telefone, T00056_A32Contratante_Ramal, T00056_n32Contratante_Ramal, T00056_A33Contratante_Fax, T00056_n33Contratante_Fax,
               T00056_A336Contratante_AgenciaNome, T00056_n336Contratante_AgenciaNome, T00056_A337Contratante_AgenciaNro, T00056_n337Contratante_AgenciaNro, T00056_A338Contratante_BancoNome, T00056_n338Contratante_BancoNome, T00056_A339Contratante_BancoNro, T00056_n339Contratante_BancoNro, T00056_A16Contratante_ContaCorrente, T00056_n16Contratante_ContaCorrente,
               T00056_A26Municipio_Nome, T00056_A30Contratante_Ativo, T00056_A547Contratante_EmailSdaHost, T00056_n547Contratante_EmailSdaHost, T00056_A548Contratante_EmailSdaUser, T00056_n548Contratante_EmailSdaUser, T00056_A551Contratante_EmailSdaAut, T00056_n551Contratante_EmailSdaAut, T00056_A552Contratante_EmailSdaPort, T00056_n552Contratante_EmailSdaPort,
               T00056_A1048Contratante_EmailSdaSec, T00056_n1048Contratante_EmailSdaSec, T00056_A593Contratante_OSAutomatica, T00056_A1652Contratante_SSAutomatica, T00056_n1652Contratante_SSAutomatica, T00056_A594Contratante_ExisteConferencia, T00056_A1448Contratante_InicioDoExpediente, T00056_n1448Contratante_InicioDoExpediente, T00056_A1192Contratante_FimDoExpediente, T00056_n1192Contratante_FimDoExpediente,
               T00056_A1727Contratante_ServicoSSPadrao, T00056_n1727Contratante_ServicoSSPadrao, T00056_A1729Contratante_RetornaSS, T00056_n1729Contratante_RetornaSS, T00056_A1732Contratante_SSStatusPlanejamento, T00056_n1732Contratante_SSStatusPlanejamento, T00056_A1733Contratante_SSPrestadoraUnica, T00056_n1733Contratante_SSPrestadoraUnica, T00056_A1738Contratante_PropostaRqrSrv, T00056_n1738Contratante_PropostaRqrSrv,
               T00056_A1739Contratante_PropostaRqrPrz, T00056_n1739Contratante_PropostaRqrPrz, T00056_A1740Contratante_PropostaRqrEsf, T00056_n1740Contratante_PropostaRqrEsf, T00056_A1741Contratante_PropostaRqrCnt, T00056_n1741Contratante_PropostaRqrCnt, T00056_A1742Contratante_PropostaNvlCnt, T00056_n1742Contratante_PropostaNvlCnt, T00056_A1757Contratante_OSGeraOS, T00056_n1757Contratante_OSGeraOS,
               T00056_A1758Contratante_OSGeraTRP, T00056_n1758Contratante_OSGeraTRP, T00056_A1759Contratante_OSGeraTH, T00056_n1759Contratante_OSGeraTH, T00056_A1760Contratante_OSGeraTRD, T00056_n1760Contratante_OSGeraTRD, T00056_A1761Contratante_OSGeraTR, T00056_n1761Contratante_OSGeraTR, T00056_A1803Contratante_OSHmlgComPnd, T00056_n1803Contratante_OSHmlgComPnd,
               T00056_A1805Contratante_AtivoCirculante, T00056_n1805Contratante_AtivoCirculante, T00056_A1806Contratante_PassivoCirculante, T00056_n1806Contratante_PassivoCirculante, T00056_A1807Contratante_PatrimonioLiquido, T00056_n1807Contratante_PatrimonioLiquido, T00056_A1808Contratante_ReceitaBruta, T00056_n1808Contratante_ReceitaBruta, T00056_A1822Contratante_UsaOSistema, T00056_n1822Contratante_UsaOSistema,
               T00056_A2034Contratante_TtlRltGerencial, T00056_n2034Contratante_TtlRltGerencial, T00056_A2084Contratante_PrzAoRtr, T00056_n2084Contratante_PrzAoRtr, T00056_A2083Contratante_PrzActRtr, T00056_n2083Contratante_PrzActRtr, T00056_A2085Contratante_RequerOrigem, T00056_n2085Contratante_RequerOrigem, T00056_A2089Contratante_SelecionaResponsavelOS, T00056_A2090Contratante_ExibePF,
               T00056_A1126Contratante_LogoTipoArq, T00056_n1126Contratante_LogoTipoArq, T00056_A1125Contratante_LogoNomeArq, T00056_n1125Contratante_LogoNomeArq, T00056_A25Municipio_Codigo, T00056_n25Municipio_Codigo, T00056_A335Contratante_PessoaCod, T00056_A23Estado_UF, T00056_A1124Contratante_LogoArquivo, T00056_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               T00057_A26Municipio_Nome, T00057_A23Estado_UF
               }
               , new Object[] {
               T00058_A12Contratante_CNPJ, T00058_n12Contratante_CNPJ, T00058_A9Contratante_RazaoSocial, T00058_n9Contratante_RazaoSocial
               }
               , new Object[] {
               T00059_A29Contratante_Codigo
               }
               , new Object[] {
               T000510_A29Contratante_Codigo
               }
               , new Object[] {
               T000511_A29Contratante_Codigo
               }
               , new Object[] {
               T000512_A29Contratante_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000516_A26Municipio_Nome, T000516_A23Estado_UF
               }
               , new Object[] {
               T000517_A12Contratante_CNPJ, T000517_n12Contratante_CNPJ, T000517_A9Contratante_RazaoSocial, T000517_n9Contratante_RazaoSocial
               }
               , new Object[] {
               T000518_A1380Redmine_Codigo
               }
               , new Object[] {
               T000519_A63ContratanteUsuario_ContratanteCod, T000519_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               T000520_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000521_A29Contratante_Codigo
               }
               , new Object[] {
               T000522_A23Estado_UF, T000522_A24Estado_Nome
               }
               , new Object[] {
               T000523_A25Municipio_Codigo, T000523_A26Municipio_Nome, T000523_A23Estado_UF
               }
               , new Object[] {
               T000524_A155Servico_Codigo, T000524_A605Servico_Sigla, T000524_A1635Servico_IsPublico
               }
               , new Object[] {
               T000525_A26Municipio_Nome, T000525_A23Estado_UF
               }
            }
         );
         Z2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         A2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         i2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         Z2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         A2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         i2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         Z1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         A1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         i1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         Z1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         A1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         i1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         Z1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         A1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         i1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         Z1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         A1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         i1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         Z551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         A551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         i551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         Z30Contratante_Ativo = true;
         A30Contratante_Ativo = true;
         i30Contratante_Ativo = true;
         AV45Pgmname = "Contratante";
      }

      private short Z552Contratante_EmailSdaPort ;
      private short Z1048Contratante_EmailSdaSec ;
      private short Z1742Contratante_PropostaNvlCnt ;
      private short Z2084Contratante_PrzAoRtr ;
      private short Z2083Contratante_PrzActRtr ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short AV30Redmine_Secure ;
      private short A1048Contratante_EmailSdaSec ;
      private short A1742Contratante_PropostaNvlCnt ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short edtContratante_LogoArquivo_Display ;
      private short A552Contratante_EmailSdaPort ;
      private short A2084Contratante_PrzAoRtr ;
      private short A2083Contratante_PrzActRtr ;
      private short Gx_BScreen ;
      private short RcdFound124 ;
      private short AV43retorno ;
      private short GXt_int1 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short i1742Contratante_PropostaNvlCnt ;
      private short wbTemp ;
      private int wcpOAV7Contratante_Codigo ;
      private int Z29Contratante_Codigo ;
      private int Z1727Contratante_ServicoSSPadrao ;
      private int Z25Municipio_Codigo ;
      private int Z335Contratante_PessoaCod ;
      private int N335Contratante_PessoaCod ;
      private int N25Municipio_Codigo ;
      private int A25Municipio_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int AV7Contratante_Codigo ;
      private int trnEnded ;
      private int A1727Contratante_ServicoSSPadrao ;
      private int A29Contratante_Codigo ;
      private int edtContratante_Codigo_Enabled ;
      private int edtContratante_Codigo_Visible ;
      private int edtContratante_LogoNomeArq_Visible ;
      private int edtContratante_LogoNomeArq_Enabled ;
      private int edtContratante_LogoTipoArq_Visible ;
      private int edtContratante_LogoTipoArq_Enabled ;
      private int lblTbjava_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratante_AtivoCirculante_Enabled ;
      private int edtContratante_PassivoCirculante_Enabled ;
      private int edtContratante_PatrimonioLiquido_Enabled ;
      private int edtContratante_ReceitaBruta_Enabled ;
      private int edtContratante_TtlRltGerencial_Enabled ;
      private int edtContratante_RazaoSocial_Enabled ;
      private int edtContratante_NomeFantasia_Enabled ;
      private int edtContratante_CNPJ_Enabled ;
      private int edtContratante_IE_Enabled ;
      private int edtContratante_Telefone_Enabled ;
      private int edtContratante_Ramal_Enabled ;
      private int edtContratante_Fax_Enabled ;
      private int edtContratante_WebSite_Enabled ;
      private int edtContratante_Email_Enabled ;
      private int edtContratante_BancoNome_Enabled ;
      private int edtContratante_BancoNro_Enabled ;
      private int edtContratante_AgenciaNome_Enabled ;
      private int edtContratante_AgenciaNro_Enabled ;
      private int edtContratante_ContaCorrente_Enabled ;
      private int edtContratante_LogoArquivo_Enabled ;
      private int lblTextblockcontratante_ativo_Visible ;
      private int edtContratante_EmailSdaHost_Enabled ;
      private int edtContratante_EmailSdaUser_Enabled ;
      private int edtContratante_EmailSdaPass_Enabled ;
      private int edtContratante_EmailSdaPort_Enabled ;
      private int bttBtntestaremail_Visible ;
      private int bttBtntestecsharp_Visible ;
      private int edtavEmailto_Enabled ;
      private int edtavRedmine_user_Enabled ;
      private int edtavRedmine_key_Enabled ;
      private int edtavRedmine_url_Enabled ;
      private int edtavRedmine_camposistema_Enabled ;
      private int edtavRedmine_camposervico_Enabled ;
      private int edtavRedmine_host_Enabled ;
      private int edtContratante_InicioDoExpediente_Enabled ;
      private int edtContratante_FimDoExpediente_Enabled ;
      private int AV16Insert_Contratante_PessoaCod ;
      private int AV17Insert_Municipio_Codigo ;
      private int Gxuitabspanel_tabs_Selectedtabindex ;
      private int AV46GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1805Contratante_AtivoCirculante ;
      private decimal Z1806Contratante_PassivoCirculante ;
      private decimal Z1807Contratante_PatrimonioLiquido ;
      private decimal Z1808Contratante_ReceitaBruta ;
      private decimal A1805Contratante_AtivoCirculante ;
      private decimal A1806Contratante_PassivoCirculante ;
      private decimal A1807Contratante_PatrimonioLiquido ;
      private decimal A1808Contratante_ReceitaBruta ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z550Contratante_EmailSdaKey ;
      private String Z10Contratante_NomeFantasia ;
      private String Z11Contratante_IE ;
      private String Z31Contratante_Telefone ;
      private String Z32Contratante_Ramal ;
      private String Z33Contratante_Fax ;
      private String Z336Contratante_AgenciaNome ;
      private String Z337Contratante_AgenciaNro ;
      private String Z338Contratante_BancoNome ;
      private String Z339Contratante_BancoNro ;
      private String Z16Contratante_ContaCorrente ;
      private String Z1732Contratante_SSStatusPlanejamento ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String AV15Estado_UF ;
      private String Gx_mode ;
      private String GXKey ;
      private String AV28Redmine_Versao ;
      private String chkContratante_Ativo_Internalname ;
      private String A1732Contratante_SSStatusPlanejamento ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratante_NomeFantasia_Internalname ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtContratante_LogoNomeArq_Internalname ;
      private String A1125Contratante_LogoNomeArq ;
      private String edtContratante_LogoNomeArq_Jsonclick ;
      private String edtContratante_LogoTipoArq_Internalname ;
      private String A1126Contratante_LogoTipoArq ;
      private String edtContratante_LogoTipoArq_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontratante_ativocirculante_Internalname ;
      private String lblTextblockcontratante_ativocirculante_Jsonclick ;
      private String edtContratante_AtivoCirculante_Internalname ;
      private String edtContratante_AtivoCirculante_Jsonclick ;
      private String lblTextblockcontratante_passivocirculante_Internalname ;
      private String lblTextblockcontratante_passivocirculante_Jsonclick ;
      private String edtContratante_PassivoCirculante_Internalname ;
      private String edtContratante_PassivoCirculante_Jsonclick ;
      private String lblTextblockcontratante_patrimonioliquido_Internalname ;
      private String lblTextblockcontratante_patrimonioliquido_Jsonclick ;
      private String edtContratante_PatrimonioLiquido_Internalname ;
      private String edtContratante_PatrimonioLiquido_Jsonclick ;
      private String lblTextblockcontratante_receitabruta_Internalname ;
      private String lblTextblockcontratante_receitabruta_Jsonclick ;
      private String edtContratante_ReceitaBruta_Internalname ;
      private String edtContratante_ReceitaBruta_Jsonclick ;
      private String lblTextblockcontratante_ttlrltgerencial_Internalname ;
      private String lblTextblockcontratante_ttlrltgerencial_Jsonclick ;
      private String edtContratante_TtlRltGerencial_Internalname ;
      private String edtContratante_TtlRltGerencial_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String grpUnnamedgroup6_Internalname ;
      private String grpUnnamedgroup8_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String lblTextblockcontratante_osautomatica_Internalname ;
      private String lblTextblockcontratante_osautomatica_Jsonclick ;
      private String cmbContratante_OSAutomatica_Internalname ;
      private String cmbContratante_OSAutomatica_Jsonclick ;
      private String lblTextblockcontratante_osgeraos_Internalname ;
      private String lblTextblockcontratante_osgeraos_Jsonclick ;
      private String cmbContratante_OSGeraOS_Internalname ;
      private String cmbContratante_OSGeraOS_Jsonclick ;
      private String lblTextblockcontratante_osgeratrp_Internalname ;
      private String lblTextblockcontratante_osgeratrp_Jsonclick ;
      private String cmbContratante_OSGeraTRP_Internalname ;
      private String cmbContratante_OSGeraTRP_Jsonclick ;
      private String lblTextblockcontratante_osgeratrd_Internalname ;
      private String lblTextblockcontratante_osgeratrd_Jsonclick ;
      private String cmbContratante_OSGeraTRD_Internalname ;
      private String cmbContratante_OSGeraTRD_Jsonclick ;
      private String lblTextblockcontratante_osgerath_Internalname ;
      private String lblTextblockcontratante_osgerath_Jsonclick ;
      private String cmbContratante_OSGeraTH_Internalname ;
      private String cmbContratante_OSGeraTH_Jsonclick ;
      private String lblTextblockcontratante_osgeratr_Internalname ;
      private String lblTextblockcontratante_osgeratr_Jsonclick ;
      private String cmbContratante_OSGeraTR_Internalname ;
      private String cmbContratante_OSGeraTR_Jsonclick ;
      private String lblTextblockcontratante_oshmlgcompnd_Internalname ;
      private String lblTextblockcontratante_oshmlgcompnd_Jsonclick ;
      private String cmbContratante_OSHmlgComPnd_Internalname ;
      private String cmbContratante_OSHmlgComPnd_Jsonclick ;
      private String lblTextblockcontratante_selecionaresponsavelos_Internalname ;
      private String lblTextblockcontratante_selecionaresponsavelos_Jsonclick ;
      private String cmbContratante_SelecionaResponsavelOS_Internalname ;
      private String cmbContratante_SelecionaResponsavelOS_Jsonclick ;
      private String lblTextblockcontratante_exibepf_Internalname ;
      private String lblTextblockcontratante_exibepf_Jsonclick ;
      private String cmbContratante_ExibePF_Internalname ;
      private String cmbContratante_ExibePF_Jsonclick ;
      private String tblUnnamedtable5_Internalname ;
      private String lblTextblockcontratante_propostarqrsrv_Internalname ;
      private String lblTextblockcontratante_propostarqrsrv_Jsonclick ;
      private String cmbContratante_PropostaRqrSrv_Internalname ;
      private String cmbContratante_PropostaRqrSrv_Jsonclick ;
      private String lblTextblockcontratante_propostarqrprz_Internalname ;
      private String lblTextblockcontratante_propostarqrprz_Jsonclick ;
      private String cmbContratante_PropostaRqrPrz_Internalname ;
      private String cmbContratante_PropostaRqrPrz_Jsonclick ;
      private String lblTextblockcontratante_propostarqresf_Internalname ;
      private String lblTextblockcontratante_propostarqresf_Jsonclick ;
      private String cmbContratante_PropostaRqrEsf_Internalname ;
      private String cmbContratante_PropostaRqrEsf_Jsonclick ;
      private String lblTextblockcontratante_propostarqrcnt_Internalname ;
      private String lblTextblockcontratante_propostarqrcnt_Jsonclick ;
      private String cmbContratante_PropostaRqrCnt_Internalname ;
      private String cmbContratante_PropostaRqrCnt_Jsonclick ;
      private String lblTextblockcontratante_propostanvlcnt_Internalname ;
      private String lblTextblockcontratante_propostanvlcnt_Jsonclick ;
      private String cmbContratante_PropostaNvlCnt_Internalname ;
      private String cmbContratante_PropostaNvlCnt_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockcontratante_ssautomatica_Internalname ;
      private String lblTextblockcontratante_ssautomatica_Jsonclick ;
      private String cmbContratante_SSAutomatica_Internalname ;
      private String cmbContratante_SSAutomatica_Jsonclick ;
      private String lblTextblockcontratante_ssprestadoraunica_Internalname ;
      private String lblTextblockcontratante_ssprestadoraunica_Jsonclick ;
      private String cmbContratante_SSPrestadoraUnica_Internalname ;
      private String cmbContratante_SSPrestadoraUnica_Jsonclick ;
      private String lblTextblockcontratante_ssstatusplanejamento_Internalname ;
      private String lblTextblockcontratante_ssstatusplanejamento_Jsonclick ;
      private String cmbContratante_SSStatusPlanejamento_Internalname ;
      private String cmbContratante_SSStatusPlanejamento_Jsonclick ;
      private String lblTextblockcontratante_servicosspadrao_Internalname ;
      private String lblTextblockcontratante_servicosspadrao_Jsonclick ;
      private String dynContratante_ServicoSSPadrao_Internalname ;
      private String dynContratante_ServicoSSPadrao_Jsonclick ;
      private String lblTextblockcontratante_retornass_Internalname ;
      private String lblTextblockcontratante_retornass_Jsonclick ;
      private String cmbContratante_RetornaSS_Internalname ;
      private String cmbContratante_RetornaSS_Jsonclick ;
      private String tblUnnamedtable9_Internalname ;
      private String lblTextblockcontratante_razaosocial_Internalname ;
      private String lblTextblockcontratante_razaosocial_Jsonclick ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String lblTextblockcontratante_nomefantasia_Internalname ;
      private String lblTextblockcontratante_nomefantasia_Jsonclick ;
      private String A10Contratante_NomeFantasia ;
      private String edtContratante_NomeFantasia_Jsonclick ;
      private String lblTextblockcontratante_cnpj_Internalname ;
      private String lblTextblockcontratante_cnpj_Jsonclick ;
      private String edtContratante_CNPJ_Internalname ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String lblTextblockcontratante_ie_Internalname ;
      private String lblTextblockcontratante_ie_Jsonclick ;
      private String edtContratante_IE_Internalname ;
      private String A11Contratante_IE ;
      private String edtContratante_IE_Jsonclick ;
      private String lblTextblockcontratante_telefone_Internalname ;
      private String lblTextblockcontratante_telefone_Jsonclick ;
      private String gxphoneLink ;
      private String A31Contratante_Telefone ;
      private String edtContratante_Telefone_Internalname ;
      private String edtContratante_Telefone_Jsonclick ;
      private String lblTextblockcontratante_ramal_Internalname ;
      private String lblTextblockcontratante_ramal_Jsonclick ;
      private String edtContratante_Ramal_Internalname ;
      private String A32Contratante_Ramal ;
      private String edtContratante_Ramal_Jsonclick ;
      private String lblTextblockcontratante_fax_Internalname ;
      private String lblTextblockcontratante_fax_Jsonclick ;
      private String A33Contratante_Fax ;
      private String edtContratante_Fax_Internalname ;
      private String edtContratante_Fax_Jsonclick ;
      private String lblTextblockcontratante_website_Internalname ;
      private String lblTextblockcontratante_website_Jsonclick ;
      private String edtContratante_WebSite_Internalname ;
      private String edtContratante_WebSite_Jsonclick ;
      private String lblTextblockcontratante_email_Internalname ;
      private String lblTextblockcontratante_email_Jsonclick ;
      private String edtContratante_Email_Internalname ;
      private String edtContratante_Email_Jsonclick ;
      private String lblTextblockcontratante_banconome_Internalname ;
      private String lblTextblockcontratante_banconome_Jsonclick ;
      private String edtContratante_BancoNome_Internalname ;
      private String A338Contratante_BancoNome ;
      private String edtContratante_BancoNome_Jsonclick ;
      private String lblTextblockcontratante_banconro_Internalname ;
      private String lblTextblockcontratante_banconro_Jsonclick ;
      private String edtContratante_BancoNro_Internalname ;
      private String A339Contratante_BancoNro ;
      private String edtContratante_BancoNro_Jsonclick ;
      private String lblTextblockcontratante_agencianome_Internalname ;
      private String lblTextblockcontratante_agencianome_Jsonclick ;
      private String edtContratante_AgenciaNome_Internalname ;
      private String A336Contratante_AgenciaNome ;
      private String edtContratante_AgenciaNome_Jsonclick ;
      private String lblTextblockcontratante_agencianro_Internalname ;
      private String lblTextblockcontratante_agencianro_Jsonclick ;
      private String edtContratante_AgenciaNro_Internalname ;
      private String A337Contratante_AgenciaNro ;
      private String edtContratante_AgenciaNro_Jsonclick ;
      private String lblTextblockcontratante_contacorrente_Internalname ;
      private String lblTextblockcontratante_contacorrente_Jsonclick ;
      private String edtContratante_ContaCorrente_Internalname ;
      private String A16Contratante_ContaCorrente ;
      private String edtContratante_ContaCorrente_Jsonclick ;
      private String lblTextblockcontratante_iniciodoexpediente_Internalname ;
      private String lblTextblockcontratante_iniciodoexpediente_Jsonclick ;
      private String lblTextblockcontratante_logoarquivo_Internalname ;
      private String lblTextblockcontratante_logoarquivo_Jsonclick ;
      private String edtContratante_LogoArquivo_Filename ;
      private String edtContratante_LogoArquivo_Filetype ;
      private String edtContratante_LogoArquivo_Internalname ;
      private String edtContratante_LogoArquivo_Contenttype ;
      private String edtContratante_LogoArquivo_Linktarget ;
      private String edtContratante_LogoArquivo_Parameters ;
      private String edtContratante_LogoArquivo_Tooltiptext ;
      private String edtContratante_LogoArquivo_Jsonclick ;
      private String lblTextblockcontratante_usaosistema_Internalname ;
      private String lblTextblockcontratante_usaosistema_Jsonclick ;
      private String cmbContratante_UsaOSistema_Internalname ;
      private String cmbContratante_UsaOSistema_Jsonclick ;
      private String grpUnnamedgroup10_Internalname ;
      private String grpUnnamedgroup11_Internalname ;
      private String lblTextblockestado_uf_Internalname ;
      private String lblTextblockestado_uf_Jsonclick ;
      private String dynavEstado_uf_Internalname ;
      private String dynavEstado_uf_Jsonclick ;
      private String lblTextblockmunicipio_codigo_Internalname ;
      private String lblTextblockmunicipio_codigo_Jsonclick ;
      private String dynMunicipio_Codigo_Internalname ;
      private String dynMunicipio_Codigo_Jsonclick ;
      private String lblTextblockcontratante_ativo_Internalname ;
      private String lblTextblockcontratante_ativo_Jsonclick ;
      private String tblEmailsaida_Internalname ;
      private String lblTextblockcontratante_emailsdahost_Internalname ;
      private String lblTextblockcontratante_emailsdahost_Jsonclick ;
      private String edtContratante_EmailSdaHost_Internalname ;
      private String edtContratante_EmailSdaHost_Jsonclick ;
      private String lblTextblockcontratante_emailsdauser_Internalname ;
      private String lblTextblockcontratante_emailsdauser_Jsonclick ;
      private String edtContratante_EmailSdaUser_Internalname ;
      private String edtContratante_EmailSdaUser_Jsonclick ;
      private String lblTextblockcontratante_emailsdapass_Internalname ;
      private String lblTextblockcontratante_emailsdapass_Jsonclick ;
      private String edtContratante_EmailSdaPass_Internalname ;
      private String edtContratante_EmailSdaPass_Jsonclick ;
      private String lblTextblockcontratante_emailsdaport_Internalname ;
      private String lblTextblockcontratante_emailsdaport_Jsonclick ;
      private String edtContratante_EmailSdaPort_Internalname ;
      private String edtContratante_EmailSdaPort_Jsonclick ;
      private String lblTextblockcontratante_emailsdaaut_Internalname ;
      private String lblTextblockcontratante_emailsdaaut_Jsonclick ;
      private String cmbContratante_EmailSdaAut_Internalname ;
      private String cmbContratante_EmailSdaAut_Jsonclick ;
      private String lblTextblockcontratante_emailsdasec_Internalname ;
      private String lblTextblockcontratante_emailsdasec_Jsonclick ;
      private String cmbContratante_EmailSdaSec_Internalname ;
      private String cmbContratante_EmailSdaSec_Jsonclick ;
      private String bttBtntestaremail_Internalname ;
      private String bttBtntestaremail_Jsonclick ;
      private String tblTablemergedtestecsharp_Internalname ;
      private String bttBtntestecsharp_Internalname ;
      private String bttBtntestecsharp_Jsonclick ;
      private String lblTextblockemailto_Internalname ;
      private String lblTextblockemailto_Jsonclick ;
      private String edtavEmailto_Internalname ;
      private String AV44EmailTo ;
      private String edtavEmailto_Jsonclick ;
      private String tblRedmine_Internalname ;
      private String lblTextblockredmine_user_Internalname ;
      private String lblTextblockredmine_user_Jsonclick ;
      private String edtavRedmine_user_Internalname ;
      private String AV31Redmine_User ;
      private String edtavRedmine_user_Jsonclick ;
      private String lblTextblockredmine_key_Internalname ;
      private String lblTextblockredmine_key_Jsonclick ;
      private String edtavRedmine_key_Internalname ;
      private String AV26Redmine_Key ;
      private String edtavRedmine_key_Jsonclick ;
      private String lblTextblockredmine_secure_Internalname ;
      private String lblTextblockredmine_secure_Jsonclick ;
      private String lblTextblockredmine_url_Internalname ;
      private String lblTextblockredmine_url_Jsonclick ;
      private String edtavRedmine_url_Internalname ;
      private String AV25Redmine_Url ;
      private String edtavRedmine_url_Jsonclick ;
      private String lblTextblockredmine_versao_Internalname ;
      private String lblTextblockredmine_versao_Jsonclick ;
      private String cmbavRedmine_versao_Internalname ;
      private String cmbavRedmine_versao_Jsonclick ;
      private String lblTextblockredmine_camposistema_Internalname ;
      private String lblTextblockredmine_camposistema_Jsonclick ;
      private String edtavRedmine_camposistema_Internalname ;
      private String AV32Redmine_CampoSistema ;
      private String edtavRedmine_camposistema_Jsonclick ;
      private String lblTextblockredmine_camposervico_Internalname ;
      private String lblTextblockredmine_camposervico_Jsonclick ;
      private String edtavRedmine_camposervico_Internalname ;
      private String AV33Redmine_CampoServico ;
      private String edtavRedmine_camposervico_Jsonclick ;
      private String tblTablemergedredmine_secure_Internalname ;
      private String cmbavRedmine_secure_Internalname ;
      private String cmbavRedmine_secure_Jsonclick ;
      private String edtavRedmine_host_Internalname ;
      private String AV24Redmine_Host ;
      private String edtavRedmine_host_Jsonclick ;
      private String tblTablemergedcontratante_iniciodoexpediente_Internalname ;
      private String edtContratante_InicioDoExpediente_Internalname ;
      private String edtContratante_InicioDoExpediente_Jsonclick ;
      private String lblTextblockcontratante_fimdoexpediente_Internalname ;
      private String lblTextblockcontratante_fimdoexpediente_Jsonclick ;
      private String edtContratante_FimDoExpediente_Internalname ;
      private String edtContratante_FimDoExpediente_Jsonclick ;
      private String cellContratante_fimdoexpediente_righttext_cell_Internalname ;
      private String lblContratante_fimdoexpediente_righttext_Internalname ;
      private String lblContratante_fimdoexpediente_righttext_Jsonclick ;
      private String A550Contratante_EmailSdaKey ;
      private String A23Estado_UF ;
      private String A26Municipio_Nome ;
      private String AV45Pgmname ;
      private String Gxuitabspanel_tabs_Width ;
      private String Gxuitabspanel_tabs_Height ;
      private String Gxuitabspanel_tabs_Cls ;
      private String Gxuitabspanel_tabs_Class ;
      private String Gxuitabspanel_tabs_Activetabid ;
      private String Gxuitabspanel_tabs_Designtimetabs ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode124 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV38Subject ;
      private String AV36Resultado ;
      private String Z1126Contratante_LogoTipoArq ;
      private String Z1125Contratante_LogoNomeArq ;
      private String Z9Contratante_RazaoSocial ;
      private String Z26Municipio_Nome ;
      private String Z23Estado_UF ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i1732Contratante_SSStatusPlanejamento ;
      private String Gxuitabspanel_tabs_Internalname ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1448Contratante_InicioDoExpediente ;
      private DateTime Z1192Contratante_FimDoExpediente ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private bool Z30Contratante_Ativo ;
      private bool Z551Contratante_EmailSdaAut ;
      private bool Z593Contratante_OSAutomatica ;
      private bool Z1652Contratante_SSAutomatica ;
      private bool Z594Contratante_ExisteConferencia ;
      private bool Z1729Contratante_RetornaSS ;
      private bool Z1733Contratante_SSPrestadoraUnica ;
      private bool Z1738Contratante_PropostaRqrSrv ;
      private bool Z1739Contratante_PropostaRqrPrz ;
      private bool Z1740Contratante_PropostaRqrEsf ;
      private bool Z1741Contratante_PropostaRqrCnt ;
      private bool Z1757Contratante_OSGeraOS ;
      private bool Z1758Contratante_OSGeraTRP ;
      private bool Z1759Contratante_OSGeraTH ;
      private bool Z1760Contratante_OSGeraTRD ;
      private bool Z1761Contratante_OSGeraTR ;
      private bool Z1803Contratante_OSHmlgComPnd ;
      private bool Z1822Contratante_UsaOSistema ;
      private bool Z2085Contratante_RequerOrigem ;
      private bool Z2089Contratante_SelecionaResponsavelOS ;
      private bool Z2090Contratante_ExibePF ;
      private bool entryPointCalled ;
      private bool n25Municipio_Codigo ;
      private bool toggleJsOutput ;
      private bool A1822Contratante_UsaOSistema ;
      private bool n1822Contratante_UsaOSistema ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool A1652Contratante_SSAutomatica ;
      private bool n1652Contratante_SSAutomatica ;
      private bool A1733Contratante_SSPrestadoraUnica ;
      private bool n1733Contratante_SSPrestadoraUnica ;
      private bool n1732Contratante_SSStatusPlanejamento ;
      private bool A1729Contratante_RetornaSS ;
      private bool n1729Contratante_RetornaSS ;
      private bool A1738Contratante_PropostaRqrSrv ;
      private bool n1738Contratante_PropostaRqrSrv ;
      private bool A1739Contratante_PropostaRqrPrz ;
      private bool n1739Contratante_PropostaRqrPrz ;
      private bool A1740Contratante_PropostaRqrEsf ;
      private bool n1740Contratante_PropostaRqrEsf ;
      private bool A1741Contratante_PropostaRqrCnt ;
      private bool n1741Contratante_PropostaRqrCnt ;
      private bool n1742Contratante_PropostaNvlCnt ;
      private bool A593Contratante_OSAutomatica ;
      private bool A1757Contratante_OSGeraOS ;
      private bool n1757Contratante_OSGeraOS ;
      private bool A1758Contratante_OSGeraTRP ;
      private bool n1758Contratante_OSGeraTRP ;
      private bool A1760Contratante_OSGeraTRD ;
      private bool n1760Contratante_OSGeraTRD ;
      private bool A1759Contratante_OSGeraTH ;
      private bool n1759Contratante_OSGeraTH ;
      private bool A1761Contratante_OSGeraTR ;
      private bool n1761Contratante_OSGeraTR ;
      private bool A1803Contratante_OSHmlgComPnd ;
      private bool n1803Contratante_OSHmlgComPnd ;
      private bool A2089Contratante_SelecionaResponsavelOS ;
      private bool A2090Contratante_ExibePF ;
      private bool wbErr ;
      private bool n1727Contratante_ServicoSSPadrao ;
      private bool n1124Contratante_LogoArquivo ;
      private bool A30Contratante_Ativo ;
      private bool n9Contratante_RazaoSocial ;
      private bool n12Contratante_CNPJ ;
      private bool n32Contratante_Ramal ;
      private bool n33Contratante_Fax ;
      private bool n13Contratante_WebSite ;
      private bool n14Contratante_Email ;
      private bool n338Contratante_BancoNome ;
      private bool n339Contratante_BancoNro ;
      private bool n336Contratante_AgenciaNome ;
      private bool n337Contratante_AgenciaNro ;
      private bool n16Contratante_ContaCorrente ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n1805Contratante_AtivoCirculante ;
      private bool n1806Contratante_PassivoCirculante ;
      private bool n1807Contratante_PatrimonioLiquido ;
      private bool n1808Contratante_ReceitaBruta ;
      private bool n2034Contratante_TtlRltGerencial ;
      private bool n29Contratante_Codigo ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n2084Contratante_PrzAoRtr ;
      private bool n2083Contratante_PrzActRtr ;
      private bool n2085Contratante_RequerOrigem ;
      private bool A2085Contratante_RequerOrigem ;
      private bool A594Contratante_ExisteConferencia ;
      private bool Gxuitabspanel_tabs_Enabled ;
      private bool Gxuitabspanel_tabs_Autowidth ;
      private bool Gxuitabspanel_tabs_Autoheight ;
      private bool Gxuitabspanel_tabs_Autoscroll ;
      private bool Gxuitabspanel_tabs_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i2085Contratante_RequerOrigem ;
      private bool i1822Contratante_UsaOSistema ;
      private bool i1738Contratante_PropostaRqrSrv ;
      private bool i551Contratante_EmailSdaAut ;
      private bool i30Contratante_Ativo ;
      private String AV37EmailText ;
      private String Z549Contratante_EmailSdaPass ;
      private String Z13Contratante_WebSite ;
      private String Z14Contratante_Email ;
      private String Z547Contratante_EmailSdaHost ;
      private String Z548Contratante_EmailSdaUser ;
      private String Z2034Contratante_TtlRltGerencial ;
      private String O549Contratante_EmailSdaPass ;
      private String A2034Contratante_TtlRltGerencial ;
      private String A12Contratante_CNPJ ;
      private String A13Contratante_WebSite ;
      private String A14Contratante_Email ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A549Contratante_EmailSdaPass ;
      private String Z12Contratante_CNPJ ;
      private String i2034Contratante_TtlRltGerencial ;
      private String A1124Contratante_LogoArquivo ;
      private String Z1124Contratante_LogoArquivo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratante_UsaOSistema ;
      private GXCombobox cmbavRedmine_secure ;
      private GXCombobox cmbavRedmine_versao ;
      private GXCombobox cmbContratante_EmailSdaAut ;
      private GXCombobox cmbContratante_EmailSdaSec ;
      private GXCombobox dynavEstado_uf ;
      private GXCombobox dynMunicipio_Codigo ;
      private GXCheckbox chkContratante_Ativo ;
      private GXCombobox cmbContratante_SSAutomatica ;
      private GXCombobox cmbContratante_SSPrestadoraUnica ;
      private GXCombobox cmbContratante_SSStatusPlanejamento ;
      private GXCombobox dynContratante_ServicoSSPadrao ;
      private GXCombobox cmbContratante_RetornaSS ;
      private GXCombobox cmbContratante_PropostaRqrSrv ;
      private GXCombobox cmbContratante_PropostaRqrPrz ;
      private GXCombobox cmbContratante_PropostaRqrEsf ;
      private GXCombobox cmbContratante_PropostaRqrCnt ;
      private GXCombobox cmbContratante_PropostaNvlCnt ;
      private GXCombobox cmbContratante_OSAutomatica ;
      private GXCombobox cmbContratante_OSGeraOS ;
      private GXCombobox cmbContratante_OSGeraTRP ;
      private GXCombobox cmbContratante_OSGeraTRD ;
      private GXCombobox cmbContratante_OSGeraTH ;
      private GXCombobox cmbContratante_OSGeraTR ;
      private GXCombobox cmbContratante_OSHmlgComPnd ;
      private GXCombobox cmbContratante_SelecionaResponsavelOS ;
      private GXCombobox cmbContratante_ExibePF ;
      private IDataStoreProvider pr_default ;
      private String[] T00054_A26Municipio_Nome ;
      private String[] T00054_A23Estado_UF ;
      private String[] T00055_A12Contratante_CNPJ ;
      private bool[] T00055_n12Contratante_CNPJ ;
      private String[] T00055_A9Contratante_RazaoSocial ;
      private bool[] T00055_n9Contratante_RazaoSocial ;
      private int[] T00056_A29Contratante_Codigo ;
      private bool[] T00056_n29Contratante_Codigo ;
      private String[] T00056_A550Contratante_EmailSdaKey ;
      private bool[] T00056_n550Contratante_EmailSdaKey ;
      private String[] T00056_A549Contratante_EmailSdaPass ;
      private bool[] T00056_n549Contratante_EmailSdaPass ;
      private String[] T00056_A12Contratante_CNPJ ;
      private bool[] T00056_n12Contratante_CNPJ ;
      private String[] T00056_A9Contratante_RazaoSocial ;
      private bool[] T00056_n9Contratante_RazaoSocial ;
      private String[] T00056_A10Contratante_NomeFantasia ;
      private String[] T00056_A11Contratante_IE ;
      private String[] T00056_A13Contratante_WebSite ;
      private bool[] T00056_n13Contratante_WebSite ;
      private String[] T00056_A14Contratante_Email ;
      private bool[] T00056_n14Contratante_Email ;
      private String[] T00056_A31Contratante_Telefone ;
      private String[] T00056_A32Contratante_Ramal ;
      private bool[] T00056_n32Contratante_Ramal ;
      private String[] T00056_A33Contratante_Fax ;
      private bool[] T00056_n33Contratante_Fax ;
      private String[] T00056_A336Contratante_AgenciaNome ;
      private bool[] T00056_n336Contratante_AgenciaNome ;
      private String[] T00056_A337Contratante_AgenciaNro ;
      private bool[] T00056_n337Contratante_AgenciaNro ;
      private String[] T00056_A338Contratante_BancoNome ;
      private bool[] T00056_n338Contratante_BancoNome ;
      private String[] T00056_A339Contratante_BancoNro ;
      private bool[] T00056_n339Contratante_BancoNro ;
      private String[] T00056_A16Contratante_ContaCorrente ;
      private bool[] T00056_n16Contratante_ContaCorrente ;
      private String[] T00056_A26Municipio_Nome ;
      private bool[] T00056_A30Contratante_Ativo ;
      private String[] T00056_A547Contratante_EmailSdaHost ;
      private bool[] T00056_n547Contratante_EmailSdaHost ;
      private String[] T00056_A548Contratante_EmailSdaUser ;
      private bool[] T00056_n548Contratante_EmailSdaUser ;
      private bool[] T00056_A551Contratante_EmailSdaAut ;
      private bool[] T00056_n551Contratante_EmailSdaAut ;
      private short[] T00056_A552Contratante_EmailSdaPort ;
      private bool[] T00056_n552Contratante_EmailSdaPort ;
      private short[] T00056_A1048Contratante_EmailSdaSec ;
      private bool[] T00056_n1048Contratante_EmailSdaSec ;
      private bool[] T00056_A593Contratante_OSAutomatica ;
      private bool[] T00056_A1652Contratante_SSAutomatica ;
      private bool[] T00056_n1652Contratante_SSAutomatica ;
      private bool[] T00056_A594Contratante_ExisteConferencia ;
      private DateTime[] T00056_A1448Contratante_InicioDoExpediente ;
      private bool[] T00056_n1448Contratante_InicioDoExpediente ;
      private DateTime[] T00056_A1192Contratante_FimDoExpediente ;
      private bool[] T00056_n1192Contratante_FimDoExpediente ;
      private int[] T00056_A1727Contratante_ServicoSSPadrao ;
      private bool[] T00056_n1727Contratante_ServicoSSPadrao ;
      private bool[] T00056_A1729Contratante_RetornaSS ;
      private bool[] T00056_n1729Contratante_RetornaSS ;
      private String[] T00056_A1732Contratante_SSStatusPlanejamento ;
      private bool[] T00056_n1732Contratante_SSStatusPlanejamento ;
      private bool[] T00056_A1733Contratante_SSPrestadoraUnica ;
      private bool[] T00056_n1733Contratante_SSPrestadoraUnica ;
      private bool[] T00056_A1738Contratante_PropostaRqrSrv ;
      private bool[] T00056_n1738Contratante_PropostaRqrSrv ;
      private bool[] T00056_A1739Contratante_PropostaRqrPrz ;
      private bool[] T00056_n1739Contratante_PropostaRqrPrz ;
      private bool[] T00056_A1740Contratante_PropostaRqrEsf ;
      private bool[] T00056_n1740Contratante_PropostaRqrEsf ;
      private bool[] T00056_A1741Contratante_PropostaRqrCnt ;
      private bool[] T00056_n1741Contratante_PropostaRqrCnt ;
      private short[] T00056_A1742Contratante_PropostaNvlCnt ;
      private bool[] T00056_n1742Contratante_PropostaNvlCnt ;
      private bool[] T00056_A1757Contratante_OSGeraOS ;
      private bool[] T00056_n1757Contratante_OSGeraOS ;
      private bool[] T00056_A1758Contratante_OSGeraTRP ;
      private bool[] T00056_n1758Contratante_OSGeraTRP ;
      private bool[] T00056_A1759Contratante_OSGeraTH ;
      private bool[] T00056_n1759Contratante_OSGeraTH ;
      private bool[] T00056_A1760Contratante_OSGeraTRD ;
      private bool[] T00056_n1760Contratante_OSGeraTRD ;
      private bool[] T00056_A1761Contratante_OSGeraTR ;
      private bool[] T00056_n1761Contratante_OSGeraTR ;
      private bool[] T00056_A1803Contratante_OSHmlgComPnd ;
      private bool[] T00056_n1803Contratante_OSHmlgComPnd ;
      private decimal[] T00056_A1805Contratante_AtivoCirculante ;
      private bool[] T00056_n1805Contratante_AtivoCirculante ;
      private decimal[] T00056_A1806Contratante_PassivoCirculante ;
      private bool[] T00056_n1806Contratante_PassivoCirculante ;
      private decimal[] T00056_A1807Contratante_PatrimonioLiquido ;
      private bool[] T00056_n1807Contratante_PatrimonioLiquido ;
      private decimal[] T00056_A1808Contratante_ReceitaBruta ;
      private bool[] T00056_n1808Contratante_ReceitaBruta ;
      private bool[] T00056_A1822Contratante_UsaOSistema ;
      private bool[] T00056_n1822Contratante_UsaOSistema ;
      private String[] T00056_A2034Contratante_TtlRltGerencial ;
      private bool[] T00056_n2034Contratante_TtlRltGerencial ;
      private short[] T00056_A2084Contratante_PrzAoRtr ;
      private bool[] T00056_n2084Contratante_PrzAoRtr ;
      private short[] T00056_A2083Contratante_PrzActRtr ;
      private bool[] T00056_n2083Contratante_PrzActRtr ;
      private bool[] T00056_A2085Contratante_RequerOrigem ;
      private bool[] T00056_n2085Contratante_RequerOrigem ;
      private bool[] T00056_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] T00056_A2090Contratante_ExibePF ;
      private String[] T00056_A1126Contratante_LogoTipoArq ;
      private bool[] T00056_n1126Contratante_LogoTipoArq ;
      private String[] T00056_A1125Contratante_LogoNomeArq ;
      private bool[] T00056_n1125Contratante_LogoNomeArq ;
      private int[] T00056_A25Municipio_Codigo ;
      private bool[] T00056_n25Municipio_Codigo ;
      private int[] T00056_A335Contratante_PessoaCod ;
      private String[] T00056_A23Estado_UF ;
      private String[] T00056_A1124Contratante_LogoArquivo ;
      private bool[] T00056_n1124Contratante_LogoArquivo ;
      private String[] T00057_A26Municipio_Nome ;
      private String[] T00057_A23Estado_UF ;
      private String[] T00058_A12Contratante_CNPJ ;
      private bool[] T00058_n12Contratante_CNPJ ;
      private String[] T00058_A9Contratante_RazaoSocial ;
      private bool[] T00058_n9Contratante_RazaoSocial ;
      private int[] T00059_A29Contratante_Codigo ;
      private bool[] T00059_n29Contratante_Codigo ;
      private int[] T00053_A29Contratante_Codigo ;
      private bool[] T00053_n29Contratante_Codigo ;
      private String[] T00053_A550Contratante_EmailSdaKey ;
      private bool[] T00053_n550Contratante_EmailSdaKey ;
      private String[] T00053_A549Contratante_EmailSdaPass ;
      private bool[] T00053_n549Contratante_EmailSdaPass ;
      private String[] T00053_A10Contratante_NomeFantasia ;
      private String[] T00053_A11Contratante_IE ;
      private String[] T00053_A13Contratante_WebSite ;
      private bool[] T00053_n13Contratante_WebSite ;
      private String[] T00053_A14Contratante_Email ;
      private bool[] T00053_n14Contratante_Email ;
      private String[] T00053_A31Contratante_Telefone ;
      private String[] T00053_A32Contratante_Ramal ;
      private bool[] T00053_n32Contratante_Ramal ;
      private String[] T00053_A33Contratante_Fax ;
      private bool[] T00053_n33Contratante_Fax ;
      private String[] T00053_A336Contratante_AgenciaNome ;
      private bool[] T00053_n336Contratante_AgenciaNome ;
      private String[] T00053_A337Contratante_AgenciaNro ;
      private bool[] T00053_n337Contratante_AgenciaNro ;
      private String[] T00053_A338Contratante_BancoNome ;
      private bool[] T00053_n338Contratante_BancoNome ;
      private String[] T00053_A339Contratante_BancoNro ;
      private bool[] T00053_n339Contratante_BancoNro ;
      private String[] T00053_A16Contratante_ContaCorrente ;
      private bool[] T00053_n16Contratante_ContaCorrente ;
      private bool[] T00053_A30Contratante_Ativo ;
      private String[] T00053_A547Contratante_EmailSdaHost ;
      private bool[] T00053_n547Contratante_EmailSdaHost ;
      private String[] T00053_A548Contratante_EmailSdaUser ;
      private bool[] T00053_n548Contratante_EmailSdaUser ;
      private bool[] T00053_A551Contratante_EmailSdaAut ;
      private bool[] T00053_n551Contratante_EmailSdaAut ;
      private short[] T00053_A552Contratante_EmailSdaPort ;
      private bool[] T00053_n552Contratante_EmailSdaPort ;
      private short[] T00053_A1048Contratante_EmailSdaSec ;
      private bool[] T00053_n1048Contratante_EmailSdaSec ;
      private bool[] T00053_A593Contratante_OSAutomatica ;
      private bool[] T00053_A1652Contratante_SSAutomatica ;
      private bool[] T00053_n1652Contratante_SSAutomatica ;
      private bool[] T00053_A594Contratante_ExisteConferencia ;
      private DateTime[] T00053_A1448Contratante_InicioDoExpediente ;
      private bool[] T00053_n1448Contratante_InicioDoExpediente ;
      private DateTime[] T00053_A1192Contratante_FimDoExpediente ;
      private bool[] T00053_n1192Contratante_FimDoExpediente ;
      private int[] T00053_A1727Contratante_ServicoSSPadrao ;
      private bool[] T00053_n1727Contratante_ServicoSSPadrao ;
      private bool[] T00053_A1729Contratante_RetornaSS ;
      private bool[] T00053_n1729Contratante_RetornaSS ;
      private String[] T00053_A1732Contratante_SSStatusPlanejamento ;
      private bool[] T00053_n1732Contratante_SSStatusPlanejamento ;
      private bool[] T00053_A1733Contratante_SSPrestadoraUnica ;
      private bool[] T00053_n1733Contratante_SSPrestadoraUnica ;
      private bool[] T00053_A1738Contratante_PropostaRqrSrv ;
      private bool[] T00053_n1738Contratante_PropostaRqrSrv ;
      private bool[] T00053_A1739Contratante_PropostaRqrPrz ;
      private bool[] T00053_n1739Contratante_PropostaRqrPrz ;
      private bool[] T00053_A1740Contratante_PropostaRqrEsf ;
      private bool[] T00053_n1740Contratante_PropostaRqrEsf ;
      private bool[] T00053_A1741Contratante_PropostaRqrCnt ;
      private bool[] T00053_n1741Contratante_PropostaRqrCnt ;
      private short[] T00053_A1742Contratante_PropostaNvlCnt ;
      private bool[] T00053_n1742Contratante_PropostaNvlCnt ;
      private bool[] T00053_A1757Contratante_OSGeraOS ;
      private bool[] T00053_n1757Contratante_OSGeraOS ;
      private bool[] T00053_A1758Contratante_OSGeraTRP ;
      private bool[] T00053_n1758Contratante_OSGeraTRP ;
      private bool[] T00053_A1759Contratante_OSGeraTH ;
      private bool[] T00053_n1759Contratante_OSGeraTH ;
      private bool[] T00053_A1760Contratante_OSGeraTRD ;
      private bool[] T00053_n1760Contratante_OSGeraTRD ;
      private bool[] T00053_A1761Contratante_OSGeraTR ;
      private bool[] T00053_n1761Contratante_OSGeraTR ;
      private bool[] T00053_A1803Contratante_OSHmlgComPnd ;
      private bool[] T00053_n1803Contratante_OSHmlgComPnd ;
      private decimal[] T00053_A1805Contratante_AtivoCirculante ;
      private bool[] T00053_n1805Contratante_AtivoCirculante ;
      private decimal[] T00053_A1806Contratante_PassivoCirculante ;
      private bool[] T00053_n1806Contratante_PassivoCirculante ;
      private decimal[] T00053_A1807Contratante_PatrimonioLiquido ;
      private bool[] T00053_n1807Contratante_PatrimonioLiquido ;
      private decimal[] T00053_A1808Contratante_ReceitaBruta ;
      private bool[] T00053_n1808Contratante_ReceitaBruta ;
      private bool[] T00053_A1822Contratante_UsaOSistema ;
      private bool[] T00053_n1822Contratante_UsaOSistema ;
      private String[] T00053_A2034Contratante_TtlRltGerencial ;
      private bool[] T00053_n2034Contratante_TtlRltGerencial ;
      private short[] T00053_A2084Contratante_PrzAoRtr ;
      private bool[] T00053_n2084Contratante_PrzAoRtr ;
      private short[] T00053_A2083Contratante_PrzActRtr ;
      private bool[] T00053_n2083Contratante_PrzActRtr ;
      private bool[] T00053_A2085Contratante_RequerOrigem ;
      private bool[] T00053_n2085Contratante_RequerOrigem ;
      private bool[] T00053_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] T00053_A2090Contratante_ExibePF ;
      private String[] T00053_A1126Contratante_LogoTipoArq ;
      private bool[] T00053_n1126Contratante_LogoTipoArq ;
      private String[] T00053_A1125Contratante_LogoNomeArq ;
      private bool[] T00053_n1125Contratante_LogoNomeArq ;
      private int[] T00053_A25Municipio_Codigo ;
      private bool[] T00053_n25Municipio_Codigo ;
      private int[] T00053_A335Contratante_PessoaCod ;
      private String[] T00053_A1124Contratante_LogoArquivo ;
      private bool[] T00053_n1124Contratante_LogoArquivo ;
      private int[] T000510_A29Contratante_Codigo ;
      private bool[] T000510_n29Contratante_Codigo ;
      private int[] T000511_A29Contratante_Codigo ;
      private bool[] T000511_n29Contratante_Codigo ;
      private int[] T00052_A29Contratante_Codigo ;
      private bool[] T00052_n29Contratante_Codigo ;
      private String[] T00052_A550Contratante_EmailSdaKey ;
      private bool[] T00052_n550Contratante_EmailSdaKey ;
      private String[] T00052_A549Contratante_EmailSdaPass ;
      private bool[] T00052_n549Contratante_EmailSdaPass ;
      private String[] T00052_A10Contratante_NomeFantasia ;
      private String[] T00052_A11Contratante_IE ;
      private String[] T00052_A13Contratante_WebSite ;
      private bool[] T00052_n13Contratante_WebSite ;
      private String[] T00052_A14Contratante_Email ;
      private bool[] T00052_n14Contratante_Email ;
      private String[] T00052_A31Contratante_Telefone ;
      private String[] T00052_A32Contratante_Ramal ;
      private bool[] T00052_n32Contratante_Ramal ;
      private String[] T00052_A33Contratante_Fax ;
      private bool[] T00052_n33Contratante_Fax ;
      private String[] T00052_A336Contratante_AgenciaNome ;
      private bool[] T00052_n336Contratante_AgenciaNome ;
      private String[] T00052_A337Contratante_AgenciaNro ;
      private bool[] T00052_n337Contratante_AgenciaNro ;
      private String[] T00052_A338Contratante_BancoNome ;
      private bool[] T00052_n338Contratante_BancoNome ;
      private String[] T00052_A339Contratante_BancoNro ;
      private bool[] T00052_n339Contratante_BancoNro ;
      private String[] T00052_A16Contratante_ContaCorrente ;
      private bool[] T00052_n16Contratante_ContaCorrente ;
      private bool[] T00052_A30Contratante_Ativo ;
      private String[] T00052_A547Contratante_EmailSdaHost ;
      private bool[] T00052_n547Contratante_EmailSdaHost ;
      private String[] T00052_A548Contratante_EmailSdaUser ;
      private bool[] T00052_n548Contratante_EmailSdaUser ;
      private bool[] T00052_A551Contratante_EmailSdaAut ;
      private bool[] T00052_n551Contratante_EmailSdaAut ;
      private short[] T00052_A552Contratante_EmailSdaPort ;
      private bool[] T00052_n552Contratante_EmailSdaPort ;
      private short[] T00052_A1048Contratante_EmailSdaSec ;
      private bool[] T00052_n1048Contratante_EmailSdaSec ;
      private bool[] T00052_A593Contratante_OSAutomatica ;
      private bool[] T00052_A1652Contratante_SSAutomatica ;
      private bool[] T00052_n1652Contratante_SSAutomatica ;
      private bool[] T00052_A594Contratante_ExisteConferencia ;
      private DateTime[] T00052_A1448Contratante_InicioDoExpediente ;
      private bool[] T00052_n1448Contratante_InicioDoExpediente ;
      private DateTime[] T00052_A1192Contratante_FimDoExpediente ;
      private bool[] T00052_n1192Contratante_FimDoExpediente ;
      private int[] T00052_A1727Contratante_ServicoSSPadrao ;
      private bool[] T00052_n1727Contratante_ServicoSSPadrao ;
      private bool[] T00052_A1729Contratante_RetornaSS ;
      private bool[] T00052_n1729Contratante_RetornaSS ;
      private String[] T00052_A1732Contratante_SSStatusPlanejamento ;
      private bool[] T00052_n1732Contratante_SSStatusPlanejamento ;
      private bool[] T00052_A1733Contratante_SSPrestadoraUnica ;
      private bool[] T00052_n1733Contratante_SSPrestadoraUnica ;
      private bool[] T00052_A1738Contratante_PropostaRqrSrv ;
      private bool[] T00052_n1738Contratante_PropostaRqrSrv ;
      private bool[] T00052_A1739Contratante_PropostaRqrPrz ;
      private bool[] T00052_n1739Contratante_PropostaRqrPrz ;
      private bool[] T00052_A1740Contratante_PropostaRqrEsf ;
      private bool[] T00052_n1740Contratante_PropostaRqrEsf ;
      private bool[] T00052_A1741Contratante_PropostaRqrCnt ;
      private bool[] T00052_n1741Contratante_PropostaRqrCnt ;
      private short[] T00052_A1742Contratante_PropostaNvlCnt ;
      private bool[] T00052_n1742Contratante_PropostaNvlCnt ;
      private bool[] T00052_A1757Contratante_OSGeraOS ;
      private bool[] T00052_n1757Contratante_OSGeraOS ;
      private bool[] T00052_A1758Contratante_OSGeraTRP ;
      private bool[] T00052_n1758Contratante_OSGeraTRP ;
      private bool[] T00052_A1759Contratante_OSGeraTH ;
      private bool[] T00052_n1759Contratante_OSGeraTH ;
      private bool[] T00052_A1760Contratante_OSGeraTRD ;
      private bool[] T00052_n1760Contratante_OSGeraTRD ;
      private bool[] T00052_A1761Contratante_OSGeraTR ;
      private bool[] T00052_n1761Contratante_OSGeraTR ;
      private bool[] T00052_A1803Contratante_OSHmlgComPnd ;
      private bool[] T00052_n1803Contratante_OSHmlgComPnd ;
      private decimal[] T00052_A1805Contratante_AtivoCirculante ;
      private bool[] T00052_n1805Contratante_AtivoCirculante ;
      private decimal[] T00052_A1806Contratante_PassivoCirculante ;
      private bool[] T00052_n1806Contratante_PassivoCirculante ;
      private decimal[] T00052_A1807Contratante_PatrimonioLiquido ;
      private bool[] T00052_n1807Contratante_PatrimonioLiquido ;
      private decimal[] T00052_A1808Contratante_ReceitaBruta ;
      private bool[] T00052_n1808Contratante_ReceitaBruta ;
      private bool[] T00052_A1822Contratante_UsaOSistema ;
      private bool[] T00052_n1822Contratante_UsaOSistema ;
      private String[] T00052_A2034Contratante_TtlRltGerencial ;
      private bool[] T00052_n2034Contratante_TtlRltGerencial ;
      private short[] T00052_A2084Contratante_PrzAoRtr ;
      private bool[] T00052_n2084Contratante_PrzAoRtr ;
      private short[] T00052_A2083Contratante_PrzActRtr ;
      private bool[] T00052_n2083Contratante_PrzActRtr ;
      private bool[] T00052_A2085Contratante_RequerOrigem ;
      private bool[] T00052_n2085Contratante_RequerOrigem ;
      private bool[] T00052_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] T00052_A2090Contratante_ExibePF ;
      private String[] T00052_A1126Contratante_LogoTipoArq ;
      private bool[] T00052_n1126Contratante_LogoTipoArq ;
      private String[] T00052_A1125Contratante_LogoNomeArq ;
      private bool[] T00052_n1125Contratante_LogoNomeArq ;
      private int[] T00052_A25Municipio_Codigo ;
      private bool[] T00052_n25Municipio_Codigo ;
      private int[] T00052_A335Contratante_PessoaCod ;
      private String[] T00052_A1124Contratante_LogoArquivo ;
      private bool[] T00052_n1124Contratante_LogoArquivo ;
      private int[] T000512_A29Contratante_Codigo ;
      private bool[] T000512_n29Contratante_Codigo ;
      private String[] T000516_A26Municipio_Nome ;
      private String[] T000516_A23Estado_UF ;
      private String[] T000517_A12Contratante_CNPJ ;
      private bool[] T000517_n12Contratante_CNPJ ;
      private String[] T000517_A9Contratante_RazaoSocial ;
      private bool[] T000517_n9Contratante_RazaoSocial ;
      private int[] T000518_A1380Redmine_Codigo ;
      private int[] T000519_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000519_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000520_A5AreaTrabalho_Codigo ;
      private int[] T000521_A29Contratante_Codigo ;
      private bool[] T000521_n29Contratante_Codigo ;
      private String[] T000522_A23Estado_UF ;
      private String[] T000522_A24Estado_Nome ;
      private int[] T000523_A25Municipio_Codigo ;
      private bool[] T000523_n25Municipio_Codigo ;
      private String[] T000523_A26Municipio_Nome ;
      private String[] T000523_A23Estado_UF ;
      private int[] T000524_A155Servico_Codigo ;
      private String[] T000524_A605Servico_Sigla ;
      private bool[] T000524_A1635Servico_IsPublico ;
      private String[] T000525_A26Municipio_Nome ;
      private String[] T000525_A23Estado_UF ;
      private GeneXus.Mail.GXMailMessage AV39Email ;
      private GeneXus.Mail.GXMailRecipient AV19MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV41SMTPSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV34Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35Attachments ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV29AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00056 ;
          prmT00056 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT00056 ;
          cmdBufferT00056=" SELECT TM1.[Contratante_Codigo], TM1.[Contratante_EmailSdaKey], TM1.[Contratante_EmailSdaPass], T2.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Pessoa_Nome] AS Contratante_RazaoSocial, TM1.[Contratante_NomeFantasia], TM1.[Contratante_IE], TM1.[Contratante_WebSite], TM1.[Contratante_Email], TM1.[Contratante_Telefone], TM1.[Contratante_Ramal], TM1.[Contratante_Fax], TM1.[Contratante_AgenciaNome], TM1.[Contratante_AgenciaNro], TM1.[Contratante_BancoNome], TM1.[Contratante_BancoNro], TM1.[Contratante_ContaCorrente], T3.[Municipio_Nome], TM1.[Contratante_Ativo], TM1.[Contratante_EmailSdaHost], TM1.[Contratante_EmailSdaUser], TM1.[Contratante_EmailSdaAut], TM1.[Contratante_EmailSdaPort], TM1.[Contratante_EmailSdaSec], TM1.[Contratante_OSAutomatica], TM1.[Contratante_SSAutomatica], TM1.[Contratante_ExisteConferencia], TM1.[Contratante_InicioDoExpediente], TM1.[Contratante_FimDoExpediente], TM1.[Contratante_ServicoSSPadrao], TM1.[Contratante_RetornaSS], TM1.[Contratante_SSStatusPlanejamento], TM1.[Contratante_SSPrestadoraUnica], TM1.[Contratante_PropostaRqrSrv], TM1.[Contratante_PropostaRqrPrz], TM1.[Contratante_PropostaRqrEsf], TM1.[Contratante_PropostaRqrCnt], TM1.[Contratante_PropostaNvlCnt], TM1.[Contratante_OSGeraOS], TM1.[Contratante_OSGeraTRP], TM1.[Contratante_OSGeraTH], TM1.[Contratante_OSGeraTRD], TM1.[Contratante_OSGeraTR], TM1.[Contratante_OSHmlgComPnd], TM1.[Contratante_AtivoCirculante], TM1.[Contratante_PassivoCirculante], TM1.[Contratante_PatrimonioLiquido], TM1.[Contratante_ReceitaBruta], TM1.[Contratante_UsaOSistema], TM1.[Contratante_TtlRltGerencial], TM1.[Contratante_PrzAoRtr], TM1.[Contratante_PrzActRtr], TM1.[Contratante_RequerOrigem], TM1.[Contratante_SelecionaResponsavelOS], TM1.[Contratante_ExibePF], TM1.[Contratante_LogoTipoArq], TM1.[Contratante_LogoNomeArq], "
          + " TM1.[Municipio_Codigo], TM1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T3.[Estado_UF], TM1.[Contratante_LogoArquivo] FROM (([Contratante] TM1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = TM1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = TM1.[Municipio_Codigo]) WHERE TM1.[Contratante_Codigo] = @Contratante_Codigo ORDER BY TM1.[Contratante_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT00054 ;
          prmT00054 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00055 ;
          prmT00055 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00057 ;
          prmT00057 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00058 ;
          prmT00058 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00059 ;
          prmT00059 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00053 ;
          prmT00053 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000510 ;
          prmT000510 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000511 ;
          prmT000511 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00052 ;
          prmT00052 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000512 ;
          prmT000512 = new Object[] {
          new Object[] {"@Contratante_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@Contratante_EmailSdaPass",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_NomeFantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@Contratante_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratante_WebSite",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_Ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Fax",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratante_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExisteConferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratante_InicioDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_FimDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_ServicoSSPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_RetornaSS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSStatusPlanejamento",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratante_SSPrestadoraUnica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrSrv",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrPrz",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrEsf",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrCnt",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaNvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSGeraOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRP",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTH",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRD",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTR",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSHmlgComPnd",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_AtivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PassivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PatrimonioLiquido",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_ReceitaBruta",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_TtlRltGerencial",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_PrzAoRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_PrzActRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_RequerOrigem",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SelecionaResponsavelOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExibePF",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000512 ;
          cmdBufferT000512=" INSERT INTO [Contratante]([Contratante_EmailSdaKey], [Contratante_EmailSdaPass], [Contratante_NomeFantasia], [Contratante_IE], [Contratante_WebSite], [Contratante_Email], [Contratante_Telefone], [Contratante_Ramal], [Contratante_Fax], [Contratante_AgenciaNome], [Contratante_AgenciaNro], [Contratante_BancoNome], [Contratante_BancoNro], [Contratante_ContaCorrente], [Contratante_Ativo], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Contratante_OSAutomatica], [Contratante_SSAutomatica], [Contratante_ExisteConferencia], [Contratante_LogoArquivo], [Contratante_InicioDoExpediente], [Contratante_FimDoExpediente], [Contratante_ServicoSSPadrao], [Contratante_RetornaSS], [Contratante_SSStatusPlanejamento], [Contratante_SSPrestadoraUnica], [Contratante_PropostaRqrSrv], [Contratante_PropostaRqrPrz], [Contratante_PropostaRqrEsf], [Contratante_PropostaRqrCnt], [Contratante_PropostaNvlCnt], [Contratante_OSGeraOS], [Contratante_OSGeraTRP], [Contratante_OSGeraTH], [Contratante_OSGeraTRD], [Contratante_OSGeraTR], [Contratante_OSHmlgComPnd], [Contratante_AtivoCirculante], [Contratante_PassivoCirculante], [Contratante_PatrimonioLiquido], [Contratante_ReceitaBruta], [Contratante_UsaOSistema], [Contratante_TtlRltGerencial], [Contratante_PrzAoRtr], [Contratante_PrzActRtr], [Contratante_RequerOrigem], [Contratante_SelecionaResponsavelOS], [Contratante_ExibePF], [Contratante_LogoTipoArq], [Contratante_LogoNomeArq], [Municipio_Codigo], [Contratante_PessoaCod]) VALUES(@Contratante_EmailSdaKey, @Contratante_EmailSdaPass, @Contratante_NomeFantasia, @Contratante_IE, @Contratante_WebSite, @Contratante_Email, @Contratante_Telefone, @Contratante_Ramal, @Contratante_Fax, @Contratante_AgenciaNome, @Contratante_AgenciaNro, "
          + " @Contratante_BancoNome, @Contratante_BancoNro, @Contratante_ContaCorrente, @Contratante_Ativo, @Contratante_EmailSdaHost, @Contratante_EmailSdaUser, @Contratante_EmailSdaAut, @Contratante_EmailSdaPort, @Contratante_EmailSdaSec, @Contratante_OSAutomatica, @Contratante_SSAutomatica, @Contratante_ExisteConferencia, @Contratante_LogoArquivo, @Contratante_InicioDoExpediente, @Contratante_FimDoExpediente, @Contratante_ServicoSSPadrao, @Contratante_RetornaSS, @Contratante_SSStatusPlanejamento, @Contratante_SSPrestadoraUnica, @Contratante_PropostaRqrSrv, @Contratante_PropostaRqrPrz, @Contratante_PropostaRqrEsf, @Contratante_PropostaRqrCnt, @Contratante_PropostaNvlCnt, @Contratante_OSGeraOS, @Contratante_OSGeraTRP, @Contratante_OSGeraTH, @Contratante_OSGeraTRD, @Contratante_OSGeraTR, @Contratante_OSHmlgComPnd, @Contratante_AtivoCirculante, @Contratante_PassivoCirculante, @Contratante_PatrimonioLiquido, @Contratante_ReceitaBruta, @Contratante_UsaOSistema, @Contratante_TtlRltGerencial, @Contratante_PrzAoRtr, @Contratante_PrzActRtr, @Contratante_RequerOrigem, @Contratante_SelecionaResponsavelOS, @Contratante_ExibePF, @Contratante_LogoTipoArq, @Contratante_LogoNomeArq, @Municipio_Codigo, @Contratante_PessoaCod); SELECT SCOPE_IDENTITY()" ;
          Object[] prmT000513 ;
          prmT000513 = new Object[] {
          new Object[] {"@Contratante_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@Contratante_EmailSdaPass",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_NomeFantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@Contratante_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratante_WebSite",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_Ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Fax",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratante_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExisteConferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_InicioDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_FimDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_ServicoSSPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_RetornaSS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSStatusPlanejamento",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratante_SSPrestadoraUnica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrSrv",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrPrz",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrEsf",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrCnt",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaNvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSGeraOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRP",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTH",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRD",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTR",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSHmlgComPnd",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_AtivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PassivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PatrimonioLiquido",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_ReceitaBruta",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_TtlRltGerencial",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_PrzAoRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_PrzActRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_RequerOrigem",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SelecionaResponsavelOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExibePF",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT000513 ;
          cmdBufferT000513=" UPDATE [Contratante] SET [Contratante_EmailSdaKey]=@Contratante_EmailSdaKey, [Contratante_EmailSdaPass]=@Contratante_EmailSdaPass, [Contratante_NomeFantasia]=@Contratante_NomeFantasia, [Contratante_IE]=@Contratante_IE, [Contratante_WebSite]=@Contratante_WebSite, [Contratante_Email]=@Contratante_Email, [Contratante_Telefone]=@Contratante_Telefone, [Contratante_Ramal]=@Contratante_Ramal, [Contratante_Fax]=@Contratante_Fax, [Contratante_AgenciaNome]=@Contratante_AgenciaNome, [Contratante_AgenciaNro]=@Contratante_AgenciaNro, [Contratante_BancoNome]=@Contratante_BancoNome, [Contratante_BancoNro]=@Contratante_BancoNro, [Contratante_ContaCorrente]=@Contratante_ContaCorrente, [Contratante_Ativo]=@Contratante_Ativo, [Contratante_EmailSdaHost]=@Contratante_EmailSdaHost, [Contratante_EmailSdaUser]=@Contratante_EmailSdaUser, [Contratante_EmailSdaAut]=@Contratante_EmailSdaAut, [Contratante_EmailSdaPort]=@Contratante_EmailSdaPort, [Contratante_EmailSdaSec]=@Contratante_EmailSdaSec, [Contratante_OSAutomatica]=@Contratante_OSAutomatica, [Contratante_SSAutomatica]=@Contratante_SSAutomatica, [Contratante_ExisteConferencia]=@Contratante_ExisteConferencia, [Contratante_InicioDoExpediente]=@Contratante_InicioDoExpediente, [Contratante_FimDoExpediente]=@Contratante_FimDoExpediente, [Contratante_ServicoSSPadrao]=@Contratante_ServicoSSPadrao, [Contratante_RetornaSS]=@Contratante_RetornaSS, [Contratante_SSStatusPlanejamento]=@Contratante_SSStatusPlanejamento, [Contratante_SSPrestadoraUnica]=@Contratante_SSPrestadoraUnica, [Contratante_PropostaRqrSrv]=@Contratante_PropostaRqrSrv, [Contratante_PropostaRqrPrz]=@Contratante_PropostaRqrPrz, [Contratante_PropostaRqrEsf]=@Contratante_PropostaRqrEsf, [Contratante_PropostaRqrCnt]=@Contratante_PropostaRqrCnt, [Contratante_PropostaNvlCnt]=@Contratante_PropostaNvlCnt, "
          + " [Contratante_OSGeraOS]=@Contratante_OSGeraOS, [Contratante_OSGeraTRP]=@Contratante_OSGeraTRP, [Contratante_OSGeraTH]=@Contratante_OSGeraTH, [Contratante_OSGeraTRD]=@Contratante_OSGeraTRD, [Contratante_OSGeraTR]=@Contratante_OSGeraTR, [Contratante_OSHmlgComPnd]=@Contratante_OSHmlgComPnd, [Contratante_AtivoCirculante]=@Contratante_AtivoCirculante, [Contratante_PassivoCirculante]=@Contratante_PassivoCirculante, [Contratante_PatrimonioLiquido]=@Contratante_PatrimonioLiquido, [Contratante_ReceitaBruta]=@Contratante_ReceitaBruta, [Contratante_UsaOSistema]=@Contratante_UsaOSistema, [Contratante_TtlRltGerencial]=@Contratante_TtlRltGerencial, [Contratante_PrzAoRtr]=@Contratante_PrzAoRtr, [Contratante_PrzActRtr]=@Contratante_PrzActRtr, [Contratante_RequerOrigem]=@Contratante_RequerOrigem, [Contratante_SelecionaResponsavelOS]=@Contratante_SelecionaResponsavelOS, [Contratante_ExibePF]=@Contratante_ExibePF, [Contratante_LogoTipoArq]=@Contratante_LogoTipoArq, [Contratante_LogoNomeArq]=@Contratante_LogoNomeArq, [Municipio_Codigo]=@Municipio_Codigo, [Contratante_PessoaCod]=@Contratante_PessoaCod  WHERE [Contratante_Codigo] = @Contratante_Codigo" ;
          Object[] prmT000514 ;
          prmT000514 = new Object[] {
          new Object[] {"@Contratante_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000515 ;
          prmT000515 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000516 ;
          prmT000516 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000517 ;
          prmT000517 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000518 ;
          prmT000518 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000519 ;
          prmT000519 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000520 ;
          prmT000520 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000521 ;
          prmT000521 = new Object[] {
          } ;
          Object[] prmT000522 ;
          prmT000522 = new Object[] {
          } ;
          Object[] prmT000523 ;
          prmT000523 = new Object[] {
          new Object[] {"@AV15Estado_UF",SqlDbType.Char,2,0}
          } ;
          Object[] prmT000524 ;
          prmT000524 = new Object[] {
          } ;
          Object[] prmT000525 ;
          prmT000525 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00052", "SELECT [Contratante_Codigo], [Contratante_EmailSdaKey], [Contratante_EmailSdaPass], [Contratante_NomeFantasia], [Contratante_IE], [Contratante_WebSite], [Contratante_Email], [Contratante_Telefone], [Contratante_Ramal], [Contratante_Fax], [Contratante_AgenciaNome], [Contratante_AgenciaNro], [Contratante_BancoNome], [Contratante_BancoNro], [Contratante_ContaCorrente], [Contratante_Ativo], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Contratante_OSAutomatica], [Contratante_SSAutomatica], [Contratante_ExisteConferencia], [Contratante_InicioDoExpediente], [Contratante_FimDoExpediente], [Contratante_ServicoSSPadrao], [Contratante_RetornaSS], [Contratante_SSStatusPlanejamento], [Contratante_SSPrestadoraUnica], [Contratante_PropostaRqrSrv], [Contratante_PropostaRqrPrz], [Contratante_PropostaRqrEsf], [Contratante_PropostaRqrCnt], [Contratante_PropostaNvlCnt], [Contratante_OSGeraOS], [Contratante_OSGeraTRP], [Contratante_OSGeraTH], [Contratante_OSGeraTRD], [Contratante_OSGeraTR], [Contratante_OSHmlgComPnd], [Contratante_AtivoCirculante], [Contratante_PassivoCirculante], [Contratante_PatrimonioLiquido], [Contratante_ReceitaBruta], [Contratante_UsaOSistema], [Contratante_TtlRltGerencial], [Contratante_PrzAoRtr], [Contratante_PrzActRtr], [Contratante_RequerOrigem], [Contratante_SelecionaResponsavelOS], [Contratante_ExibePF], [Contratante_LogoTipoArq], [Contratante_LogoNomeArq], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod, [Contratante_LogoArquivo] FROM [Contratante] WITH (UPDLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00052,1,0,true,false )
             ,new CursorDef("T00053", "SELECT [Contratante_Codigo], [Contratante_EmailSdaKey], [Contratante_EmailSdaPass], [Contratante_NomeFantasia], [Contratante_IE], [Contratante_WebSite], [Contratante_Email], [Contratante_Telefone], [Contratante_Ramal], [Contratante_Fax], [Contratante_AgenciaNome], [Contratante_AgenciaNro], [Contratante_BancoNome], [Contratante_BancoNro], [Contratante_ContaCorrente], [Contratante_Ativo], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Contratante_OSAutomatica], [Contratante_SSAutomatica], [Contratante_ExisteConferencia], [Contratante_InicioDoExpediente], [Contratante_FimDoExpediente], [Contratante_ServicoSSPadrao], [Contratante_RetornaSS], [Contratante_SSStatusPlanejamento], [Contratante_SSPrestadoraUnica], [Contratante_PropostaRqrSrv], [Contratante_PropostaRqrPrz], [Contratante_PropostaRqrEsf], [Contratante_PropostaRqrCnt], [Contratante_PropostaNvlCnt], [Contratante_OSGeraOS], [Contratante_OSGeraTRP], [Contratante_OSGeraTH], [Contratante_OSGeraTRD], [Contratante_OSGeraTR], [Contratante_OSHmlgComPnd], [Contratante_AtivoCirculante], [Contratante_PassivoCirculante], [Contratante_PatrimonioLiquido], [Contratante_ReceitaBruta], [Contratante_UsaOSistema], [Contratante_TtlRltGerencial], [Contratante_PrzAoRtr], [Contratante_PrzActRtr], [Contratante_RequerOrigem], [Contratante_SelecionaResponsavelOS], [Contratante_ExibePF], [Contratante_LogoTipoArq], [Contratante_LogoNomeArq], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod, [Contratante_LogoArquivo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00053,1,0,true,false )
             ,new CursorDef("T00054", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00054,1,0,true,false )
             ,new CursorDef("T00055", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00055,1,0,true,false )
             ,new CursorDef("T00056", cmdBufferT00056,true, GxErrorMask.GX_NOMASK, false, this,prmT00056,100,0,true,false )
             ,new CursorDef("T00057", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00057,1,0,true,false )
             ,new CursorDef("T00058", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00058,1,0,true,false )
             ,new CursorDef("T00059", "SELECT [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00059,1,0,true,false )
             ,new CursorDef("T000510", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE ( [Contratante_Codigo] > @Contratante_Codigo) ORDER BY [Contratante_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000510,1,0,true,true )
             ,new CursorDef("T000511", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE ( [Contratante_Codigo] < @Contratante_Codigo) ORDER BY [Contratante_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000511,1,0,true,true )
             ,new CursorDef("T000512", cmdBufferT000512, GxErrorMask.GX_NOMASK,prmT000512)
             ,new CursorDef("T000513", cmdBufferT000513, GxErrorMask.GX_NOMASK,prmT000513)
             ,new CursorDef("T000514", "UPDATE [Contratante] SET [Contratante_LogoArquivo]=@Contratante_LogoArquivo  WHERE [Contratante_Codigo] = @Contratante_Codigo", GxErrorMask.GX_NOMASK,prmT000514)
             ,new CursorDef("T000515", "DELETE FROM [Contratante]  WHERE [Contratante_Codigo] = @Contratante_Codigo", GxErrorMask.GX_NOMASK,prmT000515)
             ,new CursorDef("T000516", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000516,1,0,true,false )
             ,new CursorDef("T000517", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000517,1,0,true,false )
             ,new CursorDef("T000518", "SELECT TOP 1 [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000518,1,0,true,true )
             ,new CursorDef("T000519", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000519,1,0,true,true )
             ,new CursorDef("T000520", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000520,1,0,true,true )
             ,new CursorDef("T000521", "SELECT [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) ORDER BY [Contratante_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000521,100,0,true,false )
             ,new CursorDef("T000522", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000522,0,0,true,false )
             ,new CursorDef("T000523", "SELECT [Municipio_Codigo], [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Estado_UF] = @AV15Estado_UF ORDER BY [Municipio_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000523,0,0,true,false )
             ,new CursorDef("T000524", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_IsPublico] FROM [Servico] WITH (NOLOCK) WHERE [Servico_IsPublico] = 1 ORDER BY [Servico_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000524,0,0,true,false )
             ,new CursorDef("T000525", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000525,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((bool[]) buf[26])[0] = rslt.getBool(16) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((short[]) buf[33])[0] = rslt.getShort(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((short[]) buf[35])[0] = rslt.getShort(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((bool[]) buf[37])[0] = rslt.getBool(22) ;
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((bool[]) buf[40])[0] = rslt.getBool(24) ;
                ((DateTime[]) buf[41])[0] = rslt.getGXDateTime(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[43])[0] = rslt.getGXDateTime(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((int[]) buf[45])[0] = rslt.getInt(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((bool[]) buf[47])[0] = rslt.getBool(28) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((bool[]) buf[51])[0] = rslt.getBool(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((bool[]) buf[53])[0] = rslt.getBool(31) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(31);
                ((bool[]) buf[55])[0] = rslt.getBool(32) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(32);
                ((bool[]) buf[57])[0] = rslt.getBool(33) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(33);
                ((bool[]) buf[59])[0] = rslt.getBool(34) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(34);
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(35);
                ((bool[]) buf[63])[0] = rslt.getBool(36) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(36);
                ((bool[]) buf[65])[0] = rslt.getBool(37) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(37);
                ((bool[]) buf[67])[0] = rslt.getBool(38) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(38);
                ((bool[]) buf[69])[0] = rslt.getBool(39) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(39);
                ((bool[]) buf[71])[0] = rslt.getBool(40) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(40);
                ((bool[]) buf[73])[0] = rslt.getBool(41) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(41);
                ((decimal[]) buf[75])[0] = rslt.getDecimal(42) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(42);
                ((decimal[]) buf[77])[0] = rslt.getDecimal(43) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(43);
                ((decimal[]) buf[79])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(44);
                ((decimal[]) buf[81])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(45);
                ((bool[]) buf[83])[0] = rslt.getBool(46) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(46);
                ((String[]) buf[85])[0] = rslt.getVarchar(47) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(47);
                ((short[]) buf[87])[0] = rslt.getShort(48) ;
                ((bool[]) buf[88])[0] = rslt.wasNull(48);
                ((short[]) buf[89])[0] = rslt.getShort(49) ;
                ((bool[]) buf[90])[0] = rslt.wasNull(49);
                ((bool[]) buf[91])[0] = rslt.getBool(50) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(50);
                ((bool[]) buf[93])[0] = rslt.getBool(51) ;
                ((bool[]) buf[94])[0] = rslt.getBool(52) ;
                ((String[]) buf[95])[0] = rslt.getString(53, 10) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(53);
                ((String[]) buf[97])[0] = rslt.getString(54, 50) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(54);
                ((int[]) buf[99])[0] = rslt.getInt(55) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(55);
                ((int[]) buf[101])[0] = rslt.getInt(56) ;
                ((String[]) buf[102])[0] = rslt.getBLOBFile(57, rslt.getString(53, 10), rslt.getString(54, 50)) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((bool[]) buf[26])[0] = rslt.getBool(16) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((short[]) buf[33])[0] = rslt.getShort(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((short[]) buf[35])[0] = rslt.getShort(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((bool[]) buf[37])[0] = rslt.getBool(22) ;
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((bool[]) buf[40])[0] = rslt.getBool(24) ;
                ((DateTime[]) buf[41])[0] = rslt.getGXDateTime(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[43])[0] = rslt.getGXDateTime(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((int[]) buf[45])[0] = rslt.getInt(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((bool[]) buf[47])[0] = rslt.getBool(28) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((bool[]) buf[51])[0] = rslt.getBool(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((bool[]) buf[53])[0] = rslt.getBool(31) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(31);
                ((bool[]) buf[55])[0] = rslt.getBool(32) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(32);
                ((bool[]) buf[57])[0] = rslt.getBool(33) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(33);
                ((bool[]) buf[59])[0] = rslt.getBool(34) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(34);
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(35);
                ((bool[]) buf[63])[0] = rslt.getBool(36) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(36);
                ((bool[]) buf[65])[0] = rslt.getBool(37) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(37);
                ((bool[]) buf[67])[0] = rslt.getBool(38) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(38);
                ((bool[]) buf[69])[0] = rslt.getBool(39) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(39);
                ((bool[]) buf[71])[0] = rslt.getBool(40) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(40);
                ((bool[]) buf[73])[0] = rslt.getBool(41) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(41);
                ((decimal[]) buf[75])[0] = rslt.getDecimal(42) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(42);
                ((decimal[]) buf[77])[0] = rslt.getDecimal(43) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(43);
                ((decimal[]) buf[79])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(44);
                ((decimal[]) buf[81])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(45);
                ((bool[]) buf[83])[0] = rslt.getBool(46) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(46);
                ((String[]) buf[85])[0] = rslt.getVarchar(47) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(47);
                ((short[]) buf[87])[0] = rslt.getShort(48) ;
                ((bool[]) buf[88])[0] = rslt.wasNull(48);
                ((short[]) buf[89])[0] = rslt.getShort(49) ;
                ((bool[]) buf[90])[0] = rslt.wasNull(49);
                ((bool[]) buf[91])[0] = rslt.getBool(50) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(50);
                ((bool[]) buf[93])[0] = rslt.getBool(51) ;
                ((bool[]) buf[94])[0] = rslt.getBool(52) ;
                ((String[]) buf[95])[0] = rslt.getString(53, 10) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(53);
                ((String[]) buf[97])[0] = rslt.getString(54, 50) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(54);
                ((int[]) buf[99])[0] = rslt.getInt(55) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(55);
                ((int[]) buf[101])[0] = rslt.getInt(56) ;
                ((String[]) buf[102])[0] = rslt.getBLOBFile(57, rslt.getString(53, 10), rslt.getString(54, 50)) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[16])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 10) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getString(16, 6) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((String[]) buf[32])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((String[]) buf[34])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((bool[]) buf[36])[0] = rslt.getBool(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((short[]) buf[38])[0] = rslt.getShort(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((short[]) buf[40])[0] = rslt.getShort(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((bool[]) buf[43])[0] = rslt.getBool(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((bool[]) buf[45])[0] = rslt.getBool(27) ;
                ((DateTime[]) buf[46])[0] = rslt.getGXDateTime(28) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(28);
                ((DateTime[]) buf[48])[0] = rslt.getGXDateTime(29) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(29);
                ((int[]) buf[50])[0] = rslt.getInt(30) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(30);
                ((bool[]) buf[52])[0] = rslt.getBool(31) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(31);
                ((String[]) buf[54])[0] = rslt.getString(32, 1) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(32);
                ((bool[]) buf[56])[0] = rslt.getBool(33) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(33);
                ((bool[]) buf[58])[0] = rslt.getBool(34) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(34);
                ((bool[]) buf[60])[0] = rslt.getBool(35) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(35);
                ((bool[]) buf[62])[0] = rslt.getBool(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                ((bool[]) buf[64])[0] = rslt.getBool(37) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(37);
                ((short[]) buf[66])[0] = rslt.getShort(38) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(38);
                ((bool[]) buf[68])[0] = rslt.getBool(39) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(39);
                ((bool[]) buf[70])[0] = rslt.getBool(40) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(40);
                ((bool[]) buf[72])[0] = rslt.getBool(41) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(41);
                ((bool[]) buf[74])[0] = rslt.getBool(42) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(42);
                ((bool[]) buf[76])[0] = rslt.getBool(43) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(43);
                ((bool[]) buf[78])[0] = rslt.getBool(44) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(44);
                ((decimal[]) buf[80])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(45);
                ((decimal[]) buf[82])[0] = rslt.getDecimal(46) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(46);
                ((decimal[]) buf[84])[0] = rslt.getDecimal(47) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(47);
                ((decimal[]) buf[86])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(48);
                ((bool[]) buf[88])[0] = rslt.getBool(49) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(49);
                ((String[]) buf[90])[0] = rslt.getVarchar(50) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(50);
                ((short[]) buf[92])[0] = rslt.getShort(51) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(51);
                ((short[]) buf[94])[0] = rslt.getShort(52) ;
                ((bool[]) buf[95])[0] = rslt.wasNull(52);
                ((bool[]) buf[96])[0] = rslt.getBool(53) ;
                ((bool[]) buf[97])[0] = rslt.wasNull(53);
                ((bool[]) buf[98])[0] = rslt.getBool(54) ;
                ((bool[]) buf[99])[0] = rslt.getBool(55) ;
                ((String[]) buf[100])[0] = rslt.getString(56, 10) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(56);
                ((String[]) buf[102])[0] = rslt.getString(57, 50) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                ((int[]) buf[104])[0] = rslt.getInt(58) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(58);
                ((int[]) buf[106])[0] = rslt.getInt(59) ;
                ((String[]) buf[107])[0] = rslt.getString(60, 2) ;
                ((String[]) buf[108])[0] = rslt.getBLOBFile(61, rslt.getString(56, 10), rslt.getString(57, 50)) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (String)parms[4]);
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                stmt.SetParameter(7, (String)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (bool)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[35]);
                }
                stmt.SetParameter(21, (bool)parms[36]);
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[38]);
                }
                stmt.SetParameter(23, (bool)parms[39]);
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 24 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 25 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(25, (DateTime)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 26 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(26, (DateTime)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 28 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(28, (bool)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 29 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(29, (String)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 30 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(30, (bool)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 31 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(31, (bool)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 32 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(32, (bool)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 33 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(33, (bool)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 34 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(34, (bool)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 35 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(35, (short)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 36 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(36, (bool)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 37 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(37, (bool)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 38 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(38, (bool)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 39 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(39, (bool)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 40 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(40, (bool)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 41 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(41, (bool)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 42 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(42, (decimal)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 43 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(43, (decimal)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 44 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(44, (decimal)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 45 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(45, (decimal)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 46 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(46, (bool)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 47 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(47, (String)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 48 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(48, (short)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 49 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(49, (short)parms[91]);
                }
                if ( (bool)parms[92] )
                {
                   stmt.setNull( 50 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(50, (bool)parms[93]);
                }
                stmt.SetParameter(51, (bool)parms[94]);
                stmt.SetParameter(52, (bool)parms[95]);
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 53 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(53, (String)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 54 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(54, (String)parms[99]);
                }
                if ( (bool)parms[100] )
                {
                   stmt.setNull( 55 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(55, (int)parms[101]);
                }
                stmt.SetParameter(56, (int)parms[102]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (String)parms[4]);
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                stmt.SetParameter(7, (String)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (bool)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[35]);
                }
                stmt.SetParameter(21, (bool)parms[36]);
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[38]);
                }
                stmt.SetParameter(23, (bool)parms[39]);
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 24 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(24, (DateTime)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 25 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(25, (DateTime)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 27 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(27, (bool)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 28 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(28, (String)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 29 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(29, (bool)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 30 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(30, (bool)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 31 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(31, (bool)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 32 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(32, (bool)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 33 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(33, (bool)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 34 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(34, (short)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 35 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(35, (bool)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 36 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(36, (bool)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 37 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(37, (bool)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 38 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(38, (bool)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 39 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(39, (bool)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 40 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(40, (bool)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 41 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(41, (decimal)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 42 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(42, (decimal)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 43 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(43, (decimal)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 44 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(44, (decimal)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 45 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(45, (bool)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 46 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(46, (String)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 47 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(47, (short)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 48 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(48, (short)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 49 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(49, (bool)parms[91]);
                }
                stmt.SetParameter(50, (bool)parms[92]);
                stmt.SetParameter(51, (bool)parms[93]);
                if ( (bool)parms[94] )
                {
                   stmt.setNull( 52 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(52, (String)parms[95]);
                }
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 53 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(53, (String)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 54 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(54, (int)parms[99]);
                }
                stmt.SetParameter(55, (int)parms[100]);
                if ( (bool)parms[101] )
                {
                   stmt.setNull( 56 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(56, (int)parms[102]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
