/*
               File: Geral_Cargo
        Description: Cargos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:8.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_cargo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCARGO_UOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvCARGO_UOCOD2582( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A615GrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A615GrupoCargo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A1002Cargo_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1002Cargo_UOCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A1002Cargo_UOCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Cargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Cargo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCARGO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Cargo_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynGrupoCargo_Codigo.Name = "GRUPOCARGO_CODIGO";
         dynGrupoCargo_Codigo.WebTags = "";
         dynGrupoCargo_Codigo.removeAllItems();
         /* Using cursor T00256 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynGrupoCargo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T00256_A615GrupoCargo_Codigo[0]), 6, 0)), T00256_A616GrupoCargo_Nome[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynGrupoCargo_Codigo.ItemCount > 0 )
         {
            A615GrupoCargo_Codigo = (int)(NumberUtil.Val( dynGrupoCargo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         }
         dynavCargo_uocod.Name = "vCARGO_UOCOD";
         dynavCargo_uocod.WebTags = "";
         chkCargo_Ativo.Name = "CARGO_ATIVO";
         chkCargo_Ativo.WebTags = "";
         chkCargo_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCargo_Ativo_Internalname, "TitleCaption", chkCargo_Ativo.Caption);
         chkCargo_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Cargos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public geral_cargo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public geral_cargo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Cargo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Cargo_Codigo = aP1_Cargo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynGrupoCargo_Codigo = new GXCombobox();
         dynavCargo_uocod = new GXCombobox();
         chkCargo_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynGrupoCargo_Codigo.ItemCount > 0 )
         {
            A615GrupoCargo_Codigo = (int)(NumberUtil.Val( dynGrupoCargo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         }
         if ( dynavCargo_uocod.ItemCount > 0 )
         {
            AV14Cargo_UOCod = (int)(NumberUtil.Val( dynavCargo_uocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2582( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2582e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCargo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ",", "")), ((edtCargo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCargo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtCargo_Codigo_Visible, edtCargo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_Cargo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2582( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2582( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2582e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_2582( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_2582e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2582e( true) ;
         }
         else
         {
            wb_table1_2_2582e( false) ;
         }
      }

      protected void wb_table3_41_2582( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_2582e( true) ;
         }
         else
         {
            wb_table3_41_2582e( false) ;
         }
      }

      protected void wb_table2_5_2582( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2582( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2582e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2582e( true) ;
         }
         else
         {
            wb_table2_5_2582e( false) ;
         }
      }

      protected void wb_table4_13_2582( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockgrupocargo_codigo_Internalname, "Grupo", "", "", lblTextblockgrupocargo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynGrupoCargo_Codigo, dynGrupoCargo_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)), 1, dynGrupoCargo_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynGrupoCargo_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_Geral_Cargo.htm");
            dynGrupoCargo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoCargo_Codigo_Internalname, "Values", (String)(dynGrupoCargo_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcargo_uocod_Internalname, "Unidade Organizacional", "", "", lblTextblockcargo_uocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_23_2582( true) ;
         }
         return  ;
      }

      protected void wb_table5_23_2582e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcargo_nome_Internalname, "Nome", "", "", lblTextblockcargo_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCargo_Nome_Internalname, A618Cargo_Nome, StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCargo_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCargo_Nome_Enabled, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcargo_ativo_Internalname, "Ativo?", "", "", lblTextblockcargo_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcargo_ativo_Visible, 1, 0, "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkCargo_Ativo_Internalname, StringUtil.BoolToStr( A628Cargo_Ativo), "", "", chkCargo_Ativo.Visible, chkCargo_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(38, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2582e( true) ;
         }
         else
         {
            wb_table4_13_2582e( false) ;
         }
      }

      protected void wb_table5_23_2582( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcargo_uocod_Internalname, tblTablemergedcargo_uocod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavCargo_uocod, dynavCargo_uocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)), 1, dynavCargo_uocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavCargo_uocod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_Geral_Cargo.htm");
            dynavCargo_uocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod_Internalname, "Values", (String)(dynavCargo_uocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgSelectuo_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgSelectuo_Visible, 1, "", "Selecionar Unidade Organizacional", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgSelectuo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e112582_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Cargo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_2582e( true) ;
         }
         else
         {
            wb_table5_23_2582e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12252 */
         E12252 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynGrupoCargo_Codigo.CurrentValue = cgiGet( dynGrupoCargo_Codigo_Internalname);
               A615GrupoCargo_Codigo = (int)(NumberUtil.Val( cgiGet( dynGrupoCargo_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
               dynavCargo_uocod.CurrentValue = cgiGet( dynavCargo_uocod_Internalname);
               AV14Cargo_UOCod = (int)(NumberUtil.Val( cgiGet( dynavCargo_uocod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)));
               A618Cargo_Nome = StringUtil.Upper( cgiGet( edtCargo_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A618Cargo_Nome", A618Cargo_Nome);
               A628Cargo_Ativo = StringUtil.StrToBool( cgiGet( chkCargo_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A628Cargo_Ativo", A628Cargo_Ativo);
               A617Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCargo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
               /* Read saved values. */
               Z617Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z617Cargo_Codigo"), ",", "."));
               Z618Cargo_Nome = cgiGet( "Z618Cargo_Nome");
               Z628Cargo_Ativo = StringUtil.StrToBool( cgiGet( "Z628Cargo_Ativo"));
               Z615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z615GrupoCargo_Codigo"), ",", "."));
               Z1002Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( "Z1002Cargo_UOCod"), ",", "."));
               n1002Cargo_UOCod = ((0==A1002Cargo_UOCod) ? true : false);
               A1002Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( "Z1002Cargo_UOCod"), ",", "."));
               n1002Cargo_UOCod = false;
               n1002Cargo_UOCod = ((0==A1002Cargo_UOCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "N615GrupoCargo_Codigo"), ",", "."));
               N1002Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( "N1002Cargo_UOCod"), ",", "."));
               n1002Cargo_UOCod = ((0==A1002Cargo_UOCod) ? true : false);
               AV7Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCARGO_CODIGO"), ",", "."));
               AV11Insert_GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_GRUPOCARGO_CODIGO"), ",", "."));
               AV13Insert_Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CARGO_UOCOD"), ",", "."));
               A1002Cargo_UOCod = (int)(context.localUtil.CToN( cgiGet( "CARGO_UOCOD"), ",", "."));
               n1002Cargo_UOCod = ((0==A1002Cargo_UOCod) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1003Cargo_UONom = cgiGet( "CARGO_UONOM");
               n1003Cargo_UONom = false;
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Geral_Cargo";
               A617Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCargo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A617Cargo_Codigo != Z617Cargo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("geral_cargo:[SecurityCheckFailed value for]"+"Cargo_Codigo:"+context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("geral_cargo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A617Cargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode82 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode82;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound82 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_250( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CARGO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtCargo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12252 */
                           E12252 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13252 */
                           E13252 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E13252 */
            E13252 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2582( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2582( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod.Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_250( )
      {
         BeforeValidate2582( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2582( ) ;
            }
            else
            {
               CheckExtendedTable2582( ) ;
               CloseExtendedTableCursors2582( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption250( )
      {
      }

      protected void E12252( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "GrupoCargo_Codigo") == 0 )
               {
                  AV11Insert_GrupoCargo_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_GrupoCargo_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Cargo_UOCod") == 0 )
               {
                  AV13Insert_Cargo_UOCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Cargo_UOCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtCargo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCargo_Codigo_Visible), 5, 0)));
         AV14Cargo_UOCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)));
      }

      protected void E13252( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwgeral_cargo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2582( short GX_JID )
      {
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z618Cargo_Nome = T00253_A618Cargo_Nome[0];
               Z628Cargo_Ativo = T00253_A628Cargo_Ativo[0];
               Z615GrupoCargo_Codigo = T00253_A615GrupoCargo_Codigo[0];
               Z1002Cargo_UOCod = T00253_A1002Cargo_UOCod[0];
            }
            else
            {
               Z618Cargo_Nome = A618Cargo_Nome;
               Z628Cargo_Ativo = A628Cargo_Ativo;
               Z615GrupoCargo_Codigo = A615GrupoCargo_Codigo;
               Z1002Cargo_UOCod = A1002Cargo_UOCod;
            }
         }
         if ( GX_JID == -17 )
         {
            Z617Cargo_Codigo = A617Cargo_Codigo;
            Z618Cargo_Nome = A618Cargo_Nome;
            Z628Cargo_Ativo = A628Cargo_Ativo;
            Z615GrupoCargo_Codigo = A615GrupoCargo_Codigo;
            Z1002Cargo_UOCod = A1002Cargo_UOCod;
            Z1003Cargo_UONom = A1003Cargo_UONom;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvCARGO_UOCOD_html2582( ) ;
         edtCargo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCargo_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV15Pgmname = "Geral_Cargo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         edtCargo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCargo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Cargo_Codigo) )
         {
            A617Cargo_Codigo = AV7Cargo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_GrupoCargo_Codigo) )
         {
            dynGrupoCargo_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGrupoCargo_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynGrupoCargo_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGrupoCargo_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkCargo_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCargo_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkCargo_Ativo.Visible), 5, 0)));
         lblTextblockcargo_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcargo_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcargo_ativo_Visible), 5, 0)));
         imgSelectuo_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgSelectuo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgSelectuo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Cargo_UOCod) )
         {
            A1002Cargo_UOCod = AV13Insert_Cargo_UOCod;
            n1002Cargo_UOCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_GrupoCargo_Codigo) )
         {
            A615GrupoCargo_Codigo = AV11Insert_GrupoCargo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A628Cargo_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A628Cargo_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A628Cargo_Ativo", A628Cargo_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00255 */
            pr_default.execute(3, new Object[] {n1002Cargo_UOCod, A1002Cargo_UOCod});
            A1003Cargo_UONom = T00255_A1003Cargo_UONom[0];
            n1003Cargo_UONom = T00255_n1003Cargo_UONom[0];
            pr_default.close(3);
         }
      }

      protected void Load2582( )
      {
         /* Using cursor T00257 */
         pr_default.execute(5, new Object[] {A617Cargo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound82 = 1;
            A618Cargo_Nome = T00257_A618Cargo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A618Cargo_Nome", A618Cargo_Nome);
            A1003Cargo_UONom = T00257_A1003Cargo_UONom[0];
            n1003Cargo_UONom = T00257_n1003Cargo_UONom[0];
            A628Cargo_Ativo = T00257_A628Cargo_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A628Cargo_Ativo", A628Cargo_Ativo);
            A615GrupoCargo_Codigo = T00257_A615GrupoCargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
            A1002Cargo_UOCod = T00257_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = T00257_n1002Cargo_UOCod[0];
            ZM2582( -17) ;
         }
         pr_default.close(5);
         OnLoadActions2582( ) ;
      }

      protected void OnLoadActions2582( )
      {
         if ( (0==AV14Cargo_UOCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV14Cargo_UOCod = A1002Cargo_UOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)));
         }
      }

      protected void CheckExtendedTable2582( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( (0==AV14Cargo_UOCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV14Cargo_UOCod = A1002Cargo_UOCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)));
         }
         /* Using cursor T00254 */
         pr_default.execute(2, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo de Cargos'.", "ForeignKeyNotFound", 1, "GRUPOCARGO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00255 */
         pr_default.execute(3, new Object[] {n1002Cargo_UOCod, A1002Cargo_UOCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1002Cargo_UOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Cargos_Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1003Cargo_UONom = T00255_A1003Cargo_UONom[0];
         n1003Cargo_UONom = T00255_n1003Cargo_UONom[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2582( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_18( int A615GrupoCargo_Codigo )
      {
         /* Using cursor T00258 */
         pr_default.execute(6, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo de Cargos'.", "ForeignKeyNotFound", 1, "GRUPOCARGO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_19( int A1002Cargo_UOCod )
      {
         /* Using cursor T00259 */
         pr_default.execute(7, new Object[] {n1002Cargo_UOCod, A1002Cargo_UOCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A1002Cargo_UOCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Cargos_Unidade Organizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1003Cargo_UONom = T00259_A1003Cargo_UONom[0];
         n1003Cargo_UONom = T00259_n1003Cargo_UONom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1003Cargo_UONom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey2582( )
      {
         /* Using cursor T002510 */
         pr_default.execute(8, new Object[] {A617Cargo_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound82 = 1;
         }
         else
         {
            RcdFound82 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00253 */
         pr_default.execute(1, new Object[] {A617Cargo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2582( 17) ;
            RcdFound82 = 1;
            A617Cargo_Codigo = T00253_A617Cargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
            A618Cargo_Nome = T00253_A618Cargo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A618Cargo_Nome", A618Cargo_Nome);
            A628Cargo_Ativo = T00253_A628Cargo_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A628Cargo_Ativo", A628Cargo_Ativo);
            A615GrupoCargo_Codigo = T00253_A615GrupoCargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
            A1002Cargo_UOCod = T00253_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = T00253_n1002Cargo_UOCod[0];
            Z617Cargo_Codigo = A617Cargo_Codigo;
            sMode82 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2582( ) ;
            if ( AnyError == 1 )
            {
               RcdFound82 = 0;
               InitializeNonKey2582( ) ;
            }
            Gx_mode = sMode82;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound82 = 0;
            InitializeNonKey2582( ) ;
            sMode82 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode82;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2582( ) ;
         if ( RcdFound82 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound82 = 0;
         /* Using cursor T002511 */
         pr_default.execute(9, new Object[] {A617Cargo_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002511_A617Cargo_Codigo[0] < A617Cargo_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002511_A617Cargo_Codigo[0] > A617Cargo_Codigo ) ) )
            {
               A617Cargo_Codigo = T002511_A617Cargo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
               RcdFound82 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound82 = 0;
         /* Using cursor T002512 */
         pr_default.execute(10, new Object[] {A617Cargo_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002512_A617Cargo_Codigo[0] > A617Cargo_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002512_A617Cargo_Codigo[0] < A617Cargo_Codigo ) ) )
            {
               A617Cargo_Codigo = T002512_A617Cargo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
               RcdFound82 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2582( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2582( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound82 == 1 )
            {
               if ( A617Cargo_Codigo != Z617Cargo_Codigo )
               {
                  A617Cargo_Codigo = Z617Cargo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CARGO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCargo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2582( ) ;
                  GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A617Cargo_Codigo != Z617Cargo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2582( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CARGO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtCargo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2582( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A617Cargo_Codigo != Z617Cargo_Codigo )
         {
            A617Cargo_Codigo = Z617Cargo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CARGO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCargo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2582( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00252 */
            pr_default.execute(0, new Object[] {A617Cargo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Cargo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z618Cargo_Nome, T00252_A618Cargo_Nome[0]) != 0 ) || ( Z628Cargo_Ativo != T00252_A628Cargo_Ativo[0] ) || ( Z615GrupoCargo_Codigo != T00252_A615GrupoCargo_Codigo[0] ) || ( Z1002Cargo_UOCod != T00252_A1002Cargo_UOCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z618Cargo_Nome, T00252_A618Cargo_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("geral_cargo:[seudo value changed for attri]"+"Cargo_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z618Cargo_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00252_A618Cargo_Nome[0]);
               }
               if ( Z628Cargo_Ativo != T00252_A628Cargo_Ativo[0] )
               {
                  GXUtil.WriteLog("geral_cargo:[seudo value changed for attri]"+"Cargo_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z628Cargo_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00252_A628Cargo_Ativo[0]);
               }
               if ( Z615GrupoCargo_Codigo != T00252_A615GrupoCargo_Codigo[0] )
               {
                  GXUtil.WriteLog("geral_cargo:[seudo value changed for attri]"+"GrupoCargo_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z615GrupoCargo_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00252_A615GrupoCargo_Codigo[0]);
               }
               if ( Z1002Cargo_UOCod != T00252_A1002Cargo_UOCod[0] )
               {
                  GXUtil.WriteLog("geral_cargo:[seudo value changed for attri]"+"Cargo_UOCod");
                  GXUtil.WriteLogRaw("Old: ",Z1002Cargo_UOCod);
                  GXUtil.WriteLogRaw("Current: ",T00252_A1002Cargo_UOCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Geral_Cargo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2582( )
      {
         BeforeValidate2582( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2582( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2582( 0) ;
            CheckOptimisticConcurrency2582( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2582( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2582( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002513 */
                     pr_default.execute(11, new Object[] {A618Cargo_Nome, A628Cargo_Ativo, A615GrupoCargo_Codigo, n1002Cargo_UOCod, A1002Cargo_UOCod});
                     A617Cargo_Codigo = T002513_A617Cargo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Cargo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption250( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2582( ) ;
            }
            EndLevel2582( ) ;
         }
         CloseExtendedTableCursors2582( ) ;
      }

      protected void Update2582( )
      {
         BeforeValidate2582( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2582( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2582( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2582( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2582( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002514 */
                     pr_default.execute(12, new Object[] {A618Cargo_Nome, A628Cargo_Ativo, A615GrupoCargo_Codigo, n1002Cargo_UOCod, A1002Cargo_UOCod, A617Cargo_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Geral_Cargo") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Geral_Cargo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2582( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2582( ) ;
         }
         CloseExtendedTableCursors2582( ) ;
      }

      protected void DeferredUpdate2582( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2582( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2582( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2582( ) ;
            AfterConfirm2582( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2582( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002515 */
                  pr_default.execute(13, new Object[] {A617Cargo_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("Geral_Cargo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode82 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2582( ) ;
         Gx_mode = sMode82;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2582( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002516 */
            pr_default.execute(14, new Object[] {n1002Cargo_UOCod, A1002Cargo_UOCod});
            A1003Cargo_UONom = T002516_A1003Cargo_UONom[0];
            n1003Cargo_UONom = T002516_n1003Cargo_UONom[0];
            pr_default.close(14);
            if ( (0==AV14Cargo_UOCod) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV14Cargo_UOCod = A1002Cargo_UOCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Cargo_UOCod), 6, 0)));
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002517 */
            pr_default.execute(15, new Object[] {A617Cargo_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void EndLevel2582( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2582( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            context.CommitDataStores( "Geral_Cargo");
            if ( AnyError == 0 )
            {
               ConfirmValues250( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            context.RollbackDataStores( "Geral_Cargo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2582( )
      {
         /* Scan By routine */
         /* Using cursor T002518 */
         pr_default.execute(16);
         RcdFound82 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound82 = 1;
            A617Cargo_Codigo = T002518_A617Cargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2582( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound82 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound82 = 1;
            A617Cargo_Codigo = T002518_A617Cargo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2582( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm2582( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2582( )
      {
         /* Before Insert Rules */
         A1002Cargo_UOCod = AV14Cargo_UOCod;
         n1002Cargo_UOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
      }

      protected void BeforeUpdate2582( )
      {
         /* Before Update Rules */
         A1002Cargo_UOCod = AV14Cargo_UOCod;
         n1002Cargo_UOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
      }

      protected void BeforeDelete2582( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2582( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2582( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2582( )
      {
         dynGrupoCargo_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynGrupoCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynGrupoCargo_Codigo.Enabled), 5, 0)));
         dynavCargo_uocod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavCargo_uocod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCargo_uocod.Enabled), 5, 0)));
         edtCargo_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCargo_Nome_Enabled), 5, 0)));
         chkCargo_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCargo_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkCargo_Ativo.Enabled), 5, 0)));
         edtCargo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCargo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCargo_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues250( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311722104");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Cargo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z617Cargo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z617Cargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z618Cargo_Nome", Z618Cargo_Nome);
         GxWebStd.gx_boolean_hidden_field( context, "Z628Cargo_Ativo", Z628Cargo_Ativo);
         GxWebStd.gx_hidden_field( context, "Z615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z615GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1002Cargo_UOCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1002Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1002Cargo_UOCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1002Cargo_UOCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Cargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_GRUPOCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CARGO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CARGO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1002Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CARGO_UONOM", StringUtil.RTrim( A1003Cargo_UONom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCARGO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Cargo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Geral_Cargo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("geral_cargo:[SendSecurityCheck value for]"+"Cargo_Codigo:"+context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("geral_cargo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Cargo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Geral_Cargo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Cargos" ;
      }

      protected void InitializeNonKey2582( )
      {
         A615GrupoCargo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
         A1002Cargo_UOCod = 0;
         n1002Cargo_UOCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
         A618Cargo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A618Cargo_Nome", A618Cargo_Nome);
         A1003Cargo_UONom = "";
         n1003Cargo_UONom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1003Cargo_UONom", A1003Cargo_UONom);
         A628Cargo_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A628Cargo_Ativo", A628Cargo_Ativo);
         Z618Cargo_Nome = "";
         Z628Cargo_Ativo = false;
         Z615GrupoCargo_Codigo = 0;
         Z1002Cargo_UOCod = 0;
      }

      protected void InitAll2582( )
      {
         A617Cargo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A617Cargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A617Cargo_Codigo), 6, 0)));
         InitializeNonKey2582( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A628Cargo_Ativo = i628Cargo_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A628Cargo_Ativo", A628Cargo_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117221029");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("geral_cargo.js", "?20203117221029");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockgrupocargo_codigo_Internalname = "TEXTBLOCKGRUPOCARGO_CODIGO";
         dynGrupoCargo_Codigo_Internalname = "GRUPOCARGO_CODIGO";
         lblTextblockcargo_uocod_Internalname = "TEXTBLOCKCARGO_UOCOD";
         dynavCargo_uocod_Internalname = "vCARGO_UOCOD";
         imgSelectuo_Internalname = "SELECTUO";
         tblTablemergedcargo_uocod_Internalname = "TABLEMERGEDCARGO_UOCOD";
         lblTextblockcargo_nome_Internalname = "TEXTBLOCKCARGO_NOME";
         edtCargo_Nome_Internalname = "CARGO_NOME";
         lblTextblockcargo_ativo_Internalname = "TEXTBLOCKCARGO_ATIVO";
         chkCargo_Ativo_Internalname = "CARGO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtCargo_Codigo_Internalname = "CARGO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Cargo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Cargos";
         imgSelectuo_Visible = 1;
         dynavCargo_uocod_Jsonclick = "";
         dynavCargo_uocod.Enabled = 1;
         chkCargo_Ativo.Enabled = 1;
         chkCargo_Ativo.Visible = 1;
         lblTextblockcargo_ativo_Visible = 1;
         edtCargo_Nome_Jsonclick = "";
         edtCargo_Nome_Enabled = 1;
         dynGrupoCargo_Codigo_Jsonclick = "";
         dynGrupoCargo_Codigo.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtCargo_Codigo_Jsonclick = "";
         edtCargo_Codigo_Enabled = 0;
         edtCargo_Codigo_Visible = 1;
         chkCargo_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAGRUPOCARGO_CODIGO251( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAGRUPOCARGO_CODIGO_data251( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAGRUPOCARGO_CODIGO_html251( )
      {
         int gxdynajaxvalue ;
         GXDLAGRUPOCARGO_CODIGO_data251( ) ;
         gxdynajaxindex = 1;
         dynGrupoCargo_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynGrupoCargo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAGRUPOCARGO_CODIGO_data251( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002519 */
         pr_default.execute(17);
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002519_A615GrupoCargo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T002519_A616GrupoCargo_Nome[0]);
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      protected void GXDLVvCARGO_UOCOD2582( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCARGO_UOCOD_data2582( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCARGO_UOCOD_html2582( )
      {
         int gxdynajaxvalue ;
         GXDLVvCARGO_UOCOD_data2582( ) ;
         gxdynajaxindex = 1;
         dynavCargo_uocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCargo_uocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvCARGO_UOCOD_data2582( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002520 */
         pr_default.execute(18);
         while ( (pr_default.getStatus(18) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002520_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002520_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(18);
         }
         pr_default.close(18);
      }

      public void Valid_Grupocargo_codigo( GXCombobox dynGX_Parm1 )
      {
         dynGrupoCargo_Codigo = dynGX_Parm1;
         A615GrupoCargo_Codigo = (int)(NumberUtil.Val( dynGrupoCargo_Codigo.CurrentValue, "."));
         /* Using cursor T002521 */
         pr_default.execute(19, new Object[] {A615GrupoCargo_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo de Cargos'.", "ForeignKeyNotFound", 1, "GRUPOCARGO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynGrupoCargo_Codigo_Internalname;
         }
         pr_default.close(19);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Cargo_Codigo',fld:'vCARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E13252',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOSELECTUO'","{handler:'E112582',iparms:[{av:'AV14Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14Cargo_UOCod',fld:'vCARGO_UOCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z618Cargo_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T00256_A615GrupoCargo_Codigo = new int[1] ;
         T00256_A616GrupoCargo_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockgrupocargo_codigo_Jsonclick = "";
         lblTextblockcargo_uocod_Jsonclick = "";
         lblTextblockcargo_nome_Jsonclick = "";
         A618Cargo_Nome = "";
         lblTextblockcargo_ativo_Jsonclick = "";
         imgSelectuo_Jsonclick = "";
         A1003Cargo_UONom = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode82 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1003Cargo_UONom = "";
         T00255_A1003Cargo_UONom = new String[] {""} ;
         T00255_n1003Cargo_UONom = new bool[] {false} ;
         T00257_A617Cargo_Codigo = new int[1] ;
         T00257_A618Cargo_Nome = new String[] {""} ;
         T00257_A1003Cargo_UONom = new String[] {""} ;
         T00257_n1003Cargo_UONom = new bool[] {false} ;
         T00257_A628Cargo_Ativo = new bool[] {false} ;
         T00257_A615GrupoCargo_Codigo = new int[1] ;
         T00257_A1002Cargo_UOCod = new int[1] ;
         T00257_n1002Cargo_UOCod = new bool[] {false} ;
         T00254_A615GrupoCargo_Codigo = new int[1] ;
         T00258_A615GrupoCargo_Codigo = new int[1] ;
         T00259_A1003Cargo_UONom = new String[] {""} ;
         T00259_n1003Cargo_UONom = new bool[] {false} ;
         T002510_A617Cargo_Codigo = new int[1] ;
         T00253_A617Cargo_Codigo = new int[1] ;
         T00253_A618Cargo_Nome = new String[] {""} ;
         T00253_A628Cargo_Ativo = new bool[] {false} ;
         T00253_A615GrupoCargo_Codigo = new int[1] ;
         T00253_A1002Cargo_UOCod = new int[1] ;
         T00253_n1002Cargo_UOCod = new bool[] {false} ;
         T002511_A617Cargo_Codigo = new int[1] ;
         T002512_A617Cargo_Codigo = new int[1] ;
         T00252_A617Cargo_Codigo = new int[1] ;
         T00252_A618Cargo_Nome = new String[] {""} ;
         T00252_A628Cargo_Ativo = new bool[] {false} ;
         T00252_A615GrupoCargo_Codigo = new int[1] ;
         T00252_A1002Cargo_UOCod = new int[1] ;
         T00252_n1002Cargo_UOCod = new bool[] {false} ;
         T002513_A617Cargo_Codigo = new int[1] ;
         T002516_A1003Cargo_UONom = new String[] {""} ;
         T002516_n1003Cargo_UONom = new bool[] {false} ;
         T002517_A1Usuario_Codigo = new int[1] ;
         T002518_A617Cargo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002519_A615GrupoCargo_Codigo = new int[1] ;
         T002519_A616GrupoCargo_Nome = new String[] {""} ;
         T002520_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T002520_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T002520_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         T002521_A615GrupoCargo_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_cargo__default(),
            new Object[][] {
                new Object[] {
               T00252_A617Cargo_Codigo, T00252_A618Cargo_Nome, T00252_A628Cargo_Ativo, T00252_A615GrupoCargo_Codigo, T00252_A1002Cargo_UOCod, T00252_n1002Cargo_UOCod
               }
               , new Object[] {
               T00253_A617Cargo_Codigo, T00253_A618Cargo_Nome, T00253_A628Cargo_Ativo, T00253_A615GrupoCargo_Codigo, T00253_A1002Cargo_UOCod, T00253_n1002Cargo_UOCod
               }
               , new Object[] {
               T00254_A615GrupoCargo_Codigo
               }
               , new Object[] {
               T00255_A1003Cargo_UONom, T00255_n1003Cargo_UONom
               }
               , new Object[] {
               T00256_A615GrupoCargo_Codigo, T00256_A616GrupoCargo_Nome
               }
               , new Object[] {
               T00257_A617Cargo_Codigo, T00257_A618Cargo_Nome, T00257_A1003Cargo_UONom, T00257_n1003Cargo_UONom, T00257_A628Cargo_Ativo, T00257_A615GrupoCargo_Codigo, T00257_A1002Cargo_UOCod, T00257_n1002Cargo_UOCod
               }
               , new Object[] {
               T00258_A615GrupoCargo_Codigo
               }
               , new Object[] {
               T00259_A1003Cargo_UONom, T00259_n1003Cargo_UONom
               }
               , new Object[] {
               T002510_A617Cargo_Codigo
               }
               , new Object[] {
               T002511_A617Cargo_Codigo
               }
               , new Object[] {
               T002512_A617Cargo_Codigo
               }
               , new Object[] {
               T002513_A617Cargo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002516_A1003Cargo_UONom, T002516_n1003Cargo_UONom
               }
               , new Object[] {
               T002517_A1Usuario_Codigo
               }
               , new Object[] {
               T002518_A617Cargo_Codigo
               }
               , new Object[] {
               T002519_A615GrupoCargo_Codigo, T002519_A616GrupoCargo_Nome
               }
               , new Object[] {
               T002520_A611UnidadeOrganizacional_Codigo, T002520_A612UnidadeOrganizacional_Nome, T002520_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               T002521_A615GrupoCargo_Codigo
               }
            }
         );
         Z628Cargo_Ativo = true;
         A628Cargo_Ativo = true;
         i628Cargo_Ativo = true;
         AV15Pgmname = "Geral_Cargo";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound82 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Cargo_Codigo ;
      private int Z617Cargo_Codigo ;
      private int Z615GrupoCargo_Codigo ;
      private int Z1002Cargo_UOCod ;
      private int N615GrupoCargo_Codigo ;
      private int N1002Cargo_UOCod ;
      private int A615GrupoCargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int AV7Cargo_Codigo ;
      private int trnEnded ;
      private int AV14Cargo_UOCod ;
      private int A617Cargo_Codigo ;
      private int edtCargo_Codigo_Enabled ;
      private int edtCargo_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtCargo_Nome_Enabled ;
      private int lblTextblockcargo_ativo_Visible ;
      private int imgSelectuo_Visible ;
      private int AV11Insert_GrupoCargo_Codigo ;
      private int AV13Insert_Cargo_UOCod ;
      private int AV16GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkCargo_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynGrupoCargo_Codigo_Internalname ;
      private String edtCargo_Codigo_Internalname ;
      private String edtCargo_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockgrupocargo_codigo_Internalname ;
      private String lblTextblockgrupocargo_codigo_Jsonclick ;
      private String dynGrupoCargo_Codigo_Jsonclick ;
      private String lblTextblockcargo_uocod_Internalname ;
      private String lblTextblockcargo_uocod_Jsonclick ;
      private String lblTextblockcargo_nome_Internalname ;
      private String lblTextblockcargo_nome_Jsonclick ;
      private String edtCargo_Nome_Internalname ;
      private String edtCargo_Nome_Jsonclick ;
      private String lblTextblockcargo_ativo_Internalname ;
      private String lblTextblockcargo_ativo_Jsonclick ;
      private String tblTablemergedcargo_uocod_Internalname ;
      private String dynavCargo_uocod_Internalname ;
      private String dynavCargo_uocod_Jsonclick ;
      private String imgSelectuo_Internalname ;
      private String imgSelectuo_Jsonclick ;
      private String A1003Cargo_UONom ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode82 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1003Cargo_UONom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool Z628Cargo_Ativo ;
      private bool entryPointCalled ;
      private bool n1002Cargo_UOCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A628Cargo_Ativo ;
      private bool n1003Cargo_UONom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i628Cargo_Ativo ;
      private String Z618Cargo_Nome ;
      private String A618Cargo_Nome ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T00256_A615GrupoCargo_Codigo ;
      private String[] T00256_A616GrupoCargo_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynGrupoCargo_Codigo ;
      private GXCombobox dynavCargo_uocod ;
      private GXCheckbox chkCargo_Ativo ;
      private String[] T00255_A1003Cargo_UONom ;
      private bool[] T00255_n1003Cargo_UONom ;
      private int[] T00257_A617Cargo_Codigo ;
      private String[] T00257_A618Cargo_Nome ;
      private String[] T00257_A1003Cargo_UONom ;
      private bool[] T00257_n1003Cargo_UONom ;
      private bool[] T00257_A628Cargo_Ativo ;
      private int[] T00257_A615GrupoCargo_Codigo ;
      private int[] T00257_A1002Cargo_UOCod ;
      private bool[] T00257_n1002Cargo_UOCod ;
      private int[] T00254_A615GrupoCargo_Codigo ;
      private int[] T00258_A615GrupoCargo_Codigo ;
      private String[] T00259_A1003Cargo_UONom ;
      private bool[] T00259_n1003Cargo_UONom ;
      private int[] T002510_A617Cargo_Codigo ;
      private int[] T00253_A617Cargo_Codigo ;
      private String[] T00253_A618Cargo_Nome ;
      private bool[] T00253_A628Cargo_Ativo ;
      private int[] T00253_A615GrupoCargo_Codigo ;
      private int[] T00253_A1002Cargo_UOCod ;
      private bool[] T00253_n1002Cargo_UOCod ;
      private int[] T002511_A617Cargo_Codigo ;
      private int[] T002512_A617Cargo_Codigo ;
      private int[] T00252_A617Cargo_Codigo ;
      private String[] T00252_A618Cargo_Nome ;
      private bool[] T00252_A628Cargo_Ativo ;
      private int[] T00252_A615GrupoCargo_Codigo ;
      private int[] T00252_A1002Cargo_UOCod ;
      private bool[] T00252_n1002Cargo_UOCod ;
      private int[] T002513_A617Cargo_Codigo ;
      private String[] T002516_A1003Cargo_UONom ;
      private bool[] T002516_n1003Cargo_UONom ;
      private int[] T002517_A1Usuario_Codigo ;
      private int[] T002518_A617Cargo_Codigo ;
      private int[] T002519_A615GrupoCargo_Codigo ;
      private String[] T002519_A616GrupoCargo_Nome ;
      private int[] T002520_A611UnidadeOrganizacional_Codigo ;
      private String[] T002520_A612UnidadeOrganizacional_Nome ;
      private bool[] T002520_A629UnidadeOrganizacional_Ativo ;
      private int[] T002521_A615GrupoCargo_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class geral_cargo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00256 ;
          prmT00256 = new Object[] {
          } ;
          Object[] prmT00257 ;
          prmT00257 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00254 ;
          prmT00254 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00255 ;
          prmT00255 = new Object[] {
          new Object[] {"@Cargo_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00258 ;
          prmT00258 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00259 ;
          prmT00259 = new Object[] {
          new Object[] {"@Cargo_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002510 ;
          prmT002510 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00253 ;
          prmT00253 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002511 ;
          prmT002511 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002512 ;
          prmT002512 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00252 ;
          prmT00252 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002513 ;
          prmT002513 = new Object[] {
          new Object[] {"@Cargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Cargo_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Cargo_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002514 ;
          prmT002514 = new Object[] {
          new Object[] {"@Cargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Cargo_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Cargo_UOCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002515 ;
          prmT002515 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002516 ;
          prmT002516 = new Object[] {
          new Object[] {"@Cargo_UOCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002517 ;
          prmT002517 = new Object[] {
          new Object[] {"@Cargo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002518 ;
          prmT002518 = new Object[] {
          } ;
          Object[] prmT002519 ;
          prmT002519 = new Object[] {
          } ;
          Object[] prmT002520 ;
          prmT002520 = new Object[] {
          } ;
          Object[] prmT002521 ;
          prmT002521 = new Object[] {
          new Object[] {"@GrupoCargo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00252", "SELECT [Cargo_Codigo], [Cargo_Nome], [Cargo_Ativo], [GrupoCargo_Codigo], [Cargo_UOCod] AS Cargo_UOCod FROM [Geral_Cargo] WITH (UPDLOCK) WHERE [Cargo_Codigo] = @Cargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00252,1,0,true,false )
             ,new CursorDef("T00253", "SELECT [Cargo_Codigo], [Cargo_Nome], [Cargo_Ativo], [GrupoCargo_Codigo], [Cargo_UOCod] AS Cargo_UOCod FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Cargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00253,1,0,true,false )
             ,new CursorDef("T00254", "SELECT [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00254,1,0,true,false )
             ,new CursorDef("T00255", "SELECT [UnidadeOrganizacional_Nome] AS Cargo_UONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Cargo_UOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00255,1,0,true,false )
             ,new CursorDef("T00256", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) ORDER BY [GrupoCargo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT00256,0,0,true,false )
             ,new CursorDef("T00257", "SELECT TM1.[Cargo_Codigo], TM1.[Cargo_Nome], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, TM1.[Cargo_Ativo], TM1.[GrupoCargo_Codigo], TM1.[Cargo_UOCod] AS Cargo_UOCod FROM ([Geral_Cargo] TM1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = TM1.[Cargo_UOCod]) WHERE TM1.[Cargo_Codigo] = @Cargo_Codigo ORDER BY TM1.[Cargo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00257,100,0,true,false )
             ,new CursorDef("T00258", "SELECT [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00258,1,0,true,false )
             ,new CursorDef("T00259", "SELECT [UnidadeOrganizacional_Nome] AS Cargo_UONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Cargo_UOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00259,1,0,true,false )
             ,new CursorDef("T002510", "SELECT [Cargo_Codigo] FROM [Geral_Cargo] WITH (NOLOCK) WHERE [Cargo_Codigo] = @Cargo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002510,1,0,true,false )
             ,new CursorDef("T002511", "SELECT TOP 1 [Cargo_Codigo] FROM [Geral_Cargo] WITH (NOLOCK) WHERE ( [Cargo_Codigo] > @Cargo_Codigo) ORDER BY [Cargo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002511,1,0,true,true )
             ,new CursorDef("T002512", "SELECT TOP 1 [Cargo_Codigo] FROM [Geral_Cargo] WITH (NOLOCK) WHERE ( [Cargo_Codigo] < @Cargo_Codigo) ORDER BY [Cargo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002512,1,0,true,true )
             ,new CursorDef("T002513", "INSERT INTO [Geral_Cargo]([Cargo_Nome], [Cargo_Ativo], [GrupoCargo_Codigo], [Cargo_UOCod]) VALUES(@Cargo_Nome, @Cargo_Ativo, @GrupoCargo_Codigo, @Cargo_UOCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002513)
             ,new CursorDef("T002514", "UPDATE [Geral_Cargo] SET [Cargo_Nome]=@Cargo_Nome, [Cargo_Ativo]=@Cargo_Ativo, [GrupoCargo_Codigo]=@GrupoCargo_Codigo, [Cargo_UOCod]=@Cargo_UOCod  WHERE [Cargo_Codigo] = @Cargo_Codigo", GxErrorMask.GX_NOMASK,prmT002514)
             ,new CursorDef("T002515", "DELETE FROM [Geral_Cargo]  WHERE [Cargo_Codigo] = @Cargo_Codigo", GxErrorMask.GX_NOMASK,prmT002515)
             ,new CursorDef("T002516", "SELECT [UnidadeOrganizacional_Nome] AS Cargo_UONom FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Cargo_UOCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002516,1,0,true,false )
             ,new CursorDef("T002517", "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_CargoCod] = @Cargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002517,1,0,true,true )
             ,new CursorDef("T002518", "SELECT [Cargo_Codigo] FROM [Geral_Cargo] WITH (NOLOCK) ORDER BY [Cargo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002518,100,0,true,false )
             ,new CursorDef("T002519", "SELECT [GrupoCargo_Codigo], [GrupoCargo_Nome] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) ORDER BY [GrupoCargo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002519,0,0,true,false )
             ,new CursorDef("T002520", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Ativo] = 1 ORDER BY [UnidadeOrganizacional_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002520,0,0,true,false )
             ,new CursorDef("T002521", "SELECT [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK) WHERE [GrupoCargo_Codigo] = @GrupoCargo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002521,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
