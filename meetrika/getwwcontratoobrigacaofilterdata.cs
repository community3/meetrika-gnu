/*
               File: GetWWContratoObrigacaoFilterData
        Description: Get WWContrato Obrigacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:51.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoobrigacaofilterdata : GXProcedure
   {
      public getwwcontratoobrigacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoobrigacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoobrigacaofilterdata objgetwwcontratoobrigacaofilterdata;
         objgetwwcontratoobrigacaofilterdata = new getwwcontratoobrigacaofilterdata();
         objgetwwcontratoobrigacaofilterdata.AV16DDOName = aP0_DDOName;
         objgetwwcontratoobrigacaofilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwcontratoobrigacaofilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoobrigacaofilterdata.AV20OptionsJson = "" ;
         objgetwwcontratoobrigacaofilterdata.AV23OptionsDescJson = "" ;
         objgetwwcontratoobrigacaofilterdata.AV25OptionIndexesJson = "" ;
         objgetwwcontratoobrigacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoobrigacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoobrigacaofilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoobrigacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATOOBRIGACAO_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOBRIGACAO_ITEMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWContratoObrigacaoGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoObrigacaoGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWContratoObrigacaoGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_ITEM") == 0 )
            {
               AV12TFContratoObrigacao_Item = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_ITEM_SEL") == 0 )
            {
               AV13TFContratoObrigacao_Item_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34ContratoObrigacao_Item1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 )
               {
                  AV37DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV38ContratoObrigacao_Item2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV14SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 = AV34ContratoObrigacao_Item1;
         AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 = AV37DynamicFiltersOperator2;
         AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 = AV38ContratoObrigacao_Item2;
         AV50WWContratoObrigacaoDS_8_Tfcontrato_numero = AV10TFContrato_Numero;
         AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item = AV12TFContratoObrigacao_Item;
         AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel = AV13TFContratoObrigacao_Item_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 ,
                                              AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 ,
                                              AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 ,
                                              AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 ,
                                              AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 ,
                                              AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 ,
                                              AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel ,
                                              AV50WWContratoObrigacaoDS_8_Tfcontrato_numero ,
                                              AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel ,
                                              AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item ,
                                              A322ContratoObrigacao_Item ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV50WWContratoObrigacaoDS_8_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoObrigacaoDS_8_Tfcontrato_numero), 20, "%");
         lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item = StringUtil.Concat( StringUtil.RTrim( AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item), "%", "");
         /* Using cursor P00KP2 */
         pr_default.execute(0, new Object[] {AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1, AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1, AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1, AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2, AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2, AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2, lV50WWContratoObrigacaoDS_8_Tfcontrato_numero, AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel, lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item, AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKP2 = false;
            A74Contrato_Codigo = P00KP2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KP2_A77Contrato_Numero[0];
            A322ContratoObrigacao_Item = P00KP2_A322ContratoObrigacao_Item[0];
            A321ContratoObrigacao_Codigo = P00KP2_A321ContratoObrigacao_Codigo[0];
            A77Contrato_Numero = P00KP2_A77Contrato_Numero[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KP2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKP2 = false;
               A74Contrato_Codigo = P00KP2_A74Contrato_Codigo[0];
               A321ContratoObrigacao_Codigo = P00KP2_A321ContratoObrigacao_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKKP2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV18Option = A77Contrato_Numero;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKP2 )
            {
               BRKKP2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOBRIGACAO_ITEMOPTIONS' Routine */
         AV12TFContratoObrigacao_Item = AV14SearchTxt;
         AV13TFContratoObrigacao_Item_Sel = "";
         AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 = AV34ContratoObrigacao_Item1;
         AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 = AV37DynamicFiltersOperator2;
         AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 = AV38ContratoObrigacao_Item2;
         AV50WWContratoObrigacaoDS_8_Tfcontrato_numero = AV10TFContrato_Numero;
         AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item = AV12TFContratoObrigacao_Item;
         AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel = AV13TFContratoObrigacao_Item_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 ,
                                              AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 ,
                                              AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 ,
                                              AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 ,
                                              AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 ,
                                              AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 ,
                                              AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel ,
                                              AV50WWContratoObrigacaoDS_8_Tfcontrato_numero ,
                                              AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel ,
                                              AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item ,
                                              A322ContratoObrigacao_Item ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV50WWContratoObrigacaoDS_8_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoObrigacaoDS_8_Tfcontrato_numero), 20, "%");
         lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item = StringUtil.Concat( StringUtil.RTrim( AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item), "%", "");
         /* Using cursor P00KP3 */
         pr_default.execute(1, new Object[] {AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1, AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1, AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1, AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2, AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2, AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2, lV50WWContratoObrigacaoDS_8_Tfcontrato_numero, AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel, lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item, AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKP4 = false;
            A74Contrato_Codigo = P00KP3_A74Contrato_Codigo[0];
            A322ContratoObrigacao_Item = P00KP3_A322ContratoObrigacao_Item[0];
            A77Contrato_Numero = P00KP3_A77Contrato_Numero[0];
            A321ContratoObrigacao_Codigo = P00KP3_A321ContratoObrigacao_Codigo[0];
            A77Contrato_Numero = P00KP3_A77Contrato_Numero[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00KP3_A322ContratoObrigacao_Item[0], A322ContratoObrigacao_Item) == 0 ) )
            {
               BRKKP4 = false;
               A321ContratoObrigacao_Codigo = P00KP3_A321ContratoObrigacao_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKKP4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A322ContratoObrigacao_Item)) )
            {
               AV18Option = A322ContratoObrigacao_Item;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKP4 )
            {
               BRKKP4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoObrigacao_Item = "";
         AV13TFContratoObrigacao_Item_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34ContratoObrigacao_Item1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV38ContratoObrigacao_Item2 = "";
         AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 = "";
         AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 = "";
         AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 = "";
         AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 = "";
         AV50WWContratoObrigacaoDS_8_Tfcontrato_numero = "";
         AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel = "";
         AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item = "";
         AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel = "";
         scmdbuf = "";
         lV50WWContratoObrigacaoDS_8_Tfcontrato_numero = "";
         lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item = "";
         A322ContratoObrigacao_Item = "";
         A77Contrato_Numero = "";
         P00KP2_A74Contrato_Codigo = new int[1] ;
         P00KP2_A77Contrato_Numero = new String[] {""} ;
         P00KP2_A322ContratoObrigacao_Item = new String[] {""} ;
         P00KP2_A321ContratoObrigacao_Codigo = new int[1] ;
         AV18Option = "";
         P00KP3_A74Contrato_Codigo = new int[1] ;
         P00KP3_A322ContratoObrigacao_Item = new String[] {""} ;
         P00KP3_A77Contrato_Numero = new String[] {""} ;
         P00KP3_A321ContratoObrigacao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoobrigacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KP2_A74Contrato_Codigo, P00KP2_A77Contrato_Numero, P00KP2_A322ContratoObrigacao_Item, P00KP2_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               P00KP3_A74Contrato_Codigo, P00KP3_A322ContratoObrigacao_Item, P00KP3_A77Contrato_Numero, P00KP3_A321ContratoObrigacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV37DynamicFiltersOperator2 ;
      private short AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 ;
      private short AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 ;
      private int AV41GXV1 ;
      private int A74Contrato_Codigo ;
      private int A321ContratoObrigacao_Codigo ;
      private long AV26count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV50WWContratoObrigacaoDS_8_Tfcontrato_numero ;
      private String AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel ;
      private String scmdbuf ;
      private String lV50WWContratoObrigacaoDS_8_Tfcontrato_numero ;
      private String A77Contrato_Numero ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 ;
      private bool BRKKP2 ;
      private bool BRKKP4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFContratoObrigacao_Item ;
      private String AV13TFContratoObrigacao_Item_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34ContratoObrigacao_Item1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV38ContratoObrigacao_Item2 ;
      private String AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 ;
      private String AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 ;
      private String AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 ;
      private String AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 ;
      private String AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item ;
      private String AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel ;
      private String lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item ;
      private String A322ContratoObrigacao_Item ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KP2_A74Contrato_Codigo ;
      private String[] P00KP2_A77Contrato_Numero ;
      private String[] P00KP2_A322ContratoObrigacao_Item ;
      private int[] P00KP2_A321ContratoObrigacao_Codigo ;
      private int[] P00KP3_A74Contrato_Codigo ;
      private String[] P00KP3_A322ContratoObrigacao_Item ;
      private String[] P00KP3_A77Contrato_Numero ;
      private int[] P00KP3_A321ContratoObrigacao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwcontratoobrigacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KP2( IGxContext context ,
                                             String AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 ,
                                             short AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 ,
                                             bool AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 ,
                                             short AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 ,
                                             String AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel ,
                                             String AV50WWContratoObrigacaoDS_8_Tfcontrato_numero ,
                                             String AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel ,
                                             String AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item ,
                                             String A322ContratoObrigacao_Item ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoObrigacao_Item], T1.[ContratoObrigacao_Codigo] FROM ([ContratoObrigacao] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoObrigacaoDS_8_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV50WWContratoObrigacaoDS_8_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV50WWContratoObrigacaoDS_8_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] like @lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] like @lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KP3( IGxContext context ,
                                             String AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1 ,
                                             short AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1 ,
                                             bool AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2 ,
                                             short AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2 ,
                                             String AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel ,
                                             String AV50WWContratoObrigacaoDS_8_Tfcontrato_numero ,
                                             String AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel ,
                                             String AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item ,
                                             String A322ContratoObrigacao_Item ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoObrigacao_Item], T2.[Contrato_Numero], T1.[ContratoObrigacao_Codigo] FROM ([ContratoObrigacao] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43WWContratoObrigacaoDS_1_Dynamicfiltersselector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV44WWContratoObrigacaoDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV46WWContratoObrigacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoObrigacaoDS_5_Dynamicfiltersselector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV48WWContratoObrigacaoDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoObrigacaoDS_8_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV50WWContratoObrigacaoDS_8_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV50WWContratoObrigacaoDS_8_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] like @lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] like @lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoObrigacao_Item]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KP2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] );
               case 1 :
                     return conditional_P00KP3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KP2 ;
          prmP00KP2 = new Object[] {
          new Object[] {"@AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV50WWContratoObrigacaoDS_8_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel",SqlDbType.VarChar,40,0}
          } ;
          Object[] prmP00KP3 ;
          prmP00KP3 = new Object[] {
          new Object[] {"@AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV45WWContratoObrigacaoDS_3_Contratoobrigacao_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV49WWContratoObrigacaoDS_7_Contratoobrigacao_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV50WWContratoObrigacaoDS_8_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV51WWContratoObrigacaoDS_9_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52WWContratoObrigacaoDS_10_Tfcontratoobrigacao_item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV53WWContratoObrigacaoDS_11_Tfcontratoobrigacao_item_sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KP2,100,0,true,false )
             ,new CursorDef("P00KP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KP3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoobrigacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoobrigacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoobrigacaofilterdata") )
          {
             return  ;
          }
          getwwcontratoobrigacaofilterdata worker = new getwwcontratoobrigacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
