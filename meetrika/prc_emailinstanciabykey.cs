/*
               File: PRC_EmailInstanciaByKey
        Description: PRC_Email Instancia By Key
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:37.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_emailinstanciabykey : GXProcedure
   {
      public prc_emailinstanciabykey( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_emailinstanciabykey( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Email_Key ,
                           IGxCollection aP1_SDT_ID_Valor ,
                           int aP2_AreaTrabalho_Codigo ,
                           ref Guid aP3_Email_Instancia_Guid )
      {
         this.AV14Email_Key = aP0_Email_Key;
         this.AV16SDT_ID_Valor = aP1_SDT_ID_Valor;
         this.AV10AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         this.AV13Email_Instancia_Guid = aP3_Email_Instancia_Guid;
         initialize();
         executePrivate();
         aP3_Email_Instancia_Guid=this.AV13Email_Instancia_Guid;
      }

      public Guid executeUdp( String aP0_Email_Key ,
                              IGxCollection aP1_SDT_ID_Valor ,
                              int aP2_AreaTrabalho_Codigo )
      {
         this.AV14Email_Key = aP0_Email_Key;
         this.AV16SDT_ID_Valor = aP1_SDT_ID_Valor;
         this.AV10AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         this.AV13Email_Instancia_Guid = aP3_Email_Instancia_Guid;
         initialize();
         executePrivate();
         aP3_Email_Instancia_Guid=this.AV13Email_Instancia_Guid;
         return AV13Email_Instancia_Guid ;
      }

      public void executeSubmit( String aP0_Email_Key ,
                                 IGxCollection aP1_SDT_ID_Valor ,
                                 int aP2_AreaTrabalho_Codigo ,
                                 ref Guid aP3_Email_Instancia_Guid )
      {
         prc_emailinstanciabykey objprc_emailinstanciabykey;
         objprc_emailinstanciabykey = new prc_emailinstanciabykey();
         objprc_emailinstanciabykey.AV14Email_Key = aP0_Email_Key;
         objprc_emailinstanciabykey.AV16SDT_ID_Valor = aP1_SDT_ID_Valor;
         objprc_emailinstanciabykey.AV10AreaTrabalho_Codigo = aP2_AreaTrabalho_Codigo;
         objprc_emailinstanciabykey.AV13Email_Instancia_Guid = aP3_Email_Instancia_Guid;
         objprc_emailinstanciabykey.context.SetSubmitInitialConfig(context);
         objprc_emailinstanciabykey.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_emailinstanciabykey);
         aP3_Email_Instancia_Guid=this.AV13Email_Instancia_Guid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_emailinstanciabykey)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00C32 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1679ParametrosSistema_URLApp = P00C32_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = P00C32_n1679ParametrosSistema_URLApp[0];
            A330ParametrosSistema_Codigo = P00C32_A330ParametrosSistema_Codigo[0];
            AV15ParametrosSistema_URLApp = StringUtil.Trim( A1679ParametrosSistema_URLApp);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( (System.Guid.Empty==AV13Email_Instancia_Guid) )
         {
            AV13Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
         }
         AV21GXLvl8 = 0;
         /* Using cursor P00C33 */
         pr_default.execute(1, new Object[] {AV13Email_Instancia_Guid});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1666Email_Instancia_Guid = (Guid)((Guid)(P00C33_A1666Email_Instancia_Guid[0]));
            AV21GXLvl8 = 1;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV21GXLvl8 == 0 )
         {
            AV12Email_Instancia.Load(AV13Email_Instancia_Guid);
            GXt_guid1 = (Guid)(AV11Email_Guid);
            GXt_guid2 = (Guid)(GXt_guid1);
            new prc_email_get_guid_by_key(context ).execute(  AV14Email_Key, out  GXt_guid2) ;
            GXt_guid1 = (Guid)((Guid)(GXt_guid2));
            AV11Email_Guid = (Guid)(GXt_guid1);
            /* Using cursor P00C34 */
            pr_default.execute(2, new Object[] {AV11Email_Guid});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1665Email_Guid = (Guid)((Guid)(P00C34_A1665Email_Guid[0]));
               A1669Email_Titulo = P00C34_A1669Email_Titulo[0];
               A1670Email_Corpo = P00C34_A1670Email_Corpo[0];
               A1671Email_Proc_Replace = P00C34_A1671Email_Proc_Replace[0];
               AV12Email_Instancia.gxTpr_Email_guid = (Guid)(A1665Email_Guid);
               AV12Email_Instancia.gxTpr_Email_instancia_titulo = A1669Email_Titulo;
               AV12Email_Instancia.gxTpr_Email_instancia_corpo = A1670Email_Corpo;
               AV8Email_Proc_Replace = A1671Email_Proc_Replace;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            AV12Email_Instancia.gxTpr_Email_instancia_parms = AV16SDT_ID_Valor.ToJSonString(false);
            AV12Email_Instancia.gxTpr_Email_instancia_areatrabalho = AV10AreaTrabalho_Codigo;
            AV12Email_Instancia.Save();
            if ( AV12Email_Instancia.Success() )
            {
               context.CommitDataStores( "PRC_EmailInstanciaByKey");
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Email_Proc_Replace)) )
               {
                  AV17url = StringUtil.Format( "%1%2?%3", AV15ParametrosSistema_URLApp, StringUtil.Trim( AV8Email_Proc_Replace), AV13Email_Instancia_Guid.ToString(), "", "", "", "", "", "");
                  AV9httpClient.Execute("GET", AV17url);
                  if ( ! ( AV9httpClient.StatusCode == 200 ) )
                  {
                     GX_msglist.addItem("ERRO");
                  }
               }
            }
            else
            {
               context.RollbackDataStores( "PRC_EmailInstanciaByKey");
               new gravalogbc(context ).execute(  "Email",  "Email_Instancia",  AV12Email_Instancia.ToJSonString(true),  AV12Email_Instancia.GetMessages().ToJSonString(false)) ;
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00C32_A1679ParametrosSistema_URLApp = new String[] {""} ;
         P00C32_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         P00C32_A330ParametrosSistema_Codigo = new int[1] ;
         A1679ParametrosSistema_URLApp = "";
         AV15ParametrosSistema_URLApp = "";
         P00C33_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         AV12Email_Instancia = new SdtEmail_Instancia(context);
         AV11Email_Guid = (Guid)(System.Guid.Empty);
         GXt_guid1 = (Guid)(System.Guid.Empty);
         GXt_guid2 = (Guid)(System.Guid.Empty);
         P00C34_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         P00C34_A1669Email_Titulo = new String[] {""} ;
         P00C34_A1670Email_Corpo = new String[] {""} ;
         P00C34_A1671Email_Proc_Replace = new String[] {""} ;
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         A1669Email_Titulo = "";
         A1670Email_Corpo = "";
         A1671Email_Proc_Replace = "";
         AV8Email_Proc_Replace = "";
         AV17url = "";
         AV9httpClient = new GxHttpClient( context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_emailinstanciabykey__default(),
            new Object[][] {
                new Object[] {
               P00C32_A1679ParametrosSistema_URLApp, P00C32_n1679ParametrosSistema_URLApp, P00C32_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               P00C33_A1666Email_Instancia_Guid
               }
               , new Object[] {
               P00C34_A1665Email_Guid, P00C34_A1669Email_Titulo, P00C34_A1670Email_Corpo, P00C34_A1671Email_Proc_Replace
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV21GXLvl8 ;
      private int AV10AreaTrabalho_Codigo ;
      private int A330ParametrosSistema_Codigo ;
      private String scmdbuf ;
      private bool n1679ParametrosSistema_URLApp ;
      private String A1670Email_Corpo ;
      private String AV14Email_Key ;
      private String A1679ParametrosSistema_URLApp ;
      private String AV15ParametrosSistema_URLApp ;
      private String A1669Email_Titulo ;
      private String A1671Email_Proc_Replace ;
      private String AV8Email_Proc_Replace ;
      private String AV17url ;
      private Guid AV13Email_Instancia_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private Guid AV11Email_Guid ;
      private Guid GXt_guid1 ;
      private Guid GXt_guid2 ;
      private Guid A1665Email_Guid ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Guid aP3_Email_Instancia_Guid ;
      private IDataStoreProvider pr_default ;
      private String[] P00C32_A1679ParametrosSistema_URLApp ;
      private bool[] P00C32_n1679ParametrosSistema_URLApp ;
      private int[] P00C32_A330ParametrosSistema_Codigo ;
      private Guid[] P00C33_A1666Email_Instancia_Guid ;
      private Guid[] P00C34_A1665Email_Guid ;
      private String[] P00C34_A1669Email_Titulo ;
      private String[] P00C34_A1670Email_Corpo ;
      private String[] P00C34_A1671Email_Proc_Replace ;
      private GxHttpClient AV9httpClient ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ID_Valor ))]
      private IGxCollection AV16SDT_ID_Valor ;
      private SdtEmail_Instancia AV12Email_Instancia ;
   }

   public class prc_emailinstanciabykey__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C32 ;
          prmP00C32 = new Object[] {
          } ;
          Object[] prmP00C33 ;
          prmP00C33 = new Object[] {
          new Object[] {"@AV13Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmP00C34 ;
          prmP00C34 = new Object[] {
          new Object[] {"@AV11Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C32", "SELECT [ParametrosSistema_URLApp], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C32,100,0,false,false )
             ,new CursorDef("P00C33", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @AV13Email_Instancia_Guid ORDER BY [Email_Instancia_Guid] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C33,1,0,false,true )
             ,new CursorDef("P00C34", "SELECT [Email_Guid], [Email_Titulo], [Email_Corpo], [Email_Proc_Replace] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @AV11Email_Guid ORDER BY [Email_Guid] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C34,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 2 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
