/*
               File: ExportReportWWLote
        Description: Export Report WWLote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:54:44.94
       Program type: HTTP procedure
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportreportwwlote : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV94Lote_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV125TFLote_NFe = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV126TFLote_NFe_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV127TFLote_DataNfe = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV128TFLote_DataNfe_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV129TFLote_PrevPagamento = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV130TFLote_PrevPagamento_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV10OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV11OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV73GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public exportreportwwlote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportreportwwlote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_AreaTrabalhoCod ,
                           int aP1_Lote_ContratadaCod ,
                           int aP2_TFLote_NFe ,
                           int aP3_TFLote_NFe_To ,
                           DateTime aP4_TFLote_DataNfe ,
                           DateTime aP5_TFLote_DataNfe_To ,
                           DateTime aP6_TFLote_PrevPagamento ,
                           DateTime aP7_TFLote_PrevPagamento_To ,
                           short aP8_OrderedBy ,
                           bool aP9_OrderedDsc ,
                           String aP10_GridStateXML )
      {
         this.AV12Lote_AreaTrabalhoCod = aP0_Lote_AreaTrabalhoCod;
         this.AV94Lote_ContratadaCod = aP1_Lote_ContratadaCod;
         this.AV125TFLote_NFe = aP2_TFLote_NFe;
         this.AV126TFLote_NFe_To = aP3_TFLote_NFe_To;
         this.AV127TFLote_DataNfe = aP4_TFLote_DataNfe;
         this.AV128TFLote_DataNfe_To = aP5_TFLote_DataNfe_To;
         this.AV129TFLote_PrevPagamento = aP6_TFLote_PrevPagamento;
         this.AV130TFLote_PrevPagamento_To = aP7_TFLote_PrevPagamento_To;
         this.AV10OrderedBy = aP8_OrderedBy;
         this.AV11OrderedDsc = aP9_OrderedDsc;
         this.AV73GridStateXML = aP10_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Lote_AreaTrabalhoCod ,
                                 int aP1_Lote_ContratadaCod ,
                                 int aP2_TFLote_NFe ,
                                 int aP3_TFLote_NFe_To ,
                                 DateTime aP4_TFLote_DataNfe ,
                                 DateTime aP5_TFLote_DataNfe_To ,
                                 DateTime aP6_TFLote_PrevPagamento ,
                                 DateTime aP7_TFLote_PrevPagamento_To ,
                                 short aP8_OrderedBy ,
                                 bool aP9_OrderedDsc ,
                                 String aP10_GridStateXML )
      {
         exportreportwwlote objexportreportwwlote;
         objexportreportwwlote = new exportreportwwlote();
         objexportreportwwlote.AV12Lote_AreaTrabalhoCod = aP0_Lote_AreaTrabalhoCod;
         objexportreportwwlote.AV94Lote_ContratadaCod = aP1_Lote_ContratadaCod;
         objexportreportwwlote.AV125TFLote_NFe = aP2_TFLote_NFe;
         objexportreportwwlote.AV126TFLote_NFe_To = aP3_TFLote_NFe_To;
         objexportreportwwlote.AV127TFLote_DataNfe = aP4_TFLote_DataNfe;
         objexportreportwwlote.AV128TFLote_DataNfe_To = aP5_TFLote_DataNfe_To;
         objexportreportwwlote.AV129TFLote_PrevPagamento = aP6_TFLote_PrevPagamento;
         objexportreportwwlote.AV130TFLote_PrevPagamento_To = aP7_TFLote_PrevPagamento_To;
         objexportreportwwlote.AV10OrderedBy = aP8_OrderedBy;
         objexportreportwwlote.AV11OrderedDsc = aP9_OrderedDsc;
         objexportreportwwlote.AV73GridStateXML = aP10_GridStateXML;
         objexportreportwwlote.context.SetSubmitInitialConfig(context);
         objexportreportwwlote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportreportwwlote);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportreportwwlote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H690( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         H690( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Consulta de Lotes", 5, Gx_line+5, 785, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( ! (0==AV12Lote_AreaTrabalhoCod) )
         {
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Area de Trabalho", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Lote_AreaTrabalhoCod), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV94Lote_ContratadaCod) )
         {
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("", 5, Gx_line+2, 102, Gx_line+19, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV94Lote_ContratadaCod), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV74GridState.gxTpr_Dynamicfilters.FromXml(AV73GridStateXML, "");
         if ( AV74GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV75GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV74GridState.gxTpr_Dynamicfilters.Item(1));
            AV14DynamicFiltersSelector1 = AV75GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_NUMERO") == 0 )
            {
               AV15Lote_Numero1 = AV75GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15Lote_Numero1)) )
               {
                  AV76Lote_Numero = AV15Lote_Numero1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Lote", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV76Lote_Numero, "")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_NOME") == 0 )
            {
               AV16Lote_Nome1 = AV75GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Lote_Nome1)) )
               {
                  AV77Lote_Nome = AV16Lote_Nome1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Nome", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV77Lote_Nome, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_DATA") == 0 )
            {
               AV17Lote_Data1 = context.localUtil.CToT( AV75GridStateDynamicFilter.gxTpr_Value, 2);
               AV18Lote_Data_To1 = context.localUtil.CToT( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV17Lote_Data1) )
               {
                  AV78FilterLote_DataDescription = "Data do Lote";
                  AV79Lote_Data = AV17Lote_Data1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78FilterLote_DataDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV79Lote_Data, "99/99/99 99:99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV18Lote_Data_To1) )
               {
                  AV78FilterLote_DataDescription = "Data do Lote (at�)";
                  AV79Lote_Data = AV18Lote_Data_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78FilterLote_DataDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV79Lote_Data, "99/99/99 99:99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_USERNOM") == 0 )
            {
               AV19Lote_UserNom1 = AV75GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Lote_UserNom1)) )
               {
                  AV80Lote_UserNom = AV19Lote_UserNom1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Usu�rio", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Lote_UserNom, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_DATAINI") == 0 )
            {
               AV20Lote_DataIni1 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
               AV21Lote_DataIni_To1 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV20Lote_DataIni1) )
               {
                  AV81FilterLote_DataIniDescription = "Data Ini";
                  AV82Lote_DataIni = AV20Lote_DataIni1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81FilterLote_DataIniDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV82Lote_DataIni, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV21Lote_DataIni_To1) )
               {
                  AV81FilterLote_DataIniDescription = "Data Ini (at�)";
                  AV82Lote_DataIni = AV21Lote_DataIni_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81FilterLote_DataIniDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV82Lote_DataIni, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_DATAFIM") == 0 )
            {
               AV22Lote_DataFim1 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
               AV23Lote_DataFim_To1 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV22Lote_DataFim1) )
               {
                  AV83FilterLote_DataFimDescription = "Data Fim";
                  AV84Lote_DataFim = AV22Lote_DataFim1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterLote_DataFimDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV84Lote_DataFim, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV23Lote_DataFim_To1) )
               {
                  AV83FilterLote_DataFimDescription = "Data Fim (at�)";
                  AV84Lote_DataFim = AV23Lote_DataFim_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterLote_DataFimDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV84Lote_DataFim, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_NFE") == 0 )
            {
               AV24Lote_NFe1 = (int)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV24Lote_NFe1) )
               {
                  AV85Lote_NFe = AV24Lote_NFe1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("NFe", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85Lote_NFe), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_DATANFE") == 0 )
            {
               AV25Lote_DataNfe1 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
               AV26Lote_DataNfe_To1 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV25Lote_DataNfe1) )
               {
                  AV86FilterLote_DataNfeDescription = "Data Nfe";
                  AV87Lote_DataNfe = AV25Lote_DataNfe1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV86FilterLote_DataNfeDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV87Lote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV26Lote_DataNfe_To1) )
               {
                  AV86FilterLote_DataNfeDescription = "Data Nfe (at�)";
                  AV87Lote_DataNfe = AV26Lote_DataNfe_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV86FilterLote_DataNfeDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV87Lote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_QTDEDMN") == 0 )
            {
               AV27Lote_QtdeDmn1 = (short)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, "."));
               AV28Lote_QtdeDmn_To1 = (short)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, "."));
               if ( ! (0==AV27Lote_QtdeDmn1) )
               {
                  AV88FilterLote_QtdeDmnDescription = "Qtde Dmn";
                  AV89Lote_QtdeDmn = AV27Lote_QtdeDmn1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterLote_QtdeDmnDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89Lote_QtdeDmn), "ZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (0==AV28Lote_QtdeDmn_To1) )
               {
                  AV88FilterLote_QtdeDmnDescription = "Qtde Dmn (at�)";
                  AV89Lote_QtdeDmn = AV28Lote_QtdeDmn_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterLote_QtdeDmnDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89Lote_QtdeDmn), "ZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_VALOR") == 0 )
            {
               AV29Lote_Valor1 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, ".");
               AV30Lote_Valor_To1 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, ".");
               if ( ! (Convert.ToDecimal(0)==AV29Lote_Valor1) )
               {
                  AV90FilterLote_ValorDescription = "Valor R$";
                  AV91Lote_Valor = AV29Lote_Valor1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV90FilterLote_ValorDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV91Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (Convert.ToDecimal(0)==AV30Lote_Valor_To1) )
               {
                  AV90FilterLote_ValorDescription = "Valor R$ (at�)";
                  AV91Lote_Valor = AV30Lote_Valor_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV90FilterLote_ValorDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV91Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_VALORPF") == 0 )
            {
               AV31Lote_ValorPF1 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, ".");
               AV32Lote_ValorPF_To1 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, ".");
               if ( ! (Convert.ToDecimal(0)==AV31Lote_ValorPF1) )
               {
                  AV92FilterLote_ValorPFDescription = "Valor PF R$";
                  AV93Lote_ValorPF = AV31Lote_ValorPF1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92FilterLote_ValorPFDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV93Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (Convert.ToDecimal(0)==AV32Lote_ValorPF_To1) )
               {
                  AV92FilterLote_ValorPFDescription = "Valor PF R$ (at�)";
                  AV93Lote_ValorPF = AV32Lote_ValorPF_To1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92FilterLote_ValorPFDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV93Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "LOTE_CONTRATADANOM") == 0 )
            {
               AV95Lote_ContratadaNom1 = AV75GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95Lote_ContratadaNom1)) )
               {
                  AV96Lote_ContratadaNom = AV95Lote_ContratadaNom1;
                  H690( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Contratada", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96Lote_ContratadaNom, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV74GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV75GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV74GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV75GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_NUMERO") == 0 )
               {
                  AV35Lote_Numero2 = AV75GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Lote_Numero2)) )
                  {
                     AV76Lote_Numero = AV35Lote_Numero2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Lote", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV76Lote_Numero, "")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_NOME") == 0 )
               {
                  AV36Lote_Nome2 = AV75GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Lote_Nome2)) )
                  {
                     AV77Lote_Nome = AV36Lote_Nome2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Nome", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV77Lote_Nome, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_DATA") == 0 )
               {
                  AV37Lote_Data2 = context.localUtil.CToT( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                  AV38Lote_Data_To2 = context.localUtil.CToT( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV37Lote_Data2) )
                  {
                     AV78FilterLote_DataDescription = "Data do Lote";
                     AV79Lote_Data = AV37Lote_Data2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78FilterLote_DataDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV79Lote_Data, "99/99/99 99:99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV38Lote_Data_To2) )
                  {
                     AV78FilterLote_DataDescription = "Data do Lote (at�)";
                     AV79Lote_Data = AV38Lote_Data_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78FilterLote_DataDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV79Lote_Data, "99/99/99 99:99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_USERNOM") == 0 )
               {
                  AV39Lote_UserNom2 = AV75GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Lote_UserNom2)) )
                  {
                     AV80Lote_UserNom = AV39Lote_UserNom2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Usu�rio", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Lote_UserNom, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_DATAINI") == 0 )
               {
                  AV40Lote_DataIni2 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                  AV41Lote_DataIni_To2 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV40Lote_DataIni2) )
                  {
                     AV81FilterLote_DataIniDescription = "Data Ini";
                     AV82Lote_DataIni = AV40Lote_DataIni2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81FilterLote_DataIniDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV82Lote_DataIni, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV41Lote_DataIni_To2) )
                  {
                     AV81FilterLote_DataIniDescription = "Data Ini (at�)";
                     AV82Lote_DataIni = AV41Lote_DataIni_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81FilterLote_DataIniDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV82Lote_DataIni, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_DATAFIM") == 0 )
               {
                  AV42Lote_DataFim2 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                  AV43Lote_DataFim_To2 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV42Lote_DataFim2) )
                  {
                     AV83FilterLote_DataFimDescription = "Data Fim";
                     AV84Lote_DataFim = AV42Lote_DataFim2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterLote_DataFimDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV84Lote_DataFim, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV43Lote_DataFim_To2) )
                  {
                     AV83FilterLote_DataFimDescription = "Data Fim (at�)";
                     AV84Lote_DataFim = AV43Lote_DataFim_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterLote_DataFimDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV84Lote_DataFim, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_NFE") == 0 )
               {
                  AV44Lote_NFe2 = (int)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV44Lote_NFe2) )
                  {
                     AV85Lote_NFe = AV44Lote_NFe2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("NFe", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85Lote_NFe), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_DATANFE") == 0 )
               {
                  AV45Lote_DataNfe2 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                  AV46Lote_DataNfe_To2 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV45Lote_DataNfe2) )
                  {
                     AV86FilterLote_DataNfeDescription = "Data Nfe";
                     AV87Lote_DataNfe = AV45Lote_DataNfe2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV86FilterLote_DataNfeDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV87Lote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV46Lote_DataNfe_To2) )
                  {
                     AV86FilterLote_DataNfeDescription = "Data Nfe (at�)";
                     AV87Lote_DataNfe = AV46Lote_DataNfe_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV86FilterLote_DataNfeDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV87Lote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_QTDEDMN") == 0 )
               {
                  AV47Lote_QtdeDmn2 = (short)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, "."));
                  AV48Lote_QtdeDmn_To2 = (short)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, "."));
                  if ( ! (0==AV47Lote_QtdeDmn2) )
                  {
                     AV88FilterLote_QtdeDmnDescription = "Qtde Dmn";
                     AV89Lote_QtdeDmn = AV47Lote_QtdeDmn2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterLote_QtdeDmnDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89Lote_QtdeDmn), "ZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (0==AV48Lote_QtdeDmn_To2) )
                  {
                     AV88FilterLote_QtdeDmnDescription = "Qtde Dmn (at�)";
                     AV89Lote_QtdeDmn = AV48Lote_QtdeDmn_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterLote_QtdeDmnDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89Lote_QtdeDmn), "ZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_VALOR") == 0 )
               {
                  AV49Lote_Valor2 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, ".");
                  AV50Lote_Valor_To2 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, ".");
                  if ( ! (Convert.ToDecimal(0)==AV49Lote_Valor2) )
                  {
                     AV90FilterLote_ValorDescription = "Valor R$";
                     AV91Lote_Valor = AV49Lote_Valor2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV90FilterLote_ValorDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV91Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (Convert.ToDecimal(0)==AV50Lote_Valor_To2) )
                  {
                     AV90FilterLote_ValorDescription = "Valor R$ (at�)";
                     AV91Lote_Valor = AV50Lote_Valor_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV90FilterLote_ValorDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV91Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_VALORPF") == 0 )
               {
                  AV51Lote_ValorPF2 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, ".");
                  AV52Lote_ValorPF_To2 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, ".");
                  if ( ! (Convert.ToDecimal(0)==AV51Lote_ValorPF2) )
                  {
                     AV92FilterLote_ValorPFDescription = "Valor PF R$";
                     AV93Lote_ValorPF = AV51Lote_ValorPF2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92FilterLote_ValorPFDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV93Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (Convert.ToDecimal(0)==AV52Lote_ValorPF_To2) )
                  {
                     AV92FilterLote_ValorPFDescription = "Valor PF R$ (at�)";
                     AV93Lote_ValorPF = AV52Lote_ValorPF_To2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92FilterLote_ValorPFDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV93Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "LOTE_CONTRATADANOM") == 0 )
               {
                  AV97Lote_ContratadaNom2 = AV75GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97Lote_ContratadaNom2)) )
                  {
                     AV96Lote_ContratadaNom = AV97Lote_ContratadaNom2;
                     H690( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Contratada", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96Lote_ContratadaNom, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               if ( AV74GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV53DynamicFiltersEnabled3 = true;
                  AV75GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV74GridState.gxTpr_Dynamicfilters.Item(3));
                  AV54DynamicFiltersSelector3 = AV75GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_NUMERO") == 0 )
                  {
                     AV55Lote_Numero3 = AV75GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Lote_Numero3)) )
                     {
                        AV76Lote_Numero = AV55Lote_Numero3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Lote", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV76Lote_Numero, "")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_NOME") == 0 )
                  {
                     AV56Lote_Nome3 = AV75GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Lote_Nome3)) )
                     {
                        AV77Lote_Nome = AV56Lote_Nome3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Nome", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV77Lote_Nome, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_DATA") == 0 )
                  {
                     AV57Lote_Data3 = context.localUtil.CToT( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                     AV58Lote_Data_To3 = context.localUtil.CToT( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV57Lote_Data3) )
                     {
                        AV78FilterLote_DataDescription = "Data do Lote";
                        AV79Lote_Data = AV57Lote_Data3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78FilterLote_DataDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV79Lote_Data, "99/99/99 99:99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV58Lote_Data_To3) )
                     {
                        AV78FilterLote_DataDescription = "Data do Lote (at�)";
                        AV79Lote_Data = AV58Lote_Data_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78FilterLote_DataDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV79Lote_Data, "99/99/99 99:99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_USERNOM") == 0 )
                  {
                     AV59Lote_UserNom3 = AV75GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Lote_UserNom3)) )
                     {
                        AV80Lote_UserNom = AV59Lote_UserNom3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Usu�rio", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Lote_UserNom, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_DATAINI") == 0 )
                  {
                     AV60Lote_DataIni3 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                     AV61Lote_DataIni_To3 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV60Lote_DataIni3) )
                     {
                        AV81FilterLote_DataIniDescription = "Data Ini";
                        AV82Lote_DataIni = AV60Lote_DataIni3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81FilterLote_DataIniDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV82Lote_DataIni, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV61Lote_DataIni_To3) )
                     {
                        AV81FilterLote_DataIniDescription = "Data Ini (at�)";
                        AV82Lote_DataIni = AV61Lote_DataIni_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81FilterLote_DataIniDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV82Lote_DataIni, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_DATAFIM") == 0 )
                  {
                     AV62Lote_DataFim3 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                     AV63Lote_DataFim_To3 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV62Lote_DataFim3) )
                     {
                        AV83FilterLote_DataFimDescription = "Data Fim";
                        AV84Lote_DataFim = AV62Lote_DataFim3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterLote_DataFimDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV84Lote_DataFim, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV63Lote_DataFim_To3) )
                     {
                        AV83FilterLote_DataFimDescription = "Data Fim (at�)";
                        AV84Lote_DataFim = AV63Lote_DataFim_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterLote_DataFimDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV84Lote_DataFim, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_NFE") == 0 )
                  {
                     AV64Lote_NFe3 = (int)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV64Lote_NFe3) )
                     {
                        AV85Lote_NFe = AV64Lote_NFe3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("NFe", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85Lote_NFe), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_DATANFE") == 0 )
                  {
                     AV65Lote_DataNfe3 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Value, 2);
                     AV66Lote_DataNfe_To3 = context.localUtil.CToD( AV75GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV65Lote_DataNfe3) )
                     {
                        AV86FilterLote_DataNfeDescription = "Data Nfe";
                        AV87Lote_DataNfe = AV65Lote_DataNfe3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV86FilterLote_DataNfeDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV87Lote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV66Lote_DataNfe_To3) )
                     {
                        AV86FilterLote_DataNfeDescription = "Data Nfe (at�)";
                        AV87Lote_DataNfe = AV66Lote_DataNfe_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV86FilterLote_DataNfeDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV87Lote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_QTDEDMN") == 0 )
                  {
                     AV67Lote_QtdeDmn3 = (short)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, "."));
                     AV68Lote_QtdeDmn_To3 = (short)(NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, "."));
                     if ( ! (0==AV67Lote_QtdeDmn3) )
                     {
                        AV88FilterLote_QtdeDmnDescription = "Qtde Dmn";
                        AV89Lote_QtdeDmn = AV67Lote_QtdeDmn3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterLote_QtdeDmnDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89Lote_QtdeDmn), "ZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (0==AV68Lote_QtdeDmn_To3) )
                     {
                        AV88FilterLote_QtdeDmnDescription = "Qtde Dmn (at�)";
                        AV89Lote_QtdeDmn = AV68Lote_QtdeDmn_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterLote_QtdeDmnDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV89Lote_QtdeDmn), "ZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_VALOR") == 0 )
                  {
                     AV69Lote_Valor3 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, ".");
                     AV70Lote_Valor_To3 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, ".");
                     if ( ! (Convert.ToDecimal(0)==AV69Lote_Valor3) )
                     {
                        AV90FilterLote_ValorDescription = "Valor R$";
                        AV91Lote_Valor = AV69Lote_Valor3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV90FilterLote_ValorDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV91Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (Convert.ToDecimal(0)==AV70Lote_Valor_To3) )
                     {
                        AV90FilterLote_ValorDescription = "Valor R$ (at�)";
                        AV91Lote_Valor = AV70Lote_Valor_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV90FilterLote_ValorDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV91Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_VALORPF") == 0 )
                  {
                     AV71Lote_ValorPF3 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Value, ".");
                     AV72Lote_ValorPF_To3 = NumberUtil.Val( AV75GridStateDynamicFilter.gxTpr_Valueto, ".");
                     if ( ! (Convert.ToDecimal(0)==AV71Lote_ValorPF3) )
                     {
                        AV92FilterLote_ValorPFDescription = "Valor PF R$";
                        AV93Lote_ValorPF = AV71Lote_ValorPF3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92FilterLote_ValorPFDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV93Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (Convert.ToDecimal(0)==AV72Lote_ValorPF_To3) )
                     {
                        AV92FilterLote_ValorPFDescription = "Valor PF R$ (at�)";
                        AV93Lote_ValorPF = AV72Lote_ValorPF_To3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92FilterLote_ValorPFDescription, "")), 5, Gx_line+2, 102, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV93Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV54DynamicFiltersSelector3, "LOTE_CONTRATADANOM") == 0 )
                  {
                     AV98Lote_ContratadaNom3 = AV75GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98Lote_ContratadaNom3)) )
                     {
                        AV96Lote_ContratadaNom = AV98Lote_ContratadaNom3;
                        H690( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Contratada", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96Lote_ContratadaNom, "@!")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
               }
            }
         }
         if ( ! ( (0==AV125TFLote_NFe) && (0==AV126TFLote_NFe_To) ) )
         {
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("NFe", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV125TFLote_NFe), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("NFe (At�)", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV126TFLote_NFe_To), "ZZZZZ9")), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV127TFLote_DataNfe) && (DateTime.MinValue==AV128TFLote_DataNfe_To) ) )
         {
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Emiss�o", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV127TFLote_DataNfe, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Emiss�o (At�)", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV128TFLote_DataNfe_To, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV129TFLote_PrevPagamento) && (DateTime.MinValue==AV130TFLote_PrevPagamento_To) ) )
         {
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Previss�o", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV129TFLote_PrevPagamento, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H690( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Previss�o (At�)", 5, Gx_line+2, 102, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV130TFLote_PrevPagamento_To, "99/99/99"), 104, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         H690( false, 35) ;
         getPrinter().GxDrawLine(5, Gx_line+30, 60, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(65, Gx_line+30, 120, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(125, Gx_line+30, 180, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(185, Gx_line+30, 240, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(245, Gx_line+30, 300, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(305, Gx_line+30, 360, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(365, Gx_line+30, 420, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(425, Gx_line+30, 480, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(485, Gx_line+30, 540, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(545, Gx_line+30, 600, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(605, Gx_line+30, 660, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(665, Gx_line+30, 720, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(725, Gx_line+30, 780, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Lote", 5, Gx_line+15, 60, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Nome", 65, Gx_line+15, 120, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contratada", 125, Gx_line+15, 180, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Data do Lote", 185, Gx_line+15, 240, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Usu�rio", 245, Gx_line+15, 300, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Per�odo", 305, Gx_line+15, 360, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("", 365, Gx_line+15, 420, Gx_line+32, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Status", 425, Gx_line+15, 480, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Qtde Dmn", 485, Gx_line+15, 540, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("Valor R$", 545, Gx_line+15, 600, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("NFe", 605, Gx_line+15, 660, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("Emiss�o", 665, Gx_line+15, 720, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Previss�o", 725, Gx_line+15, 780, Gx_line+29, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+35);
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV134WWLoteDS_1_Lote_areatrabalhocod = AV12Lote_AreaTrabalhoCod;
         AV135WWLoteDS_2_Lote_contratadacod = AV94Lote_ContratadaCod;
         AV136WWLoteDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV137WWLoteDS_4_Lote_numero1 = AV15Lote_Numero1;
         AV138WWLoteDS_5_Lote_nome1 = AV16Lote_Nome1;
         AV139WWLoteDS_6_Lote_data1 = AV17Lote_Data1;
         AV140WWLoteDS_7_Lote_data_to1 = AV18Lote_Data_To1;
         AV141WWLoteDS_8_Lote_usernom1 = AV19Lote_UserNom1;
         AV142WWLoteDS_9_Lote_dataini1 = AV20Lote_DataIni1;
         AV143WWLoteDS_10_Lote_dataini_to1 = AV21Lote_DataIni_To1;
         AV144WWLoteDS_11_Lote_datafim1 = AV22Lote_DataFim1;
         AV145WWLoteDS_12_Lote_datafim_to1 = AV23Lote_DataFim_To1;
         AV146WWLoteDS_13_Lote_nfe1 = AV24Lote_NFe1;
         AV147WWLoteDS_14_Lote_datanfe1 = AV25Lote_DataNfe1;
         AV148WWLoteDS_15_Lote_datanfe_to1 = AV26Lote_DataNfe_To1;
         AV149WWLoteDS_16_Lote_qtdedmn1 = AV27Lote_QtdeDmn1;
         AV150WWLoteDS_17_Lote_qtdedmn_to1 = AV28Lote_QtdeDmn_To1;
         AV151WWLoteDS_18_Lote_valor1 = AV29Lote_Valor1;
         AV152WWLoteDS_19_Lote_valor_to1 = AV30Lote_Valor_To1;
         AV153WWLoteDS_20_Lote_valorpf1 = AV31Lote_ValorPF1;
         AV154WWLoteDS_21_Lote_valorpf_to1 = AV32Lote_ValorPF_To1;
         AV155WWLoteDS_22_Lote_contratadanom1 = AV95Lote_ContratadaNom1;
         AV156WWLoteDS_23_Dynamicfiltersenabled2 = AV33DynamicFiltersEnabled2;
         AV157WWLoteDS_24_Dynamicfiltersselector2 = AV34DynamicFiltersSelector2;
         AV158WWLoteDS_25_Lote_numero2 = AV35Lote_Numero2;
         AV159WWLoteDS_26_Lote_nome2 = AV36Lote_Nome2;
         AV160WWLoteDS_27_Lote_data2 = AV37Lote_Data2;
         AV161WWLoteDS_28_Lote_data_to2 = AV38Lote_Data_To2;
         AV162WWLoteDS_29_Lote_usernom2 = AV39Lote_UserNom2;
         AV163WWLoteDS_30_Lote_dataini2 = AV40Lote_DataIni2;
         AV164WWLoteDS_31_Lote_dataini_to2 = AV41Lote_DataIni_To2;
         AV165WWLoteDS_32_Lote_datafim2 = AV42Lote_DataFim2;
         AV166WWLoteDS_33_Lote_datafim_to2 = AV43Lote_DataFim_To2;
         AV167WWLoteDS_34_Lote_nfe2 = AV44Lote_NFe2;
         AV168WWLoteDS_35_Lote_datanfe2 = AV45Lote_DataNfe2;
         AV169WWLoteDS_36_Lote_datanfe_to2 = AV46Lote_DataNfe_To2;
         AV170WWLoteDS_37_Lote_qtdedmn2 = AV47Lote_QtdeDmn2;
         AV171WWLoteDS_38_Lote_qtdedmn_to2 = AV48Lote_QtdeDmn_To2;
         AV172WWLoteDS_39_Lote_valor2 = AV49Lote_Valor2;
         AV173WWLoteDS_40_Lote_valor_to2 = AV50Lote_Valor_To2;
         AV174WWLoteDS_41_Lote_valorpf2 = AV51Lote_ValorPF2;
         AV175WWLoteDS_42_Lote_valorpf_to2 = AV52Lote_ValorPF_To2;
         AV176WWLoteDS_43_Lote_contratadanom2 = AV97Lote_ContratadaNom2;
         AV177WWLoteDS_44_Dynamicfiltersenabled3 = AV53DynamicFiltersEnabled3;
         AV178WWLoteDS_45_Dynamicfiltersselector3 = AV54DynamicFiltersSelector3;
         AV179WWLoteDS_46_Lote_numero3 = AV55Lote_Numero3;
         AV180WWLoteDS_47_Lote_nome3 = AV56Lote_Nome3;
         AV181WWLoteDS_48_Lote_data3 = AV57Lote_Data3;
         AV182WWLoteDS_49_Lote_data_to3 = AV58Lote_Data_To3;
         AV183WWLoteDS_50_Lote_usernom3 = AV59Lote_UserNom3;
         AV184WWLoteDS_51_Lote_dataini3 = AV60Lote_DataIni3;
         AV185WWLoteDS_52_Lote_dataini_to3 = AV61Lote_DataIni_To3;
         AV186WWLoteDS_53_Lote_datafim3 = AV62Lote_DataFim3;
         AV187WWLoteDS_54_Lote_datafim_to3 = AV63Lote_DataFim_To3;
         AV188WWLoteDS_55_Lote_nfe3 = AV64Lote_NFe3;
         AV189WWLoteDS_56_Lote_datanfe3 = AV65Lote_DataNfe3;
         AV190WWLoteDS_57_Lote_datanfe_to3 = AV66Lote_DataNfe_To3;
         AV191WWLoteDS_58_Lote_qtdedmn3 = AV67Lote_QtdeDmn3;
         AV192WWLoteDS_59_Lote_qtdedmn_to3 = AV68Lote_QtdeDmn_To3;
         AV193WWLoteDS_60_Lote_valor3 = AV69Lote_Valor3;
         AV194WWLoteDS_61_Lote_valor_to3 = AV70Lote_Valor_To3;
         AV195WWLoteDS_62_Lote_valorpf3 = AV71Lote_ValorPF3;
         AV196WWLoteDS_63_Lote_valorpf_to3 = AV72Lote_ValorPF_To3;
         AV197WWLoteDS_64_Lote_contratadanom3 = AV98Lote_ContratadaNom3;
         AV198WWLoteDS_65_Tflote_nfe = AV125TFLote_NFe;
         AV199WWLoteDS_66_Tflote_nfe_to = AV126TFLote_NFe_To;
         AV200WWLoteDS_67_Tflote_datanfe = AV127TFLote_DataNfe;
         AV201WWLoteDS_68_Tflote_datanfe_to = AV128TFLote_DataNfe_To;
         AV202WWLoteDS_69_Tflote_prevpagamento = AV129TFLote_PrevPagamento;
         AV203WWLoteDS_70_Tflote_prevpagamento_to = AV130TFLote_PrevPagamento_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV136WWLoteDS_3_Dynamicfiltersselector1 ,
                                              AV137WWLoteDS_4_Lote_numero1 ,
                                              AV138WWLoteDS_5_Lote_nome1 ,
                                              AV139WWLoteDS_6_Lote_data1 ,
                                              AV140WWLoteDS_7_Lote_data_to1 ,
                                              AV141WWLoteDS_8_Lote_usernom1 ,
                                              AV146WWLoteDS_13_Lote_nfe1 ,
                                              AV147WWLoteDS_14_Lote_datanfe1 ,
                                              AV148WWLoteDS_15_Lote_datanfe_to1 ,
                                              AV149WWLoteDS_16_Lote_qtdedmn1 ,
                                              AV150WWLoteDS_17_Lote_qtdedmn_to1 ,
                                              AV153WWLoteDS_20_Lote_valorpf1 ,
                                              AV154WWLoteDS_21_Lote_valorpf_to1 ,
                                              AV156WWLoteDS_23_Dynamicfiltersenabled2 ,
                                              AV157WWLoteDS_24_Dynamicfiltersselector2 ,
                                              AV158WWLoteDS_25_Lote_numero2 ,
                                              AV159WWLoteDS_26_Lote_nome2 ,
                                              AV160WWLoteDS_27_Lote_data2 ,
                                              AV161WWLoteDS_28_Lote_data_to2 ,
                                              AV162WWLoteDS_29_Lote_usernom2 ,
                                              AV167WWLoteDS_34_Lote_nfe2 ,
                                              AV168WWLoteDS_35_Lote_datanfe2 ,
                                              AV169WWLoteDS_36_Lote_datanfe_to2 ,
                                              AV170WWLoteDS_37_Lote_qtdedmn2 ,
                                              AV171WWLoteDS_38_Lote_qtdedmn_to2 ,
                                              AV174WWLoteDS_41_Lote_valorpf2 ,
                                              AV175WWLoteDS_42_Lote_valorpf_to2 ,
                                              AV177WWLoteDS_44_Dynamicfiltersenabled3 ,
                                              AV178WWLoteDS_45_Dynamicfiltersselector3 ,
                                              AV179WWLoteDS_46_Lote_numero3 ,
                                              AV180WWLoteDS_47_Lote_nome3 ,
                                              AV181WWLoteDS_48_Lote_data3 ,
                                              AV182WWLoteDS_49_Lote_data_to3 ,
                                              AV183WWLoteDS_50_Lote_usernom3 ,
                                              AV188WWLoteDS_55_Lote_nfe3 ,
                                              AV189WWLoteDS_56_Lote_datanfe3 ,
                                              AV190WWLoteDS_57_Lote_datanfe_to3 ,
                                              AV191WWLoteDS_58_Lote_qtdedmn3 ,
                                              AV192WWLoteDS_59_Lote_qtdedmn_to3 ,
                                              AV195WWLoteDS_62_Lote_valorpf3 ,
                                              AV196WWLoteDS_63_Lote_valorpf_to3 ,
                                              AV198WWLoteDS_65_Tflote_nfe ,
                                              AV199WWLoteDS_66_Tflote_nfe_to ,
                                              AV200WWLoteDS_67_Tflote_datanfe ,
                                              AV201WWLoteDS_68_Tflote_datanfe_to ,
                                              AV202WWLoteDS_69_Tflote_prevpagamento ,
                                              AV203WWLoteDS_70_Tflote_prevpagamento_to ,
                                              A562Lote_Numero ,
                                              A563Lote_Nome ,
                                              A564Lote_Data ,
                                              A561Lote_UserNom ,
                                              A673Lote_NFe ,
                                              A674Lote_DataNfe ,
                                              A569Lote_QtdeDmn ,
                                              A565Lote_ValorPF ,
                                              A857Lote_PrevPagamento ,
                                              AV10OrderedBy ,
                                              AV11OrderedDsc ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV142WWLoteDS_9_Lote_dataini1 ,
                                              A567Lote_DataIni ,
                                              AV143WWLoteDS_10_Lote_dataini_to1 ,
                                              AV144WWLoteDS_11_Lote_datafim1 ,
                                              A568Lote_DataFim ,
                                              AV145WWLoteDS_12_Lote_datafim_to1 ,
                                              AV151WWLoteDS_18_Lote_valor1 ,
                                              A572Lote_Valor ,
                                              AV152WWLoteDS_19_Lote_valor_to1 ,
                                              AV155WWLoteDS_22_Lote_contratadanom1 ,
                                              A1748Lote_ContratadaNom ,
                                              AV163WWLoteDS_30_Lote_dataini2 ,
                                              AV164WWLoteDS_31_Lote_dataini_to2 ,
                                              AV165WWLoteDS_32_Lote_datafim2 ,
                                              AV166WWLoteDS_33_Lote_datafim_to2 ,
                                              AV172WWLoteDS_39_Lote_valor2 ,
                                              AV173WWLoteDS_40_Lote_valor_to2 ,
                                              AV176WWLoteDS_43_Lote_contratadanom2 ,
                                              AV184WWLoteDS_51_Lote_dataini3 ,
                                              AV185WWLoteDS_52_Lote_dataini_to3 ,
                                              AV186WWLoteDS_53_Lote_datafim3 ,
                                              AV187WWLoteDS_54_Lote_datafim_to3 ,
                                              AV193WWLoteDS_60_Lote_valor3 ,
                                              AV194WWLoteDS_61_Lote_valor_to3 ,
                                              AV197WWLoteDS_64_Lote_contratadanom3 ,
                                              AV74GridState.gxTpr_Dynamicfilters.Count ,
                                              Gx_date ,
                                              A595Lote_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV155WWLoteDS_22_Lote_contratadanom1 = StringUtil.PadR( StringUtil.RTrim( AV155WWLoteDS_22_Lote_contratadanom1), 50, "%");
         lV176WWLoteDS_43_Lote_contratadanom2 = StringUtil.PadR( StringUtil.RTrim( AV176WWLoteDS_43_Lote_contratadanom2), 50, "%");
         lV197WWLoteDS_64_Lote_contratadanom3 = StringUtil.PadR( StringUtil.RTrim( AV197WWLoteDS_64_Lote_contratadanom3), 50, "%");
         lV137WWLoteDS_4_Lote_numero1 = StringUtil.PadR( StringUtil.RTrim( AV137WWLoteDS_4_Lote_numero1), 10, "%");
         lV138WWLoteDS_5_Lote_nome1 = StringUtil.PadR( StringUtil.RTrim( AV138WWLoteDS_5_Lote_nome1), 50, "%");
         lV141WWLoteDS_8_Lote_usernom1 = StringUtil.PadR( StringUtil.RTrim( AV141WWLoteDS_8_Lote_usernom1), 100, "%");
         lV158WWLoteDS_25_Lote_numero2 = StringUtil.PadR( StringUtil.RTrim( AV158WWLoteDS_25_Lote_numero2), 10, "%");
         lV159WWLoteDS_26_Lote_nome2 = StringUtil.PadR( StringUtil.RTrim( AV159WWLoteDS_26_Lote_nome2), 50, "%");
         lV162WWLoteDS_29_Lote_usernom2 = StringUtil.PadR( StringUtil.RTrim( AV162WWLoteDS_29_Lote_usernom2), 100, "%");
         lV179WWLoteDS_46_Lote_numero3 = StringUtil.PadR( StringUtil.RTrim( AV179WWLoteDS_46_Lote_numero3), 10, "%");
         lV180WWLoteDS_47_Lote_nome3 = StringUtil.PadR( StringUtil.RTrim( AV180WWLoteDS_47_Lote_nome3), 50, "%");
         lV183WWLoteDS_50_Lote_usernom3 = StringUtil.PadR( StringUtil.RTrim( AV183WWLoteDS_50_Lote_usernom3), 100, "%");
         /* Using cursor P00699 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Userehcontratante, AV9WWPContext.gxTpr_Contratada_codigo, AV136WWLoteDS_3_Dynamicfiltersselector1, AV142WWLoteDS_9_Lote_dataini1, AV142WWLoteDS_9_Lote_dataini1, AV136WWLoteDS_3_Dynamicfiltersselector1, AV143WWLoteDS_10_Lote_dataini_to1, AV143WWLoteDS_10_Lote_dataini_to1, AV136WWLoteDS_3_Dynamicfiltersselector1, AV144WWLoteDS_11_Lote_datafim1, AV144WWLoteDS_11_Lote_datafim1, AV136WWLoteDS_3_Dynamicfiltersselector1, AV145WWLoteDS_12_Lote_datafim_to1, AV145WWLoteDS_12_Lote_datafim_to1, AV136WWLoteDS_3_Dynamicfiltersselector1, AV155WWLoteDS_22_Lote_contratadanom1, lV155WWLoteDS_22_Lote_contratadanom1, AV156WWLoteDS_23_Dynamicfiltersenabled2, AV157WWLoteDS_24_Dynamicfiltersselector2, AV163WWLoteDS_30_Lote_dataini2, AV163WWLoteDS_30_Lote_dataini2, AV156WWLoteDS_23_Dynamicfiltersenabled2, AV157WWLoteDS_24_Dynamicfiltersselector2, AV164WWLoteDS_31_Lote_dataini_to2, AV164WWLoteDS_31_Lote_dataini_to2, AV156WWLoteDS_23_Dynamicfiltersenabled2, AV157WWLoteDS_24_Dynamicfiltersselector2, AV165WWLoteDS_32_Lote_datafim2, AV165WWLoteDS_32_Lote_datafim2, AV156WWLoteDS_23_Dynamicfiltersenabled2, AV157WWLoteDS_24_Dynamicfiltersselector2, AV166WWLoteDS_33_Lote_datafim_to2, AV166WWLoteDS_33_Lote_datafim_to2, AV156WWLoteDS_23_Dynamicfiltersenabled2, AV157WWLoteDS_24_Dynamicfiltersselector2, AV176WWLoteDS_43_Lote_contratadanom2, lV176WWLoteDS_43_Lote_contratadanom2, AV177WWLoteDS_44_Dynamicfiltersenabled3, AV178WWLoteDS_45_Dynamicfiltersselector3, AV184WWLoteDS_51_Lote_dataini3, AV184WWLoteDS_51_Lote_dataini3, AV177WWLoteDS_44_Dynamicfiltersenabled3, AV178WWLoteDS_45_Dynamicfiltersselector3, AV185WWLoteDS_52_Lote_dataini_to3, AV185WWLoteDS_52_Lote_dataini_to3, AV177WWLoteDS_44_Dynamicfiltersenabled3, AV178WWLoteDS_45_Dynamicfiltersselector3, AV186WWLoteDS_53_Lote_datafim3, AV186WWLoteDS_53_Lote_datafim3, AV177WWLoteDS_44_Dynamicfiltersenabled3, AV178WWLoteDS_45_Dynamicfiltersselector3, AV187WWLoteDS_54_Lote_datafim_to3, AV187WWLoteDS_54_Lote_datafim_to3, AV177WWLoteDS_44_Dynamicfiltersenabled3, AV178WWLoteDS_45_Dynamicfiltersselector3, AV197WWLoteDS_64_Lote_contratadanom3, lV197WWLoteDS_64_Lote_contratadanom3, AV9WWPContext.gxTpr_Areatrabalho_codigo, lV137WWLoteDS_4_Lote_numero1, lV138WWLoteDS_5_Lote_nome1, AV139WWLoteDS_6_Lote_data1, AV140WWLoteDS_7_Lote_data_to1, lV141WWLoteDS_8_Lote_usernom1, AV146WWLoteDS_13_Lote_nfe1, AV147WWLoteDS_14_Lote_datanfe1, AV148WWLoteDS_15_Lote_datanfe_to1, AV149WWLoteDS_16_Lote_qtdedmn1, AV150WWLoteDS_17_Lote_qtdedmn_to1, AV153WWLoteDS_20_Lote_valorpf1, AV154WWLoteDS_21_Lote_valorpf_to1, lV158WWLoteDS_25_Lote_numero2, lV159WWLoteDS_26_Lote_nome2, AV160WWLoteDS_27_Lote_data2, AV161WWLoteDS_28_Lote_data_to2, lV162WWLoteDS_29_Lote_usernom2, AV167WWLoteDS_34_Lote_nfe2, AV168WWLoteDS_35_Lote_datanfe2, AV169WWLoteDS_36_Lote_datanfe_to2, AV170WWLoteDS_37_Lote_qtdedmn2, AV171WWLoteDS_38_Lote_qtdedmn_to2, AV174WWLoteDS_41_Lote_valorpf2, AV175WWLoteDS_42_Lote_valorpf_to2, lV179WWLoteDS_46_Lote_numero3, lV180WWLoteDS_47_Lote_nome3, AV181WWLoteDS_48_Lote_data3, AV182WWLoteDS_49_Lote_data_to3, lV183WWLoteDS_50_Lote_usernom3, AV188WWLoteDS_55_Lote_nfe3, AV189WWLoteDS_56_Lote_datanfe3, AV190WWLoteDS_57_Lote_datanfe_to3, AV191WWLoteDS_58_Lote_qtdedmn3, AV192WWLoteDS_59_Lote_qtdedmn_to3, AV195WWLoteDS_62_Lote_valorpf3, AV196WWLoteDS_63_Lote_valorpf_to3, AV198WWLoteDS_65_Tflote_nfe, AV199WWLoteDS_66_Tflote_nfe_to, AV200WWLoteDS_67_Tflote_datanfe, AV201WWLoteDS_68_Tflote_datanfe_to, AV202WWLoteDS_69_Tflote_prevpagamento, AV203WWLoteDS_70_Tflote_prevpagamento_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A559Lote_UserCod = P00699_A559Lote_UserCod[0];
            A560Lote_PessoaCod = P00699_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00699_n560Lote_PessoaCod[0];
            A857Lote_PrevPagamento = P00699_A857Lote_PrevPagamento[0];
            n857Lote_PrevPagamento = P00699_n857Lote_PrevPagamento[0];
            A565Lote_ValorPF = P00699_A565Lote_ValorPF[0];
            A674Lote_DataNfe = P00699_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P00699_n674Lote_DataNfe[0];
            A673Lote_NFe = P00699_A673Lote_NFe[0];
            n673Lote_NFe = P00699_n673Lote_NFe[0];
            A561Lote_UserNom = P00699_A561Lote_UserNom[0];
            n561Lote_UserNom = P00699_n561Lote_UserNom[0];
            A564Lote_Data = P00699_A564Lote_Data[0];
            A563Lote_Nome = P00699_A563Lote_Nome[0];
            A562Lote_Numero = P00699_A562Lote_Numero[0];
            A595Lote_AreaTrabalhoCod = P00699_A595Lote_AreaTrabalhoCod[0];
            A596Lote_Codigo = P00699_A596Lote_Codigo[0];
            A1748Lote_ContratadaNom = P00699_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00699_n1748Lote_ContratadaNom[0];
            A569Lote_QtdeDmn = P00699_A569Lote_QtdeDmn[0];
            A568Lote_DataFim = P00699_A568Lote_DataFim[0];
            A567Lote_DataIni = P00699_A567Lote_DataIni[0];
            A1231Lote_ContratadaCod = P00699_A1231Lote_ContratadaCod[0];
            A575Lote_Status = P00699_A575Lote_Status[0];
            A1057Lote_ValorGlosas = P00699_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00699_n1057Lote_ValorGlosas[0];
            A560Lote_PessoaCod = P00699_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00699_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00699_A561Lote_UserNom[0];
            n561Lote_UserNom = P00699_n561Lote_UserNom[0];
            A1748Lote_ContratadaNom = P00699_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00699_n1748Lote_ContratadaNom[0];
            A1057Lote_ValorGlosas = P00699_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00699_n1057Lote_ValorGlosas[0];
            A569Lote_QtdeDmn = P00699_A569Lote_QtdeDmn[0];
            A1231Lote_ContratadaCod = P00699_A1231Lote_ContratadaCod[0];
            A575Lote_Status = P00699_A575Lote_Status[0];
            A568Lote_DataFim = P00699_A568Lote_DataFim[0];
            A567Lote_DataIni = P00699_A567Lote_DataIni[0];
            if ( ( AV74GridState.gxTpr_Dynamicfilters.Count >= 1 ) || ( ( DateTimeUtil.Month( A564Lote_Data) == DateTimeUtil.Month( Gx_date) ) && ( DateTimeUtil.Year( A564Lote_Data) == DateTimeUtil.Year( Gx_date) ) ) )
            {
               GetLote_ValorOSs( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               if ( ! ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV151WWLoteDS_18_Lote_valor1) ) ) || ( ( A572Lote_Valor >= AV151WWLoteDS_18_Lote_valor1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV152WWLoteDS_19_Lote_valor_to1) ) ) || ( ( A572Lote_Valor <= AV152WWLoteDS_19_Lote_valor_to1 ) ) )
                  {
                     if ( ! ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV172WWLoteDS_39_Lote_valor2) ) ) || ( ( A572Lote_Valor >= AV172WWLoteDS_39_Lote_valor2 ) ) )
                     {
                        if ( ! ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV173WWLoteDS_40_Lote_valor_to2) ) ) || ( ( A572Lote_Valor <= AV173WWLoteDS_40_Lote_valor_to2 ) ) )
                        {
                           if ( ! ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV193WWLoteDS_60_Lote_valor3) ) ) || ( ( A572Lote_Valor >= AV193WWLoteDS_60_Lote_valor3 ) ) )
                           {
                              if ( ! ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV194WWLoteDS_61_Lote_valor_to3) ) ) || ( ( A572Lote_Valor <= AV194WWLoteDS_61_Lote_valor_to3 ) ) )
                              {
                                 AV13Lote_StatusDescription = gxdomainstatusdemanda.getDescription(context,A575Lote_Status);
                                 /* Execute user subroutine: 'BEFOREPRINTLINE' */
                                 S152 ();
                                 if ( returnInSub )
                                 {
                                    pr_default.close(0);
                                    returnInSub = true;
                                    if (true) return;
                                 }
                                 H690( false, 20) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, "")), 5, Gx_line+2, 60, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")), 65, Gx_line+2, 120, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1748Lote_ContratadaNom, "@!")), 125, Gx_line+2, 180, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"), 185, Gx_line+2, 240, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A561Lote_UserNom, "@!")), 245, Gx_line+2, 300, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A567Lote_DataIni, "99/99/99"), 305, Gx_line+2, 360, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A568Lote_DataFim, "99/99/99"), 365, Gx_line+2, 420, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV13Lote_StatusDescription, "")), 425, Gx_line+2, 480, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A569Lote_QtdeDmn), "ZZZ9")), 485, Gx_line+2, 540, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), 545, Gx_line+2, 600, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")), 605, Gx_line+2, 660, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A674Lote_DataNfe, "99/99/99"), 665, Gx_line+2, 720, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A857Lote_PrevPagamento, "99/99/99"), 725, Gx_line+2, 780, Gx_line+17, 2, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+20);
                                 /* Execute user subroutine: 'AFTERPRINTLINE' */
                                 S162 ();
                                 if ( returnInSub )
                                 {
                                    pr_default.close(0);
                                    returnInSub = true;
                                    if (true) return;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S152( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S162( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void H690( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor P006910 */
         pr_default.execute(1, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(1) != 101) && ( P006910_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  P006910_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*P006910_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV74GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV75GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV14DynamicFiltersSelector1 = "";
         AV15Lote_Numero1 = "";
         AV76Lote_Numero = "";
         AV16Lote_Nome1 = "";
         AV77Lote_Nome = "";
         AV17Lote_Data1 = (DateTime)(DateTime.MinValue);
         AV18Lote_Data_To1 = (DateTime)(DateTime.MinValue);
         AV78FilterLote_DataDescription = "";
         AV79Lote_Data = (DateTime)(DateTime.MinValue);
         AV19Lote_UserNom1 = "";
         AV80Lote_UserNom = "";
         AV20Lote_DataIni1 = DateTime.MinValue;
         AV21Lote_DataIni_To1 = DateTime.MinValue;
         AV81FilterLote_DataIniDescription = "";
         AV82Lote_DataIni = DateTime.MinValue;
         AV22Lote_DataFim1 = DateTime.MinValue;
         AV23Lote_DataFim_To1 = DateTime.MinValue;
         AV83FilterLote_DataFimDescription = "";
         AV84Lote_DataFim = DateTime.MinValue;
         AV25Lote_DataNfe1 = DateTime.MinValue;
         AV26Lote_DataNfe_To1 = DateTime.MinValue;
         AV86FilterLote_DataNfeDescription = "";
         AV87Lote_DataNfe = DateTime.MinValue;
         AV88FilterLote_QtdeDmnDescription = "";
         AV90FilterLote_ValorDescription = "";
         AV92FilterLote_ValorPFDescription = "";
         AV95Lote_ContratadaNom1 = "";
         AV96Lote_ContratadaNom = "";
         AV34DynamicFiltersSelector2 = "";
         AV35Lote_Numero2 = "";
         AV36Lote_Nome2 = "";
         AV37Lote_Data2 = (DateTime)(DateTime.MinValue);
         AV38Lote_Data_To2 = (DateTime)(DateTime.MinValue);
         AV39Lote_UserNom2 = "";
         AV40Lote_DataIni2 = DateTime.MinValue;
         AV41Lote_DataIni_To2 = DateTime.MinValue;
         AV42Lote_DataFim2 = DateTime.MinValue;
         AV43Lote_DataFim_To2 = DateTime.MinValue;
         AV45Lote_DataNfe2 = DateTime.MinValue;
         AV46Lote_DataNfe_To2 = DateTime.MinValue;
         AV97Lote_ContratadaNom2 = "";
         AV54DynamicFiltersSelector3 = "";
         AV55Lote_Numero3 = "";
         AV56Lote_Nome3 = "";
         AV57Lote_Data3 = (DateTime)(DateTime.MinValue);
         AV58Lote_Data_To3 = (DateTime)(DateTime.MinValue);
         AV59Lote_UserNom3 = "";
         AV60Lote_DataIni3 = DateTime.MinValue;
         AV61Lote_DataIni_To3 = DateTime.MinValue;
         AV62Lote_DataFim3 = DateTime.MinValue;
         AV63Lote_DataFim_To3 = DateTime.MinValue;
         AV65Lote_DataNfe3 = DateTime.MinValue;
         AV66Lote_DataNfe_To3 = DateTime.MinValue;
         AV98Lote_ContratadaNom3 = "";
         AV136WWLoteDS_3_Dynamicfiltersselector1 = "";
         AV137WWLoteDS_4_Lote_numero1 = "";
         AV138WWLoteDS_5_Lote_nome1 = "";
         AV139WWLoteDS_6_Lote_data1 = (DateTime)(DateTime.MinValue);
         AV140WWLoteDS_7_Lote_data_to1 = (DateTime)(DateTime.MinValue);
         AV141WWLoteDS_8_Lote_usernom1 = "";
         AV142WWLoteDS_9_Lote_dataini1 = DateTime.MinValue;
         AV143WWLoteDS_10_Lote_dataini_to1 = DateTime.MinValue;
         AV144WWLoteDS_11_Lote_datafim1 = DateTime.MinValue;
         AV145WWLoteDS_12_Lote_datafim_to1 = DateTime.MinValue;
         AV147WWLoteDS_14_Lote_datanfe1 = DateTime.MinValue;
         AV148WWLoteDS_15_Lote_datanfe_to1 = DateTime.MinValue;
         AV155WWLoteDS_22_Lote_contratadanom1 = "";
         AV157WWLoteDS_24_Dynamicfiltersselector2 = "";
         AV158WWLoteDS_25_Lote_numero2 = "";
         AV159WWLoteDS_26_Lote_nome2 = "";
         AV160WWLoteDS_27_Lote_data2 = (DateTime)(DateTime.MinValue);
         AV161WWLoteDS_28_Lote_data_to2 = (DateTime)(DateTime.MinValue);
         AV162WWLoteDS_29_Lote_usernom2 = "";
         AV163WWLoteDS_30_Lote_dataini2 = DateTime.MinValue;
         AV164WWLoteDS_31_Lote_dataini_to2 = DateTime.MinValue;
         AV165WWLoteDS_32_Lote_datafim2 = DateTime.MinValue;
         AV166WWLoteDS_33_Lote_datafim_to2 = DateTime.MinValue;
         AV168WWLoteDS_35_Lote_datanfe2 = DateTime.MinValue;
         AV169WWLoteDS_36_Lote_datanfe_to2 = DateTime.MinValue;
         AV176WWLoteDS_43_Lote_contratadanom2 = "";
         AV178WWLoteDS_45_Dynamicfiltersselector3 = "";
         AV179WWLoteDS_46_Lote_numero3 = "";
         AV180WWLoteDS_47_Lote_nome3 = "";
         AV181WWLoteDS_48_Lote_data3 = (DateTime)(DateTime.MinValue);
         AV182WWLoteDS_49_Lote_data_to3 = (DateTime)(DateTime.MinValue);
         AV183WWLoteDS_50_Lote_usernom3 = "";
         AV184WWLoteDS_51_Lote_dataini3 = DateTime.MinValue;
         AV185WWLoteDS_52_Lote_dataini_to3 = DateTime.MinValue;
         AV186WWLoteDS_53_Lote_datafim3 = DateTime.MinValue;
         AV187WWLoteDS_54_Lote_datafim_to3 = DateTime.MinValue;
         AV189WWLoteDS_56_Lote_datanfe3 = DateTime.MinValue;
         AV190WWLoteDS_57_Lote_datanfe_to3 = DateTime.MinValue;
         AV197WWLoteDS_64_Lote_contratadanom3 = "";
         AV200WWLoteDS_67_Tflote_datanfe = DateTime.MinValue;
         AV201WWLoteDS_68_Tflote_datanfe_to = DateTime.MinValue;
         AV202WWLoteDS_69_Tflote_prevpagamento = DateTime.MinValue;
         AV203WWLoteDS_70_Tflote_prevpagamento_to = DateTime.MinValue;
         scmdbuf = "";
         lV155WWLoteDS_22_Lote_contratadanom1 = "";
         lV176WWLoteDS_43_Lote_contratadanom2 = "";
         lV197WWLoteDS_64_Lote_contratadanom3 = "";
         lV137WWLoteDS_4_Lote_numero1 = "";
         lV138WWLoteDS_5_Lote_nome1 = "";
         lV141WWLoteDS_8_Lote_usernom1 = "";
         lV158WWLoteDS_25_Lote_numero2 = "";
         lV159WWLoteDS_26_Lote_nome2 = "";
         lV162WWLoteDS_29_Lote_usernom2 = "";
         lV179WWLoteDS_46_Lote_numero3 = "";
         lV180WWLoteDS_47_Lote_nome3 = "";
         lV183WWLoteDS_50_Lote_usernom3 = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A561Lote_UserNom = "";
         A674Lote_DataNfe = DateTime.MinValue;
         A857Lote_PrevPagamento = DateTime.MinValue;
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         A1748Lote_ContratadaNom = "";
         Gx_date = DateTime.MinValue;
         P00699_A559Lote_UserCod = new int[1] ;
         P00699_A560Lote_PessoaCod = new int[1] ;
         P00699_n560Lote_PessoaCod = new bool[] {false} ;
         P00699_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         P00699_n857Lote_PrevPagamento = new bool[] {false} ;
         P00699_A565Lote_ValorPF = new decimal[1] ;
         P00699_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         P00699_n674Lote_DataNfe = new bool[] {false} ;
         P00699_A673Lote_NFe = new int[1] ;
         P00699_n673Lote_NFe = new bool[] {false} ;
         P00699_A561Lote_UserNom = new String[] {""} ;
         P00699_n561Lote_UserNom = new bool[] {false} ;
         P00699_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00699_A563Lote_Nome = new String[] {""} ;
         P00699_A562Lote_Numero = new String[] {""} ;
         P00699_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00699_A596Lote_Codigo = new int[1] ;
         P00699_A1748Lote_ContratadaNom = new String[] {""} ;
         P00699_n1748Lote_ContratadaNom = new bool[] {false} ;
         P00699_A569Lote_QtdeDmn = new short[1] ;
         P00699_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00699_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00699_A1231Lote_ContratadaCod = new int[1] ;
         P00699_A575Lote_Status = new String[] {""} ;
         P00699_A1057Lote_ValorGlosas = new decimal[1] ;
         P00699_n1057Lote_ValorGlosas = new bool[] {false} ;
         A575Lote_Status = "";
         AV13Lote_StatusDescription = "";
         P006910_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006910_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006910_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006910_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P006910_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportreportwwlote__default(),
            new Object[][] {
                new Object[] {
               P00699_A559Lote_UserCod, P00699_A560Lote_PessoaCod, P00699_n560Lote_PessoaCod, P00699_A857Lote_PrevPagamento, P00699_n857Lote_PrevPagamento, P00699_A565Lote_ValorPF, P00699_A674Lote_DataNfe, P00699_n674Lote_DataNfe, P00699_A673Lote_NFe, P00699_n673Lote_NFe,
               P00699_A561Lote_UserNom, P00699_n561Lote_UserNom, P00699_A564Lote_Data, P00699_A563Lote_Nome, P00699_A562Lote_Numero, P00699_A595Lote_AreaTrabalhoCod, P00699_A596Lote_Codigo, P00699_A1748Lote_ContratadaNom, P00699_n1748Lote_ContratadaNom, P00699_A569Lote_QtdeDmn,
               P00699_A568Lote_DataFim, P00699_A567Lote_DataIni, P00699_A1231Lote_ContratadaCod, P00699_A575Lote_Status, P00699_A1057Lote_ValorGlosas, P00699_n1057Lote_ValorGlosas
               }
               , new Object[] {
               P006910_A597ContagemResultado_LoteAceiteCod, P006910_n597ContagemResultado_LoteAceiteCod, P006910_A512ContagemResultado_ValorPF, P006910_n512ContagemResultado_ValorPF, P006910_A456ContagemResultado_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV10OrderedBy ;
      private short GxWebError ;
      private short AV27Lote_QtdeDmn1 ;
      private short AV28Lote_QtdeDmn_To1 ;
      private short AV89Lote_QtdeDmn ;
      private short AV47Lote_QtdeDmn2 ;
      private short AV48Lote_QtdeDmn_To2 ;
      private short AV67Lote_QtdeDmn3 ;
      private short AV68Lote_QtdeDmn_To3 ;
      private short AV149WWLoteDS_16_Lote_qtdedmn1 ;
      private short AV150WWLoteDS_17_Lote_qtdedmn_to1 ;
      private short AV170WWLoteDS_37_Lote_qtdedmn2 ;
      private short AV171WWLoteDS_38_Lote_qtdedmn_to2 ;
      private short AV191WWLoteDS_58_Lote_qtdedmn3 ;
      private short AV192WWLoteDS_59_Lote_qtdedmn_to3 ;
      private short A569Lote_QtdeDmn ;
      private int AV12Lote_AreaTrabalhoCod ;
      private int AV94Lote_ContratadaCod ;
      private int AV125TFLote_NFe ;
      private int AV126TFLote_NFe_To ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int AV24Lote_NFe1 ;
      private int AV85Lote_NFe ;
      private int AV44Lote_NFe2 ;
      private int AV64Lote_NFe3 ;
      private int AV134WWLoteDS_1_Lote_areatrabalhocod ;
      private int AV135WWLoteDS_2_Lote_contratadacod ;
      private int AV146WWLoteDS_13_Lote_nfe1 ;
      private int AV167WWLoteDS_34_Lote_nfe2 ;
      private int AV188WWLoteDS_55_Lote_nfe3 ;
      private int AV198WWLoteDS_65_Tflote_nfe ;
      private int AV199WWLoteDS_66_Tflote_nfe_to ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV74GridState_gxTpr_Dynamicfilters_Count ;
      private int A673Lote_NFe ;
      private int A1231Lote_ContratadaCod ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int A596Lote_Codigo ;
      private decimal AV29Lote_Valor1 ;
      private decimal AV30Lote_Valor_To1 ;
      private decimal AV91Lote_Valor ;
      private decimal AV31Lote_ValorPF1 ;
      private decimal AV32Lote_ValorPF_To1 ;
      private decimal AV93Lote_ValorPF ;
      private decimal AV49Lote_Valor2 ;
      private decimal AV50Lote_Valor_To2 ;
      private decimal AV51Lote_ValorPF2 ;
      private decimal AV52Lote_ValorPF_To2 ;
      private decimal AV69Lote_Valor3 ;
      private decimal AV70Lote_Valor_To3 ;
      private decimal AV71Lote_ValorPF3 ;
      private decimal AV72Lote_ValorPF_To3 ;
      private decimal AV151WWLoteDS_18_Lote_valor1 ;
      private decimal AV152WWLoteDS_19_Lote_valor_to1 ;
      private decimal AV153WWLoteDS_20_Lote_valorpf1 ;
      private decimal AV154WWLoteDS_21_Lote_valorpf_to1 ;
      private decimal AV172WWLoteDS_39_Lote_valor2 ;
      private decimal AV173WWLoteDS_40_Lote_valor_to2 ;
      private decimal AV174WWLoteDS_41_Lote_valorpf2 ;
      private decimal AV175WWLoteDS_42_Lote_valorpf_to2 ;
      private decimal AV193WWLoteDS_60_Lote_valor3 ;
      private decimal AV194WWLoteDS_61_Lote_valor_to3 ;
      private decimal AV195WWLoteDS_62_Lote_valorpf3 ;
      private decimal AV196WWLoteDS_63_Lote_valorpf_to3 ;
      private decimal A565Lote_ValorPF ;
      private decimal A572Lote_Valor ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV15Lote_Numero1 ;
      private String AV76Lote_Numero ;
      private String AV16Lote_Nome1 ;
      private String AV77Lote_Nome ;
      private String AV19Lote_UserNom1 ;
      private String AV80Lote_UserNom ;
      private String AV95Lote_ContratadaNom1 ;
      private String AV96Lote_ContratadaNom ;
      private String AV35Lote_Numero2 ;
      private String AV36Lote_Nome2 ;
      private String AV39Lote_UserNom2 ;
      private String AV97Lote_ContratadaNom2 ;
      private String AV55Lote_Numero3 ;
      private String AV56Lote_Nome3 ;
      private String AV59Lote_UserNom3 ;
      private String AV98Lote_ContratadaNom3 ;
      private String AV137WWLoteDS_4_Lote_numero1 ;
      private String AV138WWLoteDS_5_Lote_nome1 ;
      private String AV141WWLoteDS_8_Lote_usernom1 ;
      private String AV155WWLoteDS_22_Lote_contratadanom1 ;
      private String AV158WWLoteDS_25_Lote_numero2 ;
      private String AV159WWLoteDS_26_Lote_nome2 ;
      private String AV162WWLoteDS_29_Lote_usernom2 ;
      private String AV176WWLoteDS_43_Lote_contratadanom2 ;
      private String AV179WWLoteDS_46_Lote_numero3 ;
      private String AV180WWLoteDS_47_Lote_nome3 ;
      private String AV183WWLoteDS_50_Lote_usernom3 ;
      private String AV197WWLoteDS_64_Lote_contratadanom3 ;
      private String scmdbuf ;
      private String lV155WWLoteDS_22_Lote_contratadanom1 ;
      private String lV176WWLoteDS_43_Lote_contratadanom2 ;
      private String lV197WWLoteDS_64_Lote_contratadanom3 ;
      private String lV137WWLoteDS_4_Lote_numero1 ;
      private String lV138WWLoteDS_5_Lote_nome1 ;
      private String lV141WWLoteDS_8_Lote_usernom1 ;
      private String lV158WWLoteDS_25_Lote_numero2 ;
      private String lV159WWLoteDS_26_Lote_nome2 ;
      private String lV162WWLoteDS_29_Lote_usernom2 ;
      private String lV179WWLoteDS_46_Lote_numero3 ;
      private String lV180WWLoteDS_47_Lote_nome3 ;
      private String lV183WWLoteDS_50_Lote_usernom3 ;
      private String A562Lote_Numero ;
      private String A563Lote_Nome ;
      private String A561Lote_UserNom ;
      private String A1748Lote_ContratadaNom ;
      private String A575Lote_Status ;
      private DateTime AV17Lote_Data1 ;
      private DateTime AV18Lote_Data_To1 ;
      private DateTime AV79Lote_Data ;
      private DateTime AV37Lote_Data2 ;
      private DateTime AV38Lote_Data_To2 ;
      private DateTime AV57Lote_Data3 ;
      private DateTime AV58Lote_Data_To3 ;
      private DateTime AV139WWLoteDS_6_Lote_data1 ;
      private DateTime AV140WWLoteDS_7_Lote_data_to1 ;
      private DateTime AV160WWLoteDS_27_Lote_data2 ;
      private DateTime AV161WWLoteDS_28_Lote_data_to2 ;
      private DateTime AV181WWLoteDS_48_Lote_data3 ;
      private DateTime AV182WWLoteDS_49_Lote_data_to3 ;
      private DateTime A564Lote_Data ;
      private DateTime AV127TFLote_DataNfe ;
      private DateTime AV128TFLote_DataNfe_To ;
      private DateTime AV129TFLote_PrevPagamento ;
      private DateTime AV130TFLote_PrevPagamento_To ;
      private DateTime AV20Lote_DataIni1 ;
      private DateTime AV21Lote_DataIni_To1 ;
      private DateTime AV82Lote_DataIni ;
      private DateTime AV22Lote_DataFim1 ;
      private DateTime AV23Lote_DataFim_To1 ;
      private DateTime AV84Lote_DataFim ;
      private DateTime AV25Lote_DataNfe1 ;
      private DateTime AV26Lote_DataNfe_To1 ;
      private DateTime AV87Lote_DataNfe ;
      private DateTime AV40Lote_DataIni2 ;
      private DateTime AV41Lote_DataIni_To2 ;
      private DateTime AV42Lote_DataFim2 ;
      private DateTime AV43Lote_DataFim_To2 ;
      private DateTime AV45Lote_DataNfe2 ;
      private DateTime AV46Lote_DataNfe_To2 ;
      private DateTime AV60Lote_DataIni3 ;
      private DateTime AV61Lote_DataIni_To3 ;
      private DateTime AV62Lote_DataFim3 ;
      private DateTime AV63Lote_DataFim_To3 ;
      private DateTime AV65Lote_DataNfe3 ;
      private DateTime AV66Lote_DataNfe_To3 ;
      private DateTime AV142WWLoteDS_9_Lote_dataini1 ;
      private DateTime AV143WWLoteDS_10_Lote_dataini_to1 ;
      private DateTime AV144WWLoteDS_11_Lote_datafim1 ;
      private DateTime AV145WWLoteDS_12_Lote_datafim_to1 ;
      private DateTime AV147WWLoteDS_14_Lote_datanfe1 ;
      private DateTime AV148WWLoteDS_15_Lote_datanfe_to1 ;
      private DateTime AV163WWLoteDS_30_Lote_dataini2 ;
      private DateTime AV164WWLoteDS_31_Lote_dataini_to2 ;
      private DateTime AV165WWLoteDS_32_Lote_datafim2 ;
      private DateTime AV166WWLoteDS_33_Lote_datafim_to2 ;
      private DateTime AV168WWLoteDS_35_Lote_datanfe2 ;
      private DateTime AV169WWLoteDS_36_Lote_datanfe_to2 ;
      private DateTime AV184WWLoteDS_51_Lote_dataini3 ;
      private DateTime AV185WWLoteDS_52_Lote_dataini_to3 ;
      private DateTime AV186WWLoteDS_53_Lote_datafim3 ;
      private DateTime AV187WWLoteDS_54_Lote_datafim_to3 ;
      private DateTime AV189WWLoteDS_56_Lote_datanfe3 ;
      private DateTime AV190WWLoteDS_57_Lote_datanfe_to3 ;
      private DateTime AV200WWLoteDS_67_Tflote_datanfe ;
      private DateTime AV201WWLoteDS_68_Tflote_datanfe_to ;
      private DateTime AV202WWLoteDS_69_Tflote_prevpagamento ;
      private DateTime AV203WWLoteDS_70_Tflote_prevpagamento_to ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV11OrderedDsc ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV53DynamicFiltersEnabled3 ;
      private bool AV156WWLoteDS_23_Dynamicfiltersenabled2 ;
      private bool AV177WWLoteDS_44_Dynamicfiltersenabled3 ;
      private bool AV9WWPContext_gxTpr_Userehcontratante ;
      private bool n560Lote_PessoaCod ;
      private bool n857Lote_PrevPagamento ;
      private bool n674Lote_DataNfe ;
      private bool n673Lote_NFe ;
      private bool n561Lote_UserNom ;
      private bool n1748Lote_ContratadaNom ;
      private bool n1057Lote_ValorGlosas ;
      private String AV73GridStateXML ;
      private String AV14DynamicFiltersSelector1 ;
      private String AV78FilterLote_DataDescription ;
      private String AV81FilterLote_DataIniDescription ;
      private String AV83FilterLote_DataFimDescription ;
      private String AV86FilterLote_DataNfeDescription ;
      private String AV88FilterLote_QtdeDmnDescription ;
      private String AV90FilterLote_ValorDescription ;
      private String AV92FilterLote_ValorPFDescription ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV54DynamicFiltersSelector3 ;
      private String AV136WWLoteDS_3_Dynamicfiltersselector1 ;
      private String AV157WWLoteDS_24_Dynamicfiltersselector2 ;
      private String AV178WWLoteDS_45_Dynamicfiltersselector3 ;
      private String AV13Lote_StatusDescription ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00699_A559Lote_UserCod ;
      private int[] P00699_A560Lote_PessoaCod ;
      private bool[] P00699_n560Lote_PessoaCod ;
      private DateTime[] P00699_A857Lote_PrevPagamento ;
      private bool[] P00699_n857Lote_PrevPagamento ;
      private decimal[] P00699_A565Lote_ValorPF ;
      private DateTime[] P00699_A674Lote_DataNfe ;
      private bool[] P00699_n674Lote_DataNfe ;
      private int[] P00699_A673Lote_NFe ;
      private bool[] P00699_n673Lote_NFe ;
      private String[] P00699_A561Lote_UserNom ;
      private bool[] P00699_n561Lote_UserNom ;
      private DateTime[] P00699_A564Lote_Data ;
      private String[] P00699_A563Lote_Nome ;
      private String[] P00699_A562Lote_Numero ;
      private int[] P00699_A595Lote_AreaTrabalhoCod ;
      private int[] P00699_A596Lote_Codigo ;
      private String[] P00699_A1748Lote_ContratadaNom ;
      private bool[] P00699_n1748Lote_ContratadaNom ;
      private short[] P00699_A569Lote_QtdeDmn ;
      private DateTime[] P00699_A568Lote_DataFim ;
      private DateTime[] P00699_A567Lote_DataIni ;
      private int[] P00699_A1231Lote_ContratadaCod ;
      private String[] P00699_A575Lote_Status ;
      private decimal[] P00699_A1057Lote_ValorGlosas ;
      private bool[] P00699_n1057Lote_ValorGlosas ;
      private int[] P006910_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006910_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P006910_A512ContagemResultado_ValorPF ;
      private bool[] P006910_n512ContagemResultado_ValorPF ;
      private int[] P006910_A456ContagemResultado_Codigo ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV74GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV75GridStateDynamicFilter ;
   }

   public class exportreportwwlote__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00699( IGxContext context ,
                                             String AV136WWLoteDS_3_Dynamicfiltersselector1 ,
                                             String AV137WWLoteDS_4_Lote_numero1 ,
                                             String AV138WWLoteDS_5_Lote_nome1 ,
                                             DateTime AV139WWLoteDS_6_Lote_data1 ,
                                             DateTime AV140WWLoteDS_7_Lote_data_to1 ,
                                             String AV141WWLoteDS_8_Lote_usernom1 ,
                                             int AV146WWLoteDS_13_Lote_nfe1 ,
                                             DateTime AV147WWLoteDS_14_Lote_datanfe1 ,
                                             DateTime AV148WWLoteDS_15_Lote_datanfe_to1 ,
                                             short AV149WWLoteDS_16_Lote_qtdedmn1 ,
                                             short AV150WWLoteDS_17_Lote_qtdedmn_to1 ,
                                             decimal AV153WWLoteDS_20_Lote_valorpf1 ,
                                             decimal AV154WWLoteDS_21_Lote_valorpf_to1 ,
                                             bool AV156WWLoteDS_23_Dynamicfiltersenabled2 ,
                                             String AV157WWLoteDS_24_Dynamicfiltersselector2 ,
                                             String AV158WWLoteDS_25_Lote_numero2 ,
                                             String AV159WWLoteDS_26_Lote_nome2 ,
                                             DateTime AV160WWLoteDS_27_Lote_data2 ,
                                             DateTime AV161WWLoteDS_28_Lote_data_to2 ,
                                             String AV162WWLoteDS_29_Lote_usernom2 ,
                                             int AV167WWLoteDS_34_Lote_nfe2 ,
                                             DateTime AV168WWLoteDS_35_Lote_datanfe2 ,
                                             DateTime AV169WWLoteDS_36_Lote_datanfe_to2 ,
                                             short AV170WWLoteDS_37_Lote_qtdedmn2 ,
                                             short AV171WWLoteDS_38_Lote_qtdedmn_to2 ,
                                             decimal AV174WWLoteDS_41_Lote_valorpf2 ,
                                             decimal AV175WWLoteDS_42_Lote_valorpf_to2 ,
                                             bool AV177WWLoteDS_44_Dynamicfiltersenabled3 ,
                                             String AV178WWLoteDS_45_Dynamicfiltersselector3 ,
                                             String AV179WWLoteDS_46_Lote_numero3 ,
                                             String AV180WWLoteDS_47_Lote_nome3 ,
                                             DateTime AV181WWLoteDS_48_Lote_data3 ,
                                             DateTime AV182WWLoteDS_49_Lote_data_to3 ,
                                             String AV183WWLoteDS_50_Lote_usernom3 ,
                                             int AV188WWLoteDS_55_Lote_nfe3 ,
                                             DateTime AV189WWLoteDS_56_Lote_datanfe3 ,
                                             DateTime AV190WWLoteDS_57_Lote_datanfe_to3 ,
                                             short AV191WWLoteDS_58_Lote_qtdedmn3 ,
                                             short AV192WWLoteDS_59_Lote_qtdedmn_to3 ,
                                             decimal AV195WWLoteDS_62_Lote_valorpf3 ,
                                             decimal AV196WWLoteDS_63_Lote_valorpf_to3 ,
                                             int AV198WWLoteDS_65_Tflote_nfe ,
                                             int AV199WWLoteDS_66_Tflote_nfe_to ,
                                             DateTime AV200WWLoteDS_67_Tflote_datanfe ,
                                             DateTime AV201WWLoteDS_68_Tflote_datanfe_to ,
                                             DateTime AV202WWLoteDS_69_Tflote_prevpagamento ,
                                             DateTime AV203WWLoteDS_70_Tflote_prevpagamento_to ,
                                             String A562Lote_Numero ,
                                             String A563Lote_Nome ,
                                             DateTime A564Lote_Data ,
                                             String A561Lote_UserNom ,
                                             int A673Lote_NFe ,
                                             DateTime A674Lote_DataNfe ,
                                             short A569Lote_QtdeDmn ,
                                             decimal A565Lote_ValorPF ,
                                             DateTime A857Lote_PrevPagamento ,
                                             short AV10OrderedBy ,
                                             bool AV11OrderedDsc ,
                                             bool AV9WWPContext_gxTpr_Userehcontratante ,
                                             int A1231Lote_ContratadaCod ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             DateTime AV142WWLoteDS_9_Lote_dataini1 ,
                                             DateTime A567Lote_DataIni ,
                                             DateTime AV143WWLoteDS_10_Lote_dataini_to1 ,
                                             DateTime AV144WWLoteDS_11_Lote_datafim1 ,
                                             DateTime A568Lote_DataFim ,
                                             DateTime AV145WWLoteDS_12_Lote_datafim_to1 ,
                                             decimal AV151WWLoteDS_18_Lote_valor1 ,
                                             decimal A572Lote_Valor ,
                                             decimal AV152WWLoteDS_19_Lote_valor_to1 ,
                                             String AV155WWLoteDS_22_Lote_contratadanom1 ,
                                             String A1748Lote_ContratadaNom ,
                                             DateTime AV163WWLoteDS_30_Lote_dataini2 ,
                                             DateTime AV164WWLoteDS_31_Lote_dataini_to2 ,
                                             DateTime AV165WWLoteDS_32_Lote_datafim2 ,
                                             DateTime AV166WWLoteDS_33_Lote_datafim_to2 ,
                                             decimal AV172WWLoteDS_39_Lote_valor2 ,
                                             decimal AV173WWLoteDS_40_Lote_valor_to2 ,
                                             String AV176WWLoteDS_43_Lote_contratadanom2 ,
                                             DateTime AV184WWLoteDS_51_Lote_dataini3 ,
                                             DateTime AV185WWLoteDS_52_Lote_dataini_to3 ,
                                             DateTime AV186WWLoteDS_53_Lote_datafim3 ,
                                             DateTime AV187WWLoteDS_54_Lote_datafim_to3 ,
                                             decimal AV193WWLoteDS_60_Lote_valor3 ,
                                             decimal AV194WWLoteDS_61_Lote_valor_to3 ,
                                             String AV197WWLoteDS_64_Lote_contratadanom3 ,
                                             int AV74GridState_gxTpr_Dynamicfilters_Count ,
                                             DateTime Gx_date ,
                                             int A595Lote_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [100] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Lote_UserCod] AS Lote_UserCod, T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_PrevPagamento], T1.[Lote_ValorPF], T1.[Lote_DataNfe], T1.[Lote_NFe], T3.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_Data], T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_AreaTrabalhoCod], T1.[Lote_Codigo], COALESCE( T4.[Lote_ContratadaNom], '') AS Lote_ContratadaNom, COALESCE( T6.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T6.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T6.[Lote_Status], '') AS Lote_Status, COALESCE( T5.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM (((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T10.[Pessoa_Nome]) AS Lote_ContratadaNom, T8.[ContagemResultado_LoteAceiteCod] FROM (([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T8.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T10 WITH (NOLOCK) ON T10.[Pessoa_Codigo] = T9.[Contratada_PessoaCod]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T4 ON T4.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T10.[GXC3], 0) + COALESCE( T9.[GXC4], 0) AS Lote_ValorGlosas, T8.[Lote_Codigo] FROM (([Lote] T8 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T11.[ContagemResultadoIndicadores_Valor]) AS GXC4, T12.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T11 WITH (NOLOCK),  [Lote] T12 WITH (NOLOCK) WHERE T11.[ContagemResultadoIndicadores_LoteCod] = T12.[Lote_Codigo] GROUP BY T12.[Lote_Codigo] ) T9 ON";
         scmdbuf = scmdbuf + " T9.[Lote_Codigo] = T8.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC3, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T10 ON T10.[ContagemResultado_LoteAceiteCod] = T8.[Lote_Codigo]) ) T5 ON T5.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, MIN([ContagemResultado_StatusDmn]) AS Lote_Status FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN (SELECT MAX(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T8.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T8.[ContagemResultado_Codigo]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T7 ON T7.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV9WWPCo_2Userehcontratante = 1 or ( COALESCE( T6.[Lote_ContratadaCod], 0) = @AV9WWPCo_1Contratada_codigo))";
         scmdbuf = scmdbuf + " and (Not ( @AV136WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAINI' and ( Not (@AV142WWLoteDS_9_Lote_dataini1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV142WWLoteDS_9_Lote_dataini1))";
         scmdbuf = scmdbuf + " and (Not ( @AV136WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAINI' and ( Not (@AV143WWLoteDS_10_Lote_dataini_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV143WWLoteDS_10_Lote_dataini_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV136WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAFIM' and ( Not (@AV144WWLoteDS_11_Lote_datafim1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV144WWLoteDS_11_Lote_datafim1))";
         scmdbuf = scmdbuf + " and (Not ( @AV136WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAFIM' and ( Not (@AV145WWLoteDS_12_Lote_datafim_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV145WWLoteDS_12_Lote_datafim_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV136WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_CONTRATADANOM' and ( Not (@AV155WWLoteDS_22_Lote_contratadanom1 = ''))) or ( COALESCE( T4.[Lote_ContratadaNom], '') like '%' + @lV155WWLoteDS_22_Lote_contratadanom1))";
         scmdbuf = scmdbuf + " and (Not ( @AV156WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV157WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAINI' and ( Not (@AV163WWLoteDS_30_Lote_dataini2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV163WWLoteDS_30_Lote_dataini2))";
         scmdbuf = scmdbuf + " and (Not ( @AV156WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV157WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAINI' and ( Not (@AV164WWLoteDS_31_Lote_dataini_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV164WWLoteDS_31_Lote_dataini_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV156WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV157WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAFIM' and ( Not (@AV165WWLoteDS_32_Lote_datafim2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV165WWLoteDS_32_Lote_datafim2))";
         scmdbuf = scmdbuf + " and (Not ( @AV156WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV157WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAFIM' and ( Not (@AV166WWLoteDS_33_Lote_datafim_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV166WWLoteDS_33_Lote_datafim_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV156WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV157WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_CONTRATADANOM' and ( Not (@AV176WWLoteDS_43_Lote_contratadanom2 = ''))) or ( COALESCE( T4.[Lote_ContratadaNom], '') like '%' + @lV176WWLoteDS_43_Lote_contratadanom2))";
         scmdbuf = scmdbuf + " and (Not ( @AV177WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV178WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAINI' and ( Not (@AV184WWLoteDS_51_Lote_dataini3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV184WWLoteDS_51_Lote_dataini3))";
         scmdbuf = scmdbuf + " and (Not ( @AV177WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV178WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAINI' and ( Not (@AV185WWLoteDS_52_Lote_dataini_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV185WWLoteDS_52_Lote_dataini_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV177WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV178WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAFIM' and ( Not (@AV186WWLoteDS_53_Lote_datafim3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV186WWLoteDS_53_Lote_datafim3))";
         scmdbuf = scmdbuf + " and (Not ( @AV177WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV178WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAFIM' and ( Not (@AV187WWLoteDS_54_Lote_datafim_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV187WWLoteDS_54_Lote_datafim_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV177WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV178WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_CONTRATADANOM' and ( Not (@AV197WWLoteDS_64_Lote_contratadanom3 = ''))) or ( COALESCE( T4.[Lote_ContratadaNom], '') like '%' + @lV197WWLoteDS_64_Lote_contratadanom3))";
         scmdbuf = scmdbuf + " and (T1.[Lote_AreaTrabalhoCod] = @AV9WWPCo_3Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWLoteDS_4_Lote_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV137WWLoteDS_4_Lote_numero1 + '%')";
         }
         else
         {
            GXv_int2[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV138WWLoteDS_5_Lote_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV138WWLoteDS_5_Lote_nome1 + '%')";
         }
         else
         {
            GXv_int2[59] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV139WWLoteDS_6_Lote_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV139WWLoteDS_6_Lote_data1)";
         }
         else
         {
            GXv_int2[60] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV140WWLoteDS_7_Lote_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV140WWLoteDS_7_Lote_data_to1)";
         }
         else
         {
            GXv_int2[61] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_USERNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWLoteDS_8_Lote_usernom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV141WWLoteDS_8_Lote_usernom1 + '%')";
         }
         else
         {
            GXv_int2[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_NFE") == 0 ) && ( ! (0==AV146WWLoteDS_13_Lote_nfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV146WWLoteDS_13_Lote_nfe1)";
         }
         else
         {
            GXv_int2[63] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV147WWLoteDS_14_Lote_datanfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV147WWLoteDS_14_Lote_datanfe1)";
         }
         else
         {
            GXv_int2[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV148WWLoteDS_15_Lote_datanfe_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV148WWLoteDS_15_Lote_datanfe_to1)";
         }
         else
         {
            GXv_int2[65] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV149WWLoteDS_16_Lote_qtdedmn1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) >= @AV149WWLoteDS_16_Lote_qtdedmn1)";
         }
         else
         {
            GXv_int2[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV150WWLoteDS_17_Lote_qtdedmn_to1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) <= @AV150WWLoteDS_17_Lote_qtdedmn_to1)";
         }
         else
         {
            GXv_int2[67] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV153WWLoteDS_20_Lote_valorpf1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV153WWLoteDS_20_Lote_valorpf1)";
         }
         else
         {
            GXv_int2[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV154WWLoteDS_21_Lote_valorpf_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV154WWLoteDS_21_Lote_valorpf_to1)";
         }
         else
         {
            GXv_int2[69] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWLoteDS_25_Lote_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV158WWLoteDS_25_Lote_numero2 + '%')";
         }
         else
         {
            GXv_int2[70] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWLoteDS_26_Lote_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV159WWLoteDS_26_Lote_nome2 + '%')";
         }
         else
         {
            GXv_int2[71] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV160WWLoteDS_27_Lote_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV160WWLoteDS_27_Lote_data2)";
         }
         else
         {
            GXv_int2[72] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV161WWLoteDS_28_Lote_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV161WWLoteDS_28_Lote_data_to2)";
         }
         else
         {
            GXv_int2[73] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_USERNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV162WWLoteDS_29_Lote_usernom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV162WWLoteDS_29_Lote_usernom2 + '%')";
         }
         else
         {
            GXv_int2[74] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_NFE") == 0 ) && ( ! (0==AV167WWLoteDS_34_Lote_nfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV167WWLoteDS_34_Lote_nfe2)";
         }
         else
         {
            GXv_int2[75] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV168WWLoteDS_35_Lote_datanfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV168WWLoteDS_35_Lote_datanfe2)";
         }
         else
         {
            GXv_int2[76] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV169WWLoteDS_36_Lote_datanfe_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV169WWLoteDS_36_Lote_datanfe_to2)";
         }
         else
         {
            GXv_int2[77] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV170WWLoteDS_37_Lote_qtdedmn2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) >= @AV170WWLoteDS_37_Lote_qtdedmn2)";
         }
         else
         {
            GXv_int2[78] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV171WWLoteDS_38_Lote_qtdedmn_to2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) <= @AV171WWLoteDS_38_Lote_qtdedmn_to2)";
         }
         else
         {
            GXv_int2[79] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV174WWLoteDS_41_Lote_valorpf2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV174WWLoteDS_41_Lote_valorpf2)";
         }
         else
         {
            GXv_int2[80] = 1;
         }
         if ( AV156WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV157WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV175WWLoteDS_42_Lote_valorpf_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV175WWLoteDS_42_Lote_valorpf_to2)";
         }
         else
         {
            GXv_int2[81] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV179WWLoteDS_46_Lote_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV179WWLoteDS_46_Lote_numero3 + '%')";
         }
         else
         {
            GXv_int2[82] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV180WWLoteDS_47_Lote_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV180WWLoteDS_47_Lote_nome3 + '%')";
         }
         else
         {
            GXv_int2[83] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV181WWLoteDS_48_Lote_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV181WWLoteDS_48_Lote_data3)";
         }
         else
         {
            GXv_int2[84] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV182WWLoteDS_49_Lote_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV182WWLoteDS_49_Lote_data_to3)";
         }
         else
         {
            GXv_int2[85] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_USERNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV183WWLoteDS_50_Lote_usernom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV183WWLoteDS_50_Lote_usernom3 + '%')";
         }
         else
         {
            GXv_int2[86] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_NFE") == 0 ) && ( ! (0==AV188WWLoteDS_55_Lote_nfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV188WWLoteDS_55_Lote_nfe3)";
         }
         else
         {
            GXv_int2[87] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV189WWLoteDS_56_Lote_datanfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV189WWLoteDS_56_Lote_datanfe3)";
         }
         else
         {
            GXv_int2[88] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV190WWLoteDS_57_Lote_datanfe_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV190WWLoteDS_57_Lote_datanfe_to3)";
         }
         else
         {
            GXv_int2[89] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV191WWLoteDS_58_Lote_qtdedmn3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) >= @AV191WWLoteDS_58_Lote_qtdedmn3)";
         }
         else
         {
            GXv_int2[90] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV192WWLoteDS_59_Lote_qtdedmn_to3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) <= @AV192WWLoteDS_59_Lote_qtdedmn_to3)";
         }
         else
         {
            GXv_int2[91] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV195WWLoteDS_62_Lote_valorpf3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV195WWLoteDS_62_Lote_valorpf3)";
         }
         else
         {
            GXv_int2[92] = 1;
         }
         if ( AV177WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV178WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV196WWLoteDS_63_Lote_valorpf_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV196WWLoteDS_63_Lote_valorpf_to3)";
         }
         else
         {
            GXv_int2[93] = 1;
         }
         if ( ! (0==AV198WWLoteDS_65_Tflote_nfe) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] >= @AV198WWLoteDS_65_Tflote_nfe)";
         }
         else
         {
            GXv_int2[94] = 1;
         }
         if ( ! (0==AV199WWLoteDS_66_Tflote_nfe_to) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] <= @AV199WWLoteDS_66_Tflote_nfe_to)";
         }
         else
         {
            GXv_int2[95] = 1;
         }
         if ( ! (DateTime.MinValue==AV200WWLoteDS_67_Tflote_datanfe) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV200WWLoteDS_67_Tflote_datanfe)";
         }
         else
         {
            GXv_int2[96] = 1;
         }
         if ( ! (DateTime.MinValue==AV201WWLoteDS_68_Tflote_datanfe_to) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV201WWLoteDS_68_Tflote_datanfe_to)";
         }
         else
         {
            GXv_int2[97] = 1;
         }
         if ( ! (DateTime.MinValue==AV202WWLoteDS_69_Tflote_prevpagamento) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV202WWLoteDS_69_Tflote_prevpagamento)";
         }
         else
         {
            GXv_int2[98] = 1;
         }
         if ( ! (DateTime.MinValue==AV203WWLoteDS_70_Tflote_prevpagamento_to) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV203WWLoteDS_70_Tflote_prevpagamento_to)";
         }
         else
         {
            GXv_int2[99] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV10OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Codigo] DESC";
         }
         else if ( ( AV10OrderedBy == 2 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero]";
         }
         else if ( ( AV10OrderedBy == 2 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero] DESC";
         }
         else if ( ( AV10OrderedBy == 3 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome]";
         }
         else if ( ( AV10OrderedBy == 3 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome] DESC";
         }
         else if ( ( AV10OrderedBy == 4 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Data]";
         }
         else if ( ( AV10OrderedBy == 4 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Data] DESC";
         }
         else if ( ( AV10OrderedBy == 5 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV10OrderedBy == 5 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV10OrderedBy == 6 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_NFe]";
         }
         else if ( ( AV10OrderedBy == 6 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_NFe] DESC";
         }
         else if ( ( AV10OrderedBy == 7 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_DataNfe]";
         }
         else if ( ( AV10OrderedBy == 7 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_DataNfe] DESC";
         }
         else if ( ( AV10OrderedBy == 8 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_PrevPagamento]";
         }
         else if ( ( AV10OrderedBy == 8 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_PrevPagamento] DESC";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00699(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (bool)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (short)dynConstraints[37] , (short)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (DateTime)dynConstraints[52] , (short)dynConstraints[53] , (decimal)dynConstraints[54] , (DateTime)dynConstraints[55] , (short)dynConstraints[56] , (bool)dynConstraints[57] , (bool)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (DateTime)dynConstraints[61] , (DateTime)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (decimal)dynConstraints[67] , (decimal)dynConstraints[68] , (decimal)dynConstraints[69] , (String)dynConstraints[70] , (String)dynConstraints[71] , (DateTime)dynConstraints[72] , (DateTime)dynConstraints[73] , (DateTime)dynConstraints[74] , (DateTime)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (DateTime)dynConstraints[81] , (DateTime)dynConstraints[82] , (decimal)dynConstraints[83] , (decimal)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006910 ;
          prmP006910 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00699 ;
          prmP00699 = new Object[] {
          new Object[] {"@AV9WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV142WWLoteDS_9_Lote_dataini1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV142WWLoteDS_9_Lote_dataini1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV143WWLoteDS_10_Lote_dataini_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143WWLoteDS_10_Lote_dataini_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWLoteDS_11_Lote_datafim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWLoteDS_11_Lote_datafim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWLoteDS_12_Lote_datafim_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWLoteDS_12_Lote_datafim_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV155WWLoteDS_22_Lote_contratadanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV155WWLoteDS_22_Lote_contratadanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV156WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV157WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV163WWLoteDS_30_Lote_dataini2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV163WWLoteDS_30_Lote_dataini2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV157WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV164WWLoteDS_31_Lote_dataini_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV164WWLoteDS_31_Lote_dataini_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV157WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV165WWLoteDS_32_Lote_datafim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV165WWLoteDS_32_Lote_datafim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV157WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV166WWLoteDS_33_Lote_datafim_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV166WWLoteDS_33_Lote_datafim_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV157WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV176WWLoteDS_43_Lote_contratadanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV176WWLoteDS_43_Lote_contratadanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV177WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV178WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184WWLoteDS_51_Lote_dataini3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184WWLoteDS_51_Lote_dataini3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV178WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185WWLoteDS_52_Lote_dataini_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185WWLoteDS_52_Lote_dataini_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV178WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV186WWLoteDS_53_Lote_datafim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186WWLoteDS_53_Lote_datafim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV178WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV187WWLoteDS_54_Lote_datafim_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV187WWLoteDS_54_Lote_datafim_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV178WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV197WWLoteDS_64_Lote_contratadanom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV197WWLoteDS_64_Lote_contratadanom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV9WWPCo_3Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV137WWLoteDS_4_Lote_numero1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV138WWLoteDS_5_Lote_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV139WWLoteDS_6_Lote_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV140WWLoteDS_7_Lote_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV141WWLoteDS_8_Lote_usernom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV146WWLoteDS_13_Lote_nfe1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV147WWLoteDS_14_Lote_datanfe1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148WWLoteDS_15_Lote_datanfe_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV149WWLoteDS_16_Lote_qtdedmn1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV150WWLoteDS_17_Lote_qtdedmn_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV153WWLoteDS_20_Lote_valorpf1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV154WWLoteDS_21_Lote_valorpf_to1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV158WWLoteDS_25_Lote_numero2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV159WWLoteDS_26_Lote_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV160WWLoteDS_27_Lote_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV161WWLoteDS_28_Lote_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV162WWLoteDS_29_Lote_usernom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV167WWLoteDS_34_Lote_nfe2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV168WWLoteDS_35_Lote_datanfe2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169WWLoteDS_36_Lote_datanfe_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170WWLoteDS_37_Lote_qtdedmn2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV171WWLoteDS_38_Lote_qtdedmn_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV174WWLoteDS_41_Lote_valorpf2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV175WWLoteDS_42_Lote_valorpf_to2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV179WWLoteDS_46_Lote_numero3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV180WWLoteDS_47_Lote_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV181WWLoteDS_48_Lote_data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV182WWLoteDS_49_Lote_data_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV183WWLoteDS_50_Lote_usernom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV188WWLoteDS_55_Lote_nfe3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV189WWLoteDS_56_Lote_datanfe3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV190WWLoteDS_57_Lote_datanfe_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV191WWLoteDS_58_Lote_qtdedmn3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV192WWLoteDS_59_Lote_qtdedmn_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV195WWLoteDS_62_Lote_valorpf3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV196WWLoteDS_63_Lote_valorpf_to3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV198WWLoteDS_65_Tflote_nfe",SqlDbType.Int,6,0} ,
          new Object[] {"@AV199WWLoteDS_66_Tflote_nfe_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV200WWLoteDS_67_Tflote_datanfe",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201WWLoteDS_68_Tflote_datanfe_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202WWLoteDS_69_Tflote_prevpagamento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203WWLoteDS_70_Tflote_prevpagamento_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00699", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00699,100,0,true,false )
             ,new CursorDef("P006910", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006910,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((String[]) buf[17])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((short[]) buf[19])[0] = rslt.getShort(14) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(16) ;
                ((int[]) buf[22])[0] = rslt.getInt(17) ;
                ((String[]) buf[23])[0] = rslt.getString(18, 1) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(19);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[100]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[101]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[103]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[104]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[109]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[110]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[112]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[113]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[117]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[118]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[120]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[121]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[125]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[126]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[127]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[129]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[131]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[132]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[133]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[137]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[138]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[139]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[141]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[142]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[143]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[145]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[146]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[147]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[149]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[150]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[151]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[153]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[154]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[158]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[162]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[163]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[166]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[167]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[168]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[170]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[174]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[175]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[178]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[179]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[184]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[185]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[186]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[187]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[190]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[191]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[192]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[193]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[194]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[196]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[197]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[198]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[199]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
