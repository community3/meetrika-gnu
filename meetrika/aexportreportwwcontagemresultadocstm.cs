/*
               File: ExportReportWWContagemResultadoCstm
        Description: Export Report WWContagem Resultado Customized
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:17:18.31
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aexportreportwwcontagemresultadocstm : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV159Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV160Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV278TFContratada_AreaTrabalhoDes = GetNextPar( );
                  AV279TFContratada_AreaTrabalhoDes_Sel = GetNextPar( );
                  AV245TFContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV246TFContagemResultado_DataDmn_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV247TFContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV248TFContagemResultado_DataUltCnt_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV253TFContagemResultado_OsFsOsFm = GetNextPar( );
                  AV254TFContagemResultado_OsFsOsFm_Sel = GetNextPar( );
                  AV249TFContagemResultado_Descricao = GetNextPar( );
                  AV250TFContagemResultado_Descricao_Sel = GetNextPar( );
                  AV276TFContagemrResultado_SistemaSigla = GetNextPar( );
                  AV277TFContagemrResultado_SistemaSigla_Sel = GetNextPar( );
                  AV243TFContagemResultado_ContratadaSigla = GetNextPar( );
                  AV244TFContagemResultado_ContratadaSigla_Sel = GetNextPar( );
                  AV271TFContagemResultado_StatusDmn_SelsJson = GetNextPar( );
                  AV275TFContagemResultado_StatusUltCnt_SelsJson = GetNextPar( );
                  AV242TFContagemResultado_Baseline_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV265TFContagemResultado_Servico = GetNextPar( );
                  AV266TFContagemResultado_Servico_Sel = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV267TFContagemResultado_Servico_SelDsc = GetNextPar( );
                  AV257TFContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV258TFContagemResultado_PFBFSUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV263TFContagemResultado_PFLFSUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV264TFContagemResultado_PFLFSUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV255TFContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV256TFContagemResultado_PFBFMUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV261TFContagemResultado_PFLFMUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV262TFContagemResultado_PFLFMUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV251TFContagemResultado_EsforcoTotal = GetNextPar( );
                  AV252TFContagemResultado_EsforcoTotal_Sel = GetNextPar( );
                  AV259TFContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  AV260TFContagemResultado_PFFinal_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV224OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV225OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV217GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aexportreportwwcontagemresultadocstm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aexportreportwwcontagemresultadocstm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           String aP2_TFContratada_AreaTrabalhoDes ,
                           String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                           DateTime aP4_TFContagemResultado_DataDmn ,
                           DateTime aP5_TFContagemResultado_DataDmn_To ,
                           DateTime aP6_TFContagemResultado_DataUltCnt ,
                           DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                           String aP8_TFContagemResultado_OsFsOsFm ,
                           String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP10_TFContagemResultado_Descricao ,
                           String aP11_TFContagemResultado_Descricao_Sel ,
                           String aP12_TFContagemrResultado_SistemaSigla ,
                           String aP13_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP14_TFContagemResultado_ContratadaSigla ,
                           String aP15_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                           String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                           short aP18_TFContagemResultado_Baseline_Sel ,
                           String aP19_TFContagemResultado_Servico ,
                           int aP20_TFContagemResultado_Servico_Sel ,
                           String aP21_TFContagemResultado_Servico_SelDsc ,
                           decimal aP22_TFContagemResultado_PFBFSUltima ,
                           decimal aP23_TFContagemResultado_PFBFSUltima_To ,
                           decimal aP24_TFContagemResultado_PFLFSUltima ,
                           decimal aP25_TFContagemResultado_PFLFSUltima_To ,
                           decimal aP26_TFContagemResultado_PFBFMUltima ,
                           decimal aP27_TFContagemResultado_PFBFMUltima_To ,
                           decimal aP28_TFContagemResultado_PFLFMUltima ,
                           decimal aP29_TFContagemResultado_PFLFMUltima_To ,
                           String aP30_TFContagemResultado_EsforcoTotal ,
                           String aP31_TFContagemResultado_EsforcoTotal_Sel ,
                           decimal aP32_TFContagemResultado_PFFinal ,
                           decimal aP33_TFContagemResultado_PFFinal_To ,
                           short aP34_OrderedBy ,
                           bool aP35_OrderedDsc ,
                           String aP36_GridStateXML )
      {
         this.AV159Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV160Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV278TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         this.AV279TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         this.AV245TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV246TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV247TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV248TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV253TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         this.AV254TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         this.AV249TFContagemResultado_Descricao = aP10_TFContagemResultado_Descricao;
         this.AV250TFContagemResultado_Descricao_Sel = aP11_TFContagemResultado_Descricao_Sel;
         this.AV276TFContagemrResultado_SistemaSigla = aP12_TFContagemrResultado_SistemaSigla;
         this.AV277TFContagemrResultado_SistemaSigla_Sel = aP13_TFContagemrResultado_SistemaSigla_Sel;
         this.AV243TFContagemResultado_ContratadaSigla = aP14_TFContagemResultado_ContratadaSigla;
         this.AV244TFContagemResultado_ContratadaSigla_Sel = aP15_TFContagemResultado_ContratadaSigla_Sel;
         this.AV271TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         this.AV275TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV242TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         this.AV265TFContagemResultado_Servico = aP19_TFContagemResultado_Servico;
         this.AV266TFContagemResultado_Servico_Sel = aP20_TFContagemResultado_Servico_Sel;
         this.AV267TFContagemResultado_Servico_SelDsc = aP21_TFContagemResultado_Servico_SelDsc;
         this.AV257TFContagemResultado_PFBFSUltima = aP22_TFContagemResultado_PFBFSUltima;
         this.AV258TFContagemResultado_PFBFSUltima_To = aP23_TFContagemResultado_PFBFSUltima_To;
         this.AV263TFContagemResultado_PFLFSUltima = aP24_TFContagemResultado_PFLFSUltima;
         this.AV264TFContagemResultado_PFLFSUltima_To = aP25_TFContagemResultado_PFLFSUltima_To;
         this.AV255TFContagemResultado_PFBFMUltima = aP26_TFContagemResultado_PFBFMUltima;
         this.AV256TFContagemResultado_PFBFMUltima_To = aP27_TFContagemResultado_PFBFMUltima_To;
         this.AV261TFContagemResultado_PFLFMUltima = aP28_TFContagemResultado_PFLFMUltima;
         this.AV262TFContagemResultado_PFLFMUltima_To = aP29_TFContagemResultado_PFLFMUltima_To;
         this.AV251TFContagemResultado_EsforcoTotal = aP30_TFContagemResultado_EsforcoTotal;
         this.AV252TFContagemResultado_EsforcoTotal_Sel = aP31_TFContagemResultado_EsforcoTotal_Sel;
         this.AV259TFContagemResultado_PFFinal = aP32_TFContagemResultado_PFFinal;
         this.AV260TFContagemResultado_PFFinal_To = aP33_TFContagemResultado_PFFinal_To;
         this.AV224OrderedBy = aP34_OrderedBy;
         this.AV225OrderedDsc = aP35_OrderedDsc;
         this.AV217GridStateXML = aP36_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 String aP2_TFContratada_AreaTrabalhoDes ,
                                 String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                                 DateTime aP4_TFContagemResultado_DataDmn ,
                                 DateTime aP5_TFContagemResultado_DataDmn_To ,
                                 DateTime aP6_TFContagemResultado_DataUltCnt ,
                                 DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                 String aP8_TFContagemResultado_OsFsOsFm ,
                                 String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP10_TFContagemResultado_Descricao ,
                                 String aP11_TFContagemResultado_Descricao_Sel ,
                                 String aP12_TFContagemrResultado_SistemaSigla ,
                                 String aP13_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP14_TFContagemResultado_ContratadaSigla ,
                                 String aP15_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                                 String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                                 short aP18_TFContagemResultado_Baseline_Sel ,
                                 String aP19_TFContagemResultado_Servico ,
                                 int aP20_TFContagemResultado_Servico_Sel ,
                                 String aP21_TFContagemResultado_Servico_SelDsc ,
                                 decimal aP22_TFContagemResultado_PFBFSUltima ,
                                 decimal aP23_TFContagemResultado_PFBFSUltima_To ,
                                 decimal aP24_TFContagemResultado_PFLFSUltima ,
                                 decimal aP25_TFContagemResultado_PFLFSUltima_To ,
                                 decimal aP26_TFContagemResultado_PFBFMUltima ,
                                 decimal aP27_TFContagemResultado_PFBFMUltima_To ,
                                 decimal aP28_TFContagemResultado_PFLFMUltima ,
                                 decimal aP29_TFContagemResultado_PFLFMUltima_To ,
                                 String aP30_TFContagemResultado_EsforcoTotal ,
                                 String aP31_TFContagemResultado_EsforcoTotal_Sel ,
                                 decimal aP32_TFContagemResultado_PFFinal ,
                                 decimal aP33_TFContagemResultado_PFFinal_To ,
                                 short aP34_OrderedBy ,
                                 bool aP35_OrderedDsc ,
                                 String aP36_GridStateXML )
      {
         aexportreportwwcontagemresultadocstm objaexportreportwwcontagemresultadocstm;
         objaexportreportwwcontagemresultadocstm = new aexportreportwwcontagemresultadocstm();
         objaexportreportwwcontagemresultadocstm.AV159Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objaexportreportwwcontagemresultadocstm.AV160Contratada_Codigo = aP1_Contratada_Codigo;
         objaexportreportwwcontagemresultadocstm.AV278TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         objaexportreportwwcontagemresultadocstm.AV279TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         objaexportreportwwcontagemresultadocstm.AV245TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         objaexportreportwwcontagemresultadocstm.AV246TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         objaexportreportwwcontagemresultadocstm.AV247TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         objaexportreportwwcontagemresultadocstm.AV248TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         objaexportreportwwcontagemresultadocstm.AV253TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         objaexportreportwwcontagemresultadocstm.AV254TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         objaexportreportwwcontagemresultadocstm.AV249TFContagemResultado_Descricao = aP10_TFContagemResultado_Descricao;
         objaexportreportwwcontagemresultadocstm.AV250TFContagemResultado_Descricao_Sel = aP11_TFContagemResultado_Descricao_Sel;
         objaexportreportwwcontagemresultadocstm.AV276TFContagemrResultado_SistemaSigla = aP12_TFContagemrResultado_SistemaSigla;
         objaexportreportwwcontagemresultadocstm.AV277TFContagemrResultado_SistemaSigla_Sel = aP13_TFContagemrResultado_SistemaSigla_Sel;
         objaexportreportwwcontagemresultadocstm.AV243TFContagemResultado_ContratadaSigla = aP14_TFContagemResultado_ContratadaSigla;
         objaexportreportwwcontagemresultadocstm.AV244TFContagemResultado_ContratadaSigla_Sel = aP15_TFContagemResultado_ContratadaSigla_Sel;
         objaexportreportwwcontagemresultadocstm.AV271TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         objaexportreportwwcontagemresultadocstm.AV275TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         objaexportreportwwcontagemresultadocstm.AV242TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         objaexportreportwwcontagemresultadocstm.AV265TFContagemResultado_Servico = aP19_TFContagemResultado_Servico;
         objaexportreportwwcontagemresultadocstm.AV266TFContagemResultado_Servico_Sel = aP20_TFContagemResultado_Servico_Sel;
         objaexportreportwwcontagemresultadocstm.AV267TFContagemResultado_Servico_SelDsc = aP21_TFContagemResultado_Servico_SelDsc;
         objaexportreportwwcontagemresultadocstm.AV257TFContagemResultado_PFBFSUltima = aP22_TFContagemResultado_PFBFSUltima;
         objaexportreportwwcontagemresultadocstm.AV258TFContagemResultado_PFBFSUltima_To = aP23_TFContagemResultado_PFBFSUltima_To;
         objaexportreportwwcontagemresultadocstm.AV263TFContagemResultado_PFLFSUltima = aP24_TFContagemResultado_PFLFSUltima;
         objaexportreportwwcontagemresultadocstm.AV264TFContagemResultado_PFLFSUltima_To = aP25_TFContagemResultado_PFLFSUltima_To;
         objaexportreportwwcontagemresultadocstm.AV255TFContagemResultado_PFBFMUltima = aP26_TFContagemResultado_PFBFMUltima;
         objaexportreportwwcontagemresultadocstm.AV256TFContagemResultado_PFBFMUltima_To = aP27_TFContagemResultado_PFBFMUltima_To;
         objaexportreportwwcontagemresultadocstm.AV261TFContagemResultado_PFLFMUltima = aP28_TFContagemResultado_PFLFMUltima;
         objaexportreportwwcontagemresultadocstm.AV262TFContagemResultado_PFLFMUltima_To = aP29_TFContagemResultado_PFLFMUltima_To;
         objaexportreportwwcontagemresultadocstm.AV251TFContagemResultado_EsforcoTotal = aP30_TFContagemResultado_EsforcoTotal;
         objaexportreportwwcontagemresultadocstm.AV252TFContagemResultado_EsforcoTotal_Sel = aP31_TFContagemResultado_EsforcoTotal_Sel;
         objaexportreportwwcontagemresultadocstm.AV259TFContagemResultado_PFFinal = aP32_TFContagemResultado_PFFinal;
         objaexportreportwwcontagemresultadocstm.AV260TFContagemResultado_PFFinal_To = aP33_TFContagemResultado_PFFinal_To;
         objaexportreportwwcontagemresultadocstm.AV224OrderedBy = aP34_OrderedBy;
         objaexportreportwwcontagemresultadocstm.AV225OrderedDsc = aP35_OrderedDsc;
         objaexportreportwwcontagemresultadocstm.AV217GridStateXML = aP36_GridStateXML;
         objaexportreportwwcontagemresultadocstm.context.SetSubmitInitialConfig(context);
         objaexportreportwwcontagemresultadocstm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaexportreportwwcontagemresultadocstm);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aexportreportwwcontagemresultadocstm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV287WWPContext) ;
            AV285Usuario_EhGestor = (bool)(AV287WWPContext.gxTpr_Userehadministradorgam||AV287WWPContext.gxTpr_Userehgestor);
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HT90( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         AV9Codigos.FromXml(AV286WebSession.Get("Codigos"), "Collection");
         AV168Contratadas.FromXml(AV286WebSession.Get("Contratadas"), "Collection");
         AV232SDT_FiltroConsContadorFM.FromXml(AV286WebSession.Get("FiltroConsultaContador"), "");
         if ( (0==AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod) )
         {
            AV311AreaTrabalho_Codigo = AV287WWPContext.gxTpr_Areatrabalho_codigo;
         }
         HT90( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Registro de Esfor�o", 5, Gx_line+5, 1035, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( (0==AV159Contratada_AreaTrabalhoCod) )
         {
            AV294Nome = "Todas";
         }
         else
         {
            /* Using cursor P00T92 */
            pr_default.execute(0, new Object[] {AV159Contratada_AreaTrabalhoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A5AreaTrabalho_Codigo = P00T92_A5AreaTrabalho_Codigo[0];
               A6AreaTrabalho_Descricao = P00T92_A6AreaTrabalho_Descricao[0];
               AV294Nome = A6AreaTrabalho_Descricao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         HT90( false, 20) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV294Nome, "@!")), 192, Gx_line+0, 453, Gx_line+15, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("�rea de Trabalho", 8, Gx_line+0, 189, Gx_line+14, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+20);
         if ( (0==AV160Contratada_Codigo) )
         {
            AV294Nome = "Todas";
         }
         else
         {
            /* Using cursor P00T93 */
            pr_default.execute(1, new Object[] {AV160Contratada_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A40Contratada_PessoaCod = P00T93_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00T93_A39Contratada_Codigo[0];
               A41Contratada_PessoaNom = P00T93_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00T93_n41Contratada_PessoaNom[0];
               A41Contratada_PessoaNom = P00T93_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00T93_n41Contratada_PessoaNom[0];
               AV294Nome = A41Contratada_PessoaNom;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         HT90( false, 20) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV294Nome, "@!")), 192, Gx_line+0, 453, Gx_line+15, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+20);
         AV214GridState.gxTpr_Dynamicfilters.FromXml(AV217GridStateXML, "");
         if ( AV214GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV215GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV214GridState.gxTpr_Dynamicfilters.Item(1));
            AV178DynamicFiltersSelector1 = AV215GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV173DynamicFiltersOperator1 = AV215GridStateDynamicFilter.gxTpr_Operator;
               AV124ContagemResultado_OsFsOsFm1 = AV215GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124ContagemResultado_OsFsOsFm1)) )
               {
                  if ( AV173DynamicFiltersOperator1 == 0 )
                  {
                     AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV173DynamicFiltersOperator1 == 1 )
                  {
                     AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV173DynamicFiltersOperator1 == 2 )
                  {
                     AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV123ContagemResultado_OsFsOsFm = AV124ContagemResultado_OsFsOsFm1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV196FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV123ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV65ContagemResultado_DataDmn1 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
               AV60ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV65ContagemResultado_DataDmn1) )
               {
                  AV190FilterContagemResultado_DataDmnDescription = "Data Demanda";
                  AV59ContagemResultado_DataDmn = AV65ContagemResultado_DataDmn1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV60ContagemResultado_DataDmn_To1) )
               {
                  AV190FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                  AV59ContagemResultado_DataDmn = AV60ContagemResultado_DataDmn_To1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV83ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
               AV78ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV83ContagemResultado_DataUltCnt1) )
               {
                  AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                  AV77ContagemResultado_DataUltCnt = AV83ContagemResultado_DataUltCnt1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV78ContagemResultado_DataUltCnt_To1) )
               {
                  AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                  AV77ContagemResultado_DataUltCnt = AV78ContagemResultado_DataUltCnt_To1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV146ContagemResultado_StatusDmn1 = AV215GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146ContagemResultado_StatusDmn1)) )
               {
                  AV198FilterContagemResultado_StatusDmn1ValueDescription = gxdomainstatusdemanda.getDescription(context,AV146ContagemResultado_StatusDmn1);
                  AV203FilterContagemResultado_StatusDmnValueDescription = AV198FilterContagemResultado_StatusDmn1ValueDescription;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "OUTROSSTATUS") == 0 )
            {
               AV227OutrosStatus1 = AV215GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227OutrosStatus1)) )
               {
                  AV226OutrosStatus = AV227OutrosStatus1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status Demanda +", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226OutrosStatus, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
            {
               AV153ContagemResultado_StatusUltCnt1 = (short)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV153ContagemResultado_StatusUltCnt1) )
               {
                  AV204FilterContagemResultado_StatusUltCnt1ValueDescription = gxdomainstatuscontagem.getDescription(context,AV153ContagemResultado_StatusUltCnt1);
                  AV209FilterContagemResultado_StatusUltCntValueDescription = AV204FilterContagemResultado_StatusUltCnt1ValueDescription;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV209FilterContagemResultado_StatusUltCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV130ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV130ContagemResultado_Servico1) )
               {
                  AV129ContagemResultado_Servico = AV130ContagemResultado_Servico1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV129ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV30ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV30ContagemResultado_ContadorFM1) )
               {
                  AV29ContagemResultado_ContadorFM = AV30ContagemResultado_ContadorFM1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV136ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV136ContagemResultado_SistemaCod1) )
               {
                  AV135ContagemResultado_SistemaCod = AV136ContagemResultado_SistemaCod1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV135ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV41ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV41ContagemResultado_ContratadaCod1) )
               {
                  AV40ContagemResultado_ContratadaCod = AV41ContagemResultado_ContratadaCod1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
            {
               AV47ContagemResultado_ContratadaOrigemCod1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV47ContagemResultado_ContratadaOrigemCod1) )
               {
                  AV46ContagemResultado_ContratadaOrigemCod = AV47ContagemResultado_ContratadaOrigemCod1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV114ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV114ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV113ContagemResultado_NaoCnfDmnCod = AV114ContagemResultado_NaoCnfDmnCod1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV113ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV17ContagemResultado_Baseline1 = AV215GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContagemResultado_Baseline1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV17ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV183FilterContagemResultado_Baseline1ValueDescription = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV17ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV183FilterContagemResultado_Baseline1ValueDescription = "N�o";
                  }
                  AV188FilterContagemResultado_BaselineValueDescription = AV183FilterContagemResultado_Baseline1ValueDescription;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
            {
               AV173DynamicFiltersOperator1 = AV215GridStateDynamicFilter.gxTpr_Operator;
               AV100ContagemResultado_EsforcoSoma1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV100ContagemResultado_EsforcoSoma1) )
               {
                  if ( AV173DynamicFiltersOperator1 == 0 )
                  {
                     AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                  }
                  else if ( AV173DynamicFiltersOperator1 == 1 )
                  {
                     AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                  }
                  else if ( AV173DynamicFiltersOperator1 == 2 )
                  {
                     AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                  }
                  AV99ContagemResultado_EsforcoSoma = AV100ContagemResultado_EsforcoSoma1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV193FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV99ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV11ContagemResultado_Agrupador1 = AV215GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ContagemResultado_Agrupador1)) )
               {
                  AV10ContagemResultado_Agrupador = AV11ContagemResultado_Agrupador1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV93ContagemResultado_Descricao1 = AV215GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93ContagemResultado_Descricao1)) )
               {
                  AV92ContagemResultado_Descricao = AV93ContagemResultado_Descricao1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV178DynamicFiltersSelector1, "CONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV24ContagemResultado_Codigo1 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV24ContagemResultado_Codigo1) )
               {
                  AV23ContagemResultado_Codigo = AV24ContagemResultado_Codigo1;
                  HT90( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("ID", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23ContagemResultado_Codigo), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV214GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV169DynamicFiltersEnabled2 = true;
               AV215GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV214GridState.gxTpr_Dynamicfilters.Item(2));
               AV179DynamicFiltersSelector2 = AV215GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV174DynamicFiltersOperator2 = AV215GridStateDynamicFilter.gxTpr_Operator;
                  AV125ContagemResultado_OsFsOsFm2 = AV215GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125ContagemResultado_OsFsOsFm2)) )
                  {
                     if ( AV174DynamicFiltersOperator2 == 0 )
                     {
                        AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV174DynamicFiltersOperator2 == 1 )
                     {
                        AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV174DynamicFiltersOperator2 == 2 )
                     {
                        AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV123ContagemResultado_OsFsOsFm = AV125ContagemResultado_OsFsOsFm2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV196FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV123ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV66ContagemResultado_DataDmn2 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                  AV61ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV66ContagemResultado_DataDmn2) )
                  {
                     AV190FilterContagemResultado_DataDmnDescription = "Data Demanda";
                     AV59ContagemResultado_DataDmn = AV66ContagemResultado_DataDmn2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV61ContagemResultado_DataDmn_To2) )
                  {
                     AV190FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                     AV59ContagemResultado_DataDmn = AV61ContagemResultado_DataDmn_To2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV84ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                  AV79ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV84ContagemResultado_DataUltCnt2) )
                  {
                     AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                     AV77ContagemResultado_DataUltCnt = AV84ContagemResultado_DataUltCnt2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV79ContagemResultado_DataUltCnt_To2) )
                  {
                     AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                     AV77ContagemResultado_DataUltCnt = AV79ContagemResultado_DataUltCnt_To2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV147ContagemResultado_StatusDmn2 = AV215GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147ContagemResultado_StatusDmn2)) )
                  {
                     AV199FilterContagemResultado_StatusDmn2ValueDescription = gxdomainstatusdemanda.getDescription(context,AV147ContagemResultado_StatusDmn2);
                     AV203FilterContagemResultado_StatusDmnValueDescription = AV199FilterContagemResultado_StatusDmn2ValueDescription;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "OUTROSSTATUS") == 0 )
               {
                  AV228OutrosStatus2 = AV215GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV228OutrosStatus2)) )
                  {
                     AV226OutrosStatus = AV228OutrosStatus2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status Demanda +", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226OutrosStatus, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
               {
                  AV154ContagemResultado_StatusUltCnt2 = (short)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV154ContagemResultado_StatusUltCnt2) )
                  {
                     AV205FilterContagemResultado_StatusUltCnt2ValueDescription = gxdomainstatuscontagem.getDescription(context,AV154ContagemResultado_StatusUltCnt2);
                     AV209FilterContagemResultado_StatusUltCntValueDescription = AV205FilterContagemResultado_StatusUltCnt2ValueDescription;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV209FilterContagemResultado_StatusUltCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV131ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV131ContagemResultado_Servico2) )
                  {
                     AV129ContagemResultado_Servico = AV131ContagemResultado_Servico2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV129ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV31ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV31ContagemResultado_ContadorFM2) )
                  {
                     AV29ContagemResultado_ContadorFM = AV31ContagemResultado_ContadorFM2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV137ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV137ContagemResultado_SistemaCod2) )
                  {
                     AV135ContagemResultado_SistemaCod = AV137ContagemResultado_SistemaCod2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV135ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV42ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV42ContagemResultado_ContratadaCod2) )
                  {
                     AV40ContagemResultado_ContratadaCod = AV42ContagemResultado_ContratadaCod2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
               {
                  AV48ContagemResultado_ContratadaOrigemCod2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV48ContagemResultado_ContratadaOrigemCod2) )
                  {
                     AV46ContagemResultado_ContratadaOrigemCod = AV48ContagemResultado_ContratadaOrigemCod2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV115ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV115ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV113ContagemResultado_NaoCnfDmnCod = AV115ContagemResultado_NaoCnfDmnCod2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV113ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV18ContagemResultado_Baseline2 = AV215GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContagemResultado_Baseline2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV18ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV184FilterContagemResultado_Baseline2ValueDescription = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV18ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV184FilterContagemResultado_Baseline2ValueDescription = "N�o";
                     }
                     AV188FilterContagemResultado_BaselineValueDescription = AV184FilterContagemResultado_Baseline2ValueDescription;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
               {
                  AV174DynamicFiltersOperator2 = AV215GridStateDynamicFilter.gxTpr_Operator;
                  AV101ContagemResultado_EsforcoSoma2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV101ContagemResultado_EsforcoSoma2) )
                  {
                     if ( AV174DynamicFiltersOperator2 == 0 )
                     {
                        AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                     }
                     else if ( AV174DynamicFiltersOperator2 == 1 )
                     {
                        AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                     }
                     else if ( AV174DynamicFiltersOperator2 == 2 )
                     {
                        AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                     }
                     AV99ContagemResultado_EsforcoSoma = AV101ContagemResultado_EsforcoSoma2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV193FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV99ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV12ContagemResultado_Agrupador2 = AV215GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12ContagemResultado_Agrupador2)) )
                  {
                     AV10ContagemResultado_Agrupador = AV12ContagemResultado_Agrupador2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV94ContagemResultado_Descricao2 = AV215GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94ContagemResultado_Descricao2)) )
                  {
                     AV92ContagemResultado_Descricao = AV94ContagemResultado_Descricao2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV179DynamicFiltersSelector2, "CONTAGEMRESULTADO_CODIGO") == 0 )
               {
                  AV25ContagemResultado_Codigo2 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV25ContagemResultado_Codigo2) )
                  {
                     AV23ContagemResultado_Codigo = AV25ContagemResultado_Codigo2;
                     HT90( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("ID", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23ContagemResultado_Codigo), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               if ( AV214GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV170DynamicFiltersEnabled3 = true;
                  AV215GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV214GridState.gxTpr_Dynamicfilters.Item(3));
                  AV180DynamicFiltersSelector3 = AV215GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV175DynamicFiltersOperator3 = AV215GridStateDynamicFilter.gxTpr_Operator;
                     AV126ContagemResultado_OsFsOsFm3 = AV215GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126ContagemResultado_OsFsOsFm3)) )
                     {
                        if ( AV175DynamicFiltersOperator3 == 0 )
                        {
                           AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV175DynamicFiltersOperator3 == 1 )
                        {
                           AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV175DynamicFiltersOperator3 == 2 )
                        {
                           AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV123ContagemResultado_OsFsOsFm = AV126ContagemResultado_OsFsOsFm3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV196FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV123ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV67ContagemResultado_DataDmn3 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                     AV62ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV67ContagemResultado_DataDmn3) )
                     {
                        AV190FilterContagemResultado_DataDmnDescription = "Data Demanda";
                        AV59ContagemResultado_DataDmn = AV67ContagemResultado_DataDmn3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV62ContagemResultado_DataDmn_To3) )
                     {
                        AV190FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                        AV59ContagemResultado_DataDmn = AV62ContagemResultado_DataDmn_To3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV85ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                     AV80ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV85ContagemResultado_DataUltCnt3) )
                     {
                        AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                        AV77ContagemResultado_DataUltCnt = AV85ContagemResultado_DataUltCnt3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV80ContagemResultado_DataUltCnt_To3) )
                     {
                        AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                        AV77ContagemResultado_DataUltCnt = AV80ContagemResultado_DataUltCnt_To3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV148ContagemResultado_StatusDmn3 = AV215GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148ContagemResultado_StatusDmn3)) )
                     {
                        AV200FilterContagemResultado_StatusDmn3ValueDescription = gxdomainstatusdemanda.getDescription(context,AV148ContagemResultado_StatusDmn3);
                        AV203FilterContagemResultado_StatusDmnValueDescription = AV200FilterContagemResultado_StatusDmn3ValueDescription;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "OUTROSSTATUS") == 0 )
                  {
                     AV229OutrosStatus3 = AV215GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229OutrosStatus3)) )
                     {
                        AV226OutrosStatus = AV229OutrosStatus3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status Demanda +", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226OutrosStatus, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
                  {
                     AV155ContagemResultado_StatusUltCnt3 = (short)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV155ContagemResultado_StatusUltCnt3) )
                     {
                        AV206FilterContagemResultado_StatusUltCnt3ValueDescription = gxdomainstatuscontagem.getDescription(context,AV155ContagemResultado_StatusUltCnt3);
                        AV209FilterContagemResultado_StatusUltCntValueDescription = AV206FilterContagemResultado_StatusUltCnt3ValueDescription;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV209FilterContagemResultado_StatusUltCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV132ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV132ContagemResultado_Servico3) )
                     {
                        AV129ContagemResultado_Servico = AV132ContagemResultado_Servico3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV129ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV32ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV32ContagemResultado_ContadorFM3) )
                     {
                        AV29ContagemResultado_ContadorFM = AV32ContagemResultado_ContadorFM3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV138ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV138ContagemResultado_SistemaCod3) )
                     {
                        AV135ContagemResultado_SistemaCod = AV138ContagemResultado_SistemaCod3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV135ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV43ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV43ContagemResultado_ContratadaCod3) )
                     {
                        AV40ContagemResultado_ContratadaCod = AV43ContagemResultado_ContratadaCod3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                  {
                     AV49ContagemResultado_ContratadaOrigemCod3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV49ContagemResultado_ContratadaOrigemCod3) )
                     {
                        AV46ContagemResultado_ContratadaOrigemCod = AV49ContagemResultado_ContratadaOrigemCod3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV116ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV116ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV113ContagemResultado_NaoCnfDmnCod = AV116ContagemResultado_NaoCnfDmnCod3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV113ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV19ContagemResultado_Baseline3 = AV215GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultado_Baseline3)) )
                     {
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV19ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV185FilterContagemResultado_Baseline3ValueDescription = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV19ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV185FilterContagemResultado_Baseline3ValueDescription = "N�o";
                        }
                        AV188FilterContagemResultado_BaselineValueDescription = AV185FilterContagemResultado_Baseline3ValueDescription;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                  {
                     AV175DynamicFiltersOperator3 = AV215GridStateDynamicFilter.gxTpr_Operator;
                     AV102ContagemResultado_EsforcoSoma3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV102ContagemResultado_EsforcoSoma3) )
                     {
                        if ( AV175DynamicFiltersOperator3 == 0 )
                        {
                           AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                        }
                        else if ( AV175DynamicFiltersOperator3 == 1 )
                        {
                           AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                        }
                        else if ( AV175DynamicFiltersOperator3 == 2 )
                        {
                           AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                        }
                        AV99ContagemResultado_EsforcoSoma = AV102ContagemResultado_EsforcoSoma3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV193FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV99ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV13ContagemResultado_Agrupador3 = AV215GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13ContagemResultado_Agrupador3)) )
                     {
                        AV10ContagemResultado_Agrupador = AV13ContagemResultado_Agrupador3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV95ContagemResultado_Descricao3 = AV215GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95ContagemResultado_Descricao3)) )
                     {
                        AV92ContagemResultado_Descricao = AV95ContagemResultado_Descricao3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV180DynamicFiltersSelector3, "CONTAGEMRESULTADO_CODIGO") == 0 )
                  {
                     AV26ContagemResultado_Codigo3 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV26ContagemResultado_Codigo3) )
                     {
                        AV23ContagemResultado_Codigo = AV26ContagemResultado_Codigo3;
                        HT90( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("ID", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23ContagemResultado_Codigo), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  if ( AV214GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV171DynamicFiltersEnabled4 = true;
                     AV215GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV214GridState.gxTpr_Dynamicfilters.Item(4));
                     AV181DynamicFiltersSelector4 = AV215GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV176DynamicFiltersOperator4 = AV215GridStateDynamicFilter.gxTpr_Operator;
                        AV127ContagemResultado_OsFsOsFm4 = AV215GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127ContagemResultado_OsFsOsFm4)) )
                        {
                           if ( AV176DynamicFiltersOperator4 == 0 )
                           {
                              AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                           }
                           else if ( AV176DynamicFiltersOperator4 == 1 )
                           {
                              AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                           }
                           else if ( AV176DynamicFiltersOperator4 == 2 )
                           {
                              AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                           }
                           AV123ContagemResultado_OsFsOsFm = AV127ContagemResultado_OsFsOsFm4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV196FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV123ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV68ContagemResultado_DataDmn4 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                        AV63ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV68ContagemResultado_DataDmn4) )
                        {
                           AV190FilterContagemResultado_DataDmnDescription = "Data Demanda";
                           AV59ContagemResultado_DataDmn = AV68ContagemResultado_DataDmn4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                        if ( ! (DateTime.MinValue==AV63ContagemResultado_DataDmn_To4) )
                        {
                           AV190FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                           AV59ContagemResultado_DataDmn = AV63ContagemResultado_DataDmn_To4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                     {
                        AV86ContagemResultado_DataUltCnt4 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                        AV81ContagemResultado_DataUltCnt_To4 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                        if ( ! (DateTime.MinValue==AV86ContagemResultado_DataUltCnt4) )
                        {
                           AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                           AV77ContagemResultado_DataUltCnt = AV86ContagemResultado_DataUltCnt4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                        if ( ! (DateTime.MinValue==AV81ContagemResultado_DataUltCnt_To4) )
                        {
                           AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                           AV77ContagemResultado_DataUltCnt = AV81ContagemResultado_DataUltCnt_To4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV149ContagemResultado_StatusDmn4 = AV215GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149ContagemResultado_StatusDmn4)) )
                        {
                           AV201FilterContagemResultado_StatusDmn4ValueDescription = gxdomainstatusdemanda.getDescription(context,AV149ContagemResultado_StatusDmn4);
                           AV203FilterContagemResultado_StatusDmnValueDescription = AV201FilterContagemResultado_StatusDmn4ValueDescription;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "OUTROSSTATUS") == 0 )
                     {
                        AV230OutrosStatus4 = AV215GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV230OutrosStatus4)) )
                        {
                           AV226OutrosStatus = AV230OutrosStatus4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Status Demanda +", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226OutrosStatus, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
                     {
                        AV156ContagemResultado_StatusUltCnt4 = (short)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV156ContagemResultado_StatusUltCnt4) )
                        {
                           AV207FilterContagemResultado_StatusUltCnt4ValueDescription = gxdomainstatuscontagem.getDescription(context,AV156ContagemResultado_StatusUltCnt4);
                           AV209FilterContagemResultado_StatusUltCntValueDescription = AV207FilterContagemResultado_StatusUltCnt4ValueDescription;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV209FilterContagemResultado_StatusUltCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV133ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV133ContagemResultado_Servico4) )
                        {
                           AV129ContagemResultado_Servico = AV133ContagemResultado_Servico4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV129ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV33ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV33ContagemResultado_ContadorFM4) )
                        {
                           AV29ContagemResultado_ContadorFM = AV33ContagemResultado_ContadorFM4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV139ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV139ContagemResultado_SistemaCod4) )
                        {
                           AV135ContagemResultado_SistemaCod = AV139ContagemResultado_SistemaCod4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV135ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV44ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV44ContagemResultado_ContratadaCod4) )
                        {
                           AV40ContagemResultado_ContratadaCod = AV44ContagemResultado_ContratadaCod4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                     {
                        AV50ContagemResultado_ContratadaOrigemCod4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV50ContagemResultado_ContratadaOrigemCod4) )
                        {
                           AV46ContagemResultado_ContratadaOrigemCod = AV50ContagemResultado_ContratadaOrigemCod4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV117ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV117ContagemResultado_NaoCnfDmnCod4) )
                        {
                           AV113ContagemResultado_NaoCnfDmnCod = AV117ContagemResultado_NaoCnfDmnCod4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV113ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV20ContagemResultado_Baseline4 = AV215GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultado_Baseline4)) )
                        {
                           if ( StringUtil.StrCmp(StringUtil.Trim( AV20ContagemResultado_Baseline4), "S") == 0 )
                           {
                              AV186FilterContagemResultado_Baseline4ValueDescription = "Sim";
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Trim( AV20ContagemResultado_Baseline4), "N") == 0 )
                           {
                              AV186FilterContagemResultado_Baseline4ValueDescription = "N�o";
                           }
                           AV188FilterContagemResultado_BaselineValueDescription = AV186FilterContagemResultado_Baseline4ValueDescription;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                     {
                        AV176DynamicFiltersOperator4 = AV215GridStateDynamicFilter.gxTpr_Operator;
                        AV103ContagemResultado_EsforcoSoma4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV103ContagemResultado_EsforcoSoma4) )
                        {
                           if ( AV176DynamicFiltersOperator4 == 0 )
                           {
                              AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                           }
                           else if ( AV176DynamicFiltersOperator4 == 1 )
                           {
                              AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                           }
                           else if ( AV176DynamicFiltersOperator4 == 2 )
                           {
                              AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                           }
                           AV99ContagemResultado_EsforcoSoma = AV103ContagemResultado_EsforcoSoma4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV193FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV99ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV14ContagemResultado_Agrupador4 = AV215GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14ContagemResultado_Agrupador4)) )
                        {
                           AV10ContagemResultado_Agrupador = AV14ContagemResultado_Agrupador4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV96ContagemResultado_Descricao4 = AV215GridStateDynamicFilter.gxTpr_Value;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96ContagemResultado_Descricao4)) )
                        {
                           AV92ContagemResultado_Descricao = AV96ContagemResultado_Descricao4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     else if ( StringUtil.StrCmp(AV181DynamicFiltersSelector4, "CONTAGEMRESULTADO_CODIGO") == 0 )
                     {
                        AV27ContagemResultado_Codigo4 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                        if ( ! (0==AV27ContagemResultado_Codigo4) )
                        {
                           AV23ContagemResultado_Codigo = AV27ContagemResultado_Codigo4;
                           HT90( false, 20) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText("ID", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23ContagemResultado_Codigo), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+20);
                        }
                     }
                     if ( AV214GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV172DynamicFiltersEnabled5 = true;
                        AV215GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV214GridState.gxTpr_Dynamicfilters.Item(5));
                        AV182DynamicFiltersSelector5 = AV215GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV177DynamicFiltersOperator5 = AV215GridStateDynamicFilter.gxTpr_Operator;
                           AV128ContagemResultado_OsFsOsFm5 = AV215GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128ContagemResultado_OsFsOsFm5)) )
                           {
                              if ( AV177DynamicFiltersOperator5 == 0 )
                              {
                                 AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                              }
                              else if ( AV177DynamicFiltersOperator5 == 1 )
                              {
                                 AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                              }
                              else if ( AV177DynamicFiltersOperator5 == 2 )
                              {
                                 AV196FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                              }
                              AV123ContagemResultado_OsFsOsFm = AV128ContagemResultado_OsFsOsFm5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV196FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV123ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV69ContagemResultado_DataDmn5 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                           AV64ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV69ContagemResultado_DataDmn5) )
                           {
                              AV190FilterContagemResultado_DataDmnDescription = "Data Demanda";
                              AV59ContagemResultado_DataDmn = AV69ContagemResultado_DataDmn5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                           if ( ! (DateTime.MinValue==AV64ContagemResultado_DataDmn_To5) )
                           {
                              AV190FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                              AV59ContagemResultado_DataDmn = AV64ContagemResultado_DataDmn_To5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV190FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV59ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                        {
                           AV87ContagemResultado_DataUltCnt5 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Value, 2);
                           AV82ContagemResultado_DataUltCnt_To5 = context.localUtil.CToD( AV215GridStateDynamicFilter.gxTpr_Valueto, 2);
                           if ( ! (DateTime.MinValue==AV87ContagemResultado_DataUltCnt5) )
                           {
                              AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                              AV77ContagemResultado_DataUltCnt = AV87ContagemResultado_DataUltCnt5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                           if ( ! (DateTime.MinValue==AV82ContagemResultado_DataUltCnt_To5) )
                           {
                              AV192FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                              AV77ContagemResultado_DataUltCnt = AV82ContagemResultado_DataUltCnt_To5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV192FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(context.localUtil.Format( AV77ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV150ContagemResultado_StatusDmn5 = AV215GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150ContagemResultado_StatusDmn5)) )
                           {
                              AV202FilterContagemResultado_StatusDmn5ValueDescription = gxdomainstatusdemanda.getDescription(context,AV150ContagemResultado_StatusDmn5);
                              AV203FilterContagemResultado_StatusDmnValueDescription = AV202FilterContagemResultado_StatusDmn5ValueDescription;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV203FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "OUTROSSTATUS") == 0 )
                        {
                           AV231OutrosStatus5 = AV215GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231OutrosStatus5)) )
                           {
                              AV226OutrosStatus = AV231OutrosStatus5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Status Demanda +", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV226OutrosStatus, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSULTCNT") == 0 )
                        {
                           AV157ContagemResultado_StatusUltCnt5 = (short)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV157ContagemResultado_StatusUltCnt5) )
                           {
                              AV208FilterContagemResultado_StatusUltCnt5ValueDescription = gxdomainstatuscontagem.getDescription(context,AV157ContagemResultado_StatusUltCnt5);
                              AV209FilterContagemResultado_StatusUltCntValueDescription = AV208FilterContagemResultado_StatusUltCnt5ValueDescription;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Status Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV209FilterContagemResultado_StatusUltCntValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV134ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV134ContagemResultado_Servico5) )
                           {
                              AV129ContagemResultado_Servico = AV134ContagemResultado_Servico5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV129ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV34ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV34ContagemResultado_ContadorFM5) )
                           {
                              AV29ContagemResultado_ContadorFM = AV34ContagemResultado_ContadorFM5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV140ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV140ContagemResultado_SistemaCod5) )
                           {
                              AV135ContagemResultado_SistemaCod = AV140ContagemResultado_SistemaCod5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV135ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV45ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV45ContagemResultado_ContratadaCod5) )
                           {
                              AV40ContagemResultado_ContratadaCod = AV45ContagemResultado_ContratadaCod5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                        {
                           AV51ContagemResultado_ContratadaOrigemCod5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV51ContagemResultado_ContratadaOrigemCod5) )
                           {
                              AV46ContagemResultado_ContratadaOrigemCod = AV51ContagemResultado_ContratadaOrigemCod5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV118ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV118ContagemResultado_NaoCnfDmnCod5) )
                           {
                              AV113ContagemResultado_NaoCnfDmnCod = AV118ContagemResultado_NaoCnfDmnCod5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV113ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV21ContagemResultado_Baseline5 = AV215GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContagemResultado_Baseline5)) )
                           {
                              if ( StringUtil.StrCmp(StringUtil.Trim( AV21ContagemResultado_Baseline5), "S") == 0 )
                              {
                                 AV187FilterContagemResultado_Baseline5ValueDescription = "Sim";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( AV21ContagemResultado_Baseline5), "N") == 0 )
                              {
                                 AV187FilterContagemResultado_Baseline5ValueDescription = "N�o";
                              }
                              AV188FilterContagemResultado_BaselineValueDescription = AV187FilterContagemResultado_Baseline5ValueDescription;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                        {
                           AV177DynamicFiltersOperator5 = AV215GridStateDynamicFilter.gxTpr_Operator;
                           AV104ContagemResultado_EsforcoSoma5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV104ContagemResultado_EsforcoSoma5) )
                           {
                              if ( AV177DynamicFiltersOperator5 == 0 )
                              {
                                 AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                              }
                              else if ( AV177DynamicFiltersOperator5 == 1 )
                              {
                                 AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                              }
                              else if ( AV177DynamicFiltersOperator5 == 2 )
                              {
                                 AV193FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                              }
                              AV99ContagemResultado_EsforcoSoma = AV104ContagemResultado_EsforcoSoma5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV193FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV99ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV15ContagemResultado_Agrupador5 = AV215GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15ContagemResultado_Agrupador5)) )
                           {
                              AV10ContagemResultado_Agrupador = AV15ContagemResultado_Agrupador5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV97ContagemResultado_Descricao5 = AV215GridStateDynamicFilter.gxTpr_Value;
                           if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97ContagemResultado_Descricao5)) )
                           {
                              AV92ContagemResultado_Descricao = AV97ContagemResultado_Descricao5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                        else if ( StringUtil.StrCmp(AV182DynamicFiltersSelector5, "CONTAGEMRESULTADO_CODIGO") == 0 )
                        {
                           AV28ContagemResultado_Codigo5 = (int)(NumberUtil.Val( AV215GridStateDynamicFilter.gxTpr_Value, "."));
                           if ( ! (0==AV28ContagemResultado_Codigo5) )
                           {
                              AV23ContagemResultado_Codigo = AV28ContagemResultado_Codigo5;
                              HT90( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText("ID", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23ContagemResultado_Codigo), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                           }
                        }
                     }
                  }
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV279TFContratada_AreaTrabalhoDes_Sel)) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("�rea", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV279TFContratada_AreaTrabalhoDes_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV278TFContratada_AreaTrabalhoDes)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("�rea", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV278TFContratada_AreaTrabalhoDes, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (DateTime.MinValue==AV245TFContagemResultado_DataDmn) && (DateTime.MinValue==AV246TFContagemResultado_DataDmn_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Dt.Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV245TFContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Dt.Demanda (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV246TFContagemResultado_DataDmn_To, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV247TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV248TFContagemResultado_DataUltCnt_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Dt.Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV247TFContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Dt.Contagem (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV248TFContagemResultado_DataUltCnt_To, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV254TFContagemResultado_OsFsOsFm_Sel)) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV254TFContagemResultado_OsFsOsFm_Sel, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV253TFContagemResultado_OsFsOsFm)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV253TFContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV250TFContagemResultado_Descricao_Sel)) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV250TFContagemResultado_Descricao_Sel, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV249TFContagemResultado_Descricao)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV249TFContagemResultado_Descricao, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV277TFContagemrResultado_SistemaSigla_Sel)) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV277TFContagemrResultado_SistemaSigla_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV276TFContagemrResultado_SistemaSigla)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV276TFContagemrResultado_SistemaSigla, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV244TFContagemResultado_ContratadaSigla_Sel)) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV244TFContagemResultado_ContratadaSigla_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243TFContagemResultado_ContratadaSigla)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV243TFContagemResultado_ContratadaSigla, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         AV270TFContagemResultado_StatusDmn_Sels.FromJSonString(AV271TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( AV270TFContagemResultado_StatusDmn_Sels.Count == 0 ) )
         {
            AV218i = 1;
            AV322GXV1 = 1;
            while ( AV322GXV1 <= AV270TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV268TFContagemResultado_StatusDmn_Sel = AV270TFContagemResultado_StatusDmn_Sels.GetString(AV322GXV1);
               if ( AV218i == 1 )
               {
                  AV269TFContagemResultado_StatusDmn_SelDscs = "";
               }
               else
               {
                  AV269TFContagemResultado_StatusDmn_SelDscs = AV269TFContagemResultado_StatusDmn_SelDscs + ", ";
               }
               AV212FilterTFContagemResultado_StatusDmn_SelValueDescription = gxdomainstatusdemanda.getDescription(context,AV268TFContagemResultado_StatusDmn_Sel);
               AV269TFContagemResultado_StatusDmn_SelDscs = AV269TFContagemResultado_StatusDmn_SelDscs + AV212FilterTFContagemResultado_StatusDmn_SelValueDescription;
               AV218i = (long)(AV218i+1);
               AV322GXV1 = (int)(AV322GXV1+1);
            }
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV269TFContagemResultado_StatusDmn_SelDscs, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV274TFContagemResultado_StatusUltCnt_Sels.FromJSonString(AV275TFContagemResultado_StatusUltCnt_SelsJson);
         if ( ! ( AV274TFContagemResultado_StatusUltCnt_Sels.Count == 0 ) )
         {
            AV218i = 1;
            AV323GXV2 = 1;
            while ( AV323GXV2 <= AV274TFContagemResultado_StatusUltCnt_Sels.Count )
            {
               AV272TFContagemResultado_StatusUltCnt_Sel = (short)(AV274TFContagemResultado_StatusUltCnt_Sels.GetNumeric(AV323GXV2));
               if ( AV218i == 1 )
               {
                  AV273TFContagemResultado_StatusUltCnt_SelDscs = "";
               }
               else
               {
                  AV273TFContagemResultado_StatusUltCnt_SelDscs = AV273TFContagemResultado_StatusUltCnt_SelDscs + ", ";
               }
               AV213FilterTFContagemResultado_StatusUltCnt_SelValueDescription = gxdomainstatuscontagem.getDescription(context,AV272TFContagemResultado_StatusUltCnt_Sel);
               AV273TFContagemResultado_StatusUltCnt_SelDscs = AV273TFContagemResultado_StatusUltCnt_SelDscs + AV213FilterTFContagemResultado_StatusUltCnt_SelValueDescription;
               AV218i = (long)(AV218i+1);
               AV323GXV2 = (int)(AV323GXV2+1);
            }
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status Cnt", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV273TFContagemResultado_StatusUltCnt_SelDscs, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV242TFContagemResultado_Baseline_Sel) )
         {
            if ( AV242TFContagemResultado_Baseline_Sel == 1 )
            {
               AV211FilterTFContagemResultado_Baseline_SelValueDescription = "Marcado";
            }
            else if ( AV242TFContagemResultado_Baseline_Sel == 2 )
            {
               AV211FilterTFContagemResultado_Baseline_SelValueDescription = "Desmarcado";
            }
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("BS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV211FilterTFContagemResultado_Baseline_SelValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV266TFContagemResultado_Servico_Sel) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV267TFContagemResultado_Servico_SelDsc, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV265TFContagemResultado_Servico)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV265TFContagemResultado_Servico, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV257TFContagemResultado_PFBFSUltima) && (Convert.ToDecimal(0)==AV258TFContagemResultado_PFBFSUltima_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV257TFContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FS (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV258TFContagemResultado_PFBFSUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV263TFContagemResultado_PFLFSUltima) && (Convert.ToDecimal(0)==AV264TFContagemResultado_PFLFSUltima_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV263TFContagemResultado_PFLFSUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FS (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV264TFContagemResultado_PFLFSUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV255TFContagemResultado_PFBFMUltima) && (Convert.ToDecimal(0)==AV256TFContagemResultado_PFBFMUltima_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV255TFContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FM (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV256TFContagemResultado_PFBFMUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV261TFContagemResultado_PFLFMUltima) && (Convert.ToDecimal(0)==AV262TFContagemResultado_PFLFMUltima_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV261TFContagemResultado_PFLFMUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FM (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV262TFContagemResultado_PFLFMUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV252TFContagemResultado_EsforcoTotal_Sel)) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Esfor�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV252TFContagemResultado_EsforcoTotal_Sel, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV251TFContagemResultado_EsforcoTotal)) )
            {
               HT90( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Esfor�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV251TFContagemResultado_EsforcoTotal, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV259TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV260TFContagemResultado_PFFinal_To) ) )
         {
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV259TFContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            HT90( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV260TFContagemResultado_PFFinal_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         if ( (0==AV159Contratada_AreaTrabalhoCod) )
         {
            HT90( false, 35) ;
            getPrinter().GxDrawLine(5, Gx_line+30, 110, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(119, Gx_line+30, 174, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(182, Gx_line+30, 237, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(243, Gx_line+30, 298, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(305, Gx_line+30, 360, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(366, Gx_line+30, 421, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(428, Gx_line+30, 483, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(489, Gx_line+30, 544, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(549, Gx_line+30, 604, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(608, Gx_line+30, 663, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(669, Gx_line+30, 724, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(731, Gx_line+30, 786, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(792, Gx_line+30, 847, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(852, Gx_line+30, 907, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(911, Gx_line+30, 966, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(972, Gx_line+30, 1027, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(1031, Gx_line+30, 1086, Gx_line+30, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("�rea", 5, Gx_line+15, 110, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Dt.Demanda", 119, Gx_line+15, 174, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Dt.Contagem", 182, Gx_line+15, 237, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("OS Ref|OS", 243, Gx_line+15, 298, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Titulo", 305, Gx_line+15, 360, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Sistema", 366, Gx_line+15, 421, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Prestadora", 428, Gx_line+15, 483, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Status Dmn", 489, Gx_line+15, 544, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Status Cnt", 549, Gx_line+15, 604, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("BS", 608, Gx_line+15, 663, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Servi�o", 669, Gx_line+15, 724, Gx_line+29, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFB FS", 731, Gx_line+15, 786, Gx_line+29, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFL FS", 792, Gx_line+15, 847, Gx_line+29, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFB FM", 852, Gx_line+15, 907, Gx_line+29, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFL FM", 911, Gx_line+15, 966, Gx_line+29, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Esfor�o", 972, Gx_line+15, 1027, Gx_line+29, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("PF Final", 1031, Gx_line+15, 1086, Gx_line+29, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+35);
         }
         else
         {
            HT90( false, 35) ;
            getPrinter().GxDrawLine(946, Gx_line+33, 1001, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(884, Gx_line+33, 939, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(823, Gx_line+33, 878, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(753, Gx_line+33, 808, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(692, Gx_line+33, 747, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(630, Gx_line+33, 685, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(569, Gx_line+33, 624, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(507, Gx_line+33, 562, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(447, Gx_line+33, 502, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(385, Gx_line+33, 440, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(324, Gx_line+33, 379, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(262, Gx_line+33, 317, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(194, Gx_line+33, 249, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(131, Gx_line+33, 186, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(72, Gx_line+33, 127, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(10, Gx_line+33, 65, Gx_line+33, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final", 944, Gx_line+17, 999, Gx_line+31, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Esfor�o", 882, Gx_line+17, 937, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("PFL FM", 813, Gx_line+17, 868, Gx_line+31, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFB FM", 751, Gx_line+17, 806, Gx_line+31, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFL FS", 690, Gx_line+17, 745, Gx_line+31, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("PFB FS", 628, Gx_line+17, 683, Gx_line+31, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Servi�o", 567, Gx_line+17, 622, Gx_line+31, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("BS", 507, Gx_line+17, 562, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Status Cnt", 445, Gx_line+17, 500, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Status Dmn", 383, Gx_line+17, 438, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Prestadora", 322, Gx_line+17, 377, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Sistema", 260, Gx_line+17, 315, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Titulo", 194, Gx_line+17, 249, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("OS Ref|OS", 131, Gx_line+17, 186, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Dt.Contagem", 70, Gx_line+17, 125, Gx_line+31, 0, 0, 0, 0) ;
            getPrinter().GxDrawText("Dt.Demanda", 8, Gx_line+17, 63, Gx_line+31, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+35);
         }
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod = AV159Contratada_AreaTrabalhoCod;
         AV326WWContagemResultadoDS_2_Contratada_codigo = AV160Contratada_Codigo;
         AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 = AV178DynamicFiltersSelector1;
         AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 = AV173DynamicFiltersOperator1;
         AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = AV124ContagemResultado_OsFsOsFm1;
         AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1 = AV65ContagemResultado_DataDmn1;
         AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 = AV60ContagemResultado_DataDmn_To1;
         AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 = AV83ContagemResultado_DataUltCnt1;
         AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 = AV78ContagemResultado_DataUltCnt_To1;
         AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1 = DateTimeUtil.ResetTime(AV302ContagemResultado_DataPrevista1);
         AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 = DateTimeUtil.ResetTime(AV297ContagemResultado_DataPrevista_To1);
         AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1 = AV146ContagemResultado_StatusDmn1;
         AV337WWContagemResultadoDS_13_Outrosstatus1 = AV227OutrosStatus1;
         AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 = AV153ContagemResultado_StatusUltCnt1;
         AV339WWContagemResultadoDS_15_Contagemresultado_servico1 = AV130ContagemResultado_Servico1;
         AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 = AV312ContagemResultado_CntSrvPrrCod1;
         AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1 = AV30ContagemResultado_ContadorFM1;
         AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1 = AV136ContagemResultado_SistemaCod1;
         AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1 = AV41ContagemResultado_ContratadaCod1;
         AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 = AV47ContagemResultado_ContratadaOrigemCod1;
         AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 = AV114ContagemResultado_NaoCnfDmnCod1;
         AV346WWContagemResultadoDS_22_Contagemresultado_baseline1 = AV17ContagemResultado_Baseline1;
         AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 = AV100ContagemResultado_EsforcoSoma1;
         AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1 = AV11ContagemResultado_Agrupador1;
         AV349WWContagemResultadoDS_25_Contagemresultado_descricao1 = AV93ContagemResultado_Descricao1;
         AV350WWContagemResultadoDS_26_Contagemresultado_codigo1 = AV24ContagemResultado_Codigo1;
         AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 = AV169DynamicFiltersEnabled2;
         AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 = AV179DynamicFiltersSelector2;
         AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 = AV174DynamicFiltersOperator2;
         AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = AV125ContagemResultado_OsFsOsFm2;
         AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2 = AV66ContagemResultado_DataDmn2;
         AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 = AV61ContagemResultado_DataDmn_To2;
         AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 = AV84ContagemResultado_DataUltCnt2;
         AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 = AV79ContagemResultado_DataUltCnt_To2;
         AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2 = DateTimeUtil.ResetTime(AV303ContagemResultado_DataPrevista2);
         AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 = DateTimeUtil.ResetTime(AV298ContagemResultado_DataPrevista_To2);
         AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2 = AV147ContagemResultado_StatusDmn2;
         AV362WWContagemResultadoDS_38_Outrosstatus2 = AV228OutrosStatus2;
         AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 = AV154ContagemResultado_StatusUltCnt2;
         AV364WWContagemResultadoDS_40_Contagemresultado_servico2 = AV131ContagemResultado_Servico2;
         AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 = AV313ContagemResultado_CntSrvPrrCod2;
         AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2 = AV31ContagemResultado_ContadorFM2;
         AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2 = AV137ContagemResultado_SistemaCod2;
         AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2 = AV42ContagemResultado_ContratadaCod2;
         AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 = AV48ContagemResultado_ContratadaOrigemCod2;
         AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 = AV115ContagemResultado_NaoCnfDmnCod2;
         AV371WWContagemResultadoDS_47_Contagemresultado_baseline2 = AV18ContagemResultado_Baseline2;
         AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 = AV101ContagemResultado_EsforcoSoma2;
         AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2 = AV12ContagemResultado_Agrupador2;
         AV374WWContagemResultadoDS_50_Contagemresultado_descricao2 = AV94ContagemResultado_Descricao2;
         AV375WWContagemResultadoDS_51_Contagemresultado_codigo2 = AV25ContagemResultado_Codigo2;
         AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 = AV170DynamicFiltersEnabled3;
         AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 = AV180DynamicFiltersSelector3;
         AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 = AV175DynamicFiltersOperator3;
         AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = AV126ContagemResultado_OsFsOsFm3;
         AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3 = AV67ContagemResultado_DataDmn3;
         AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 = AV62ContagemResultado_DataDmn_To3;
         AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 = AV85ContagemResultado_DataUltCnt3;
         AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 = AV80ContagemResultado_DataUltCnt_To3;
         AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3 = DateTimeUtil.ResetTime(AV304ContagemResultado_DataPrevista3);
         AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 = DateTimeUtil.ResetTime(AV299ContagemResultado_DataPrevista_To3);
         AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3 = AV148ContagemResultado_StatusDmn3;
         AV387WWContagemResultadoDS_63_Outrosstatus3 = AV229OutrosStatus3;
         AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 = AV155ContagemResultado_StatusUltCnt3;
         AV389WWContagemResultadoDS_65_Contagemresultado_servico3 = AV132ContagemResultado_Servico3;
         AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 = AV314ContagemResultado_CntSrvPrrCod3;
         AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3 = AV32ContagemResultado_ContadorFM3;
         AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3 = AV138ContagemResultado_SistemaCod3;
         AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3 = AV43ContagemResultado_ContratadaCod3;
         AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 = AV49ContagemResultado_ContratadaOrigemCod3;
         AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 = AV116ContagemResultado_NaoCnfDmnCod3;
         AV396WWContagemResultadoDS_72_Contagemresultado_baseline3 = AV19ContagemResultado_Baseline3;
         AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 = AV102ContagemResultado_EsforcoSoma3;
         AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3 = AV13ContagemResultado_Agrupador3;
         AV399WWContagemResultadoDS_75_Contagemresultado_descricao3 = AV95ContagemResultado_Descricao3;
         AV400WWContagemResultadoDS_76_Contagemresultado_codigo3 = AV26ContagemResultado_Codigo3;
         AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 = AV171DynamicFiltersEnabled4;
         AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 = AV181DynamicFiltersSelector4;
         AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 = AV176DynamicFiltersOperator4;
         AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = AV127ContagemResultado_OsFsOsFm4;
         AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4 = AV68ContagemResultado_DataDmn4;
         AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 = AV63ContagemResultado_DataDmn_To4;
         AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 = AV86ContagemResultado_DataUltCnt4;
         AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 = AV81ContagemResultado_DataUltCnt_To4;
         AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4 = DateTimeUtil.ResetTime(AV305ContagemResultado_DataPrevista4);
         AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 = DateTimeUtil.ResetTime(AV300ContagemResultado_DataPrevista_To4);
         AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4 = AV149ContagemResultado_StatusDmn4;
         AV412WWContagemResultadoDS_88_Outrosstatus4 = AV230OutrosStatus4;
         AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 = AV156ContagemResultado_StatusUltCnt4;
         AV414WWContagemResultadoDS_90_Contagemresultado_servico4 = AV133ContagemResultado_Servico4;
         AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 = AV315ContagemResultado_CntSrvPrrCod4;
         AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4 = AV33ContagemResultado_ContadorFM4;
         AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4 = AV139ContagemResultado_SistemaCod4;
         AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4 = AV44ContagemResultado_ContratadaCod4;
         AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 = AV50ContagemResultado_ContratadaOrigemCod4;
         AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 = AV117ContagemResultado_NaoCnfDmnCod4;
         AV421WWContagemResultadoDS_97_Contagemresultado_baseline4 = AV20ContagemResultado_Baseline4;
         AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 = AV103ContagemResultado_EsforcoSoma4;
         AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4 = AV14ContagemResultado_Agrupador4;
         AV424WWContagemResultadoDS_100_Contagemresultado_descricao4 = AV96ContagemResultado_Descricao4;
         AV425WWContagemResultadoDS_101_Contagemresultado_codigo4 = AV27ContagemResultado_Codigo4;
         AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 = AV172DynamicFiltersEnabled5;
         AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 = AV182DynamicFiltersSelector5;
         AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 = AV177DynamicFiltersOperator5;
         AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = AV128ContagemResultado_OsFsOsFm5;
         AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5 = AV69ContagemResultado_DataDmn5;
         AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 = AV64ContagemResultado_DataDmn_To5;
         AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 = AV87ContagemResultado_DataUltCnt5;
         AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 = AV82ContagemResultado_DataUltCnt_To5;
         AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5 = DateTimeUtil.ResetTime(AV306ContagemResultado_DataPrevista5);
         AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 = DateTimeUtil.ResetTime(AV301ContagemResultado_DataPrevista_To5);
         AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5 = AV150ContagemResultado_StatusDmn5;
         AV437WWContagemResultadoDS_113_Outrosstatus5 = AV231OutrosStatus5;
         AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 = AV157ContagemResultado_StatusUltCnt5;
         AV439WWContagemResultadoDS_115_Contagemresultado_servico5 = AV134ContagemResultado_Servico5;
         AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 = AV317ContagemResultado_CntSrvPrrCod5;
         AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5 = AV34ContagemResultado_ContadorFM5;
         AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5 = AV140ContagemResultado_SistemaCod5;
         AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5 = AV45ContagemResultado_ContratadaCod5;
         AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 = AV51ContagemResultado_ContratadaOrigemCod5;
         AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 = AV118ContagemResultado_NaoCnfDmnCod5;
         AV446WWContagemResultadoDS_122_Contagemresultado_baseline5 = AV21ContagemResultado_Baseline5;
         AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 = AV104ContagemResultado_EsforcoSoma5;
         AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5 = AV15ContagemResultado_Agrupador5;
         AV449WWContagemResultadoDS_125_Contagemresultado_descricao5 = AV97ContagemResultado_Descricao5;
         AV450WWContagemResultadoDS_126_Contagemresultado_codigo5 = AV28ContagemResultado_Codigo5;
         AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = AV278TFContratada_AreaTrabalhoDes;
         AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel = AV279TFContratada_AreaTrabalhoDes_Sel;
         AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn = AV245TFContagemResultado_DataDmn;
         AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to = AV246TFContagemResultado_DataDmn_To;
         AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt = AV247TFContagemResultado_DataUltCnt;
         AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to = AV248TFContagemResultado_DataUltCnt_To;
         AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista = AV296TFContagemResultado_DataPrevista;
         AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to = AV308TFContagemResultado_DataPrevista_To;
         AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = AV253TFContagemResultado_OsFsOsFm;
         AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel = AV254TFContagemResultado_OsFsOsFm_Sel;
         AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao = AV249TFContagemResultado_Descricao;
         AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel = AV250TFContagemResultado_Descricao_Sel;
         AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = AV276TFContagemrResultado_SistemaSigla;
         AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel = AV277TFContagemrResultado_SistemaSigla_Sel;
         AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = AV243TFContagemResultado_ContratadaSigla;
         AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel = AV244TFContagemResultado_ContratadaSigla_Sel;
         AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = AV309TFContagemResultado_CntNum;
         AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel = AV310TFContagemResultado_CntNum_Sel;
         AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels = AV270TFContagemResultado_StatusDmn_Sels;
         AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels = AV274TFContagemResultado_StatusUltCnt_Sels;
         AV471WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel = AV242TFContagemResultado_Baseline_Sel;
         AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico = AV265TFContagemResultado_Servico;
         AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel = AV266TFContagemResultado_Servico_Sel;
         AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal = AV251TFContagemResultado_EsforcoTotal;
         AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel = AV252TFContagemResultado_EsforcoTotal_Sel;
         AV476WWContagemResultadoDS_152_Tfcontagemresultado_pffinal = AV259TFContagemResultado_PFFinal;
         AV477WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to = AV260TFContagemResultado_PFFinal_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              A456ContagemResultado_Codigo ,
                                              AV9Codigos ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV168Contratadas ,
                                              AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod ,
                                              AV287WWPContext.gxTpr_Contratada_codigo ,
                                              AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 ,
                                              AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 ,
                                              AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ,
                                              AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1 ,
                                              AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 ,
                                              AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1 ,
                                              AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 ,
                                              AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1 ,
                                              AV337WWContagemResultadoDS_13_Outrosstatus1 ,
                                              AV339WWContagemResultadoDS_15_Contagemresultado_servico1 ,
                                              AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 ,
                                              AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1 ,
                                              AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1 ,
                                              AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 ,
                                              AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 ,
                                              AV346WWContagemResultadoDS_22_Contagemresultado_baseline1 ,
                                              AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 ,
                                              AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1 ,
                                              AV349WWContagemResultadoDS_25_Contagemresultado_descricao1 ,
                                              AV350WWContagemResultadoDS_26_Contagemresultado_codigo1 ,
                                              AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 ,
                                              AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 ,
                                              AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 ,
                                              AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ,
                                              AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2 ,
                                              AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 ,
                                              AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2 ,
                                              AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 ,
                                              AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2 ,
                                              AV362WWContagemResultadoDS_38_Outrosstatus2 ,
                                              AV364WWContagemResultadoDS_40_Contagemresultado_servico2 ,
                                              AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 ,
                                              AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2 ,
                                              AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2 ,
                                              AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 ,
                                              AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 ,
                                              AV371WWContagemResultadoDS_47_Contagemresultado_baseline2 ,
                                              AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 ,
                                              AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2 ,
                                              AV374WWContagemResultadoDS_50_Contagemresultado_descricao2 ,
                                              AV375WWContagemResultadoDS_51_Contagemresultado_codigo2 ,
                                              AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 ,
                                              AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 ,
                                              AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 ,
                                              AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ,
                                              AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3 ,
                                              AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 ,
                                              AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3 ,
                                              AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 ,
                                              AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3 ,
                                              AV387WWContagemResultadoDS_63_Outrosstatus3 ,
                                              AV389WWContagemResultadoDS_65_Contagemresultado_servico3 ,
                                              AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 ,
                                              AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3 ,
                                              AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3 ,
                                              AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 ,
                                              AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 ,
                                              AV396WWContagemResultadoDS_72_Contagemresultado_baseline3 ,
                                              AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 ,
                                              AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3 ,
                                              AV399WWContagemResultadoDS_75_Contagemresultado_descricao3 ,
                                              AV400WWContagemResultadoDS_76_Contagemresultado_codigo3 ,
                                              AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 ,
                                              AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 ,
                                              AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 ,
                                              AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ,
                                              AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4 ,
                                              AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 ,
                                              AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4 ,
                                              AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 ,
                                              AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4 ,
                                              AV412WWContagemResultadoDS_88_Outrosstatus4 ,
                                              AV414WWContagemResultadoDS_90_Contagemresultado_servico4 ,
                                              AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 ,
                                              AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4 ,
                                              AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4 ,
                                              AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 ,
                                              AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 ,
                                              AV421WWContagemResultadoDS_97_Contagemresultado_baseline4 ,
                                              AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 ,
                                              AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4 ,
                                              AV424WWContagemResultadoDS_100_Contagemresultado_descricao4 ,
                                              AV425WWContagemResultadoDS_101_Contagemresultado_codigo4 ,
                                              AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 ,
                                              AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 ,
                                              AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 ,
                                              AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ,
                                              AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5 ,
                                              AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 ,
                                              AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5 ,
                                              AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 ,
                                              AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5 ,
                                              AV437WWContagemResultadoDS_113_Outrosstatus5 ,
                                              AV439WWContagemResultadoDS_115_Contagemresultado_servico5 ,
                                              AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 ,
                                              AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5 ,
                                              AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5 ,
                                              AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 ,
                                              AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 ,
                                              AV446WWContagemResultadoDS_122_Contagemresultado_baseline5 ,
                                              AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 ,
                                              AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5 ,
                                              AV449WWContagemResultadoDS_125_Contagemresultado_descricao5 ,
                                              AV450WWContagemResultadoDS_126_Contagemresultado_codigo5 ,
                                              AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel ,
                                              AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ,
                                              AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn ,
                                              AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to ,
                                              AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista ,
                                              AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to ,
                                              AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel ,
                                              AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ,
                                              AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel ,
                                              AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao ,
                                              AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ,
                                              AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ,
                                              AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel ,
                                              AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ,
                                              AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV471WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel ,
                                              AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel ,
                                              AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico ,
                                              AV9Codigos.Count ,
                                              AV168Contratadas.Count ,
                                              AV159Contratada_AreaTrabalhoCod ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Abertas ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Solicitadas ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Soconfirmadas ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Servico ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Sistema ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Semliquidar ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Lote ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV287WWPContext.gxTpr_Contratada_pessoacod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A1443ContagemResultado_CntSrvPrrCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A805ContagemResultado_ContratadaOrigemCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A598ContagemResultado_Baseline ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A1612ContagemResultado_CntNum ,
                                              A605Servico_Sigla ,
                                              A1043ContagemResultado_LiqLogCod ,
                                              A597ContagemResultado_LoteAceiteCod ,
                                              AV224OrderedBy ,
                                              AV225OrderedDsc ,
                                              AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 ,
                                              AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 ,
                                              AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 ,
                                              AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 ,
                                              AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2 ,
                                              AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 ,
                                              AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 ,
                                              AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 ,
                                              AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3 ,
                                              AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 ,
                                              AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 ,
                                              AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 ,
                                              AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4 ,
                                              AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 ,
                                              AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 ,
                                              AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 ,
                                              AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5 ,
                                              AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt ,
                                              AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to ,
                                              AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel ,
                                              AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal ,
                                              A486ContagemResultado_EsforcoTotal ,
                                              AV476WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV477WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ,
                                              AV214GridState.gxTpr_Dynamicfilters.Count ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod ,
                                              Gx_date ,
                                              AV285Usuario_EhGestor ,
                                              AV287WWPContext.gxTpr_Userehfinanceiro ,
                                              AV287WWPContext.gxTpr_Userehcontratante ,
                                              AV287WWPContext.gxTpr_Userid ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Ano ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Mes ,
                                              AV232SDT_FiltroConsContadorFM.gxTpr_Comerro ,
                                              A585ContagemResultado_Erros ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.SHORT
                                              }
         });
         lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV349WWContagemResultadoDS_25_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV349WWContagemResultadoDS_25_Contagemresultado_descricao1), "%", "");
         lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2), "%", "");
         lV374WWContagemResultadoDS_50_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV374WWContagemResultadoDS_50_Contagemresultado_descricao2), "%", "");
         lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3), "%", "");
         lV399WWContagemResultadoDS_75_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV399WWContagemResultadoDS_75_Contagemresultado_descricao3), "%", "");
         lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4), "%", "");
         lV424WWContagemResultadoDS_100_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV424WWContagemResultadoDS_100_Contagemresultado_descricao4), "%", "");
         lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5), "%", "");
         lV449WWContagemResultadoDS_125_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV449WWContagemResultadoDS_125_Contagemresultado_descricao5), "%", "");
         lV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes), "%", "");
         lV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm), "%", "");
         lV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao), "%", "");
         lV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
         lV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = StringUtil.PadR( StringUtil.RTrim( AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum), 20, "%");
         lV472WWContagemResultadoDS_148_Tfcontagemresultado_servico = StringUtil.PadR( StringUtil.RTrim( AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico), 15, "%");
         /* Using cursor P00T96 */
         pr_default.execute(2, new Object[] {AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1, AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1, AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1, AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1, AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1, AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1, AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1, AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1, AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1, AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2, AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2, AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2, AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2, AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2, AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2, AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2, AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2, AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2, AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2, AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3, AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3, AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3, AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3, AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3, AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3, AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3, AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3, AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3, AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3, AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4, AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4, AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4, AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4, AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4, AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4, AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4, AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4, AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4, AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4, AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5, AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5, AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5, AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5, AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5, AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5, AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5, AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5, AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5, AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5, AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt, AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt, AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to, AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to, AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels.Count, AV214GridState.gxTpr_Dynamicfilters.Count, AV9Codigos.Count, AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV232SDT_FiltroConsContadorFM.gxTpr_Sistema, AV232SDT_FiltroConsContadorFM.gxTpr_Lote, AV232SDT_FiltroConsContadorFM.gxTpr_Solicitadas, AV232SDT_FiltroConsContadorFM.gxTpr_Semliquidar, Gx_date, AV9Codigos.Count, AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV9Codigos.Count, AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV232SDT_FiltroConsContadorFM.gxTpr_Ano, AV232SDT_FiltroConsContadorFM.gxTpr_Mes, AV285Usuario_EhGestor, AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV9Codigos.Count, AV232SDT_FiltroConsContadorFM.gxTpr_Mes, AV232SDT_FiltroConsContadorFM.gxTpr_Mes, AV9Codigos.Count, AV232SDT_FiltroConsContadorFM.gxTpr_Ano, AV232SDT_FiltroConsContadorFM.gxTpr_Ano, AV9Codigos.Count, AV232SDT_FiltroConsContadorFM.gxTpr_Soconfirmadas, AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod, AV287WWPContext.gxTpr_Contratada_codigo, AV287WWPContext.gxTpr_Contratada_pessoacod, AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1, AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1, AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1,
         AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1, AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1, AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1, AV337WWContagemResultadoDS_13_Outrosstatus1, AV339WWContagemResultadoDS_15_Contagemresultado_servico1, AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1, AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1, AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1, AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1, AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1, AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1, AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1, AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1, AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1, lV349WWContagemResultadoDS_25_Contagemresultado_descricao1, AV350WWContagemResultadoDS_26_Contagemresultado_codigo1, AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2, AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2, AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2, AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2, AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2, AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2, AV362WWContagemResultadoDS_38_Outrosstatus2, AV364WWContagemResultadoDS_40_Contagemresultado_servico2, AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2, AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2, AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2, AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2, AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2, AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2, AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2, AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2, AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2, lV374WWContagemResultadoDS_50_Contagemresultado_descricao2, AV375WWContagemResultadoDS_51_Contagemresultado_codigo2, AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3, AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3, AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3, AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3, AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3, AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3, AV387WWContagemResultadoDS_63_Outrosstatus3, AV389WWContagemResultadoDS_65_Contagemresultado_servico3, AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3, AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3, AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3, AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3, AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3, AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3, AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3, AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3, AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3, lV399WWContagemResultadoDS_75_Contagemresultado_descricao3, AV400WWContagemResultadoDS_76_Contagemresultado_codigo3, AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4, AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4, AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4, AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4, AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4, AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4, AV412WWContagemResultadoDS_88_Outrosstatus4, AV414WWContagemResultadoDS_90_Contagemresultado_servico4, AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4, AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4, AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4, AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4, AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4, AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4, AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4, AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4, AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4, lV424WWContagemResultadoDS_100_Contagemresultado_descricao4, AV425WWContagemResultadoDS_101_Contagemresultado_codigo4, AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5, AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5, AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5, AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5, AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5, AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5, AV437WWContagemResultadoDS_113_Outrosstatus5, AV439WWContagemResultadoDS_115_Contagemresultado_servico5, AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5, AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5, AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5, AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5, AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5, AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5, AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5, AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5, AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5, lV449WWContagemResultadoDS_125_Contagemresultado_descricao5, AV450WWContagemResultadoDS_126_Contagemresultado_codigo5, lV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes, AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel, AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn, AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to, AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista, AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to, lV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm, AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel, lV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao, AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel, lV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla, AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel, lV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla,
         AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel, lV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum, AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel, lV472WWContagemResultadoDS_148_Tfcontagemresultado_servico, AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel, AV232SDT_FiltroConsContadorFM.gxTpr_Servico, AV232SDT_FiltroConsContadorFM.gxTpr_Sistema, AV232SDT_FiltroConsContadorFM.gxTpr_Lote});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A146Modulo_Codigo = P00T96_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00T96_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P00T96_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00T96_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00T96_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00T96_n830AreaTrabalho_ServicoPadrao[0];
            A1553ContagemResultado_CntSrvCod = P00T96_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00T96_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P00T96_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00T96_n1603ContagemResultado_CntCod[0];
            A1583ContagemResultado_TipoRegistro = P00T96_A1583ContagemResultado_TipoRegistro[0];
            A597ContagemResultado_LoteAceiteCod = P00T96_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00T96_n597ContagemResultado_LoteAceiteCod[0];
            A1043ContagemResultado_LiqLogCod = P00T96_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = P00T96_n1043ContagemResultado_LiqLogCod[0];
            A605Servico_Sigla = P00T96_A605Servico_Sigla[0];
            n605Servico_Sigla = P00T96_n605Servico_Sigla[0];
            A1612ContagemResultado_CntNum = P00T96_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00T96_n1612ContagemResultado_CntNum[0];
            A803ContagemResultado_ContratadaSigla = P00T96_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00T96_n803ContagemResultado_ContratadaSigla[0];
            A509ContagemrResultado_SistemaSigla = P00T96_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00T96_n509ContagemrResultado_SistemaSigla[0];
            A501ContagemResultado_OsFsOsFm = P00T96_A501ContagemResultado_OsFsOsFm[0];
            A53Contratada_AreaTrabalhoDes = P00T96_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00T96_n53Contratada_AreaTrabalhoDes[0];
            A494ContagemResultado_Descricao = P00T96_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00T96_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P00T96_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00T96_n1046ContagemResultado_Agrupador[0];
            A598ContagemResultado_Baseline = P00T96_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00T96_n598ContagemResultado_Baseline[0];
            A468ContagemResultado_NaoCnfDmnCod = P00T96_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00T96_n468ContagemResultado_NaoCnfDmnCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P00T96_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P00T96_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = P00T96_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00T96_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00T96_A508ContagemResultado_Owner[0];
            A1443ContagemResultado_CntSrvPrrCod = P00T96_A1443ContagemResultado_CntSrvPrrCod[0];
            n1443ContagemResultado_CntSrvPrrCod = P00T96_n1443ContagemResultado_CntSrvPrrCod[0];
            A601ContagemResultado_Servico = P00T96_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00T96_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P00T96_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00T96_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P00T96_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00T96_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00T96_A471ContagemResultado_DataDmn[0];
            A499ContagemResultado_ContratadaPessoaCod = P00T96_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00T96_n499ContagemResultado_ContratadaPessoaCod[0];
            A490ContagemResultado_ContratadaCod = P00T96_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00T96_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00T96_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00T96_n52Contratada_AreaTrabalhoCod[0];
            A684ContagemResultado_PFBFSUltima = P00T96_A684ContagemResultado_PFBFSUltima[0];
            A457ContagemResultado_Demanda = P00T96_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00T96_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00T96_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00T96_n493ContagemResultado_DemandaFM[0];
            A510ContagemResultado_EsforcoSoma = P00T96_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00T96_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00T96_A584ContagemResultado_ContadorFM[0];
            A531ContagemResultado_StatusUltCnt = P00T96_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P00T96_A566ContagemResultado_DataUltCnt[0];
            A683ContagemResultado_PFLFMUltima = P00T96_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00T96_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00T96_A685ContagemResultado_PFLFSUltima[0];
            A456ContagemResultado_Codigo = P00T96_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P00T96_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00T96_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00T96_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00T96_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P00T96_A605Servico_Sigla[0];
            n605Servico_Sigla = P00T96_n605Servico_Sigla[0];
            A1603ContagemResultado_CntCod = P00T96_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00T96_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P00T96_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00T96_n601ContagemResultado_Servico[0];
            A1612ContagemResultado_CntNum = P00T96_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P00T96_n1612ContagemResultado_CntNum[0];
            A509ContagemrResultado_SistemaSigla = P00T96_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00T96_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00T96_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00T96_n803ContagemResultado_ContratadaSigla[0];
            A499ContagemResultado_ContratadaPessoaCod = P00T96_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00T96_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00T96_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00T96_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P00T96_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00T96_n53Contratada_AreaTrabalhoDes[0];
            A684ContagemResultado_PFBFSUltima = P00T96_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P00T96_A584ContagemResultado_ContadorFM[0];
            A531ContagemResultado_StatusUltCnt = P00T96_A531ContagemResultado_StatusUltCnt[0];
            A566ContagemResultado_DataUltCnt = P00T96_A566ContagemResultado_DataUltCnt[0];
            A683ContagemResultado_PFLFMUltima = P00T96_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00T96_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00T96_A685ContagemResultado_PFLFSUltima[0];
            A510ContagemResultado_EsforcoSoma = P00T96_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00T96_n510ContagemResultado_EsforcoSoma[0];
            if ( ! ( ! ( AV285Usuario_EhGestor || AV287WWPContext.gxTpr_Userehfinanceiro || AV287WWPContext.gxTpr_Userehcontratante ) && ( AV9Codigos.Count == 0 ) && (0==AV232SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod) && (0==AV232SDT_FiltroConsContadorFM.gxTpr_Sistema) && (0==AV232SDT_FiltroConsContadorFM.gxTpr_Lote) && ! AV232SDT_FiltroConsContadorFM.gxTpr_Solicitadas && ! AV232SDT_FiltroConsContadorFM.gxTpr_Semliquidar ) || ( ( A508ContagemResultado_Owner == AV287WWPContext.gxTpr_Userid ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) || new prc_existecontagem(context).executeUdp(  A456ContagemResultado_Codigo,  AV287WWPContext.gxTpr_Userid) ) )
            {
               GXt_char1 = A486ContagemResultado_EsforcoTotal;
               new prc_contageresultado_esforcototal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_char1) ;
               A486ContagemResultado_EsforcoTotal = GXt_char1;
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal)) ) ) || ( StringUtil.Like( A486ContagemResultado_EsforcoTotal , StringUtil.PadR( AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal , 10 , "%"),  ' ' ) ) )
               {
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel)) || ( ( StringUtil.StrCmp(A486ContagemResultado_EsforcoTotal, AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel) == 0 ) ) )
                  {
                     GXt_decimal2 = A574ContagemResultado_PFFinal;
                     new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
                     A574ContagemResultado_PFFinal = GXt_decimal2;
                     if ( (Convert.ToDecimal(0)==AV476WWContagemResultadoDS_152_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV476WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ) ) )
                     {
                        if ( (Convert.ToDecimal(0)==AV477WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV477WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ) ) )
                        {
                           GXt_int3 = A585ContagemResultado_Erros;
                           new prc_pendenciasdaos(context ).execute(  A456ContagemResultado_Codigo, out  GXt_int3) ;
                           A585ContagemResultado_Erros = GXt_int3;
                           if ( ! ( ( AV9Codigos.Count == 0 ) && ( AV232SDT_FiltroConsContadorFM.gxTpr_Comerro ) ) || ( ( A585ContagemResultado_Erros > 0 ) ) )
                           {
                              AV151ContagemResultado_StatusDmnDescription = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                              AV158ContagemResultado_StatusUltCntDescription = gxdomainstatuscontagem.getDescription(context,A531ContagemResultado_StatusUltCnt);
                              if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                              {
                                 AV22ContagemResultado_BaselineDescription = "*";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                              {
                                 AV22ContagemResultado_BaselineDescription = "";
                              }
                              /* Execute user subroutine: 'BEFOREPRINTLINE' */
                              S154 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(2);
                                 returnInSub = true;
                                 if (true) return;
                              }
                              if ( (0==AV159Contratada_AreaTrabalhoCod) )
                              {
                                 HT90( false, 17) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A53Contratada_AreaTrabalhoDes, "@!")), 5, Gx_line+2, 110, Gx_line+16, 0, 0, 0, 0) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 117, Gx_line+2, 172, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 182, Gx_line+2, 237, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), 243, Gx_line+2, 298, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), 305, Gx_line+2, 360, Gx_line+17, 3, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 366, Gx_line+2, 421, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A803ContagemResultado_ContratadaSigla, "@!")), 428, Gx_line+2, 483, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV151ContagemResultado_StatusDmnDescription, "")), 489, Gx_line+2, 544, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158ContagemResultado_StatusUltCntDescription, "")), 549, Gx_line+2, 604, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22ContagemResultado_BaselineDescription, "")), 608, Gx_line+2, 663, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A601ContagemResultado_Servico), "ZZZZZ9")), 669, Gx_line+2, 724, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A684ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), 731, Gx_line+2, 786, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A685ContagemResultado_PFLFSUltima, "ZZ,ZZZ,ZZ9.999")), 792, Gx_line+2, 847, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A682ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), 852, Gx_line+2, 907, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A683ContagemResultado_PFLFMUltima, "ZZ,ZZZ,ZZ9.999")), 911, Gx_line+2, 966, Gx_line+17, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A486ContagemResultado_EsforcoTotal, "")), 972, Gx_line+2, 1027, Gx_line+17, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 1031, Gx_line+2, 1086, Gx_line+17, 2, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+17);
                              }
                              else
                              {
                                 HT90( false, 17) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 944, Gx_line+0, 999, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A486ContagemResultado_EsforcoTotal, "")), 882, Gx_line+0, 937, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A683ContagemResultado_PFLFMUltima, "ZZ,ZZZ,ZZ9.999")), 813, Gx_line+0, 868, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A682ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), 751, Gx_line+0, 806, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A685ContagemResultado_PFLFSUltima, "ZZ,ZZZ,ZZ9.999")), 690, Gx_line+0, 745, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A684ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), 628, Gx_line+0, 683, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A601ContagemResultado_Servico), "ZZZZZ9")), 567, Gx_line+0, 622, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22ContagemResultado_BaselineDescription, "")), 507, Gx_line+0, 562, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158ContagemResultado_StatusUltCntDescription, "")), 445, Gx_line+0, 500, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV151ContagemResultado_StatusDmnDescription, "")), 383, Gx_line+0, 438, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A803ContagemResultado_ContratadaSigla, "@!")), 322, Gx_line+0, 377, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 260, Gx_line+0, 315, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), 194, Gx_line+0, 249, Gx_line+15, 3, 0, 0, 0) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), 131, Gx_line+0, 186, Gx_line+15, 0, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 70, Gx_line+0, 125, Gx_line+15, 2, 0, 0, 0) ;
                                 getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 8, Gx_line+0, 63, Gx_line+15, 2, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+17);
                              }
                              /* Execute user subroutine: 'AFTERPRINTLINE' */
                              S164 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(2);
                                 returnInSub = true;
                                 if (true) return;
                              }
                              AV288Quantidade = (short)(AV288Quantidade+1);
                              AV289ContagemResultado_PFBFSTotal = (decimal)(AV289ContagemResultado_PFBFSTotal+A684ContagemResultado_PFBFSUltima);
                              AV290ContagemResultado_PFLFSTotal = (decimal)(AV290ContagemResultado_PFLFSTotal+A685ContagemResultado_PFLFSUltima);
                              AV291ContagemResultado_PFBFMTotal = (decimal)(AV291ContagemResultado_PFBFMTotal+A682ContagemResultado_PFBFMUltima);
                              AV292ContagemResultado_PFLFMTotal = (decimal)(AV292ContagemResultado_PFLFMTotal+A683ContagemResultado_PFLFMUltima);
                              AV295PFFinal = (decimal)(AV295PFFinal+A574ContagemResultado_PFFinal);
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         HT90( false, 32) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV291ContagemResultado_PFBFMTotal, "ZZ,ZZZ,ZZ9.999")), 600, Gx_line+17, 689, Gx_line+32, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV295PFFinal, "ZZ,ZZZ,ZZ9.999")), 900, Gx_line+17, 989, Gx_line+32, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV288Quantidade), "ZZ9")), 225, Gx_line+17, 245, Gx_line+32, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV289ContagemResultado_PFBFSTotal, "ZZ,ZZZ,ZZ9.999")), 300, Gx_line+17, 389, Gx_line+32, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV290ContagemResultado_PFLFSTotal, "ZZ,ZZZ,ZZ9.999")), 450, Gx_line+17, 539, Gx_line+32, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV292ContagemResultado_PFLFMTotal, "ZZ,ZZZ,ZZ9.999")), 750, Gx_line+17, 839, Gx_line+32, 2+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("PFFinal:", 858, Gx_line+17, 913, Gx_line+31, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Qtd:", 183, Gx_line+17, 216, Gx_line+31, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("PFB FS:", 258, Gx_line+17, 309, Gx_line+31, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("PFL FS:", 408, Gx_line+17, 458, Gx_line+31, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("PFB FM:", 558, Gx_line+17, 613, Gx_line+31, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("PFL FM:", 708, Gx_line+17, 763, Gx_line+31, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+32);
      }

      protected void S154( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S164( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void HT90( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV287WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV285Usuario_EhGestor = false;
         AV9Codigos = new GxSimpleCollection();
         AV286WebSession = context.GetSession();
         AV168Contratadas = new GxSimpleCollection();
         AV232SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         AV294Nome = "";
         scmdbuf = "";
         P00T92_A5AreaTrabalho_Codigo = new int[1] ;
         P00T92_A6AreaTrabalho_Descricao = new String[] {""} ;
         A6AreaTrabalho_Descricao = "";
         P00T93_A40Contratada_PessoaCod = new int[1] ;
         P00T93_A39Contratada_Codigo = new int[1] ;
         P00T93_A41Contratada_PessoaNom = new String[] {""} ;
         P00T93_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         AV214GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV215GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV178DynamicFiltersSelector1 = "";
         AV124ContagemResultado_OsFsOsFm1 = "";
         AV196FilterContagemResultado_OsFsOsFmDescription = "";
         AV123ContagemResultado_OsFsOsFm = "";
         AV65ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV60ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV190FilterContagemResultado_DataDmnDescription = "";
         AV59ContagemResultado_DataDmn = DateTime.MinValue;
         AV83ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV78ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV192FilterContagemResultado_DataUltCntDescription = "";
         AV77ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV146ContagemResultado_StatusDmn1 = "";
         AV198FilterContagemResultado_StatusDmn1ValueDescription = "";
         AV203FilterContagemResultado_StatusDmnValueDescription = "";
         AV227OutrosStatus1 = "";
         AV226OutrosStatus = "";
         AV204FilterContagemResultado_StatusUltCnt1ValueDescription = "";
         AV209FilterContagemResultado_StatusUltCntValueDescription = "";
         AV17ContagemResultado_Baseline1 = "";
         AV183FilterContagemResultado_Baseline1ValueDescription = "";
         AV188FilterContagemResultado_BaselineValueDescription = "";
         AV193FilterContagemResultado_EsforcoSomaDescription = "";
         AV11ContagemResultado_Agrupador1 = "";
         AV10ContagemResultado_Agrupador = "";
         AV93ContagemResultado_Descricao1 = "";
         AV92ContagemResultado_Descricao = "";
         AV179DynamicFiltersSelector2 = "";
         AV125ContagemResultado_OsFsOsFm2 = "";
         AV66ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV61ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV84ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV79ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV147ContagemResultado_StatusDmn2 = "";
         AV199FilterContagemResultado_StatusDmn2ValueDescription = "";
         AV228OutrosStatus2 = "";
         AV205FilterContagemResultado_StatusUltCnt2ValueDescription = "";
         AV18ContagemResultado_Baseline2 = "";
         AV184FilterContagemResultado_Baseline2ValueDescription = "";
         AV12ContagemResultado_Agrupador2 = "";
         AV94ContagemResultado_Descricao2 = "";
         AV180DynamicFiltersSelector3 = "";
         AV126ContagemResultado_OsFsOsFm3 = "";
         AV67ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV62ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV85ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV80ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV148ContagemResultado_StatusDmn3 = "";
         AV200FilterContagemResultado_StatusDmn3ValueDescription = "";
         AV229OutrosStatus3 = "";
         AV206FilterContagemResultado_StatusUltCnt3ValueDescription = "";
         AV19ContagemResultado_Baseline3 = "";
         AV185FilterContagemResultado_Baseline3ValueDescription = "";
         AV13ContagemResultado_Agrupador3 = "";
         AV95ContagemResultado_Descricao3 = "";
         AV181DynamicFiltersSelector4 = "";
         AV127ContagemResultado_OsFsOsFm4 = "";
         AV68ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV63ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV86ContagemResultado_DataUltCnt4 = DateTime.MinValue;
         AV81ContagemResultado_DataUltCnt_To4 = DateTime.MinValue;
         AV149ContagemResultado_StatusDmn4 = "";
         AV201FilterContagemResultado_StatusDmn4ValueDescription = "";
         AV230OutrosStatus4 = "";
         AV207FilterContagemResultado_StatusUltCnt4ValueDescription = "";
         AV20ContagemResultado_Baseline4 = "";
         AV186FilterContagemResultado_Baseline4ValueDescription = "";
         AV14ContagemResultado_Agrupador4 = "";
         AV96ContagemResultado_Descricao4 = "";
         AV182DynamicFiltersSelector5 = "";
         AV128ContagemResultado_OsFsOsFm5 = "";
         AV69ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV64ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV87ContagemResultado_DataUltCnt5 = DateTime.MinValue;
         AV82ContagemResultado_DataUltCnt_To5 = DateTime.MinValue;
         AV150ContagemResultado_StatusDmn5 = "";
         AV202FilterContagemResultado_StatusDmn5ValueDescription = "";
         AV231OutrosStatus5 = "";
         AV208FilterContagemResultado_StatusUltCnt5ValueDescription = "";
         AV21ContagemResultado_Baseline5 = "";
         AV187FilterContagemResultado_Baseline5ValueDescription = "";
         AV15ContagemResultado_Agrupador5 = "";
         AV97ContagemResultado_Descricao5 = "";
         AV270TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV268TFContagemResultado_StatusDmn_Sel = "";
         AV269TFContagemResultado_StatusDmn_SelDscs = "";
         AV212FilterTFContagemResultado_StatusDmn_SelValueDescription = "";
         AV274TFContagemResultado_StatusUltCnt_Sels = new GxSimpleCollection();
         AV273TFContagemResultado_StatusUltCnt_SelDscs = "";
         AV213FilterTFContagemResultado_StatusUltCnt_SelValueDescription = "";
         AV211FilterTFContagemResultado_Baseline_SelValueDescription = "";
         AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 = "";
         AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = "";
         AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV302ContagemResultado_DataPrevista1 = (DateTime)(DateTime.MinValue);
         AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV297ContagemResultado_DataPrevista_To1 = (DateTime)(DateTime.MinValue);
         AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1 = "";
         AV337WWContagemResultadoDS_13_Outrosstatus1 = "";
         AV346WWContagemResultadoDS_22_Contagemresultado_baseline1 = "";
         AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1 = "";
         AV349WWContagemResultadoDS_25_Contagemresultado_descricao1 = "";
         AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 = "";
         AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = "";
         AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV303ContagemResultado_DataPrevista2 = (DateTime)(DateTime.MinValue);
         AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV298ContagemResultado_DataPrevista_To2 = (DateTime)(DateTime.MinValue);
         AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2 = "";
         AV362WWContagemResultadoDS_38_Outrosstatus2 = "";
         AV371WWContagemResultadoDS_47_Contagemresultado_baseline2 = "";
         AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2 = "";
         AV374WWContagemResultadoDS_50_Contagemresultado_descricao2 = "";
         AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 = "";
         AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = "";
         AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV304ContagemResultado_DataPrevista3 = (DateTime)(DateTime.MinValue);
         AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV299ContagemResultado_DataPrevista_To3 = (DateTime)(DateTime.MinValue);
         AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3 = "";
         AV387WWContagemResultadoDS_63_Outrosstatus3 = "";
         AV396WWContagemResultadoDS_72_Contagemresultado_baseline3 = "";
         AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3 = "";
         AV399WWContagemResultadoDS_75_Contagemresultado_descricao3 = "";
         AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 = "";
         AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = "";
         AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 = DateTime.MinValue;
         AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 = DateTime.MinValue;
         AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV305ContagemResultado_DataPrevista4 = (DateTime)(DateTime.MinValue);
         AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV300ContagemResultado_DataPrevista_To4 = (DateTime)(DateTime.MinValue);
         AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4 = "";
         AV412WWContagemResultadoDS_88_Outrosstatus4 = "";
         AV421WWContagemResultadoDS_97_Contagemresultado_baseline4 = "";
         AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4 = "";
         AV424WWContagemResultadoDS_100_Contagemresultado_descricao4 = "";
         AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 = "";
         AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = "";
         AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 = DateTime.MinValue;
         AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 = DateTime.MinValue;
         AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV306ContagemResultado_DataPrevista5 = (DateTime)(DateTime.MinValue);
         AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV301ContagemResultado_DataPrevista_To5 = (DateTime)(DateTime.MinValue);
         AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5 = "";
         AV437WWContagemResultadoDS_113_Outrosstatus5 = "";
         AV446WWContagemResultadoDS_122_Contagemresultado_baseline5 = "";
         AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5 = "";
         AV449WWContagemResultadoDS_125_Contagemresultado_descricao5 = "";
         AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = "";
         AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel = "";
         AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         AV296TFContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to = (DateTime)(DateTime.MinValue);
         AV308TFContagemResultado_DataPrevista_To = (DateTime)(DateTime.MinValue);
         AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = "";
         AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel = "";
         AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao = "";
         AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel = "";
         AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = "";
         AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel = "";
         AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = "";
         AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel = "";
         AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = "";
         AV309TFContagemResultado_CntNum = "";
         AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel = "";
         AV310TFContagemResultado_CntNum_Sel = "";
         AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels = new GxSimpleCollection();
         AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico = "";
         AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal = "";
         AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel = "";
         lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 = "";
         lV349WWContagemResultadoDS_25_Contagemresultado_descricao1 = "";
         lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 = "";
         lV374WWContagemResultadoDS_50_Contagemresultado_descricao2 = "";
         lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 = "";
         lV399WWContagemResultadoDS_75_Contagemresultado_descricao3 = "";
         lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 = "";
         lV424WWContagemResultadoDS_100_Contagemresultado_descricao4 = "";
         lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 = "";
         lV449WWContagemResultadoDS_125_Contagemresultado_descricao5 = "";
         lV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes = "";
         lV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm = "";
         lV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao = "";
         lV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla = "";
         lV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla = "";
         lV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum = "";
         lV472WWContagemResultadoDS_148_Tfcontagemresultado_servico = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A53Contratada_AreaTrabalhoDes = "";
         A509ContagemrResultado_SistemaSigla = "";
         A803ContagemResultado_ContratadaSigla = "";
         A1612ContagemResultado_CntNum = "";
         A605Servico_Sigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A486ContagemResultado_EsforcoTotal = "";
         Gx_date = DateTime.MinValue;
         P00T96_A146Modulo_Codigo = new int[1] ;
         P00T96_n146Modulo_Codigo = new bool[] {false} ;
         P00T96_A127Sistema_Codigo = new int[1] ;
         P00T96_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00T96_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00T96_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00T96_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00T96_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00T96_A1603ContagemResultado_CntCod = new int[1] ;
         P00T96_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00T96_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00T96_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00T96_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00T96_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P00T96_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P00T96_A605Servico_Sigla = new String[] {""} ;
         P00T96_n605Servico_Sigla = new bool[] {false} ;
         P00T96_A1612ContagemResultado_CntNum = new String[] {""} ;
         P00T96_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P00T96_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00T96_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00T96_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00T96_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00T96_A501ContagemResultado_OsFsOsFm = new String[] {""} ;
         P00T96_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00T96_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00T96_A494ContagemResultado_Descricao = new String[] {""} ;
         P00T96_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00T96_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00T96_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00T96_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00T96_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00T96_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00T96_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00T96_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00T96_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00T96_A489ContagemResultado_SistemaCod = new int[1] ;
         P00T96_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00T96_A508ContagemResultado_Owner = new int[1] ;
         P00T96_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         P00T96_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         P00T96_A601ContagemResultado_Servico = new int[1] ;
         P00T96_n601ContagemResultado_Servico = new bool[] {false} ;
         P00T96_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00T96_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00T96_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00T96_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00T96_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00T96_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00T96_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00T96_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00T96_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00T96_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00T96_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00T96_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00T96_A457ContagemResultado_Demanda = new String[] {""} ;
         P00T96_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00T96_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00T96_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00T96_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00T96_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00T96_A584ContagemResultado_ContadorFM = new int[1] ;
         P00T96_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00T96_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00T96_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00T96_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00T96_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00T96_A456ContagemResultado_Codigo = new int[1] ;
         A501ContagemResultado_OsFsOsFm = "";
         GXt_char1 = "";
         AV151ContagemResultado_StatusDmnDescription = "";
         AV158ContagemResultado_StatusUltCntDescription = "";
         AV22ContagemResultado_BaselineDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aexportreportwwcontagemresultadocstm__default(),
            new Object[][] {
                new Object[] {
               P00T92_A5AreaTrabalho_Codigo, P00T92_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00T93_A40Contratada_PessoaCod, P00T93_A39Contratada_Codigo, P00T93_A41Contratada_PessoaNom, P00T93_n41Contratada_PessoaNom
               }
               , new Object[] {
               P00T96_A146Modulo_Codigo, P00T96_n146Modulo_Codigo, P00T96_A127Sistema_Codigo, P00T96_A135Sistema_AreaTrabalhoCod, P00T96_A830AreaTrabalho_ServicoPadrao, P00T96_n830AreaTrabalho_ServicoPadrao, P00T96_A1553ContagemResultado_CntSrvCod, P00T96_n1553ContagemResultado_CntSrvCod, P00T96_A1603ContagemResultado_CntCod, P00T96_n1603ContagemResultado_CntCod,
               P00T96_A1583ContagemResultado_TipoRegistro, P00T96_A597ContagemResultado_LoteAceiteCod, P00T96_n597ContagemResultado_LoteAceiteCod, P00T96_A1043ContagemResultado_LiqLogCod, P00T96_n1043ContagemResultado_LiqLogCod, P00T96_A605Servico_Sigla, P00T96_n605Servico_Sigla, P00T96_A1612ContagemResultado_CntNum, P00T96_n1612ContagemResultado_CntNum, P00T96_A803ContagemResultado_ContratadaSigla,
               P00T96_n803ContagemResultado_ContratadaSigla, P00T96_A509ContagemrResultado_SistemaSigla, P00T96_n509ContagemrResultado_SistemaSigla, P00T96_A501ContagemResultado_OsFsOsFm, P00T96_A53Contratada_AreaTrabalhoDes, P00T96_n53Contratada_AreaTrabalhoDes, P00T96_A494ContagemResultado_Descricao, P00T96_n494ContagemResultado_Descricao, P00T96_A1046ContagemResultado_Agrupador, P00T96_n1046ContagemResultado_Agrupador,
               P00T96_A598ContagemResultado_Baseline, P00T96_n598ContagemResultado_Baseline, P00T96_A468ContagemResultado_NaoCnfDmnCod, P00T96_n468ContagemResultado_NaoCnfDmnCod, P00T96_A805ContagemResultado_ContratadaOrigemCod, P00T96_n805ContagemResultado_ContratadaOrigemCod, P00T96_A489ContagemResultado_SistemaCod, P00T96_n489ContagemResultado_SistemaCod, P00T96_A508ContagemResultado_Owner, P00T96_A1443ContagemResultado_CntSrvPrrCod,
               P00T96_n1443ContagemResultado_CntSrvPrrCod, P00T96_A601ContagemResultado_Servico, P00T96_n601ContagemResultado_Servico, P00T96_A484ContagemResultado_StatusDmn, P00T96_n484ContagemResultado_StatusDmn, P00T96_A1351ContagemResultado_DataPrevista, P00T96_n1351ContagemResultado_DataPrevista, P00T96_A471ContagemResultado_DataDmn, P00T96_A499ContagemResultado_ContratadaPessoaCod, P00T96_n499ContagemResultado_ContratadaPessoaCod,
               P00T96_A490ContagemResultado_ContratadaCod, P00T96_n490ContagemResultado_ContratadaCod, P00T96_A52Contratada_AreaTrabalhoCod, P00T96_n52Contratada_AreaTrabalhoCod, P00T96_A684ContagemResultado_PFBFSUltima, P00T96_A457ContagemResultado_Demanda, P00T96_n457ContagemResultado_Demanda, P00T96_A493ContagemResultado_DemandaFM, P00T96_n493ContagemResultado_DemandaFM, P00T96_A510ContagemResultado_EsforcoSoma,
               P00T96_n510ContagemResultado_EsforcoSoma, P00T96_A584ContagemResultado_ContadorFM, P00T96_A531ContagemResultado_StatusUltCnt, P00T96_A566ContagemResultado_DataUltCnt, P00T96_A683ContagemResultado_PFLFMUltima, P00T96_A682ContagemResultado_PFBFMUltima, P00T96_A685ContagemResultado_PFLFSUltima, P00T96_A456ContagemResultado_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV242TFContagemResultado_Baseline_Sel ;
      private short AV224OrderedBy ;
      private short GxWebError ;
      private short AV173DynamicFiltersOperator1 ;
      private short AV153ContagemResultado_StatusUltCnt1 ;
      private short AV174DynamicFiltersOperator2 ;
      private short AV154ContagemResultado_StatusUltCnt2 ;
      private short AV175DynamicFiltersOperator3 ;
      private short AV155ContagemResultado_StatusUltCnt3 ;
      private short AV176DynamicFiltersOperator4 ;
      private short AV156ContagemResultado_StatusUltCnt4 ;
      private short AV177DynamicFiltersOperator5 ;
      private short AV157ContagemResultado_StatusUltCnt5 ;
      private short AV272TFContagemResultado_StatusUltCnt_Sel ;
      private short AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 ;
      private short AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 ;
      private short AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 ;
      private short AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 ;
      private short AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 ;
      private short AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 ;
      private short AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 ;
      private short AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 ;
      private short AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 ;
      private short AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 ;
      private short AV471WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel ;
      private short AV232SDT_FiltroConsContadorFM_gxTpr_Ano ;
      private short AV287WWPContext_gxTpr_Userid ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A585ContagemResultado_Erros ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short GXt_int3 ;
      private short AV288Quantidade ;
      private int AV159Contratada_AreaTrabalhoCod ;
      private int AV160Contratada_Codigo ;
      private int AV266TFContagemResultado_Servico_Sel ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV311AreaTrabalho_Codigo ;
      private int Gx_OldLine ;
      private int A5AreaTrabalho_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int AV130ContagemResultado_Servico1 ;
      private int AV129ContagemResultado_Servico ;
      private int AV30ContagemResultado_ContadorFM1 ;
      private int AV29ContagemResultado_ContadorFM ;
      private int AV136ContagemResultado_SistemaCod1 ;
      private int AV135ContagemResultado_SistemaCod ;
      private int AV41ContagemResultado_ContratadaCod1 ;
      private int AV40ContagemResultado_ContratadaCod ;
      private int AV47ContagemResultado_ContratadaOrigemCod1 ;
      private int AV46ContagemResultado_ContratadaOrigemCod ;
      private int AV114ContagemResultado_NaoCnfDmnCod1 ;
      private int AV113ContagemResultado_NaoCnfDmnCod ;
      private int AV100ContagemResultado_EsforcoSoma1 ;
      private int AV99ContagemResultado_EsforcoSoma ;
      private int AV24ContagemResultado_Codigo1 ;
      private int AV23ContagemResultado_Codigo ;
      private int AV131ContagemResultado_Servico2 ;
      private int AV31ContagemResultado_ContadorFM2 ;
      private int AV137ContagemResultado_SistemaCod2 ;
      private int AV42ContagemResultado_ContratadaCod2 ;
      private int AV48ContagemResultado_ContratadaOrigemCod2 ;
      private int AV115ContagemResultado_NaoCnfDmnCod2 ;
      private int AV101ContagemResultado_EsforcoSoma2 ;
      private int AV25ContagemResultado_Codigo2 ;
      private int AV132ContagemResultado_Servico3 ;
      private int AV32ContagemResultado_ContadorFM3 ;
      private int AV138ContagemResultado_SistemaCod3 ;
      private int AV43ContagemResultado_ContratadaCod3 ;
      private int AV49ContagemResultado_ContratadaOrigemCod3 ;
      private int AV116ContagemResultado_NaoCnfDmnCod3 ;
      private int AV102ContagemResultado_EsforcoSoma3 ;
      private int AV26ContagemResultado_Codigo3 ;
      private int AV133ContagemResultado_Servico4 ;
      private int AV33ContagemResultado_ContadorFM4 ;
      private int AV139ContagemResultado_SistemaCod4 ;
      private int AV44ContagemResultado_ContratadaCod4 ;
      private int AV50ContagemResultado_ContratadaOrigemCod4 ;
      private int AV117ContagemResultado_NaoCnfDmnCod4 ;
      private int AV103ContagemResultado_EsforcoSoma4 ;
      private int AV27ContagemResultado_Codigo4 ;
      private int AV134ContagemResultado_Servico5 ;
      private int AV34ContagemResultado_ContadorFM5 ;
      private int AV140ContagemResultado_SistemaCod5 ;
      private int AV45ContagemResultado_ContratadaCod5 ;
      private int AV51ContagemResultado_ContratadaOrigemCod5 ;
      private int AV118ContagemResultado_NaoCnfDmnCod5 ;
      private int AV104ContagemResultado_EsforcoSoma5 ;
      private int AV28ContagemResultado_Codigo5 ;
      private int AV322GXV1 ;
      private int AV323GXV2 ;
      private int AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod ;
      private int AV326WWContagemResultadoDS_2_Contratada_codigo ;
      private int AV339WWContagemResultadoDS_15_Contagemresultado_servico1 ;
      private int AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 ;
      private int AV312ContagemResultado_CntSrvPrrCod1 ;
      private int AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1 ;
      private int AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1 ;
      private int AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1 ;
      private int AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 ;
      private int AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 ;
      private int AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 ;
      private int AV350WWContagemResultadoDS_26_Contagemresultado_codigo1 ;
      private int AV364WWContagemResultadoDS_40_Contagemresultado_servico2 ;
      private int AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 ;
      private int AV313ContagemResultado_CntSrvPrrCod2 ;
      private int AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2 ;
      private int AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2 ;
      private int AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2 ;
      private int AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 ;
      private int AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 ;
      private int AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 ;
      private int AV375WWContagemResultadoDS_51_Contagemresultado_codigo2 ;
      private int AV389WWContagemResultadoDS_65_Contagemresultado_servico3 ;
      private int AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 ;
      private int AV314ContagemResultado_CntSrvPrrCod3 ;
      private int AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3 ;
      private int AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3 ;
      private int AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3 ;
      private int AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 ;
      private int AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 ;
      private int AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 ;
      private int AV400WWContagemResultadoDS_76_Contagemresultado_codigo3 ;
      private int AV414WWContagemResultadoDS_90_Contagemresultado_servico4 ;
      private int AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 ;
      private int AV315ContagemResultado_CntSrvPrrCod4 ;
      private int AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4 ;
      private int AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4 ;
      private int AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4 ;
      private int AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 ;
      private int AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 ;
      private int AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 ;
      private int AV425WWContagemResultadoDS_101_Contagemresultado_codigo4 ;
      private int AV439WWContagemResultadoDS_115_Contagemresultado_servico5 ;
      private int AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 ;
      private int AV317ContagemResultado_CntSrvPrrCod5 ;
      private int AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5 ;
      private int AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5 ;
      private int AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5 ;
      private int AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 ;
      private int AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 ;
      private int AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 ;
      private int AV450WWContagemResultadoDS_126_Contagemresultado_codigo5 ;
      private int AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel ;
      private int AV287WWPContext_gxTpr_Contratada_codigo ;
      private int AV287WWPContext_gxTpr_Contratada_pessoacod ;
      private int AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV9Codigos_Count ;
      private int AV168Contratadas_Count ;
      private int AV232SDT_FiltroConsContadorFM_gxTpr_Servico ;
      private int AV232SDT_FiltroConsContadorFM_gxTpr_Sistema ;
      private int AV232SDT_FiltroConsContadorFM_gxTpr_Lote ;
      private int AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels_Count ;
      private int AV214GridState_gxTpr_Dynamicfilters_Count ;
      private int AV232SDT_FiltroConsContadorFM_gxTpr_Contagemresultado_contadorfmcod ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A510ContagemResultado_EsforcoSoma ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private long AV218i ;
      private long AV232SDT_FiltroConsContadorFM_gxTpr_Mes ;
      private decimal AV257TFContagemResultado_PFBFSUltima ;
      private decimal AV258TFContagemResultado_PFBFSUltima_To ;
      private decimal AV263TFContagemResultado_PFLFSUltima ;
      private decimal AV264TFContagemResultado_PFLFSUltima_To ;
      private decimal AV255TFContagemResultado_PFBFMUltima ;
      private decimal AV256TFContagemResultado_PFBFMUltima_To ;
      private decimal AV261TFContagemResultado_PFLFMUltima ;
      private decimal AV262TFContagemResultado_PFLFMUltima_To ;
      private decimal AV259TFContagemResultado_PFFinal ;
      private decimal AV260TFContagemResultado_PFFinal_To ;
      private decimal AV476WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ;
      private decimal AV477WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal GXt_decimal2 ;
      private decimal AV289ContagemResultado_PFBFSTotal ;
      private decimal AV290ContagemResultado_PFLFSTotal ;
      private decimal AV291ContagemResultado_PFBFMTotal ;
      private decimal AV292ContagemResultado_PFLFMTotal ;
      private decimal AV295PFFinal ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV276TFContagemrResultado_SistemaSigla ;
      private String AV277TFContagemrResultado_SistemaSigla_Sel ;
      private String AV243TFContagemResultado_ContratadaSigla ;
      private String AV244TFContagemResultado_ContratadaSigla_Sel ;
      private String AV265TFContagemResultado_Servico ;
      private String AV251TFContagemResultado_EsforcoTotal ;
      private String AV252TFContagemResultado_EsforcoTotal_Sel ;
      private String AV294Nome ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String AV146ContagemResultado_StatusDmn1 ;
      private String AV227OutrosStatus1 ;
      private String AV226OutrosStatus ;
      private String AV17ContagemResultado_Baseline1 ;
      private String AV11ContagemResultado_Agrupador1 ;
      private String AV10ContagemResultado_Agrupador ;
      private String AV147ContagemResultado_StatusDmn2 ;
      private String AV228OutrosStatus2 ;
      private String AV18ContagemResultado_Baseline2 ;
      private String AV12ContagemResultado_Agrupador2 ;
      private String AV148ContagemResultado_StatusDmn3 ;
      private String AV229OutrosStatus3 ;
      private String AV19ContagemResultado_Baseline3 ;
      private String AV13ContagemResultado_Agrupador3 ;
      private String AV149ContagemResultado_StatusDmn4 ;
      private String AV230OutrosStatus4 ;
      private String AV20ContagemResultado_Baseline4 ;
      private String AV14ContagemResultado_Agrupador4 ;
      private String AV150ContagemResultado_StatusDmn5 ;
      private String AV231OutrosStatus5 ;
      private String AV21ContagemResultado_Baseline5 ;
      private String AV15ContagemResultado_Agrupador5 ;
      private String AV268TFContagemResultado_StatusDmn_Sel ;
      private String AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1 ;
      private String AV337WWContagemResultadoDS_13_Outrosstatus1 ;
      private String AV346WWContagemResultadoDS_22_Contagemresultado_baseline1 ;
      private String AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1 ;
      private String AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2 ;
      private String AV362WWContagemResultadoDS_38_Outrosstatus2 ;
      private String AV371WWContagemResultadoDS_47_Contagemresultado_baseline2 ;
      private String AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2 ;
      private String AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3 ;
      private String AV387WWContagemResultadoDS_63_Outrosstatus3 ;
      private String AV396WWContagemResultadoDS_72_Contagemresultado_baseline3 ;
      private String AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3 ;
      private String AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4 ;
      private String AV412WWContagemResultadoDS_88_Outrosstatus4 ;
      private String AV421WWContagemResultadoDS_97_Contagemresultado_baseline4 ;
      private String AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4 ;
      private String AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5 ;
      private String AV437WWContagemResultadoDS_113_Outrosstatus5 ;
      private String AV446WWContagemResultadoDS_122_Contagemresultado_baseline5 ;
      private String AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5 ;
      private String AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ;
      private String AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ;
      private String AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel ;
      private String AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ;
      private String AV309TFContagemResultado_CntNum ;
      private String AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel ;
      private String AV310TFContagemResultado_CntNum_Sel ;
      private String AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico ;
      private String AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal ;
      private String AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel ;
      private String lV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ;
      private String lV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ;
      private String lV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ;
      private String lV472WWContagemResultadoDS_148_Tfcontagemresultado_servico ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A1612ContagemResultado_CntNum ;
      private String A605Servico_Sigla ;
      private String A486ContagemResultado_EsforcoTotal ;
      private String GXt_char1 ;
      private DateTime AV302ContagemResultado_DataPrevista1 ;
      private DateTime AV297ContagemResultado_DataPrevista_To1 ;
      private DateTime AV303ContagemResultado_DataPrevista2 ;
      private DateTime AV298ContagemResultado_DataPrevista_To2 ;
      private DateTime AV304ContagemResultado_DataPrevista3 ;
      private DateTime AV299ContagemResultado_DataPrevista_To3 ;
      private DateTime AV305ContagemResultado_DataPrevista4 ;
      private DateTime AV300ContagemResultado_DataPrevista_To4 ;
      private DateTime AV306ContagemResultado_DataPrevista5 ;
      private DateTime AV301ContagemResultado_DataPrevista_To5 ;
      private DateTime AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista ;
      private DateTime AV296TFContagemResultado_DataPrevista ;
      private DateTime AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to ;
      private DateTime AV308TFContagemResultado_DataPrevista_To ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV245TFContagemResultado_DataDmn ;
      private DateTime AV246TFContagemResultado_DataDmn_To ;
      private DateTime AV247TFContagemResultado_DataUltCnt ;
      private DateTime AV248TFContagemResultado_DataUltCnt_To ;
      private DateTime AV65ContagemResultado_DataDmn1 ;
      private DateTime AV60ContagemResultado_DataDmn_To1 ;
      private DateTime AV59ContagemResultado_DataDmn ;
      private DateTime AV83ContagemResultado_DataUltCnt1 ;
      private DateTime AV78ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV77ContagemResultado_DataUltCnt ;
      private DateTime AV66ContagemResultado_DataDmn2 ;
      private DateTime AV61ContagemResultado_DataDmn_To2 ;
      private DateTime AV84ContagemResultado_DataUltCnt2 ;
      private DateTime AV79ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV67ContagemResultado_DataDmn3 ;
      private DateTime AV62ContagemResultado_DataDmn_To3 ;
      private DateTime AV85ContagemResultado_DataUltCnt3 ;
      private DateTime AV80ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV68ContagemResultado_DataDmn4 ;
      private DateTime AV63ContagemResultado_DataDmn_To4 ;
      private DateTime AV86ContagemResultado_DataUltCnt4 ;
      private DateTime AV81ContagemResultado_DataUltCnt_To4 ;
      private DateTime AV69ContagemResultado_DataDmn5 ;
      private DateTime AV64ContagemResultado_DataDmn_To5 ;
      private DateTime AV87ContagemResultado_DataUltCnt5 ;
      private DateTime AV82ContagemResultado_DataUltCnt_To5 ;
      private DateTime AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 ;
      private DateTime AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1 ;
      private DateTime AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 ;
      private DateTime AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2 ;
      private DateTime AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 ;
      private DateTime AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 ;
      private DateTime AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2 ;
      private DateTime AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 ;
      private DateTime AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3 ;
      private DateTime AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 ;
      private DateTime AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 ;
      private DateTime AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3 ;
      private DateTime AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 ;
      private DateTime AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4 ;
      private DateTime AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 ;
      private DateTime AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 ;
      private DateTime AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 ;
      private DateTime AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4 ;
      private DateTime AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 ;
      private DateTime AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5 ;
      private DateTime AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 ;
      private DateTime AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 ;
      private DateTime AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 ;
      private DateTime AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5 ;
      private DateTime AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 ;
      private DateTime AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn ;
      private DateTime AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to ;
      private DateTime AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt ;
      private DateTime AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV225OrderedDsc ;
      private bool AV285Usuario_EhGestor ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool AV169DynamicFiltersEnabled2 ;
      private bool AV170DynamicFiltersEnabled3 ;
      private bool AV171DynamicFiltersEnabled4 ;
      private bool AV172DynamicFiltersEnabled5 ;
      private bool AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 ;
      private bool AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 ;
      private bool AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 ;
      private bool AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 ;
      private bool AV232SDT_FiltroConsContadorFM_gxTpr_Abertas ;
      private bool AV232SDT_FiltroConsContadorFM_gxTpr_Solicitadas ;
      private bool AV232SDT_FiltroConsContadorFM_gxTpr_Soconfirmadas ;
      private bool AV232SDT_FiltroConsContadorFM_gxTpr_Semliquidar ;
      private bool AV232SDT_FiltroConsContadorFM_gxTpr_Comerro ;
      private bool AV287WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV287WWPContext_gxTpr_Userehcontratante ;
      private bool A598ContagemResultado_Baseline ;
      private bool n146Modulo_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n605Servico_Sigla ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n598ContagemResultado_Baseline ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n510ContagemResultado_EsforcoSoma ;
      private String AV271TFContagemResultado_StatusDmn_SelsJson ;
      private String AV275TFContagemResultado_StatusUltCnt_SelsJson ;
      private String AV217GridStateXML ;
      private String AV278TFContratada_AreaTrabalhoDes ;
      private String AV279TFContratada_AreaTrabalhoDes_Sel ;
      private String AV253TFContagemResultado_OsFsOsFm ;
      private String AV254TFContagemResultado_OsFsOsFm_Sel ;
      private String AV249TFContagemResultado_Descricao ;
      private String AV250TFContagemResultado_Descricao_Sel ;
      private String AV267TFContagemResultado_Servico_SelDsc ;
      private String A6AreaTrabalho_Descricao ;
      private String AV178DynamicFiltersSelector1 ;
      private String AV124ContagemResultado_OsFsOsFm1 ;
      private String AV196FilterContagemResultado_OsFsOsFmDescription ;
      private String AV123ContagemResultado_OsFsOsFm ;
      private String AV190FilterContagemResultado_DataDmnDescription ;
      private String AV192FilterContagemResultado_DataUltCntDescription ;
      private String AV198FilterContagemResultado_StatusDmn1ValueDescription ;
      private String AV203FilterContagemResultado_StatusDmnValueDescription ;
      private String AV204FilterContagemResultado_StatusUltCnt1ValueDescription ;
      private String AV209FilterContagemResultado_StatusUltCntValueDescription ;
      private String AV183FilterContagemResultado_Baseline1ValueDescription ;
      private String AV188FilterContagemResultado_BaselineValueDescription ;
      private String AV193FilterContagemResultado_EsforcoSomaDescription ;
      private String AV93ContagemResultado_Descricao1 ;
      private String AV92ContagemResultado_Descricao ;
      private String AV179DynamicFiltersSelector2 ;
      private String AV125ContagemResultado_OsFsOsFm2 ;
      private String AV199FilterContagemResultado_StatusDmn2ValueDescription ;
      private String AV205FilterContagemResultado_StatusUltCnt2ValueDescription ;
      private String AV184FilterContagemResultado_Baseline2ValueDescription ;
      private String AV94ContagemResultado_Descricao2 ;
      private String AV180DynamicFiltersSelector3 ;
      private String AV126ContagemResultado_OsFsOsFm3 ;
      private String AV200FilterContagemResultado_StatusDmn3ValueDescription ;
      private String AV206FilterContagemResultado_StatusUltCnt3ValueDescription ;
      private String AV185FilterContagemResultado_Baseline3ValueDescription ;
      private String AV95ContagemResultado_Descricao3 ;
      private String AV181DynamicFiltersSelector4 ;
      private String AV127ContagemResultado_OsFsOsFm4 ;
      private String AV201FilterContagemResultado_StatusDmn4ValueDescription ;
      private String AV207FilterContagemResultado_StatusUltCnt4ValueDescription ;
      private String AV186FilterContagemResultado_Baseline4ValueDescription ;
      private String AV96ContagemResultado_Descricao4 ;
      private String AV182DynamicFiltersSelector5 ;
      private String AV128ContagemResultado_OsFsOsFm5 ;
      private String AV202FilterContagemResultado_StatusDmn5ValueDescription ;
      private String AV208FilterContagemResultado_StatusUltCnt5ValueDescription ;
      private String AV187FilterContagemResultado_Baseline5ValueDescription ;
      private String AV97ContagemResultado_Descricao5 ;
      private String AV269TFContagemResultado_StatusDmn_SelDscs ;
      private String AV212FilterTFContagemResultado_StatusDmn_SelValueDescription ;
      private String AV273TFContagemResultado_StatusUltCnt_SelDscs ;
      private String AV213FilterTFContagemResultado_StatusUltCnt_SelValueDescription ;
      private String AV211FilterTFContagemResultado_Baseline_SelValueDescription ;
      private String AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 ;
      private String AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ;
      private String AV349WWContagemResultadoDS_25_Contagemresultado_descricao1 ;
      private String AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 ;
      private String AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ;
      private String AV374WWContagemResultadoDS_50_Contagemresultado_descricao2 ;
      private String AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 ;
      private String AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ;
      private String AV399WWContagemResultadoDS_75_Contagemresultado_descricao3 ;
      private String AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 ;
      private String AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ;
      private String AV424WWContagemResultadoDS_100_Contagemresultado_descricao4 ;
      private String AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 ;
      private String AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ;
      private String AV449WWContagemResultadoDS_125_Contagemresultado_descricao5 ;
      private String AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ;
      private String AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel ;
      private String AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ;
      private String AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel ;
      private String AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao ;
      private String AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel ;
      private String lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ;
      private String lV349WWContagemResultadoDS_25_Contagemresultado_descricao1 ;
      private String lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ;
      private String lV374WWContagemResultadoDS_50_Contagemresultado_descricao2 ;
      private String lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ;
      private String lV399WWContagemResultadoDS_75_Contagemresultado_descricao3 ;
      private String lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ;
      private String lV424WWContagemResultadoDS_100_Contagemresultado_descricao4 ;
      private String lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ;
      private String lV449WWContagemResultadoDS_125_Contagemresultado_descricao5 ;
      private String lV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ;
      private String lV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ;
      private String lV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV151ContagemResultado_StatusDmnDescription ;
      private String AV158ContagemResultado_StatusUltCntDescription ;
      private String AV22ContagemResultado_BaselineDescription ;
      private IGxSession AV286WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00T92_A5AreaTrabalho_Codigo ;
      private String[] P00T92_A6AreaTrabalho_Descricao ;
      private int[] P00T93_A40Contratada_PessoaCod ;
      private int[] P00T93_A39Contratada_Codigo ;
      private String[] P00T93_A41Contratada_PessoaNom ;
      private bool[] P00T93_n41Contratada_PessoaNom ;
      private int[] P00T96_A146Modulo_Codigo ;
      private bool[] P00T96_n146Modulo_Codigo ;
      private int[] P00T96_A127Sistema_Codigo ;
      private int[] P00T96_A135Sistema_AreaTrabalhoCod ;
      private int[] P00T96_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00T96_n830AreaTrabalho_ServicoPadrao ;
      private int[] P00T96_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00T96_n1553ContagemResultado_CntSrvCod ;
      private int[] P00T96_A1603ContagemResultado_CntCod ;
      private bool[] P00T96_n1603ContagemResultado_CntCod ;
      private short[] P00T96_A1583ContagemResultado_TipoRegistro ;
      private int[] P00T96_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00T96_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00T96_A1043ContagemResultado_LiqLogCod ;
      private bool[] P00T96_n1043ContagemResultado_LiqLogCod ;
      private String[] P00T96_A605Servico_Sigla ;
      private bool[] P00T96_n605Servico_Sigla ;
      private String[] P00T96_A1612ContagemResultado_CntNum ;
      private bool[] P00T96_n1612ContagemResultado_CntNum ;
      private String[] P00T96_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00T96_n803ContagemResultado_ContratadaSigla ;
      private String[] P00T96_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00T96_n509ContagemrResultado_SistemaSigla ;
      private String[] P00T96_A501ContagemResultado_OsFsOsFm ;
      private String[] P00T96_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00T96_n53Contratada_AreaTrabalhoDes ;
      private String[] P00T96_A494ContagemResultado_Descricao ;
      private bool[] P00T96_n494ContagemResultado_Descricao ;
      private String[] P00T96_A1046ContagemResultado_Agrupador ;
      private bool[] P00T96_n1046ContagemResultado_Agrupador ;
      private bool[] P00T96_A598ContagemResultado_Baseline ;
      private bool[] P00T96_n598ContagemResultado_Baseline ;
      private int[] P00T96_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00T96_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00T96_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00T96_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P00T96_A489ContagemResultado_SistemaCod ;
      private bool[] P00T96_n489ContagemResultado_SistemaCod ;
      private int[] P00T96_A508ContagemResultado_Owner ;
      private int[] P00T96_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] P00T96_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] P00T96_A601ContagemResultado_Servico ;
      private bool[] P00T96_n601ContagemResultado_Servico ;
      private String[] P00T96_A484ContagemResultado_StatusDmn ;
      private bool[] P00T96_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00T96_A1351ContagemResultado_DataPrevista ;
      private bool[] P00T96_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00T96_A471ContagemResultado_DataDmn ;
      private int[] P00T96_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00T96_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00T96_A490ContagemResultado_ContratadaCod ;
      private bool[] P00T96_n490ContagemResultado_ContratadaCod ;
      private int[] P00T96_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00T96_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00T96_A684ContagemResultado_PFBFSUltima ;
      private String[] P00T96_A457ContagemResultado_Demanda ;
      private bool[] P00T96_n457ContagemResultado_Demanda ;
      private String[] P00T96_A493ContagemResultado_DemandaFM ;
      private bool[] P00T96_n493ContagemResultado_DemandaFM ;
      private int[] P00T96_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00T96_n510ContagemResultado_EsforcoSoma ;
      private int[] P00T96_A584ContagemResultado_ContadorFM ;
      private short[] P00T96_A531ContagemResultado_StatusUltCnt ;
      private DateTime[] P00T96_A566ContagemResultado_DataUltCnt ;
      private decimal[] P00T96_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00T96_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00T96_A685ContagemResultado_PFLFSUltima ;
      private int[] P00T96_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV9Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV168Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV274TFContagemResultado_StatusUltCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV270TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPGridState AV214GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV215GridStateDynamicFilter ;
      private SdtSDT_FiltroConsContadorFM AV232SDT_FiltroConsContadorFM ;
      private wwpbaseobjects.SdtWWPContext AV287WWPContext ;
   }

   public class aexportreportwwcontagemresultadocstm__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00T96( IGxContext context ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             IGxCollection AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV9Codigos ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV168Contratadas ,
                                             int AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod ,
                                             int AV287WWPContext_gxTpr_Contratada_codigo ,
                                             String AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 ,
                                             short AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 ,
                                             String AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 ,
                                             DateTime AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1 ,
                                             DateTime AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1 ,
                                             DateTime AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1 ,
                                             String AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1 ,
                                             String AV337WWContagemResultadoDS_13_Outrosstatus1 ,
                                             int AV339WWContagemResultadoDS_15_Contagemresultado_servico1 ,
                                             int AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1 ,
                                             int AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1 ,
                                             int AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1 ,
                                             int AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1 ,
                                             int AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1 ,
                                             String AV346WWContagemResultadoDS_22_Contagemresultado_baseline1 ,
                                             int AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1 ,
                                             String AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1 ,
                                             String AV349WWContagemResultadoDS_25_Contagemresultado_descricao1 ,
                                             int AV350WWContagemResultadoDS_26_Contagemresultado_codigo1 ,
                                             bool AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 ,
                                             String AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 ,
                                             short AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 ,
                                             String AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 ,
                                             DateTime AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2 ,
                                             DateTime AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2 ,
                                             DateTime AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2 ,
                                             DateTime AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2 ,
                                             String AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2 ,
                                             String AV362WWContagemResultadoDS_38_Outrosstatus2 ,
                                             int AV364WWContagemResultadoDS_40_Contagemresultado_servico2 ,
                                             int AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2 ,
                                             int AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2 ,
                                             int AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2 ,
                                             int AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2 ,
                                             int AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2 ,
                                             String AV371WWContagemResultadoDS_47_Contagemresultado_baseline2 ,
                                             int AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2 ,
                                             String AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2 ,
                                             String AV374WWContagemResultadoDS_50_Contagemresultado_descricao2 ,
                                             int AV375WWContagemResultadoDS_51_Contagemresultado_codigo2 ,
                                             bool AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 ,
                                             String AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 ,
                                             short AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 ,
                                             String AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 ,
                                             DateTime AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3 ,
                                             DateTime AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3 ,
                                             DateTime AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3 ,
                                             DateTime AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3 ,
                                             String AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3 ,
                                             String AV387WWContagemResultadoDS_63_Outrosstatus3 ,
                                             int AV389WWContagemResultadoDS_65_Contagemresultado_servico3 ,
                                             int AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3 ,
                                             int AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3 ,
                                             int AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3 ,
                                             int AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3 ,
                                             int AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3 ,
                                             String AV396WWContagemResultadoDS_72_Contagemresultado_baseline3 ,
                                             int AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3 ,
                                             String AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3 ,
                                             String AV399WWContagemResultadoDS_75_Contagemresultado_descricao3 ,
                                             int AV400WWContagemResultadoDS_76_Contagemresultado_codigo3 ,
                                             bool AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 ,
                                             String AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 ,
                                             short AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 ,
                                             String AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 ,
                                             DateTime AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4 ,
                                             DateTime AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4 ,
                                             DateTime AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4 ,
                                             DateTime AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4 ,
                                             String AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4 ,
                                             String AV412WWContagemResultadoDS_88_Outrosstatus4 ,
                                             int AV414WWContagemResultadoDS_90_Contagemresultado_servico4 ,
                                             int AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4 ,
                                             int AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4 ,
                                             int AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4 ,
                                             int AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4 ,
                                             int AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4 ,
                                             String AV421WWContagemResultadoDS_97_Contagemresultado_baseline4 ,
                                             int AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4 ,
                                             String AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4 ,
                                             String AV424WWContagemResultadoDS_100_Contagemresultado_descricao4 ,
                                             int AV425WWContagemResultadoDS_101_Contagemresultado_codigo4 ,
                                             bool AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 ,
                                             String AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 ,
                                             short AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 ,
                                             String AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 ,
                                             DateTime AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5 ,
                                             DateTime AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5 ,
                                             DateTime AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5 ,
                                             DateTime AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5 ,
                                             String AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5 ,
                                             String AV437WWContagemResultadoDS_113_Outrosstatus5 ,
                                             int AV439WWContagemResultadoDS_115_Contagemresultado_servico5 ,
                                             int AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5 ,
                                             int AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5 ,
                                             int AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5 ,
                                             int AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5 ,
                                             int AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5 ,
                                             String AV446WWContagemResultadoDS_122_Contagemresultado_baseline5 ,
                                             int AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5 ,
                                             String AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5 ,
                                             String AV449WWContagemResultadoDS_125_Contagemresultado_descricao5 ,
                                             int AV450WWContagemResultadoDS_126_Contagemresultado_codigo5 ,
                                             String AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel ,
                                             String AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes ,
                                             DateTime AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn ,
                                             DateTime AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista ,
                                             DateTime AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to ,
                                             String AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm ,
                                             String AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel ,
                                             String AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao ,
                                             String AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla ,
                                             String AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla ,
                                             String AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel ,
                                             String AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum ,
                                             int AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV471WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel ,
                                             int AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel ,
                                             String AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico ,
                                             int AV9Codigos_Count ,
                                             int AV168Contratadas_Count ,
                                             int AV159Contratada_AreaTrabalhoCod ,
                                             bool AV232SDT_FiltroConsContadorFM_gxTpr_Abertas ,
                                             bool AV232SDT_FiltroConsContadorFM_gxTpr_Solicitadas ,
                                             bool AV232SDT_FiltroConsContadorFM_gxTpr_Soconfirmadas ,
                                             int AV232SDT_FiltroConsContadorFM_gxTpr_Servico ,
                                             int AV232SDT_FiltroConsContadorFM_gxTpr_Sistema ,
                                             bool AV232SDT_FiltroConsContadorFM_gxTpr_Semliquidar ,
                                             int AV232SDT_FiltroConsContadorFM_gxTpr_Lote ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A499ContagemResultado_ContratadaPessoaCod ,
                                             int AV287WWPContext_gxTpr_Contratada_pessoacod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A1443ContagemResultado_CntSrvPrrCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A805ContagemResultado_ContratadaOrigemCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A510ContagemResultado_EsforcoSoma ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A1612ContagemResultado_CntNum ,
                                             String A605Servico_Sigla ,
                                             int A1043ContagemResultado_LiqLogCod ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             short AV224OrderedBy ,
                                             bool AV225OrderedDsc ,
                                             DateTime AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 ,
                                             short AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 ,
                                             int AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             DateTime AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 ,
                                             DateTime AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 ,
                                             short AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 ,
                                             int AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2 ,
                                             DateTime AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 ,
                                             DateTime AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 ,
                                             short AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 ,
                                             int AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3 ,
                                             DateTime AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 ,
                                             DateTime AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 ,
                                             short AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 ,
                                             int AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4 ,
                                             DateTime AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 ,
                                             DateTime AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 ,
                                             short AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 ,
                                             int AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5 ,
                                             DateTime AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to ,
                                             int AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels_Count ,
                                             String AV475WWContagemResultadoDS_151_Tfcontagemresultado_esforcototal_sel ,
                                             String AV474WWContagemResultadoDS_150_Tfcontagemresultado_esforcototal ,
                                             String A486ContagemResultado_EsforcoTotal ,
                                             decimal AV476WWContagemResultadoDS_152_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV477WWContagemResultadoDS_153_Tfcontagemresultado_pffinal_to ,
                                             int AV214GridState_gxTpr_Dynamicfilters_Count ,
                                             int AV232SDT_FiltroConsContadorFM_gxTpr_Contagemresultado_contadorfmcod ,
                                             DateTime Gx_date ,
                                             bool AV285Usuario_EhGestor ,
                                             bool AV287WWPContext_gxTpr_Userehfinanceiro ,
                                             bool AV287WWPContext_gxTpr_Userehcontratante ,
                                             short AV287WWPContext_gxTpr_Userid ,
                                             short AV232SDT_FiltroConsContadorFM_gxTpr_Ano ,
                                             long AV232SDT_FiltroConsContadorFM_gxTpr_Mes ,
                                             bool AV232SDT_FiltroConsContadorFM_gxTpr_Comerro ,
                                             short A585ContagemResultado_Erros ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [255] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_LiqLogCod], T5.[Servico_Sigla], T7.[Contrato_Numero] AS ContagemResultado_CntNum, T9.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T8.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, RTRIM(LTRIM(COALESCE( T1.[ContagemResultado_Demanda], ''))) + CASE  WHEN (COALESCE( T1.[ContagemResultado_DemandaFM], '') = '') THEN '' ELSE '|' + RTRIM(LTRIM(COALESCE( T1.[ContagemResultado_DemandaFM], ''))) END AS ContagemResultado_OsFsOsFm, T10.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Baseline], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_CntSrvPrrCod], T6.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T9.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T9.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, COALESCE( T11.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T11.[ContagemResultado_ContadorFM],";
         scmdbuf = scmdbuf + " 0) AS ContagemResultado_ContadorFM, COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T11.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T11.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T11.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, T1.[ContagemResultado_Codigo] FROM ((((((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T7 WITH (NOLOCK) ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) LEFT JOIN [Sistema] T8 WITH (NOLOCK) ON T8.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T10 WITH (NOLOCK) ON T10.[AreaTrabalho_Codigo] = T9.[Contratada_AreaTrabalhoCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima,";
         scmdbuf = scmdbuf + " MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T11 ON T11.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T12 ON T12.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV327WWContagemResultadoDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 = 1 and @AV352WWContagemResultadoDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 = 1 and @AV377WWContagemResultadoDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 = 1 and @AV402WWContagemResultadoDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_STATUSULTCNT' and ( Not (@AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) = @AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 = 1 and @AV427WWContagemResultadoDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5 or ( (COALESCE( T11.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and ((@AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (@AV470WWCCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV470WWContagemResultadoDS_146_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T11.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and (Not ( @AV214Gri_7DynamicfiltersCount < 1 and @AV9Codigos_Count = 0 and (@AV232SDT_6Contagemresultado_c = convert(int, 0)) and (@AV232SDT_5Sistema = convert(int, 0)) and (@AV232SDT_4Lote = convert(int, 0)) and @AV232SDT_3Solicitadas = 0 and @AV232SDT_2Semliquidar = 0) or ( COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) = @Gx_date))";
         scmdbuf = scmdbuf + " and (Not ( @AV9Codigos_Count = 0 and Not (@AV232SDT_6Contagemresultado_c = convert(int, 0))) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV232SDT_6Contagemresultado_c))";
         scmdbuf = scmdbuf + " and (Not ( @AV9Codigos_Count = 0 and Not (@AV232SDT_6Contagemresultado_c = convert(int, 0)) and @AV232SDT_9Ano + @AV232SDT_8Mes = 0 and Not @AV285Usuario_EhGestor = 1) or ( COALESCE( T11.[ContagemResultado_ContadorFM], 0) = @AV232SDT_6Contagemresultado_c))";
         scmdbuf = scmdbuf + " and (Not ( @AV9Codigos_Count = 0 and Not (@AV232SDT_8Mes = convert(int, 0))) or ( MONTH(COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV232SDT_8Mes))";
         scmdbuf = scmdbuf + " and (Not ( @AV9Codigos_Count = 0 and Not (@AV232SDT_9Ano = convert(int, 0))) or ( YEAR(COALESCE( T11.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV232SDT_9Ano))";
         scmdbuf = scmdbuf + " and (Not ( @AV9Codigos_Count = 0 and @AV232SDT_10Soconfirmadas = 1) or ( COALESCE( T11.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_AreaTrabalhoCod] = @AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int4[111] = 1;
         }
         if ( ( AV287WWPContext_gxTpr_Contratada_codigo > 0 ) && ( AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod > 0 ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV287WWP_11Contratada_codigo)";
         }
         else
         {
            GXv_int4[112] = 1;
         }
         if ( ( AV287WWPContext_gxTpr_Contratada_codigo > 0 ) && (0==AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_PessoaCod] = @AV287WWP_12Contratada_pessoac)";
         }
         else
         {
            GXv_int4[113] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[114] = 1;
            GXv_int4[115] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[116] = 1;
            GXv_int4[117] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[118] = 1;
            GXv_int4[119] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int4[120] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int4[121] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int4[122] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int4[123] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int4[124] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV337WWContagemResultadoDS_13_Outrosstatus1)) && ( StringUtil.StrCmp(AV337WWContagemResultadoDS_13_Outrosstatus1, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV337WWContagemResultadoDS_13_Outrosstatus1)) > 0)";
         }
         else
         {
            GXv_int4[125] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV339WWContagemResultadoDS_15_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV339WWContagemResultadoDS_15_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int4[126] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1)";
         }
         else
         {
            GXv_int4[127] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int4[128] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1) && ! (0==AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int4[129] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1)";
         }
         else
         {
            GXv_int4[130] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int4[131] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV346WWContagemResultadoDS_22_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV346WWContagemResultadoDS_22_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int4[132] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int4[133] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV328WWContagemResultadoDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int4[134] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int4[135] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV349WWContagemResultadoDS_25_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV349WWContagemResultadoDS_25_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int4[136] = 1;
         }
         if ( ( StringUtil.StrCmp(AV327WWContagemResultadoDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV350WWContagemResultadoDS_26_Contagemresultado_codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV350WWContagemResultadoDS_26_Contagemresultado_codigo1)";
         }
         else
         {
            GXv_int4[137] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[138] = 1;
            GXv_int4[139] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[140] = 1;
            GXv_int4[141] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[142] = 1;
            GXv_int4[143] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int4[144] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int4[145] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int4[146] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int4[147] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int4[148] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV362WWContagemResultadoDS_38_Outrosstatus2)) && ( StringUtil.StrCmp(AV362WWContagemResultadoDS_38_Outrosstatus2, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV362WWContagemResultadoDS_38_Outrosstatus2)) > 0)";
         }
         else
         {
            GXv_int4[149] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV364WWContagemResultadoDS_40_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV364WWContagemResultadoDS_40_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int4[150] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2)";
         }
         else
         {
            GXv_int4[151] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int4[152] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2) && ! (0==AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int4[153] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2)";
         }
         else
         {
            GXv_int4[154] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int4[155] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV371WWContagemResultadoDS_47_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV371WWContagemResultadoDS_47_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int4[156] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int4[157] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV353WWContagemResultadoDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int4[158] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int4[159] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV374WWContagemResultadoDS_50_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV374WWContagemResultadoDS_50_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int4[160] = 1;
         }
         if ( AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV352WWContagemResultadoDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV375WWContagemResultadoDS_51_Contagemresultado_codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV375WWContagemResultadoDS_51_Contagemresultado_codigo2)";
         }
         else
         {
            GXv_int4[161] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[162] = 1;
            GXv_int4[163] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[164] = 1;
            GXv_int4[165] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[166] = 1;
            GXv_int4[167] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int4[168] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int4[169] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int4[170] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int4[171] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int4[172] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV387WWContagemResultadoDS_63_Outrosstatus3)) && ( StringUtil.StrCmp(AV387WWContagemResultadoDS_63_Outrosstatus3, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV387WWContagemResultadoDS_63_Outrosstatus3)) > 0)";
         }
         else
         {
            GXv_int4[173] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV389WWContagemResultadoDS_65_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV389WWContagemResultadoDS_65_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int4[174] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3)";
         }
         else
         {
            GXv_int4[175] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int4[176] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3) && ! (0==AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int4[177] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3)";
         }
         else
         {
            GXv_int4[178] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int4[179] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV396WWContagemResultadoDS_72_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV396WWContagemResultadoDS_72_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int4[180] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int4[181] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV378WWContagemResultadoDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int4[182] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int4[183] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV399WWContagemResultadoDS_75_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV399WWContagemResultadoDS_75_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int4[184] = 1;
         }
         if ( AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV377WWContagemResultadoDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV400WWContagemResultadoDS_76_Contagemresultado_codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV400WWContagemResultadoDS_76_Contagemresultado_codigo3)";
         }
         else
         {
            GXv_int4[185] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int4[186] = 1;
            GXv_int4[187] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int4[188] = 1;
            GXv_int4[189] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int4[190] = 1;
            GXv_int4[191] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int4[192] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int4[193] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int4[194] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int4[195] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int4[196] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV412WWContagemResultadoDS_88_Outrosstatus4)) && ( StringUtil.StrCmp(AV412WWContagemResultadoDS_88_Outrosstatus4, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV412WWContagemResultadoDS_88_Outrosstatus4)) > 0)";
         }
         else
         {
            GXv_int4[197] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV414WWContagemResultadoDS_90_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV414WWContagemResultadoDS_90_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int4[198] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4)";
         }
         else
         {
            GXv_int4[199] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int4[200] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4) && ! (0==AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int4[201] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4)";
         }
         else
         {
            GXv_int4[202] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int4[203] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV421WWContagemResultadoDS_97_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV421WWContagemResultadoDS_97_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! (0==AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4)";
         }
         else
         {
            GXv_int4[204] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! (0==AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4)";
         }
         else
         {
            GXv_int4[205] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV403WWContagemResultadoDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! (0==AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4)";
         }
         else
         {
            GXv_int4[206] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int4[207] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV424WWContagemResultadoDS_100_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV424WWContagemResultadoDS_100_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int4[208] = 1;
         }
         if ( AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV402WWContagemResultadoDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV425WWContagemResultadoDS_101_Contagemresultado_codigo4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV425WWContagemResultadoDS_101_Contagemresultado_codigo4)";
         }
         else
         {
            GXv_int4[209] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int4[210] = 1;
            GXv_int4[211] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int4[212] = 1;
            GXv_int4[213] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int4[214] = 1;
            GXv_int4[215] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int4[216] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int4[217] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int4[218] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int4[219] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int4[220] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "OUTROSSTATUS") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV437WWContagemResultadoDS_113_Outrosstatus5)) && ( StringUtil.StrCmp(AV437WWContagemResultadoDS_113_Outrosstatus5, "*") != 0 ) ) )
         {
            sWhereString = sWhereString + " and ((CHARINDEX(RTRIM(T1.[ContagemResultado_StatusDmn]), @AV437WWContagemResultadoDS_113_Outrosstatus5)) > 0)";
         }
         else
         {
            GXv_int4[221] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV439WWContagemResultadoDS_115_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV439WWContagemResultadoDS_115_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int4[222] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTSRVPRRCOD") == 0 ) && ( ! (0==AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvPrrCod] = @AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5)";
         }
         else
         {
            GXv_int4[223] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int4[224] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5) && ! (0==AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int4[225] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ! (0==AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5)";
         }
         else
         {
            GXv_int4[226] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int4[227] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV446WWContagemResultadoDS_122_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV446WWContagemResultadoDS_122_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! (0==AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) > @AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5)";
         }
         else
         {
            GXv_int4[228] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! (0==AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) < @AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5)";
         }
         else
         {
            GXv_int4[229] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV428WWContagemResultadoDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! (0==AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T12.[ContagemResultado_EsforcoSoma], 0) = @AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5)";
         }
         else
         {
            GXv_int4[230] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int4[231] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV449WWContagemResultadoDS_125_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV449WWContagemResultadoDS_125_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int4[232] = 1;
         }
         if ( AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV427WWContagemResultadoDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CODIGO") == 0 ) && ( ! (0==AV450WWContagemResultadoDS_126_Contagemresultado_codigo5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV450WWContagemResultadoDS_126_Contagemresultado_codigo5)";
         }
         else
         {
            GXv_int4[233] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes)) ) )
         {
            sWhereString = sWhereString + " and (T10.[AreaTrabalho_Descricao] like @lV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes)";
         }
         else
         {
            GXv_int4[234] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel)) )
         {
            sWhereString = sWhereString + " and (T10.[AreaTrabalho_Descricao] = @AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel)";
         }
         else
         {
            GXv_int4[235] = 1;
         }
         if ( ! (DateTime.MinValue==AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int4[236] = 1;
         }
         if ( ! (DateTime.MinValue==AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int4[237] = 1;
         }
         if ( ! (DateTime.MinValue==AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int4[238] = 1;
         }
         if ( ! (DateTime.MinValue==AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int4[239] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int4[240] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int4[241] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int4[242] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int4[243] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T8.[Sistema_Sigla] like @lV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int4[244] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T8.[Sistema_Sigla] = @AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int4[245] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_Sigla] like @lV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int4[246] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Contratada_Sigla] = @AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int4[247] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Contrato_Numero] like @lV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum)";
         }
         else
         {
            GXv_int4[248] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Contrato_Numero] = @AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel)";
         }
         else
         {
            GXv_int4[249] = 1;
         }
         if ( AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV469WWContagemResultadoDS_145_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV471WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV471WWContagemResultadoDS_147_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( (0==AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV472WWContagemResultadoDS_148_Tfcontagemresultado_servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV472WWContagemResultadoDS_148_Tfcontagemresultado_servico)";
         }
         else
         {
            GXv_int4[250] = 1;
         }
         if ( ! (0==AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel)";
         }
         else
         {
            GXv_int4[251] = 1;
         }
         if ( AV9Codigos_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV9Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         }
         if ( ( AV9Codigos_Count == 0 ) && ( AV168Contratadas_Count > 0 ) && (0==AV159Contratada_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV168Contratadas, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( ( AV9Codigos_Count == 0 ) && ( AV232SDT_FiltroConsContadorFM_gxTpr_Abertas ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = 'A')";
         }
         if ( ( AV9Codigos_Count == 0 ) && ( AV232SDT_FiltroConsContadorFM_gxTpr_Solicitadas ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'B' or T1.[ContagemResultado_StatusDmn] = 'E')";
         }
         if ( ( AV9Codigos_Count == 0 ) && ( AV232SDT_FiltroConsContadorFM_gxTpr_Soconfirmadas ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'C' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')";
         }
         if ( ( AV9Codigos_Count == 0 ) && ! (0==AV232SDT_FiltroConsContadorFM_gxTpr_Servico) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV232SDT_13Servico)";
         }
         else
         {
            GXv_int4[252] = 1;
         }
         if ( ( AV9Codigos_Count == 0 ) && ! (0==AV232SDT_FiltroConsContadorFM_gxTpr_Sistema) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV232SDT_5Sistema)";
         }
         else
         {
            GXv_int4[253] = 1;
         }
         if ( ( AV9Codigos_Count == 0 ) && ( AV232SDT_FiltroConsContadorFM_gxTpr_Semliquidar ) )
         {
            sWhereString = sWhereString + " and (( T1.[ContagemResultado_LiqLogCod] IS NULL or (T1.[ContagemResultado_LiqLogCod] = convert(int, 0))) and ( T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L'))";
         }
         if ( ( AV9Codigos_Count == 0 ) && ! (0==AV232SDT_FiltroConsContadorFM_gxTpr_Lote) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_LoteAceiteCod] = @AV232SDT_4Lote)";
         }
         else
         {
            GXv_int4[254] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV224OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo] DESC";
         }
         else if ( AV224OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV224OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn] DESC";
         }
         else if ( AV224OrderedBy == 4 )
         {
            scmdbuf = scmdbuf + " ORDER BY T11.[ContagemResultado_StatusUltCnt] DESC";
         }
         else if ( AV224OrderedBy == 5 )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_OsFsOsFm] DESC";
         }
         else if ( AV224OrderedBy == 6 )
         {
            scmdbuf = scmdbuf + " ORDER BY T8.[Sistema_Sigla] DESC";
         }
         else if ( AV224OrderedBy == 7 )
         {
            scmdbuf = scmdbuf + " ORDER BY T11.[ContagemResultado_DataUltCnt] DESC";
         }
         else if ( ( AV224OrderedBy == 8 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T10.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV224OrderedBy == 8 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T10.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV224OrderedBy == 9 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista]";
         }
         else if ( ( AV224OrderedBy == 9 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataPrevista] DESC";
         }
         else if ( ( AV224OrderedBy == 10 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao]";
         }
         else if ( ( AV224OrderedBy == 10 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao] DESC";
         }
         else if ( ( AV224OrderedBy == 11 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[Contratada_Sigla]";
         }
         else if ( ( AV224OrderedBy == 11 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[Contratada_Sigla] DESC";
         }
         else if ( ( AV224OrderedBy == 12 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[Contrato_Numero]";
         }
         else if ( ( AV224OrderedBy == 12 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[Contrato_Numero] DESC";
         }
         else if ( ( AV224OrderedBy == 13 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline]";
         }
         else if ( ( AV224OrderedBy == 13 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline] DESC";
         }
         else if ( ( AV224OrderedBy == 14 ) && ! AV225OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Codigo]";
         }
         else if ( ( AV224OrderedBy == 14 ) && ( AV225OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Codigo] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_P00T96(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (int)dynConstraints[6] , (IGxCollection)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (bool)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (String)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (int)dynConstraints[47] , (String)dynConstraints[48] , (String)dynConstraints[49] , (int)dynConstraints[50] , (bool)dynConstraints[51] , (String)dynConstraints[52] , (short)dynConstraints[53] , (String)dynConstraints[54] , (DateTime)dynConstraints[55] , (DateTime)dynConstraints[56] , (DateTime)dynConstraints[57] , (DateTime)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (int)dynConstraints[61] , (int)dynConstraints[62] , (int)dynConstraints[63] , (int)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (String)dynConstraints[69] , (String)dynConstraints[70] , (int)dynConstraints[71] , (bool)dynConstraints[72] , (String)dynConstraints[73] , (short)dynConstraints[74] , (String)dynConstraints[75] , (DateTime)dynConstraints[76] , (DateTime)dynConstraints[77] , (DateTime)dynConstraints[78] , (DateTime)dynConstraints[79] , (String)dynConstraints[80] , (String)dynConstraints[81] , (int)dynConstraints[82] , (int)dynConstraints[83] , (int)dynConstraints[84] , (int)dynConstraints[85] , (int)dynConstraints[86] , (int)dynConstraints[87] , (String)dynConstraints[88] , (int)dynConstraints[89] , (String)dynConstraints[90] , (String)dynConstraints[91] , (int)dynConstraints[92] , (bool)dynConstraints[93] , (String)dynConstraints[94] , (short)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (String)dynConstraints[101] , (String)dynConstraints[102] , (int)dynConstraints[103] , (int)dynConstraints[104] , (int)dynConstraints[105] , (int)dynConstraints[106] , (int)dynConstraints[107] , (int)dynConstraints[108] , (String)dynConstraints[109] , (int)dynConstraints[110] , (String)dynConstraints[111] , (String)dynConstraints[112] , (int)dynConstraints[113] , (String)dynConstraints[114] , (String)dynConstraints[115] , (DateTime)dynConstraints[116] , (DateTime)dynConstraints[117] , (DateTime)dynConstraints[118] , (DateTime)dynConstraints[119] , (String)dynConstraints[120] , (String)dynConstraints[121] , (String)dynConstraints[122] , (String)dynConstraints[123] , (String)dynConstraints[124] , (String)dynConstraints[125] , (String)dynConstraints[126] , (String)dynConstraints[127] , (String)dynConstraints[128] , (String)dynConstraints[129] , (int)dynConstraints[130] , (short)dynConstraints[131] , (int)dynConstraints[132] , (String)dynConstraints[133] , (int)dynConstraints[134] , (int)dynConstraints[135] , (int)dynConstraints[136] , (bool)dynConstraints[137] , (bool)dynConstraints[138] , (bool)dynConstraints[139] , (int)dynConstraints[140] , (int)dynConstraints[141] , (bool)dynConstraints[142] , (int)dynConstraints[143] , (int)dynConstraints[144] , (int)dynConstraints[145] , (int)dynConstraints[146] , (String)dynConstraints[147] , (String)dynConstraints[148] , (DateTime)dynConstraints[149] , (DateTime)dynConstraints[150] , (int)dynConstraints[151] , (int)dynConstraints[152] , (int)dynConstraints[153] , (int)dynConstraints[154] , (int)dynConstraints[155] , (bool)dynConstraints[156] , (int)dynConstraints[157] , (String)dynConstraints[158] , (String)dynConstraints[159] , (String)dynConstraints[160] , (String)dynConstraints[161] , (String)dynConstraints[162] , (String)dynConstraints[163] , (String)dynConstraints[164] , (int)dynConstraints[165] , (int)dynConstraints[166] , (short)dynConstraints[167] , (bool)dynConstraints[168] , (DateTime)dynConstraints[169] , (DateTime)dynConstraints[170] , (DateTime)dynConstraints[171] , (short)dynConstraints[172] , (int)dynConstraints[173] , (int)dynConstraints[174] , (int)dynConstraints[175] , (DateTime)dynConstraints[176] , (DateTime)dynConstraints[177] , (short)dynConstraints[178] , (int)dynConstraints[179] , (DateTime)dynConstraints[180] , (DateTime)dynConstraints[181] , (short)dynConstraints[182] , (int)dynConstraints[183] , (DateTime)dynConstraints[184] , (DateTime)dynConstraints[185] , (short)dynConstraints[186] , (int)dynConstraints[187] , (DateTime)dynConstraints[188] , (DateTime)dynConstraints[189] , (short)dynConstraints[190] , (int)dynConstraints[191] , (DateTime)dynConstraints[192] , (DateTime)dynConstraints[193] , (int)dynConstraints[194] , (String)dynConstraints[195] , (String)dynConstraints[196] , (String)dynConstraints[197] , (decimal)dynConstraints[198] , (decimal)dynConstraints[199] , (decimal)dynConstraints[200] , (int)dynConstraints[201] , (int)dynConstraints[202] , (DateTime)dynConstraints[203] , (bool)dynConstraints[204] , (bool)dynConstraints[205] , (bool)dynConstraints[206] , (short)dynConstraints[207] , (short)dynConstraints[208] , (long)dynConstraints[209] , (bool)dynConstraints[210] , (short)dynConstraints[211] , (decimal)dynConstraints[212] , (short)dynConstraints[213] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T92 ;
          prmP00T92 = new Object[] {
          new Object[] {"@AV159Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T93 ;
          prmP00T93 = new Object[] {
          new Object[] {"@AV160Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T96 ;
          prmP00T96 = new Object[] {
          new Object[] {"@AV327WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV332WWContagemResultadoDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV327WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV333WWContagemResultadoDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV327WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV338WWContagemResultadoDS_14_Contagemresultado_statusultcnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV327WWContagemResultadoDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV341WWContagemResultadoDS_17_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV352WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV357WWContagemResultadoDS_33_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV352WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV358WWContagemResultadoDS_34_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV352WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV363WWContagemResultadoDS_39_Contagemresultado_statusultcnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV351WWContagemResultadoDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV352WWContagemResultadoDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV366WWContagemResultadoDS_42_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV377WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV382WWContagemResultadoDS_58_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV377WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV383WWContagemResultadoDS_59_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV377WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV388WWContagemResultadoDS_64_Contagemresultado_statusultcnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV376WWContagemResultadoDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV377WWContagemResultadoDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV391WWContagemResultadoDS_67_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV402WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV407WWContagemResultadoDS_83_Contagemresultado_dataultcnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV402WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV408WWContagemResultadoDS_84_Contagemresultado_dataultcnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV402WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV413WWContagemResultadoDS_89_Contagemresultado_statusultcnt4",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV401WWContagemResultadoDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV402WWContagemResultadoDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV416WWContagemResultadoDS_92_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV427WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV432WWContagemResultadoDS_108_Contagemresultado_dataultcnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV427WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV433WWContagemResultadoDS_109_Contagemresultado_dataultcnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV427WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV438WWContagemResultadoDS_114_Contagemresultado_statusultcnt5",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV426WWContagemResultadoDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV427WWContagemResultadoDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV441WWContagemResultadoDS_117_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV455WWContagemResultadoDS_131_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV456WWContagemResultadoDS_132_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV470WWCCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV214Gri_7DynamicfiltersCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV9Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV232SDT_6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_5Sistema",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_4Lote",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_3Solicitadas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV232SDT_2Semliquidar",SqlDbType.Bit,4,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV232SDT_6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV232SDT_6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_9Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV232SDT_8Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV285Usuario_EhGestor",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV232SDT_6Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV232SDT_8Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV232SDT_8Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV9Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV232SDT_9Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV232SDT_9Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9Codigos_Count",SqlDbType.Int,9,0} ,
          new Object[] {"@AV232SDT_10Soconfirmadas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV325WWContagemResultadoDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV287WWP_11Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV287WWP_12Contratada_pessoac",SqlDbType.Int,6,0} ,
          new Object[] {"@AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV329WWContagemResultadoDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV330WWContagemResultadoDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV331WWContagemResultadoDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV334WWContagemResultadoDS_10_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV335WWContagemResultadoDS_11_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV336WWContagemResultadoDS_12_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV337WWContagemResultadoDS_13_Outrosstatus1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV339WWContagemResultadoDS_15_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340WWContagemResultadoDS_16_Contagemresultado_cntsrvprrcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV342WWContagemResultadoDS_18_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV343WWContagemResultadoDS_19_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV344WWContagemResultadoDS_20_Contagemresultado_contratadaorigemcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV345WWContagemResultadoDS_21_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV347WWContagemResultadoDS_23_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV348WWContagemResultadoDS_24_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV349WWContagemResultadoDS_25_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV350WWContagemResultadoDS_26_Contagemresultado_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV354WWContagemResultadoDS_30_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV355WWContagemResultadoDS_31_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV356WWContagemResultadoDS_32_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV359WWContagemResultadoDS_35_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV360WWContagemResultadoDS_36_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV361WWContagemResultadoDS_37_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV362WWContagemResultadoDS_38_Outrosstatus2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV364WWContagemResultadoDS_40_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365WWContagemResultadoDS_41_Contagemresultado_cntsrvprrcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV367WWContagemResultadoDS_43_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV368WWContagemResultadoDS_44_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV369WWContagemResultadoDS_45_Contagemresultado_contratadaorigemcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV370WWContagemResultadoDS_46_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV372WWContagemResultadoDS_48_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV373WWContagemResultadoDS_49_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV374WWContagemResultadoDS_50_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV375WWContagemResultadoDS_51_Contagemresultado_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV379WWContagemResultadoDS_55_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV380WWContagemResultadoDS_56_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV381WWContagemResultadoDS_57_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV384WWContagemResultadoDS_60_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV385WWContagemResultadoDS_61_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV386WWContagemResultadoDS_62_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV387WWContagemResultadoDS_63_Outrosstatus3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV389WWContagemResultadoDS_65_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390WWContagemResultadoDS_66_Contagemresultado_cntsrvprrcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV392WWContagemResultadoDS_68_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV393WWContagemResultadoDS_69_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV394WWContagemResultadoDS_70_Contagemresultado_contratadaorigemcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV395WWContagemResultadoDS_71_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV397WWContagemResultadoDS_73_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV398WWContagemResultadoDS_74_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV399WWContagemResultadoDS_75_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV400WWContagemResultadoDS_76_Contagemresultado_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV404WWContagemResultadoDS_80_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV405WWContagemResultadoDS_81_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV406WWContagemResultadoDS_82_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV409WWContagemResultadoDS_85_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV410WWContagemResultadoDS_86_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV411WWContagemResultadoDS_87_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV412WWContagemResultadoDS_88_Outrosstatus4",SqlDbType.Char,20,0} ,
          new Object[] {"@AV414WWContagemResultadoDS_90_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415WWContagemResultadoDS_91_Contagemresultado_cntsrvprrcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV417WWContagemResultadoDS_93_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV418WWContagemResultadoDS_94_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV419WWContagemResultadoDS_95_Contagemresultado_contratadaorigemcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV420WWContagemResultadoDS_96_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4",SqlDbType.Int,8,0} ,
          new Object[] {"@AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4",SqlDbType.Int,8,0} ,
          new Object[] {"@AV422WWContagemResultadoDS_98_Contagemresultado_esforcosoma4",SqlDbType.Int,8,0} ,
          new Object[] {"@AV423WWContagemResultadoDS_99_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV424WWContagemResultadoDS_100_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV425WWContagemResultadoDS_101_Contagemresultado_codigo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV429WWContagemResultadoDS_105_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV430WWContagemResultadoDS_106_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV431WWContagemResultadoDS_107_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV434WWContagemResultadoDS_110_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV435WWContagemResultadoDS_111_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV436WWContagemResultadoDS_112_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV437WWContagemResultadoDS_113_Outrosstatus5",SqlDbType.Char,20,0} ,
          new Object[] {"@AV439WWContagemResultadoDS_115_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV440WWContagemResultadoDS_116_Contagemresultado_cntsrvprrcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV442WWContagemResultadoDS_118_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV443WWContagemResultadoDS_119_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV444WWContagemResultadoDS_120_Contagemresultado_contratadaorigemcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV445WWContagemResultadoDS_121_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5",SqlDbType.Int,8,0} ,
          new Object[] {"@AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5",SqlDbType.Int,8,0} ,
          new Object[] {"@AV447WWContagemResultadoDS_123_Contagemresultado_esforcosoma5",SqlDbType.Int,8,0} ,
          new Object[] {"@AV448WWContagemResultadoDS_124_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV449WWContagemResultadoDS_125_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV450WWContagemResultadoDS_126_Contagemresultado_codigo5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV451WWContagemResultadoDS_127_Tfcontratada_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV452WWContagemResultadoDS_128_Tfcontratada_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV453WWContagemResultadoDS_129_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV454WWContagemResultadoDS_130_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV457WWContagemResultadoDS_133_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV458WWContagemResultadoDS_134_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV459WWContagemResultadoDS_135_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV460WWContagemResultadoDS_136_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV461WWContagemResultadoDS_137_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV462WWContagemResultadoDS_138_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV463WWContagemResultadoDS_139_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV464WWContagemResultadoDS_140_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV465WWContagemResultadoDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV466WWContagemResultadoDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV467WWContagemResultadoDS_143_Tfcontagemresultado_cntnum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV468WWContagemResultadoDS_144_Tfcontagemresultado_cntnum_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV472WWContagemResultadoDS_148_Tfcontagemresultado_servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV473WWContagemResultadoDS_149_Tfcontagemresultado_servico_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_13Servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_5Sistema",SqlDbType.Int,6,0} ,
          new Object[] {"@AV232SDT_4Lote",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T92", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV159Contratada_AreaTrabalhoCod ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T92,1,0,false,true )
             ,new CursorDef("P00T93", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV160Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T93,1,0,false,true )
             ,new CursorDef("P00T96", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T96,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 20) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 25) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((String[]) buf[24])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((int[]) buf[34])[0] = rslt.getInt(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((int[]) buf[36])[0] = rslt.getInt(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((int[]) buf[38])[0] = rslt.getInt(22) ;
                ((int[]) buf[39])[0] = rslt.getInt(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((int[]) buf[41])[0] = rslt.getInt(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getString(25, 1) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[45])[0] = rslt.getGXDateTime(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((DateTime[]) buf[47])[0] = rslt.getGXDate(27) ;
                ((int[]) buf[48])[0] = rslt.getInt(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((int[]) buf[50])[0] = rslt.getInt(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((decimal[]) buf[54])[0] = rslt.getDecimal(31) ;
                ((String[]) buf[55])[0] = rslt.getVarchar(32) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(32);
                ((String[]) buf[57])[0] = rslt.getVarchar(33) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(33);
                ((int[]) buf[59])[0] = rslt.getInt(34) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(34);
                ((int[]) buf[61])[0] = rslt.getInt(35) ;
                ((short[]) buf[62])[0] = rslt.getShort(36) ;
                ((DateTime[]) buf[63])[0] = rslt.getGXDate(37) ;
                ((decimal[]) buf[64])[0] = rslt.getDecimal(38) ;
                ((decimal[]) buf[65])[0] = rslt.getDecimal(39) ;
                ((decimal[]) buf[66])[0] = rslt.getDecimal(40) ;
                ((int[]) buf[67])[0] = rslt.getInt(41) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[255]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[256]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[257]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[259]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[260]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[262]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[263]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[265]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[266]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[267]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[268]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[270]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[271]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[272]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[273]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[274]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[275]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[276]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[277]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[278]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[279]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[280]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[281]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[282]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[283]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[284]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[285]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[286]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[287]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[288]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[289]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[290]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[291]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[292]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[293]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[294]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[295]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[296]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[297]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[298]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[299]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[300]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[301]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[302]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[304]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[305]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[306]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[308]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[309]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[310]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[311]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[312]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[313]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[314]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[315]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[318]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[319]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[320]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[321]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[322]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[323]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[325]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[326]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[327]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[329]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[330]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[331]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[332]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[334]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[336]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[337]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[338]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[339]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[340]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[341]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[342]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[343]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[344]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[345]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[346]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[347]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[348]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[349]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[350]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[351]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[352]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[354]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[355]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[356]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[358]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[359]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[360]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[361]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[362]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[363]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[364]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[365]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[366]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[367]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[368]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[369]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[370]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[371]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[372]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[373]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[374]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[375]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[376]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[377]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[378]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[380]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[381]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[382]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[383]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[384]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[385]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[386]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[387]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[388]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[389]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[390]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[391]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[392]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[393]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[394]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[395]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[396]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[397]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[398]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[399]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[400]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[401]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[402]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[403]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[405]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[406]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[407]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[408]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[409]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[410]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[411]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[412]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[413]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[414]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[415]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[416]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[420]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[421]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[422]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[423]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[424]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[425]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[426]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[427]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[428]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[429]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[430]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[431]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[432]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[433]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[434]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[435]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[436]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[437]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[438]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[439]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[440]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[441]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[442]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[443]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[444]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[445]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[446]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[447]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[448]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[449]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[450]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[451]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[452]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[453]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[454]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[455]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[456]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[457]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[458]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[459]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[460]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[461]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[462]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[463]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[464]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[465]);
                }
                if ( (short)parms[211] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[466]);
                }
                if ( (short)parms[212] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[467]);
                }
                if ( (short)parms[213] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[468]);
                }
                if ( (short)parms[214] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[469]);
                }
                if ( (short)parms[215] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[470]);
                }
                if ( (short)parms[216] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[471]);
                }
                if ( (short)parms[217] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[472]);
                }
                if ( (short)parms[218] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[473]);
                }
                if ( (short)parms[219] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[474]);
                }
                if ( (short)parms[220] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[475]);
                }
                if ( (short)parms[221] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[476]);
                }
                if ( (short)parms[222] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[477]);
                }
                if ( (short)parms[223] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[478]);
                }
                if ( (short)parms[224] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[479]);
                }
                if ( (short)parms[225] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[480]);
                }
                if ( (short)parms[226] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[481]);
                }
                if ( (short)parms[227] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[482]);
                }
                if ( (short)parms[228] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[483]);
                }
                if ( (short)parms[229] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[484]);
                }
                if ( (short)parms[230] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[485]);
                }
                if ( (short)parms[231] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[486]);
                }
                if ( (short)parms[232] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[487]);
                }
                if ( (short)parms[233] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[488]);
                }
                if ( (short)parms[234] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[489]);
                }
                if ( (short)parms[235] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[490]);
                }
                if ( (short)parms[236] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[491]);
                }
                if ( (short)parms[237] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[492]);
                }
                if ( (short)parms[238] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[493]);
                }
                if ( (short)parms[239] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[494]);
                }
                if ( (short)parms[240] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[495]);
                }
                if ( (short)parms[241] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[496]);
                }
                if ( (short)parms[242] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[497]);
                }
                if ( (short)parms[243] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[498]);
                }
                if ( (short)parms[244] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[499]);
                }
                if ( (short)parms[245] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[500]);
                }
                if ( (short)parms[246] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[501]);
                }
                if ( (short)parms[247] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[502]);
                }
                if ( (short)parms[248] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[503]);
                }
                if ( (short)parms[249] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[504]);
                }
                if ( (short)parms[250] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[505]);
                }
                if ( (short)parms[251] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[506]);
                }
                if ( (short)parms[252] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[507]);
                }
                if ( (short)parms[253] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[508]);
                }
                if ( (short)parms[254] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[509]);
                }
                return;
       }
    }

 }

}
