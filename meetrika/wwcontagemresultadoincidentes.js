/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:18:25.83
*/
gx.evt.autoSkip = false;
gx.define('wwcontagemresultadoincidentes', false, function () {
   this.ServerClass =  "wwcontagemresultadoincidentes" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV72Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV30DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV29DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
   };
   this.Validv_Incidentes_data1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vINCIDENTES_DATA1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV16Incidentes_Data1)==0) || new gx.date.gxdate( this.AV16Incidentes_Data1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Incidentes_Data1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Incidentes_data_to1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vINCIDENTES_DATA_TO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV17Incidentes_Data_To1)==0) || new gx.date.gxdate( this.AV17Incidentes_Data_To1 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Incidentes_Data_To1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Incidentes_data2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vINCIDENTES_DATA2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV21Incidentes_Data2)==0) || new gx.date.gxdate( this.AV21Incidentes_Data2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Incidentes_Data2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Incidentes_data_to2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vINCIDENTES_DATA_TO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV22Incidentes_Data_To2)==0) || new gx.date.gxdate( this.AV22Incidentes_Data_To2 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Incidentes_Data_To2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Incidentes_data3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vINCIDENTES_DATA3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV26Incidentes_Data3)==0) || new gx.date.gxdate( this.AV26Incidentes_Data3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Incidentes_Data3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Incidentes_data_to3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vINCIDENTES_DATA_TO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV27Incidentes_Data_To3)==0) || new gx.date.gxdate( this.AV27Incidentes_Data_To3 ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Incidentes_Data_To3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Incidentes_demandacod=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(97) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("INCIDENTES_DEMANDACOD", gx.fn.currentGridRowImpl(97));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfincidentes_data=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFINCIDENTES_DATA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV37TFIncidentes_Data)==0) || new gx.date.gxdate( this.AV37TFIncidentes_Data ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFIncidentes_Data fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfincidentes_data_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFINCIDENTES_DATA_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV38TFIncidentes_Data_To)==0) || new gx.date.gxdate( this.AV38TFIncidentes_Data_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFIncidentes_Data_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_incidentes_dataauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_INCIDENTES_DATAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV39DDO_Incidentes_DataAuxDate)==0) || new gx.date.gxdate( this.AV39DDO_Incidentes_DataAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Incidentes_Data Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_incidentes_dataauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_INCIDENTES_DATAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV40DDO_Incidentes_DataAuxDateTo)==0) || new gx.date.gxdate( this.AV40DDO_Incidentes_DataAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Incidentes_Data Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible", false );
      gx.fn.setCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "INCIDENTES_DATA" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "INCIDENTES_DEMANDANUM" )
      {
         gx.fn.setCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible", false );
      gx.fn.setCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible", false );
      if ( this.AV20DynamicFiltersSelector2 == "INCIDENTES_DATA" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible", true );
      }
      else if ( this.AV20DynamicFiltersSelector2 == "INCIDENTES_DEMANDANUM" )
      {
         gx.fn.setCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible", false );
      gx.fn.setCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible", false );
      if ( this.AV25DynamicFiltersSelector3 == "INCIDENTES_DATA" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible", true );
      }
      else if ( this.AV25DynamicFiltersSelector3 == "INCIDENTES_DEMANDANUM" )
      {
         gx.fn.setCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible", true );
      }
   };
   this.s162_client=function()
   {
      this.s182_client();
      if ( this.AV13OrderedBy == 1 )
      {
         this.DDO_INCIDENTES_DATAContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 2 )
      {
         this.DDO_INCIDENTES_DEMANDANUMContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s182_client=function()
   {
      this.DDO_INCIDENTES_DATAContainer.SortedStatus =  ""  ;
      this.DDO_INCIDENTES_DEMANDANUMContainer.SortedStatus =  ""  ;
   };
   this.s202_client=function()
   {
      this.AV19DynamicFiltersEnabled2 =  false  ;
      this.AV20DynamicFiltersSelector2 =  "INCIDENTES_DATA"  ;
      this.AV21Incidentes_Data2 =  ''  ;
      this.AV22Incidentes_Data_To2 =  ''  ;
      this.s122_client();
      this.AV24DynamicFiltersEnabled3 =  false  ;
      this.AV25DynamicFiltersSelector3 =  "INCIDENTES_DATA"  ;
      this.AV26Incidentes_Data3 =  ''  ;
      this.AV27Incidentes_Data_To3 =  ''  ;
      this.s132_client();
   };
   this.e11j42_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12j42_client=function()
   {
      this.executeServerEvent("DDO_INCIDENTES_DATA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13j42_client=function()
   {
      this.executeServerEvent("DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14j42_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e15j42_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e16j42_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e17j42_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e18j42_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e19j42_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e20j42_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e21j42_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e22j42_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e23j42_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e24j42_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e28j42_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e29j42_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,18,20,21,23,26,28,31,33,35,37,40,42,44,45,47,48,51,53,55,57,60,62,64,65,67,68,71,73,75,77,80,82,84,85,87,88,91,94,98,99,100,101,102,103,108,109,110,111,112,113,114,115,116,118,120];
   this.GXLastCtrlId =120;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",97,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwcontagemresultadoincidentes",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",98,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",99,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(1241,100,"INCIDENTES_CODIGO","Codigo","","Incidentes_Codigo","int",0,"px",6,6,"right",null,[],1241,"Incidentes_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1242,101,"INCIDENTES_DATA","","","Incidentes_Data","dtime",0,"px",14,14,"right",null,[],1242,"Incidentes_Data",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1239,102,"INCIDENTES_DEMANDACOD","Cod","","Incidentes_DemandaCod","int",0,"px",6,6,"right",null,[],1239,"Incidentes_DemandaCod",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1240,103,"INCIDENTES_DEMANDANUM","","","Incidentes_DemandaNum","svchar",0,"px",30,30,"left",null,[],1240,"Incidentes_DemandaNum",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 107, 20, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_INCIDENTES_DATAContainer = gx.uc.getNew(this, 117, 20, "BootstrapDropDownOptions", "DDO_INCIDENTES_DATAContainer", "Ddo_incidentes_data");
   var DDO_INCIDENTES_DATAContainer = this.DDO_INCIDENTES_DATAContainer;
   DDO_INCIDENTES_DATAContainer.setProp("Icon", "Icon", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("Caption", "Caption", "", "str");
   DDO_INCIDENTES_DATAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_INCIDENTES_DATAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_INCIDENTES_DATAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_INCIDENTES_DATAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_INCIDENTES_DATAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_INCIDENTES_DATAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_INCIDENTES_DATAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_INCIDENTES_DATAContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_INCIDENTES_DATAContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_INCIDENTES_DATAContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_INCIDENTES_DATAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_INCIDENTES_DATAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_INCIDENTES_DATAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_INCIDENTES_DATAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_INCIDENTES_DATAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_INCIDENTES_DATAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_INCIDENTES_DATAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_INCIDENTES_DATAContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_INCIDENTES_DATAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_INCIDENTES_DATAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_INCIDENTES_DATAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_INCIDENTES_DATAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_INCIDENTES_DATAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_INCIDENTES_DATAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_INCIDENTES_DATAContainer.addV2CFunction('AV46DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_INCIDENTES_DATAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV46DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV46DDO_TitleSettingsIcons); });
   DDO_INCIDENTES_DATAContainer.addV2CFunction('AV36Incidentes_DataTitleFilterData', "vINCIDENTES_DATATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_INCIDENTES_DATAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV36Incidentes_DataTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vINCIDENTES_DATATITLEFILTERDATA",UC.ParentObject.AV36Incidentes_DataTitleFilterData); });
   DDO_INCIDENTES_DATAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_INCIDENTES_DATAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_INCIDENTES_DATAContainer.setProp("Class", "Class", "", "char");
   DDO_INCIDENTES_DATAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_INCIDENTES_DATAContainer.addEventHandler("OnOptionClicked", this.e12j42_client);
   this.setUserControl(DDO_INCIDENTES_DATAContainer);
   this.DDO_INCIDENTES_DEMANDANUMContainer = gx.uc.getNew(this, 119, 20, "BootstrapDropDownOptions", "DDO_INCIDENTES_DEMANDANUMContainer", "Ddo_incidentes_demandanum");
   var DDO_INCIDENTES_DEMANDANUMContainer = this.DDO_INCIDENTES_DEMANDANUMContainer;
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Icon", "Icon", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Caption", "Caption", "", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("DataListProc", "Datalistproc", "GetWWContagemResultadoIncidentesFilterData", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_INCIDENTES_DEMANDANUMContainer.addV2CFunction('AV46DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_INCIDENTES_DEMANDANUMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV46DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV46DDO_TitleSettingsIcons); });
   DDO_INCIDENTES_DEMANDANUMContainer.addV2CFunction('AV42Incidentes_DemandaNumTitleFilterData', "vINCIDENTES_DEMANDANUMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_INCIDENTES_DEMANDANUMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV42Incidentes_DemandaNumTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vINCIDENTES_DEMANDANUMTITLEFILTERDATA",UC.ParentObject.AV42Incidentes_DemandaNumTitleFilterData); });
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_INCIDENTES_DEMANDANUMContainer.setProp("Class", "Class", "", "char");
   DDO_INCIDENTES_DEMANDANUMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_INCIDENTES_DEMANDANUMContainer.addEventHandler("OnOptionClicked", this.e13j42_client);
   this.setUserControl(DDO_INCIDENTES_DEMANDANUMContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 106, 20, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV48GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV48GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV48GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV49GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV49GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV49GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11j42_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"CONTAGEMRESULTADOINCIDENTESTITLE", format:0,grid:0};
   GXValidFnc[13]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[16]={fld:"INSERT",grid:0};
   GXValidFnc[18]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[20]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV13OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[21]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[26]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[28]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[31]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[35]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[37]={fld:"TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1",grid:0};
   GXValidFnc[40]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Incidentes_data1,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DATA1",gxz:"ZV16Incidentes_Data1",gxold:"OV16Incidentes_Data1",gxvar:"AV16Incidentes_Data1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[40],ip:[40],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV16Incidentes_Data1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16Incidentes_Data1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DATA1",gx.O.AV16Incidentes_Data1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV16Incidentes_Data1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vINCIDENTES_DATA1")},nac:gx.falseFn};
   GXValidFnc[42]={fld:"DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[44]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Incidentes_data_to1,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DATA_TO1",gxz:"ZV17Incidentes_Data_To1",gxold:"OV17Incidentes_Data_To1",gxvar:"AV17Incidentes_Data_To1",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[44],ip:[44],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17Incidentes_Data_To1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Incidentes_Data_To1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DATA_TO1",gx.O.AV17Incidentes_Data_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Incidentes_Data_To1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vINCIDENTES_DATA_TO1")},nac:gx.falseFn};
   GXValidFnc[45]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DEMANDANUM1",gxz:"ZV18Incidentes_DemandaNum1",gxold:"OV18Incidentes_DemandaNum1",gxvar:"AV18Incidentes_DemandaNum1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18Incidentes_DemandaNum1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18Incidentes_DemandaNum1=Value},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DEMANDANUM1",gx.O.AV18Incidentes_DemandaNum1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV18Incidentes_DemandaNum1=this.val()},val:function(){return gx.fn.getControlValue("vINCIDENTES_DEMANDANUM1")},nac:gx.falseFn};
   GXValidFnc[47]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[48]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[51]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[53]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV20DynamicFiltersSelector2",gxold:"OV20DynamicFiltersSelector2",gxvar:"AV20DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV20DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV20DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV20DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV20DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[55]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[57]={fld:"TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2",grid:0};
   GXValidFnc[60]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Incidentes_data2,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DATA2",gxz:"ZV21Incidentes_Data2",gxold:"OV21Incidentes_Data2",gxvar:"AV21Incidentes_Data2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[60],ip:[60],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21Incidentes_Data2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21Incidentes_Data2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DATA2",gx.O.AV21Incidentes_Data2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21Incidentes_Data2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vINCIDENTES_DATA2")},nac:gx.falseFn};
   GXValidFnc[62]={fld:"DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[64]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Incidentes_data_to2,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DATA_TO2",gxz:"ZV22Incidentes_Data_To2",gxold:"OV22Incidentes_Data_To2",gxvar:"AV22Incidentes_Data_To2",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[64],ip:[64],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV22Incidentes_Data_To2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22Incidentes_Data_To2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DATA_TO2",gx.O.AV22Incidentes_Data_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV22Incidentes_Data_To2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vINCIDENTES_DATA_TO2")},nac:gx.falseFn};
   GXValidFnc[65]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DEMANDANUM2",gxz:"ZV23Incidentes_DemandaNum2",gxold:"OV23Incidentes_DemandaNum2",gxvar:"AV23Incidentes_DemandaNum2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23Incidentes_DemandaNum2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23Incidentes_DemandaNum2=Value},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DEMANDANUM2",gx.O.AV23Incidentes_DemandaNum2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23Incidentes_DemandaNum2=this.val()},val:function(){return gx.fn.getControlValue("vINCIDENTES_DEMANDANUM2")},nac:gx.falseFn};
   GXValidFnc[67]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[68]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[71]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[73]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV25DynamicFiltersSelector3",gxold:"OV25DynamicFiltersSelector3",gxvar:"AV25DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV25DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV25DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV25DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV25DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[75]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[77]={fld:"TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3",grid:0};
   GXValidFnc[80]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Incidentes_data3,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DATA3",gxz:"ZV26Incidentes_Data3",gxold:"OV26Incidentes_Data3",gxvar:"AV26Incidentes_Data3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[80],ip:[80],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV26Incidentes_Data3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV26Incidentes_Data3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DATA3",gx.O.AV26Incidentes_Data3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV26Incidentes_Data3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vINCIDENTES_DATA3")},nac:gx.falseFn};
   GXValidFnc[82]={fld:"DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[84]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Incidentes_data_to3,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DATA_TO3",gxz:"ZV27Incidentes_Data_To3",gxold:"OV27Incidentes_Data_To3",gxvar:"AV27Incidentes_Data_To3",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[84],ip:[84],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV27Incidentes_Data_To3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV27Incidentes_Data_To3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DATA_TO3",gx.O.AV27Incidentes_Data_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV27Incidentes_Data_To3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vINCIDENTES_DATA_TO3")},nac:gx.falseFn};
   GXValidFnc[85]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vINCIDENTES_DEMANDANUM3",gxz:"ZV28Incidentes_DemandaNum3",gxold:"OV28Incidentes_DemandaNum3",gxvar:"AV28Incidentes_DemandaNum3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV28Incidentes_DemandaNum3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28Incidentes_DemandaNum3=Value},v2c:function(){gx.fn.setControlValue("vINCIDENTES_DEMANDANUM3",gx.O.AV28Incidentes_DemandaNum3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV28Incidentes_DemandaNum3=this.val()},val:function(){return gx.fn.getControlValue("vINCIDENTES_DEMANDANUM3")},nac:gx.falseFn};
   GXValidFnc[87]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[88]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[91]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[94]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[98]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:97,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV31Update",gxold:"OV31Update",gxvar:"AV31Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV31Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV31Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(97),gx.O.AV31Update,gx.O.AV70Update_GXI)},c2v:function(){gx.O.AV70Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV31Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(97))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(97))}, gxvar_GXI:'AV70Update_GXI',nac:gx.falseFn};
   GXValidFnc[99]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:97,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV32Delete",gxold:"OV32Delete",gxvar:"AV32Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV32Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(97),gx.O.AV32Delete,gx.O.AV71Delete_GXI)},c2v:function(){gx.O.AV71Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV32Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(97))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(97))}, gxvar_GXI:'AV71Delete_GXI',nac:gx.falseFn};
   GXValidFnc[100]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:97,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"INCIDENTES_CODIGO",gxz:"Z1241Incidentes_Codigo",gxold:"O1241Incidentes_Codigo",gxvar:"A1241Incidentes_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1241Incidentes_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1241Incidentes_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("INCIDENTES_CODIGO",row || gx.fn.currentGridRowImpl(97),gx.O.A1241Incidentes_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1241Incidentes_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("INCIDENTES_CODIGO",row || gx.fn.currentGridRowImpl(97),'.')},nac:gx.falseFn};
   GXValidFnc[101]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:97,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"INCIDENTES_DATA",gxz:"Z1242Incidentes_Data",gxold:"O1242Incidentes_Data",gxvar:"A1242Incidentes_Data",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1242Incidentes_Data=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1242Incidentes_Data=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("INCIDENTES_DATA",row || gx.fn.currentGridRowImpl(97),gx.O.A1242Incidentes_Data,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1242Incidentes_Data=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("INCIDENTES_DATA",row || gx.fn.currentGridRowImpl(97))},nac:gx.falseFn};
   GXValidFnc[102]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:97,gxgrid:this.GridContainer,fnc:this.Valid_Incidentes_demandacod,isvalid:null,rgrid:[],fld:"INCIDENTES_DEMANDACOD",gxz:"Z1239Incidentes_DemandaCod",gxold:"O1239Incidentes_DemandaCod",gxvar:"A1239Incidentes_DemandaCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1239Incidentes_DemandaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1239Incidentes_DemandaCod=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("INCIDENTES_DEMANDACOD",row || gx.fn.currentGridRowImpl(97),gx.O.A1239Incidentes_DemandaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1239Incidentes_DemandaCod=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("INCIDENTES_DEMANDACOD",row || gx.fn.currentGridRowImpl(97),'.')},nac:gx.falseFn};
   GXValidFnc[103]={lvl:2,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:97,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"INCIDENTES_DEMANDANUM",gxz:"Z1240Incidentes_DemandaNum",gxold:"O1240Incidentes_DemandaNum",gxvar:"A1240Incidentes_DemandaNum",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A1240Incidentes_DemandaNum=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1240Incidentes_DemandaNum=Value},v2c:function(row){gx.fn.setGridControlValue("INCIDENTES_DEMANDANUM",row || gx.fn.currentGridRowImpl(97),gx.O.A1240Incidentes_DemandaNum,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1240Incidentes_DemandaNum=this.val()},val:function(row){return gx.fn.getGridControlValue("INCIDENTES_DEMANDANUM",row || gx.fn.currentGridRowImpl(97))},nac:gx.falseFn};
   GXValidFnc[108]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV19DynamicFiltersEnabled2",gxold:"OV19DynamicFiltersEnabled2",gxvar:"AV19DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV19DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[109]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV24DynamicFiltersEnabled3",gxold:"OV24DynamicFiltersEnabled3",gxvar:"AV24DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV24DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV24DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV24DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[110]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfincidentes_data,isvalid:null,rgrid:[this.GridContainer],fld:"vTFINCIDENTES_DATA",gxz:"ZV37TFIncidentes_Data",gxold:"OV37TFIncidentes_Data",gxvar:"AV37TFIncidentes_Data",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[110],ip:[110],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV37TFIncidentes_Data=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV37TFIncidentes_Data=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFINCIDENTES_DATA",gx.O.AV37TFIncidentes_Data,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV37TFIncidentes_Data=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFINCIDENTES_DATA")},nac:gx.falseFn};
   GXValidFnc[111]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfincidentes_data_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFINCIDENTES_DATA_TO",gxz:"ZV38TFIncidentes_Data_To",gxold:"OV38TFIncidentes_Data_To",gxvar:"AV38TFIncidentes_Data_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[111],ip:[111],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV38TFIncidentes_Data_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38TFIncidentes_Data_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFINCIDENTES_DATA_TO",gx.O.AV38TFIncidentes_Data_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38TFIncidentes_Data_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFINCIDENTES_DATA_TO")},nac:gx.falseFn};
   GXValidFnc[112]={fld:"DDO_INCIDENTES_DATAAUXDATES",grid:0};
   GXValidFnc[113]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_incidentes_dataauxdate,isvalid:null,rgrid:[],fld:"vDDO_INCIDENTES_DATAAUXDATE",gxz:"ZV39DDO_Incidentes_DataAuxDate",gxold:"OV39DDO_Incidentes_DataAuxDate",gxvar:"AV39DDO_Incidentes_DataAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[113],ip:[113],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV39DDO_Incidentes_DataAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV39DDO_Incidentes_DataAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_INCIDENTES_DATAAUXDATE",gx.O.AV39DDO_Incidentes_DataAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV39DDO_Incidentes_DataAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_INCIDENTES_DATAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[114]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_incidentes_dataauxdateto,isvalid:null,rgrid:[],fld:"vDDO_INCIDENTES_DATAAUXDATETO",gxz:"ZV40DDO_Incidentes_DataAuxDateTo",gxold:"OV40DDO_Incidentes_DataAuxDateTo",gxvar:"AV40DDO_Incidentes_DataAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[114],ip:[114],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV40DDO_Incidentes_DataAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV40DDO_Incidentes_DataAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_INCIDENTES_DATAAUXDATETO",gx.O.AV40DDO_Incidentes_DataAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40DDO_Incidentes_DataAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_INCIDENTES_DATAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[115]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFINCIDENTES_DEMANDANUM",gxz:"ZV43TFIncidentes_DemandaNum",gxold:"OV43TFIncidentes_DemandaNum",gxvar:"AV43TFIncidentes_DemandaNum",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43TFIncidentes_DemandaNum=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV43TFIncidentes_DemandaNum=Value},v2c:function(){gx.fn.setControlValue("vTFINCIDENTES_DEMANDANUM",gx.O.AV43TFIncidentes_DemandaNum,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43TFIncidentes_DemandaNum=this.val()},val:function(){return gx.fn.getControlValue("vTFINCIDENTES_DEMANDANUM")},nac:gx.falseFn};
   GXValidFnc[116]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFINCIDENTES_DEMANDANUM_SEL",gxz:"ZV44TFIncidentes_DemandaNum_Sel",gxold:"OV44TFIncidentes_DemandaNum_Sel",gxvar:"AV44TFIncidentes_DemandaNum_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44TFIncidentes_DemandaNum_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV44TFIncidentes_DemandaNum_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFINCIDENTES_DEMANDANUM_SEL",gx.O.AV44TFIncidentes_DemandaNum_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44TFIncidentes_DemandaNum_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFINCIDENTES_DEMANDANUM_SEL")},nac:gx.falseFn};
   GXValidFnc[118]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE",gxz:"ZV41ddo_Incidentes_DataTitleControlIdToReplace",gxold:"OV41ddo_Incidentes_DataTitleControlIdToReplace",gxvar:"AV41ddo_Incidentes_DataTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV41ddo_Incidentes_DataTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV41ddo_Incidentes_DataTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE",gx.O.AV41ddo_Incidentes_DataTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV41ddo_Incidentes_DataTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[120]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE",gxz:"ZV45ddo_Incidentes_DemandaNumTitleControlIdToReplace",gxold:"OV45ddo_Incidentes_DemandaNumTitleControlIdToReplace",gxvar:"AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV45ddo_Incidentes_DemandaNumTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE",gx.O.AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16Incidentes_Data1 = gx.date.nullDate() ;
   this.ZV16Incidentes_Data1 = gx.date.nullDate() ;
   this.OV16Incidentes_Data1 = gx.date.nullDate() ;
   this.AV17Incidentes_Data_To1 = gx.date.nullDate() ;
   this.ZV17Incidentes_Data_To1 = gx.date.nullDate() ;
   this.OV17Incidentes_Data_To1 = gx.date.nullDate() ;
   this.AV18Incidentes_DemandaNum1 = "" ;
   this.ZV18Incidentes_DemandaNum1 = "" ;
   this.OV18Incidentes_DemandaNum1 = "" ;
   this.AV20DynamicFiltersSelector2 = "" ;
   this.ZV20DynamicFiltersSelector2 = "" ;
   this.OV20DynamicFiltersSelector2 = "" ;
   this.AV21Incidentes_Data2 = gx.date.nullDate() ;
   this.ZV21Incidentes_Data2 = gx.date.nullDate() ;
   this.OV21Incidentes_Data2 = gx.date.nullDate() ;
   this.AV22Incidentes_Data_To2 = gx.date.nullDate() ;
   this.ZV22Incidentes_Data_To2 = gx.date.nullDate() ;
   this.OV22Incidentes_Data_To2 = gx.date.nullDate() ;
   this.AV23Incidentes_DemandaNum2 = "" ;
   this.ZV23Incidentes_DemandaNum2 = "" ;
   this.OV23Incidentes_DemandaNum2 = "" ;
   this.AV25DynamicFiltersSelector3 = "" ;
   this.ZV25DynamicFiltersSelector3 = "" ;
   this.OV25DynamicFiltersSelector3 = "" ;
   this.AV26Incidentes_Data3 = gx.date.nullDate() ;
   this.ZV26Incidentes_Data3 = gx.date.nullDate() ;
   this.OV26Incidentes_Data3 = gx.date.nullDate() ;
   this.AV27Incidentes_Data_To3 = gx.date.nullDate() ;
   this.ZV27Incidentes_Data_To3 = gx.date.nullDate() ;
   this.OV27Incidentes_Data_To3 = gx.date.nullDate() ;
   this.AV28Incidentes_DemandaNum3 = "" ;
   this.ZV28Incidentes_DemandaNum3 = "" ;
   this.OV28Incidentes_DemandaNum3 = "" ;
   this.ZV31Update = "" ;
   this.OV31Update = "" ;
   this.ZV32Delete = "" ;
   this.OV32Delete = "" ;
   this.Z1241Incidentes_Codigo = 0 ;
   this.O1241Incidentes_Codigo = 0 ;
   this.Z1242Incidentes_Data = gx.date.nullDate() ;
   this.O1242Incidentes_Data = gx.date.nullDate() ;
   this.Z1239Incidentes_DemandaCod = 0 ;
   this.O1239Incidentes_DemandaCod = 0 ;
   this.Z1240Incidentes_DemandaNum = "" ;
   this.O1240Incidentes_DemandaNum = "" ;
   this.AV19DynamicFiltersEnabled2 = false ;
   this.ZV19DynamicFiltersEnabled2 = false ;
   this.OV19DynamicFiltersEnabled2 = false ;
   this.AV24DynamicFiltersEnabled3 = false ;
   this.ZV24DynamicFiltersEnabled3 = false ;
   this.OV24DynamicFiltersEnabled3 = false ;
   this.AV37TFIncidentes_Data = gx.date.nullDate() ;
   this.ZV37TFIncidentes_Data = gx.date.nullDate() ;
   this.OV37TFIncidentes_Data = gx.date.nullDate() ;
   this.AV38TFIncidentes_Data_To = gx.date.nullDate() ;
   this.ZV38TFIncidentes_Data_To = gx.date.nullDate() ;
   this.OV38TFIncidentes_Data_To = gx.date.nullDate() ;
   this.AV39DDO_Incidentes_DataAuxDate = gx.date.nullDate() ;
   this.ZV39DDO_Incidentes_DataAuxDate = gx.date.nullDate() ;
   this.OV39DDO_Incidentes_DataAuxDate = gx.date.nullDate() ;
   this.AV40DDO_Incidentes_DataAuxDateTo = gx.date.nullDate() ;
   this.ZV40DDO_Incidentes_DataAuxDateTo = gx.date.nullDate() ;
   this.OV40DDO_Incidentes_DataAuxDateTo = gx.date.nullDate() ;
   this.AV43TFIncidentes_DemandaNum = "" ;
   this.ZV43TFIncidentes_DemandaNum = "" ;
   this.OV43TFIncidentes_DemandaNum = "" ;
   this.AV44TFIncidentes_DemandaNum_Sel = "" ;
   this.ZV44TFIncidentes_DemandaNum_Sel = "" ;
   this.OV44TFIncidentes_DemandaNum_Sel = "" ;
   this.AV41ddo_Incidentes_DataTitleControlIdToReplace = "" ;
   this.ZV41ddo_Incidentes_DataTitleControlIdToReplace = "" ;
   this.OV41ddo_Incidentes_DataTitleControlIdToReplace = "" ;
   this.AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = "" ;
   this.ZV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = "" ;
   this.OV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = "" ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16Incidentes_Data1 = gx.date.nullDate() ;
   this.AV17Incidentes_Data_To1 = gx.date.nullDate() ;
   this.AV18Incidentes_DemandaNum1 = "" ;
   this.AV20DynamicFiltersSelector2 = "" ;
   this.AV21Incidentes_Data2 = gx.date.nullDate() ;
   this.AV22Incidentes_Data_To2 = gx.date.nullDate() ;
   this.AV23Incidentes_DemandaNum2 = "" ;
   this.AV25DynamicFiltersSelector3 = "" ;
   this.AV26Incidentes_Data3 = gx.date.nullDate() ;
   this.AV27Incidentes_Data_To3 = gx.date.nullDate() ;
   this.AV28Incidentes_DemandaNum3 = "" ;
   this.AV48GridCurrentPage = 0 ;
   this.AV19DynamicFiltersEnabled2 = false ;
   this.AV24DynamicFiltersEnabled3 = false ;
   this.AV37TFIncidentes_Data = gx.date.nullDate() ;
   this.AV38TFIncidentes_Data_To = gx.date.nullDate() ;
   this.AV39DDO_Incidentes_DataAuxDate = gx.date.nullDate() ;
   this.AV40DDO_Incidentes_DataAuxDateTo = gx.date.nullDate() ;
   this.AV43TFIncidentes_DemandaNum = "" ;
   this.AV44TFIncidentes_DemandaNum_Sel = "" ;
   this.AV46DDO_TitleSettingsIcons = {} ;
   this.AV41ddo_Incidentes_DataTitleControlIdToReplace = "" ;
   this.AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace = "" ;
   this.AV31Update = "" ;
   this.AV32Delete = "" ;
   this.A1241Incidentes_Codigo = 0 ;
   this.A1242Incidentes_Data = gx.date.nullDate() ;
   this.A1239Incidentes_DemandaCod = 0 ;
   this.A1240Incidentes_DemandaNum = "" ;
   this.AV72Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV30DynamicFiltersIgnoreFirst = false ;
   this.AV29DynamicFiltersRemoving = false ;
   this.Events = {"e11j42_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12j42_client": ["DDO_INCIDENTES_DATA.ONOPTIONCLICKED", true] ,"e13j42_client": ["DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED", true] ,"e14j42_client": ["VORDEREDBY.CLICK", true] ,"e15j42_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e16j42_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e17j42_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e18j42_client": ["'DOCLEANFILTERS'", true] ,"e19j42_client": ["'DOINSERT'", true] ,"e20j42_client": ["'ADDDYNAMICFILTERS1'", true] ,"e21j42_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e22j42_client": ["'ADDDYNAMICFILTERS2'", true] ,"e23j42_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e24j42_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e28j42_client": ["ENTER", true] ,"e29j42_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV36Incidentes_DataTitleFilterData',fld:'vINCIDENTES_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV42Incidentes_DemandaNumTitleFilterData',fld:'vINCIDENTES_DEMANDANUMTITLEFILTERDATA',pic:'',nv:null},{ctrl:'INCIDENTES_DATA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("INCIDENTES_DATA","Title")',ctrl:'INCIDENTES_DATA',prop:'Title'},{ctrl:'INCIDENTES_DEMANDANUM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("INCIDENTES_DEMANDANUM","Title")',ctrl:'INCIDENTES_DEMANDANUM',prop:'Title'},{av:'AV48GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV49GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_INCIDENTES_DATA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_INCIDENTES_DATAContainer.ActiveEventKey',ctrl:'DDO_INCIDENTES_DATA',prop:'ActiveEventKey'},{av:'this.DDO_INCIDENTES_DATAContainer.FilteredText_get',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredText_get'},{av:'this.DDO_INCIDENTES_DATAContainer.FilteredTextTo_get',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_INCIDENTES_DATAContainer.SortedStatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.SortedStatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'}]];
   this.EvtParms["DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.ActiveEventKey',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'ActiveEventKey'},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.FilteredText_get',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'FilteredText_get'},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.SelectedValue_get',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SelectedValue_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.SortedStatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'this.DDO_INCIDENTES_DATAContainer.SortedStatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'gx.fn.getCtrlProperty("INCIDENTES_DATA","Link")',ctrl:'INCIDENTES_DATA',prop:'Link'}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible")',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible")',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible")',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible")',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible")',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible")',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible")',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible")',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible")',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible")',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible")',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible")',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV41ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV37TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_INCIDENTES_DATAContainer.FilteredText_set',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredText_set'},{av:'AV38TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_INCIDENTES_DATAContainer.FilteredTextTo_set',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredTextTo_set'},{av:'AV43TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.FilteredText_set',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'FilteredText_set'},{av:'AV44TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'this.DDO_INCIDENTES_DEMANDANUMContainer.SelectedValue_set',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM1","Visible")',ctrl:'vINCIDENTES_DEMANDANUM1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV22Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV27Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Incidentes_DemandaNum1',fld:'vINCIDENTES_DEMANDANUM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Incidentes_DemandaNum2',fld:'vINCIDENTES_DEMANDANUM2',pic:'@!',nv:''},{av:'AV28Incidentes_DemandaNum3',fld:'vINCIDENTES_DEMANDANUM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM2","Visible")',ctrl:'vINCIDENTES_DEMANDANUM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vINCIDENTES_DEMANDANUM3","Visible")',ctrl:'vINCIDENTES_DEMANDANUM3',prop:'Visible'}]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.setVCMap("AV72Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV30DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV29DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("AV72Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV30DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV29DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   GridContainer.addRefreshingVar(this.GXValidFnc[20]);
   GridContainer.addRefreshingVar(this.GXValidFnc[21]);
   GridContainer.addRefreshingVar(this.GXValidFnc[33]);
   GridContainer.addRefreshingVar(this.GXValidFnc[40]);
   GridContainer.addRefreshingVar(this.GXValidFnc[44]);
   GridContainer.addRefreshingVar(this.GXValidFnc[45]);
   GridContainer.addRefreshingVar(this.GXValidFnc[53]);
   GridContainer.addRefreshingVar(this.GXValidFnc[60]);
   GridContainer.addRefreshingVar(this.GXValidFnc[64]);
   GridContainer.addRefreshingVar(this.GXValidFnc[65]);
   GridContainer.addRefreshingVar(this.GXValidFnc[73]);
   GridContainer.addRefreshingVar(this.GXValidFnc[80]);
   GridContainer.addRefreshingVar(this.GXValidFnc[84]);
   GridContainer.addRefreshingVar(this.GXValidFnc[85]);
   GridContainer.addRefreshingVar(this.GXValidFnc[108]);
   GridContainer.addRefreshingVar(this.GXValidFnc[109]);
   GridContainer.addRefreshingVar(this.GXValidFnc[110]);
   GridContainer.addRefreshingVar(this.GXValidFnc[111]);
   GridContainer.addRefreshingVar(this.GXValidFnc[115]);
   GridContainer.addRefreshingVar(this.GXValidFnc[116]);
   GridContainer.addRefreshingVar(this.GXValidFnc[118]);
   GridContainer.addRefreshingVar(this.GXValidFnc[120]);
   GridContainer.addRefreshingVar({rfrVar:"AV72Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV30DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV29DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A1241Incidentes_Codigo", rfrProp:"Value", gxAttId:"1241"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwcontagemresultadoincidentes);
