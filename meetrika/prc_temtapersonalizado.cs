/*
               File: PRC_TemTAPersonalizado
        Description: Tem TAPersonalizado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:48.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temtapersonalizado : GXProcedure
   {
      public prc_temtapersonalizado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temtapersonalizado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out bool aP1_TemTAPersonalizado )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8TemTAPersonalizado = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_TemTAPersonalizado=this.AV8TemTAPersonalizado;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8TemTAPersonalizado = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_TemTAPersonalizado=this.AV8TemTAPersonalizado;
         return AV8TemTAPersonalizado ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out bool aP1_TemTAPersonalizado )
      {
         prc_temtapersonalizado objprc_temtapersonalizado;
         objprc_temtapersonalizado = new prc_temtapersonalizado();
         objprc_temtapersonalizado.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_temtapersonalizado.AV8TemTAPersonalizado = false ;
         objprc_temtapersonalizado.context.SetSubmitInitialConfig(context);
         objprc_temtapersonalizado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temtapersonalizado);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_TemTAPersonalizado=this.AV8TemTAPersonalizado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temtapersonalizado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XM2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2081AreaTrabalho_VerTA = P00XM2_A2081AreaTrabalho_VerTA[0];
            n2081AreaTrabalho_VerTA = P00XM2_n2081AreaTrabalho_VerTA[0];
            AV8TemTAPersonalizado = (bool)(((A2081AreaTrabalho_VerTA>0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XM2_A5AreaTrabalho_Codigo = new int[1] ;
         P00XM2_A2081AreaTrabalho_VerTA = new short[1] ;
         P00XM2_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temtapersonalizado__default(),
            new Object[][] {
                new Object[] {
               P00XM2_A5AreaTrabalho_Codigo, P00XM2_A2081AreaTrabalho_VerTA, P00XM2_n2081AreaTrabalho_VerTA
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2081AreaTrabalho_VerTA ;
      private int A5AreaTrabalho_Codigo ;
      private String scmdbuf ;
      private bool AV8TemTAPersonalizado ;
      private bool n2081AreaTrabalho_VerTA ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00XM2_A5AreaTrabalho_Codigo ;
      private short[] P00XM2_A2081AreaTrabalho_VerTA ;
      private bool[] P00XM2_n2081AreaTrabalho_VerTA ;
      private bool aP1_TemTAPersonalizado ;
   }

   public class prc_temtapersonalizado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XM2 ;
          prmP00XM2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XM2", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_VerTA] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XM2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
