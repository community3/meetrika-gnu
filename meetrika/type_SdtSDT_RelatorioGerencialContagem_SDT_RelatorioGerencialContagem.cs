/*
               File: type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem
        Description: SDT_RelatorioGerencialContagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:6.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem" )]
   [XmlType(TypeName =  "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS ))]
   [Serializable]
   public class SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem : GxUserType
   {
      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom = "";
      }

      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_contratadaorigemcod = deserialized.gxTpr_Contagemresultado_contratadaorigemcod;
         obj.gxTpr_Contagemresultado_contratadaorigemsigla = deserialized.gxTpr_Contagemresultado_contratadaorigemsigla;
         obj.gxTpr_Contagemresultado_contratadaorigempescod = deserialized.gxTpr_Contagemresultado_contratadaorigempescod;
         obj.gxTpr_Contagemresultado_contratadaorigempesnom = deserialized.gxTpr_Contagemresultado_contratadaorigempesnom;
         obj.gxTpr_Os = deserialized.gxTpr_Os;
         obj.gxTpr_Totalos = deserialized.gxTpr_Totalos;
         obj.gxTpr_Totalosdivergentes = deserialized.gxTpr_Totalosdivergentes;
         obj.gxTpr_Percentualosdivergentes = deserialized.gxTpr_Percentualosdivergentes;
         obj.gxTpr_Totalpfdivergentes = deserialized.gxTpr_Totalpfdivergentes;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemSigla") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemPesCod") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemPesNom") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OS") )
               {
                  if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os == null )
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os.readxmlcollection(oReader, "OS", "OS");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TotalOS") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalos = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TotalOSDivergentes") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalosdivergentes = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PercentualOSDivergentes") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TotalPFDivergentes") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemSigla", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemPesNom", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os.writexmlcollection(oWriter, "OS", sNameSpace1, "OS", sNameSpace1);
         }
         oWriter.WriteElement("TotalOS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalos), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TotalOSDivergentes", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalosdivergentes), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PercentualOSDivergentes", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TotalPFDivergentes", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultado_ContratadaOrigemCod", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemcod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemSigla", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemPesCod", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempescod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemPesNom", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom, false);
         if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os != null )
         {
            AddObjectProperty("OS", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os, false);
         }
         AddObjectProperty("TotalOS", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalos, false);
         AddObjectProperty("TotalOSDivergentes", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalosdivergentes, false);
         AddObjectProperty("PercentualOSDivergentes", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes, false);
         AddObjectProperty("TotalPFDivergentes", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes, false);
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemcod ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemSigla" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemSigla"   )]
      public String gxTpr_Contagemresultado_contratadaorigemsigla
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemPesCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemPesCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigempescod
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempescod ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempescod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemPesNom" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemPesNom"   )]
      public String gxTpr_Contagemresultado_contratadaorigempesnom
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom = (String)(value);
         }

      }

      public class gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_80compatibility:SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS {}
      [  SoapElement( ElementName = "OS" )]
      [  XmlArray( ElementName = "OS"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS ), ElementName= "OS"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_80compatibility ), ElementName= "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Os_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os == null )
            {
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os ;
         }

         set {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os == null )
            {
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS", "GeneXus.Programs");
            }
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Os
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os == null )
            {
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = value;
         }

      }

      public void gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os_SetNull( )
      {
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os_IsNull( )
      {
         if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "TotalOS" )]
      [  XmlElement( ElementName = "TotalOS"   )]
      public short gxTpr_Totalos
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalos ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalos = (short)(value);
         }

      }

      [  SoapElement( ElementName = "TotalOSDivergentes" )]
      [  XmlElement( ElementName = "TotalOSDivergentes"   )]
      public short gxTpr_Totalosdivergentes
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalosdivergentes ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalosdivergentes = (short)(value);
         }

      }

      [  SoapElement( ElementName = "PercentualOSDivergentes" )]
      [  XmlElement( ElementName = "PercentualOSDivergentes"   )]
      public double gxTpr_Percentualosdivergentes_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Percentualosdivergentes
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "TotalPFDivergentes" )]
      [  XmlElement( ElementName = "TotalPFDivergentes"   )]
      public double gxTpr_Totalpfdivergentes_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Totalpfdivergentes
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes = (decimal)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalos ;
      protected short gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalosdivergentes ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemcod ;
      protected int gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempescod ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Percentualosdivergentes ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Totalpfdivergentes ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigemsigla ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Contagemresultado_contratadaorigempesnom ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS ))]
      protected IGxCollection gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_Os=null ;
   }

   [DataContract(Name = @"SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_RESTInterface : GxGenericCollectionItem<SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_RESTInterface( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemCod" , Order = 0 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigemcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemSigla" , Order = 1 )]
      public String gxTpr_Contagemresultado_contratadaorigemsigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadaorigemsigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemsigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemPesCod" , Order = 2 )]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigempescod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigempescod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigempescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemPesNom" , Order = 3 )]
      public String gxTpr_Contagemresultado_contratadaorigempesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contratadaorigempesnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigempesnom = (String)(value);
         }

      }

      [DataMember( Name = "OS" , Order = 4 )]
      public GxGenericCollection<SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_RESTInterface> gxTpr_Os
      {
         get {
            return new GxGenericCollection<SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_RESTInterface>(sdt.gxTpr_Os) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Os);
         }

      }

      [DataMember( Name = "TotalOS" , Order = 5 )]
      public Nullable<short> gxTpr_Totalos
      {
         get {
            return sdt.gxTpr_Totalos ;
         }

         set {
            sdt.gxTpr_Totalos = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TotalOSDivergentes" , Order = 6 )]
      public Nullable<short> gxTpr_Totalosdivergentes
      {
         get {
            return sdt.gxTpr_Totalosdivergentes ;
         }

         set {
            sdt.gxTpr_Totalosdivergentes = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "PercentualOSDivergentes" , Order = 7 )]
      public Nullable<decimal> gxTpr_Percentualosdivergentes
      {
         get {
            return sdt.gxTpr_Percentualosdivergentes ;
         }

         set {
            sdt.gxTpr_Percentualosdivergentes = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TotalPFDivergentes" , Order = 8 )]
      public String gxTpr_Totalpfdivergentes
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Totalpfdivergentes, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Totalpfdivergentes = NumberUtil.Val( (String)(value), ".");
         }

      }

      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem sdt
      {
         get {
            return (SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem() ;
         }
      }

   }

}
