/*
               File: PRC_TemUsuarios
        Description: Contratada Tem Usu�rios?
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:29.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temusuarios : GXProcedure
   {
      public prc_temusuarios( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temusuarios( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratadaUsuario_ContratadaCod ,
                           out bool aP1_Tem )
      {
         this.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         this.AV8Tem = false ;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_ContratadaCod=this.A66ContratadaUsuario_ContratadaCod;
         aP1_Tem=this.AV8Tem;
      }

      public bool executeUdp( ref int aP0_ContratadaUsuario_ContratadaCod )
      {
         this.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         this.AV8Tem = false ;
         initialize();
         executePrivate();
         aP0_ContratadaUsuario_ContratadaCod=this.A66ContratadaUsuario_ContratadaCod;
         aP1_Tem=this.AV8Tem;
         return AV8Tem ;
      }

      public void executeSubmit( ref int aP0_ContratadaUsuario_ContratadaCod ,
                                 out bool aP1_Tem )
      {
         prc_temusuarios objprc_temusuarios;
         objprc_temusuarios = new prc_temusuarios();
         objprc_temusuarios.A66ContratadaUsuario_ContratadaCod = aP0_ContratadaUsuario_ContratadaCod;
         objprc_temusuarios.AV8Tem = false ;
         objprc_temusuarios.context.SetSubmitInitialConfig(context);
         objprc_temusuarios.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temusuarios);
         aP0_ContratadaUsuario_ContratadaCod=this.A66ContratadaUsuario_ContratadaCod;
         aP1_Tem=this.AV8Tem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temusuarios)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AR2 */
         pr_default.execute(0, new Object[] {A66ContratadaUsuario_ContratadaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = P00AR2_A69ContratadaUsuario_UsuarioCod[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00AR2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00AR2_n1394ContratadaUsuario_UsuarioAtivo[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00AR2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00AR2_n1394ContratadaUsuario_UsuarioAtivo[0];
            AV8Tem = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AR2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00AR2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00AR2_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00AR2_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temusuarios__default(),
            new Object[][] {
                new Object[] {
               P00AR2_A69ContratadaUsuario_UsuarioCod, P00AR2_A66ContratadaUsuario_ContratadaCod, P00AR2_A1394ContratadaUsuario_UsuarioAtivo, P00AR2_n1394ContratadaUsuario_UsuarioAtivo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private String scmdbuf ;
      private bool AV8Tem ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratadaUsuario_ContratadaCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00AR2_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00AR2_A66ContratadaUsuario_ContratadaCod ;
      private bool[] P00AR2_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P00AR2_n1394ContratadaUsuario_UsuarioAtivo ;
      private bool aP1_Tem ;
   }

   public class prc_temusuarios__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AR2 ;
          prmP00AR2 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AR2", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AR2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
