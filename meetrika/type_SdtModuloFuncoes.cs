/*
               File: type_SdtModuloFuncoes
        Description: Modulo Funcoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:55.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ModuloFuncoes" )]
   [XmlType(TypeName =  "ModuloFuncoes" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtModuloFuncoes : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtModuloFuncoes( )
      {
         /* Constructor for serialization */
         gxTv_SdtModuloFuncoes_Modulo_descricao = "";
         gxTv_SdtModuloFuncoes_Funcaousuario_nome = "";
         gxTv_SdtModuloFuncoes_Mode = "";
         gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z = "";
      }

      public SdtModuloFuncoes( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV146Modulo_Codigo ,
                        int AV161FuncaoUsuario_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV146Modulo_Codigo,(int)AV161FuncaoUsuario_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Modulo_Codigo", typeof(int)}, new Object[]{"FuncaoUsuario_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ModuloFuncoes");
         metadata.Set("BT", "ModuloFuncoes1");
         metadata.Set("PK", "[ \"Modulo_Codigo\",\"FuncaoUsuario_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"FuncaoUsuario_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Modulo_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaousuario_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaousuario_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_descricao_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtModuloFuncoes deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtModuloFuncoes)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtModuloFuncoes obj ;
         obj = this;
         obj.gxTpr_Modulo_codigo = deserialized.gxTpr_Modulo_codigo;
         obj.gxTpr_Modulo_descricao = deserialized.gxTpr_Modulo_descricao;
         obj.gxTpr_Funcaousuario_codigo = deserialized.gxTpr_Funcaousuario_codigo;
         obj.gxTpr_Funcaousuario_nome = deserialized.gxTpr_Funcaousuario_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Modulo_codigo_Z = deserialized.gxTpr_Modulo_codigo_Z;
         obj.gxTpr_Funcaousuario_codigo_Z = deserialized.gxTpr_Funcaousuario_codigo_Z;
         obj.gxTpr_Funcaousuario_nome_Z = deserialized.gxTpr_Funcaousuario_nome_Z;
         obj.gxTpr_Modulo_descricao_N = deserialized.gxTpr_Modulo_descricao_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo") )
               {
                  gxTv_SdtModuloFuncoes_Modulo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Descricao") )
               {
                  gxTv_SdtModuloFuncoes_Modulo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Codigo") )
               {
                  gxTv_SdtModuloFuncoes_Funcaousuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Nome") )
               {
                  gxTv_SdtModuloFuncoes_Funcaousuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtModuloFuncoes_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtModuloFuncoes_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo_Z") )
               {
                  gxTv_SdtModuloFuncoes_Modulo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Codigo_Z") )
               {
                  gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Nome_Z") )
               {
                  gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Descricao_N") )
               {
                  gxTv_SdtModuloFuncoes_Modulo_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ModuloFuncoes";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Modulo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModuloFuncoes_Modulo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Modulo_Descricao", StringUtil.RTrim( gxTv_SdtModuloFuncoes_Modulo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FuncaoUsuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModuloFuncoes_Funcaousuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FuncaoUsuario_Nome", StringUtil.RTrim( gxTv_SdtModuloFuncoes_Funcaousuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtModuloFuncoes_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModuloFuncoes_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Modulo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModuloFuncoes_Modulo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("FuncaoUsuario_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("FuncaoUsuario_Nome_Z", StringUtil.RTrim( gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Modulo_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtModuloFuncoes_Modulo_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Modulo_Codigo", gxTv_SdtModuloFuncoes_Modulo_codigo, false);
         AddObjectProperty("Modulo_Descricao", gxTv_SdtModuloFuncoes_Modulo_descricao, false);
         AddObjectProperty("FuncaoUsuario_Codigo", gxTv_SdtModuloFuncoes_Funcaousuario_codigo, false);
         AddObjectProperty("FuncaoUsuario_Nome", gxTv_SdtModuloFuncoes_Funcaousuario_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtModuloFuncoes_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtModuloFuncoes_Initialized, false);
            AddObjectProperty("Modulo_Codigo_Z", gxTv_SdtModuloFuncoes_Modulo_codigo_Z, false);
            AddObjectProperty("FuncaoUsuario_Codigo_Z", gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z, false);
            AddObjectProperty("FuncaoUsuario_Nome_Z", gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z, false);
            AddObjectProperty("Modulo_Descricao_N", gxTv_SdtModuloFuncoes_Modulo_descricao_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo" )]
      [  XmlElement( ElementName = "Modulo_Codigo"   )]
      public int gxTpr_Modulo_codigo
      {
         get {
            return gxTv_SdtModuloFuncoes_Modulo_codigo ;
         }

         set {
            if ( gxTv_SdtModuloFuncoes_Modulo_codigo != value )
            {
               gxTv_SdtModuloFuncoes_Mode = "INS";
               this.gxTv_SdtModuloFuncoes_Modulo_codigo_Z_SetNull( );
               this.gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z_SetNull( );
               this.gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z_SetNull( );
            }
            gxTv_SdtModuloFuncoes_Modulo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo_Descricao" )]
      [  XmlElement( ElementName = "Modulo_Descricao"   )]
      public String gxTpr_Modulo_descricao
      {
         get {
            return gxTv_SdtModuloFuncoes_Modulo_descricao ;
         }

         set {
            gxTv_SdtModuloFuncoes_Modulo_descricao_N = 0;
            gxTv_SdtModuloFuncoes_Modulo_descricao = (String)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Modulo_descricao_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Modulo_descricao_N = 1;
         gxTv_SdtModuloFuncoes_Modulo_descricao = "";
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Modulo_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoUsuario_Codigo" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Codigo"   )]
      public int gxTpr_Funcaousuario_codigo
      {
         get {
            return gxTv_SdtModuloFuncoes_Funcaousuario_codigo ;
         }

         set {
            if ( gxTv_SdtModuloFuncoes_Funcaousuario_codigo != value )
            {
               gxTv_SdtModuloFuncoes_Mode = "INS";
               this.gxTv_SdtModuloFuncoes_Modulo_codigo_Z_SetNull( );
               this.gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z_SetNull( );
               this.gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z_SetNull( );
            }
            gxTv_SdtModuloFuncoes_Funcaousuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "FuncaoUsuario_Nome" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Nome"   )]
      public String gxTpr_Funcaousuario_nome
      {
         get {
            return gxTv_SdtModuloFuncoes_Funcaousuario_nome ;
         }

         set {
            gxTv_SdtModuloFuncoes_Funcaousuario_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtModuloFuncoes_Mode ;
         }

         set {
            gxTv_SdtModuloFuncoes_Mode = (String)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Mode_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Mode = "";
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtModuloFuncoes_Initialized ;
         }

         set {
            gxTv_SdtModuloFuncoes_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Initialized_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo_Z" )]
      [  XmlElement( ElementName = "Modulo_Codigo_Z"   )]
      public int gxTpr_Modulo_codigo_Z
      {
         get {
            return gxTv_SdtModuloFuncoes_Modulo_codigo_Z ;
         }

         set {
            gxTv_SdtModuloFuncoes_Modulo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Modulo_codigo_Z_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Modulo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Modulo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoUsuario_Codigo_Z" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Codigo_Z"   )]
      public int gxTpr_Funcaousuario_codigo_Z
      {
         get {
            return gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z ;
         }

         set {
            gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoUsuario_Nome_Z" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Nome_Z"   )]
      public String gxTpr_Funcaousuario_nome_Z
      {
         get {
            return gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z ;
         }

         set {
            gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Descricao_N" )]
      [  XmlElement( ElementName = "Modulo_Descricao_N"   )]
      public short gxTpr_Modulo_descricao_N
      {
         get {
            return gxTv_SdtModuloFuncoes_Modulo_descricao_N ;
         }

         set {
            gxTv_SdtModuloFuncoes_Modulo_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtModuloFuncoes_Modulo_descricao_N_SetNull( )
      {
         gxTv_SdtModuloFuncoes_Modulo_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtModuloFuncoes_Modulo_descricao_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtModuloFuncoes_Modulo_descricao = "";
         gxTv_SdtModuloFuncoes_Funcaousuario_nome = "";
         gxTv_SdtModuloFuncoes_Mode = "";
         gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "modulofuncoes", "GeneXus.Programs.modulofuncoes_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtModuloFuncoes_Initialized ;
      private short gxTv_SdtModuloFuncoes_Modulo_descricao_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtModuloFuncoes_Modulo_codigo ;
      private int gxTv_SdtModuloFuncoes_Funcaousuario_codigo ;
      private int gxTv_SdtModuloFuncoes_Modulo_codigo_Z ;
      private int gxTv_SdtModuloFuncoes_Funcaousuario_codigo_Z ;
      private String gxTv_SdtModuloFuncoes_Mode ;
      private String sTagName ;
      private String gxTv_SdtModuloFuncoes_Modulo_descricao ;
      private String gxTv_SdtModuloFuncoes_Funcaousuario_nome ;
      private String gxTv_SdtModuloFuncoes_Funcaousuario_nome_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ModuloFuncoes", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtModuloFuncoes_RESTInterface : GxGenericCollectionItem<SdtModuloFuncoes>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtModuloFuncoes_RESTInterface( ) : base()
      {
      }

      public SdtModuloFuncoes_RESTInterface( SdtModuloFuncoes psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Modulo_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Modulo_codigo
      {
         get {
            return sdt.gxTpr_Modulo_codigo ;
         }

         set {
            sdt.gxTpr_Modulo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Modulo_Descricao" , Order = 1 )]
      public String gxTpr_Modulo_descricao
      {
         get {
            return sdt.gxTpr_Modulo_descricao ;
         }

         set {
            sdt.gxTpr_Modulo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "FuncaoUsuario_Codigo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaousuario_codigo
      {
         get {
            return sdt.gxTpr_Funcaousuario_codigo ;
         }

         set {
            sdt.gxTpr_Funcaousuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoUsuario_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Funcaousuario_nome
      {
         get {
            return sdt.gxTpr_Funcaousuario_nome ;
         }

         set {
            sdt.gxTpr_Funcaousuario_nome = (String)(value);
         }

      }

      public SdtModuloFuncoes sdt
      {
         get {
            return (SdtModuloFuncoes)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtModuloFuncoes() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 10 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
