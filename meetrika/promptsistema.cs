/*
               File: PromptSistema
        Description: Selecione Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:31.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptsistema : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutSistema_Codigo ,
                           ref String aP1_InOutSistema_Descricao )
      {
         this.AV7InOutSistema_Codigo = aP0_InOutSistema_Codigo;
         this.AV8InOutSistema_Descricao = aP1_InOutSistema_Descricao;
         executePrivate();
         aP0_InOutSistema_Codigo=this.AV7InOutSistema_Codigo;
         aP1_InOutSistema_Descricao=this.AV8InOutSistema_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkavSistema_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_50 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_50_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_50_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV66Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV68Sistema_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome1", AV68Sistema_Nome1);
               AV70TFSistema_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFSistema_Nome", AV70TFSistema_Nome);
               AV71TFSistema_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFSistema_Nome_Sel", AV71TFSistema_Nome_Sel);
               AV74TFSistema_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFSistema_Sigla", AV74TFSistema_Sigla);
               AV75TFSistema_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFSistema_Sigla_Sel", AV75TFSistema_Sigla_Sel);
               AV78TFAmbienteTecnologico_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFAmbienteTecnologico_Descricao", AV78TFAmbienteTecnologico_Descricao);
               AV79TFAmbienteTecnologico_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAmbienteTecnologico_Descricao_Sel", AV79TFAmbienteTecnologico_Descricao_Sel);
               AV82TFMetodologia_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologia_Descricao", AV82TFMetodologia_Descricao);
               AV83TFMetodologia_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMetodologia_Descricao_Sel", AV83TFMetodologia_Descricao_Sel);
               AV72ddo_Sistema_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Sistema_NomeTitleControlIdToReplace", AV72ddo_Sistema_NomeTitleControlIdToReplace);
               AV76ddo_Sistema_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Sistema_SiglaTitleControlIdToReplace", AV76ddo_Sistema_SiglaTitleControlIdToReplace);
               AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
               AV84ddo_Metodologia_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Metodologia_DescricaoTitleControlIdToReplace", AV84ddo_Metodologia_DescricaoTitleControlIdToReplace);
               AV67Sistema_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Ativo", AV67Sistema_Ativo);
               AV92Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV66Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV68Sistema_Nome1, AV70TFSistema_Nome, AV71TFSistema_Nome_Sel, AV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, AV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, AV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, AV72ddo_Sistema_NomeTitleControlIdToReplace, AV76ddo_Sistema_SiglaTitleControlIdToReplace, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, AV67Sistema_Ativo, AV92Pgmname, AV10GridState) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptSistema";
               forbiddenHiddens = forbiddenHiddens + A128Sistema_Descricao;
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptsistema:[SendSecurityCheck value for]"+"Sistema_Descricao:"+A128Sistema_Descricao);
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutSistema_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSistema_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutSistema_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSistema_Descricao", AV8InOutSistema_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA3H2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV92Pgmname = "PromptSistema";
               context.Gx_err = 0;
               WS3H2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE3H2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020326912323");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptsistema.aspx") + "?" + UrlEncode("" +AV7InOutSistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutSistema_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_NOME1", AV68Sistema_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_NOME", AV70TFSistema_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_NOME_SEL", AV71TFSistema_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_SIGLA", StringUtil.RTrim( AV74TFSistema_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_SIGLA_SEL", StringUtil.RTrim( AV75TFSistema_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO", AV78TFAmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL", AV79TFAmbienteTecnologico_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIA_DESCRICAO", AV82TFMetodologia_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIA_DESCRICAO_SEL", AV83TFMetodologia_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_50", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_50), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV85DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV85DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_NOMETITLEFILTERDATA", AV69Sistema_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_NOMETITLEFILTERDATA", AV69Sistema_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_SIGLATITLEFILTERDATA", AV73Sistema_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_SIGLATITLEFILTERDATA", AV73Sistema_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV77AmbienteTecnologico_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV77AmbienteTecnologico_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIA_DESCRICAOTITLEFILTERDATA", AV81Metodologia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIA_DESCRICAOTITLEFILTERDATA", AV81Metodologia_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV92Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "vINOUTSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSISTEMA_DESCRICAO", AV8InOutSistema_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Caption", StringUtil.RTrim( Ddo_sistema_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Tooltip", StringUtil.RTrim( Ddo_sistema_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Cls", StringUtil.RTrim( Ddo_sistema_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_sistema_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_sistema_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Filtertype", StringUtil.RTrim( Ddo_sistema_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Datalisttype", StringUtil.RTrim( Ddo_sistema_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Datalistproc", StringUtil.RTrim( Ddo_sistema_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Sortasc", StringUtil.RTrim( Ddo_sistema_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Sortdsc", StringUtil.RTrim( Ddo_sistema_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Loadingdata", StringUtil.RTrim( Ddo_sistema_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_sistema_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_sistema_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Caption", StringUtil.RTrim( Ddo_sistema_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Tooltip", StringUtil.RTrim( Ddo_sistema_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Cls", StringUtil.RTrim( Ddo_sistema_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_sistema_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_sistema_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filtertype", StringUtil.RTrim( Ddo_sistema_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_sistema_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_sistema_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Sortasc", StringUtil.RTrim( Ddo_sistema_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_sistema_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_sistema_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_sistema_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_sistema_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_metodologia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_metodologia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_metodologia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_metodologia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_metodologia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_metodologia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_metodologia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_metodologia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_metodologia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_metodologia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_metodologia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_metodologia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_metodologia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_metodologia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_metodologia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_metodologia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_metodologia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_sistema_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_sistema_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_metodologia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_metodologia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_metodologia_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptSistema";
         forbiddenHiddens = forbiddenHiddens + A128Sistema_Descricao;
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptsistema:[SendSecurityCheck value for]"+"Sistema_Descricao:"+A128Sistema_Descricao);
      }

      protected void RenderHtmlCloseForm3H2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Sistema" ;
      }

      protected void WB3H0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_3H2( true) ;
         }
         else
         {
            wb_table1_2_3H2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3H2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistema_Descricao_Internalname, A128Sistema_Descricao, "", "", 0, edtSistema_Descricao_Visible, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_PromptSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_nome_Internalname, AV70TFSistema_Nome, StringUtil.RTrim( context.localUtil.Format( AV70TFSistema_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_nome_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_nome_sel_Internalname, AV71TFSistema_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV71TFSistema_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_nome_sel_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_sigla_Internalname, StringUtil.RTrim( AV74TFSistema_Sigla), StringUtil.RTrim( context.localUtil.Format( AV74TFSistema_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_sigla_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_sigla_sel_Internalname, StringUtil.RTrim( AV75TFSistema_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV75TFSistema_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_sigla_sel_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_Internalname, AV78TFAmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( AV78TFAmbienteTecnologico_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_sel_Internalname, AV79TFAmbienteTecnologico_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV79TFAmbienteTecnologico_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologia_descricao_Internalname, AV82TFMetodologia_Descricao, StringUtil.RTrim( context.localUtil.Format( AV82TFMetodologia_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologia_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologia_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologia_descricao_sel_Internalname, AV83TFMetodologia_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV83TFMetodologia_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologia_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologia_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_50_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_nometitlecontrolidtoreplace_Internalname, AV72ddo_Sistema_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDdo_sistema_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_50_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname, AV76ddo_Sistema_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_50_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_50_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", 0, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSistema.htm");
         }
         wbLoad = true;
      }

      protected void START3H2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3H0( ) ;
      }

      protected void WS3H2( )
      {
         START3H2( ) ;
         EVT3H2( ) ;
      }

      protected void EVT3H2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113H2 */
                           E113H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123H2 */
                           E123H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_SIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E133H2 */
                           E133H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E143H2 */
                           E143H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E153H2 */
                           E153H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E163H2 */
                           E163H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E173H2 */
                           E173H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E183H2 */
                           E183H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_50_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_50_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_50_idx), 4, 0)), 4, "0");
                           SubsflControlProps_502( ) ;
                           AV37Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV37Select)) ? AV91Select_GXI : context.convertURL( context.PathToRelativeUrl( AV37Select))));
                           A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                           A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
                           A129Sistema_Sigla = StringUtil.Upper( cgiGet( edtSistema_Sigla_Internalname));
                           A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
                           A138Metodologia_Descricao = StringUtil.Upper( cgiGet( edtMetodologia_Descricao_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E193H2 */
                                 E193H2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E203H2 */
                                 E203H2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E213H2 */
                                 E213H2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Sistema_areatrabalhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV66Sistema_AreaTrabalhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Sistema_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_NOME1"), AV68Sistema_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsistema_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_NOME"), AV70TFSistema_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsistema_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_NOME_SEL"), AV71TFSistema_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsistema_sigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA"), AV74TFSistema_Sigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsistema_sigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA_SEL"), AV75TFSistema_Sigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV78TFAmbienteTecnologico_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfambientetecnologico_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV79TFAmbienteTecnologico_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfmetodologia_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO"), AV82TFMetodologia_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfmetodologia_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO_SEL"), AV83TFMetodologia_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E223H2 */
                                       E223H2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE3H2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3H2( ) ;
            }
         }
      }

      protected void PA3H2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            chkavSistema_ativo.Name = "vSISTEMA_ATIVO";
            chkavSistema_ativo.WebTags = "";
            chkavSistema_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSistema_ativo_Internalname, "TitleCaption", chkavSistema_ativo.Caption);
            chkavSistema_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SISTEMA_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_502( ) ;
         while ( nGXsfl_50_idx <= nRC_GXsfl_50 )
         {
            sendrow_502( ) ;
            nGXsfl_50_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_50_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_50_idx+1));
            sGXsfl_50_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_50_idx), 4, 0)), 4, "0");
            SubsflControlProps_502( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV66Sistema_AreaTrabalhoCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV68Sistema_Nome1 ,
                                       String AV70TFSistema_Nome ,
                                       String AV71TFSistema_Nome_Sel ,
                                       String AV74TFSistema_Sigla ,
                                       String AV75TFSistema_Sigla_Sel ,
                                       String AV78TFAmbienteTecnologico_Descricao ,
                                       String AV79TFAmbienteTecnologico_Descricao_Sel ,
                                       String AV82TFMetodologia_Descricao ,
                                       String AV83TFMetodologia_Descricao_Sel ,
                                       String AV72ddo_Sistema_NomeTitleControlIdToReplace ,
                                       String AV76ddo_Sistema_SiglaTitleControlIdToReplace ,
                                       String AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ,
                                       String AV84ddo_Metodologia_DescricaoTitleControlIdToReplace ,
                                       bool AV67Sistema_Ativo ,
                                       String AV92Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3H2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3H2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV92Pgmname = "PromptSistema";
         context.Gx_err = 0;
      }

      protected void RF3H2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 50;
         /* Execute user event: E203H2 */
         E203H2 ();
         nGXsfl_50_idx = 1;
         sGXsfl_50_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_50_idx), 4, 0)), 4, "0");
         SubsflControlProps_502( ) ;
         nGXsfl_50_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_502( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV66Sistema_AreaTrabalhoCod ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV68Sistema_Nome1 ,
                                                 AV71TFSistema_Nome_Sel ,
                                                 AV70TFSistema_Nome ,
                                                 AV75TFSistema_Sigla_Sel ,
                                                 AV74TFSistema_Sigla ,
                                                 AV79TFAmbienteTecnologico_Descricao_Sel ,
                                                 AV78TFAmbienteTecnologico_Descricao ,
                                                 AV83TFMetodologia_Descricao_Sel ,
                                                 AV82TFMetodologia_Descricao ,
                                                 A135Sistema_AreaTrabalhoCod ,
                                                 A416Sistema_Nome ,
                                                 A129Sistema_Sigla ,
                                                 A352AmbienteTecnologico_Descricao ,
                                                 A138Metodologia_Descricao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A130Sistema_Ativo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            lV68Sistema_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV68Sistema_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome1", AV68Sistema_Nome1);
            lV70TFSistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV70TFSistema_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFSistema_Nome", AV70TFSistema_Nome);
            lV74TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV74TFSistema_Sigla), 25, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFSistema_Sigla", AV74TFSistema_Sigla);
            lV78TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV78TFAmbienteTecnologico_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFAmbienteTecnologico_Descricao", AV78TFAmbienteTecnologico_Descricao);
            lV82TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV82TFMetodologia_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologia_Descricao", AV82TFMetodologia_Descricao);
            /* Using cursor H003H2 */
            pr_default.execute(0, new Object[] {AV66Sistema_AreaTrabalhoCod, lV68Sistema_Nome1, lV70TFSistema_Nome, AV71TFSistema_Nome_Sel, lV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, lV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, lV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_50_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A137Metodologia_Codigo = H003H2_A137Metodologia_Codigo[0];
               n137Metodologia_Codigo = H003H2_n137Metodologia_Codigo[0];
               A351AmbienteTecnologico_Codigo = H003H2_A351AmbienteTecnologico_Codigo[0];
               n351AmbienteTecnologico_Codigo = H003H2_n351AmbienteTecnologico_Codigo[0];
               A135Sistema_AreaTrabalhoCod = H003H2_A135Sistema_AreaTrabalhoCod[0];
               A130Sistema_Ativo = H003H2_A130Sistema_Ativo[0];
               A128Sistema_Descricao = H003H2_A128Sistema_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
               n128Sistema_Descricao = H003H2_n128Sistema_Descricao[0];
               A138Metodologia_Descricao = H003H2_A138Metodologia_Descricao[0];
               A352AmbienteTecnologico_Descricao = H003H2_A352AmbienteTecnologico_Descricao[0];
               A129Sistema_Sigla = H003H2_A129Sistema_Sigla[0];
               A416Sistema_Nome = H003H2_A416Sistema_Nome[0];
               A127Sistema_Codigo = H003H2_A127Sistema_Codigo[0];
               A138Metodologia_Descricao = H003H2_A138Metodologia_Descricao[0];
               A352AmbienteTecnologico_Descricao = H003H2_A352AmbienteTecnologico_Descricao[0];
               /* Execute user event: E213H2 */
               E213H2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 50;
            WB3H0( ) ;
         }
         nGXsfl_50_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV66Sistema_AreaTrabalhoCod ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV68Sistema_Nome1 ,
                                              AV71TFSistema_Nome_Sel ,
                                              AV70TFSistema_Nome ,
                                              AV75TFSistema_Sigla_Sel ,
                                              AV74TFSistema_Sigla ,
                                              AV79TFAmbienteTecnologico_Descricao_Sel ,
                                              AV78TFAmbienteTecnologico_Descricao ,
                                              AV83TFMetodologia_Descricao_Sel ,
                                              AV82TFMetodologia_Descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A130Sistema_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV68Sistema_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV68Sistema_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome1", AV68Sistema_Nome1);
         lV70TFSistema_Nome = StringUtil.Concat( StringUtil.RTrim( AV70TFSistema_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFSistema_Nome", AV70TFSistema_Nome);
         lV74TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV74TFSistema_Sigla), 25, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFSistema_Sigla", AV74TFSistema_Sigla);
         lV78TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV78TFAmbienteTecnologico_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFAmbienteTecnologico_Descricao", AV78TFAmbienteTecnologico_Descricao);
         lV82TFMetodologia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV82TFMetodologia_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologia_Descricao", AV82TFMetodologia_Descricao);
         /* Using cursor H003H3 */
         pr_default.execute(1, new Object[] {AV66Sistema_AreaTrabalhoCod, lV68Sistema_Nome1, lV70TFSistema_Nome, AV71TFSistema_Nome_Sel, lV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, lV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, lV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel});
         GRID_nRecordCount = H003H3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV66Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV68Sistema_Nome1, AV70TFSistema_Nome, AV71TFSistema_Nome_Sel, AV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, AV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, AV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, AV72ddo_Sistema_NomeTitleControlIdToReplace, AV76ddo_Sistema_SiglaTitleControlIdToReplace, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, AV67Sistema_Ativo, AV92Pgmname, AV10GridState) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV66Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV68Sistema_Nome1, AV70TFSistema_Nome, AV71TFSistema_Nome_Sel, AV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, AV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, AV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, AV72ddo_Sistema_NomeTitleControlIdToReplace, AV76ddo_Sistema_SiglaTitleControlIdToReplace, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, AV67Sistema_Ativo, AV92Pgmname, AV10GridState) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV66Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV68Sistema_Nome1, AV70TFSistema_Nome, AV71TFSistema_Nome_Sel, AV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, AV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, AV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, AV72ddo_Sistema_NomeTitleControlIdToReplace, AV76ddo_Sistema_SiglaTitleControlIdToReplace, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, AV67Sistema_Ativo, AV92Pgmname, AV10GridState) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV66Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV68Sistema_Nome1, AV70TFSistema_Nome, AV71TFSistema_Nome_Sel, AV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, AV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, AV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, AV72ddo_Sistema_NomeTitleControlIdToReplace, AV76ddo_Sistema_SiglaTitleControlIdToReplace, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, AV67Sistema_Ativo, AV92Pgmname, AV10GridState) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV66Sistema_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV68Sistema_Nome1, AV70TFSistema_Nome, AV71TFSistema_Nome_Sel, AV74TFSistema_Sigla, AV75TFSistema_Sigla_Sel, AV78TFAmbienteTecnologico_Descricao, AV79TFAmbienteTecnologico_Descricao_Sel, AV82TFMetodologia_Descricao, AV83TFMetodologia_Descricao_Sel, AV72ddo_Sistema_NomeTitleControlIdToReplace, AV76ddo_Sistema_SiglaTitleControlIdToReplace, AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, AV67Sistema_Ativo, AV92Pgmname, AV10GridState) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3H0( )
      {
         /* Before Start, stand alone formulas. */
         AV92Pgmname = "PromptSistema";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E193H2 */
         E193H2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV85DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_NOMETITLEFILTERDATA"), AV69Sistema_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_SIGLATITLEFILTERDATA"), AV73Sistema_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA"), AV77AmbienteTecnologico_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIA_DESCRICAOTITLEFILTERDATA"), AV81Metodologia_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_AREATRABALHOCOD");
               GX_FocusControl = edtavSistema_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66Sistema_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV66Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavSistema_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0)));
            }
            AV67Sistema_Ativo = StringUtil.StrToBool( cgiGet( chkavSistema_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Ativo", AV67Sistema_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV68Sistema_Nome1 = StringUtil.Upper( cgiGet( edtavSistema_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome1", AV68Sistema_Nome1);
            A128Sistema_Descricao = cgiGet( edtSistema_Descricao_Internalname);
            n128Sistema_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
            AV70TFSistema_Nome = StringUtil.Upper( cgiGet( edtavTfsistema_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFSistema_Nome", AV70TFSistema_Nome);
            AV71TFSistema_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFSistema_Nome_Sel", AV71TFSistema_Nome_Sel);
            AV74TFSistema_Sigla = StringUtil.Upper( cgiGet( edtavTfsistema_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFSistema_Sigla", AV74TFSistema_Sigla);
            AV75TFSistema_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFSistema_Sigla_Sel", AV75TFSistema_Sigla_Sel);
            AV78TFAmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFAmbienteTecnologico_Descricao", AV78TFAmbienteTecnologico_Descricao);
            AV79TFAmbienteTecnologico_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAmbienteTecnologico_Descricao_Sel", AV79TFAmbienteTecnologico_Descricao_Sel);
            AV82TFMetodologia_Descricao = StringUtil.Upper( cgiGet( edtavTfmetodologia_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologia_Descricao", AV82TFMetodologia_Descricao);
            AV83TFMetodologia_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfmetodologia_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMetodologia_Descricao_Sel", AV83TFMetodologia_Descricao_Sel);
            AV72ddo_Sistema_NomeTitleControlIdToReplace = cgiGet( edtavDdo_sistema_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Sistema_NomeTitleControlIdToReplace", AV72ddo_Sistema_NomeTitleControlIdToReplace);
            AV76ddo_Sistema_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Sistema_SiglaTitleControlIdToReplace", AV76ddo_Sistema_SiglaTitleControlIdToReplace);
            AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
            AV84ddo_Metodologia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Metodologia_DescricaoTitleControlIdToReplace", AV84ddo_Metodologia_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_50 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_50"), ",", "."));
            AV87GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV88GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_sistema_nome_Caption = cgiGet( "DDO_SISTEMA_NOME_Caption");
            Ddo_sistema_nome_Tooltip = cgiGet( "DDO_SISTEMA_NOME_Tooltip");
            Ddo_sistema_nome_Cls = cgiGet( "DDO_SISTEMA_NOME_Cls");
            Ddo_sistema_nome_Filteredtext_set = cgiGet( "DDO_SISTEMA_NOME_Filteredtext_set");
            Ddo_sistema_nome_Selectedvalue_set = cgiGet( "DDO_SISTEMA_NOME_Selectedvalue_set");
            Ddo_sistema_nome_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_NOME_Dropdownoptionstype");
            Ddo_sistema_nome_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_NOME_Titlecontrolidtoreplace");
            Ddo_sistema_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_NOME_Includesortasc"));
            Ddo_sistema_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_NOME_Includesortdsc"));
            Ddo_sistema_nome_Sortedstatus = cgiGet( "DDO_SISTEMA_NOME_Sortedstatus");
            Ddo_sistema_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_NOME_Includefilter"));
            Ddo_sistema_nome_Filtertype = cgiGet( "DDO_SISTEMA_NOME_Filtertype");
            Ddo_sistema_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_NOME_Filterisrange"));
            Ddo_sistema_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_NOME_Includedatalist"));
            Ddo_sistema_nome_Datalisttype = cgiGet( "DDO_SISTEMA_NOME_Datalisttype");
            Ddo_sistema_nome_Datalistproc = cgiGet( "DDO_SISTEMA_NOME_Datalistproc");
            Ddo_sistema_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_nome_Sortasc = cgiGet( "DDO_SISTEMA_NOME_Sortasc");
            Ddo_sistema_nome_Sortdsc = cgiGet( "DDO_SISTEMA_NOME_Sortdsc");
            Ddo_sistema_nome_Loadingdata = cgiGet( "DDO_SISTEMA_NOME_Loadingdata");
            Ddo_sistema_nome_Cleanfilter = cgiGet( "DDO_SISTEMA_NOME_Cleanfilter");
            Ddo_sistema_nome_Noresultsfound = cgiGet( "DDO_SISTEMA_NOME_Noresultsfound");
            Ddo_sistema_nome_Searchbuttontext = cgiGet( "DDO_SISTEMA_NOME_Searchbuttontext");
            Ddo_sistema_sigla_Caption = cgiGet( "DDO_SISTEMA_SIGLA_Caption");
            Ddo_sistema_sigla_Tooltip = cgiGet( "DDO_SISTEMA_SIGLA_Tooltip");
            Ddo_sistema_sigla_Cls = cgiGet( "DDO_SISTEMA_SIGLA_Cls");
            Ddo_sistema_sigla_Filteredtext_set = cgiGet( "DDO_SISTEMA_SIGLA_Filteredtext_set");
            Ddo_sistema_sigla_Selectedvalue_set = cgiGet( "DDO_SISTEMA_SIGLA_Selectedvalue_set");
            Ddo_sistema_sigla_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_SIGLA_Dropdownoptionstype");
            Ddo_sistema_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_SIGLA_Titlecontrolidtoreplace");
            Ddo_sistema_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includesortasc"));
            Ddo_sistema_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includesortdsc"));
            Ddo_sistema_sigla_Sortedstatus = cgiGet( "DDO_SISTEMA_SIGLA_Sortedstatus");
            Ddo_sistema_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includefilter"));
            Ddo_sistema_sigla_Filtertype = cgiGet( "DDO_SISTEMA_SIGLA_Filtertype");
            Ddo_sistema_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Filterisrange"));
            Ddo_sistema_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includedatalist"));
            Ddo_sistema_sigla_Datalisttype = cgiGet( "DDO_SISTEMA_SIGLA_Datalisttype");
            Ddo_sistema_sigla_Datalistproc = cgiGet( "DDO_SISTEMA_SIGLA_Datalistproc");
            Ddo_sistema_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_sigla_Sortasc = cgiGet( "DDO_SISTEMA_SIGLA_Sortasc");
            Ddo_sistema_sigla_Sortdsc = cgiGet( "DDO_SISTEMA_SIGLA_Sortdsc");
            Ddo_sistema_sigla_Loadingdata = cgiGet( "DDO_SISTEMA_SIGLA_Loadingdata");
            Ddo_sistema_sigla_Cleanfilter = cgiGet( "DDO_SISTEMA_SIGLA_Cleanfilter");
            Ddo_sistema_sigla_Noresultsfound = cgiGet( "DDO_SISTEMA_SIGLA_Noresultsfound");
            Ddo_sistema_sigla_Searchbuttontext = cgiGet( "DDO_SISTEMA_SIGLA_Searchbuttontext");
            Ddo_ambientetecnologico_descricao_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption");
            Ddo_ambientetecnologico_descricao_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip");
            Ddo_ambientetecnologico_descricao_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls");
            Ddo_ambientetecnologico_descricao_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set");
            Ddo_ambientetecnologico_descricao_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set");
            Ddo_ambientetecnologico_descricao_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype");
            Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc"));
            Ddo_ambientetecnologico_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc"));
            Ddo_ambientetecnologico_descricao_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus");
            Ddo_ambientetecnologico_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter"));
            Ddo_ambientetecnologico_descricao_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype");
            Ddo_ambientetecnologico_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange"));
            Ddo_ambientetecnologico_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist"));
            Ddo_ambientetecnologico_descricao_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype");
            Ddo_ambientetecnologico_descricao_Datalistproc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc");
            Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_descricao_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc");
            Ddo_ambientetecnologico_descricao_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc");
            Ddo_ambientetecnologico_descricao_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata");
            Ddo_ambientetecnologico_descricao_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter");
            Ddo_ambientetecnologico_descricao_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound");
            Ddo_ambientetecnologico_descricao_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext");
            Ddo_metodologia_descricao_Caption = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Caption");
            Ddo_metodologia_descricao_Tooltip = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Tooltip");
            Ddo_metodologia_descricao_Cls = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Cls");
            Ddo_metodologia_descricao_Filteredtext_set = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filteredtext_set");
            Ddo_metodologia_descricao_Selectedvalue_set = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_set");
            Ddo_metodologia_descricao_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Dropdownoptionstype");
            Ddo_metodologia_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_metodologia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includesortasc"));
            Ddo_metodologia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includesortdsc"));
            Ddo_metodologia_descricao_Sortedstatus = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortedstatus");
            Ddo_metodologia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includefilter"));
            Ddo_metodologia_descricao_Filtertype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filtertype");
            Ddo_metodologia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filterisrange"));
            Ddo_metodologia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includedatalist"));
            Ddo_metodologia_descricao_Datalisttype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalisttype");
            Ddo_metodologia_descricao_Datalistproc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalistproc");
            Ddo_metodologia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_metodologia_descricao_Sortasc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortasc");
            Ddo_metodologia_descricao_Sortdsc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortdsc");
            Ddo_metodologia_descricao_Loadingdata = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Loadingdata");
            Ddo_metodologia_descricao_Cleanfilter = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Cleanfilter");
            Ddo_metodologia_descricao_Noresultsfound = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Noresultsfound");
            Ddo_metodologia_descricao_Searchbuttontext = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_sistema_nome_Activeeventkey = cgiGet( "DDO_SISTEMA_NOME_Activeeventkey");
            Ddo_sistema_nome_Filteredtext_get = cgiGet( "DDO_SISTEMA_NOME_Filteredtext_get");
            Ddo_sistema_nome_Selectedvalue_get = cgiGet( "DDO_SISTEMA_NOME_Selectedvalue_get");
            Ddo_sistema_sigla_Activeeventkey = cgiGet( "DDO_SISTEMA_SIGLA_Activeeventkey");
            Ddo_sistema_sigla_Filteredtext_get = cgiGet( "DDO_SISTEMA_SIGLA_Filteredtext_get");
            Ddo_sistema_sigla_Selectedvalue_get = cgiGet( "DDO_SISTEMA_SIGLA_Selectedvalue_get");
            Ddo_ambientetecnologico_descricao_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey");
            Ddo_ambientetecnologico_descricao_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get");
            Ddo_ambientetecnologico_descricao_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get");
            Ddo_metodologia_descricao_Activeeventkey = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Activeeventkey");
            Ddo_metodologia_descricao_Filteredtext_get = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filteredtext_get");
            Ddo_metodologia_descricao_Selectedvalue_get = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptSistema";
            A128Sistema_Descricao = cgiGet( edtSistema_Descricao_Internalname);
            n128Sistema_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128Sistema_Descricao", A128Sistema_Descricao);
            forbiddenHiddens = forbiddenHiddens + A128Sistema_Descricao;
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptsistema:[SecurityCheckFailed value for]"+"Sistema_Descricao:"+A128Sistema_Descricao);
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSISTEMA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV66Sistema_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_NOME1"), AV68Sistema_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_NOME"), AV70TFSistema_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_NOME_SEL"), AV71TFSistema_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA"), AV74TFSistema_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA_SEL"), AV75TFSistema_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV78TFAmbienteTecnologico_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV79TFAmbienteTecnologico_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO"), AV82TFMetodologia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO_SEL"), AV83TFMetodologia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E193H2 */
         E193H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E193H2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 16/03/2020 20:40", 0) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "SISTEMA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTfsistema_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_nome_Visible), 5, 0)));
         edtavTfsistema_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_nome_sel_Visible), 5, 0)));
         edtavTfsistema_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_sigla_Visible), 5, 0)));
         edtavTfsistema_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_sigla_sel_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_sel_Visible), 5, 0)));
         edtavTfmetodologia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologia_descricao_Visible), 5, 0)));
         edtavTfmetodologia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologia_descricao_sel_Visible), 5, 0)));
         Ddo_sistema_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "TitleControlIdToReplace", Ddo_sistema_nome_Titlecontrolidtoreplace);
         AV72ddo_Sistema_NomeTitleControlIdToReplace = Ddo_sistema_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Sistema_NomeTitleControlIdToReplace", AV72ddo_Sistema_NomeTitleControlIdToReplace);
         edtavDdo_sistema_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "TitleControlIdToReplace", Ddo_sistema_sigla_Titlecontrolidtoreplace);
         AV76ddo_Sistema_SiglaTitleControlIdToReplace = Ddo_sistema_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Sistema_SiglaTitleControlIdToReplace", AV76ddo_Sistema_SiglaTitleControlIdToReplace);
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace);
         AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_metodologia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Metodologia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "TitleControlIdToReplace", Ddo_metodologia_descricao_Titlecontrolidtoreplace);
         AV84ddo_Metodologia_DescricaoTitleControlIdToReplace = Ddo_metodologia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_Metodologia_DescricaoTitleControlIdToReplace", AV84ddo_Metodologia_DescricaoTitleControlIdToReplace);
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Sistema";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtSistema_Descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Descricao_Visible), 5, 0)));
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o do Sistema", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Sigla", 0);
         cmbavOrderedby.addItem("4", "Ambiente Tecnol�gico", 0);
         cmbavOrderedby.addItem("5", "Metodologia", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV85DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV85DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E203H2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV69Sistema_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Sistema_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81Metodologia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSistema_Nome_Titleformat = 2;
         edtSistema_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV72ddo_Sistema_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Nome_Internalname, "Title", edtSistema_Nome_Title);
         edtSistema_Sigla_Titleformat = 2;
         edtSistema_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV76ddo_Sistema_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Sigla_Internalname, "Title", edtSistema_Sigla_Title);
         edtAmbienteTecnologico_Descricao_Titleformat = 2;
         edtAmbienteTecnologico_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ambiente Tecnol�gico", AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Descricao_Internalname, "Title", edtAmbienteTecnologico_Descricao_Title);
         edtMetodologia_Descricao_Titleformat = 2;
         edtMetodologia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Metodologia", AV84ddo_Metodologia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologia_Descricao_Internalname, "Title", edtMetodologia_Descricao_Title);
         AV87GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87GridCurrentPage), 10, 0)));
         AV88GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69Sistema_NomeTitleFilterData", AV69Sistema_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73Sistema_SiglaTitleFilterData", AV73Sistema_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV77AmbienteTecnologico_DescricaoTitleFilterData", AV77AmbienteTecnologico_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV81Metodologia_DescricaoTitleFilterData", AV81Metodologia_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E113H2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV86PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV86PageToGo) ;
         }
      }

      protected void E123H2( )
      {
         /* Ddo_sistema_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "SortedStatus", Ddo_sistema_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "SortedStatus", Ddo_sistema_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFSistema_Nome = Ddo_sistema_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFSistema_Nome", AV70TFSistema_Nome);
            AV71TFSistema_Nome_Sel = Ddo_sistema_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFSistema_Nome_Sel", AV71TFSistema_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E133H2( )
      {
         /* Ddo_sistema_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFSistema_Sigla = Ddo_sistema_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFSistema_Sigla", AV74TFSistema_Sigla);
            AV75TFSistema_Sigla_Sel = Ddo_sistema_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFSistema_Sigla_Sel", AV75TFSistema_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E143H2( )
      {
         /* Ddo_ambientetecnologico_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV78TFAmbienteTecnologico_Descricao = Ddo_ambientetecnologico_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFAmbienteTecnologico_Descricao", AV78TFAmbienteTecnologico_Descricao);
            AV79TFAmbienteTecnologico_Descricao_Sel = Ddo_ambientetecnologico_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAmbienteTecnologico_Descricao_Sel", AV79TFAmbienteTecnologico_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E153H2( )
      {
         /* Ddo_metodologia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV82TFMetodologia_Descricao = Ddo_metodologia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologia_Descricao", AV82TFMetodologia_Descricao);
            AV83TFMetodologia_Descricao_Sel = Ddo_metodologia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMetodologia_Descricao_Sel", AV83TFMetodologia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E213H2( )
      {
         /* Grid_Load Routine */
         AV37Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV37Select);
         AV91Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 50;
         }
         sendrow_502( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_50_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(50, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E223H2 */
         E223H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E223H2( )
      {
         /* Enter Routine */
         AV7InOutSistema_Codigo = A127Sistema_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSistema_Codigo), 6, 0)));
         AV8InOutSistema_Descricao = A128Sistema_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSistema_Descricao", AV8InOutSistema_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutSistema_Codigo,(String)AV8InOutSistema_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E163H2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E183H2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E173H2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void S142( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_sistema_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "SortedStatus", Ddo_sistema_nome_Sortedstatus);
         Ddo_sistema_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         Ddo_metodologia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
      }

      protected void S122( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_sistema_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "SortedStatus", Ddo_sistema_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_sistema_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_ambientetecnologico_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_metodologia_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavSistema_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
         {
            edtavSistema_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome1_Visible), 5, 0)));
         }
      }

      protected void S152( )
      {
         /* 'CLEANFILTERS' Routine */
         AV66Sistema_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0)));
         AV67Sistema_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Ativo", AV67Sistema_Ativo);
         AV70TFSistema_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFSistema_Nome", AV70TFSistema_Nome);
         Ddo_sistema_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "FilteredText_set", Ddo_sistema_nome_Filteredtext_set);
         AV71TFSistema_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFSistema_Nome_Sel", AV71TFSistema_Nome_Sel);
         Ddo_sistema_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_nome_Internalname, "SelectedValue_set", Ddo_sistema_nome_Selectedvalue_set);
         AV74TFSistema_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFSistema_Sigla", AV74TFSistema_Sigla);
         Ddo_sistema_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "FilteredText_set", Ddo_sistema_sigla_Filteredtext_set);
         AV75TFSistema_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFSistema_Sigla_Sel", AV75TFSistema_Sigla_Sel);
         Ddo_sistema_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SelectedValue_set", Ddo_sistema_sigla_Selectedvalue_set);
         AV78TFAmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFAmbienteTecnologico_Descricao", AV78TFAmbienteTecnologico_Descricao);
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
         AV79TFAmbienteTecnologico_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFAmbienteTecnologico_Descricao_Sel", AV79TFAmbienteTecnologico_Descricao_Sel);
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
         AV82TFMetodologia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFMetodologia_Descricao", AV82TFMetodologia_Descricao);
         Ddo_metodologia_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "FilteredText_set", Ddo_metodologia_descricao_Filteredtext_set);
         AV83TFMetodologia_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMetodologia_Descricao_Sel", AV83TFMetodologia_Descricao_Sel);
         Ddo_metodologia_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SelectedValue_set", Ddo_metodologia_descricao_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SISTEMA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV68Sistema_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome1", AV68Sistema_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
            {
               AV68Sistema_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome1", AV68Sistema_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV66Sistema_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SISTEMA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (false==AV67Sistema_Ativo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SISTEMA_ATIVO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV67Sistema_Ativo);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFSistema_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFSistema_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFSistema_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFSistema_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFSistema_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFSistema_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFSistema_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFSistema_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFAmbienteTecnologico_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV78TFAmbienteTecnologico_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAmbienteTecnologico_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFAmbienteTecnologico_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFMetodologia_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV82TFMetodologia_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFMetodologia_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV83TFMetodologia_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV92Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV68Sistema_Nome1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV68Sistema_Nome1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_3H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_3H2( true) ;
         }
         else
         {
            wb_table2_5_3H2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_3H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_44_3H2( true) ;
         }
         else
         {
            wb_table3_44_3H2( false) ;
         }
         return  ;
      }

      protected void wb_table3_44_3H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3H2e( true) ;
         }
         else
         {
            wb_table1_2_3H2e( false) ;
         }
      }

      protected void wb_table3_44_3H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_47_3H2( true) ;
         }
         else
         {
            wb_table4_47_3H2( false) ;
         }
         return  ;
      }

      protected void wb_table4_47_3H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_44_3H2e( true) ;
         }
         else
         {
            wb_table3_44_3H2e( false) ;
         }
      }

      protected void wb_table4_47_3H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"50\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV37Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A416Sistema_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A129Sistema_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A352AmbienteTecnologico_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A138Metodologia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologia_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 50 )
         {
            wbEnd = 0;
            nRC_GXsfl_50 = (short)(nGXsfl_50_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_47_3H2e( true) ;
         }
         else
         {
            wb_table4_47_3H2e( false) ;
         }
      }

      protected void wb_table2_5_3H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_50_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptSistema.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_3H2( true) ;
         }
         else
         {
            wb_table5_14_3H2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_3H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3H2e( true) ;
         }
         else
         {
            wb_table2_5_3H2e( false) ;
         }
      }

      protected void wb_table5_14_3H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_areatrabalhocod_Internalname, "C�d. �rea Trabalho", "", "", lblFiltertextsistema_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66Sistema_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV66Sistema_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_ativo_Internalname, "Ativo", "", "", lblFiltertextsistema_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_50_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSistema_ativo_Internalname, StringUtil.BoolToStr( AV67Sistema_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(27, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_29_3H2( true) ;
         }
         else
         {
            wb_table6_29_3H2( false) ;
         }
         return  ;
      }

      protected void wb_table6_29_3H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_3H2e( true) ;
         }
         else
         {
            wb_table5_14_3H2e( false) ;
         }
      }

      protected void wb_table6_29_3H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_50_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_PromptSistema.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_50_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_nome1_Internalname, AV68Sistema_Nome1, StringUtil.RTrim( context.localUtil.Format( AV68Sistema_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_nome1_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_29_3H2e( true) ;
         }
         else
         {
            wb_table6_29_3H2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutSistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSistema_Codigo), 6, 0)));
         AV8InOutSistema_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSistema_Descricao", AV8InOutSistema_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3H2( ) ;
         WS3H2( ) ;
         WE3H2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203269123652");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptsistema.js", "?20203269123652");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_502( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_50_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_50_idx;
         edtSistema_Nome_Internalname = "SISTEMA_NOME_"+sGXsfl_50_idx;
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA_"+sGXsfl_50_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_50_idx;
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO_"+sGXsfl_50_idx;
      }

      protected void SubsflControlProps_fel_502( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_50_fel_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_50_fel_idx;
         edtSistema_Nome_Internalname = "SISTEMA_NOME_"+sGXsfl_50_fel_idx;
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA_"+sGXsfl_50_fel_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_50_fel_idx;
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO_"+sGXsfl_50_fel_idx;
      }

      protected void sendrow_502( )
      {
         SubsflControlProps_502( ) ;
         WB3H0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_50_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_50_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_50_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 51,'',false,'',50)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV37Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV37Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV91Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV37Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV37Select)) ? AV91Select_GXI : context.PathToRelativeUrl( AV37Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_50_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV37Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)50,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Nome_Internalname,(String)A416Sistema_Nome,StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)50,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Sigla_Internalname,StringUtil.RTrim( A129Sistema_Sigla),StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)50,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Descricao_Internalname,(String)A352AmbienteTecnologico_Descricao,StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)50,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologia_Descricao_Internalname,(String)A138Metodologia_Descricao,StringUtil.RTrim( context.localUtil.Format( A138Metodologia_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)50,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO"+"_"+sGXsfl_50_idx, GetSecureSignedToken( sGXsfl_50_idx, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME"+"_"+sGXsfl_50_idx, GetSecureSignedToken( sGXsfl_50_idx, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_SIGLA"+"_"+sGXsfl_50_idx, GetSecureSignedToken( sGXsfl_50_idx, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_50_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_50_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_50_idx+1));
            sGXsfl_50_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_50_idx), 4, 0)), 4, "0");
            SubsflControlProps_502( ) ;
         }
         /* End function sendrow_502 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextsistema_areatrabalhocod_Internalname = "FILTERTEXTSISTEMA_AREATRABALHOCOD";
         edtavSistema_areatrabalhocod_Internalname = "vSISTEMA_AREATRABALHOCOD";
         lblFiltertextsistema_ativo_Internalname = "FILTERTEXTSISTEMA_ATIVO";
         chkavSistema_ativo_Internalname = "vSISTEMA_ATIVO";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavSistema_nome1_Internalname = "vSISTEMA_NOME1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         edtSistema_Nome_Internalname = "SISTEMA_NOME";
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA";
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO";
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtSistema_Descricao_Internalname = "SISTEMA_DESCRICAO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfsistema_nome_Internalname = "vTFSISTEMA_NOME";
         edtavTfsistema_nome_sel_Internalname = "vTFSISTEMA_NOME_SEL";
         edtavTfsistema_sigla_Internalname = "vTFSISTEMA_SIGLA";
         edtavTfsistema_sigla_sel_Internalname = "vTFSISTEMA_SIGLA_SEL";
         edtavTfambientetecnologico_descricao_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO";
         edtavTfambientetecnologico_descricao_sel_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
         edtavTfmetodologia_descricao_Internalname = "vTFMETODOLOGIA_DESCRICAO";
         edtavTfmetodologia_descricao_sel_Internalname = "vTFMETODOLOGIA_DESCRICAO_SEL";
         Ddo_sistema_nome_Internalname = "DDO_SISTEMA_NOME";
         edtavDdo_sistema_nometitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_sistema_sigla_Internalname = "DDO_SISTEMA_SIGLA";
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_descricao_Internalname = "DDO_AMBIENTETECNOLOGICO_DESCRICAO";
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_metodologia_descricao_Internalname = "DDO_METODOLOGIA_DESCRICAO";
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtMetodologia_Descricao_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         edtSistema_Sigla_Jsonclick = "";
         edtSistema_Nome_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavSistema_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavSistema_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtMetodologia_Descricao_Titleformat = 0;
         edtAmbienteTecnologico_Descricao_Titleformat = 0;
         edtSistema_Sigla_Titleformat = 0;
         edtSistema_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavSistema_nome1_Visible = 1;
         edtMetodologia_Descricao_Title = "Metodologia";
         edtAmbienteTecnologico_Descricao_Title = "Ambiente Tecnol�gico";
         edtSistema_Sigla_Title = "Sigla";
         edtSistema_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavSistema_ativo.Caption = "";
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfmetodologia_descricao_sel_Jsonclick = "";
         edtavTfmetodologia_descricao_sel_Visible = 1;
         edtavTfmetodologia_descricao_Jsonclick = "";
         edtavTfmetodologia_descricao_Visible = 1;
         edtavTfambientetecnologico_descricao_sel_Jsonclick = "";
         edtavTfambientetecnologico_descricao_sel_Visible = 1;
         edtavTfambientetecnologico_descricao_Jsonclick = "";
         edtavTfambientetecnologico_descricao_Visible = 1;
         edtavTfsistema_sigla_sel_Jsonclick = "";
         edtavTfsistema_sigla_sel_Visible = 1;
         edtavTfsistema_sigla_Jsonclick = "";
         edtavTfsistema_sigla_Visible = 1;
         edtavTfsistema_nome_sel_Jsonclick = "";
         edtavTfsistema_nome_sel_Visible = 1;
         edtavTfsistema_nome_Jsonclick = "";
         edtavTfsistema_nome_Visible = 1;
         edtSistema_Descricao_Visible = 1;
         Ddo_metodologia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_metodologia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_metodologia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologia_descricao_Loadingdata = "Carregando dados...";
         Ddo_metodologia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_metodologia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_metodologia_descricao_Datalistproc = "GetPromptSistemaFilterData";
         Ddo_metodologia_descricao_Datalisttype = "Dynamic";
         Ddo_metodologia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_metodologia_descricao_Filtertype = "Character";
         Ddo_metodologia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Titlecontrolidtoreplace = "";
         Ddo_metodologia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologia_descricao_Cls = "ColumnSettings";
         Ddo_metodologia_descricao_Tooltip = "Op��es";
         Ddo_metodologia_descricao_Caption = "";
         Ddo_ambientetecnologico_descricao_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_descricao_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_descricao_Datalistproc = "GetPromptSistemaFilterData";
         Ddo_ambientetecnologico_descricao_Datalisttype = "Dynamic";
         Ddo_ambientetecnologico_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_descricao_Filtertype = "Character";
         Ddo_ambientetecnologico_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_descricao_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_descricao_Tooltip = "Op��es";
         Ddo_ambientetecnologico_descricao_Caption = "";
         Ddo_sistema_sigla_Searchbuttontext = "Pesquisar";
         Ddo_sistema_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_sigla_Loadingdata = "Carregando dados...";
         Ddo_sistema_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_sigla_Datalistproc = "GetPromptSistemaFilterData";
         Ddo_sistema_sigla_Datalisttype = "Dynamic";
         Ddo_sistema_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_sigla_Filtertype = "Character";
         Ddo_sistema_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Titlecontrolidtoreplace = "";
         Ddo_sistema_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_sigla_Cls = "ColumnSettings";
         Ddo_sistema_sigla_Tooltip = "Op��es";
         Ddo_sistema_sigla_Caption = "";
         Ddo_sistema_nome_Searchbuttontext = "Pesquisar";
         Ddo_sistema_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_nome_Loadingdata = "Carregando dados...";
         Ddo_sistema_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_nome_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_nome_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_nome_Datalistproc = "GetPromptSistemaFilterData";
         Ddo_sistema_nome_Datalisttype = "Dynamic";
         Ddo_sistema_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_nome_Filtertype = "Character";
         Ddo_sistema_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_nome_Titlecontrolidtoreplace = "";
         Ddo_sistema_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_nome_Cls = "ColumnSettings";
         Ddo_sistema_nome_Tooltip = "Op��es";
         Ddo_sistema_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Sistema";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''}],oparms:[{av:'AV69Sistema_NomeTitleFilterData',fld:'vSISTEMA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV73Sistema_SiglaTitleFilterData',fld:'vSISTEMA_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV77AmbienteTecnologico_DescricaoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV81Metodologia_DescricaoTitleFilterData',fld:'vMETODOLOGIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtSistema_Nome_Titleformat',ctrl:'SISTEMA_NOME',prop:'Titleformat'},{av:'edtSistema_Nome_Title',ctrl:'SISTEMA_NOME',prop:'Title'},{av:'edtSistema_Sigla_Titleformat',ctrl:'SISTEMA_SIGLA',prop:'Titleformat'},{av:'edtSistema_Sigla_Title',ctrl:'SISTEMA_SIGLA',prop:'Title'},{av:'edtAmbienteTecnologico_Descricao_Titleformat',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Titleformat'},{av:'edtAmbienteTecnologico_Descricao_Title',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Title'},{av:'edtMetodologia_Descricao_Titleformat',ctrl:'METODOLOGIA_DESCRICAO',prop:'Titleformat'},{av:'edtMetodologia_Descricao_Title',ctrl:'METODOLOGIA_DESCRICAO',prop:'Title'},{av:'AV87GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV88GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SISTEMA_NOME.ONOPTIONCLICKED","{handler:'E123H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Ddo_sistema_nome_Activeeventkey',ctrl:'DDO_SISTEMA_NOME',prop:'ActiveEventKey'},{av:'Ddo_sistema_nome_Filteredtext_get',ctrl:'DDO_SISTEMA_NOME',prop:'FilteredText_get'},{av:'Ddo_sistema_nome_Selectedvalue_get',ctrl:'DDO_SISTEMA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_nome_Sortedstatus',ctrl:'DDO_SISTEMA_NOME',prop:'SortedStatus'},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_SIGLA.ONOPTIONCLICKED","{handler:'E133H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Ddo_sistema_sigla_Activeeventkey',ctrl:'DDO_SISTEMA_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_sistema_sigla_Filteredtext_get',ctrl:'DDO_SISTEMA_SIGLA',prop:'FilteredText_get'},{av:'Ddo_sistema_sigla_Selectedvalue_get',ctrl:'DDO_SISTEMA_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_nome_Sortedstatus',ctrl:'DDO_SISTEMA_NOME',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED","{handler:'E143H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Ddo_ambientetecnologico_descricao_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_nome_Sortedstatus',ctrl:'DDO_SISTEMA_NOME',prop:'SortedStatus'},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_METODOLOGIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E153H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Ddo_metodologia_descricao_Activeeventkey',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_metodologia_descricao_Filteredtext_get',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_metodologia_descricao_Selectedvalue_get',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_nome_Sortedstatus',ctrl:'DDO_SISTEMA_NOME',prop:'SortedStatus'},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E213H2',iparms:[],oparms:[{av:'AV37Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E223H2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A128Sistema_Descricao',fld:'SISTEMA_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV7InOutSistema_Codigo',fld:'vINOUTSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutSistema_Descricao',fld:'vINOUTSISTEMA_DESCRICAO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E163H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E183H2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavSistema_nome1_Visible',ctrl:'vSISTEMA_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E173H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV72ddo_Sistema_NomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV92Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV66Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV67Sistema_Ativo',fld:'vSISTEMA_ATIVO',pic:'',nv:false},{av:'AV70TFSistema_Nome',fld:'vTFSISTEMA_NOME',pic:'@!',nv:''},{av:'Ddo_sistema_nome_Filteredtext_set',ctrl:'DDO_SISTEMA_NOME',prop:'FilteredText_set'},{av:'AV71TFSistema_Nome_Sel',fld:'vTFSISTEMA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_nome_Selectedvalue_set',ctrl:'DDO_SISTEMA_NOME',prop:'SelectedValue_set'},{av:'AV74TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Filteredtext_set',ctrl:'DDO_SISTEMA_SIGLA',prop:'FilteredText_set'},{av:'AV75TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Selectedvalue_set',ctrl:'DDO_SISTEMA_SIGLA',prop:'SelectedValue_set'},{av:'AV78TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_set'},{av:'AV79TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV82TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_metodologia_descricao_Filteredtext_set',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'FilteredText_set'},{av:'AV83TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_metodologia_descricao_Selectedvalue_set',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV68Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavSistema_nome1_Visible',ctrl:'vSISTEMA_NOME1',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutSistema_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_sistema_nome_Activeeventkey = "";
         Ddo_sistema_nome_Filteredtext_get = "";
         Ddo_sistema_nome_Selectedvalue_get = "";
         Ddo_sistema_sigla_Activeeventkey = "";
         Ddo_sistema_sigla_Filteredtext_get = "";
         Ddo_sistema_sigla_Selectedvalue_get = "";
         Ddo_ambientetecnologico_descricao_Activeeventkey = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_get = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_get = "";
         Ddo_metodologia_descricao_Activeeventkey = "";
         Ddo_metodologia_descricao_Filteredtext_get = "";
         Ddo_metodologia_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV68Sistema_Nome1 = "";
         AV70TFSistema_Nome = "";
         AV71TFSistema_Nome_Sel = "";
         AV74TFSistema_Sigla = "";
         AV75TFSistema_Sigla_Sel = "";
         AV78TFAmbienteTecnologico_Descricao = "";
         AV79TFAmbienteTecnologico_Descricao_Sel = "";
         AV82TFMetodologia_Descricao = "";
         AV83TFMetodologia_Descricao_Sel = "";
         AV72ddo_Sistema_NomeTitleControlIdToReplace = "";
         AV76ddo_Sistema_SiglaTitleControlIdToReplace = "";
         AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = "";
         AV84ddo_Metodologia_DescricaoTitleControlIdToReplace = "";
         AV67Sistema_Ativo = true;
         AV92Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         A128Sistema_Descricao = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV85DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV69Sistema_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Sistema_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81Metodologia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_sistema_nome_Filteredtext_set = "";
         Ddo_sistema_nome_Selectedvalue_set = "";
         Ddo_sistema_nome_Sortedstatus = "";
         Ddo_sistema_sigla_Filteredtext_set = "";
         Ddo_sistema_sigla_Selectedvalue_set = "";
         Ddo_sistema_sigla_Sortedstatus = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         Ddo_metodologia_descricao_Filteredtext_set = "";
         Ddo_metodologia_descricao_Selectedvalue_set = "";
         Ddo_metodologia_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV37Select = "";
         AV91Select_GXI = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A352AmbienteTecnologico_Descricao = "";
         A138Metodologia_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV68Sistema_Nome1 = "";
         lV70TFSistema_Nome = "";
         lV74TFSistema_Sigla = "";
         lV78TFAmbienteTecnologico_Descricao = "";
         lV82TFMetodologia_Descricao = "";
         H003H2_A137Metodologia_Codigo = new int[1] ;
         H003H2_n137Metodologia_Codigo = new bool[] {false} ;
         H003H2_A351AmbienteTecnologico_Codigo = new int[1] ;
         H003H2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H003H2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H003H2_A130Sistema_Ativo = new bool[] {false} ;
         H003H2_A128Sistema_Descricao = new String[] {""} ;
         H003H2_n128Sistema_Descricao = new bool[] {false} ;
         H003H2_A138Metodologia_Descricao = new String[] {""} ;
         H003H2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H003H2_A129Sistema_Sigla = new String[] {""} ;
         H003H2_A416Sistema_Nome = new String[] {""} ;
         H003H2_A127Sistema_Codigo = new int[1] ;
         H003H3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblFiltertextsistema_areatrabalhocod_Jsonclick = "";
         lblFiltertextsistema_ativo_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptsistema__default(),
            new Object[][] {
                new Object[] {
               H003H2_A137Metodologia_Codigo, H003H2_n137Metodologia_Codigo, H003H2_A351AmbienteTecnologico_Codigo, H003H2_n351AmbienteTecnologico_Codigo, H003H2_A135Sistema_AreaTrabalhoCod, H003H2_A130Sistema_Ativo, H003H2_A128Sistema_Descricao, H003H2_n128Sistema_Descricao, H003H2_A138Metodologia_Descricao, H003H2_A352AmbienteTecnologico_Descricao,
               H003H2_A129Sistema_Sigla, H003H2_A416Sistema_Nome, H003H2_A127Sistema_Codigo
               }
               , new Object[] {
               H003H3_AGRID_nRecordCount
               }
            }
         );
         AV92Pgmname = "PromptSistema";
         /* GeneXus formulas. */
         AV92Pgmname = "PromptSistema";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_50 ;
      private short nGXsfl_50_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_50_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSistema_Nome_Titleformat ;
      private short edtSistema_Sigla_Titleformat ;
      private short edtAmbienteTecnologico_Descricao_Titleformat ;
      private short edtMetodologia_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutSistema_Codigo ;
      private int wcpOAV7InOutSistema_Codigo ;
      private int subGrid_Rows ;
      private int AV66Sistema_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_sistema_nome_Datalistupdateminimumcharacters ;
      private int Ddo_sistema_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_metodologia_descricao_Datalistupdateminimumcharacters ;
      private int edtSistema_Descricao_Visible ;
      private int edtavTfsistema_nome_Visible ;
      private int edtavTfsistema_nome_sel_Visible ;
      private int edtavTfsistema_sigla_Visible ;
      private int edtavTfsistema_sigla_sel_Visible ;
      private int edtavTfambientetecnologico_descricao_Visible ;
      private int edtavTfambientetecnologico_descricao_sel_Visible ;
      private int edtavTfmetodologia_descricao_Visible ;
      private int edtavTfmetodologia_descricao_sel_Visible ;
      private int edtavDdo_sistema_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible ;
      private int A127Sistema_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A137Metodologia_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV86PageToGo ;
      private int edtavSistema_nome1_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV87GridCurrentPage ;
      private long AV88GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_sistema_nome_Activeeventkey ;
      private String Ddo_sistema_nome_Filteredtext_get ;
      private String Ddo_sistema_nome_Selectedvalue_get ;
      private String Ddo_sistema_sigla_Activeeventkey ;
      private String Ddo_sistema_sigla_Filteredtext_get ;
      private String Ddo_sistema_sigla_Selectedvalue_get ;
      private String Ddo_ambientetecnologico_descricao_Activeeventkey ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_get ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_get ;
      private String Ddo_metodologia_descricao_Activeeventkey ;
      private String Ddo_metodologia_descricao_Filteredtext_get ;
      private String Ddo_metodologia_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_50_idx="0001" ;
      private String AV74TFSistema_Sigla ;
      private String AV75TFSistema_Sigla_Sel ;
      private String AV92Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_sistema_nome_Caption ;
      private String Ddo_sistema_nome_Tooltip ;
      private String Ddo_sistema_nome_Cls ;
      private String Ddo_sistema_nome_Filteredtext_set ;
      private String Ddo_sistema_nome_Selectedvalue_set ;
      private String Ddo_sistema_nome_Dropdownoptionstype ;
      private String Ddo_sistema_nome_Titlecontrolidtoreplace ;
      private String Ddo_sistema_nome_Sortedstatus ;
      private String Ddo_sistema_nome_Filtertype ;
      private String Ddo_sistema_nome_Datalisttype ;
      private String Ddo_sistema_nome_Datalistproc ;
      private String Ddo_sistema_nome_Sortasc ;
      private String Ddo_sistema_nome_Sortdsc ;
      private String Ddo_sistema_nome_Loadingdata ;
      private String Ddo_sistema_nome_Cleanfilter ;
      private String Ddo_sistema_nome_Noresultsfound ;
      private String Ddo_sistema_nome_Searchbuttontext ;
      private String Ddo_sistema_sigla_Caption ;
      private String Ddo_sistema_sigla_Tooltip ;
      private String Ddo_sistema_sigla_Cls ;
      private String Ddo_sistema_sigla_Filteredtext_set ;
      private String Ddo_sistema_sigla_Selectedvalue_set ;
      private String Ddo_sistema_sigla_Dropdownoptionstype ;
      private String Ddo_sistema_sigla_Titlecontrolidtoreplace ;
      private String Ddo_sistema_sigla_Sortedstatus ;
      private String Ddo_sistema_sigla_Filtertype ;
      private String Ddo_sistema_sigla_Datalisttype ;
      private String Ddo_sistema_sigla_Datalistproc ;
      private String Ddo_sistema_sigla_Sortasc ;
      private String Ddo_sistema_sigla_Sortdsc ;
      private String Ddo_sistema_sigla_Loadingdata ;
      private String Ddo_sistema_sigla_Cleanfilter ;
      private String Ddo_sistema_sigla_Noresultsfound ;
      private String Ddo_sistema_sigla_Searchbuttontext ;
      private String Ddo_ambientetecnologico_descricao_Caption ;
      private String Ddo_ambientetecnologico_descricao_Tooltip ;
      private String Ddo_ambientetecnologico_descricao_Cls ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_set ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_descricao_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_descricao_Sortedstatus ;
      private String Ddo_ambientetecnologico_descricao_Filtertype ;
      private String Ddo_ambientetecnologico_descricao_Datalisttype ;
      private String Ddo_ambientetecnologico_descricao_Datalistproc ;
      private String Ddo_ambientetecnologico_descricao_Sortasc ;
      private String Ddo_ambientetecnologico_descricao_Sortdsc ;
      private String Ddo_ambientetecnologico_descricao_Loadingdata ;
      private String Ddo_ambientetecnologico_descricao_Cleanfilter ;
      private String Ddo_ambientetecnologico_descricao_Noresultsfound ;
      private String Ddo_ambientetecnologico_descricao_Searchbuttontext ;
      private String Ddo_metodologia_descricao_Caption ;
      private String Ddo_metodologia_descricao_Tooltip ;
      private String Ddo_metodologia_descricao_Cls ;
      private String Ddo_metodologia_descricao_Filteredtext_set ;
      private String Ddo_metodologia_descricao_Selectedvalue_set ;
      private String Ddo_metodologia_descricao_Dropdownoptionstype ;
      private String Ddo_metodologia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_metodologia_descricao_Sortedstatus ;
      private String Ddo_metodologia_descricao_Filtertype ;
      private String Ddo_metodologia_descricao_Datalisttype ;
      private String Ddo_metodologia_descricao_Datalistproc ;
      private String Ddo_metodologia_descricao_Sortasc ;
      private String Ddo_metodologia_descricao_Sortdsc ;
      private String Ddo_metodologia_descricao_Loadingdata ;
      private String Ddo_metodologia_descricao_Cleanfilter ;
      private String Ddo_metodologia_descricao_Noresultsfound ;
      private String Ddo_metodologia_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String edtSistema_Descricao_Internalname ;
      private String TempTags ;
      private String edtavTfsistema_nome_Internalname ;
      private String edtavTfsistema_nome_Jsonclick ;
      private String edtavTfsistema_nome_sel_Internalname ;
      private String edtavTfsistema_nome_sel_Jsonclick ;
      private String edtavTfsistema_sigla_Internalname ;
      private String edtavTfsistema_sigla_Jsonclick ;
      private String edtavTfsistema_sigla_sel_Internalname ;
      private String edtavTfsistema_sigla_sel_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_Internalname ;
      private String edtavTfambientetecnologico_descricao_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_sel_Internalname ;
      private String edtavTfambientetecnologico_descricao_sel_Jsonclick ;
      private String edtavTfmetodologia_descricao_Internalname ;
      private String edtavTfmetodologia_descricao_Jsonclick ;
      private String edtavTfmetodologia_descricao_sel_Internalname ;
      private String edtavTfmetodologia_descricao_sel_Jsonclick ;
      private String edtavDdo_sistema_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Nome_Internalname ;
      private String A129Sistema_Sigla ;
      private String edtSistema_Sigla_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtMetodologia_Descricao_Internalname ;
      private String chkavSistema_ativo_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV74TFSistema_Sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavSistema_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavSistema_nome1_Internalname ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_sistema_nome_Internalname ;
      private String Ddo_sistema_sigla_Internalname ;
      private String Ddo_ambientetecnologico_descricao_Internalname ;
      private String Ddo_metodologia_descricao_Internalname ;
      private String edtSistema_Nome_Title ;
      private String edtSistema_Sigla_Title ;
      private String edtAmbienteTecnologico_Descricao_Title ;
      private String edtMetodologia_Descricao_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextsistema_areatrabalhocod_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Jsonclick ;
      private String edtavSistema_areatrabalhocod_Jsonclick ;
      private String lblFiltertextsistema_ativo_Internalname ;
      private String lblFiltertextsistema_ativo_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavSistema_nome1_Jsonclick ;
      private String sGXsfl_50_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtSistema_Codigo_Jsonclick ;
      private String edtSistema_Nome_Jsonclick ;
      private String edtSistema_Sigla_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String edtMetodologia_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV67Sistema_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_sistema_nome_Includesortasc ;
      private bool Ddo_sistema_nome_Includesortdsc ;
      private bool Ddo_sistema_nome_Includefilter ;
      private bool Ddo_sistema_nome_Filterisrange ;
      private bool Ddo_sistema_nome_Includedatalist ;
      private bool Ddo_sistema_sigla_Includesortasc ;
      private bool Ddo_sistema_sigla_Includesortdsc ;
      private bool Ddo_sistema_sigla_Includefilter ;
      private bool Ddo_sistema_sigla_Filterisrange ;
      private bool Ddo_sistema_sigla_Includedatalist ;
      private bool Ddo_ambientetecnologico_descricao_Includesortasc ;
      private bool Ddo_ambientetecnologico_descricao_Includesortdsc ;
      private bool Ddo_ambientetecnologico_descricao_Includefilter ;
      private bool Ddo_ambientetecnologico_descricao_Filterisrange ;
      private bool Ddo_ambientetecnologico_descricao_Includedatalist ;
      private bool Ddo_metodologia_descricao_Includesortasc ;
      private bool Ddo_metodologia_descricao_Includesortdsc ;
      private bool Ddo_metodologia_descricao_Includefilter ;
      private bool Ddo_metodologia_descricao_Filterisrange ;
      private bool Ddo_metodologia_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A130Sistema_Ativo ;
      private bool n137Metodologia_Codigo ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n128Sistema_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV37Select_IsBlob ;
      private String AV8InOutSistema_Descricao ;
      private String wcpOAV8InOutSistema_Descricao ;
      private String A128Sistema_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV68Sistema_Nome1 ;
      private String AV70TFSistema_Nome ;
      private String AV71TFSistema_Nome_Sel ;
      private String AV78TFAmbienteTecnologico_Descricao ;
      private String AV79TFAmbienteTecnologico_Descricao_Sel ;
      private String AV82TFMetodologia_Descricao ;
      private String AV83TFMetodologia_Descricao_Sel ;
      private String AV72ddo_Sistema_NomeTitleControlIdToReplace ;
      private String AV76ddo_Sistema_SiglaTitleControlIdToReplace ;
      private String AV80ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ;
      private String AV84ddo_Metodologia_DescricaoTitleControlIdToReplace ;
      private String AV91Select_GXI ;
      private String A416Sistema_Nome ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A138Metodologia_Descricao ;
      private String lV68Sistema_Nome1 ;
      private String lV70TFSistema_Nome ;
      private String lV78TFAmbienteTecnologico_Descricao ;
      private String lV82TFMetodologia_Descricao ;
      private String AV37Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutSistema_Codigo ;
      private String aP1_InOutSistema_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkavSistema_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private IDataStoreProvider pr_default ;
      private int[] H003H2_A137Metodologia_Codigo ;
      private bool[] H003H2_n137Metodologia_Codigo ;
      private int[] H003H2_A351AmbienteTecnologico_Codigo ;
      private bool[] H003H2_n351AmbienteTecnologico_Codigo ;
      private int[] H003H2_A135Sistema_AreaTrabalhoCod ;
      private bool[] H003H2_A130Sistema_Ativo ;
      private String[] H003H2_A128Sistema_Descricao ;
      private bool[] H003H2_n128Sistema_Descricao ;
      private String[] H003H2_A138Metodologia_Descricao ;
      private String[] H003H2_A352AmbienteTecnologico_Descricao ;
      private String[] H003H2_A129Sistema_Sigla ;
      private String[] H003H2_A416Sistema_Nome ;
      private int[] H003H2_A127Sistema_Codigo ;
      private long[] H003H3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Sistema_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73Sistema_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV77AmbienteTecnologico_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV81Metodologia_DescricaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV85DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003H2( IGxContext context ,
                                             int AV66Sistema_AreaTrabalhoCod ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV68Sistema_Nome1 ,
                                             String AV71TFSistema_Nome_Sel ,
                                             String AV70TFSistema_Nome ,
                                             String AV75TFSistema_Sigla_Sel ,
                                             String AV74TFSistema_Sigla ,
                                             String AV79TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV78TFAmbienteTecnologico_Descricao ,
                                             String AV83TFMetodologia_Descricao_Sel ,
                                             String AV82TFMetodologia_Descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             bool A130Sistema_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Ativo], T1.[Sistema_Descricao], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_Codigo]";
         sFromString = " FROM (([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Sistema_Ativo] = 1)";
         if ( ! (0==AV66Sistema_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_AreaTrabalhoCod] = @AV66Sistema_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68Sistema_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV68Sistema_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFSistema_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFSistema_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like @lV70TFSistema_Nome)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFSistema_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] = @AV71TFSistema_Nome_Sel)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV74TFSistema_Sigla)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV75TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV78TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV79TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFMetodologia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV82TFMetodologia_Descricao)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFMetodologia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV83TFMetodologia_Descricao_Sel)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Sistema_Descricao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Sistema_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Sistema_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Sistema_Sigla]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Sistema_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[AmbienteTecnologico_Descricao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[AmbienteTecnologico_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Metodologia_Descricao]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Metodologia_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Sistema_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H003H3( IGxContext context ,
                                             int AV66Sistema_AreaTrabalhoCod ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV68Sistema_Nome1 ,
                                             String AV71TFSistema_Nome_Sel ,
                                             String AV70TFSistema_Nome ,
                                             String AV75TFSistema_Sigla_Sel ,
                                             String AV74TFSistema_Sigla ,
                                             String AV79TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV78TFAmbienteTecnologico_Descricao ,
                                             String AV83TFMetodologia_Descricao_Sel ,
                                             String AV82TFMetodologia_Descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             bool A130Sistema_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_Ativo] = 1)";
         if ( ! (0==AV66Sistema_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_AreaTrabalhoCod] = @AV66Sistema_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68Sistema_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV68Sistema_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFSistema_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFSistema_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like @lV70TFSistema_Nome)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFSistema_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] = @AV71TFSistema_Nome_Sel)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV74TFSistema_Sigla)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV75TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFAmbienteTecnologico_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV78TFAmbienteTecnologico_Descricao)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFAmbienteTecnologico_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV79TFAmbienteTecnologico_Descricao_Sel)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83TFMetodologia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFMetodologia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV82TFMetodologia_Descricao)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFMetodologia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV83TFMetodologia_Descricao_Sel)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003H2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (bool)dynConstraints[18] );
               case 1 :
                     return conditional_H003H3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (bool)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003H2 ;
          prmH003H2 = new Object[] {
          new Object[] {"@AV66Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV68Sistema_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV70TFSistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV71TFSistema_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV74TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV75TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV78TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV82TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV83TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH003H3 ;
          prmH003H3 = new Object[] {
          new Object[] {"@AV66Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV68Sistema_Nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV70TFSistema_Nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV71TFSistema_Nome_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV74TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV75TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV78TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV82TFMetodologia_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV83TFMetodologia_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003H2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003H2,11,0,true,false )
             ,new CursorDef("H003H3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003H3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 25) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
