/*
               File: LoteArquivoAnexoGeneral
        Description: Lote Arquivo Anexo General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:55.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class lotearquivoanexogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public lotearquivoanexogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public lotearquivoanexogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_LoteArquivoAnexo_LoteCod ,
                           DateTime aP1_LoteArquivoAnexo_Data )
      {
         this.A841LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         this.A836LoteArquivoAnexo_Data = aP1_LoteArquivoAnexo_Data;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
                  A836LoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A841LoteArquivoAnexo_LoteCod,(DateTime)A836LoteArquivoAnexo_Data});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAEY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "LoteArquivoAnexoGeneral";
               context.Gx_err = 0;
               WSEY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Lote Arquivo Anexo General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117295580");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("lotearquivoanexogeneral.aspx") + "?" + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA836LoteArquivoAnexo_Data", context.localUtil.TToC( wcpOA836LoteArquivoAnexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTEARQUIVOANEXO_DESCRICAO", GetSecureSignedToken( sPrefix, A837LoteArquivoAnexo_Descricao));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEY2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("lotearquivoanexogeneral.js", "?20203117295581");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "LoteArquivoAnexoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Lote Arquivo Anexo General" ;
      }

      protected void WBEY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "lotearquivoanexogeneral.aspx");
            }
            wb_table1_2_EY2( true) ;
         }
         else
         {
            wb_table1_2_EY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_LoteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_LoteCod_Jsonclick, 0, "Attribute", "", "", "", edtLoteArquivoAnexo_LoteCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LoteArquivoAnexoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTEY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Lote Arquivo Anexo General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPEY0( ) ;
            }
         }
      }

      protected void WSEY2( )
      {
         STARTEY2( ) ;
         EVTEY2( ) ;
      }

      protected void EVTEY2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11EY2 */
                                    E11EY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12EY2 */
                                    E12EY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13EY2 */
                                    E13EY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14EY2 */
                                    E14EY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15EY2 */
                                    E15EY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEY2( ) ;
            }
         }
      }

      protected void PAEY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "LoteArquivoAnexoGeneral";
         context.Gx_err = 0;
      }

      protected void RFEY2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00EY2 */
            pr_default.execute(0, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A839LoteArquivoAnexo_NomeArq = H00EY2_A839LoteArquivoAnexo_NomeArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
               n839LoteArquivoAnexo_NomeArq = H00EY2_n839LoteArquivoAnexo_NomeArq[0];
               edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
               A840LoteArquivoAnexo_TipoArq = H00EY2_A840LoteArquivoAnexo_TipoArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
               n840LoteArquivoAnexo_TipoArq = H00EY2_n840LoteArquivoAnexo_TipoArq[0];
               edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               A645TipoDocumento_Codigo = H00EY2_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = H00EY2_n645TipoDocumento_Codigo[0];
               A837LoteArquivoAnexo_Descricao = H00EY2_A837LoteArquivoAnexo_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A837LoteArquivoAnexo_Descricao", A837LoteArquivoAnexo_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTEARQUIVOANEXO_DESCRICAO", GetSecureSignedToken( sPrefix, A837LoteArquivoAnexo_Descricao));
               n837LoteArquivoAnexo_Descricao = H00EY2_n837LoteArquivoAnexo_Descricao[0];
               A646TipoDocumento_Nome = H00EY2_A646TipoDocumento_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
               A838LoteArquivoAnexo_Arquivo = H00EY2_A838LoteArquivoAnexo_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
               n838LoteArquivoAnexo_Arquivo = H00EY2_n838LoteArquivoAnexo_Arquivo[0];
               A646TipoDocumento_Nome = H00EY2_A646TipoDocumento_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
               /* Execute user event: E12EY2 */
               E12EY2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBEY0( ) ;
         }
      }

      protected void STRUPEY0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "LoteArquivoAnexoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11EY2 */
         E11EY2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A838LoteArquivoAnexo_Arquivo = cgiGet( edtLoteArquivoAnexo_Arquivo_Internalname);
            n838LoteArquivoAnexo_Arquivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
            A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
            A837LoteArquivoAnexo_Descricao = cgiGet( edtLoteArquivoAnexo_Descricao_Internalname);
            n837LoteArquivoAnexo_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A837LoteArquivoAnexo_Descricao", A837LoteArquivoAnexo_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTEARQUIVOANEXO_DESCRICAO", GetSecureSignedToken( sPrefix, A837LoteArquivoAnexo_Descricao));
            /* Read saved values. */
            wcpOA841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA841LoteArquivoAnexo_LoteCod"), ",", "."));
            wcpOA836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( sPrefix+"wcpOA836LoteArquivoAnexo_Data"), 0);
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11EY2 */
         E11EY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11EY2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLoteArquivoAnexo_Arquivo_Display = 1;
         edtLoteArquivoAnexo_Arquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "Linktarget", edtLoteArquivoAnexo_Arquivo_Linktarget);
      }

      protected void nextLoad( )
      {
      }

      protected void E12EY2( )
      {
         /* Load Routine */
         edtTipoDocumento_Nome_Link = formatLink("viewtipodocumento.aspx") + "?" + UrlEncode("" +A645TipoDocumento_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTipoDocumento_Nome_Internalname, "Link", edtTipoDocumento_Nome_Link);
         edtLoteArquivoAnexo_LoteCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_LoteCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_LoteCod_Visible), 5, 0)));
      }

      protected void E13EY2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data));
         context.wjLocDisableFrm = 1;
      }

      protected void E14EY2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data));
         context.wjLocDisableFrm = 1;
      }

      protected void E15EY2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewlote.aspx") + "?" + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(StringUtil.RTrim("LoteArquivoAnexo"));
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "LoteArquivoAnexo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "LoteArquivoAnexo_LoteCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7LoteArquivoAnexo_LoteCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "LoteArquivoAnexo_Data";
         AV10TrnContextAtt.gxTpr_Attributevalue = context.localUtil.TToC( AV8LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " ");
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_EY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_EY2( true) ;
         }
         else
         {
            wb_table2_8_EY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_EY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_42_EY2( true) ;
         }
         else
         {
            wb_table3_42_EY2( false) ;
         }
         return  ;
      }

      protected void wb_table3_42_EY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EY2e( true) ;
         }
         else
         {
            wb_table1_2_EY2e( false) ;
         }
      }

      protected void wb_table3_42_EY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_42_EY2e( true) ;
         }
         else
         {
            wb_table3_42_EY2e( false) ;
         }
      }

      protected void wb_table2_8_EY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_nomearq_Internalname, "Nome do Arquivo", "", "", lblTextblocklotearquivoanexo_nomearq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_NomeArq_Internalname, StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq), StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_NomeArq_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_arquivo_Internalname, "Arquivo", "", "", lblTextblocklotearquivoanexo_arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCellView'>") ;
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            edtLoteArquivoAnexo_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
            {
               gxblobfileaux.Source = A838LoteArquivoAnexo_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtLoteArquivoAnexo_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtLoteArquivoAnexo_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A838LoteArquivoAnexo_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n838LoteArquivoAnexo_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                  edtLoteArquivoAnexo_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtLoteArquivoAnexo_Arquivo_Internalname, StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo), context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype)) ? A838LoteArquivoAnexo_Arquivo : edtLoteArquivoAnexo_Arquivo_Filetype)) : edtLoteArquivoAnexo_Arquivo_Contenttype), true, edtLoteArquivoAnexo_Arquivo_Linktarget, edtLoteArquivoAnexo_Arquivo_Parameters, edtLoteArquivoAnexo_Arquivo_Display, 0, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtLoteArquivoAnexo_Arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+"", "", "", "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_tipoarq_Internalname, "Tipo de Arquivo", "", "", lblTextblocklotearquivoanexo_tipoarq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_TipoArq_Internalname, StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq), StringUtil.RTrim( context.localUtil.Format( A840LoteArquivoAnexo_TipoArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_TipoArq_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodocumento_nome_Internalname, "Tipo de Documento", "", "", lblTextblocktipodocumento_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoDocumento_Nome_Internalname, StringUtil.RTrim( A646TipoDocumento_Nome), StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtTipoDocumento_Nome_Link, "", "", "", edtTipoDocumento_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_descricao_Internalname, "Descri��o", "", "", lblTextblocklotearquivoanexo_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLoteArquivoAnexo_Descricao_Internalname, A837LoteArquivoAnexo_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_data_Internalname, "Data/Hora Upload", "", "", lblTextblocklotearquivoanexo_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLoteArquivoAnexo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_Data_Internalname, context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_LoteArquivoAnexoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLoteArquivoAnexo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteArquivoAnexoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_EY2e( true) ;
         }
         else
         {
            wb_table2_8_EY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A841LoteArquivoAnexo_LoteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
         A836LoteArquivoAnexo_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEY2( ) ;
         WSEY2( ) ;
         WEEY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA841LoteArquivoAnexo_LoteCod = (String)((String)getParm(obj,0));
         sCtrlA836LoteArquivoAnexo_Data = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAEY2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "lotearquivoanexogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAEY2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A841LoteArquivoAnexo_LoteCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
            A836LoteArquivoAnexo_Data = (DateTime)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         wcpOA841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA841LoteArquivoAnexo_LoteCod"), ",", "."));
         wcpOA836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( sPrefix+"wcpOA836LoteArquivoAnexo_Data"), 0);
         if ( ! GetJustCreated( ) && ( ( A841LoteArquivoAnexo_LoteCod != wcpOA841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != wcpOA836LoteArquivoAnexo_Data ) ) )
         {
            setjustcreated();
         }
         wcpOA841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
         wcpOA836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA841LoteArquivoAnexo_LoteCod = cgiGet( sPrefix+"A841LoteArquivoAnexo_LoteCod_CTRL");
         if ( StringUtil.Len( sCtrlA841LoteArquivoAnexo_LoteCod) > 0 )
         {
            A841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA841LoteArquivoAnexo_LoteCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
         }
         else
         {
            A841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A841LoteArquivoAnexo_LoteCod_PARM"), ",", "."));
         }
         sCtrlA836LoteArquivoAnexo_Data = cgiGet( sPrefix+"A836LoteArquivoAnexo_Data_CTRL");
         if ( StringUtil.Len( sCtrlA836LoteArquivoAnexo_Data) > 0 )
         {
            A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( sCtrlA836LoteArquivoAnexo_Data), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         else
         {
            A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( sPrefix+"A836LoteArquivoAnexo_Data_PARM"), 0);
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAEY2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSEY2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSEY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A841LoteArquivoAnexo_LoteCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA841LoteArquivoAnexo_LoteCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A841LoteArquivoAnexo_LoteCod_CTRL", StringUtil.RTrim( sCtrlA841LoteArquivoAnexo_LoteCod));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A836LoteArquivoAnexo_Data_PARM", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 0, "/", ":", " "));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA836LoteArquivoAnexo_Data)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A836LoteArquivoAnexo_Data_CTRL", StringUtil.RTrim( sCtrlA836LoteArquivoAnexo_Data));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEEY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311729565");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("lotearquivoanexogeneral.js", "?2020311729565");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocklotearquivoanexo_nomearq_Internalname = sPrefix+"TEXTBLOCKLOTEARQUIVOANEXO_NOMEARQ";
         edtLoteArquivoAnexo_NomeArq_Internalname = sPrefix+"LOTEARQUIVOANEXO_NOMEARQ";
         lblTextblocklotearquivoanexo_arquivo_Internalname = sPrefix+"TEXTBLOCKLOTEARQUIVOANEXO_ARQUIVO";
         edtLoteArquivoAnexo_Arquivo_Internalname = sPrefix+"LOTEARQUIVOANEXO_ARQUIVO";
         lblTextblocklotearquivoanexo_tipoarq_Internalname = sPrefix+"TEXTBLOCKLOTEARQUIVOANEXO_TIPOARQ";
         edtLoteArquivoAnexo_TipoArq_Internalname = sPrefix+"LOTEARQUIVOANEXO_TIPOARQ";
         lblTextblocktipodocumento_nome_Internalname = sPrefix+"TEXTBLOCKTIPODOCUMENTO_NOME";
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME";
         lblTextblocklotearquivoanexo_descricao_Internalname = sPrefix+"TEXTBLOCKLOTEARQUIVOANEXO_DESCRICAO";
         edtLoteArquivoAnexo_Descricao_Internalname = sPrefix+"LOTEARQUIVOANEXO_DESCRICAO";
         lblTextblocklotearquivoanexo_data_Internalname = sPrefix+"TEXTBLOCKLOTEARQUIVOANEXO_DATA";
         edtLoteArquivoAnexo_Data_Internalname = sPrefix+"LOTEARQUIVOANEXO_DATA";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtLoteArquivoAnexo_LoteCod_Internalname = sPrefix+"LOTEARQUIVOANEXO_LOTECOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtLoteArquivoAnexo_Data_Jsonclick = "";
         edtTipoDocumento_Nome_Jsonclick = "";
         edtLoteArquivoAnexo_TipoArq_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Parameters = "";
         edtLoteArquivoAnexo_Arquivo_Contenttype = "";
         edtLoteArquivoAnexo_Arquivo_Display = 0;
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         edtTipoDocumento_Nome_Link = "";
         edtLoteArquivoAnexo_Arquivo_Linktarget = "";
         edtLoteArquivoAnexo_Arquivo_Filetype = "";
         edtLoteArquivoAnexo_LoteCod_Jsonclick = "";
         edtLoteArquivoAnexo_LoteCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13EY2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14EY2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15EY2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOA836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A837LoteArquivoAnexo_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00EY2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00EY2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00EY2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00EY2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         H00EY2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         H00EY2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         H00EY2_A645TipoDocumento_Codigo = new int[1] ;
         H00EY2_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00EY2_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         H00EY2_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         H00EY2_A646TipoDocumento_Nome = new String[] {""} ;
         H00EY2_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         H00EY2_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         A839LoteArquivoAnexo_NomeArq = "";
         edtLoteArquivoAnexo_Arquivo_Filename = "";
         A840LoteArquivoAnexo_TipoArq = "";
         A646TipoDocumento_Nome = "";
         A838LoteArquivoAnexo_Arquivo = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV8LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblocklotearquivoanexo_nomearq_Jsonclick = "";
         lblTextblocklotearquivoanexo_arquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblocklotearquivoanexo_tipoarq_Jsonclick = "";
         lblTextblocktipodocumento_nome_Jsonclick = "";
         lblTextblocklotearquivoanexo_descricao_Jsonclick = "";
         lblTextblocklotearquivoanexo_data_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA841LoteArquivoAnexo_LoteCod = "";
         sCtrlA836LoteArquivoAnexo_Data = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.lotearquivoanexogeneral__default(),
            new Object[][] {
                new Object[] {
               H00EY2_A841LoteArquivoAnexo_LoteCod, H00EY2_A836LoteArquivoAnexo_Data, H00EY2_A839LoteArquivoAnexo_NomeArq, H00EY2_n839LoteArquivoAnexo_NomeArq, H00EY2_A840LoteArquivoAnexo_TipoArq, H00EY2_n840LoteArquivoAnexo_TipoArq, H00EY2_A645TipoDocumento_Codigo, H00EY2_n645TipoDocumento_Codigo, H00EY2_A837LoteArquivoAnexo_Descricao, H00EY2_n837LoteArquivoAnexo_Descricao,
               H00EY2_A646TipoDocumento_Nome, H00EY2_A838LoteArquivoAnexo_Arquivo, H00EY2_n838LoteArquivoAnexo_Arquivo
               }
            }
         );
         AV15Pgmname = "LoteArquivoAnexoGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "LoteArquivoAnexoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short edtLoteArquivoAnexo_Arquivo_Display ;
      private short nGXWrapped ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int wcpOA841LoteArquivoAnexo_LoteCod ;
      private int edtLoteArquivoAnexo_LoteCod_Visible ;
      private int A645TipoDocumento_Codigo ;
      private int AV7LoteArquivoAnexo_LoteCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtLoteArquivoAnexo_LoteCod_Internalname ;
      private String edtLoteArquivoAnexo_LoteCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String edtLoteArquivoAnexo_Arquivo_Filename ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String edtLoteArquivoAnexo_Arquivo_Filetype ;
      private String edtLoteArquivoAnexo_Arquivo_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String edtLoteArquivoAnexo_Descricao_Internalname ;
      private String edtLoteArquivoAnexo_Arquivo_Linktarget ;
      private String edtTipoDocumento_Nome_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocklotearquivoanexo_nomearq_Internalname ;
      private String lblTextblocklotearquivoanexo_nomearq_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private String lblTextblocklotearquivoanexo_arquivo_Internalname ;
      private String lblTextblocklotearquivoanexo_arquivo_Jsonclick ;
      private String edtLoteArquivoAnexo_Arquivo_Contenttype ;
      private String edtLoteArquivoAnexo_Arquivo_Parameters ;
      private String edtLoteArquivoAnexo_Arquivo_Jsonclick ;
      private String lblTextblocklotearquivoanexo_tipoarq_Internalname ;
      private String lblTextblocklotearquivoanexo_tipoarq_Jsonclick ;
      private String edtLoteArquivoAnexo_TipoArq_Internalname ;
      private String edtLoteArquivoAnexo_TipoArq_Jsonclick ;
      private String lblTextblocktipodocumento_nome_Internalname ;
      private String lblTextblocktipodocumento_nome_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String lblTextblocklotearquivoanexo_descricao_Internalname ;
      private String lblTextblocklotearquivoanexo_descricao_Jsonclick ;
      private String lblTextblocklotearquivoanexo_data_Internalname ;
      private String lblTextblocklotearquivoanexo_data_Jsonclick ;
      private String edtLoteArquivoAnexo_Data_Internalname ;
      private String edtLoteArquivoAnexo_Data_Jsonclick ;
      private String sCtrlA841LoteArquivoAnexo_LoteCod ;
      private String sCtrlA836LoteArquivoAnexo_Data ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private DateTime wcpOA836LoteArquivoAnexo_Data ;
      private DateTime AV8LoteArquivoAnexo_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool n645TipoDocumento_Codigo ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool returnInSub ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00EY2_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] H00EY2_A836LoteArquivoAnexo_Data ;
      private String[] H00EY2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00EY2_n839LoteArquivoAnexo_NomeArq ;
      private String[] H00EY2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] H00EY2_n840LoteArquivoAnexo_TipoArq ;
      private int[] H00EY2_A645TipoDocumento_Codigo ;
      private bool[] H00EY2_n645TipoDocumento_Codigo ;
      private String[] H00EY2_A837LoteArquivoAnexo_Descricao ;
      private bool[] H00EY2_n837LoteArquivoAnexo_Descricao ;
      private String[] H00EY2_A646TipoDocumento_Nome ;
      private String[] H00EY2_A838LoteArquivoAnexo_Arquivo ;
      private bool[] H00EY2_n838LoteArquivoAnexo_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class lotearquivoanexogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EY2 ;
          prmH00EY2 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EY2", "SELECT T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Data], T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_TipoArq], T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_Descricao], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_Arquivo] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod and T1.[LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ORDER BY T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Data] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EY2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[11])[0] = rslt.getBLOBFile(8, rslt.getString(4, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
       }
    }

 }

}
