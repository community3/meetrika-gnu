/*
               File: SolicitacaoServicoReqNegocio
        Description: Requisitos de Neg�cio da Solicita��o de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:45:29.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaoservicoreqnegocio : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A456ContagemResultado_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbSolicServicoReqNeg_Prioridade.Name = "SOLICSERVICOREQNEG_PRIORIDADE";
         cmbSolicServicoReqNeg_Prioridade.WebTags = "";
         cmbSolicServicoReqNeg_Prioridade.addItem("1", "Alta", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("2", "Alta M�dia", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("3", "Alta Baixa", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("4", "M�dia Alta", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("5", "M�dia M�dia", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("6", "M�dia Baixa", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("7", "Baixa Alta", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("8", "Baixa M�dia", 0);
         cmbSolicServicoReqNeg_Prioridade.addItem("9", "Baixa Baixa", 0);
         if ( cmbSolicServicoReqNeg_Prioridade.ItemCount > 0 )
         {
            A1911SolicServicoReqNeg_Prioridade = (short)(NumberUtil.Val( cmbSolicServicoReqNeg_Prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)));
         }
         cmbSolicServicoReqNeg_Situacao.Name = "SOLICSERVICOREQNEG_SITUACAO";
         cmbSolicServicoReqNeg_Situacao.WebTags = "";
         cmbSolicServicoReqNeg_Situacao.addItem("1", "Solicitado", 0);
         cmbSolicServicoReqNeg_Situacao.addItem("2", "Aprovado", 0);
         cmbSolicServicoReqNeg_Situacao.addItem("3", "N�o Aprovado", 0);
         cmbSolicServicoReqNeg_Situacao.addItem("4", "Inativo", 0);
         cmbSolicServicoReqNeg_Situacao.addItem("5", "Cancelado", 0);
         if ( cmbSolicServicoReqNeg_Situacao.ItemCount > 0 )
         {
            A1914SolicServicoReqNeg_Situacao = (short)(NumberUtil.Val( cmbSolicServicoReqNeg_Situacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Requisitos de Neg�cio da Solicita��o de Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public solicitacaoservicoreqnegocio( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaoservicoreqnegocio( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbSolicServicoReqNeg_Prioridade = new GXCombobox();
         cmbSolicServicoReqNeg_Situacao = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbSolicServicoReqNeg_Prioridade.ItemCount > 0 )
         {
            A1911SolicServicoReqNeg_Prioridade = (short)(NumberUtil.Val( cmbSolicServicoReqNeg_Prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)));
         }
         if ( cmbSolicServicoReqNeg_Situacao.ItemCount > 0 )
         {
            A1914SolicServicoReqNeg_Situacao = (short)(NumberUtil.Val( cmbSolicServicoReqNeg_Situacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4T214( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4T214e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicServicoReqNeg_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")), ((edtSolicServicoReqNeg_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1895SolicServicoReqNeg_Codigo), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1895SolicServicoReqNeg_Codigo), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicServicoReqNeg_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSolicServicoReqNeg_Codigo_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_SolicitacaoServicoReqNegocio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), ((edtContagemResultado_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultado_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServicoReqNegocio.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4T214( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4T214( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4T214e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_46_4T214( true) ;
         }
         return  ;
      }

      protected void wb_table3_46_4T214e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4T214e( true) ;
         }
         else
         {
            wb_table1_2_4T214e( false) ;
         }
      }

      protected void wb_table3_46_4T214( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_46_4T214e( true) ;
         }
         else
         {
            wb_table3_46_4T214e( false) ;
         }
      }

      protected void wb_table2_5_4T214( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4T214( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4T214e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4T214e( true) ;
         }
         else
         {
            wb_table2_5_4T214e( false) ;
         }
      }

      protected void wb_table4_13_4T214( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_identificador_Internalname, "Identificador", "", "", lblTextblocksolicservicoreqneg_identificador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicServicoReqNeg_Identificador_Internalname, A1915SolicServicoReqNeg_Identificador, StringUtil.RTrim( context.localUtil.Format( A1915SolicServicoReqNeg_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicServicoReqNeg_Identificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicServicoReqNeg_Identificador_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "ReqIdentificador", "left", true, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_nome_Internalname, "Nome", "", "", lblTextblocksolicservicoreqneg_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSolicServicoReqNeg_Nome_Internalname, A1909SolicServicoReqNeg_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtSolicServicoReqNeg_Nome_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "ReqNome", "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_prioridade_Internalname, "Prioridade", "", "", lblTextblocksolicservicoreqneg_prioridade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSolicServicoReqNeg_Prioridade, cmbSolicServicoReqNeg_Prioridade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)), 1, cmbSolicServicoReqNeg_Prioridade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbSolicServicoReqNeg_Prioridade.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_SolicitacaoServicoReqNegocio.htm");
            cmbSolicServicoReqNeg_Prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicServicoReqNeg_Prioridade_Internalname, "Values", (String)(cmbSolicServicoReqNeg_Prioridade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_agrupador_Internalname, "Agrupador", "", "", lblTextblocksolicservicoreqneg_agrupador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicServicoReqNeg_Agrupador_Internalname, A1912SolicServicoReqNeg_Agrupador, StringUtil.RTrim( context.localUtil.Format( A1912SolicServicoReqNeg_Agrupador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicServicoReqNeg_Agrupador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicServicoReqNeg_Agrupador_Enabled, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "ReqAgrupador", "left", true, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_tamanho_Internalname, "Tamanho", "", "", lblTextblocksolicservicoreqneg_tamanho_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicServicoReqNeg_Tamanho_Internalname, StringUtil.LTrim( StringUtil.NToC( A1913SolicServicoReqNeg_Tamanho, 7, 3, ",", "")), ((edtSolicServicoReqNeg_Tamanho_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1913SolicServicoReqNeg_Tamanho, "ZZ9.999")) : context.localUtil.Format( A1913SolicServicoReqNeg_Tamanho, "ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','3');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicServicoReqNeg_Tamanho_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicServicoReqNeg_Tamanho_Enabled, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "ReqTamanho", "right", false, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_situacao_Internalname, "Situa��o", "", "", lblTextblocksolicservicoreqneg_situacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServicoReqNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSolicServicoReqNeg_Situacao, cmbSolicServicoReqNeg_Situacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)), 1, cmbSolicServicoReqNeg_Situacao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbSolicServicoReqNeg_Situacao.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_SolicitacaoServicoReqNegocio.htm");
            cmbSolicServicoReqNeg_Situacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicServicoReqNeg_Situacao_Internalname, "Values", (String)(cmbSolicServicoReqNeg_Situacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4T214e( true) ;
         }
         else
         {
            wb_table4_13_4T214e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114T2 */
         E114T2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1915SolicServicoReqNeg_Identificador = cgiGet( edtSolicServicoReqNeg_Identificador_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1915SolicServicoReqNeg_Identificador", A1915SolicServicoReqNeg_Identificador);
               A1909SolicServicoReqNeg_Nome = cgiGet( edtSolicServicoReqNeg_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1909SolicServicoReqNeg_Nome", A1909SolicServicoReqNeg_Nome);
               cmbSolicServicoReqNeg_Prioridade.CurrentValue = cgiGet( cmbSolicServicoReqNeg_Prioridade_Internalname);
               A1911SolicServicoReqNeg_Prioridade = (short)(NumberUtil.Val( cgiGet( cmbSolicServicoReqNeg_Prioridade_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)));
               A1912SolicServicoReqNeg_Agrupador = cgiGet( edtSolicServicoReqNeg_Agrupador_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1912SolicServicoReqNeg_Agrupador", A1912SolicServicoReqNeg_Agrupador);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Tamanho_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Tamanho_Internalname), ",", ".") > 999.999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICSERVICOREQNEG_TAMANHO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicServicoReqNeg_Tamanho_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1913SolicServicoReqNeg_Tamanho = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1913SolicServicoReqNeg_Tamanho", StringUtil.LTrim( StringUtil.Str( A1913SolicServicoReqNeg_Tamanho, 7, 3)));
               }
               else
               {
                  A1913SolicServicoReqNeg_Tamanho = context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Tamanho_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1913SolicServicoReqNeg_Tamanho", StringUtil.LTrim( StringUtil.Str( A1913SolicServicoReqNeg_Tamanho, 7, 3)));
               }
               cmbSolicServicoReqNeg_Situacao.CurrentValue = cgiGet( cmbSolicServicoReqNeg_Situacao_Internalname);
               A1914SolicServicoReqNeg_Situacao = (short)(NumberUtil.Val( cgiGet( cmbSolicServicoReqNeg_Situacao_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICSERVICOREQNEG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1895SolicServicoReqNeg_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               }
               else
               {
                  A1895SolicServicoReqNeg_Codigo = (long)(context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A456ContagemResultado_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               else
               {
                  A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z1895SolicServicoReqNeg_Codigo = (long)(context.localUtil.CToN( cgiGet( "Z1895SolicServicoReqNeg_Codigo"), ",", "."));
               Z1915SolicServicoReqNeg_Identificador = cgiGet( "Z1915SolicServicoReqNeg_Identificador");
               Z1909SolicServicoReqNeg_Nome = cgiGet( "Z1909SolicServicoReqNeg_Nome");
               Z1911SolicServicoReqNeg_Prioridade = (short)(context.localUtil.CToN( cgiGet( "Z1911SolicServicoReqNeg_Prioridade"), ",", "."));
               Z1912SolicServicoReqNeg_Agrupador = cgiGet( "Z1912SolicServicoReqNeg_Agrupador");
               Z1913SolicServicoReqNeg_Tamanho = context.localUtil.CToN( cgiGet( "Z1913SolicServicoReqNeg_Tamanho"), ",", ".");
               Z1914SolicServicoReqNeg_Situacao = (short)(context.localUtil.CToN( cgiGet( "Z1914SolicServicoReqNeg_Situacao"), ",", "."));
               Z456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z456ContagemResultado_Codigo"), ",", "."));
               Z1947SolicServicoReqNeg_ReqNeqCod = (long)(context.localUtil.CToN( cgiGet( "Z1947SolicServicoReqNeg_ReqNeqCod"), ",", "."));
               n1947SolicServicoReqNeg_ReqNeqCod = ((0==A1947SolicServicoReqNeg_ReqNeqCod) ? true : false);
               A1947SolicServicoReqNeg_ReqNeqCod = (long)(context.localUtil.CToN( cgiGet( "Z1947SolicServicoReqNeg_ReqNeqCod"), ",", "."));
               n1947SolicServicoReqNeg_ReqNeqCod = false;
               n1947SolicServicoReqNeg_ReqNeqCod = ((0==A1947SolicServicoReqNeg_ReqNeqCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A1910SolicServicoReqNeg_Descricao = cgiGet( "SOLICSERVICOREQNEG_DESCRICAO");
               A1947SolicServicoReqNeg_ReqNeqCod = (long)(context.localUtil.CToN( cgiGet( "SOLICSERVICOREQNEG_REQNEQCOD"), ",", "."));
               n1947SolicServicoReqNeg_ReqNeqCod = ((0==A1947SolicServicoReqNeg_ReqNeqCod) ? true : false);
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "SolicitacaoServicoReqNegocio";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), "ZZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[SecurityCheckFailed value for]"+"SolicServicoReqNeg_ReqNeqCod:"+context.localUtil.Format( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), "ZZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1895SolicServicoReqNeg_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114T2 */
                           E114T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124T2 */
                           E124T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124T2 */
            E124T2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4T214( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         }
         DisableAttributes4T214( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption4T0( )
      {
      }

      protected void E114T2( )
      {
         /* Start Routine */
      }

      protected void E124T2( )
      {
         /* After Trn Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4T214( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1915SolicServicoReqNeg_Identificador = T004T3_A1915SolicServicoReqNeg_Identificador[0];
               Z1909SolicServicoReqNeg_Nome = T004T3_A1909SolicServicoReqNeg_Nome[0];
               Z1911SolicServicoReqNeg_Prioridade = T004T3_A1911SolicServicoReqNeg_Prioridade[0];
               Z1912SolicServicoReqNeg_Agrupador = T004T3_A1912SolicServicoReqNeg_Agrupador[0];
               Z1913SolicServicoReqNeg_Tamanho = T004T3_A1913SolicServicoReqNeg_Tamanho[0];
               Z1914SolicServicoReqNeg_Situacao = T004T3_A1914SolicServicoReqNeg_Situacao[0];
               Z456ContagemResultado_Codigo = T004T3_A456ContagemResultado_Codigo[0];
               Z1947SolicServicoReqNeg_ReqNeqCod = T004T3_A1947SolicServicoReqNeg_ReqNeqCod[0];
            }
            else
            {
               Z1915SolicServicoReqNeg_Identificador = A1915SolicServicoReqNeg_Identificador;
               Z1909SolicServicoReqNeg_Nome = A1909SolicServicoReqNeg_Nome;
               Z1911SolicServicoReqNeg_Prioridade = A1911SolicServicoReqNeg_Prioridade;
               Z1912SolicServicoReqNeg_Agrupador = A1912SolicServicoReqNeg_Agrupador;
               Z1913SolicServicoReqNeg_Tamanho = A1913SolicServicoReqNeg_Tamanho;
               Z1914SolicServicoReqNeg_Situacao = A1914SolicServicoReqNeg_Situacao;
               Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               Z1947SolicServicoReqNeg_ReqNeqCod = A1947SolicServicoReqNeg_ReqNeqCod;
            }
         }
         if ( GX_JID == -3 )
         {
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1915SolicServicoReqNeg_Identificador = A1915SolicServicoReqNeg_Identificador;
            Z1909SolicServicoReqNeg_Nome = A1909SolicServicoReqNeg_Nome;
            Z1910SolicServicoReqNeg_Descricao = A1910SolicServicoReqNeg_Descricao;
            Z1911SolicServicoReqNeg_Prioridade = A1911SolicServicoReqNeg_Prioridade;
            Z1912SolicServicoReqNeg_Agrupador = A1912SolicServicoReqNeg_Agrupador;
            Z1913SolicServicoReqNeg_Tamanho = A1913SolicServicoReqNeg_Tamanho;
            Z1914SolicServicoReqNeg_Situacao = A1914SolicServicoReqNeg_Situacao;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z1947SolicServicoReqNeg_ReqNeqCod = A1947SolicServicoReqNeg_ReqNeqCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         /* Using cursor T004T5 */
         pr_default.execute(3, new Object[] {n1947SolicServicoReqNeg_ReqNeqCod, A1947SolicServicoReqNeg_ReqNeqCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1947SolicServicoReqNeg_ReqNeqCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Sol Ser Req Neg_Sol Ser Req Neg'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(3);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load4T214( )
      {
         /* Using cursor T004T6 */
         pr_default.execute(4, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound214 = 1;
            A1915SolicServicoReqNeg_Identificador = T004T6_A1915SolicServicoReqNeg_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1915SolicServicoReqNeg_Identificador", A1915SolicServicoReqNeg_Identificador);
            A1909SolicServicoReqNeg_Nome = T004T6_A1909SolicServicoReqNeg_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1909SolicServicoReqNeg_Nome", A1909SolicServicoReqNeg_Nome);
            A1910SolicServicoReqNeg_Descricao = T004T6_A1910SolicServicoReqNeg_Descricao[0];
            A1911SolicServicoReqNeg_Prioridade = T004T6_A1911SolicServicoReqNeg_Prioridade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)));
            A1912SolicServicoReqNeg_Agrupador = T004T6_A1912SolicServicoReqNeg_Agrupador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1912SolicServicoReqNeg_Agrupador", A1912SolicServicoReqNeg_Agrupador);
            A1913SolicServicoReqNeg_Tamanho = T004T6_A1913SolicServicoReqNeg_Tamanho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1913SolicServicoReqNeg_Tamanho", StringUtil.LTrim( StringUtil.Str( A1913SolicServicoReqNeg_Tamanho, 7, 3)));
            A1914SolicServicoReqNeg_Situacao = T004T6_A1914SolicServicoReqNeg_Situacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
            A456ContagemResultado_Codigo = T004T6_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A1947SolicServicoReqNeg_ReqNeqCod = T004T6_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = T004T6_n1947SolicServicoReqNeg_ReqNeqCod[0];
            ZM4T214( -3) ;
         }
         pr_default.close(4);
         OnLoadActions4T214( ) ;
      }

      protected void OnLoadActions4T214( )
      {
      }

      protected void CheckExtendedTable4T214( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T004T4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( ( A1911SolicServicoReqNeg_Prioridade == 1 ) || ( A1911SolicServicoReqNeg_Prioridade == 2 ) || ( A1911SolicServicoReqNeg_Prioridade == 3 ) || ( A1911SolicServicoReqNeg_Prioridade == 4 ) || ( A1911SolicServicoReqNeg_Prioridade == 5 ) || ( A1911SolicServicoReqNeg_Prioridade == 6 ) || ( A1911SolicServicoReqNeg_Prioridade == 7 ) || ( A1911SolicServicoReqNeg_Prioridade == 8 ) || ( A1911SolicServicoReqNeg_Prioridade == 9 ) ) )
         {
            GX_msglist.addItem("Campo Prioridade fora do intervalo", "OutOfRange", 1, "SOLICSERVICOREQNEG_PRIORIDADE");
            AnyError = 1;
            GX_FocusControl = cmbSolicServicoReqNeg_Prioridade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A1914SolicServicoReqNeg_Situacao == 1 ) || ( A1914SolicServicoReqNeg_Situacao == 2 ) || ( A1914SolicServicoReqNeg_Situacao == 3 ) || ( A1914SolicServicoReqNeg_Situacao == 4 ) || ( A1914SolicServicoReqNeg_Situacao == 5 ) ) )
         {
            GX_msglist.addItem("Campo Situa��o fora do intervalo", "OutOfRange", 1, "SOLICSERVICOREQNEG_SITUACAO");
            AnyError = 1;
            GX_FocusControl = cmbSolicServicoReqNeg_Situacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4T214( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_4( int A456ContagemResultado_Codigo )
      {
         /* Using cursor T004T7 */
         pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey4T214( )
      {
         /* Using cursor T004T8 */
         pr_default.execute(6, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound214 = 1;
         }
         else
         {
            RcdFound214 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004T3 */
         pr_default.execute(1, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4T214( 3) ;
            RcdFound214 = 1;
            A1895SolicServicoReqNeg_Codigo = T004T3_A1895SolicServicoReqNeg_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            A1915SolicServicoReqNeg_Identificador = T004T3_A1915SolicServicoReqNeg_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1915SolicServicoReqNeg_Identificador", A1915SolicServicoReqNeg_Identificador);
            A1909SolicServicoReqNeg_Nome = T004T3_A1909SolicServicoReqNeg_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1909SolicServicoReqNeg_Nome", A1909SolicServicoReqNeg_Nome);
            A1910SolicServicoReqNeg_Descricao = T004T3_A1910SolicServicoReqNeg_Descricao[0];
            A1911SolicServicoReqNeg_Prioridade = T004T3_A1911SolicServicoReqNeg_Prioridade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)));
            A1912SolicServicoReqNeg_Agrupador = T004T3_A1912SolicServicoReqNeg_Agrupador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1912SolicServicoReqNeg_Agrupador", A1912SolicServicoReqNeg_Agrupador);
            A1913SolicServicoReqNeg_Tamanho = T004T3_A1913SolicServicoReqNeg_Tamanho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1913SolicServicoReqNeg_Tamanho", StringUtil.LTrim( StringUtil.Str( A1913SolicServicoReqNeg_Tamanho, 7, 3)));
            A1914SolicServicoReqNeg_Situacao = T004T3_A1914SolicServicoReqNeg_Situacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
            A456ContagemResultado_Codigo = T004T3_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A1947SolicServicoReqNeg_ReqNeqCod = T004T3_A1947SolicServicoReqNeg_ReqNeqCod[0];
            n1947SolicServicoReqNeg_ReqNeqCod = T004T3_n1947SolicServicoReqNeg_ReqNeqCod[0];
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            sMode214 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4T214( ) ;
            if ( AnyError == 1 )
            {
               RcdFound214 = 0;
               InitializeNonKey4T214( ) ;
            }
            Gx_mode = sMode214;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound214 = 0;
            InitializeNonKey4T214( ) ;
            sMode214 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode214;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound214 = 0;
         /* Using cursor T004T9 */
         pr_default.execute(7, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T004T9_A1895SolicServicoReqNeg_Codigo[0] < A1895SolicServicoReqNeg_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T004T9_A1895SolicServicoReqNeg_Codigo[0] > A1895SolicServicoReqNeg_Codigo ) ) )
            {
               A1895SolicServicoReqNeg_Codigo = T004T9_A1895SolicServicoReqNeg_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               RcdFound214 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound214 = 0;
         /* Using cursor T004T10 */
         pr_default.execute(8, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004T10_A1895SolicServicoReqNeg_Codigo[0] > A1895SolicServicoReqNeg_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004T10_A1895SolicServicoReqNeg_Codigo[0] < A1895SolicServicoReqNeg_Codigo ) ) )
            {
               A1895SolicServicoReqNeg_Codigo = T004T10_A1895SolicServicoReqNeg_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               RcdFound214 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4T214( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4T214( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound214 == 1 )
            {
               if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
               {
                  A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4T214( ) ;
                  GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4T214( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SOLICSERVICOREQNEG_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4T214( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo )
         {
            A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4T214( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4T214( ) ;
         if ( RcdFound214 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound214 != 0 )
            {
               ScanNext4T214( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4T214( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4T214( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004T2 */
            pr_default.execute(0, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoServicoReqNegocio"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1915SolicServicoReqNeg_Identificador, T004T2_A1915SolicServicoReqNeg_Identificador[0]) != 0 ) || ( StringUtil.StrCmp(Z1909SolicServicoReqNeg_Nome, T004T2_A1909SolicServicoReqNeg_Nome[0]) != 0 ) || ( Z1911SolicServicoReqNeg_Prioridade != T004T2_A1911SolicServicoReqNeg_Prioridade[0] ) || ( StringUtil.StrCmp(Z1912SolicServicoReqNeg_Agrupador, T004T2_A1912SolicServicoReqNeg_Agrupador[0]) != 0 ) || ( Z1913SolicServicoReqNeg_Tamanho != T004T2_A1913SolicServicoReqNeg_Tamanho[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1914SolicServicoReqNeg_Situacao != T004T2_A1914SolicServicoReqNeg_Situacao[0] ) || ( Z456ContagemResultado_Codigo != T004T2_A456ContagemResultado_Codigo[0] ) || ( Z1947SolicServicoReqNeg_ReqNeqCod != T004T2_A1947SolicServicoReqNeg_ReqNeqCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z1915SolicServicoReqNeg_Identificador, T004T2_A1915SolicServicoReqNeg_Identificador[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_Identificador");
                  GXUtil.WriteLogRaw("Old: ",Z1915SolicServicoReqNeg_Identificador);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1915SolicServicoReqNeg_Identificador[0]);
               }
               if ( StringUtil.StrCmp(Z1909SolicServicoReqNeg_Nome, T004T2_A1909SolicServicoReqNeg_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1909SolicServicoReqNeg_Nome);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1909SolicServicoReqNeg_Nome[0]);
               }
               if ( Z1911SolicServicoReqNeg_Prioridade != T004T2_A1911SolicServicoReqNeg_Prioridade[0] )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_Prioridade");
                  GXUtil.WriteLogRaw("Old: ",Z1911SolicServicoReqNeg_Prioridade);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1911SolicServicoReqNeg_Prioridade[0]);
               }
               if ( StringUtil.StrCmp(Z1912SolicServicoReqNeg_Agrupador, T004T2_A1912SolicServicoReqNeg_Agrupador[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_Agrupador");
                  GXUtil.WriteLogRaw("Old: ",Z1912SolicServicoReqNeg_Agrupador);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1912SolicServicoReqNeg_Agrupador[0]);
               }
               if ( Z1913SolicServicoReqNeg_Tamanho != T004T2_A1913SolicServicoReqNeg_Tamanho[0] )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_Tamanho");
                  GXUtil.WriteLogRaw("Old: ",Z1913SolicServicoReqNeg_Tamanho);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1913SolicServicoReqNeg_Tamanho[0]);
               }
               if ( Z1914SolicServicoReqNeg_Situacao != T004T2_A1914SolicServicoReqNeg_Situacao[0] )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_Situacao");
                  GXUtil.WriteLogRaw("Old: ",Z1914SolicServicoReqNeg_Situacao);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1914SolicServicoReqNeg_Situacao[0]);
               }
               if ( Z456ContagemResultado_Codigo != T004T2_A456ContagemResultado_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"ContagemResultado_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z456ContagemResultado_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A456ContagemResultado_Codigo[0]);
               }
               if ( Z1947SolicServicoReqNeg_ReqNeqCod != T004T2_A1947SolicServicoReqNeg_ReqNeqCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservicoreqnegocio:[seudo value changed for attri]"+"SolicServicoReqNeg_ReqNeqCod");
                  GXUtil.WriteLogRaw("Old: ",Z1947SolicServicoReqNeg_ReqNeqCod);
                  GXUtil.WriteLogRaw("Current: ",T004T2_A1947SolicServicoReqNeg_ReqNeqCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SolicitacaoServicoReqNegocio"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4T214( )
      {
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4T214( 0) ;
            CheckOptimisticConcurrency4T214( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4T214( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4T214( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004T11 */
                     pr_default.execute(9, new Object[] {A1915SolicServicoReqNeg_Identificador, A1909SolicServicoReqNeg_Nome, A1910SolicServicoReqNeg_Descricao, A1911SolicServicoReqNeg_Prioridade, A1912SolicServicoReqNeg_Agrupador, A1913SolicServicoReqNeg_Tamanho, A1914SolicServicoReqNeg_Situacao, A456ContagemResultado_Codigo, n1947SolicServicoReqNeg_ReqNeqCod, A1947SolicServicoReqNeg_ReqNeqCod});
                     A1895SolicServicoReqNeg_Codigo = T004T11_A1895SolicServicoReqNeg_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoServicoReqNegocio") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4T0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4T214( ) ;
            }
            EndLevel4T214( ) ;
         }
         CloseExtendedTableCursors4T214( ) ;
      }

      protected void Update4T214( )
      {
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4T214( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4T214( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4T214( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004T12 */
                     pr_default.execute(10, new Object[] {A1915SolicServicoReqNeg_Identificador, A1909SolicServicoReqNeg_Nome, A1910SolicServicoReqNeg_Descricao, A1911SolicServicoReqNeg_Prioridade, A1912SolicServicoReqNeg_Agrupador, A1913SolicServicoReqNeg_Tamanho, A1914SolicServicoReqNeg_Situacao, A456ContagemResultado_Codigo, n1947SolicServicoReqNeg_ReqNeqCod, A1947SolicServicoReqNeg_ReqNeqCod, A1895SolicServicoReqNeg_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoServicoReqNegocio") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoServicoReqNegocio"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4T214( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4T0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4T214( ) ;
         }
         CloseExtendedTableCursors4T214( ) ;
      }

      protected void DeferredUpdate4T214( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4T214( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4T214( ) ;
            AfterConfirm4T214( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4T214( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004T13 */
                  pr_default.execute(11, new Object[] {A1895SolicServicoReqNeg_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoServicoReqNegocio") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound214 == 0 )
                        {
                           InitAll4T214( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4T0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode214 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4T214( ) ;
         Gx_mode = sMode214;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4T214( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004T14 */
            pr_default.execute(12, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Requisitos de Neg�cio da Solicita��o de Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T004T15 */
            pr_default.execute(13, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Req Neg Req Tec"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel4T214( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4T214( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "SolicitacaoServicoReqNegocio");
            if ( AnyError == 0 )
            {
               ConfirmValues4T0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "SolicitacaoServicoReqNegocio");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4T214( )
      {
         /* Scan By routine */
         /* Using cursor T004T16 */
         pr_default.execute(14);
         RcdFound214 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound214 = 1;
            A1895SolicServicoReqNeg_Codigo = T004T16_A1895SolicServicoReqNeg_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4T214( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound214 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound214 = 1;
            A1895SolicServicoReqNeg_Codigo = T004T16_A1895SolicServicoReqNeg_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
         }
      }

      protected void ScanEnd4T214( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm4T214( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4T214( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4T214( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4T214( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4T214( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4T214( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4T214( )
      {
         edtSolicServicoReqNeg_Identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicServicoReqNeg_Identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicServicoReqNeg_Identificador_Enabled), 5, 0)));
         edtSolicServicoReqNeg_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicServicoReqNeg_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicServicoReqNeg_Nome_Enabled), 5, 0)));
         cmbSolicServicoReqNeg_Prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicServicoReqNeg_Prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSolicServicoReqNeg_Prioridade.Enabled), 5, 0)));
         edtSolicServicoReqNeg_Agrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicServicoReqNeg_Agrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicServicoReqNeg_Agrupador_Enabled), 5, 0)));
         edtSolicServicoReqNeg_Tamanho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicServicoReqNeg_Tamanho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicServicoReqNeg_Tamanho_Enabled), 5, 0)));
         cmbSolicServicoReqNeg_Situacao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicServicoReqNeg_Situacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSolicServicoReqNeg_Situacao.Enabled), 5, 0)));
         edtSolicServicoReqNeg_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicServicoReqNeg_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicServicoReqNeg_Codigo_Enabled), 5, 0)));
         edtContagemResultado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4T0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120452998");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaoservicoreqnegocio.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1915SolicServicoReqNeg_Identificador", Z1915SolicServicoReqNeg_Identificador);
         GxWebStd.gx_hidden_field( context, "Z1909SolicServicoReqNeg_Nome", Z1909SolicServicoReqNeg_Nome);
         GxWebStd.gx_hidden_field( context, "Z1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1911SolicServicoReqNeg_Prioridade), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1912SolicServicoReqNeg_Agrupador", Z1912SolicServicoReqNeg_Agrupador);
         GxWebStd.gx_hidden_field( context, "Z1913SolicServicoReqNeg_Tamanho", StringUtil.LTrim( StringUtil.NToC( Z1913SolicServicoReqNeg_Tamanho, 7, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1914SolicServicoReqNeg_Situacao), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1947SolicServicoReqNeg_ReqNeqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1947SolicServicoReqNeg_ReqNeqCod), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "SOLICSERVICOREQNEG_DESCRICAO", A1910SolicServicoReqNeg_Descricao);
         GxWebStd.gx_hidden_field( context, "SOLICSERVICOREQNEG_REQNEQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "SolicitacaoServicoReqNegocio";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), "ZZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("solicitacaoservicoreqnegocio:[SendSecurityCheck value for]"+"SolicServicoReqNeg_ReqNeqCod:"+context.localUtil.Format( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), "ZZZZZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacaoservicoreqnegocio.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "SolicitacaoServicoReqNegocio" ;
      }

      public override String GetPgmdesc( )
      {
         return "Requisitos de Neg�cio da Solicita��o de Servi�o" ;
      }

      protected void InitializeNonKey4T214( )
      {
         A456ContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         A1915SolicServicoReqNeg_Identificador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1915SolicServicoReqNeg_Identificador", A1915SolicServicoReqNeg_Identificador);
         A1909SolicServicoReqNeg_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1909SolicServicoReqNeg_Nome", A1909SolicServicoReqNeg_Nome);
         A1910SolicServicoReqNeg_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1910SolicServicoReqNeg_Descricao", A1910SolicServicoReqNeg_Descricao);
         A1911SolicServicoReqNeg_Prioridade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1911SolicServicoReqNeg_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0)));
         A1912SolicServicoReqNeg_Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1912SolicServicoReqNeg_Agrupador", A1912SolicServicoReqNeg_Agrupador);
         A1913SolicServicoReqNeg_Tamanho = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1913SolicServicoReqNeg_Tamanho", StringUtil.LTrim( StringUtil.Str( A1913SolicServicoReqNeg_Tamanho, 7, 3)));
         A1914SolicServicoReqNeg_Situacao = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
         A1947SolicServicoReqNeg_ReqNeqCod = 0;
         n1947SolicServicoReqNeg_ReqNeqCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1947SolicServicoReqNeg_ReqNeqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), 10, 0)));
         Z1915SolicServicoReqNeg_Identificador = "";
         Z1909SolicServicoReqNeg_Nome = "";
         Z1911SolicServicoReqNeg_Prioridade = 0;
         Z1912SolicServicoReqNeg_Agrupador = "";
         Z1913SolicServicoReqNeg_Tamanho = 0;
         Z1914SolicServicoReqNeg_Situacao = 0;
         Z456ContagemResultado_Codigo = 0;
         Z1947SolicServicoReqNeg_ReqNeqCod = 0;
      }

      protected void InitAll4T214( )
      {
         A1895SolicServicoReqNeg_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
         InitializeNonKey4T214( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120453011");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacaoservicoreqnegocio.js", "?20203120453011");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksolicservicoreqneg_identificador_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_IDENTIFICADOR";
         edtSolicServicoReqNeg_Identificador_Internalname = "SOLICSERVICOREQNEG_IDENTIFICADOR";
         lblTextblocksolicservicoreqneg_nome_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_NOME";
         edtSolicServicoReqNeg_Nome_Internalname = "SOLICSERVICOREQNEG_NOME";
         lblTextblocksolicservicoreqneg_prioridade_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_PRIORIDADE";
         cmbSolicServicoReqNeg_Prioridade_Internalname = "SOLICSERVICOREQNEG_PRIORIDADE";
         lblTextblocksolicservicoreqneg_agrupador_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_AGRUPADOR";
         edtSolicServicoReqNeg_Agrupador_Internalname = "SOLICSERVICOREQNEG_AGRUPADOR";
         lblTextblocksolicservicoreqneg_tamanho_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_TAMANHO";
         edtSolicServicoReqNeg_Tamanho_Internalname = "SOLICSERVICOREQNEG_TAMANHO";
         lblTextblocksolicservicoreqneg_situacao_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_SITUACAO";
         cmbSolicServicoReqNeg_Situacao_Internalname = "SOLICSERVICOREQNEG_SITUACAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtSolicServicoReqNeg_Codigo_Internalname = "SOLICSERVICOREQNEG_CODIGO";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Requisito de Neg�cio";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Requisitos de Neg�cio da Solicita��o de Servi�o";
         cmbSolicServicoReqNeg_Situacao_Jsonclick = "";
         cmbSolicServicoReqNeg_Situacao.Enabled = 1;
         edtSolicServicoReqNeg_Tamanho_Jsonclick = "";
         edtSolicServicoReqNeg_Tamanho_Enabled = 1;
         edtSolicServicoReqNeg_Agrupador_Jsonclick = "";
         edtSolicServicoReqNeg_Agrupador_Enabled = 1;
         cmbSolicServicoReqNeg_Prioridade_Jsonclick = "";
         cmbSolicServicoReqNeg_Prioridade.Enabled = 1;
         edtSolicServicoReqNeg_Nome_Enabled = 1;
         edtSolicServicoReqNeg_Identificador_Jsonclick = "";
         edtSolicServicoReqNeg_Identificador_Enabled = 1;
         bttBtn_trn_delete_Enabled = 1;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Enabled = 1;
         edtSolicServicoReqNeg_Codigo_Jsonclick = "";
         edtSolicServicoReqNeg_Codigo_Enabled = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtSolicServicoReqNeg_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Solicservicoreqneg_codigo( long GX_Parm1 ,
                                                   String GX_Parm2 ,
                                                   String GX_Parm3 ,
                                                   String GX_Parm4 ,
                                                   GXCombobox cmbGX_Parm5 ,
                                                   String GX_Parm6 ,
                                                   decimal GX_Parm7 ,
                                                   GXCombobox cmbGX_Parm8 ,
                                                   int GX_Parm9 ,
                                                   long GX_Parm10 )
      {
         A1895SolicServicoReqNeg_Codigo = GX_Parm1;
         A1915SolicServicoReqNeg_Identificador = GX_Parm2;
         A1909SolicServicoReqNeg_Nome = GX_Parm3;
         A1910SolicServicoReqNeg_Descricao = GX_Parm4;
         cmbSolicServicoReqNeg_Prioridade = cmbGX_Parm5;
         A1911SolicServicoReqNeg_Prioridade = (short)(NumberUtil.Val( cmbSolicServicoReqNeg_Prioridade.CurrentValue, "."));
         cmbSolicServicoReqNeg_Prioridade.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0));
         A1912SolicServicoReqNeg_Agrupador = GX_Parm6;
         A1913SolicServicoReqNeg_Tamanho = GX_Parm7;
         cmbSolicServicoReqNeg_Situacao = cmbGX_Parm8;
         A1914SolicServicoReqNeg_Situacao = (short)(NumberUtil.Val( cmbSolicServicoReqNeg_Situacao.CurrentValue, "."));
         cmbSolicServicoReqNeg_Situacao.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0));
         A456ContagemResultado_Codigo = GX_Parm9;
         A1947SolicServicoReqNeg_ReqNeqCod = GX_Parm10;
         n1947SolicServicoReqNeg_ReqNeqCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(A1915SolicServicoReqNeg_Identificador);
         isValidOutput.Add(A1909SolicServicoReqNeg_Nome);
         isValidOutput.Add(A1910SolicServicoReqNeg_Descricao);
         cmbSolicServicoReqNeg_Prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1911SolicServicoReqNeg_Prioridade), 2, 0));
         isValidOutput.Add(cmbSolicServicoReqNeg_Prioridade);
         isValidOutput.Add(A1912SolicServicoReqNeg_Agrupador);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1913SolicServicoReqNeg_Tamanho, 7, 3, ".", "")));
         cmbSolicServicoReqNeg_Situacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0));
         isValidOutput.Add(cmbSolicServicoReqNeg_Situacao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1947SolicServicoReqNeg_ReqNeqCod), 10, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z1915SolicServicoReqNeg_Identificador);
         isValidOutput.Add(Z1909SolicServicoReqNeg_Nome);
         isValidOutput.Add(Z1910SolicServicoReqNeg_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1911SolicServicoReqNeg_Prioridade), 2, 0, ",", "")));
         isValidOutput.Add(Z1912SolicServicoReqNeg_Agrupador);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z1913SolicServicoReqNeg_Tamanho, 7, 3, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1914SolicServicoReqNeg_Situacao), 2, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1947SolicServicoReqNeg_ReqNeqCod), 10, 0, ",", "")));
         isValidOutput.Add(bttBtn_trn_delete_Enabled);
         isValidOutput.Add(bttBtn_trn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_codigo( int GX_Parm1 )
      {
         A456ContagemResultado_Codigo = GX_Parm1;
         /* Using cursor T004T17 */
         pr_default.execute(15, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124T2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1915SolicServicoReqNeg_Identificador = "";
         Z1909SolicServicoReqNeg_Nome = "";
         Z1912SolicServicoReqNeg_Agrupador = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocksolicservicoreqneg_identificador_Jsonclick = "";
         A1915SolicServicoReqNeg_Identificador = "";
         lblTextblocksolicservicoreqneg_nome_Jsonclick = "";
         A1909SolicServicoReqNeg_Nome = "";
         lblTextblocksolicservicoreqneg_prioridade_Jsonclick = "";
         lblTextblocksolicservicoreqneg_agrupador_Jsonclick = "";
         A1912SolicServicoReqNeg_Agrupador = "";
         lblTextblocksolicservicoreqneg_tamanho_Jsonclick = "";
         lblTextblocksolicservicoreqneg_situacao_Jsonclick = "";
         Gx_mode = "";
         A1910SolicServicoReqNeg_Descricao = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1910SolicServicoReqNeg_Descricao = "";
         T004T5_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         T004T5_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         T004T6_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T6_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         T004T6_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         T004T6_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         T004T6_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         T004T6_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         T004T6_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         T004T6_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         T004T6_A456ContagemResultado_Codigo = new int[1] ;
         T004T6_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         T004T6_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         T004T4_A456ContagemResultado_Codigo = new int[1] ;
         T004T7_A456ContagemResultado_Codigo = new int[1] ;
         T004T8_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T3_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T3_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         T004T3_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         T004T3_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         T004T3_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         T004T3_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         T004T3_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         T004T3_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         T004T3_A456ContagemResultado_Codigo = new int[1] ;
         T004T3_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         T004T3_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         sMode214 = "";
         T004T9_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T10_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T2_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T2_A1915SolicServicoReqNeg_Identificador = new String[] {""} ;
         T004T2_A1909SolicServicoReqNeg_Nome = new String[] {""} ;
         T004T2_A1910SolicServicoReqNeg_Descricao = new String[] {""} ;
         T004T2_A1911SolicServicoReqNeg_Prioridade = new short[1] ;
         T004T2_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         T004T2_A1913SolicServicoReqNeg_Tamanho = new decimal[1] ;
         T004T2_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         T004T2_A456ContagemResultado_Codigo = new int[1] ;
         T004T2_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         T004T2_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         T004T11_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T14_A1947SolicServicoReqNeg_ReqNeqCod = new long[1] ;
         T004T14_n1947SolicServicoReqNeg_ReqNeqCod = new bool[] {false} ;
         T004T15_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004T15_A1919Requisito_Codigo = new int[1] ;
         T004T16_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T004T17_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaoservicoreqnegocio__default(),
            new Object[][] {
                new Object[] {
               T004T2_A1895SolicServicoReqNeg_Codigo, T004T2_A1915SolicServicoReqNeg_Identificador, T004T2_A1909SolicServicoReqNeg_Nome, T004T2_A1910SolicServicoReqNeg_Descricao, T004T2_A1911SolicServicoReqNeg_Prioridade, T004T2_A1912SolicServicoReqNeg_Agrupador, T004T2_A1913SolicServicoReqNeg_Tamanho, T004T2_A1914SolicServicoReqNeg_Situacao, T004T2_A456ContagemResultado_Codigo, T004T2_A1947SolicServicoReqNeg_ReqNeqCod,
               T004T2_n1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               T004T3_A1895SolicServicoReqNeg_Codigo, T004T3_A1915SolicServicoReqNeg_Identificador, T004T3_A1909SolicServicoReqNeg_Nome, T004T3_A1910SolicServicoReqNeg_Descricao, T004T3_A1911SolicServicoReqNeg_Prioridade, T004T3_A1912SolicServicoReqNeg_Agrupador, T004T3_A1913SolicServicoReqNeg_Tamanho, T004T3_A1914SolicServicoReqNeg_Situacao, T004T3_A456ContagemResultado_Codigo, T004T3_A1947SolicServicoReqNeg_ReqNeqCod,
               T004T3_n1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               T004T4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004T5_A1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               T004T6_A1895SolicServicoReqNeg_Codigo, T004T6_A1915SolicServicoReqNeg_Identificador, T004T6_A1909SolicServicoReqNeg_Nome, T004T6_A1910SolicServicoReqNeg_Descricao, T004T6_A1911SolicServicoReqNeg_Prioridade, T004T6_A1912SolicServicoReqNeg_Agrupador, T004T6_A1913SolicServicoReqNeg_Tamanho, T004T6_A1914SolicServicoReqNeg_Situacao, T004T6_A456ContagemResultado_Codigo, T004T6_A1947SolicServicoReqNeg_ReqNeqCod,
               T004T6_n1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               T004T7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004T8_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004T9_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004T10_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004T11_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004T14_A1947SolicServicoReqNeg_ReqNeqCod
               }
               , new Object[] {
               T004T15_A1895SolicServicoReqNeg_Codigo, T004T15_A1919Requisito_Codigo
               }
               , new Object[] {
               T004T16_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004T17_A456ContagemResultado_Codigo
               }
            }
         );
      }

      private short Z1911SolicServicoReqNeg_Prioridade ;
      private short Z1914SolicServicoReqNeg_Situacao ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1911SolicServicoReqNeg_Prioridade ;
      private short A1914SolicServicoReqNeg_Situacao ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound214 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z456ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int trnEnded ;
      private int edtSolicServicoReqNeg_Codigo_Enabled ;
      private int edtContagemResultado_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtSolicServicoReqNeg_Identificador_Enabled ;
      private int edtSolicServicoReqNeg_Nome_Enabled ;
      private int edtSolicServicoReqNeg_Agrupador_Enabled ;
      private int edtSolicServicoReqNeg_Tamanho_Enabled ;
      private int idxLst ;
      private long Z1895SolicServicoReqNeg_Codigo ;
      private long Z1947SolicServicoReqNeg_ReqNeqCod ;
      private long A1895SolicServicoReqNeg_Codigo ;
      private long A1947SolicServicoReqNeg_ReqNeqCod ;
      private decimal Z1913SolicServicoReqNeg_Tamanho ;
      private decimal A1913SolicServicoReqNeg_Tamanho ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSolicServicoReqNeg_Identificador_Internalname ;
      private String TempTags ;
      private String edtSolicServicoReqNeg_Codigo_Internalname ;
      private String edtSolicServicoReqNeg_Codigo_Jsonclick ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksolicservicoreqneg_identificador_Internalname ;
      private String lblTextblocksolicservicoreqneg_identificador_Jsonclick ;
      private String edtSolicServicoReqNeg_Identificador_Jsonclick ;
      private String lblTextblocksolicservicoreqneg_nome_Internalname ;
      private String lblTextblocksolicservicoreqneg_nome_Jsonclick ;
      private String edtSolicServicoReqNeg_Nome_Internalname ;
      private String lblTextblocksolicservicoreqneg_prioridade_Internalname ;
      private String lblTextblocksolicservicoreqneg_prioridade_Jsonclick ;
      private String cmbSolicServicoReqNeg_Prioridade_Internalname ;
      private String cmbSolicServicoReqNeg_Prioridade_Jsonclick ;
      private String lblTextblocksolicservicoreqneg_agrupador_Internalname ;
      private String lblTextblocksolicservicoreqneg_agrupador_Jsonclick ;
      private String edtSolicServicoReqNeg_Agrupador_Internalname ;
      private String edtSolicServicoReqNeg_Agrupador_Jsonclick ;
      private String lblTextblocksolicservicoreqneg_tamanho_Internalname ;
      private String lblTextblocksolicservicoreqneg_tamanho_Jsonclick ;
      private String edtSolicServicoReqNeg_Tamanho_Internalname ;
      private String edtSolicServicoReqNeg_Tamanho_Jsonclick ;
      private String lblTextblocksolicservicoreqneg_situacao_Internalname ;
      private String lblTextblocksolicservicoreqneg_situacao_Jsonclick ;
      private String cmbSolicServicoReqNeg_Situacao_Internalname ;
      private String cmbSolicServicoReqNeg_Situacao_Jsonclick ;
      private String Gx_mode ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode214 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1947SolicServicoReqNeg_ReqNeqCod ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1910SolicServicoReqNeg_Descricao ;
      private String Z1910SolicServicoReqNeg_Descricao ;
      private String Z1915SolicServicoReqNeg_Identificador ;
      private String Z1909SolicServicoReqNeg_Nome ;
      private String Z1912SolicServicoReqNeg_Agrupador ;
      private String A1915SolicServicoReqNeg_Identificador ;
      private String A1909SolicServicoReqNeg_Nome ;
      private String A1912SolicServicoReqNeg_Agrupador ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbSolicServicoReqNeg_Prioridade ;
      private GXCombobox cmbSolicServicoReqNeg_Situacao ;
      private IDataStoreProvider pr_default ;
      private long[] T004T5_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] T004T5_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] T004T6_A1895SolicServicoReqNeg_Codigo ;
      private String[] T004T6_A1915SolicServicoReqNeg_Identificador ;
      private String[] T004T6_A1909SolicServicoReqNeg_Nome ;
      private String[] T004T6_A1910SolicServicoReqNeg_Descricao ;
      private short[] T004T6_A1911SolicServicoReqNeg_Prioridade ;
      private String[] T004T6_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] T004T6_A1913SolicServicoReqNeg_Tamanho ;
      private short[] T004T6_A1914SolicServicoReqNeg_Situacao ;
      private int[] T004T6_A456ContagemResultado_Codigo ;
      private long[] T004T6_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] T004T6_n1947SolicServicoReqNeg_ReqNeqCod ;
      private int[] T004T4_A456ContagemResultado_Codigo ;
      private int[] T004T7_A456ContagemResultado_Codigo ;
      private long[] T004T8_A1895SolicServicoReqNeg_Codigo ;
      private long[] T004T3_A1895SolicServicoReqNeg_Codigo ;
      private String[] T004T3_A1915SolicServicoReqNeg_Identificador ;
      private String[] T004T3_A1909SolicServicoReqNeg_Nome ;
      private String[] T004T3_A1910SolicServicoReqNeg_Descricao ;
      private short[] T004T3_A1911SolicServicoReqNeg_Prioridade ;
      private String[] T004T3_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] T004T3_A1913SolicServicoReqNeg_Tamanho ;
      private short[] T004T3_A1914SolicServicoReqNeg_Situacao ;
      private int[] T004T3_A456ContagemResultado_Codigo ;
      private long[] T004T3_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] T004T3_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] T004T9_A1895SolicServicoReqNeg_Codigo ;
      private long[] T004T10_A1895SolicServicoReqNeg_Codigo ;
      private long[] T004T2_A1895SolicServicoReqNeg_Codigo ;
      private String[] T004T2_A1915SolicServicoReqNeg_Identificador ;
      private String[] T004T2_A1909SolicServicoReqNeg_Nome ;
      private String[] T004T2_A1910SolicServicoReqNeg_Descricao ;
      private short[] T004T2_A1911SolicServicoReqNeg_Prioridade ;
      private String[] T004T2_A1912SolicServicoReqNeg_Agrupador ;
      private decimal[] T004T2_A1913SolicServicoReqNeg_Tamanho ;
      private short[] T004T2_A1914SolicServicoReqNeg_Situacao ;
      private int[] T004T2_A456ContagemResultado_Codigo ;
      private long[] T004T2_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] T004T2_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] T004T11_A1895SolicServicoReqNeg_Codigo ;
      private long[] T004T14_A1947SolicServicoReqNeg_ReqNeqCod ;
      private bool[] T004T14_n1947SolicServicoReqNeg_ReqNeqCod ;
      private long[] T004T15_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004T15_A1919Requisito_Codigo ;
      private long[] T004T16_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004T17_A456ContagemResultado_Codigo ;
      private GXWebForm Form ;
   }

   public class solicitacaoservicoreqnegocio__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004T5 ;
          prmT004T5 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_ReqNeqCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T6 ;
          prmT004T6 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T4 ;
          prmT004T4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004T7 ;
          prmT004T7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004T8 ;
          prmT004T8 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T3 ;
          prmT004T3 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T9 ;
          prmT004T9 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T10 ;
          prmT004T10 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T2 ;
          prmT004T2 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T11 ;
          prmT004T11 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@SolicServicoReqNeg_Nome",SqlDbType.VarChar,250,0} ,
          new Object[] {"@SolicServicoReqNeg_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@SolicServicoReqNeg_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@SolicServicoReqNeg_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@SolicServicoReqNeg_Tamanho",SqlDbType.Decimal,7,3} ,
          new Object[] {"@SolicServicoReqNeg_Situacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_ReqNeqCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T12 ;
          prmT004T12 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@SolicServicoReqNeg_Nome",SqlDbType.VarChar,250,0} ,
          new Object[] {"@SolicServicoReqNeg_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@SolicServicoReqNeg_Prioridade",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@SolicServicoReqNeg_Agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@SolicServicoReqNeg_Tamanho",SqlDbType.Decimal,7,3} ,
          new Object[] {"@SolicServicoReqNeg_Situacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_ReqNeqCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T13 ;
          prmT004T13 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T14 ;
          prmT004T14 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T15 ;
          prmT004T15 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004T16 ;
          prmT004T16 = new Object[] {
          } ;
          Object[] prmT004T17 ;
          prmT004T17 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004T2", "SELECT [SolicServicoReqNeg_Codigo], [SolicServicoReqNeg_Identificador], [SolicServicoReqNeg_Nome], [SolicServicoReqNeg_Descricao], [SolicServicoReqNeg_Prioridade], [SolicServicoReqNeg_Agrupador], [SolicServicoReqNeg_Tamanho], [SolicServicoReqNeg_Situacao], [ContagemResultado_Codigo], [SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (UPDLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T2,1,0,true,false )
             ,new CursorDef("T004T3", "SELECT [SolicServicoReqNeg_Codigo], [SolicServicoReqNeg_Identificador], [SolicServicoReqNeg_Nome], [SolicServicoReqNeg_Descricao], [SolicServicoReqNeg_Prioridade], [SolicServicoReqNeg_Agrupador], [SolicServicoReqNeg_Tamanho], [SolicServicoReqNeg_Situacao], [ContagemResultado_Codigo], [SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T3,1,0,true,false )
             ,new CursorDef("T004T4", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T4,1,0,true,false )
             ,new CursorDef("T004T5", "SELECT [SolicServicoReqNeg_Codigo] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_ReqNeqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T5,1,0,true,false )
             ,new CursorDef("T004T6", "SELECT TM1.[SolicServicoReqNeg_Codigo], TM1.[SolicServicoReqNeg_Identificador], TM1.[SolicServicoReqNeg_Nome], TM1.[SolicServicoReqNeg_Descricao], TM1.[SolicServicoReqNeg_Prioridade], TM1.[SolicServicoReqNeg_Agrupador], TM1.[SolicServicoReqNeg_Tamanho], TM1.[SolicServicoReqNeg_Situacao], TM1.[ContagemResultado_Codigo], TM1.[SolicServicoReqNeg_ReqNeqCod] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] TM1 WITH (NOLOCK) WHERE TM1.[SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ORDER BY TM1.[SolicServicoReqNeg_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004T6,100,0,true,false )
             ,new CursorDef("T004T7", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T7,1,0,true,false )
             ,new CursorDef("T004T8", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004T8,1,0,true,false )
             ,new CursorDef("T004T9", "SELECT TOP 1 [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE ( [SolicServicoReqNeg_Codigo] > @SolicServicoReqNeg_Codigo) ORDER BY [SolicServicoReqNeg_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004T9,1,0,true,true )
             ,new CursorDef("T004T10", "SELECT TOP 1 [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE ( [SolicServicoReqNeg_Codigo] < @SolicServicoReqNeg_Codigo) ORDER BY [SolicServicoReqNeg_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004T10,1,0,true,true )
             ,new CursorDef("T004T11", "INSERT INTO [SolicitacaoServicoReqNegocio]([SolicServicoReqNeg_Identificador], [SolicServicoReqNeg_Nome], [SolicServicoReqNeg_Descricao], [SolicServicoReqNeg_Prioridade], [SolicServicoReqNeg_Agrupador], [SolicServicoReqNeg_Tamanho], [SolicServicoReqNeg_Situacao], [ContagemResultado_Codigo], [SolicServicoReqNeg_ReqNeqCod]) VALUES(@SolicServicoReqNeg_Identificador, @SolicServicoReqNeg_Nome, @SolicServicoReqNeg_Descricao, @SolicServicoReqNeg_Prioridade, @SolicServicoReqNeg_Agrupador, @SolicServicoReqNeg_Tamanho, @SolicServicoReqNeg_Situacao, @ContagemResultado_Codigo, @SolicServicoReqNeg_ReqNeqCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004T11)
             ,new CursorDef("T004T12", "UPDATE [SolicitacaoServicoReqNegocio] SET [SolicServicoReqNeg_Identificador]=@SolicServicoReqNeg_Identificador, [SolicServicoReqNeg_Nome]=@SolicServicoReqNeg_Nome, [SolicServicoReqNeg_Descricao]=@SolicServicoReqNeg_Descricao, [SolicServicoReqNeg_Prioridade]=@SolicServicoReqNeg_Prioridade, [SolicServicoReqNeg_Agrupador]=@SolicServicoReqNeg_Agrupador, [SolicServicoReqNeg_Tamanho]=@SolicServicoReqNeg_Tamanho, [SolicServicoReqNeg_Situacao]=@SolicServicoReqNeg_Situacao, [ContagemResultado_Codigo]=@ContagemResultado_Codigo, [SolicServicoReqNeg_ReqNeqCod]=@SolicServicoReqNeg_ReqNeqCod  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo", GxErrorMask.GX_NOMASK,prmT004T12)
             ,new CursorDef("T004T13", "DELETE FROM [SolicitacaoServicoReqNegocio]  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo", GxErrorMask.GX_NOMASK,prmT004T13)
             ,new CursorDef("T004T14", "SELECT TOP 1 [SolicServicoReqNeg_Codigo] AS SolicServicoReqNeg_ReqNeqCod FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_ReqNeqCod] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T14,1,0,true,true )
             ,new CursorDef("T004T15", "SELECT TOP 1 [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T15,1,0,true,true )
             ,new CursorDef("T004T16", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) ORDER BY [SolicServicoReqNeg_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004T16,100,0,true,false )
             ,new CursorDef("T004T17", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004T17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((long[]) buf[9])[0] = rslt.getLong(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 8 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 12 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 13 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (long)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (long)parms[9]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (long)parms[9]);
                }
                stmt.SetParameter(10, (long)parms[10]);
                return;
             case 11 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
