/*
               File: PRC_Tabela_PF
        Description: Pontos de Fun��o da Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_tabela_pf : GXProcedure
   {
      public prc_tabela_pf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_tabela_pf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tabela_Codigo ,
                           out decimal aP1_Tabela_PF )
      {
         this.A172Tabela_Codigo = aP0_Tabela_Codigo;
         this.AV8Tabela_PF = 0 ;
         initialize();
         executePrivate();
         aP1_Tabela_PF=this.AV8Tabela_PF;
      }

      public decimal executeUdp( int aP0_Tabela_Codigo )
      {
         this.A172Tabela_Codigo = aP0_Tabela_Codigo;
         this.AV8Tabela_PF = 0 ;
         initialize();
         executePrivate();
         aP1_Tabela_PF=this.AV8Tabela_PF;
         return AV8Tabela_PF ;
      }

      public void executeSubmit( int aP0_Tabela_Codigo ,
                                 out decimal aP1_Tabela_PF )
      {
         prc_tabela_pf objprc_tabela_pf;
         objprc_tabela_pf = new prc_tabela_pf();
         objprc_tabela_pf.A172Tabela_Codigo = aP0_Tabela_Codigo;
         objprc_tabela_pf.AV8Tabela_PF = 0 ;
         objprc_tabela_pf.context.SetSubmitInitialConfig(context);
         objprc_tabela_pf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_tabela_pf);
         aP1_Tabela_PF=this.AV8Tabela_PF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_tabela_pf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Tabela_PF = 0;
         /* Using cursor P00252 */
         pr_default.execute(0, new Object[] {A172Tabela_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A394FuncaoDados_Ativo = P00252_A394FuncaoDados_Ativo[0];
            A755FuncaoDados_Contar = P00252_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00252_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00252_A368FuncaoDados_Codigo[0];
            A394FuncaoDados_Ativo = P00252_A394FuncaoDados_Ativo[0];
            A755FuncaoDados_Contar = P00252_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00252_n755FuncaoDados_Contar[0];
            if ( A755FuncaoDados_Contar )
            {
               GXt_int1 = (short)(A377FuncaoDados_PF);
               new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
               A377FuncaoDados_PF = (decimal)(GXt_int1);
            }
            else
            {
               A377FuncaoDados_PF = 0;
            }
            AV8Tabela_PF = (decimal)(AV8Tabela_PF+A377FuncaoDados_PF);
            /* Using cursor P00253 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A165FuncaoAPF_Codigo = P00253_A165FuncaoAPF_Codigo[0];
               GXt_decimal2 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal2) ;
               A386FuncaoAPF_PF = GXt_decimal2;
               /* Using cursor P00254 */
               pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  AV8Tabela_PF = (decimal)(AV8Tabela_PF+A386FuncaoAPF_PF);
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00252_A172Tabela_Codigo = new int[1] ;
         P00252_A394FuncaoDados_Ativo = new String[] {""} ;
         P00252_A755FuncaoDados_Contar = new bool[] {false} ;
         P00252_n755FuncaoDados_Contar = new bool[] {false} ;
         P00252_A368FuncaoDados_Codigo = new int[1] ;
         A394FuncaoDados_Ativo = "";
         P00253_A165FuncaoAPF_Codigo = new int[1] ;
         P00254_A165FuncaoAPF_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_tabela_pf__default(),
            new Object[][] {
                new Object[] {
               P00252_A172Tabela_Codigo, P00252_A394FuncaoDados_Ativo, P00252_A755FuncaoDados_Contar, P00252_n755FuncaoDados_Contar, P00252_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P00253_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P00254_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXt_int1 ;
      private int A172Tabela_Codigo ;
      private int A368FuncaoDados_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private decimal AV8Tabela_PF ;
      private decimal A377FuncaoDados_PF ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal2 ;
      private String scmdbuf ;
      private String A394FuncaoDados_Ativo ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00252_A172Tabela_Codigo ;
      private String[] P00252_A394FuncaoDados_Ativo ;
      private bool[] P00252_A755FuncaoDados_Contar ;
      private bool[] P00252_n755FuncaoDados_Contar ;
      private int[] P00252_A368FuncaoDados_Codigo ;
      private int[] P00253_A165FuncaoAPF_Codigo ;
      private int[] P00254_A165FuncaoAPF_Codigo ;
      private decimal aP1_Tabela_PF ;
   }

   public class prc_tabela_pf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00252 ;
          prmP00252 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00253 ;
          prmP00253 = new Object[] {
          } ;
          Object[] prmP00254 ;
          prmP00254 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00252", "SELECT T1.[Tabela_Codigo], T2.[FuncaoDados_Ativo], T2.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo] FROM ([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_Codigo]) WHERE (T1.[Tabela_Codigo] = @Tabela_Codigo) AND (T2.[FuncaoDados_Ativo] = 'A') ORDER BY T1.[Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00252,100,0,true,false )
             ,new CursorDef("P00253", "SELECT DISTINCT [FuncaoAPF_Codigo] FROM [FuncoesAPFAtributos] WITH (NOLOCK) ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00253,100,0,true,false )
             ,new CursorDef("P00254", "SELECT [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00254,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
